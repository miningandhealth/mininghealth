\documentclass[pdf]{beamer}
\usepackage{booktabs}
\usepackage{tabularx,colortbl}
\usepackage{multirow}
\usepackage[export]{adjustbox}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{comment}
\usepackage{wrapfig}
\usepackage{dsfont}
\usepackage{bbm}
\usepackage[flushleft]{threeparttable}      % Option: Table notes
\usepackage{floatpag}
\usepackage{wrapfig}
\usepackage{relsize}
\usepackage{setspace}  
\usepackage{apacite}  


\usepackage{nameref}
\usepackage{float}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{latexsym}
\usepackage{amsmath,amsthm}
\usepackage{amsfonts}

\usepackage{apacite}

                   
\mode<presentation>{}
\usetheme{Warsaw}


\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}
 \newcommand{\specialcell}[2][c]{%
    \begin{tabular}[#1]{@{}c@{}}#2\end{tabular}
}
\expandafter\def\expandafter\insertshorttitle\expandafter{%
  \insertshorttitle\hfill%
  \insertframenumber\,/\,\inserttotalframenumber}
\title[Mining and health]{The hidden health costs and benefits of mining}

\author[Romero-Saavedra]{Mauricio Romero (UC - San Diego)  \\ Santiago Saavedra (Stanford)  }

\normalsize

\date{September 24th 2014}




\begin{document}

\frame{\titlepage}
 
\AtBeginSection[]
{
   \begin{frame}
       \frametitle{Table of Contents}
       \tableofcontents[currentsection]
   \end{frame}
}

\section{Motivation and context}

%\begin{frame}[plain]
%
%\frametitle{Motivation}

%\begin{figure}[H]
%\begin{center}
%
%\includegraphics[scale=0.7]{M2003.png} \quad
%\includegraphics[scale=0.7]{M2008.png}
%\\
%\caption{Location of gold mines in Colombia 2003 vs 2008}
%\footnotesize{Source: Tierra Minada}
%\end{center}
%
%\label{Motiv_map}
%\end{figure}

%\end{frame}

\begin{frame}[plain]

\frametitle{Motivation}

\begin{figure}[H]
\begin{center}
\includegraphics[width=0.45\textwidth]{Prop_2000.pdf} \quad
\includegraphics[width=0.45\textwidth]{Prop_2013.pdf} 
\caption{ \scriptsize{Proportion of area mined in each municipality. Each panel presents the proportion of the area in a municipality that is part of a mining title. Source: Tierra Minada. Calculations: Authors.}}
\label{fig:evol_mineria}
\end{center}
\end{figure}

\end{frame}



\begin{frame}
\frametitle{Recent mining boom}
\begin{itemize}
\item Number of gold permits given from 2004-2008, doubled the number allocated from 1990-2004.
\item Government expects gold extraction to increase over 70\% by 2020 \cite{Minas2011}.
\item Mercury used for amalgamation is dropped in water sources.
\item Besides drinking water, mercury is ingested by humans through fish consumption.
\item Evidence that EPA mercury limits are exceeded in Colombia near mining areas \cite{GUIZA2013,Olivero2002}.
\item But not historical measurements of mercury in all municipalities.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Research Objectives}
\textbf {General objective:} Quantify the net impacts of gold mining on the health of surrounding population. 

\begin{enumerate}
\item Quantify the effect of gold mining on the APGAR score, gestation period, weight and height of newborns.
\item Separately identify, if possible, income and pollution effects using fish consumption.
\item Estimate how the effects of vary by the timing of in utero exposure to methyl-mercury. 
\item Estimate how the net health effects of mining vary by different mine types: large scale vs. small scale, legal vs. illegal. 
\item Compare the health costs with the income and royalties derived from mining.
\end{enumerate}
\end{frame}

\begin{frame}
\frametitle{Research Question}
What are the health costs/benefits of gold mining ?
\begin{itemize}
\item Diff-in-diff strategy using to calculate \textbf{reduced form estimates} (+income-pollution) effects of mining.
\item ``Treatment intensity'' depending on size of the mine
\item To isolate ``pollution effect'' we plan to:
\begin{enumerate}
\item Use spatial variation: municipalities upstream/downstream from the mines.
\item Use holyweek as an instrument for exposure to mercury (through fish consumption).
\end{enumerate}
\item Measure the impact on newborns and morbidity.
\end{itemize}
\end{frame}


\section{Brief Literature Review}

\begin{frame}
\frametitle{Related Economics literature}
\begin{itemize}
\item Literature on mining has focus mainly on input reallocation, ignoring externalities.
\item \citeA{Aragon2013a} find that mining reduces agricultural productivity by almost 40\% due to environmental degradation.
\item \citeA{Aragon2013} find a positive effect of mining on real income for non-mining workers in Peru.
\item \citeA{Goltz2014} finds that communities near mines in Africa increase asset wealth and anemia rates. Only has extensive margin variation.
\item \citeA{Swenson2011} (Peru), \citeA{Hilson2002} (Ghana),  \citeA{Fernandez-Navarro2012} (Spain), \citeA{Attfield2008} (US), and \citeA{Chakraborti2013} (India) focus on correlation or lack a credible contractual. 
\end{itemize}
\end{frame}



\begin{frame}
\frametitle{Related literature}
\begin{itemize}

\item Artisanal and small scale gold mining is the largest contributor to atmospheric mercury emissions (727 tones or $35\%$ of emissions) \cite{UNEP2013}.
\item Releases from small scale and artisanal to water bodies are estimated at 800 tones ($63\%$ of emissions) \cite{UNEP2013}.  
\item Fetal brain is especially susceptible to damage from exposure to mercury \cite{Davidson2004}.
\item Long term effects of low APGAR (Appearance, Pulse, Grimace, Activity, Respiration) scores on cognitive skills: 2 IQ points \cite{Ehrenstein2009}.

\end{itemize}
\end{frame}


\section{Data}

\begin{frame}[label=dat2]
\frametitle{Mining Data}
Mines
\begin{itemize}
\item Mining permits location and database collected from 1990 to 2012. 
\item Gold production per municipality (Yearly 2001-2003, Quaterly 2004-2013)

\end{itemize}
%\hyperlink{sumstat2<1>}{\beamergotobutton{Summary Statistics}}
\end{frame}

\begin{frame}[plain,label=sumstat2]

\input{SummaryMines}

\end{frame}

\begin{frame}[plain]
\frametitle{Mining Data}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.45\textwidth]{ProdArea} \quad
\includegraphics[width=0.45\textwidth]{MiningMunicipalities} 
\caption{ \scriptsize{Left panel presents the evolution over time of gold production (in kilograms) and total mined area (in square kilometers). The right panel presents the evolution of the proportion of municipalities with some gold production and with some area designated for gold mining. Source: Ministerio de Minas y Energia de Colombia. Calculations: Authors.}}
\label{fig:evol_mineria}
\end{center}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Illegal Mining}
\begin{itemize}
\item Over 60\% of mines do not have a permit \cite{Ministerio2012}.
\item Classifiy municipalities are prone to illegal mining or not
\item Information from 2010-2011 mining census and police reports of illegal mines.
\item National government allocates mining permits
\item A mining permit only allows exploration in an area; but an environmental permit is needed in order to extract minerals.
\item Mining permits as a proxy for mineral discovery which would lead either legal or illegal mining.
\end{itemize}
\end{frame}

\begin{frame}[label=dat1]
\frametitle{Vital Statistics Data}
Health
\begin{itemize}
\item Vital Statistics for all 1,100 Colombian municipalities 
\item From 1999-2012, there is information on all newborns:
\begin{itemize}
\item Sex
\item Weight 
\item Height
\item APGAR score 
\item Mother's age
\end{itemize}
\end{itemize}
%\hyperlink{sumstat1<1>}{\beamergotobutton{Summary Statistics}}
\end{frame}


\begin{frame}[plain,label=sumstat1]
\frametitle{Vital Statistics Data}
{\scriptsize 
\input{SummaryBirth}
}
\end{frame}

\begin{frame}[plain]
\frametitle{Vital Statistics Data}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.65\textwidth]{apgar_region} 
\caption{ \scriptsize{Evolution of the proportion of newborns with low APGAR by region Source: Vital Statistics. Calculations: Authors.}}
\label{fig:evol_apgar}
\end{center}
\end{figure}
\end{frame}







\section{Identification Strategy and preliminary results}


\begin{frame}
\frametitle{Preliminary results}
{\scriptsize $Y_{imt}=\beta_1 Gold_{mt}+ \beta_2 Gold^2_{mt}+X_i \alpha+\gamma_m+ \gamma_{t}+\lambda_{m}\times t+\varepsilon_{imt} $}
\begin{scriptsize}
\begin{itemize}
\item $Y_{imt}$ outcome variable for birth $i$ in municipality $m$ at time $t$/
\item $Gold_{mt}$ measure of exposure to gold mining in municipality $m$ at time $t$
\item $\gamma_m$ municipality fixed effects
\item $\gamma_{t}$ year and week of birth fixed effects 
\item $\lambda_{m}\times t$ regional trends
\item $X_i$ individual controls (mothers age, mother educations, mothers marital status).
\item $ \varepsilon_{imt}$ error term
\end{itemize}
$\beta_1$ and $\beta_2$ are reduced form estimates of the effect of gold mining on health
\end{scriptsize}

\end{frame}

\begin{frame}
\begin{table}[H]\centering
\caption{Effect of mining on birth outcomes -Individual level data}
\begin{tiny}
\begin{tabular}{l*{3}{c}}
\toprule
                    &\multicolumn{1}{c}{(1)}&\multicolumn{1}{c}{(2)} &\multicolumn{1}{c}{(3)}\\
                    &\multicolumn{1}{c}{APGAR}&\multicolumn{1}{c}{LBW} &\multicolumn{1}{c}{Height}\\
\midrule
\multicolumn{4}{l}{\textbf{Panel A: Proportion of mined area}} \\
\input{IndividualMeasure_AreaMinadaProp} 
\midrule
\multicolumn{4}{l}{\textbf{Panel B: Production per area}} \\
\input{IndividualMeasure_ProduccionPerArea}
\bottomrule
Time F.E. & Yes & Yes & Yes\\
Municipality F.E. & Yes & Yes & Yes\\
Regional Trends & Yes & Yes & Yes\\
\bottomrule
\multicolumn{4}{l}{\tiny \specialcell{Clustered standard errors, by municipality, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }}\\
\end{tabular}
\end{tiny}
\label{tab:regIndividualMeasure}
\end{table}
\end{frame}





\begin{frame}
\frametitle{River pollution}
\begin{itemize}
\item Estimate pollution exposure of population through rivers and proximity to mines.
\item Knowing the location of the mine we identify the closest river (usually inside the mine) and attach a ``pollution'' index equal to the size of the mine.
\item Diffuse mercury pollution downstream according to the function $\sum_i 0.76^{riverdist_i}Pol_i$.
\item Buffer of 10km around the river.
\item Combine with population density to find an average (weighted) exposure to river pollution.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{River pollution}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.40\textwidth]{Data_mines.png} \quad
\includegraphics[width=0.40\textwidth]{Data_polu.png} 
\caption{Left panel shows an example of mines (in brown) located in municipalities (boundaries in black) and the rivers (in blue) in the area. The right panel shows the diffusion of pollution through rivers (low pollution in green and high pollution in red).}
\label{fig:1}
\end{center}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{River pollution}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.40\textwidth]{Data_pop.png} \quad
\includegraphics[width=0.40\textwidth]{Data_exp.png} 
\caption{Light pink (low) to dark red (High) scale: population density by pixel (left) and resultant exposure (right). Exposure is the product of population and pollution values.}
\end{center}
\end{figure}
\end{frame}



\begin{frame}
\frametitle{Holy Week}
\begin{itemize}
\item Colombia is predominantly a catholic country ($> 75\%$ of population). Latinbarometro 2011
\item Holy Week is major catholic holidays and Maundy Thursday and Good Friday are national holidays despite the self proclaim secular nature of the state.
\item Fish consumption increases during holy week by 60\% \cite{ElColombiano2014}. 
\item Gives us exogenous variation of in-utero mercury exposure.
\item Holy Week date varies from year to year (partially remove seasonality).
\end{itemize}
\end{frame}






\section{Closing remarks and the future}

\begin{frame}
\frametitle{Limitations}
\begin{itemize}
\item No direct measurement of mercury.
\item Fish traveling upstream or local fish markets 
\item From where does the fish that people eat comes from?
\item \citeA{Olivero2002} find great variability in mercury level across fish species in Colombia. 
\begin{enumerate}
\item Arenca $\rightarrow$ 0.177 $\mu$g/kg body weight/day.
\item Bocachico $\rightarrow$ 0.016 $\mu$g/kg body weight/day
\end{enumerate}
\end{itemize}
\end{frame}



\begin{frame}
\frametitle{Future steps}
\begin{itemize}
\item Robustness checks of pollution diffusion parameters and gold production.
\item Heterogeneity by legality of the mines.
\item Data on fish markets by municipality.
\item Measures of illegal mining using satellite images?


\end{itemize}

\end{frame}


\begin{frame}
\frametitle{Thank you}
\begin{itemize}
\item Gracias
\item Asante Sana
\item Merci
\item Obrigado
\item Grazie
\end{itemize}

\end{frame}


\renewcommand*{\refname}{} % This will define heading of bibliography to be empty, so you can...
\begin{frame}[allowframebreaks]{Bibliography}
	\bibliographystyle{apacite}
\bibliography{bibmin}
\end{frame}

%\begin{frame}[label=sumstat2]
%\frametitle{Summary Statistics II}
%\begin{table}[H]\centering \caption{Summary statistics \label{sumstat}}
%\begin{tabular}{l c c c c }\hline\hline
%\multicolumn{1}{c}{\textbf{Variable}} & \textbf{Mean}
% & \textbf{Std. Dev.}& \textbf{Min.} &  \textbf{Max.} \\ \hline
%Area km\^{}2 & 1.338 & 7.091 & 0 & 190.609 \\
%Year & 2003.338 & 3.87 & 1990 & 2008 \\
%\multicolumn{1}{c}{N} & \multicolumn{4}{c}{7638}\\ \hline
%\end{tabular}
%\end{table}
%\hyperlink{dat2<1>}{\beamerreturnbutton{Go Back}}
%\end{frame}

\end{document}

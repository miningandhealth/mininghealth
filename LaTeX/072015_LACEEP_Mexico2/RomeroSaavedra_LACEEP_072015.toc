\beamer@endinputifotherversion {3.33pt}
\select@language {english}
\beamer@sectionintoc {1}{Motivation and context}{2}{0}{1}
\beamer@sectionintoc {2}{This paper}{11}{0}{2}
\beamer@sectionintoc {3}{Data}{20}{0}{3}
\beamer@sectionintoc {4}{Identification Strategy and results}{23}{0}{4}
\beamer@sectionintoc {5}{Robustness Checks}{31}{0}{5}
\beamer@sectionintoc {6}{Closing remarks}{38}{0}{6}
\contentsline {section}{}

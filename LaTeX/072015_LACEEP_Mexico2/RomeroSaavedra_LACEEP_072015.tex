\documentclass[pdf]{beamer}
\usepackage{booktabs}
\usepackage{tabularx,colortbl}
\usepackage{multirow}
\usepackage[export]{adjustbox}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{comment}
\usepackage{wrapfig}
\usepackage{dsfont}
\usepackage{bbm}
\usepackage[flushleft]{threeparttable}      % Option: Table notes
\usepackage{floatpag}
\usepackage{wrapfig}
\usepackage{relsize}
\usepackage{setspace}  
\usepackage{apacite}  
\usepackage{rotating}
\usepackage{fancybox}
\usepackage{nameref}
\usepackage{float}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{latexsym}
\usepackage{amsmath,amsthm}
\usepackage{amsfonts}

\usepackage{apacite}

                   
\mode<presentation>{}
\usetheme{Warsaw}


\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}
 \newcommand{\specialcell}[2][c]{%
    \begin{tabular}[#1]{@{}c@{}}#2\end{tabular}
}
\expandafter\def\expandafter\insertshorttitle\expandafter{%
  \insertshorttitle\hfill%
  \insertframenumber\,/\,\inserttotalframenumber}
\title[Mining and health]{The Effect of Gold Mining on the Health of Newborns}

\author[Romero-Saavedra]{Mauricio Romero (UC - San Diego)  \\ Santiago Saavedra (Stanford)  }

\normalsize

\date{July 23rd 2015}



\begin{document}

\frame{\titlepage

{\tiny This study was carried out with support from the Latin American and Caribbean Environmental Economics Program (LACEEP)}

}
 


\section{Motivation and context}

\begin{frame}[plain]
\frametitle{Mining Boom}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.7\textwidth]{ProdArea} 
\caption{\tiny{Evolution over time of gold production (in kilograms) and total mined area (in square kilometers). Source: Ministerio de Minas y Energia de Colombia. Calculations: Authors.}}
\label{fig:evol_mineria}
\end{center}
\end{figure}
\end{frame}

\begin{frame}[plain]
\frametitle{Growth and environmental degradation}
\begin{itemize}[<+->]
\item Growth often comes at the expense of environmental degradation $\rightarrow$ esp. in developing countries
\item Net impact on human development (e.g. health) is unclear
\begin{itemize}
\item[+] Income effect
\item[-] Pollution effect
\end{itemize}
\item Mercury releases from gold mining to water bodies are $63\%$ of emissions \cite{UNEP2013}. 
\item Mercury is bio-accumulative toxin ingested by humans through fish consumption.
\item Evidence that EPA mercury limits are exceeded in Colombia near mining areas \cite{GUIZA2013,Olivero2002}.
\item 63\% of the mines in Colombia are illegal (at least 51,826
miners, around 50\% of mining employment)

\end{itemize}
\end{frame}





\section{This paper}

\begin{frame}[plain]
\frametitle{This paper}
\begin{itemize}[<+->]
\item Contributes to the literature that quantifies the externalities of mining activities... 
\begin{itemize}
\item Reduces agricultural productivity \cite{Aragon2013a}
\item Positive effect on real income for non-miners \cite{Aragon2013}
\item Increase asset wealth and anemia rates \cite{Goltz2014}
\end{itemize}
\item Diff-in-diff strategy to calculate \textbf{reduced} form  impact of mining on \textit{infant} health 
\item Separately identify the ``income'' and ``pollution'' effect 
\begin{itemize}
\item First paper to explore intensive margin: heterogeneity of impacts across mining activity levels
\item Spatial variation (downstream from a mine)  %\cite{Schlenker2011,Luechinger2014}
\item Religious holidays (increase in fish consumption) %\cite{Almond2011,Schofield2014}
\end{itemize}
%\item First paper to explore intensive margin: heterogeneity of impacts across mining activity levels
\end{itemize}
\end{frame}


\section{Data}


\begin{frame}[plain,label=dat2]
\frametitle{Mining Data}
Mines
\begin{itemize}
\item Mining permits location and database collected from 1990 to 2012.
\begin{itemize}
\item Proxy for mineral discovery which would lead either legal or illegal mining.
\end{itemize} 
\item Gold production per municipality (Yearly 2001-2003, Quarterly 2004-2013)
\end{itemize}
\hyperlink{sumstat2<1>}{\beamergotobutton{Summary Statistics}}
\end{frame}



\begin{frame}[plain,label=dat1]
\frametitle{Vital Statistics Data}
Health
\begin{itemize}
\item Vital Statistics for all 1,100 Colombian municipalities 
\begin{itemize}
\item Restrict to 625 municipalities close to mines in Andean, Caribbean and Pacific regions
\end{itemize} 
\item From 1999-2012, there is information on all newborns
\begin{itemize}
\item Fetal brain is especially susceptible to damage from exposure to mercury (Davidson, 2004).
\item APGAR (Appearance, Pulse, Grimace, Activity and Respiration) score (0 bad, 10 good health)
\item APGAR score is a significant predictor of health, cognitive ability, and behavioral problems (Almond, Chay \& Lee, 2005)
\item Long term effects of low APGAR scores on cognitive skills: 2 IQ points (Ehrenstein, 2009).
\end{itemize}
\note{A municipality, the unit of analysis, has median population 13,270 (max 462,209) and 248 km^2 approx 95 square miles. max of 3051.75 sq mi}

%\begin{itemize}
%\item Gender
%\item Weight 
%\item Height
%\item APGAR score 
%\item Mother's age
%\end{itemize}
\end{itemize}
\hyperlink{sumstat1<1>}{\beamergotobutton{Summary Statistics}}
\end{frame}


\begin{frame}[plain]
\frametitle{Municipalities}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.6\textwidth]{Muni_analysis} 
\end{center}
\end{figure}
\end{frame}

\section{Identification Strategy and results}


\begin{frame}[plain,label=empesp]
\frametitle{Net effect of gold mining}

{\scriptsize $
LowAPGAR_{imt}=\beta f( Gold_{mt})+X_{imt} \alpha+\gamma_m+ \gamma_{t}+\lambda_{r(m)}\times t+\varepsilon_{imt} $}
\begin{scriptsize}
\begin{itemize}
\item $LowAPGAR_{imt}$ for birth $i$ in municipality $m$ at time $t$ (score less than 7, poor health)
\item $Gold_{mt}$ measure of exposure to gold mining in municipality $m$ at time $t$
\item $\gamma_m$ municipality fixed effects
\item $\gamma_{t}$ year and week of birth fixed effects 
\item $\lambda_{m}\times t$ regional trends
\item $X_i$ individual controls (mothers age, mother educations, mothers marital status).
\item $ \varepsilon_{imt}$ error term
\end{itemize}
$\beta$ reduced form estimates of the effect of gold mining on health
\note{Lots of measurement error so we use dummy variables}
\end{scriptsize}
\hyperlink{sumstat3<1>}{\beamergotobutton{Summary Statistics}}
\end{frame}




\begin{frame}[plain]
\frametitle{Effect of mining on birth outcomes -Individual level data}
\begin{center}
\resizebox{!}{0.55\textheight}{\begin{tabular}{l*{4}{c}}
\toprule          
                  \multicolumn{4}{c}{Dependent variable: Low APGAR}\\
                    &  \multicolumn{1}{c}{(1)}  &  \multicolumn{1}{c}{(2)}  &  \multicolumn{1}{c}{(3)} \\
\midrule
\multicolumn{4}{l}{\textbf{Panel A: Proportion of mined area}} \\
\input{IndividualMeasure_AreaMinadaProp} 
%\midrule
%\multicolumn{4}{l}{\textbf{Panel B: Production per area}} \\
%\input{IndividualMeasure_ProduccionPerArea}
\midrule
\multicolumn{4}{l}{\textbf{Panel B: Accumulated Production per area}} \\
\input{IndividualMeasure_ProduccionAccPerArea}
\midrule
\multicolumn{4}{l}{\textbf{Panel C: Any mining title}} \\
\input{IndividualDummies_AreaMinadaProp}
\bottomrule
%\multicolumn{4}{p{0.9\textwidth}}{\scriptsize Time fixed effects are year and week fixed effects. Individual controls include mother's age, an indicator for single mother and an indicator for whether the mother has any post-primary education. The regional trends allow for a trend for each geographic region (Pacific, Andean and Caribbean). Standard errors clustered by municipality are in parenthesis.}\\
\multicolumn{4}{c}{\scriptsize Clustered standard errors, by municipality, in parentheses. Mean Panel A:0.41, Panel B: 0.23, Panel C: 0.19 }\\
\multicolumn{4}{c}{\scriptsize \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }\\
\end{tabular}}
\end{center}
\end{frame}


\begin{frame}[plain]
\frametitle{Intensive margin}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.8\textwidth]{HeterogeneousImpact} 
\caption{\tiny Coefficient on decile of proportion of mined area.}
\label{fig:evolSalud}
\end{center}
\note{Explain axis and so on}
\end{figure}
\end{frame}



\begin{frame}[plain,label=rivpoldesc]
\frametitle{River pollution}
\begin{itemize}
\item Estimate pollution exposure of population through rivers and proximity to mines.
\item For each mine identify the closest river (usually inside the mine) and attach a ``pollution'' index equal to the size of the mine.
\item Diffuse pollution downstream.
\item Buffer of 10km around the river.
\item Combine with population density to find an average (weighted) exposure to river pollution.
\end{itemize}
\hyperlink{rivpolgraph<1>}{\beamergotobutton{Diagram}}
\end{frame}



\begin{frame}[plain]
\frametitle{Identification Strategy}
\begin{center}
\begin{table}
\caption{Births within each category}
    \begin{tabular}{rrrr}
    \toprule
          &       & \multicolumn{2}{c}{{\small Proximity Exp.}} \\
    \midrule
    \multicolumn{1}{c}{\multirow{3}[0]{*}{\begin{sideways}{\small River Exp.}\end{sideways}}}      &       & Yes   & No \\
   \multicolumn{1}{c}{}  & Yes   & 40\%  &  18\% \\
    \multicolumn{1}{c}{} & No    & 3.10\%   & 38.9\% \\
    \bottomrule
    \end{tabular}%
    \end{table}
\end{center}
\end{frame}






%\begin{frame}
%\frametitle{River pollution}
%\begin{figure}[H]
%\begin{center}
%\includegraphics[width=0.40\textwidth]{Data_mines.png} \quad
%\includegraphics[width=0.40\textwidth]{Data_polu.png} 
%\caption{Left panel shows an example of mines (in brown) located in municipalities (boundaries in black) and the rivers (in blue) in the area. The right panel shows the diffusion of pollution through rivers (low pollution in green and high pollution in red).}
%\label{fig:1}
%\end{center}
%\end{figure}
%\end{frame}
%
%\begin{frame}
%\frametitle{River pollution}
%\begin{figure}[H]
%\begin{center}
%\includegraphics[width=0.40\textwidth]{Data_pop.png} \quad
%\includegraphics[width=0.40\textwidth]{Data_exp.png} 
%\caption{Light pink (low) to dark red (High) scale: population density by pixel (left) and resultant exposure (right). Exposure is the product of population and pollution values.}
%\end{center}
%\end{figure}
%\end{frame}

\begin{frame}[plain]
\frametitle{River Exposure}
\begin{center}
\resizebox{!}{0.55\textheight}{
\begin{tabular}{l*{4}{c}}
\toprule          
                  \multicolumn{4}{c}{Dependent variable: Low APGAR}\\
                    &  \multicolumn{1}{c}{(1)}  &  \multicolumn{1}{c}{(2)}  &  \multicolumn{1}{c}{(3)} \\
\midrule
\multicolumn{4}{l}{\textbf{Panel A: Exposure}} \\
\input{IndividualMeasure_Combo} 
\midrule
\multicolumn{4}{l}{\textbf{Panel B: Any Exposure}} \\
\input{IndividualDummies_Combo}
\bottomrule
%\multicolumn{4}{p{0.7\textwidth}}{\scriptsize  Time fixed effects are year and week fixed effects. Individual controls include mother's age, an indicator for single mother and an indicator for whether the mother has any post-primary education. The regional trends allow for a trend for each geographic region (Pacific, Andean and Caribbean). Standard errors clustered by municipality are in parenthesis.}\\
\multicolumn{4}{c}{\scriptsize Clustered standard errors, by municipality, in parentheses}\\
\multicolumn{4}{c}{\scriptsize \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }\\
\end{tabular}}
\end{center}
\end{frame}





\begin{frame}[plain]
\frametitle{Holy Week}
\begin{itemize}
\item Colombia is predominantly a catholic country ($> 75\%$ of population). Latinbarometro 2011
\item Holy Week is major catholic holiday.
\item Fish consumption increases during holy week by 60\% \cite{ElColombiano2014}. 
\item Gives us exogenous variation of in-utero mercury exposure.
\item Holy Week date varies from year to year (partially remove seasonality).
\end{itemize}
\end{frame}

\begin{frame}[plain]
\frametitle{Holy Week}
\begin{center}
\resizebox{!}{0.6\textheight}{
\begin{tabular}{l*{2}{c}}
\toprule
      \multicolumn{3}{c}{Dependent variable: Low APGAR}\\
                    &\multicolumn{1}{c}{(1)}&\multicolumn{1}{c}{(2)} \\    
                                   
                      \midrule
\input{IndividualHWDummies_Combo_20sem}

\bottomrule
%\multicolumn{4}{p{0.7\textwidth}}{\scriptsize Time fixed effects are year and week fixed effects. Individual controls include mother's age, an indicator for single mother and an indicator for whether the mother has any post-primary education. The regional trends allow for a trend for each geographic region (Pacific, Andean and Caribbean). Standard errors clustered by municipality are in parenthesis.}\\
\multicolumn{3}{c}{\scriptsize Individual controls, regional trends, Municipality, year and week FE}\\
\multicolumn{3}{c}{\scriptsize Clustered standard errors, by municipality, in parentheses}\\
\multicolumn{3}{c}{\scriptsize \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }\\
\end{tabular}}
\end{center}
\end{frame}


\section{Robustness Checks}



\begin{frame}[plain]
\frametitle{Robustness Checks}
\begin{itemize}
\item Mother characteristics do not vary consistently with upstream and proximity measures \hyperlink{upvsdown<1>}{\beamergotobutton{Upstream}}
\item No effect on LBW or height (in line with mercury mainly affecting neurological development) \hyperlink{otheroutcomes<1>}{\beamergotobutton{Other outcomes}}
\item Use surge in gold price as additional source of exogenous variation \hyperlink{price<1>}{\beamergotobutton{Price}}
\item No significant effect on fertility or number of births \hyperlink{fertility<1>}{\beamergotobutton{Fertility and migration}}
\item Heterogeneous effect illegal prone municipalities
\end{itemize}
\end{frame}

\begin{frame}[plain,label=illegal]
\frametitle{Legal vs illegal mining}
\begin{center}
\resizebox{!}{0.35\textheight}{\begin{tabular}{l*{4}{c}}
\toprule
                    &\multicolumn{1}{c}{(1)}&\multicolumn{1}{c}{(2)} &\multicolumn{1}{c}{(3)}\\
                    &\multicolumn{1}{c}{All}&\multicolumn{1}{c}{Legal} &\multicolumn{1}{c}{Illegal} \\
\midrule
\input{IndividualDummies_Combo_illegal}

\bottomrule
%\multicolumn{4}{p{0.7\textwidth}}{\scriptsize Time fixed effects are year and week fixed effects. Individual controls include mother's age, an indicator for single mother and an indicator for whether the mother has any post-primary education. The regional trends allow for a trend for each geographic region (Pacific, Andean and Caribbean). Standard errors clustered by municipality are in parenthesis.}\\
\multicolumn{3}{c}{\scriptsize Individual controls, regional trends, Municipality, year and week FE}\\
\multicolumn{3}{c}{\scriptsize Clustered standard errors, by municipality, in parentheses}\\
\multicolumn{3}{c}{\scriptsize \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }\\
\end{tabular}}
\end{center}
\end{frame}

\frame[plain]{\frametitle{Next step: locate illegal mines using satelite data}
\includegraphics[scale=0.4]{AllLayers.png} 
}

\begin{frame}[plain]
%Split this in two and more colorful
\frametitle{Identifying Illegal Mines}
\begin{enumerate}
\item The 2010 Census identified legal and illegal mines in 23 states
\begin{itemize}
\item Separate 80\% of mines for training, 20\% testing
\end{itemize}
\item Remove clouds from each scene (Goslee, 2011)
\begin{itemize}
\item More than 1 billion pixels every 2 weeks initially for the country
\end{itemize}
\item Yearly cloudless composite
\begin{itemize}
\item Eight color bands (30x30mts) with values from 1 to 255 according to reflected energy
\end{itemize}
\item Subbaging: Random forest on several balanced subsamples (Paleologo et. al. 2010)
\item Cross-validate with remaining 20\%
\item ``Subtract'' legal mines
\item Predict for all years

\end{enumerate}
\end{frame}

\begin{frame}[plain]
\frametitle{Sample zoom in on predicted pixel}
\includegraphics[scale=0.48]{ZoomPixel.png} 
\end{frame}

\begin{frame}[plain,label=roc]
\frametitle{Receiver Operating Characteristic (ROC) curve}
\includegraphics[scale=0.48]{ROCconCoordenadas.png} 

\hyperlink{rocstate}{\beamergotobutton{ROC One State}} \hyperlink{confmat}{\beamergotobutton{Confusion Matrix}}
\end{frame}

\begin{frame}[plain]

\frametitle{Evolution of predicted illegal mining in selected sample}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.87\textwidth]{evolv_illegal.pdf} 
\end{center}
\end{figure}

\end{frame}


\section{Closing remarks}

\begin{frame}[plain]
\frametitle{Conclusions}
\begin{itemize}
\item The presence of mining activity reduces the probability of having low APGAR score by 0.8 percentage points (from a base of 4.74\%)
\item However, river pollution increases probability of low APGAR for downstream municipalities by 0.7 percentage points
\item We do not find additional benefits of larger areas mined
\item Slight evidence of fish consumption effects
\item Future project on detecting illegal mines using satellites
\end{itemize}
\end{frame}

\begin{frame}[plain]
\frametitle{Thank you}
\begin{itemize}
\item Gracias
\item Asante Sana
\item Merci
\item Obrigado
\item Grazie
\end{itemize}

\end{frame}

\begin{frame}[plain]
\frametitle{Research Objectives}
\textbf {General objective:} Quantify the net impacts of gold mining on the health of surrounding population. 

\begin{enumerate}
\item Quantify the effect of gold mining on the APGAR score, gestation period, weight and height of newborns.
\item Separately identify, if possible, income and pollution effects using fish consumption.
\item Estimate how the effects of vary by the timing of in utero exposure to methyl-mercury. 
\item Estimate how the net health effects of mining vary by different mine types: large scale vs. small scale, legal vs. illegal. 
\item Compare the health costs with the income and royalties derived from mining.
\end{enumerate}
\end{frame}

\begin{frame}[plain]
\frametitle{Research Objectives}
\textbf {General objective:} Quantify the net impacts of gold mining on the health of surrounding population. 

\begin{enumerate}
\item Quantify the effect of gold mining on the APGAR score, gestation period, weight and height of newborns.
\begin{itemize}
\item Improve 0.8 ppts probability of low APGAR, not stable results height and weight
\end{itemize}
\item Separately identify, if possible, income and pollution effects using fish consumption.
\begin{itemize}
\item Affect municipality downstream 0.7ppts of low APGAR, fish used in next bullet
\end{itemize}
\item Estimate how the effects of vary by the timing of in utero exposure to methyl-mercury. 
\begin{itemize}
\item Not significant results, but sugestive of effect first weeks of gestation
\end{itemize}
\item Estimate how the net health effects of mining vary by different mine types: large scale vs. small scale, legal vs. illegal. 
\begin{itemize}
\item Larger on illegal
\end{itemize}
\item Compare the health costs with the income and royalties derived from mining.
\begin{itemize}
\item Estimated NEWBORN health costs around 3\% of royalties 
\end{itemize}
\end{enumerate}
\end{frame}

\begin{frame}[plain]
\frametitle{Future steps September 2014}
\begin{itemize}
\item Finish Upstream/Downstream exercise.
\item Finish Holyweek (triple-diff).
\item Robustness checks.
\item Heterogeneity by legality of the mines.
\item Data on fish markets by municipality?
\item Measures of illegal mining using satellite images?
\end{itemize}

\end{frame}

\begin{frame}[plain]
\frametitle{Future steps September 2014}
\begin{itemize}
\item Finish Upstream/Downstream exercise.
\begin{itemize}
\item DONE
\end{itemize}
\item Finish Holyweek (triple-diff).
\begin{itemize}
\item DONE
\end{itemize}
\item Robustness checks.
\begin{itemize}
\item DONE
\end{itemize}
\item Heterogeneity by legality of the mines.
\begin{itemize}
\item DONE
\end{itemize}
\item Data on fish markets by municipality?
\begin{itemize}
\item PILOT FOUND HETEROGENEITY AND HARD TO TRACK ORIGIN
\end{itemize}
\item Measures of illegal mining using satellite images?
\begin{itemize}
\item WORK IN PROGRESS
\end{itemize}
\end{itemize}

\end{frame}


\begin{frame}[plain,label=rivpolgraph]
\frametitle{Identification Strategy}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.8\textwidth]{Drawing_Schematic_Part1} 
\end{center}
\end{figure}
\end{frame}

\begin{frame}[plain]
\frametitle{Identification Strategy}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.8\textwidth]{Drawing_Schematic_Part2} 
\end{center}
\end{figure}
\end{frame}

\begin{frame}[plain]
\frametitle{Identification Strategy}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.8\textwidth]{Drawing_Schematic_Part3} 
\end{center}
\end{figure}
\end{frame}

\begin{frame}[plain]
\frametitle{Identification Strategy}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.8\textwidth]{Drawing_Schematic_Part4} 
\end{center}
\end{figure}
\end{frame}

\begin{frame}[plain]
\frametitle{Identification Strategy}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.8\textwidth]{Drawing_Schematic_Part5} 
\end{center}
\end{figure}

\end{frame}

\begin{frame}[plain]
\frametitle{Identification Strategy}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.8\textwidth]{Drawing_Schematic_Part6} 
\end{center}
\end{figure}
\hyperlink{rivpoldesc<1>}{\beamergotobutton{Return}}
\end{frame}


\begin{frame}[plain,label=otheroutcomes]
\frametitle{Other outcomes}
\begin{center}
\resizebox{!}{0.45\textheight}{
\begin{tabular}{l*{4}{c}}
\toprule          
                  \multicolumn{4}{c}{Dependent variable: }\\
                    &  \multicolumn{1}{c}{Low APGAR}  &  \multicolumn{1}{c}{Low Birth Weight} &  \multicolumn{1}{c}{Z-height}   \\
                     &  \multicolumn{1}{c}{(1)}  &  \multicolumn{1}{c}{(2)} &  \multicolumn{1}{c}{(3)}   \\
\midrule
\multicolumn{4}{l}{\textbf{Panel A: Proportion of mined area}} \\
\input{IndividualMeasure_AreaMinadaProp_lbwzh} 
\midrule
\multicolumn{4}{l}{\textbf{Panel B: Any mining title}} \\
\input{IndividualDummies_AreaMinadaProp_lbwzh}
\midrule
\multicolumn{4}{l}{\textbf{Panel C: Any Exposure}} \\
\input{IndividualDummies_Combo_lbwzh}
\bottomrule
%\multicolumn{4}{p{0.9\textwidth}}{\scriptsize  Time fixed effects are year and week fixed effects. Individual controls include mother's age, an indicator for single mother and an indicator for whether the mother has any post-primary education. The regional trends allow for a trend for each geographic region (Pacific, Andean and Caribbean). Standard errors clustered by municipality are in parenthesis.}\\
\multicolumn{4}{c}{\scriptsize Clustered standard errors, by municipality, in parentheses}\\
\multicolumn{4}{c}{\scriptsize \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }\\
\end{tabular}}
\end{center}
\end{frame}
%
%
%
\begin{frame}[plain,label=price]
\frametitle{Price surge}
\begin{center}
\resizebox{!}{0.45\textheight}{
\begin{tabular}{l*{3}{c}}
\toprule          
                  \multicolumn{3}{c}{Dependent variable: Low APGAR}\\
                    &  \multicolumn{1}{c}{(1)}  &  \multicolumn{1}{c}{(2)}   \\
\midrule
\multicolumn{3}{l}{\textbf{Panel A: Proportion of mined area}} \\
\input{IndividualMeasure_AreaMinadaProp_price} 
\midrule
\multicolumn{3}{l}{\textbf{Panel B: Any mining title}} \\
\input{IndividualDummies_AreaMinadaProp_price}
\midrule
\multicolumn{3}{l}{\textbf{Panel C: Any Exposure}} \\
\input{IndividualDummies_Combo_price}
\bottomrule
%\multicolumn{3}{p{0.9\textwidth}}{\scriptsize  Time fixed effects are year and week fixed effects. Individual controls include mother's age, an indicator for single mother and an indicator for whether the mother has any post-primary education. The regional trends allow for a trend for each geographic region (Pacific, Andean and Caribbean). Standard errors clustered by municipality are in parenthesis.}\\
\multicolumn{3}{c}{\scriptsize Clustered standard errors, by municipality, in parentheses}\\
\multicolumn{3}{c}{\scriptsize \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }\\
\end{tabular}}
\end{center}
\end{frame}



\begin{frame}[plain,label=fertility]
\frametitle{Fertility and migration}
\begin{center}
\resizebox{!}{0.45\textheight}{\begin{tabular}{l*{2}{c}}
\toprule
                    &\multicolumn{1}{c}{(1)}&\multicolumn{1}{c}{(2)}\\
                    &\multicolumn{1}{c}{Fertility}&\multicolumn{1}{c}{Births}\\
\midrule
\multicolumn{2}{l}{\textbf{Panel A: Proportion of mined area}} \\
\input{FertilidadMeassure_AreaMinadaProp} 
\midrule
\multicolumn{2}{l}{\textbf{Panel B: Production per area}} \\
\input{FertilidadMeassure_ProduccionPerArea}
\midrule
\multicolumn{2}{l}{\textbf{Panel C: Any mining title}} \\
\input{FertilidadDummies_AreaMinadaProp} 
\midrule
\multicolumn{2}{l}{\textbf{Panel D: Any production}} \\
\input{FertilidadDummies_ProduccionPerArea}
\bottomrule
\multicolumn{3}{l}{\footnotesize Observations are weighted by population in the case of fertility. }\\
\multicolumn{3}{l}{\footnotesize \specialcell{Clustered standard errors, by municipality, are in parenthesis \\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }}\\
\end{tabular}}
\end{center}
\end{frame}





\begin{frame}[plain,label=upvsdown]
\frametitle{Difference between municipalities}
\begin{center}
\resizebox{!}{0.45\textheight}{
\begin{tabular}{l*{4}{c}}
\toprule          
                  \multicolumn{5}{c}{Dependent variable: Mother Characteristic}\\
                    &  \multicolumn{1}{c}{Single}  &  \multicolumn{1}{c}{$>$ Primary Edu.}  &  \multicolumn{1}{c}{Age} &  \multicolumn{1}{c}{Contributory} \\             
\midrule
\input{Exposure_Placebo}
\multicolumn{4}{c}{\scriptsize Clustered standard errors, by municipality, in parentheses}\\
\multicolumn{4}{c}{\scriptsize \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }\\
\bottomrule
\end{tabular}}
\end{center}
\end{frame}



\begin{frame}[plain,label=sumstat1]
\frametitle{Vital Statistics Data}
\resizebox{!}{0.2\textheight}{
\input{SummaryBirth}
}
\hyperlink{dat1<1>}{\beamergotobutton{Return}}
\end{frame}

\begin{frame}[plain,label=sumstat2]

{\footnotesize \input{SummaryMines}}
\hyperlink{dat2<1>}{\beamergotobutton{Return}}
\end{frame}

\begin{frame}[plain,label=sumstat3]
\frametitle{Measures of exposure to gold mining}
\begin{center}
\resizebox{1\textwidth}{!}{
\begin{tabular}{l*{1}{cccccc}}
\toprule
                    &        Mean&      Median&   Std. Dev.&         Min&         Max&           N\\
\midrule
Mining Area/Municipality Area&        0.41&           0&        1.69&           0&        15.0&     4360340\\
Mining Area/Municipality Area if Mining Area$>0$&  2.11&        0.58&        3.34&     - &        15.0&      843930\\
Mining Area$>0$ & 0.19&           0&        0.40&           0&           1&     4360340\\
Accumulated Production ($Kg/Km^2$)&        0.23&      0.0015&        1.25&           0&        51.9&     3846088\\
\bottomrule
\end{tabular}}
\end{center}
\hyperlink{empesp<1>}{\beamergotobutton{Return}}

\end{frame}

\begin{frame}[plain]
\frametitle{Mining Data}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.7\textwidth]{ProdArea} 
\caption{ \tiny{Evolution over time of gold production (in kilograms) and total mined area (in square kilometers). Source: Ministerio de Minas y Energia de Colombia. Calculations: Authors.}}

\label{fig:evol_mineria}
\end{center}
\end{figure}

\end{frame}

\begin{frame}[plain]
\frametitle{Summary gold exposure}

\begin{tiny}
\begin{tabular}{l*{1}{ccccccc}}
\toprule
                    &        Mean&      Median&   Std. Dev.&         Min&         Max&    Perc. 95&           N\\
\midrule
\multicolumn{4}{l}{\textbf{Panel A: All observations}} \\
Mining Area/Municipality Area&      0.0044&           0&       0.029&           0&        0.90&      0.0078&     9281041\\
Production/Municipality Area&      0.0087&           0&        0.12&           0&        11.4&       0.026&     7797167\\
\midrule
\multicolumn{4}{l}{\textbf{Panel B: Positive Values}} \\
Mining Area/Municipality Area&       0.041&      0.0075&       0.081&    &        0.90&        0.19&     1001736\\
Production/Municipality Area&       0.039&      0.0036&        0.26&  &        11.4&        0.13&     1748194\\
\bottomrule
\end{tabular}
\end{tiny}

\end{frame}

\renewcommand*{\refname}{} % This will define heading of bibliography to be empty, so you can...
\begin{frame}[allowframebreaks]{Bibliography}
	\bibliographystyle{apacite}
\bibliography{bibmin}
\end{frame}

%\begin{frame}[label=sumstat2]
%\frametitle{Summary Statistics II}
%\begin{table}[H]\centering \caption{Summary statistics \label{sumstat}}
%\begin{tabular}{l c c c c }\hline\hline
%\multicolumn{1}{c}{\textbf{Variable}} & \textbf{Mean}
% & \textbf{Std. Dev.}& \textbf{Min.} &  \textbf{Max.} \\ \hline
%Area km\^{}2 & 1.338 & 7.091 & 0 & 190.609 \\
%Year & 2003.338 & 3.87 & 1990 & 2008 \\
%\multicolumn{1}{c}{N} & \multicolumn{4}{c}{7638}\\ \hline
%\end{tabular}
%\end{table}
%\hyperlink{dat2<1>}{\beamerreturnbutton{Go Back}}
%\end{frame}

\end{document}

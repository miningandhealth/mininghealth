\begin{thebibliography}{}

\bibitem [\protect \citeauthoryear {%
Aragon%
\ \BBA {} Rud%
}{%
Aragon%
\ \BBA {} Rud%
}{%
{\protect \APACyear {2013}}%
{\protect \APACexlab {{\protect \BCnt {1}}}}}]{%
Aragon2013a}
\APACinsertmetastar {%
Aragon2013a}%
\begin{APACrefauthors}%
Aragon, F\BPBI M.%
\BCBT {}\ \BBA {} Rud, J\BPBI P.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013{\protect \BCnt {1}}}{February}{}.
\newblock
\APACrefbtitle {Modern Industries, Pollution and Agricultural Productivity:
  Evidence from Mining in Ghana.} {Modern industries, pollution and
  agricultural productivity: Evidence from mining in ghana.}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Aragon%
\ \BBA {} Rud%
}{%
Aragon%
\ \BBA {} Rud%
}{%
{\protect \APACyear {2013}}%
{\protect \APACexlab {{\protect \BCnt {2}}}}}]{%
Aragon2013}
\APACinsertmetastar {%
Aragon2013}%
\begin{APACrefauthors}%
Aragon, F\BPBI M.%
\BCBT {}\ \BBA {} Rud, J\BPBI P.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013{\protect \BCnt {2}}}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Natural Resources and Local Communities: Evidence from a
  Peruvian Gold Mine} {Natural resources and local communities: Evidence from a
  peruvian gold mine}.{\BBCQ}
\newblock
\APACjournalVolNumPages{American Economic Journal: Economic
  Policy}{5}{2}{1-25}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Attfield%
\ \BBA {} Kuempel%
}{%
Attfield%
\ \BBA {} Kuempel%
}{%
{\protect \APACyear {2008}}%
}]{%
Attfield2008}
\APACinsertmetastar {%
Attfield2008}%
\begin{APACrefauthors}%
Attfield, M.%
\BCBT {}\ \BBA {} Kuempel, E.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2008}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Mortality among U.S. underground coal miners: A 23-year
  follow-up} {Mortality among u.s. underground coal miners: A 23-year
  follow-up}.{\BBCQ}
\newblock
\APACjournalVolNumPages{American Journal of Industrial
  Medicine}{51}{4}{231-245}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Chakraborti%
\ \protect \BOthers {.}}{%
Chakraborti%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2013}}%
}]{%
Chakraborti2013}
\APACinsertmetastar {%
Chakraborti2013}%
\begin{APACrefauthors}%
Chakraborti, D.%
, Rahman, M\BPBI M.%
, Murrill, M.%
, Das, R.%
, Siddayya%
, Patil, S.%
\BDBL {}Das, K\BPBI K.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Environmental arsenic contamination and its health
  effects in a historic gold mining area of the Mangalur greenstone belt of
  Northeastern Karnataka, India} {Environmental arsenic contamination and its
  health effects in a historic gold mining area of the mangalur greenstone belt
  of northeastern karnataka, india}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Journal of Hazardous Materials}{262}{0}{1048 - 1055}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
{Congreso de la Republica}%
}{%
{Congreso de la Republica}%
}{%
{\protect \APACyear {2013}}%
}]{%
Congreso2013}
\APACinsertmetastar {%
Congreso2013}%
\begin{APACrefauthors}%
{Congreso de la Republica}%
\end{APACrefauthors}%
\ (\BED).
\unskip\
\newblock
\APACrefYearMonthDay{2013}{}{}.
\newblock
\APACrefbtitle {{Ley No. 1658}.} {{Ley No. 1658}.}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Cordy%
\ \protect \BOthers {.}}{%
Cordy%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2011}}%
}]{%
Cordy2011}
\APACinsertmetastar {%
Cordy2011}%
\begin{APACrefauthors}%
Cordy, P.%
, Veiga, M\BPBI M.%
, Salih, I.%
, Al-Saadi, S.%
, Console, S.%
, Garcia, O.%
\BDBL {}Roeser, M.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2011}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Mercury contamination from artisanal gold mining in
  Antioquia, Colombia: The world's highest per capita mercury pollution}
  {Mercury contamination from artisanal gold mining in antioquia, colombia: The
  world's highest per capita mercury pollution}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Science of The Total Environment}{410 - 411}{0}{154 -
  160}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Davidson%
, Myers%
\BCBL {}\ \BBA {} Weiss%
}{%
Davidson%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2004}}%
}]{%
Davidson2004}
\APACinsertmetastar {%
Davidson2004}%
\begin{APACrefauthors}%
Davidson, P\BPBI W.%
, Myers, G\BPBI J.%
\BCBL {}\ \BBA {} Weiss, B.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2004}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Mercury Exposure and Child Development Outcomes}
  {Mercury exposure and child development outcomes}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Pediatrics}{113}{Supplement 3}{1023-1029}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Fernandez-Navarro%
, Garcia-Perez%
, Ramis%
, Boldo%
\BCBL {}\ \BBA {} Lopez-Abente%
}{%
Fernandez-Navarro%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2012}}%
}]{%
Fernandez-Navarro2012}
\APACinsertmetastar {%
Fernandez-Navarro2012}%
\begin{APACrefauthors}%
Fernandez-Navarro, P.%
, Garcia-Perez, J.%
, Ramis, R.%
, Boldo, E.%
\BCBL {}\ \BBA {} Lopez-Abente, G.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2012}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Proximity to mining industry and cancer mortality}
  {Proximity to mining industry and cancer mortality}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Science of The Total Environment}{}{0}{66 - 73}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Hilson%
}{%
Hilson%
}{%
{\protect \APACyear {2002}}%
}]{%
Hilson2002}
\APACinsertmetastar {%
Hilson2002}%
\begin{APACrefauthors}%
Hilson, G.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2002}{}{}.
\newblock
{\BBOQ}\APACrefatitle {The environmental impact of small-scale gold mining in
  Ghana: identifying problems and possible solutions} {The environmental impact
  of small-scale gold mining in ghana: identifying problems and possible
  solutions}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Geographical Journal}{168}{1}{57-72}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Olivero%
\ \BBA {} Johnson%
}{%
Olivero%
\ \BBA {} Johnson%
}{%
{\protect \APACyear {2002}}%
}]{%
Olivero2002}
\APACinsertmetastar {%
Olivero2002}%
\begin{APACrefauthors}%
Olivero, J.%
\BCBT {}\ \BBA {} Johnson, B.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYear{2002}.
\unskip\
\newblock
\APACrefbtitle {El lado gris de la mineria de oro: La contaminacion con
  mercurio en el norte de Colombia} {El lado gris de la mineria de oro: La
  contaminacion con mercurio en el norte de colombia}\ \APACtypeAddressSchool
  {\BUMTh}{}{}.
\unskip\
\newblock
\APACaddressSchool {}{Universidad de Cartagena}.
\PrintBackRefs{\CurrentBib}

\end{thebibliography}

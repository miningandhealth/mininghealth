\begin{thebibliography}{}

\bibitem [\protect \citeauthoryear {%
Aragon%
\ \BBA {} Rud%
}{%
Aragon%
\ \BBA {} Rud%
}{%
{\protect \APACyear {2013}}%
{\protect \APACexlab {{\protect \BCnt {1}}}}}]{%
Aragon2013a}
\APACinsertmetastar {%
Aragon2013a}%
\begin{APACrefauthors}%
Aragon, F\BPBI M.%
\BCBT {}\ \BBA {} Rud, J\BPBI P.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013{\protect \BCnt {1}}}{February}{}.
\newblock
\APACrefbtitle {Modern Industries, Pollution and Agricultural Productivity:
  Evidence from Mining in Ghana.} {Modern industries, pollution and
  agricultural productivity: Evidence from mining in ghana.}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Aragon%
\ \BBA {} Rud%
}{%
Aragon%
\ \BBA {} Rud%
}{%
{\protect \APACyear {2013}}%
{\protect \APACexlab {{\protect \BCnt {2}}}}}]{%
Aragon2013}
\APACinsertmetastar {%
Aragon2013}%
\begin{APACrefauthors}%
Aragon, F\BPBI M.%
\BCBT {}\ \BBA {} Rud, J\BPBI P.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013{\protect \BCnt {2}}}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Natural Resources and Local Communities: Evidence from a
  Peruvian Gold Mine} {Natural resources and local communities: Evidence from a
  peruvian gold mine}.{\BBCQ}
\newblock
\APACjournalVolNumPages{American Economic Journal: Economic
  Policy}{5}{2}{1-25}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Ashe%
}{%
Ashe%
}{%
{\protect \APACyear {2012}}%
}]{%
Ashe2012}
\APACinsertmetastar {%
Ashe2012}%
\begin{APACrefauthors}%
Ashe, K.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2012}{03}{}.
\newblock
{\BBOQ}\APACrefatitle {Elevated Mercury Concentrations in Humans of Madre de
  Dios, Peru} {Elevated mercury concentrations in humans of madre de dios,
  peru}.{\BBCQ}
\newblock
\APACjournalVolNumPages{PLoS ONE}{7}{3}{}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Attfield%
\ \BBA {} Kuempel%
}{%
Attfield%
\ \BBA {} Kuempel%
}{%
{\protect \APACyear {2008}}%
}]{%
Attfield2008}
\APACinsertmetastar {%
Attfield2008}%
\begin{APACrefauthors}%
Attfield, M.%
\BCBT {}\ \BBA {} Kuempel, E.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2008}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Mortality among U.S. underground coal miners: A 23-year
  follow-up} {Mortality among u.s. underground coal miners: A 23-year
  follow-up}.{\BBCQ}
\newblock
\APACjournalVolNumPages{American Journal of Industrial
  Medicine}{51}{4}{231-245}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Bastos%
\ \protect \BOthers {.}}{%
Bastos%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2006}}%
}]{%
Bastos2006}
\APACinsertmetastar {%
Bastos2006}%
\begin{APACrefauthors}%
Bastos, W\BPBI R.%
, Gomes, J\BPBI P\BPBI O.%
, Oliveira, R\BPBI C.%
, Almeida, R.%
, Nascimento, E\BPBI L.%
, Bernardi, J\BPBI V\BPBI E.%
\BDBL {}Pfeiffer, W\BPBI C.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2006}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Mercury in the environment and riverside population in
  the Madeira River Basin, Amazon, Brazil} {Mercury in the environment and
  riverside population in the madeira river basin, amazon, brazil}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Science of The Total Environment}{368}{1}{344 - 351}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
{Behre Dolbear Group}%
}{%
{Behre Dolbear Group}%
}{%
{\protect \APACyear {2012}}%
}]{%
Behre2012}
\APACinsertmetastar {%
Behre2012}%
\begin{APACrefauthors}%
{Behre Dolbear Group}.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2012}{}{}.
\newblock
\APACrefbtitle {2012 Ranking of countries for mining investment} {2012 ranking
  of countries for mining investment}\ \APACbVolEdTR{}{\BTR{}}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Chakraborti%
\ \protect \BOthers {.}}{%
Chakraborti%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2013}}%
}]{%
Chakraborti2013}
\APACinsertmetastar {%
Chakraborti2013}%
\begin{APACrefauthors}%
Chakraborti, D.%
, Rahman, M\BPBI M.%
, Murrill, M.%
, Das, R.%
, Siddayya%
, Patil, S.%
\BDBL {}Das, K\BPBI K.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Environmental arsenic contamination and its health
  effects in a historic gold mining area of the Mangalur greenstone belt of
  Northeastern Karnataka, India} {Environmental arsenic contamination and its
  health effects in a historic gold mining area of the mangalur greenstone belt
  of northeastern karnataka, india}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Journal of Hazardous Materials}{262}{0}{1048 - 1055}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Clarkson%
, Magos%
\BCBL {}\ \BBA {} Myers%
}{%
Clarkson%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2003}}%
}]{%
Clarkson2003}
\APACinsertmetastar {%
Clarkson2003}%
\begin{APACrefauthors}%
Clarkson, T\BPBI W.%
, Magos, L.%
\BCBL {}\ \BBA {} Myers, G\BPBI J.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2003}{}{}.
\newblock
{\BBOQ}\APACrefatitle {The Toxicology of Mercury - Current Exposures and
  Clinical Manifestations} {The toxicology of mercury - current exposures and
  clinical manifestations}.{\BBCQ}
\newblock
\APACjournalVolNumPages{New England Journal of Medicine}{349}{18}{1731-1737}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
{Congreso de la Republica}%
}{%
{Congreso de la Republica}%
}{%
{\protect \APACyear {2013}}%
}]{%
Congreso2013}
\APACinsertmetastar {%
Congreso2013}%
\begin{APACrefauthors}%
{Congreso de la Republica}%
\end{APACrefauthors}%
\ (\BED).
\unskip\
\newblock
\APACrefYearMonthDay{2013}{}{}.
\newblock
\APACrefbtitle {{Ley No. 1658}.} {{Ley No. 1658}.}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Cordy%
\ \protect \BOthers {.}}{%
Cordy%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2011}}%
}]{%
Cordy2011}
\APACinsertmetastar {%
Cordy2011}%
\begin{APACrefauthors}%
Cordy, P.%
, Veiga, M\BPBI M.%
, Salih, I.%
, Al-Saadi, S.%
, Console, S.%
, Garcia, O.%
\BDBL {}Roeser, M.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2011}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Mercury contamination from artisanal gold mining in
  Antioquia, Colombia: The world's highest per capita mercury pollution}
  {Mercury contamination from artisanal gold mining in antioquia, colombia: The
  world's highest per capita mercury pollution}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Science of The Total Environment}{410 - 411}{0}{154 -
  160}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Cortes-Maramba%
\ \protect \BOthers {.}}{%
Cortes-Maramba%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2006}}%
}]{%
CortesMaramba2006}
\APACinsertmetastar {%
CortesMaramba2006}%
\begin{APACrefauthors}%
Cortes-Maramba, N.%
, Reyes, J\BPBI P.%
, Francisco-Rivera, A\BPBI T.%
, Akagi, H.%
, Sunio, R.%
\BCBL {}\ \BBA {} Panganiban, L\BPBI C.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2006}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Health and environmental assessment of mercury exposure
  in a gold mining community in Western Mindanao, Philippines} {Health and
  environmental assessment of mercury exposure in a gold mining community in
  western mindanao, philippines}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Journal of Environmental Management}{81}{2}{126 - 134}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Counter%
\ \BBA {} Buchanan%
}{%
Counter%
\ \BBA {} Buchanan%
}{%
{\protect \APACyear {2004}}%
}]{%
Counter2004}
\APACinsertmetastar {%
Counter2004}%
\begin{APACrefauthors}%
Counter, S.%
\BCBT {}\ \BBA {} Buchanan, L\BPBI H.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2004}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Mercury exposure in children: a review} {Mercury
  exposure in children: a review}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Toxicology and Applied Pharmacology}{198}{2}{209 -
  230}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
da Veiga%
}{%
da Veiga%
}{%
{\protect \APACyear {1997}}%
}]{%
Veiga1997}
\APACinsertmetastar {%
Veiga1997}%
\begin{APACrefauthors}%
da Veiga, M.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{1997}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Introducing New Technologies for Abatement of Global
  Mercury Pollution in Latin America} {Introducing new technologies for
  abatement of global mercury pollution in latin america}.{\BBCQ}
\newblock

\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Davidson%
, Myers%
\BCBL {}\ \BBA {} Weiss%
}{%
Davidson%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2004}}%
}]{%
Davidson2004}
\APACinsertmetastar {%
Davidson2004}%
\begin{APACrefauthors}%
Davidson, P\BPBI W.%
, Myers, G\BPBI J.%
\BCBL {}\ \BBA {} Weiss, B.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2004}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Mercury Exposure and Child Development Outcomes}
  {Mercury exposure and child development outcomes}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Pediatrics}{113}{Supplement 3}{1023-1029}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Fernandez-Navarro%
, Garcia-Perez%
, Ramis%
, Boldo%
\BCBL {}\ \BBA {} Lopez-Abente%
}{%
Fernandez-Navarro%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2012}}%
}]{%
Fernandez-Navarro2012}
\APACinsertmetastar {%
Fernandez-Navarro2012}%
\begin{APACrefauthors}%
Fernandez-Navarro, P.%
, Garcia-Perez, J.%
, Ramis, R.%
, Boldo, E.%
\BCBL {}\ \BBA {} Lopez-Abente, G.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2012}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Proximity to mining industry and cancer mortality}
  {Proximity to mining industry and cancer mortality}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Science of The Total Environment}{}{0}{66 - 73}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Guiza%
\ \BBA {} Aristizabal%
}{%
Guiza%
\ \BBA {} Aristizabal%
}{%
{\protect \APACyear {2013}}%
}]{%
GUIZA2013}
\APACinsertmetastar {%
GUIZA2013}%
\begin{APACrefauthors}%
Guiza, L.%
\BCBT {}\ \BBA {} Aristizabal, J\BPBI D.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013}{04}{}.
\newblock
{\BBOQ}\APACrefatitle {Mercury and gold mining in Colombia: a failed state}
  {Mercury and gold mining in colombia: a failed state}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Universitas Scientiarum}{18}{}{33 - 49}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Hilson%
}{%
Hilson%
}{%
{\protect \APACyear {2002}}%
}]{%
Hilson2002}
\APACinsertmetastar {%
Hilson2002}%
\begin{APACrefauthors}%
Hilson, G.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2002}{}{}.
\newblock
{\BBOQ}\APACrefatitle {The environmental impact of small-scale gold mining in
  Ghana: identifying problems and possible solutions} {The environmental impact
  of small-scale gold mining in ghana: identifying problems and possible
  solutions}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Geographical Journal}{168}{1}{57-72}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Huertas%
, Huertas%
, Izquierdo%
\BCBL {}\ \BBA {} Gonzalez%
}{%
Huertas%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2012}}%
}]{%
Huertas2012}
\APACinsertmetastar {%
Huertas2012}%
\begin{APACrefauthors}%
Huertas, J\BPBI I.%
, Huertas, M\BPBI E.%
, Izquierdo, S.%
\BCBL {}\ \BBA {} Gonzalez, E\BPBI D.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2012}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Air quality impact assessment of multiple open pit coal
  mines in northern Colombia} {Air quality impact assessment of multiple open
  pit coal mines in northern colombia}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Journal of Environmental Management}{93}{1}{121 - 129}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Ikingura%
\ \BBA {} Akagi%
}{%
Ikingura%
\ \BBA {} Akagi%
}{%
{\protect \APACyear {1996}}%
}]{%
Ikingura1996}
\APACinsertmetastar {%
Ikingura1996}%
\begin{APACrefauthors}%
Ikingura, J.%
\BCBT {}\ \BBA {} Akagi, H.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{1996}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Monitoring of fish and human exposure to mercury due to
  gold mining in the Lake Victoria goldfields, Tanzania} {Monitoring of fish
  and human exposure to mercury due to gold mining in the lake victoria
  goldfields, tanzania}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Science of The Total Environment}{191}{}{59-68}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Leiva%
\ \BBA {} Morales%
}{%
Leiva%
\ \BBA {} Morales%
}{%
{\protect \APACyear {2013}}%
}]{%
Leiva2013}
\APACinsertmetastar {%
Leiva2013}%
\begin{APACrefauthors}%
Leiva, M.%
\BCBT {}\ \BBA {} Morales, S.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Environmental assessment of mercury pollution in urban
  tailings from gold mining} {Environmental assessment of mercury pollution in
  urban tailings from gold mining}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Ecotoxicology and Environmental Safety}{90}{0}{167 -
  173}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Lusilao-Makiese%
, Cukrowska%
, Tessier%
, Amouroux%
\BCBL {}\ \BBA {} Weiersbye%
}{%
Lusilao-Makiese%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2013}}%
}]{%
LusilaoMakiese2013}
\APACinsertmetastar {%
LusilaoMakiese2013}%
\begin{APACrefauthors}%
Lusilao-Makiese, J.%
, Cukrowska, E.%
, Tessier, E.%
, Amouroux, D.%
\BCBL {}\ \BBA {} Weiersbye, I.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013}{}{}.
\newblock
{\BBOQ}\APACrefatitle {The impact of post gold mining on mercury pollution in
  the West Rand region, Gauteng, South Africa} {The impact of post gold mining
  on mercury pollution in the west rand region, gauteng, south africa}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Journal of Geochemical Exploration}{134}{0}{111 - 119}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Male%
, Reichelt-Brushett%
, Pocock%
\BCBL {}\ \BBA {} Nanlohy%
}{%
Male%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2013}}%
}]{%
Male2013}
\APACinsertmetastar {%
Male2013}%
\begin{APACrefauthors}%
Male, Y.%
, Reichelt-Brushett, A.%
, Pocock, M.%
\BCBL {}\ \BBA {} Nanlohy, A.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Recent mercury contamination from artisanal gold mining
  on Buru Island, Indonesia - Potential future risks to environmental health
  and food safety} {Recent mercury contamination from artisanal gold mining on
  buru island, indonesia - potential future risks to environmental health and
  food safety}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Marine Pollution Bulletin}{}{0}{}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
McMahon%
\ \BBA {} Remy%
}{%
McMahon%
\ \BBA {} Remy%
}{%
{\protect \APACyear {2001}}%
}]{%
McMahon2001}
\APACinsertmetastar {%
McMahon2001}%
\begin{APACrefauthors}%
McMahon, G.%
\BCBT {}\ \BBA {} Remy, F.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2001}{}{}.
\newblock
\APACrefbtitle {Large Mines and the Community : Socioeconomic and Environmental
  Effects in Latin America, Canada and Spain} {Large mines and the community :
  Socioeconomic and environmental effects in latin america, canada and spain}\
  \APACbVolEdTR{}{\BTR{}}.
\newblock
\APACaddressInstitution{}{World Bank and the International Development Research
  Centre}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Mergler%
\ \protect \BOthers {.}}{%
Mergler%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2007}}%
}]{%
Mergler2007}
\APACinsertmetastar {%
Mergler2007}%
\begin{APACrefauthors}%
Mergler, D.%
, Anderson, H\BPBI A.%
, Chan, L\BPBI H\BPBI M.%
, Mahaffey, K\BPBI R.%
, Murray, M.%
, Sakamoto, M.%
\BCBL {}\ \BBA {} Stern, A\BPBI H.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2007}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Methylmercury Exposure and Health Effects in Humans: A
  Worldwide Concern} {Methylmercury exposure and health effects in humans: A
  worldwide concern}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Ambio}{36}{1}{3-11}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
{Ministerio de Minas y Energia}%
}{%
{Ministerio de Minas y Energia}%
}{%
{\protect \APACyear {2011}}%
}]{%
Minas2011}
\APACinsertmetastar {%
Minas2011}%
\begin{APACrefauthors}%
{Ministerio de Minas y Energia}.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2011}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Chapter 2: Regalias} {Chapter 2: Regalias}.{\BBCQ}
\newblock
\BIn{} \APACrefbtitle {Memorias al Congreso de la Republica 2010 - 2011.}
  {Memorias al congreso de la republica 2010 - 2011.}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
{Ministerio de Minas y Energia}%
}{%
{Ministerio de Minas y Energia}%
}{%
{\protect \APACyear {2012}}%
}]{%
Ministerio2012}
\APACinsertmetastar {%
Ministerio2012}%
\begin{APACrefauthors}%
{Ministerio de Minas y Energia}.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2012}{}{}.
\newblock
\APACrefbtitle {Censo Minero Departamental Colombiano 2010-2011} {Censo minero
  departamental colombiano 2010-2011}\ \APACbVolEdTR{}{\BTR{}}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Mol%
\ \BBA {} Ouboter%
}{%
Mol%
\ \BBA {} Ouboter%
}{%
{\protect \APACyear {2004}}%
}]{%
Mol2004}
\APACinsertmetastar {%
Mol2004}%
\begin{APACrefauthors}%
Mol, J.%
\BCBT {}\ \BBA {} Ouboter, P.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2004}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Downstream Effects of Erosion from Small-Scale Gold
  Mining on the Instream Habitat and Fish Community of a Small Neotropical
  Rainforest Stream} {Downstream effects of erosion from small-scale gold
  mining on the instream habitat and fish community of a small neotropical
  rainforest stream}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Conservation Biology}{18}{1}{201--214}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Mol%
, Ramlal%
, Lietar%
\BCBL {}\ \BBA {} Verloo%
}{%
Mol%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2001}}%
}]{%
Mol2001}
\APACinsertmetastar {%
Mol2001}%
\begin{APACrefauthors}%
Mol, J.%
, Ramlal, J.%
, Lietar, C.%
\BCBL {}\ \BBA {} Verloo, M.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2001}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Mercury Contamination in Freshwater, Estuarine, and
  Marine Fishes in Relation to Small-Scale Gold Mining in Suriname, South
  America} {Mercury contamination in freshwater, estuarine, and marine fishes
  in relation to small-scale gold mining in suriname, south america}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Environmental Research}{86}{2}{183 - 197}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Morel%
, Kraepiel%
\BCBL {}\ \BBA {} Amyot%
}{%
Morel%
\ \protect \BOthers {.}}{%
{\protect \APACyear {1998}}%
}]{%
Morel1998}
\APACinsertmetastar {%
Morel1998}%
\begin{APACrefauthors}%
Morel, F\BPBI M\BPBI M.%
, Kraepiel, A\BPBI M\BPBI L.%
\BCBL {}\ \BBA {} Amyot, M.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{1998}{}{}.
\newblock
{\BBOQ}\APACrefatitle {The Chemical Cycle and Bioaccumulation of Mercury} {The
  chemical cycle and bioaccumulation of mercury}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Annual Review of Ecology and
  Systematics}{29}{1}{543-566}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Olivero%
\ \BBA {} Johnson%
}{%
Olivero%
\ \BBA {} Johnson%
}{%
{\protect \APACyear {2002}}%
}]{%
Olivero2002}
\APACinsertmetastar {%
Olivero2002}%
\begin{APACrefauthors}%
Olivero, J.%
\BCBT {}\ \BBA {} Johnson, B.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYear{2002}.
\newblock
\APACrefbtitle {El lado gris de la mineria de oro: La contaminacion con
  mercurio en el norte de Colombia} {El lado gris de la mineria de oro: La
  contaminacion con mercurio en el norte de colombia}.
\newblock
\BUMTh, Universidad de Cartagena.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Pfeiffer%
\ \BBA {} de Lacerda%
}{%
Pfeiffer%
\ \BBA {} de Lacerda%
}{%
{\protect \APACyear {1988}}%
}]{%
Pfeiffer1988}
\APACinsertmetastar {%
Pfeiffer1988}%
\begin{APACrefauthors}%
Pfeiffer, W\BPBI C.%
\BCBT {}\ \BBA {} de Lacerda, L\BPBI D.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{1988}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Mercury inputs into the Amazon Region, Brazil} {Mercury
  inputs into the amazon region, brazil}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Environmental Technology Letters}{9}{4}{325-330}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Pfeiffer%
, Lacerda%
, Salomons%
\BCBL {}\ \BBA {} Malm%
}{%
Pfeiffer%
\ \protect \BOthers {.}}{%
{\protect \APACyear {1993}}%
}]{%
Pfeiffer1993}
\APACinsertmetastar {%
Pfeiffer1993}%
\begin{APACrefauthors}%
Pfeiffer, W\BPBI C.%
, Lacerda, L\BPBI D.%
, Salomons, W.%
\BCBL {}\ \BBA {} Malm, O.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{1993}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Environmental fate of mercury from gold mining in the
  Brazilian Amazon} {Environmental fate of mercury from gold mining in the
  brazilian amazon}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Environmental Reviews}{1}{1}{26-37}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Swenson%
, Carter%
, Domec%
\BCBL {}\ \BBA {} Delgado%
}{%
Swenson%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2011}}%
}]{%
Swenson2011}
\APACinsertmetastar {%
Swenson2011}%
\begin{APACrefauthors}%
Swenson, J\BPBI J.%
, Carter, C\BPBI E.%
, Domec, J\BHBI C.%
\BCBL {}\ \BBA {} Delgado, C\BPBI I.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2011}{04}{}.
\newblock
{\BBOQ}\APACrefatitle {Gold Mining in the Peruvian Amazon: Global Prices,
  Deforestation, and Mercury Imports} {Gold mining in the peruvian amazon:
  Global prices, deforestation, and mercury imports}.{\BBCQ}
\newblock
\APACjournalVolNumPages{PLoS ONE}{6}{4}{e18875}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
van Straaten%
}{%
van Straaten%
}{%
{\protect \APACyear {2000}}%
}]{%
Straaten2000}
\APACinsertmetastar {%
Straaten2000}%
\begin{APACrefauthors}%
van Straaten, P.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2000}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Human exposure to mercury due to small scale gold mining
  in northern Tanzania} {Human exposure to mercury due to small scale gold
  mining in northern tanzania}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Science of The Total Environment}{259}{}{45-53}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Villar%
}{%
Villar%
}{%
{\protect \APACyear {2013}}%
}]{%
Villar2013}
\APACinsertmetastar {%
Villar2013}%
\begin{APACrefauthors}%
Villar, L.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013}{February}{}.
\newblock
\APACrefbtitle {El sector minero en Colombia: Impactos macroeconomicos y
  encadenamientos sectoriales} {El sector minero en colombia: Impactos
  macroeconomicos y encadenamientos sectoriales}\ \APACbVolEdTR {}{II Congreso
  del Sector de la Mineria a Gran Escala (SMGE)}.
\newblock
\APACaddressInstitution{}{Fedesarollo}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Webb%
\ \protect \BOthers {.}}{%
Webb%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2004}}%
}]{%
Webb2004}
\APACinsertmetastar {%
Webb2004}%
\begin{APACrefauthors}%
Webb, J.%
, Mainville, N.%
, Mergler, D.%
, Lucotte, M.%
, Betancourt, O.%
, Davidson, R.%
\BDBL {}Quizhpe, E.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2004}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Mercury in Fish-eating Communities of the Andean Amazon,
  Napo River Valley, Ecuador} {Mercury in fish-eating communities of the andean
  amazon, napo river valley, ecuador}.{\BBCQ}
\newblock
\APACjournalVolNumPages{EcoHealth}{1}{2}{59-71}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
{World Bank}%
}{%
{World Bank}%
}{%
{\protect \APACyear {2009}}%
}]{%
World2009}
\APACinsertmetastar {%
World2009}%
\begin{APACrefauthors}%
{World Bank}.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2009}{}{}.
\newblock
\APACrefbtitle {Mining Together : Large-Scale Mining Meets Artisanal Mining, A
  Guide for Action} {Mining together : Large-scale mining meets artisanal
  mining, a guide for action}\ \APACbVolEdTR{}{\BTR{}}.
\PrintBackRefs{\CurrentBib}

\end{thebibliography}

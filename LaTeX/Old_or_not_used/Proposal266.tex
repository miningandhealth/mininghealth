%\input{tcilatex}
\documentclass{article}
\usepackage[cm]{fullpage}
\usepackage{nameref}
\usepackage{float}
\usepackage{pdflscape}
\usepackage[plainpages=false,pdfpagelabels]{hyperref}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{latexsym}
\usepackage{amsmath,amsthm}
\usepackage{amsfonts}
\usepackage{epsfig}
\usepackage{amssymb}
\usepackage{longtable}
\usepackage{tabularx,colortbl}
\usepackage{multirow}
\usepackage[table]{xcolor}
\usepackage{graphicx,epsfig,psfrag}
\usepackage{color}
\usepackage{eso-pic}
\usepackage{pdflscape}
\usepackage{dcolumn}
\usepackage{flafter}
\usepackage[mathscr]{euscript}
\usepackage{array, lscape}
\usepackage{pb-diagram}
\usepackage{setspace}
\usepackage{apacite}
\usepackage[all]{hypcap}
\setcounter{MaxMatrixCols}{10}
\newtheorem{acknowledgement}{Acknowledgement}
\newtheorem{algorithm}{Algorithm}
\newtheorem{axiom}{Axiom}
\newtheorem{case}{Case}
\newtheorem{claim}{Claim}
\newtheorem{conclusion}{Conclusion}
\newtheorem{condition}{Condition}
\newtheorem{conjecture}{Conjecture}
\newtheorem{corollary}{Corollary}
\newtheorem{criterion}{Criterion}
\newtheorem{definition}{Definition}
\newtheorem{example}{Example}
\newtheorem{exercise}{Exercise}
\newtheorem{lemma}{Lemma}
\newtheorem{notation}{Notation}
\newtheorem{problem}{Problem}
\newtheorem{proposition}{Proposition}
\newtheorem{remark}{Remark}
\newtheorem{solution}{Solution}
\newtheorem{summary}{Summary}
\usepackage{geometry}
\date{}     % Deleting this command produces today's date.
\newcommand{\ip}[2]{(#1, #2)}
 \newcommand{\Lagr}{\mathcal{L}}
      % Defines \ip{arg1}{arg2} to mean
                             % (arg1, arg2).
%\newcommand{\ip}[2]{\langle #1 | #2\rangle}
                             % This is an alternative definition of
                             % \ip that is commented out.
\title{The hidden health costs of gold mining\thanks{I intend to pursue this idea with Santiago Saavedra, a third year PhD student at Stanford. This research proposal has been enriched by several discussions with Santiago.}}
 
\author{Mauricio Romero\thanks{University of California - San Diego, 9500 Gilman Dr, San Diego, CA 92093, U.S. \textit{e-mail contact:} \url{mtromero@ucsd.edu} }}
\begin{document}             % End of preamble and beginning of text.
                   % Produces the title.
\maketitle
\begin{abstract}
 
Developing countries often face a trade-off when it comes to mining since it is a big source of revenue with unaccounted environmental impacts. In the case of gold mining, the use and uncontrolled release of mercury influence health outcomes in surrounding populations. The medical literature is clear about the effect of exposure to high levels of mercury, but the critical threshold is not clear. Using a difference in difference approach, that compares municipalities in Colombia upstream and downstream from gold mines, this paper estimates the impact of gold mines on health outcomes.
\end{abstract}
 \section{Introduction}
 
%Mining is a big source of revenue for some developing countries, but is has many unaccounted costs. For example, in Colombia the mining industry is currently very small, representing less than 2.5\% of the country's GDP\cite{Villar2013}\footnote{This figure does not include oil extraction.}, but the country is the largest mercury polluter per capita\cite{Cordy2011}.
In many countries, mining is an important revenue source for both the government and its inhabitants \cite{World2009}. However, there is mixed evidence on the overall welfare effect of mining. On one hand, some studies find negative effects on the environment, agricultural productivity and local communities \cite{McMahon2001}, while others find that mining can have positive effects on local communities \cite{Aragon2013}. The dust has yet to settle in this debate and in particular there is little evidence of the indirect effect of mining on human health. This topic is more important in developing countries where mining is often the source of income for poor communities and thus mining regulation does not only have environmental impacts, it also has distributional effects.
 
Mercury is widely used in gold mining in Colombia \cite{Cordy2011} and as a results large quantities of this chemical are release into the environment. The effects of mercury exposure on health are not completely understood yet. The medical literature agrees that mercury is a doses-dependent toxin and that in high doses it can rapidly be fatal \cite{Davidson2004} but the evidence is inconclusive when it comes to low dosages. Previous studies lack power (have small sample size) or an experimental design that allows researchers to accurately identify the effects of different exposure levels on health.

This article will explore gold mines in Colombia and the impact they have on health outcomes. We look at mines located next to rivers and compare populations living upstream and downstream across time. There are two underlying assumption in this approach: first, that pollutants that are dropped in the river do not affect populations living upstream; second, that populations living on both sides of the mine have similar trends in health outcomes before mining starts.
 
The contribution of this article is twofold. First, it adds to the literature assessing the impact of mining on health. Currently, to the best of our knowledge, there is no causal evidence of the impact of mining on health.  Second, it adds to the literature that evaluates the impact of exposure to low dosages of mercury on health.

The next section presents an overview of the mining sector in Colombia. In section \ref{sec:rev} we present a literature review of the effects of mining on welfare and the effects of mercury on health. Section \ref{sec:meth} describes the data sets that will be used in this study and the methodologies that could be used to identify the effect of gold mining on health. Section \ref{sec:disc} discusses possible limitations before Section \ref{sec:conc} concludes.

\section{Colombia's mining sector} 

Colombia is currently not a ``mining country'' compared to other Latin American nations. As of 2011 mining represented 2.20\% of Colombia's GDP, compared to 13.2\% in Chile and 4.2\% in Peru.  The export share was barely over 20\% for Colombia, while it was around 60\% for both Chile and Peru. Worldwide the country currently occupies the ninth place in carbon and nickel extraction and the 19th place in gold \cite{Villar2013}. However, there are signs that the country could rapidly experience a mining boom. The number of mining titles, and the area where mining is allowed, has seen a dramatic increase in recent years (see figure \ref{fig:1}). The government expects coal and gold extraction to increase by 70\% by 2020 \cite{Minas2011} and according to the Behre Dolbear Group survey Colombia is the 7th most attractive country for mining investment \cite{Behre2012}. 

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.5]{MiningArea.pdf}
\end{center}
\caption{Total area in $m^2$ of the mining permits given in a year. Calculations: Authors. Source: INGEOMINAS.}
\label{fig:1}
\end{figure}
%
%The stand of the 
%It is common in Colombia to see communities endorsing mining in their territory while the central government, pressured by environmental groups, places stringent regulation on mining. At the same time, there are several examples where the roles are inverted, with the government endorsing mining and local communities rejecting. The case of the \textit{Paramo de Santurban} is an example of the former, while the case of \textit{Piedras} is an example of the later. 
%
%In the area near the \textit{Paramo de Santurban}, in the municipalities of California and Vetas, local communities have extracted gold at a small scale for a long time, but in 2010 several international companies, such as GrayStar, arrived with the objective of extracting gold at a large scale. The local community, were mining has been the main source of income for years, endorses large scale mining \cite{Rodriguez2013}, while environmentalist argue that mining in the \textit{paramo} will not only affect the balance of this unique ecosystem, it could also jeopardize the water supply for nearby communities and the city of Bucaramanga, since \textit{paramos} are often considered ``water factories''\cite{Maya2013}. In the case of Piedras, the community has voted to ban mining from their territory but the central government has been clear that the soil belongs to the nation and thus they have the right to issue mining permits to AngloGold Ashanti\cite{Tiempo2013}. 
%The government has yet to take a definite stand on mining and environmental regulation. It is common in Colombia to see communities endorsing mining in their territory while the central government, pressured by environmental groups, places stringent regulation on mining. At the same time, there are several examples where the roles are inverted, with the government endorsing mining and local communities rejecting.

The regulation that the country places today will determine the welfare effects of the upcoming mining boom. In July of 2013 congress ruled that mercury couldn't be used in mining by 2018 \cite{Congreso2013}. The environmental and distributional effects of this law are hard to foresee and with the current stock of knowledge it is unclear that banning mercury altogether is indeed optimal. Miners favor of the use of mercury partly cause its inexpensive \cite{GUIZA2013}. We need to fully understand the effects of mining on income, health, agricultural productivity, cognitive development, education, and other outcomes of interest in order to formulate optimal policies that maximize welfare. In this article we will try to add a piece to this puzzle by looking at gold mining, one of the largest mining industries in Colombia.

%Recently, in July of 2013, congress ruled that mercury couldn't be used in mining by 2018 \cite{Congreso2013}. This stringent regulation will have both environmental and distributional effects. Understanding the effects of mining on income, health, agricultural productivity, cognitive development, education, and other outcomes of interest, help policy makers formulate optimal policies. In this article we will try to add a piece to this puzzle by looking at gold mining, one of the largest mining industries in Colombia. 
%Restricting mining could very well lead to an increase in welfare for both local and non-local communities but we currently don't know, or it could be the case that mining regulation could be laxer since mercury exposure (at the current levels) doesn't have negative health effects. In this article we will try to add a piece to this puzzle by looking at gold mining, one of the largest mining industries in Colombia. 
% However, this public good can also have big positive impacts on local communities and thus the tax can be offset by the benefits. Health benefits can be one of those positive impacts that offsets the costs of restricting mining. Identifies all negative and positive impacts can help us understand whether environmental regulation is justifiable on distributional grounds or not, and whether compensation must be made to local communities when we take into account distributional effects. 
%
%Most of this debate, however, has neglected the effects of mining on human health. 
Even though this study focuses on gold mines, further research could be done to study the health effects of different mining activities. In particular, it would be interesting to study the effects on health of mining of coal, nickel and gold, the country's largest mining industries. For example, most of Colombia's coal is produced in an open pit mine where there is some evidence that local communities are affected by airborne pollution \cite{Huertas2012}.

Finally, it is important to note that small-scale artisan or illegal mining is widespread in Colombia. According to the government over 60\% of mines do not have a permit, and this number is probably a lower bound since the governments' survey couldn't access some remote area, due to security issues, where illegal mining is believed to be widespread \cite{Ministerio2012}. However, due to its nature we can't observe illegal mining in our database and thus in this project we will only focus on legal mining. Given that small-scale mining is an important source of mercury in Colombia \cite{Cordy2011} and an important source of revenue for poor and marginalized people \cite{World2009}, further research must follow in this area.

\section{Literature review}\label{sec:rev}

The economics literature on mining has focus mainly on input reallocation, ignoring externalities until very recently. There is evidence of both positive and negative spillovers. A working paper by \citeA{Aragon2013a} looks at the impact of mining on agricultural productivity. They find that mining reduces agricultural productivity by almost 40\%, and that this is not due to changes in the use or price of agricultural inputs, but due to environmental degradation. On the other hand, the same authors \citeA{Aragon2013}, find evidence of a positive effect of mining on real income for non-mining workers in Peru.

The academic literature outside of social sciences has long been exploring these questions. For example, \citeA{Swenson2011} find that increases in gold prices have boosted small scale gold mining in Peru which has increase deforestation in the amazon jungle and \citeA{Hilson2002} finds that small scale gold mining in Ghana has led to socio-economic growth accompanied by mercury pollution and land degradation. However, the exact impact of mining is uncertain since these studies often lack a contractual of what would have occurred in the absence of mining. Similarly, there are studies linking health to mining such as \citeA{Fernandez-Navarro2012, Attfield2008, Chakraborti2013} but these studies often focus on correlation and due to confounding effects can not imply causation. 

Metallic mercury is used in mining for amalgamation \cite{Mol2001}. Approximately 10 Kg of mercury are needed in order to extract 1 Kg of gold, and in this process 9 Kg are released into the environment \cite{Pfeiffer1993,Veiga1997,GUIZA2013}. Of the mercury used, 55\% of goes in the atmosphere, while 45\% goes in water bodies \cite{Pfeiffer1988}. Aquatic micro-organisms transform the metallic mercury into methyl mercury \cite{Morel1998}, and in turn fish eat these organisms (for a more detail explanation see \citeA{Mol2001}). Thus, the use of mercury in gold mining increases the exposure to both metallic mercury and to methyl mercury. In the U.S. the EPA limits for metallic mercury in drinking water and for methyl mercury consumption are 0.002 $mg/L$ and 0.1 $\mu$g/kg body weight/day respectably and there is 
evidence that those limits are exceeded in Colombia near mining areas \cite{GUIZA2013,Olivero2002}. However, in this article we will focus mainly on methyl mercury since it is more dangerous to humans \cite{GUIZA2013}.
 
%Since fish is the main source of methyl mercury exposure for humans \cite{Clarkson2003}, in this article we will focus on exposure to methyl mercury caused by metallic mercury used in gold mining. It is important to note, that over two thirds of mercury emissions come from anthropogenic sources \cite{Mason1994}, and thus mining indeed increases the exposure to mercury for some people. As motivated in the case of Santurban in Colombia, there are also concerns of contamination of drinking water sources. In the United States the limit is set at $0.002 mg/L$ \cite{EPA}.

Most of the literature looking at the effects of mining on health focuses on exposure to harmful chemical rather than on health outcomes. In the particular case of gold mining most studies focus on the exposure to mercury. Studies from Peru \cite{Ashe2012}, Suriname \cite{Mol2001,Mol2004}, The Philippines \cite{CortesMaramba2006}, Chile \cite{Leiva2013}, South Africa \cite{LusilaoMakiese2013}, Indonesia \cite{Male2013}, Brazil \cite{Bastos2006}, Ecuador \cite{Webb2004} and Tanzania \cite{Ikingura1996,Straaten2000} all look at mercury exposure but not at health outcomes. To the best of my knowledge there is not a single study that explores the effect of gold mining on health outcomes directly. 


The medical literature consistently points out that exposure to methyl mercury in high doses can be fatal \cite{Davidson2004} but there is no consensus on how dosage/exposure variation affects health. Most of the studies looking at exposure to low doses of mercury lack a sufficiently high sample size or experimental design to be conclusive (see \citeA{Mergler2007,Davidson2004,Counter2004,Clarkson2003} for a review of the literature). One of the biggest problems these studies face is that blood mercury levels are endogenous. For example, fish consumption, which increases blood mercury levels, could be positively correlated with income, ability or social status, invalidating any causal inference draw from correlation. 

In short, there is a gap in the literature. The literature has focus on mercury exposure caused by gold mining but this is not enough since we do not know exactly how different exposure levels affect health. This article will try to bridge that gap in the literature by looking directly at the effects of mining on health. 
 
 %The lack of studies linking mercury to health outcomes is particularly important in Colombia since the country is the world's largest mercury polluter per capita from small scale gold mining\cite{Cordy2011}.
%On the other hand, one could imagine that mine workers have higher blood mercury levels due to higher exposure but since working in a mine usually implies strenuous physical labor in a confined environment one could overestimate the impact of mercury exposure.

\section{Data and Methodology}\label{sec:meth}

In this study we will use mines located next to rivers and compare municipalities upstream and downstream across time.  After the mine is placed we could see how outcomes on the municipality living downstream change across time, compared to how they change in the municipality upstream. This difference in difference would give us the impact of having a mine upstream from a community's location. The model that reflects this idea is:
$$Y_{itv}=\alpha+\beta_1 D_{itv}+\beta_2 M_{itv}+\beta_3 D \times M_{itv}+\beta_4 X_{itv}+Year_{t}+Municipality_{v}+\varepsilon_{itv},$$
where $Y_{itv}$ is the outcome of interest of for individual $i$, in time $t$, in municipality $v$, $D_{itv}$ is a dummy variable indicating whether the municipality is downstream or upstream, $M_{itv}$ is a dummy variable that takes the value of one after the mine starts operating, $X_{itv}$ are other individual characteristics. $Year_{t}$ and $Municipality_{v}$ are year and municipalities fix effects respectively. The coefficient of interest is $\beta_3$, which will give us the effect on $Y_{itv}$ of mining for communities living downstream compared to those living upstream.

There are two underlying assumption in this approach: first, that pollutants that are dropped in the river do not affect populations living upstream; second, that populations living on both sides of the mine had similar trends before mining began. A necessary condition for the first assumptions is that fishes that are eaten by humans are caught locally and do not travel upstream. The second assumption can be tested using our database but given that the communities are near each other and the only difference is whether they are up or downstream from the mine location, one would expect the trends to be similar. Hopefully, we will have enough data to assess if this is true or not. It is also important to note that all the identifying variation is coming from changes in mean outcomes per municipality, and thus we need a large set of mines being created across time to have accurate estimates.
 
In order to identify mining location and activity we can use the mining permit database collected by the NGO Tierra Minada\footnote{The full data set can be found in \url{https://sites.google.com/site/tierraminada/}} based on official government records. There are a total of 20,928 permits given from 1990 to 2010. Since we are only going to look at gold mines the sample size reduces to 9,964. More than half of this permits were given in 2004 as can be seen in figure \ref{fig:2}, but there is still time variation in our sample. Within this permits we would like to identify which ones are located near rivers. We can combine this information with the municipality gold production released by the government agency SIMCO (\textit{Sistema de Informacion Minero Colombiano}).

\begin{figure}[H]
\begin{center}
\caption{Gold mining permits}
\includegraphics[scale=0.5]{GoldPermits.pdf}
\\
\footnotesize{Number of permits given each year for gold mining. Source: INGEOMINAS. Calculations: Author.}
\end{center}
\label{fig:2}
\end{figure}

In order to assess health outcomes we can look at two data bases. The first is the Vital Statistics database from Colombia which can be requested from the government statistics department (DANE). There is information on birth weight and height, as well as on number of gestation weeks and the Appearance, Pulse, Grimace, Activity and Respiration-APGAR score. All this variables could be used as outcome variables. The APGAR score is of particular interest since there is evidence that the fetal brain is especially susceptible to damage from exposure to mercury \cite{Davidson2004}.

The second data base corresponds to the RIPS (Individual Register of Health Services Provision) database which contains individual-level registers of medical consultations that took place in any health service institution in Colombia. With this information we could see if there is an increase in the number of consultations associated with mercury exposure.

\section{Discussion}\label{sec:disc}

There are several roadblocks I can foresee in this research project. The first one is that we will not be able to identify the exact effect of mercury on health, since we do not have mercury levels for different spots in a river or the amount of mercury in fish or human populations. We will only be able to identify the effect of living downstream from a mine. This is an important constraint since one of the motivations for this article is that we will be able to contribute to the medical literature in this area. However, if we are able to find mercury levels we can overcome this issue. Juan Camilo Cardenas, a professor at the Universidad de los Andes, is currently working on a project called ``A citizens' based monitoring system for the mining industry'', where crowd-sourcing would be used to assess mercury levels in different points across the country.

A second important problem is that fishes do not only travel downstream. Although, it's very likely that pollution only travels downstream, fishes could easily travel upstream, which could lead to the same treatment intensity for populations living upstream and downstream. In order to assess this claim, we must know what the diet of individuals look like, and whether the species of fish they consume travel upstream or not. Moreover, mercury concentration varies across fish species. For example, \citeA{Olivero2002} find great variability in mercury level across fish species in Colombia, the consumption of one \textit{Arenca} by an 80 kg person results in 0.177 $\mu$g/kg body weight/day, while the consumption of one \textit{Bocachico} results in 0.016 $\mu$g/kg body weight/day, thus making the type of fish consumed extremely important. Hopefully, the literature in biology and ecology can shed some light here. A similar problem has to do with how ``local'' the fish markets are. We believe this is not an issue since the municipalities we are going to look at are located next to the river and thus it seems reasonable that most of the fish they eat is caught locally.

Another important issue has to do with the distance of municipalities to the mines and how to deal with multiple mines located upstream. It is unclear how treatment intensity varies with distance from the mine. It is not only a matter of including distance as a control variable since treatment intensity would change how we deal with having multiple mines upstream.  Hopefully, the literature in biology, geology and hydrology, can shed some light into this. We believe it is important to do this before the econometric analysis or we could find ourselves ``cherry picking'' the results.
 
The fact that most mines do not have a permit creates several problems. First, it adds measurement error. Second, if illegal mines are located where legal mines are not, this could bias $\beta_3$ towards zero. Finally, if we believe that illegal mines are fundamentally different from legal mines, for example in the amount of mercury they use, our estimate of $\beta_3$ would be non-informative. We can overcome this by looking at three states in which according to the government the fraction of illegal mining is below 5\%. 
Finally, there is a timing issue. Since metallic mercury must be transformed by micro-organisms and then eaten by fish, and then these fish eaten by humans, the time between the mine starts operating and exposure to methyl mercury begins is uncertain and not immediate, making the time cut-off for our difference in difference approach unclear. Hopefully the literature in other fields will shed some light here too.

\section{Concluding Remarks}\label{sec:conc}

Environmental regulation in developing countries can often be thought of as placing a tax on a poor community (by restricting mining or making it more costly) in order to finance a public good (clean environment) that everyone can enjoy. However, this public good can also have big positive impacts on local communities and thus the tax can be offset by the benefits. Identifying all negative and positive impacts can help us understand whether environmental regulation is justifiable on distributional grounds or not, and whether compensation must be made to local communities when we take into account distributional effects. 

Mercury regulation can have huge impacts on the environmental and at the same time affect the livelihood of poor and marginalize populations that depend on mining as their main source of income. We hope the results of this project will lead to better policies that maximize welfare in mining countries.  

There are several obstacles that must be overcome in order to get consistent estimators of the causal effects of mining on health outcomes, but we are excited about the potential of this project and we believe most of these roadblocks can be overcome.

%Another related issue, is that not every permit evolves into a mine and that mining doesn't necessarily start as soon as the permit allows it.
%
%La ausencia de correlación entre la longitud de los peces y el contenido de
%mercurio en músculo de los mismos ha sido reportado para varias especies
%(Jackson, 1991).
%
%De acuerdo con Hakansson (1984), concentraciones de mercurio
%superiores a 0.075 μg Hg/g en especies acuáticas pueden ser atribuidos a
%actividades de tipo antropogénico
%
%Aunque las concentraciones de mercurio
%detectadas en la arenca superan estos niveles, no alcanzan a ser lo
%suficientemente altos para sobrepasar los límites internacionales vigentes de
%0.5 μg Hg/g en peces para consumo humano (WHO, 1990)
%
%No obstante,
%considerando que la arenca posee un peso promedio de 94.5 g (n=28) y la
%concentración media de mercurio encontrada en esta especie en María la
%Baja fue de 0.150 μg Hg/g, el consumo diario de tres arencas representa una
%ingestión total de 297.7 μg Hg/semana, valor muy cercano al límite
%provisional propuesto para consumo tolerable semanal de mercurio de 300
%μg (Galal, 1993).
%Identifying the treatment effect of being expose to pollution due to gold mining upstream has several policy implications. When policy makers enact regulations that adversely affect mining, they are imposing a cost on vulnerable populations who's livelihood greatly depends on mining. It is not uncommon in Colombia to see local communities endorsing mining while the central government, pressured by environmental groups, places stringent regulation\footnote{It is also not uncommon to see the governess push mining in certain areas.}. This can be thought of as placing a tax on a poor community (restricting mining) in order to finance a public good (clean environment) that everyone can enjoy. However, this public good can also have big positive impacts on local communities and thus the tax can be offset by the benefits. Health benefits can be one of those positive impacts that offsets the costs of restricting mining. Identifies all negative and positive impacts can help us understand whether environmental regulation is justifiable on distributional grounds or not, and whether compensation must be made to local communities when we take into account distributional effects. 
%la explotación de los recursos naturales se considera uno de los principales medios de desarrollo económico (DNP, 2010) y sus impactos ambientales son muchas veces ignorados o subvalorados
%
%
%a minería es cada vez más un importante renglón de la economía colombiana. Según el Sector de la Minería a Gran Escala (SMGE) la minería representó en 2011 el 24.2% de las exportaciones; el 2.4% del PIB; 20% del total de la inversión extranjera directa; 650 mil millones de pesos en construcción de infraestructura; 2.6 billones de pesos en compras a proveedores nacionales, 65 mil millones de pesos de inversión en responsabilidad social y 178 mil millones de pesos en responsabilidad ambiental.
%
%
%The principal source of human exposure to organic
%mercury is fish consumption. This form of mercury is
%monomethyl mercury (MeHg). \cite{Davidson2004}
%The fetal brain is especially susceptible to damage
%from exposure to organic mercury. The data are less
%clear concerning prenatal exposure to inorganic mercury.
%There is some animal evidence that effects of
%low exposures to MeHg early in development may
%not appear until later in life.15 Such delayed neurotoxicity
%appearing years after exposure has yet to be
%documented in humans and remains an open question.
%
%Exposure to MeHg in high doses has profound
%effects on the CNS and can be rapidly fatal.
%
%
%
%McKeown-Eyssen et al. (1983) investigated the relationship
%of neurological and developmental deficits to MeHg
%exposure from contaminated fish in 234 Northern Quebec
%Cree Indian children aged 12–30 month. In essence, this study can be interpreted
%as showing no developmental effects related to MeHg for
%the Cree children.
%
%
%Clinical neurological outcomes of MeHg exposure were
%also investigated in a Peruvian population of 131 mother–
%child pairs in a fishing village in Mancora, Peru, between
%1981 and 1984 (Marsh et al., 1995; Myers et al., 2000).
%This study showed no relationships
%between MeHg exposure and neurological outcomes.
%
%
%Murata et al. (1999a) performed neurological, neurodevelopmental,
%and neurophysiological tests on 149 first
%graders (age range: 6.4–7.4 years) living in the settlement of
%Caˆmara de Lobos on the island of Madeira (Portugal) who
%were exposed to MeHg from eating fish.
%The study results showed no
%clear MeHg-associated neurodevelopmental effects on behavioral
%testing.
%
%
%Cordier et al. (2002) (see also Cordier et al., 1998)
%investigated the effects of Hg exposure on neurological
%and neurodevelopmental functioning in a retrospective
%study of 378 Amer-Indian children aged 9 months to 6
%years living in different villages along the Maroni and
%Oyapock rivers in French Guiana. The results of this study showed
%increased deep tendon reflexes for children 2 years of age
%and older, which was significant in boys, but not in girls
%However, the study by
%Cordier et al. is difficult to interpret because of performance
%
%
%
%Perhaps the most frequently cited and informative large
%sample epidemiological studies of the developmental neurotoxicity
%of MeHg are the Seychelles Child Development
%Study (Davidson et al., 1995, 1998; Myers et al., 1995a,
%1995b) and the Faroe Islands investigations (Grandjean et al.,
%1997, 1998, 1999). Because these two studies are important
%epidemiological studies, they will be reviewed in detail.
%
%According to Grandjean et al. (1997), the results on 824
%children (aged 7 years) indicated that BAER peak latencies
%for waves III and V were significantly associated with Hg
%
%
%In summary, the two major longitudinal epidemiological
%studies of Hg exposure in children continue to show
%divergent results.
%
%
%
%\cite{Aragon2013} we find evidence of a positive effect of the mine’s demand for
%local inputs on real income. Taken together, our results underline the potential of backward
%linkages from extractive industries to create positive spillovers in
%less developed economies.
%
%
%It is estimated that all over the world more than 100
%million people are directly or indirectly exposed to
%mercury from small-scale mining (Spiegel et al., 2005),
%
%Poverty is the main reason for the disastrous health
%status in the small-scale mining communities.
%
 
 \nocite{}
\bibliographystyle{apacite} 
 \bibliography{bibmin}
 
\end{document}



\documentclass{beamer}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{threeparttable}
\usepackage{tabularx}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage[singlespacing]{setspace}
\usepackage{color}
\usepackage{beamerthemeshadow}
\usepackage{tabularx,colortbl}
\usepackage{multirow}
\setcounter{MaxMatrixCols}{10}
\usepackage{apacite}
\usepackage{filecontents}

%\usecolortheme{crane}
\definecolor{oliva}{rgb}{0.26,0.34,0.38}
\setbeamercovered{transparent}
%\usecolortheme[named=Berkeley]{structure}
\usetheme{Copenhagen}
\title[The hidden health costs of gold mining]{The hidden health costs of gold mining}
\author[Mauricio Romero]{Mauricio Romero}
%\author{Alvaro J. Riascos Villegas \\ Universidad de los Andes \& Quantil \and Eduardo Alfonso \\ Quantil  \and Mauricio Romero \\ University of California - San Diego}
\date{December 4,2013}
\begin{document}

\begin{frame}
\titlepage
\end{frame}

\AtBeginSection[]
{
   \begin{frame}
       \frametitle{Table of Contents}
       \tableofcontents[currentsection]
   \end{frame}
}

\section{Motivation}

\begin{frame}
\frametitle{Motivation}
\begin{itemize}
\item Colombia is at a crucial point where it has to make important decisions regarding mining and environmental regulation.
\item Currently mining represents less than 2.5\% of the country's GDP, but the government expects coal and gold extraction to increase over 70\% by 2020. 
\item In developing countries the communities next to mines are often poor, thus making regulation not only a environmental policy but a distributional one too. 
\item There is few causal evidence of the effect of mining on health

\end{itemize}
\end{frame}

\begin{frame}[plain]
\frametitle{Motivation}
\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.4]{MiningArea.pdf}
\\
\end{center}
\footnotesize{Total area in $m^2$ of the mining permits given in a year.}\\
\footnotesize{Source: INGEOMINAS.}

\label{fig:1}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Research question}
What is the effect of gold mining on health?
\begin{itemize}
\item Diff-diff strategy using municipalities upstream/downstream from the mines
\item Measure the impacts on newborns and medical consultation.
\end{itemize}

\end{frame}


\section{Mining Sector in Colombia}

\begin{frame}
\frametitle{Mining in Colombia}
\begin{itemize}
\item In 2011 mining represented 2.20\% of Colombia's GDP, compared to 13.2\% in Chile and 4.2\% in Peru. 
\item Colombia is the world's largest mercury polluter per capita from small scale gold mining\cite{Cordy2011}.
\item In 2013 congress ruled that mercury couldn't be used in mining by 2018 \cite{Congreso2013}.
\item Some communities support mining other have banned it.
\end{itemize}
\end{frame}





\section{Literature Review}

\begin{frame}
\frametitle{Literature Review}
\begin{itemize}
\item \citeA{Aragon2013a} finds that mining reduces agricultural productivity by almost 40\% in surrounding areas.

\item \citeA{Hilson2002} finds that small scale gold mining in Ghana has lead to socio-economic growth accompanied by mercury pollution and land degradation.


\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Literature Review}
\begin{itemize}
\item Studies often focus on correlation between mining and health but due to confounding effects can not imply causation. \cite{Fernandez-Navarro2012, Attfield2008, Chakraborti2013}.
\item \citeA{Aragon2013} find evidence of a positive effect of mining on real income, that could improve welfare in the long run.
\end{itemize}
\end{frame}




\begin{frame}
\frametitle{Literature Review}
\begin{itemize}
\item Humans are exposed to both metallic and methyl mercury due to the use of the former in gold mining. 

\item There is evidence that those limits are exceeded in Colombia near mining areas \cite{GUIZA2013,Olivero2002}
\end{itemize}
\end{frame}

\section{Data and Methodology}

\begin{frame}
\frametitle{Methodology}

\begin{center}
    \includegraphics[scale=0.95]{ddmining.png}
  \end{center}
\[ y_{it}= \alpha + \beta \  Mine_{it} + \gamma_i +\delta_t + \epsilon_{it} \]
\end{frame}


\begin{frame}
\frametitle{Underlying Assumptions}
\begin{itemize}

\item Populations living on both sides of the mine have similar trends before the mine. 
\item Pollutants that are dropped in the river travel downstream and thus do not affect populations living upstream.
\item Fish do not travel upstream in the mine border.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Data}
\begin{itemize}
\item Use the mining permit database collected by the NGO Tierra Minada from official government records.
\item There is a total of 20,928 permits given from 1990 to 2010. 
\item The number of gold mine permits is 9,964 .
\item More than half the permits where given in 2004.
\end{itemize}
\end{frame}


\begin{frame}[plain]
\frametitle{Data}
\begin{figure}[H]
\begin{center}
\caption{Gold mining permits}
\includegraphics[scale=0.35]{GoldPermits.pdf}
\\
\footnotesize{Number of permits given each year for gold mining. Source: INGEOMINAS. Calcuations: Author.}
\end{center}

\label{fig:2}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Data}
\begin{itemize}
\item Vital Statistics survey from Colombia. 
\item There is information on birth weight and height, as well as on number of gestation weeks and APGAR score. 
\item The APGAR score is of particular interest since there is evidence that the fetal brain is especially susceptible to damage from exposure to mercury \cite{Davidson2004}.
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Data}
\begin{itemize}
\item The second data base corresponds to the RIPS (Individual Register of Health Services Provision) data base  which contains individual-level registers of medical consultations that took place in any health service institution in Colombia. 
\item  With this information we could see if there is an increase in the number of consultations associated with mercury exposure.
\end{itemize}
\end{frame}

\section{Discussion}

\begin{frame}
\frametitle{Problems...}
\begin{itemize}
\item I do not have mercury levels for different spots in a river or the amount of mercury in fish or human populations.
\item According to the government over 60\% mining units do not have a permit (lower bound).
\item Fish do not only travel downstream. 
\item \citeA{Olivero2002} find great variability in mercury level across fish species in Colombia. 

\begin{enumerate}
\item Arenca $\rightarrow$ 0.177 $\mu$g/kg body weight/day.
\item Bocachico $\rightarrow$ 0.016 $\mu$g/kg body weight/day
\end{enumerate}

\end{itemize}
\end{frame}

\renewcommand*{\refname}{} % This will define heading of bibliography to be empty, so you can...
\section{Bibliography} 

\begin{frame}[allowframebreaks]{Bibliography}
\bibliographystyle{apacite}
\bibliography{bibmin}
\end{frame}



\end{document}


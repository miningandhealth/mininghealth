\beamer@endinputifotherversion {3.22pt}
\select@language {english}
\beamer@sectionintoc {1}{Motivation}{2}{0}{1}
\beamer@sectionintoc {2}{Mining Sector in Colombia}{6}{0}{2}
\beamer@sectionintoc {3}{Literature Review}{8}{0}{3}
\beamer@sectionintoc {4}{Data and Methodology}{12}{0}{4}
\beamer@sectionintoc {5}{Discussion}{19}{0}{5}
\beamer@sectionintoc {6}{Bibliography}{21}{0}{6}
\contentsline {section}{}

\beamer@endinputifotherversion {3.26pt}
\select@language {english}
\beamer@sectionintoc {1}{Motivation and context}{2}{0}{1}
\beamer@sectionintoc {2}{This paper}{9}{0}{2}
\beamer@sectionintoc {3}{Data}{19}{0}{3}
\beamer@sectionintoc {4}{Identification Strategy and results}{23}{0}{4}
\beamer@sectionintoc {5}{Robustness Checks}{39}{0}{5}
\beamer@sectionintoc {6}{Closing remarks}{41}{0}{6}
\contentsline {section}{}

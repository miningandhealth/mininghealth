\begin{thebibliography}{}

\bibitem [\protect \citeauthoryear {%
Almond%
\ \BBA {} Mazumder%
}{%
Almond%
\ \BBA {} Mazumder%
}{%
{\protect \APACyear {2011}}%
}]{%
Almond2011}
\APACinsertmetastar {%
Almond2011}%
\begin{APACrefauthors}%
Almond, D.%
\BCBT {}\ \BBA {} Mazumder, B.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2011}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Health Capital and the Prenatal Environment: The Effect
  of Ramadan Observance during Pregnancy} {Health capital and the prenatal
  environment: The effect of ramadan observance during pregnancy}.{\BBCQ}
\newblock
\APACjournalVolNumPages{American Economic Journal: Applied
  Economics}{3}{4}{56-85}.
\newblock
\begin{APACrefURL}
  \url{http://www.aeaweb.org/articles.php?doi=10.1257/app.3.4.56}
  \end{APACrefURL}
\newblock
\begin{APACrefDOI} \doi{10.1257/app.3.4.56} \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Aragon%
\ \BBA {} Rud%
}{%
Aragon%
\ \BBA {} Rud%
}{%
{\protect \APACyear {2013}}%
{\protect \APACexlab {{\protect \BCnt {1}}}}}]{%
Aragon2013a}
\APACinsertmetastar {%
Aragon2013a}%
\begin{APACrefauthors}%
Aragon, F\BPBI M.%
\BCBT {}\ \BBA {} Rud, J\BPBI P.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013{\protect \BCnt {1}}}{February}{}.
\newblock
\APACrefbtitle {Modern Industries, Pollution and Agricultural Productivity:
  Evidence from Mining in Ghana.} {Modern industries, pollution and
  agricultural productivity: Evidence from mining in ghana.}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Aragon%
\ \BBA {} Rud%
}{%
Aragon%
\ \BBA {} Rud%
}{%
{\protect \APACyear {2013}}%
{\protect \APACexlab {{\protect \BCnt {2}}}}}]{%
Aragon2013}
\APACinsertmetastar {%
Aragon2013}%
\begin{APACrefauthors}%
Aragon, F\BPBI M.%
\BCBT {}\ \BBA {} Rud, J\BPBI P.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013{\protect \BCnt {2}}}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Natural Resources and Local Communities: Evidence from a
  Peruvian Gold Mine} {Natural resources and local communities: Evidence from a
  peruvian gold mine}.{\BBCQ}
\newblock
\APACjournalVolNumPages{American Economic Journal: Economic
  Policy}{5}{2}{1-25}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
{El Colombiano}%
}{%
{El Colombiano}%
}{%
{\protect \APACyear {2014}}%
}]{%
ElColombiano2014}
\APACinsertmetastar {%
ElColombiano2014}%
\begin{APACrefauthors}%
{El Colombiano}.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2014}{}{}.
\newblock
\APACrefbtitle {En 60\% sube consumo de pescado en Colombia durante Semana
  Santa.} {En 60\% sube consumo de pescado en colombia durante semana santa.}
\newblock
\APAChowpublished
  {\url{http://www.eltiempo.com/colombia/tolima/ARTICULO-WEB-NEW_NOTA_INTERIOR-12955288.html}}.
\newblock
\APACrefnote{Accessed: 07/08/2014}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Guiza%
\ \BBA {} Aristizabal%
}{%
Guiza%
\ \BBA {} Aristizabal%
}{%
{\protect \APACyear {2013}}%
}]{%
GUIZA2013}
\APACinsertmetastar {%
GUIZA2013}%
\begin{APACrefauthors}%
Guiza, L.%
\BCBT {}\ \BBA {} Aristizabal, J\BPBI D.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013}{04}{}.
\newblock
{\BBOQ}\APACrefatitle {Mercury and gold mining in Colombia: a failed state}
  {Mercury and gold mining in colombia: a failed state}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Universitas Scientiarum}{18}{}{33 - 49}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Luechinger%
}{%
Luechinger%
}{%
{\protect \APACyear {2014}}%
}]{%
Luechinger2014}
\APACinsertmetastar {%
Luechinger2014}%
\begin{APACrefauthors}%
Luechinger, S.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2014}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Air pollution and infant mortality: A natural experiment
  from power plant desulfurization} {Air pollution and infant mortality: A
  natural experiment from power plant desulfurization}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Journal of Health Economics}{37}{0}{219 - 231}.
\newblock
\begin{APACrefURL}
  \url{http://www.sciencedirect.com/science/article/pii/S0167629614000897}
  \end{APACrefURL}
\newblock
\begin{APACrefDOI} \doi{http://dx.doi.org/10.1016/j.jhealeco.2014.06.009}
  \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Olivero%
\ \BBA {} Johnson%
}{%
Olivero%
\ \BBA {} Johnson%
}{%
{\protect \APACyear {2002}}%
}]{%
Olivero2002}
\APACinsertmetastar {%
Olivero2002}%
\begin{APACrefauthors}%
Olivero, J.%
\BCBT {}\ \BBA {} Johnson, B.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYear{2002}.
\newblock
\APACrefbtitle {El lado gris de la mineria de oro: La contaminacion con
  mercurio en el norte de Colombia} {El lado gris de la mineria de oro: La
  contaminacion con mercurio en el norte de colombia}.
\newblock
\BUMTh, Universidad de Cartagena.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Schlenker%
\ \BBA {} Walker%
}{%
Schlenker%
\ \BBA {} Walker%
}{%
{\protect \APACyear {2011}}%
}]{%
Schlenker2011}
\APACinsertmetastar {%
Schlenker2011}%
\begin{APACrefauthors}%
Schlenker, W.%
\BCBT {}\ \BBA {} Walker, W\BPBI R.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2011}{December}{}.
\newblock
{\BBOQ}\APACrefatitle {Airports, Air Pollution, and Contemporaneous Health}
  {Airports, air pollution, and contemporaneous health}{\BBCQ}\ [Working
  Paper].
\newblock
\APACjournalVolNumPages{}{}{17684}{}.
\newblock
\begin{APACrefURL} \url{http://www.nber.org/papers/w17684} \end{APACrefURL}
\newblock
\begin{APACrefDOI} \doi{10.3386/w17684} \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Schofield%
}{%
Schofield%
}{%
{\protect \APACyear {2014}}%
}]{%
Schofield2014}
\APACinsertmetastar {%
Schofield2014}%
\begin{APACrefauthors}%
Schofield, H.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYear{2014}.
\newblock
\APACrefbtitle {The Economic Costs of Low Caloric Intake: Evidence from India}
  {The economic costs of low caloric intake: Evidence from india}.
\newblock
\BUPhD.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
UNEP%
}{%
UNEP%
}{%
{\protect \APACyear {2013}}%
}]{%
UNEP2013}
\APACinsertmetastar {%
UNEP2013}%
\begin{APACrefauthors}%
UNEP.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013}{}{}.
\newblock
\APACrefbtitle {Global Mercury Assessment 2013: Sources, emissions, releases,
  and environmental transport} {Global mercury assessment 2013: Sources,
  emissions, releases, and environmental transport}\ \APACbVolEdTR{}{\BTR{}}.
\newblock
\APACaddressInstitution{}{United Nations Environment Programme Chemicals
  Branch, Geneva, Switzerland}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
von~der Goltz%
\ \BBA {} Barnwal%
}{%
von~der Goltz%
\ \BBA {} Barnwal%
}{%
{\protect \APACyear {2014}}%
}]{%
Goltz2014}
\APACinsertmetastar {%
Goltz2014}%
\begin{APACrefauthors}%
von~der Goltz, J.%
\BCBT {}\ \BBA {} Barnwal, P.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2014}{February}{}.
\newblock
{\BBOQ}\APACrefatitle {Mines: The Local Welfare Effects of Mineral Mining in
  Developing Countries} {Mines: The local welfare effects of mineral mining in
  developing countries}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Discussion Paper No.: 1314-19 Columbia
  University}{}{}{}.
\PrintBackRefs{\CurrentBib}

\end{thebibliography}

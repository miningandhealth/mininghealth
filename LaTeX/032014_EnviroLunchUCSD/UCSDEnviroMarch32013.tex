\documentclass[pdf]{beamer}
\usepackage{booktabs}
\usepackage{tabularx,colortbl}
\usepackage{multirow}
\usepackage[export]{adjustbox}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{comment}
\usepackage{wrapfig}
\usepackage{dsfont}
\usepackage{bbm}
\usepackage[flushleft]{threeparttable}      % Option: Table notes
\usepackage{floatpag}
\usepackage{wrapfig}
\usepackage{relsize}
\usepackage{setspace}                       
\mode<presentation>{}
\usetheme{Warsaw}


\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}

\expandafter\def\expandafter\insertshorttitle\expandafter{%
  \insertshorttitle\hfill%
  \insertframenumber\,/\,\inserttotalframenumber}
\title[Mining and health]{The hidden health costs and benefits of mining}

\author[Romero-Saavedra]{Mauricio Romero  \\ Santiago Saavedra (Stanford) }

\normalsize

\date{March 10 2014}




\begin{document}

\frame{\titlepage}
 
\AtBeginSection[]
{
   \begin{frame}
       \frametitle{Table of Contents}
       \tableofcontents[currentsection]
   \end{frame}
}
\section{Motivation}

\begin{frame}[plain]

\frametitle{Motivation}

\begin{figure}[H]
\begin{center}

\includegraphics[scale=0.3]{Motiv_map.jpg}
\\
\caption{Location of gold mines in Colombia 2010}
\footnotesize{Source: Tierra Minada}
\end{center}

\label{Motiv_map}
\end{figure}


\end{frame}

\begin{frame}

\frametitle{Recent mining boom}



\begin{itemize}
\item Number of gold mine permits in $2004$ alone, more than double the existing permits in Colombia.
\item Mercury used for amalgamation is dropped in water sources.
\item Besides drinking water, mercury is ingested by humans through fish consumption.
\item Evidence that EPA mercury limits are exceeded in Colombia near mining areas. (Guiza and Aristizabal, 2013).
\item But not historical measurements in all municipalities.
\end{itemize}

\end{frame}
\begin{frame}
\frametitle{Research Question}
What are the health costs/benefits of gold mining ?
\begin{itemize}
\item Diff-in-diff strategy using municipalities upstream/downstream from the mines across time.
\item ``Treatment intensity'' depending on size of the mine.
\item Measure the impact on newborns .
\item Welfare analysis of income and health tradeoffs.
\end{itemize}
\end{frame}

\section{Brief Literature Review}



\begin{frame}
\frametitle{Related literature}
\begin{itemize}
\item Fetal brain is especially susceptible to damage from exposure to mercury. Davidson (2004)
\item Long term effects of low APGAR scores on cognitive skills (Ehrenstein 2009).
\item Aragon-Rud (2013a) finds that mining reduced agricultural productivity through ecological damage by almost 40\% in Ghana.
\item Evidence of a positive effect of mining on real income in Peru  (Aragon-Rud, 2013b)
\end{itemize}
\end{frame}



\section{Data and Research Design}
\begin{frame}[label=dat1]
\frametitle{Data}
Health
\begin{itemize}
\item Vital Statistics for all 1,102 Colombian municipalities 
\item From 1998-2008, there is information on all newborns weight and height, as well as on number of gestation weeks and APGAR score (Appearance, Pulse, Grimace, Activity, Respiration). 
\end{itemize}
\hyperlink{sumstat1<1>}{\beamergotobutton{Summary Statistics}}
\end{frame}






\begin{frame}[label=dat2]
\frametitle{Data}
Mines
\begin{itemize}
\item Mining permits database collected from 1990 to 2010. 
\item Includes GPS coordinates, area and mineral for each mine.
\item The number of gold mine permits is 1,759 out of 8,973.
\item The number of permits before 2009 is 975.
\end{itemize}
%\hyperlink{sumstat2<1>}{\beamergotobutton{Summary Statistics}}
\end{frame}




\begin{frame}
\frametitle{ID strategy}
\begin{figure}[H]
\begin{center}

\includegraphics[scale=0.50]{Ideal_id.jpg}
\\
\caption{Ideal ID Strategy}
\footnotesize{Green: Control Municipality, Yellow:Municipality with mine, Orange: Downstream municipality}
\end{center}

\label{Ideal_id}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{ID strategy}
\begin{figure}[H]
\begin{center}

\includegraphics[scale=0.40]{Problem_id.jpg}
\\
\caption{Problem ID strategy}
\footnotesize{What to do with controls that get mines or duality upstream-downstream?}
\end{center}

\label{Motiv_map}
\end{figure}

\end{frame}



\section{Preliminary Results}

\begin{frame}
\frametitle{Parallel Trends}
{\scriptsize $LowAPGAR_{imt}=\alpha+ \beta_1 TimeTrend_{t}+ \beta_2 Mining\_Municipality_{m} + \beta_3 TimeTrend_{t} \times Mining\_Municipality_{m} + \beta_4 X_{i}+ \gamma_{Year(t)}+ \gamma_{Month(t)} +\gamma_m + \varepsilon_{imt} $}
\\[1cm]
\begin{scriptsize}
\begin{itemize}
\item $LowAPGAR_{imt}$ whether baby $i$ had low APGAR in municipality $m$ day $t$
\item $TimeTrend_{t}$ Time trend
\item $Mining\_Municipality_{m}=1$ if municipality that has a mine at some point in time.
\item $X_i$ individual control (age of the mom)
\item $\gamma_{Year(t)}$ Year fixed effects
\item $\gamma_{Month(t)}$ month fixed effects
\item $\gamma_m$ state fixed effects
\item $ \varepsilon_{imt}$ error term
\end{itemize}
\end{scriptsize}

\end{frame}


\begin{frame}[plain]
\begin{table}[H]\centering

\begin{scriptsize}


\begin{tabular}{l*{3}{c}}
\toprule
                            &\multicolumn{1}{c}{(2)}         &\multicolumn{1}{c}{(3)}         &\multicolumn{1}{c}{(4)}         \\
\midrule
Time Trend                &   -0.000047\sym{*}  &   -0.000046\sym{**} &   -0.000041\sym{***}\\
                           &  (0.000020)         &  (0.000017)         &  (0.000012)         \\
Mining Dummy    &      -0.049         &      -0.041         &      -0.019         \\
                             &     (0.038)         &     (0.036)         &     (0.031)         \\
Time Trend X Mining Dummy  &   0.0000031         &   0.0000027         &   0.0000013         \\
                            & (0.0000023)         & (0.0000022)         & (0.0000019)         \\
Month and Year F.E.         &         Yes         &         Yes         &         Yes         \\
Mother Characteristics       &         Yes         &         Yes         &         Yes         \\
State F.E.                 &         Yes         &         Yes         &         Yes         \\
Municipalities & $<30 KM$ & $<50 KM$ & All \\
\midrule
Observations              &     2561536         &     2983965         &     6759345         \\
Adjusted ($R^{2}$)          &       0.006         &       0.006         &       0.008         \\
\bottomrule
\multicolumn{4}{l}{\footnotesize Municipality clustered standard errors in parentheses}\\
\multicolumn{4}{l}{\footnotesize \sym{*} \(p<0.05\), \sym{**} \(p<0.01\), \sym{***} \(p<0.001\)}\\
\end{tabular}
\end{scriptsize}
\end{table}


\end{frame}


\begin{frame}
\frametitle{Preliminary results}
{\scriptsize $LowAPGAR_{imt}=\alpha+ \beta_1 Mined\_Area_{mt}+ \beta_1 (Mined\_Area_{mt})^2+ \beta_3 Upstream\_Area_{mt}+ \beta_4 (Upstream\_Area_{mt})^2  + \beta_5 X_{i}+ \gamma_{Year(t)}+ \gamma_{Month(t)}+ \gamma_m  + \varepsilon_{imt} $}
\\[1cm]
\begin{scriptsize}
\begin{itemize}
\item $LowAPGAR_{imt}$ whether baby $i$ had low APGAR in municipality $m$ day $t$
\item $Mined\_Area_{mt}$ proportion of area mined in the municipality $m$ at time $t$
\item $Upstream\_Area_{mt}$ area mined upstream (weighted by distance , $\delta=0.97$) as a proportion of municipality area.
\item $X_i$ individual control (age of the mom)
\item $\gamma_{Year(t)}$ Year fixed effects
\item $\gamma_{Month(t)}$ month fixed effects
\item $\gamma_m$ state fixed effects
\item $ \varepsilon_{imt}$ error term
\end{itemize}
\end{scriptsize}

\end{frame}

%\begin{frame}[plain]
%\frametitle{Preliminary results}
%\begin{tiny}
%\begin{tabular}{l*{5}{c}}
%\toprule
%                    &\multicolumn{1}{c}{(1)}         &\multicolumn{1}{c}{(2)}         &\multicolumn{1}{c}{(3)}                &\multicolumn{1}{c}{(4)}         &\multicolumn{1}{c}{(5)}         \\
%\midrule
%$Mined Area$      &       -0.15         &       -0.12         &      -0.045                &      -0.062         &      -0.025         \\
%                    &      (0.29)         &      (0.28)         &      (0.16)               &      (0.18)         &      (0.17)         \\
%\addlinespace
%$(Mined Area)^2$     &        0.88         &        0.77         &        0.37                &        0.46         &        0.32         \\
%                    &      (1.04)         &      (0.99)         &      (0.51)                &      (0.58)         &      (0.53)         \\
%\addlinespace
%$Upstream Area$ &   0.0000099         &   0.0000073         &   0.0000021         &     0.0000029         &   0.0000046         \\
%                    &  (0.000014)         &  (0.000012)         &  (0.000014)         &  (0.000027)         &  (0.000025)         \\
%\addlinespace
%$(Upstream Area)^2$&    -2.8e-09         &    -2.3e-09         &    -1.6e-09         &      -1.1e-09         &    -1.6e-09         \\
%                    &   (2.6e-09)         &   (2.3e-09)         &   (2.5e-09)         &     (6.2e-09)         &   (5.9e-09)         \\
%\addlinespace
%Month and Year F.E. &         Yes         &         Yes         &         Yes              &         Yes         &         Yes         \\
%\addlinespace
%Mother Characteristics&          No         &         Yes         &         Yes                &         Yes         &         Yes         \\
%\addlinespace
%State F.E.          &          No         &          No         &         Yes                &         Yes         &         Yes         \\
%Municipalities & All & All & All &  $<30 KM$ & $<50 KM$ \\
%\midrule
%Observations        &     3885420         &     3867154         &     3867154               &     2176645         &     2598288         \\
%Adjusted \(R^{2}\)  &       0.000         &       0.002         &       0.009                &       0.007         &       0.007         \\
%\bottomrule
%\multicolumn{6}{l}{\footnotesize Municipality clustered standard errors in parentheses}\\
%\multicolumn{6}{l}{\footnotesize \sym{*} \(p<0.05\), \sym{**} \(p<0.01\), \sym{***} \(p<0.001\)}\\
%\end{tabular}
%
%
%\end{tiny}


%\end{frame}

\begin{frame}[plain]
\frametitle{Preliminary results}
\begin{tiny}
\begin{tabular}{l*{5}{c}}
\toprule
                    &\multicolumn{1}{c}{(1)}         &\multicolumn{1}{c}{(2)}         &\multicolumn{1}{c}{(3)}                &\multicolumn{1}{c}{(4)}         &\multicolumn{1}{c}{(5)}         \\
\midrule
$Mined\_Area$      &       0.098         &        0.10         &      -0.076              &      -0.068         &      -0.038         \\
                    &      (0.28)         &      (0.27)         &      (0.16)                 &      (0.19)         &      (0.18)         \\
\addlinespace
$(Mined\_Area)^2$     &       0.039         &      -0.017         &        0.40                &        0.45         &        0.34         \\
                    &      (1.03)         &      (0.97)         &      (0.51)                  &      (0.61)         &      (0.57)         \\
\addlinespace
$Upstream\_Area$ &    0.000017         &    0.000014         &  -0.0000025        &  -0.0000028         & -0.00000083         \\
                    &  (0.000015)         &  (0.000014)         &  (0.000014)                &  (0.000032)         &  (0.000030)         \\
\addlinespace
$(Upstream\_Area)^2$&    -4.1e-09         &    -3.6e-09         &    -9.0e-10         &         5.5e-11         &    -4.3e-10         \\
                    &   (2.8e-09)         &   (2.6e-09)         &   (2.6e-09)                &   (7.4e-09)         &   (7.0e-09)         \\
\addlinespace
Month and Year F.E. &         Yes         &         Yes         &         Yes               &         Yes         &         Yes         \\
\addlinespace
Mother Characteristics&          No         &         Yes         &         Yes                 &         Yes         &         Yes         \\
\addlinespace
State F.E.          &          No         &          No         &         Yes                &         Yes         &         Yes         \\
Municipalities & All & All & All &  $<30 KM$ & $<50 KM$ \\
\midrule
Observations        &     6785820         &     6759345         &     6759345                 &     2561536         &     2983965         \\
Adjusted \(R^{2}\)  &       0.000         &       0.002         &       0.008                &       0.006         &       0.006         \\
\bottomrule
\multicolumn{6}{l}{\footnotesize Municipality clustered standard errors in parentheses}\\
\multicolumn{6}{l}{\footnotesize \sym{*} \(p<0.05\), \sym{**} \(p<0.01\), \sym{***} \(p<0.001\)}\\
\end{tabular}


\end{tiny}


\end{frame}


\begin{frame}
\frametitle{Limitations}
\begin{itemize}
\item No direct measurement of mercury.
\begin{itemize}
\item U\$ 21 for 50 colorimetric paper tests
\end{itemize}
\item No data on mercury use or gold production
\begin{itemize}
\item Quarterly since 2004.
\end{itemize}
\item According to the government over 60\% mining units do not have a permit (lower bound).
\begin{itemize}
\item Map of illegal mining in 2008.
\end{itemize}
\item Fishes traveling upstream or local fish markets
\item Olivero (2002) find great variability in mercury level across fish species in Colombia. 
\begin{enumerate}
\item Arenca $\rightarrow$ 0.177 $\mu$g/kg body weight/day.
\item Bocachico $\rightarrow$ 0.016 $\mu$g/kg body weight/day
\end{enumerate}
\end{itemize}
\end{frame}



\begin{frame}
\frametitle{Future steps}
\begin{itemize}
\item Automatize/Improve  upstream-downstream process (Python-GIS)
\item Vital Statistics beyond 2008.
\item Buffer analysis.
\item Location within municipalities.
\item Account for migration to work in the mines.
\item Account for rivers' flow.
\item Welfare analysis of income and health tradeoffs.
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Thank you}
\begin{itemize}
\item Gracias
\item Asante Sana
\item Merci
\item Obrigado
\item Grazie
\end{itemize}

\end{frame}


\begin{frame}[label=sumstat1]
\frametitle{Summary Statistics I}

\begin{tabular}{lrrrr}
  \hline
 & Mean & Std. Dev. & Min & Max \\ 
  \hline
Births per day & 1849.09 & 174.99 & 1328.00 & 2502.00 \\ 
  Male & 0.51 & 0.50 & 0.00 & 1.00 \\ 
  Weight & 3142.37 & 522.88 & 145.00 & 9900.00 \\ 
  Height & 49.50 & 2.83 & 9.00 & 95.00 \\ 
  Low APGAR ($\leq$ 6) & 0.06 & 0.24 & 0.00 & 1.00 \\ 
  Mother's Age & 25.13 & 6.55 & 9.00 & 54.00 \\ 
   \hline
\end{tabular}
\hyperlink{dat1<1>}{\beamerreturnbutton{Go Back}}
\end{frame}

%\begin{frame}[label=sumstat2]
%\frametitle{Summary Statistics II}
%\begin{table}[H]\centering \caption{Summary statistics \label{sumstat}}
%\begin{tabular}{l c c c c }\hline\hline
%\multicolumn{1}{c}{\textbf{Variable}} & \textbf{Mean}
% & \textbf{Std. Dev.}& \textbf{Min.} &  \textbf{Max.} \\ \hline
%Area km\^{}2 & 1.338 & 7.091 & 0 & 190.609 \\
%Year & 2003.338 & 3.87 & 1990 & 2008 \\
%\multicolumn{1}{c}{N} & \multicolumn{4}{c}{7638}\\ \hline
%\end{tabular}
%\end{table}
%\hyperlink{dat2<1>}{\beamerreturnbutton{Go Back}}
%\end{frame}

\end{document}

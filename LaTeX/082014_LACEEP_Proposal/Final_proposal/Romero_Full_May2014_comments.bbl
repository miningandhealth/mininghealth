\begin{thebibliography}{}

\bibitem [\protect \citeauthoryear {%
Aragon%
\ \BBA {} Rud%
}{%
Aragon%
\ \BBA {} Rud%
}{%
{\protect \APACyear {2013}}%
{\protect \APACexlab {{\protect \BCnt {1}}}}}]{%
Aragon2013a}
\APACinsertmetastar {%
Aragon2013a}%
\begin{APACrefauthors}%
Aragon, F\BPBI M.%
\BCBT {}\ \BBA {} Rud, J\BPBI P.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013{\protect \BCnt {1}}}{February}{}.
\newblock
\APACrefbtitle {Modern Industries, Pollution and Agricultural Productivity:
  Evidence from Mining in Ghana.} {Modern industries, pollution and
  agricultural productivity: Evidence from mining in ghana.}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Aragon%
\ \BBA {} Rud%
}{%
Aragon%
\ \BBA {} Rud%
}{%
{\protect \APACyear {2013}}%
{\protect \APACexlab {{\protect \BCnt {2}}}}}]{%
Aragon2013}
\APACinsertmetastar {%
Aragon2013}%
\begin{APACrefauthors}%
Aragon, F\BPBI M.%
\BCBT {}\ \BBA {} Rud, J\BPBI P.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013{\protect \BCnt {2}}}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Natural Resources and Local Communities: Evidence from a
  Peruvian Gold Mine} {Natural resources and local communities: Evidence from a
  peruvian gold mine}.{\BBCQ}
\newblock
\APACjournalVolNumPages{American Economic Journal: Economic
  Policy}{5}{2}{1-25}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Asner%
, Llactayo%
, Tupayachi%
\BCBL {}\ \BBA {} Luna%
}{%
Asner%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2013}}%
}]{%
Asner2013}
\APACinsertmetastar {%
Asner2013}%
\begin{APACrefauthors}%
Asner, G\BPBI P.%
, Llactayo, W.%
, Tupayachi, R.%
\BCBL {}\ \BBA {} Luna, E\BPBI R.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Elevated rates of gold mining in the Amazon revealed
  through high-resolution monitoring} {Elevated rates of gold mining in the
  amazon revealed through high-resolution monitoring}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Proceedings of the National Academy of Sciences}{}{}{}.
\newblock
\begin{APACrefURL}
  \url{http://www.pnas.org/content/early/2013/10/23/1318271110.abstract}
  \end{APACrefURL}
\newblock
\begin{APACrefDOI} \doi{10.1073/pnas.1318271110} \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Attfield%
\ \BBA {} Kuempel%
}{%
Attfield%
\ \BBA {} Kuempel%
}{%
{\protect \APACyear {2008}}%
}]{%
Attfield2008}
\APACinsertmetastar {%
Attfield2008}%
\begin{APACrefauthors}%
Attfield, M.%
\BCBT {}\ \BBA {} Kuempel, E.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2008}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Mortality among U.S. underground coal miners: A 23-year
  follow-up} {Mortality among u.s. underground coal miners: A 23-year
  follow-up}.{\BBCQ}
\newblock
\APACjournalVolNumPages{American Journal of Industrial
  Medicine}{51}{4}{231-245}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Bose-O'Reilly%
}{%
Bose-O'Reilly%
}{%
{\protect \APACyear {2010}}%
}]{%
OReily}
\APACinsertmetastar {%
OReily}%
\begin{APACrefauthors}%
Bose-O'Reilly, S\BPBI e.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2010}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Health assessment of artisanal gold miners in Tanzania}
  {Health assessment of artisanal gold miners in tanzania}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Science of the Total Environment}{}{408}{796-805}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Chakraborti%
\ \protect \BOthers {.}}{%
Chakraborti%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2013}}%
}]{%
Chakraborti2013}
\APACinsertmetastar {%
Chakraborti2013}%
\begin{APACrefauthors}%
Chakraborti, D.%
, Rahman, M\BPBI M.%
, Murrill, M.%
, Das, R.%
, Siddayya%
, Patil, S.%
\BDBL {}Das, K\BPBI K.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Environmental arsenic contamination and its health
  effects in a historic gold mining area of the Mangalur greenstone belt of
  Northeastern Karnataka, India} {Environmental arsenic contamination and its
  health effects in a historic gold mining area of the mangalur greenstone belt
  of northeastern karnataka, india}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Journal of Hazardous Materials}{262}{0}{1048 - 1055}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Clarkson%
, Magos%
\BCBL {}\ \BBA {} Myers%
}{%
Clarkson%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2003}}%
}]{%
Clarkson2003}
\APACinsertmetastar {%
Clarkson2003}%
\begin{APACrefauthors}%
Clarkson, T\BPBI W.%
, Magos, L.%
\BCBL {}\ \BBA {} Myers, G\BPBI J.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2003}{}{}.
\newblock
{\BBOQ}\APACrefatitle {The Toxicology of Mercury - Current Exposures and
  Clinical Manifestations} {The toxicology of mercury - current exposures and
  clinical manifestations}.{\BBCQ}
\newblock
\APACjournalVolNumPages{New England Journal of Medicine}{349}{18}{1731-1737}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Cordy%
\ \protect \BOthers {.}}{%
Cordy%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2011}}%
}]{%
Cordy2011}
\APACinsertmetastar {%
Cordy2011}%
\begin{APACrefauthors}%
Cordy, P.%
, Veiga, M\BPBI M.%
, Salih, I.%
, Al-Saadi, S.%
, Console, S.%
, Garcia, O.%
\BDBL {}Roeser, M.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2011}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Mercury contamination from artisanal gold mining in
  Antioquia, Colombia: The world's highest per capita mercury pollution}
  {Mercury contamination from artisanal gold mining in antioquia, colombia: The
  world's highest per capita mercury pollution}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Science of The Total Environment}{410 - 411}{0}{154 -
  160}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Counter%
\ \BBA {} Buchanan%
}{%
Counter%
\ \BBA {} Buchanan%
}{%
{\protect \APACyear {2004}}%
}]{%
Counter2004}
\APACinsertmetastar {%
Counter2004}%
\begin{APACrefauthors}%
Counter, S.%
\BCBT {}\ \BBA {} Buchanan, L\BPBI H.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2004}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Mercury exposure in children: a review} {Mercury
  exposure in children: a review}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Toxicology and Applied Pharmacology}{198}{2}{209 -
  230}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
da Veiga%
}{%
da Veiga%
}{%
{\protect \APACyear {1997}}%
}]{%
Veiga1997}
\APACinsertmetastar {%
Veiga1997}%
\begin{APACrefauthors}%
da Veiga, M.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{1997}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Introducing New Technologies for Abatement of Global
  Mercury Pollution in Latin America} {Introducing new technologies for
  abatement of global mercury pollution in latin america}.{\BBCQ}
\newblock

\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Davidson%
, Myers%
\BCBL {}\ \BBA {} Weiss%
}{%
Davidson%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2004}}%
}]{%
Davidson2004}
\APACinsertmetastar {%
Davidson2004}%
\begin{APACrefauthors}%
Davidson, P\BPBI W.%
, Myers, G\BPBI J.%
\BCBL {}\ \BBA {} Weiss, B.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2004}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Mercury Exposure and Child Development Outcomes}
  {Mercury exposure and child development outcomes}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Pediatrics}{113}{Supplement 3}{1023-1029}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
{El Colombiano}%
}{%
{El Colombiano}%
}{%
{\protect \APACyear {2014}}%
}]{%
ElColombiano2014}
\APACinsertmetastar {%
ElColombiano2014}%
\begin{APACrefauthors}%
{El Colombiano}.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2014}{}{}.
\newblock
\APACrefbtitle {En 60\% sube consumo de pescado en Colombia durante Semana
  Santa.} {En 60\% sube consumo de pescado en colombia durante semana santa.}
\newblock
\APAChowpublished
  {\url{http://www.eltiempo.com/colombia/tolima/ARTICULO-WEB-NEW_NOTA_INTERIOR-12955288.html}}.
\newblock
\APACrefnote{Accessed: 07/08/2014}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Fernandez-Navarro%
, Garcia-Perez%
, Ramis%
, Boldo%
\BCBL {}\ \BBA {} Lopez-Abente%
}{%
Fernandez-Navarro%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2012}}%
}]{%
Fernandez-Navarro2012}
\APACinsertmetastar {%
Fernandez-Navarro2012}%
\begin{APACrefauthors}%
Fernandez-Navarro, P.%
, Garcia-Perez, J.%
, Ramis, R.%
, Boldo, E.%
\BCBL {}\ \BBA {} Lopez-Abente, G.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2012}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Proximity to mining industry and cancer mortality}
  {Proximity to mining industry and cancer mortality}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Science of The Total Environment}{}{0}{66 - 73}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Guiza%
\ \BBA {} Aristizabal%
}{%
Guiza%
\ \BBA {} Aristizabal%
}{%
{\protect \APACyear {2013}}%
}]{%
GUIZA2013}
\APACinsertmetastar {%
GUIZA2013}%
\begin{APACrefauthors}%
Guiza, L.%
\BCBT {}\ \BBA {} Aristizabal, J\BPBI D.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013}{04}{}.
\newblock
{\BBOQ}\APACrefatitle {Mercury and gold mining in Colombia: a failed state}
  {Mercury and gold mining in colombia: a failed state}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Universitas Scientiarum}{18}{}{33 - 49}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Hilson%
}{%
Hilson%
}{%
{\protect \APACyear {2002}}%
}]{%
Hilson2002}
\APACinsertmetastar {%
Hilson2002}%
\begin{APACrefauthors}%
Hilson, G.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2002}{}{}.
\newblock
{\BBOQ}\APACrefatitle {The environmental impact of small-scale gold mining in
  Ghana: identifying problems and possible solutions} {The environmental impact
  of small-scale gold mining in ghana: identifying problems and possible
  solutions}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Geographical Journal}{168}{1}{57-72}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
McMahon%
\ \BBA {} Remy%
}{%
McMahon%
\ \BBA {} Remy%
}{%
{\protect \APACyear {2001}}%
}]{%
McMahon2001}
\APACinsertmetastar {%
McMahon2001}%
\begin{APACrefauthors}%
McMahon, G.%
\BCBT {}\ \BBA {} Remy, F.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2001}{}{}.
\newblock
\APACrefbtitle {Large Mines and the Community : Socioeconomic and Environmental
  Effects in Latin America, Canada and Spain} {Large mines and the community :
  Socioeconomic and environmental effects in latin america, canada and spain}\
  \APACbVolEdTR{}{\BTR{}}.
\newblock
\APACaddressInstitution{}{World Bank and the International Development Research
  Centre}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Mergler%
\ \protect \BOthers {.}}{%
Mergler%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2007}}%
}]{%
Mergler2007}
\APACinsertmetastar {%
Mergler2007}%
\begin{APACrefauthors}%
Mergler, D.%
, Anderson, H\BPBI A.%
, Chan, L\BPBI H\BPBI M.%
, Mahaffey, K\BPBI R.%
, Murray, M.%
, Sakamoto, M.%
\BCBL {}\ \BBA {} Stern, A\BPBI H.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2007}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Methylmercury Exposure and Health Effects in Humans: A
  Worldwide Concern} {Methylmercury exposure and health effects in humans: A
  worldwide concern}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Ambio}{36}{1}{3-11}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
{Ministerio de Minas y Energia}%
}{%
{Ministerio de Minas y Energia}%
}{%
{\protect \APACyear {2011}}%
}]{%
Minas2011}
\APACinsertmetastar {%
Minas2011}%
\begin{APACrefauthors}%
{Ministerio de Minas y Energia}.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2011}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Chapter 2: Regalias} {Chapter 2: Regalias}.{\BBCQ}
\newblock
\BIn{} \APACrefbtitle {Memorias al Congreso de la Republica 2010 - 2011.}
  {Memorias al congreso de la republica 2010 - 2011.}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
{Ministerio de Minas y Energia}%
}{%
{Ministerio de Minas y Energia}%
}{%
{\protect \APACyear {2012}}%
}]{%
Ministerio2012}
\APACinsertmetastar {%
Ministerio2012}%
\begin{APACrefauthors}%
{Ministerio de Minas y Energia}.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2012}{}{}.
\newblock
\APACrefbtitle {Censo Minero Departamental Colombiano 2010-2011} {Censo minero
  departamental colombiano 2010-2011}\ \APACbVolEdTR{}{\BTR{}}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Mol%
, Ramlal%
, Lietar%
\BCBL {}\ \BBA {} Verloo%
}{%
Mol%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2001}}%
}]{%
Mol2001}
\APACinsertmetastar {%
Mol2001}%
\begin{APACrefauthors}%
Mol, J.%
, Ramlal, J.%
, Lietar, C.%
\BCBL {}\ \BBA {} Verloo, M.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2001}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Mercury Contamination in Freshwater, Estuarine, and
  Marine Fishes in Relation to Small-Scale Gold Mining in Suriname, South
  America} {Mercury contamination in freshwater, estuarine, and marine fishes
  in relation to small-scale gold mining in suriname, south america}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Environmental Research}{86}{2}{183 - 197}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Morales%
}{%
Morales%
}{%
{\protect \APACyear {2013}}%
}]{%
Morales2013}
\APACinsertmetastar {%
Morales2013}%
\begin{APACrefauthors}%
Morales, L.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013}{}{}.
\newblock
\APACrefbtitle {Mineria desde el espacio.} {Mineria desde el espacio.}
\newblock
\APAChowpublished
  {\url{http://lasillavacia.com/elblogueo/blog/mineria-desde-el-espacio-46235}}.
\newblock
\APACrefnote{Accessed: 05/01/2014}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Morel%
, Kraepiel%
\BCBL {}\ \BBA {} Amyot%
}{%
Morel%
\ \protect \BOthers {.}}{%
{\protect \APACyear {1998}}%
}]{%
Morel1998}
\APACinsertmetastar {%
Morel1998}%
\begin{APACrefauthors}%
Morel, F\BPBI M\BPBI M.%
, Kraepiel, A\BPBI M\BPBI L.%
\BCBL {}\ \BBA {} Amyot, M.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{1998}{}{}.
\newblock
{\BBOQ}\APACrefatitle {The Chemical Cycle and Bioaccumulation of Mercury} {The
  chemical cycle and bioaccumulation of mercury}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Annual Review of Ecology and
  Systematics}{29}{1}{543-566}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Olivero%
\ \BBA {} Johnson%
}{%
Olivero%
\ \BBA {} Johnson%
}{%
{\protect \APACyear {2002}}%
}]{%
Olivero2002}
\APACinsertmetastar {%
Olivero2002}%
\begin{APACrefauthors}%
Olivero, J.%
\BCBT {}\ \BBA {} Johnson, B.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYear{2002}.
\unskip\
\newblock
\APACrefbtitle {El lado gris de la mineria de oro: La contaminacion con
  mercurio en el norte de Colombia} {El lado gris de la mineria de oro: La
  contaminacion con mercurio en el norte de colombia}\ \APACtypeAddressSchool
  {\BUMTh}{}{}.
\unskip\
\newblock
\APACaddressSchool {}{Universidad de Cartagena}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Pfeiffer%
\ \BBA {} de Lacerda%
}{%
Pfeiffer%
\ \BBA {} de Lacerda%
}{%
{\protect \APACyear {1988}}%
}]{%
Pfeiffer1988}
\APACinsertmetastar {%
Pfeiffer1988}%
\begin{APACrefauthors}%
Pfeiffer, W\BPBI C.%
\BCBT {}\ \BBA {} de Lacerda, L\BPBI D.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{1988}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Mercury inputs into the Amazon Region, Brazil} {Mercury
  inputs into the amazon region, brazil}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Environmental Technology Letters}{9}{4}{325-330}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Pfeiffer%
, Lacerda%
, Salomons%
\BCBL {}\ \BBA {} Malm%
}{%
Pfeiffer%
\ \protect \BOthers {.}}{%
{\protect \APACyear {1993}}%
}]{%
Pfeiffer1993}
\APACinsertmetastar {%
Pfeiffer1993}%
\begin{APACrefauthors}%
Pfeiffer, W\BPBI C.%
, Lacerda, L\BPBI D.%
, Salomons, W.%
\BCBL {}\ \BBA {} Malm, O.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{1993}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Environmental fate of mercury from gold mining in the
  Brazilian Amazon} {Environmental fate of mercury from gold mining in the
  brazilian amazon}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Environmental Reviews}{1}{1}{26-37}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
{Procuradoria General de la Nacion}%
}{%
{Procuradoria General de la Nacion}%
}{%
{\protect \APACyear {2011}}%
}]{%
PGN2011}
\APACinsertmetastar {%
PGN2011}%
\begin{APACrefauthors}%
{Procuradoria General de la Nacion}.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2011}{}{}.
\newblock
\APACrefbtitle {MINERIA ILEGAL EN COLOMBIA: Informe preventivo} {Mineria ilegal
  en colombia: Informe preventivo}\ \APACbVolEdTR{}{\BTR{}}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Swenson%
, Carter%
, Domec%
\BCBL {}\ \BBA {} Delgado%
}{%
Swenson%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2011}}%
}]{%
Swenson2011}
\APACinsertmetastar {%
Swenson2011}%
\begin{APACrefauthors}%
Swenson, J\BPBI J.%
, Carter, C\BPBI E.%
, Domec, J\BHBI C.%
\BCBL {}\ \BBA {} Delgado, C\BPBI I.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2011}{04}{}.
\newblock
{\BBOQ}\APACrefatitle {Gold Mining in the Peruvian Amazon: Global Prices,
  Deforestation, and Mercury Imports} {Gold mining in the peruvian amazon:
  Global prices, deforestation, and mercury imports}.{\BBCQ}
\newblock
\APACjournalVolNumPages{PLoS ONE}{6}{4}{e18875}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
{The Times of India}%
}{%
{The Times of India}%
}{%
{\protect \APACyear {2012}}%
}]{%
TimesIndia2012}
\APACinsertmetastar {%
TimesIndia2012}%
\begin{APACrefauthors}%
{The Times of India}.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2012}{}{}.
\newblock
\APACrefbtitle {Tamil Nadu plans satellite imaging to check illegal mining.}
  {Tamil nadu plans satellite imaging to check illegal mining.}
\newblock
\APAChowpublished
  {\url{http://timesofindia.indiatimes.com/city/chennai/Tamil-Nadu-plans-satellite-imaging-to-check-illegal-mining/articleshow/16921715.cms}}.
\newblock
\APACrefnote{Accessed: 07/08/2014}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
UNEP%
}{%
UNEP%
}{%
{\protect \APACyear {2013}}%
}]{%
UNEP2013}
\APACinsertmetastar {%
UNEP2013}%
\begin{APACrefauthors}%
UNEP.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013}{}{}.
\newblock
\APACrefbtitle {Global Mercury Assessment 2013: Sources, emissions, releases,
  and environmental transport} {Global mercury assessment 2013: Sources,
  emissions, releases, and environmental transport}\ \APACbVolEdTR{}{\BTR{}}.
\newblock
\APACaddressInstitution{}{United Nations Environment Programme Chemicals
  Branch, Geneva, Switzerland}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
von~der Goltz%
\ \BBA {} Barnwal%
}{%
von~der Goltz%
\ \BBA {} Barnwal%
}{%
{\protect \APACyear {2014}}%
}]{%
Columbiaguys}
\APACinsertmetastar {%
Columbiaguys}%
\begin{APACrefauthors}%
von~der Goltz, J.%
\BCBT {}\ \BBA {} Barnwal, P.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2014}{February}{}.
\newblock
{\BBOQ}\APACrefatitle {Mines: The Local Welfare Effects of Mineral Mining in
  Developing Countries} {Mines: The local welfare effects of mineral mining in
  developing countries}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Discussion Paper No.: 1314-19 Columbia
  University}{}{}{}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
{World Bank}%
}{%
{World Bank}%
}{%
{\protect \APACyear {2009}}%
}]{%
World2009}
\APACinsertmetastar {%
World2009}%
\begin{APACrefauthors}%
{World Bank}.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2009}{}{}.
\newblock
\APACrefbtitle {Mining Together : Large-Scale Mining Meets Artisanal Mining, A
  Guide for Action} {Mining together : Large-scale mining meets artisanal
  mining, a guide for action}\ \APACbVolEdTR{}{\BTR{}}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
{World Health Organization}%
}{%
{World Health Organization}%
}{%
{\protect \APACyear {1990}}%
}]{%
World1990}
\APACinsertmetastar {%
World1990}%
\begin{APACrefauthors}%
{World Health Organization}.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{1990}{}{}.
\newblock
\APACrefbtitle {Environmental Health Criteria: Methylmercury.} {Environmental
  health criteria: Methylmercury.}
\PrintBackRefs{\CurrentBib}

\end{thebibliography}

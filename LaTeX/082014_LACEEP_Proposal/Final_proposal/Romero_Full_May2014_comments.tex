%\input{tcilatex}
\documentclass[12pt]{article}
\usepackage[cm]{fullpage}
\usepackage{nameref}
\usepackage{float}
\usepackage{pdflscape}
\usepackage[plainpages=false,pdfpagelabels]{hyperref}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{latexsym}
\usepackage{amsmath,amsthm}
\usepackage{amsfonts}
\usepackage{epsfig}
\usepackage{amssymb}
\usepackage{longtable}
\usepackage{tabularx,colortbl}
\usepackage{multirow}
\usepackage[table]{xcolor}
\usepackage{graphicx,epsfig,psfrag}
\usepackage{color}
\usepackage{eso-pic}
\usepackage{pdflscape}
\usepackage{dcolumn}
\usepackage{flafter}
\usepackage[mathscr]{euscript}
\usepackage{array, lscape}
\usepackage{pb-diagram}
\usepackage{setspace}
\usepackage{apacite}
\usepackage[all]{hypcap}
\setcounter{MaxMatrixCols}{10}
\newtheorem{acknowledgement}{Acknowledgement}
\newtheorem{algorithm}{Algorithm}
\newtheorem{axiom}{Axiom}
\newtheorem{case}{Case}
\newtheorem{claim}{Claim}
\newtheorem{conclusion}{Conclusion}
\newtheorem{condition}{Condition}
\newtheorem{conjecture}{Conjecture}
\newtheorem{corollary}{Corollary}
\newtheorem{criterion}{Criterion}
\newtheorem{definition}{Definition}
\newtheorem{example}{Example}
\newtheorem{exercise}{Exercise}
\newtheorem{lemma}{Lemma}
\newtheorem{notation}{Notation}
\newtheorem{problem}{Problem}
\newtheorem{proposition}{Proposition}
\newtheorem{remark}{Remark}
\newtheorem{solution}{Solution}
\newtheorem{summary}{Summary}
\usepackage{geometry}
%\date{}     % Deleting this command produces today's date.
\newcommand{\ip}[2]{(#1, #2)}
 \newcommand{\Lagr}{\mathcal{L}}
      % Defines \ip{arg1}{arg2} to mean
                             % (arg1, arg2).
%\newcommand{\ip}[2]{\langle #1 | #2\rangle}
                             % This is an alternative definition of
                             % \ip that is commented out.
\title{The hidden health costs and benefits of mining}
 
\author{Project leader: Mauricio Romero (Colombia)\thanks{University of California - San Diego, Tel (1) 858- 610 7632, Address: 9500 Gilman Dr, San Diego, CA 92093, U.S. \textit{e-mail contact:} \url{mtromero@ucsd.edu} }}
\begin{document}             % End of preamble and beginning of text.
                   % Produces the title.
\maketitle
\begin{abstract}
 
Mining presents a trade-off for developing countries because it is an important revenue source with environmental impacts. Given the lack of data on air and water quality around mines, health impacts on population are often unaccounted for. In the case of gold mining, the use and uncontrolled release of mercury influence health outcomes in surrounding populations. The net impact of mining on newborns' health is unclear as income from gold mining could potentially improve health, for example through better feeding and pre-natal care, but pollution could potentially offset these benefits. We use the spatial location of mines, rivers and population to construct an index of population exposure to pollution across time. Then we use two difference in difference approaches to estimate the treatment effect of mining on health. First, we compare the health of newborns before and after mining activities began in municipalities upstream and downstream from gold mines in Colombia.  Second, we use Holy Week, a mayor holiday in Colombia in which exposure to methyl-mercury increases through fish consumption. We compare newborns with a gestational period that overlaps with Holy Week, with those with a gestational period that does not overlap with this week across areas with and without mining. We intend to check how our results vary depending on the prevalence of illegal mines in an area and the scale of mining activities.
\end{abstract}
\section{Research Problem}
\onehalfspacing
In many countries, mining is an important revenue source for both the government and its inhabitants \cite{World2009}. However, there is mixed evidence on the overall welfare effect of mining. On one hand, some studies find negative effects on the environment, agricultural productivity and local communities \cite{McMahon2001,Aragon2013a}, while others find that mining can have positive effects on local communities \cite{Aragon2013}. The dust has yet to settle in this debate and in particular there is little evidence of the indirect effect of mining on human health.
 \\[0.5cm]
Colombia is experiencing a mining boom. The number of gold permits given from 2004-2008, doubled the number allocated from 1990-2004; and the Government expects gold extraction to increase over 70\% by 2020 \cite{Minas2011}. Mercury is widely used in gold mining in Colombia for amalgamation and as a results large quantities of this chemical are released into the environment \cite{Cordy2011}. Miners favor the use of mercury partly because its inexpensive \cite{GUIZA2013}. The effects of mercury exposure on health are not completely understood yet. The medical literature agrees that mercury is a doses-dependent toxin and that in high doses it can rapidly be fatal \cite{Davidson2004} but the evidence is inconclusive when it comes to low dosages. Previous studies lack power (have small sample size) or an experimental design that allows researchers to accurately identify the effects of different exposure levels on health.
 \\[0.5cm]
Our research agenda aims to understand the full effects of mining, including employment, government revenue, health impacts, productivity and environmental degradation. For this specific proposal we are going to estimate the net effect on the health of newborns due to mining. We expect that the additional income from gold mining improves the feeding and pre-natal care of mothers which should, in turn, improve the health of newborns. At the same time, mines are associated with pollution and environmental degradation, and in particular gold mining is associated with mercury release. Damage from in utero exposure to mercury could offset the benefits from additional income. We intend to estimate the reduce form estimation of the impact on newborns health from mining activities (i.e. the net effect on health from mining).
 \\[0.5cm]
We plan to use two different difference-difference strategies to calculate the effects of mining on health. The first strategy compares municipalities that lie upstream and downstream from a mine across time.  Given that municipalities neighbor each other and the only difference is whether they are up or downstream from the mine's location, we expect the health trends before the mines start operating to be similar. After the mine is placed we could observe how outcomes on the municipality downstream change across time, compared to how they change in the municipality upstream.  
 \\[0.5cm]
The second strategy aims to separate the income from the pollution effect. It compares newborns in-utero during holy week with those not who are not, across areas with and without mining. Identification relies on fish being the main source of (methyl)-mercury ingestion, and consumption of fish increasing by over 60\% \cite{ElColombiano2014} during this period.
 \\[0.5cm]
The main contribution to the empirical literature is providing an indirect estimate of the health costs of mining pollution. As developing countries lack complete and historical water quality measures, this research overcomes this barrier by looking at health outcomes. Finding an estimate of the health impacts associated with mining will allow for better environmental regulations to be designed. For example, the process of permit allocation can take into account the existing pollution of a municipality before authorizing more mines. This calculation is especially important because health impacts are ``hidden'', in the sense that they manifest in the long run, while the income effects are received in present time.
\subsection{Literature review}
The economics literature on mining has focus mainly on input reallocation, ignoring externalities until very recently; with evidence of both positive and negative spillovers. \citeA{Columbiaguys} found that communities near mines in Africa increase asset wealth, but there is an associated increase in anemia rates, due to heavy metals. A working paper by \citeA{Aragon2013a} looks at the impact of mining on agricultural productivity. They find that mining reduces agricultural productivity by almost 40\%, and that this is not due to changes in the use or price of agricultural inputs, but due to environmental degradation. On the other hand, the same authors \citeA{Aragon2013}, find evidence of a positive effect of mining on real income for non-mining workers in Peru.
 \\[0.5cm]
The academic literature outside of social sciences has long been exploring these questions. For example, \citeA{Swenson2011} find that increases in gold prices have boosted small scale gold mining in Peru which in turned increased deforestation in the amazon jungle and \citeA{Hilson2002} finds that small scale gold mining in Ghana has led to socio-economic growth accompanied by mercury pollution and land degradation. However, the exact impact of mining is uncertain since these studies often lack a credible contractual of what would have occurred in the absence of mining. Similarly, there are studies linking health to mining such as \citeA{Fernandez-Navarro2012, Attfield2008, Chakraborti2013} but these studies often focus on correlation and due to confounding effects can not imply causation. 
 \\[0.5cm]
Metallic mercury is used in mining for amalgamation, that is the process of separating gold particles from other minerals by trapping them with mercury \cite{Mol2001}. Approximately 10 Kg of mercury are needed in order to extract 1 Kg of gold, and in this process 9 Kg are released into the environment \cite{Pfeiffer1993,Veiga1997,GUIZA2013}. Of the mercury used, 55\% of goes in the atmosphere, while 45\% goes in water bodies \cite{Pfeiffer1988}. Aquatic micro-organisms transform the metallic mercury into methyl mercury \cite{Morel1998}, and in turn fish eat these organisms (for a more detail explanation see \citeA{Mol2001}). Thus, the use of mercury in gold mining increases the exposure to both metallic mercury and to methyl mercury. In the U.S. the EPA limits for metallic mercury in drinking water and for methyl mercury consumption are 0.002 $mg/L$ and 0.1 $\mu$g/kg body weight/day respectably and there is evidence that those limits are exceeded in Colombia near mining areas \cite{GUIZA2013,Olivero2002}. 
  \\[0.5cm]
Mercury, whose chemical symbol is Hg, is considered in the class of persistent bio-accumulative toxins. This means it moves from locations, but is never removed from the environment. Mercury occurs naturally in soils and rocks, and it’s released in volcanic eruptions and wildfires.  Anthropogenic sources include coal combustion, medical residues and gold mining. The last UNEP Global Mercury Assessment 2013 was commissioned for atmospheric mercury but included releases of mercury to water, because the main source of exposure to humans is through fish consumption. Another big change of the report, compared to the 2008 edition, is that mercury use by artisanal and small scale gold mining is included. In fact this sector is largest contributor with 727 tones, which is $35\%$ of anthropogenic atmospheric emissions. While releases to water are estimated at 800 tones, accounting for $63\%$ of anthropogenic human releases \cite{UNEP2013}. Large scale gold mining is classified in the unintended mercury emission, because it is assumed that mercury is captured and stockpiled or sold for other uses. It accounts for 97 tones ($5\%$ of emissions). Mercury damages heart, kidney and the central nervous system. Children and women are the most sensitive group to the effects of mercury.
   \\[0.5cm]
The medical literature consistently points out that exposure to methyl mercury in high doses can be fatal \cite{Davidson2004} but there is no consensus on how dosage/exposure variation affects health. Most of the studies looking at exposure to low doses of mercury lack a sufficiently high sample size or experimental design to be conclusive (see \citeA{Mergler2007,Davidson2004,Counter2004,Clarkson2003} for a review of the literature). One of the biggest problems these studies face is that blood mercury levels are endogenous. For example, fish consumption, which increases blood mercury levels, could be positively correlated with income, ability or social status, invalidating any causal inference draw from correlation. 
 \\[0.5cm]
In short, there is a gap in the literature. The literature has focus on mercury exposure caused by gold mining but this is not enough since we do not know exactly how different exposure levels affect health. This article will try to bridge that gap in the literature by looking directly at the effects of gold mining on the health of newborns. 
\section{Research Objectives}
General objective: Quantify the net impacts of gold mining on the health of surrounding population. 
 \\[0.25cm]
Specific objectives:
\begin{enumerate}
\item Quantify the effect of gold mining income on the APGAR score, gestation period, weight and length of newborns in surrounding municipalities.
\item Separately identify, if possible, income and pollution effects using fish consumption.
\item Estimate how the effects of vary by the timing of in utero exposure to methyl-mercury. 
\item Estimate how the net health effects of mining vary by different mine types. Specifically compare large scale vs. small scale mining and legal vs. illegal mining. 
\item Compare the health costs with the income and royalties derived from mining.
\end{enumerate}
\section{Research Methods}
The main objective of the paper is to quantify the impacts of gold mining in the health of the surrounding population. As mentioned before, there are no measurements of mercury in water; consequently we are going to approximate mercury use with gold extraction (by production and area of the mine). Mining location and area is obtained from the mining permit database collected by the NGO Tierra Minada\footnote{The full data set can be found in \url{https://sites.google.com/site/tierraminada/}} based on official government records. There are a total of 9,258 permits given from 1990 to 2012. Since we are only going to look at gold mines the sample size reduces to 1,878. We can combine this information with the municipality gold production released by the government agency SIMCO (\textit{Sistema de Informacion Minero Colombiano}), although this is available only since 2004.
 \\[0.5cm]
In Colombia the national government is the one that gives out mining permits. However, a mining permit only allows the exploration in an area but an environmental permit is needed in order to extract minerals. Thus, the actual production in a given region might not be correlated with the number of mining permits. We consider however the application for mining permits as a good proxy for mineral discovery which would lead either legally or illegally to the extraction of the mineral. 
 \\[0.5cm]
In order to assess health outcomes we can look at the Vital Statistics database from Colombia provided by the government statistics department (DANE) and database on health service visits (\textit{Registros Individuales de Presetacion de Servicios} [RIPS]) provided by the Ministry of Health . The Vital Statistics data base contains information for all newborns in the country from 1997-2011. Information includes birth weight and height, as well as on number of gestation weeks and the Appearance, Pulse, Grimace, Activity and Respiration-APGAR score. All this variables could be used as outcome variables. The APGAR score is of particular interest since there is evidence that the fetal brain is especially susceptible to damage from exposure to mercury \cite{Davidson2004}. Some summary statistics\footnote{This statistics only include information from 1997 to 2009, as the authors received information for 2010 and 2011 recently}  are presented below.
\begin{table}[H]
\caption{}
\begin{center}
\begin{tabular}{lrrrr}
  \hline
 & Mean & Std. Dev. & Min & Max \\ 
  \hline
Births per day & 1849.09 & 174.99 & 1328 & 2502 \\ 
  Male & 0.51 & 0.50 & 0 & 1 \\ 
  Weight (grams) & 3142.37 & 522.88 & 145 & 9900 \\ 
  Height (cm) & 49.50 & 2.83 & 9 & 95 \\ 
  Low APGAR ($\leq$ 6) & 0.06 & 0.24 & 0 & 1 \\ 
  Mother's Age & 25.13 & 6.55 & 9& 54 \\ 
   \hline
\end{tabular}
\end{center}
\end{table}
The database on health service visits, or RIPS for its acronym in Spanish, has rich information on service delivery and diagnosis for individuals in one of the two health regimes in Colombia since 2009. However, since most of the mines are located in areas where the database has low coverage and since we only have information after 2009 this information could be used later for robustness checks to assess if gold mining causes an increase in diagnosis related to mercury intoxication.
 \\[0.5cm]
Given that we are using a difference in difference approach, endogeneity of mining permits is a possible concern. In particular, one could imagine that municipalities that are obtaining mining permits are different from those that are not; however given that the permits are given at a national level we do not believe this is a problem and we intend to test the parallel trends assumption in our analysis. 
\subsection{Difference in Difference: Upstream vs. Downstream}
This identification strategy relies on the assumption that municipalities upstream and downstream from a mine are only affected differentially by the pollution produced by the mine and affected in the same way by everything else. In this setting downstream municipalities are ``treated'' with pollution, while upstream municipalities serve as controls. As river and municipalities' boundaries aren't perfectly classified as up and down stream, we have to estimate pollution exposure. Knowing the location of the mine we identify the closest river (usually inside the mine) and diffuse mercury pollution through the river according to the function $\sum_i 0.76^{riverdist_i}Pol_i$ \footnote{The concentration of mercury in fish is low 25km downstream for a mine in Tanzania (\citeA{OReily} so we calibrate with that. Robustness check will be performed because the actual concentration on mercury depends on the type of soil, water flow and other idiosyncratic variables for which we don't have information}.After having a level of mercury in each segment of the river we create a buffer of 10km around the river, and assign that value of pollution as seen in figure \ref{fig:1}. All this is done using Geographical Information Systems (GIS).
\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.40]{Data_mines.png} \quad
\includegraphics[scale=0.40]{Data_polu.png} 
\caption{Left panel shows an example of mines (in brown) located in municipalities (boundaries in black) and the rivers (in blue) in the area. The right panel shows the diffusion of pollution through rivers (low pollution in green and high pollution in red).}
\label{fig:1}
\end{center}
\end{figure}
Then we use a map with population location at 1km resolution\footnote{LandScan 2012 Global Population Database, Oak Ridge National Laboratory}. For every pixel we take the product of the population and pollution values to obtain exposure, which we then average over the pixels of each municipality.
\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.40]{Data_pop.png} \quad
\includegraphics[scale=0.40]{Data_exp.png} 
\caption{Light pink (low) to dark red (High) scale: population density by pixel (left) and resultant exposure (right)}
\end{center}
\end{figure}
We would estimate the following equation\\
$
Y_{imt}= \beta_1 Mined\_Area_{mt}+ \beta_2  Mined\_Area^2_{mt}+  \beta_3 Upstream\_Exposure_{mt}+X_{it}\beta_4  \gamma_m +   \gamma_{Year(t)} + \gamma_{Month(t)}  + \varepsilon_{imt},
$\\
where $Y_{imt}$ is an outcome  variable for birth $i$ in municipality $m$ at time $t$, $ Mined\_Area_{mt}$ is the mined area of municipality $m$ at time $t$, $Upstream\_Exposure_{mt}$ is the average exposure to pollution in the municipality calculated with the procedure described above, $X_it$ are characteristics of birth $i$ such as mother's age and education. $\gamma_m$ are municipality fixed effects, $\gamma_{Year(t)}$ year fixed effects, $\gamma_{Month(t)}$ month fixed effects and $ \varepsilon_{imt}$ error term. The coefficients of interest are $\beta_1$, $\beta_2$ and $\beta_3$ which capture the treatment effect of living in a mining area ($\beta_1$ and $\beta_2$) and the treatment effect of the pollution generated from mines upstream.
  \\[0.5cm]
There are two underlying assumption in this approach: first, that pollutants that are dropped in the river do not affect populations living upstream; second, that populations living on both sides of the mine had similar trends before mining began. A necessary condition for the first assumptions is that fishes that are eaten by humans are caught locally and do not travel upstream. The second assumption can be tested using our database but given that the communities are near each other and the only difference is whether they are up or downstream from the mine location, one would expect the trends to be similar. 
\subsection{Difference in Difference: Gestation and holy week}
Colombia is predominantly a catholic country, with over 75\% of the population declaring themselves as catholic\footnote{According to Latinbarometro 2011. The number is closer to 80\% according to the Pew Research: Religion \& Public Life Project 2012}. Holy week, and in particular Easter, is one of the major catholic holidays and in fact  Maundy Thursday and Good Friday are national holidays. Fish consumption increases during holy week by 60\%   \cite{ElColombiano2014}. We inted to use this increase in fish consumption to estimate the treatment effect of methyl-mercury exposure, through fish, on health. An advantage of using holy week is that its exact date varies from year to year\footnote{The last day of holy week (Easter) is set, since the 525 d.c., to be the Sunday after the first full moon after the march equinox; thus the holiday varies from year to year between March 22 and April 25.} allowing us to partially separate the effect of mercury exposure from seasonal effects\footnote{This is only partially true as holy week only varies within a 34 day period, making it impossible to separate exposure from seasonal effects completely.} . Additionally, by comparing individuals near mines with those far away, allow us to separate the effect of air mercury exposure from the effect of increased fish consumption. 
Specifically, we would estimate the following equation\\
$
Y_{imt}= \beta_1 Mined\_Area_{mt}+\beta_2 Holy\_week_{it}+\beta_3 Mined\_Area_{mt}\times Holy\_week_{it}+X_{it}\beta_4  \gamma_m +   \gamma_{Year(t)} + \gamma_{Week(t)}  + \varepsilon_{imt},
$\\
where $Y_{imt}$ is an outcome  variable for birth $i$ in municipality $m$ at time $t$, $Mined\_Area_{mt}$ is the proportion of area mined in the municipality $m$ at time $t$, $Holy\_week_{it}$ is a dummy variable that takes the value of one if the gestation of $i$ was exposed to with holy week. The mean lifetime of mercury in the body is 50 days \cite{World1990}. $\gamma_m$ are municipality fixed effects, $\gamma_{Year(t)}$ year fixed effects, $\gamma_{Week(t)}$ week fixed effects and $ \varepsilon_{imt}$ error term. The coefficient of interest is $\beta_3$ which captures the effect of increased fish consumption in mining areas during gestation. If fish near gold mining areas have bio-accumulated methyl-mercury $\beta_3$ will capture the effect of contaminated fish consumption on the outcome variable. 
  \\[0.5cm]
Using this identification strategy we can estimate heterogeneous effects by gestational week overlap with holy week. This will allows us to estimate how the effects of in utero exposure to methyl-mercury vary by gestational age. 
  \\[0.5cm]
We plan to use the research budget to sample fish markets in the different municipalities, and inquiry about type of fish, origin, and sales patterns. This will shed light on whether fish is consumed locally or is transported to municipalities upstream.
\subsection{Artisan and illegal mining}
Finally, it is important to note that small-scale artisan or illegal mining is widespread in Colombia. According to the government over 60\% of mines do not have a permit, over 70\% of mines have under six employees and there is high positive correlation between the size of a mine and the probability of having a mine permit\footnote{These numbers are probably a lower bound since the governments' survey couldn't access some remote areas, due to security issues, where illegal mining and artisan mining are believed to be widespread.} \cite{Ministerio2012}. Given that small-scale mining is an important source of mercury in Colombia \cite{Cordy2011} and an important source of revenue for poor and marginalized people \cite{World2009}, our analysis will try to take the scale and legality of mining into account. 
 \\[0.5cm]
In order to do include small-scale mining in our analysis we propose four strategies.  First, in our database we can observe if the mining title is given to a company or to a natural person. We will use the former as a proxy for large scale mining and the latter as a proxy for artisan mining, and check how our results vary by the mining type. Second, we have a mining census from 2010-2011 which we will use to do robustness checks where we will perform the analysis only in areas with a a low proportion of illegal mines and compare them to the results on areas with widespread illegal mining. Third, we will use data of illegal mine interventions from the police department as a proxy for illegal mining. Finally, we might use part of our budget to buy high frequency/high resolution satellite imagine data to construct a database for illegal mining in Colombia.  This has been proposed for some countries such as India \cite{TimesIndia2012} and Colombia \cite{Morales2013}, and has been done to some extent in other countries (e.g. Peru\cite{Swenson2011,Asner2013}).
 \\[0.5cm]
Additionally, there have been some efforts from the government to legalize illegal miners. These efforts usually include some grace period where illegal miners can obtain a mining permit without punishment. However, government reports show that most of these efforts have failed as the number of permit request is small, but moreover most of the permits are denied in the end \cite{PGN2011}. Nevertheless, we plan to study these periods carefully in order to avoid interpreting an increase in mining permits as an increase in mining activity where it is just the legalization of an activity that was already taking place. 
\section{Expected Results and Dissemination}
The research results will determine whether the health impacts from gold mining are bigger or smaller than the associated income and royalties benefits. The results will shed light on the consequences of exposure to low doses of mercury using an innovative identification strategy. These could be used to decide when to allocate more mining permits in already polluted areas. The results will be disseminated in seminars in UC San Diego, Stanford University, Universidad de los Andes, and other conferences where possible. A paper with the results from the analysis will be submitted for publication in peer reviewed journals.
\section{Institution and Personnel}
Mauricio Romero is a PhD student of economics at UC San Diego. He worked as consultant for Quantil SA and research assistant for three years before in the Center for Development Economic Studies (CEDE) (see Annex CV for more information). Santiago Saavedra is a PhD candidate in Economics at Stanford University. He worked as consultant for the National Planning Department and the Ministry of Environment in Colombia and research assistant two years before in CEDE (see Annex CV for more information). Both authors have BA in economics and mathematics from Universidad de los Andes.
\section{Timetable}
\begin{itemize}
\item Ongoing: analysis of Vital Statistics and gold production

\textbf{After LACEEP funding arrives:}

\item Intensify data analysis, perform robustness checks and collect data of fish markets species and origins (6 months)
\item Finish first draft of paper (2 Months)
\item Disseminate results (2 Months)
\item Final version of the paper (2 Months)
\end{itemize}
\section{Budget}
\subsection{Direct Research Costs}
\begin{enumerate}
\item Research expenses
\begin{itemize}
\item Data collection and processing $\$7,700$
\item Research Assistant $\$5,000$
\item Travel $\$1,500$
\end{itemize}
\item Dissemination
\begin{itemize}
\item Professional English style and grammar correction of final draft ($ \$200$) 
\end{itemize}
\item Support services
\begin{itemize}
\item Two computers with intel7 or equivalent processor $\$600$ ($\$300$ each, equivalent to $30\%$ of computer $\$1,000$ cost)
\end{itemize}
\item Overheads
\begin{itemize}
\item Not applicable
\end{itemize}
\item Recipient contribution
\begin{itemize}
\item We have funding and office space from our universities. We also get access to journal subscriptions and software as ArcGIS, STATA. 
\end{itemize}
\end{enumerate}
\subsection{Remuneration}
Not applicable
 
 \nocite{}
\bibliographystyle{apacite} 
 \bibliography{bibmin}
 
\end{document}


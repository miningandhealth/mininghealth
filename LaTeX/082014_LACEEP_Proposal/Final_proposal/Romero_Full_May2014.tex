%\input{tcilatex}
\documentclass[12pt]{article}
\usepackage[cm]{fullpage}
\usepackage{nameref}
\usepackage{float}
\usepackage{pdflscape}
\usepackage[plainpages=false,pdfpagelabels]{hyperref}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{latexsym}
\usepackage{amsmath,amsthm}
\usepackage{amsfonts}
\usepackage{epsfig}
\usepackage{amssymb}
\usepackage{longtable}
\usepackage{tabularx,colortbl}
\usepackage{multirow}
\usepackage[table]{xcolor}
\usepackage{graphicx,epsfig,psfrag}
\usepackage{color}
\usepackage{eso-pic}
\usepackage{pdflscape}
\usepackage{dcolumn}
\usepackage{flafter}
\usepackage[mathscr]{euscript}
\usepackage{array, lscape}
\usepackage{pb-diagram}
\usepackage{setspace}
\usepackage{apacite}
\usepackage[all]{hypcap}
\setcounter{MaxMatrixCols}{10}
\newtheorem{acknowledgement}{Acknowledgement}
\newtheorem{algorithm}{Algorithm}
\newtheorem{axiom}{Axiom}
\newtheorem{case}{Case}
\newtheorem{claim}{Claim}
\newtheorem{conclusion}{Conclusion}
\newtheorem{condition}{Condition}
\newtheorem{conjecture}{Conjecture}
\newtheorem{corollary}{Corollary}
\newtheorem{criterion}{Criterion}
\newtheorem{definition}{Definition}
\newtheorem{example}{Example}
\newtheorem{exercise}{Exercise}
\newtheorem{lemma}{Lemma}
\newtheorem{notation}{Notation}
\newtheorem{problem}{Problem}
\newtheorem{proposition}{Proposition}
\newtheorem{remark}{Remark}
\newtheorem{solution}{Solution}
\newtheorem{summary}{Summary}
\usepackage{geometry}
%\date{}     % Deleting this command produces today's date.
\newcommand{\ip}[2]{(#1, #2)}
 \newcommand{\Lagr}{\mathcal{L}}
      % Defines \ip{arg1}{arg2} to mean
                             % (arg1, arg2).
%\newcommand{\ip}[2]{\langle #1 | #2\rangle}
                             % This is an alternative definition of
                             % \ip that is commented out.
\title{The hidden health costs and benefits of mining}
 
\author{Project leader: Mauricio Romero (Colombia)\thanks{University of California - San Diego, Tel (1) 858- 610 7632, Address: 9500 Gilman Dr, San Diego, CA 92093, U.S. \textit{e-mail contact:} \url{mtromero@ucsd.edu}, \url{mauricioromerolondono@gmail.com} }}
\begin{document}             % End of preamble and beginning of text.
                   % Produces the title.
\maketitle
\begin{abstract}
 
Mining presents a trade-off for developing countries because it is an important revenue source with unaccounted environmental impacts. Given the lack of data on air and water quality around mines, health impacts on population are often unaccounted for. In the case of gold mining, the use and uncontrolled release of mercury influence health outcomes in surrounding populations. The net impact of mining on health is unclear as income from gold mining could potentially improve health, for example through better feeding and pre-natal care, but pollution could potentially offset these benefits. We use the spatial location of mines, rivers and population to construct an index of population exposure to pollution. Then we use a difference in difference approach, that compares the health of newborns  before and after mining activities began in municipalities upstream and downstream from gold mines in Colombia.   
\end{abstract}


\section{Research Problem}
\onehalfspacing
In many countries, mining is an important revenue source for both the government and its inhabitants \cite{World2009}. However, there is mixed evidence on the overall welfare effect of mining. On one hand, some studies find negative effects on the environment, agricultural productivity and local communities \cite{McMahon2001}, while others find that mining can have positive effects on local communities \cite{Aragon2013}. The dust has yet to settle in this debate and in particular there is little evidence of the indirect effect of mining on human health.
 \\[0.5cm]
Colombia is experiencing a mining boom. The number of gold permits given from 2004-2008, doubles the number allocated from 1990-2004; and the Government expects gold extraction to increase over 70\% by 2020. Mercury is widely used in gold mining in Colombia \cite{Cordy2011} for amalgamation and as a results large quantities of this chemical are released into the environment. Miners favor of the use of mercury partly because its inexpensive \cite{GUIZA2013}. The effects of mercury exposure on health are not completely understood yet. The medical literature agrees that mercury is a doses-dependent toxin and that in high doses it can rapidly be fatal \cite{Davidson2004} but the evidence is inconclusive when it comes to low dosages. Previous studies lack power (have small sample size) or an experimental design that allows researchers to accurately identify the effects of different exposure levels on health.
 \\[0.5cm]
This research estimates the health impacts of mercury in surrounding population and compare it to the income benefits of employment and royalties. Specifically, we are going to compare newborns before and after mining activities begin in municipalities upstream and downstream from a mine. Regarding health, we expect that the additional income from gold mining improves the feeding and pre-natal care of mothers which should, in turn, improve the health of newborns. At the same time, mines are associated with pollution and environmental degradation. Fetal brain is especially susceptible to damage from exposure to mercury. Given the lack of data on water quality around mines, health impacts on population are often unaccounted for. 
 \\[0.5cm]
We plan to use a difference-difference strategy comparing municipalities that lie upstream and downstream from a mine across time.  Given that municipalities neighbor each other and the only difference is whether they are up or downstream from the mine's location, one would expect the trends before the mines to be similar. After the mine is placed we could observe how outcomes on the municipality downstream change across time, compared to how they change in the municipality upstream.  
 \\[0.5cm]
The main contribution to the empirical literature on providing an indirect estimate of the health costs of mining pollution. As developing countries lack complete and historical water quality measures, this research overcomes this barrier by looking at health outcomes. Finding an estimate of the health impacts associated with mining will allow for better environmental regulations to be designed. For example, the process of permit allocation can take into account the existing pollution of a municipality before authorizing more mines. This calculation is especially important because health impacts are ``hidden'', in the sense that they manifest in the long run, while the income effects are received in present time.


\subsection{Literature review}

The economics literature on mining has focus mainly on input reallocation, ignoring externalities until very recently; with evidence of both positive and negative spillovers. A working paper by \citeA{Aragon2013a} looks at the impact of mining on agricultural productivity. They find that mining reduces agricultural productivity by almost 40\%, and that this is not due to changes in the use or price of agricultural inputs, but due to environmental degradation. On the other hand, the same authors \citeA{Aragon2013}, find evidence of a positive effect of mining on real income for non-mining workers in Peru.
 \\[0.5cm]
The academic literature outside of social sciences has long been exploring these questions. For example, \citeA{Swenson2011} find that increases in gold prices have boosted small scale gold mining in Peru which in turned increased deforestation in the amazon jungle and \citeA{Hilson2002} finds that small scale gold mining in Ghana has led to socio-economic growth accompanied by mercury pollution and land degradation. However, the exact impact of mining is uncertain since these studies often lack a contractual of what would have occurred in the absence of mining. Similarly, there are studies linking health to mining such as \citeA{Fernandez-Navarro2012, Attfield2008, Chakraborti2013} but these studies often focus on correlation and due to confounding effects can not imply causation. 
 \\[0.5cm]
Metallic mercury is used in mining for amalgamation \cite{Mol2001}. Approximately 10 Kg of mercury are needed in order to extract 1 Kg of gold, and in this process 9 Kg are released into the environment \cite{Pfeiffer1993,Veiga1997,GUIZA2013}. Of the mercury used, 55\% of goes in the atmosphere, while 45\% goes in water bodies \cite{Pfeiffer1988}. Aquatic micro-organisms transform the metallic mercury into methyl mercury \cite{Morel1998}, and in turn fish eat these organisms (for a more detail explanation see \citeA{Mol2001}). Thus, the use of mercury in gold mining increases the exposure to both metallic mercury and to methyl mercury. In the U.S. the EPA limits for metallic mercury in drinking water and for methyl mercury consumption are 0.002 $mg/L$ and 0.1 $\mu$g/kg body weight/day respectably and there is 
evidence that those limits are exceeded in Colombia near mining areas \cite{GUIZA2013,Olivero2002}. 
  \\[0.5cm]
Mercury, whose chemical symbol is Hg, is considered in the class of persistent bio-accumulative toxins. This means it moves from locations, but is never removed from the environment. Mercury occurs naturally in soils and rocks, and it’s released in volcanic eruptions and wildfires.  Anthropogenic sources include coal combustion, medical residues and gold mining. The last UNEP Global Mercury Assessment 2013 was commissioned for atmospheric mercury but included releases of mercury to water, because the main source of exposure to humans is through fish consumption. Specifically inorganic mercury is transformed into the toxic methylmercury by aquatic microorganisms. Another big change of the report, compared to the 2008 edition, is that mercury use by artisanal and small scale gold mining is included. In fact this sector is largest contributor with 727 tones, which is $35\%$ of anthropogenic atmospheric emissions. While releases to water are estimated at 800 tones, accounting for $63\%$ of anthropogenic human releases (UNEP, 2013). Large scale gold mining is classified in the unintended mercury emission, because it is assumed that mercury is captured and stockpiled or sold for other uses. It accounts for 97 tones ($5\%$ of emissions). Mercury damages heart, kidney and the central nervous system. Children and women are the most sensitive group to the effects of mercury.
  \\[0.5cm]
Most of the literature looking at the effects of mining on health focuses on exposure to harmful chemical rather than on health outcomes. In the particular case of gold mining most studies focus on the exposure to mercury. Studies from Peru \cite{Ashe2012}, Suriname \cite{Mol2001,Mol2004}, The Philippines \cite{CortesMaramba2006}, Chile \cite{Leiva2013}, South Africa \cite{LusilaoMakiese2013}, Indonesia \cite{Male2013}, Brazil \cite{Bastos2006}, Ecuador \cite{Webb2004} and Tanzania \cite{Ikingura1996,Straaten2000} all look at mercury exposure but not at health outcomes. To the best of our knowledge there is not a single study that explores the effect of gold mining on health outcomes directly. 
 \\[0.5cm]
The medical literature consistently points out that exposure to methyl mercury in high doses can be fatal \cite{Davidson2004} but there is no consensus on how dosage/exposure variation affects health. Most of the studies looking at exposure to low doses of mercury lack a sufficiently high sample size or experimental design to be conclusive (see \citeA{Mergler2007,Davidson2004,Counter2004,Clarkson2003} for a review of the literature). One of the biggest problems these studies face is that blood mercury levels are endogenous. For example, fish consumption, which increases blood mercury levels, could be positively correlated with income, ability or social status, invalidating any causal inference draw from correlation. 
 \\[0.5cm]
In short, there is a gap in the literature. The literature has focus on mercury exposure caused by gold mining but this is not enough since we do not know exactly how different exposure levels affect health. This article will try to bridge that gap in the literature by looking directly at the effects of mining on health. 

\section{Research Objectives}
General objective: Quantify the net impacts of gold mining on the health of surrounding population. 
 \\[0.25cm]
Specific objectives:
\begin{itemize}
\item Quantify the effect of gold mining on the APGAR score, gestation period, weight and length of newborns in surrounding municipalities
\item Calibrate the relation between mining and mercury in the blood of the newborn.
\item Monetize the impact on the health of the newborn.
\item Compare the health costs with the income and royalties derived from mining.
\end{itemize}

\section{Research Methods}

The main objective of the paper is to quantify the impacts of gold mining in the health of the surrounding population. As mentioned before, there are no measurements of mercury in water; consequently we are going to approximate mercury use with gold extraction (by production and area of the mine). Mining location and area is obtained from the mining permit database collected by the NGO Tierra Minada\footnote{The full data set can be found in \url{https://sites.google.com/site/tierraminada/}} based on official government records. There are a total of 9,258 permits given from 1990 to 2012. Since we are only going to look at gold mines the sample size reduces to 1,878. We can combine this information with the municipality gold production released by the government agency SIMCO (\textit{Sistema de Informacion Minero Colombiano}), although this is available only since 2004. Knowing the location of the mine we identify the closest river (usually inside the mine) and diffuse mercury pollution through it according to the function $\sum_i 0.97^{riverdist_i}Pol_i$. Finally we create a buffer of 10km around the river, and assign that value of pollution as seen in the images. All this is done using Geographical Information Systems (GIS).

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.40]{Data_mines.png} \quad
\includegraphics[scale=0.40]{Data_polu.png} 
\caption{Location of the gold mines (left) and diffusion of pollution through rivers (right)}
\end{center}
\end{figure}

Then we use map with population location at 1km resolution \footnote{LandScan 2012 Global Population Database, Oak Ridge National Laboratory}. The product of the population and pollution gives the exposure, which we average by each municipality.
\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.40]{Data_pop.png} \quad
\includegraphics[scale=0.40]{Data_exp.png} 
\caption{Location of population (left) and exposure (right)}
\end{center}
\end{figure}
In order to assess health outcomes we can look at the Vital Statistics database from Colombia provided by the government statistics department (DANE). There is information for all newborns in the country from 1997-2011. Information includes birth weight and height, as well as on number of gestation weeks and the Appearance, Pulse, Grimace, Activity and Respiration-APGAR score. All this variables could be used as outcome variables. The APGAR score is of particular interest since there is evidence that the fetal brain is especially susceptible to damage from exposure to mercury \cite{Davidson2004}. Some summary statistics\footnote{This statistics only include information from 1997 to 2009, as the authors received information for 2010 and 2011 recently}  are presented below.

\begin{table}[H]
\caption{}
\begin{center}
\begin{tabular}{lrrrr}
  \hline
 & Mean & Std. Dev. & Min & Max \\ 
  \hline
Births per day & 1849.09 & 174.99 & 1328.00 & 2502.00 \\ 
  Male & 0.51 & 0.50 & 0.00 & 1.00 \\ 
  Weight (grams) & 3142.37 & 522.88 & 145.00 & 9900.00 \\ 
  Height (cm) & 49.50 & 2.83 & 9.00 & 95.00 \\ 
  Low APGAR ($\leq$ 6) & 0.06 & 0.24 & 0.00 & 1.00 \\ 
  Mother's Age & 25.13 & 6.55 & 9.00 & 54.00 \\ 
   \hline
\end{tabular}
\end{center}
\end{table}


Specifically, we would estimate the following equation\\

$
LowAPGAR_{imt}= \beta_1 Mined\_Area_{mt}+ \beta_2 Mined\_Area^2_{mt}+  \beta_3 Upstream\_Area_{mt}+X_{it}\beta_4  \gamma_m +   \gamma_{Year(t)} + \gamma_{Month(t)}  + \varepsilon_{imt},
$\\


where $LowAPGAR_{imt}$ is an indicator variable for whether birth $i$,  has low APGAR in municipality $m$ at time $t$, $Mined\_Area_{mt}$ is the proportion of area mined in the municipality $m$ at time $t$, $Upstream\_Area_{mt}$ is the average exposure to pollution in the municipality calculated with the procedure described above, $X_it$ are characteristics of birth $i$ such as mother's age and education.$\gamma_m$ are municipality fixed effects, $\gamma_{Year(t)}$ year fixed effects, $\gamma_{Month(t)}$ month fixed effects and $ \varepsilon_{imt}$ error term. The coefficients of interest are $\beta_1$, $\beta_2$ and $\beta_3$. There is also evidence that mercury can impact gestational duration, birth weight and height \cite{Schoeman2009}, thus we would perform similar regressions with these variables instead of low APGAR. 

 
There are two underlying assumption in this approach: first, that pollutants that are dropped in the river do not affect populations living upstream; second, that populations living on both sides of the mine had similar trends before mining began. A necessary condition for the first assumptions is that fishes that are eaten by humans are caught locally and do not travel upstream. The second assumption can be tested using our database but given that the communities are near each other and the only difference is whether they are up or downstream from the mine location, one would expect the trends to be similar. 



Finally, it is important to note that small-scale artisan or illegal mining is widespread in Colombia. According to the government over 60\% of mines do not have a permit, and this number is probably a lower bound since the governments' survey couldn't access some remote area, due to security issues, where illegal mining is believed to be widespread \cite{Ministerio2012}. However, due to its nature we can't observe illegal mining in our database and thus in this project we will only focus on legal mining. Given that small-scale mining is an important source of mercury in Colombia \cite{Cordy2011} and an important source of revenue for poor and marginalized people \cite{World2009}, further research must follow in this area.

\section{Expected Results and Dissemination}

The research results will determine whether the health impacts from gold mining are bigger or smaller than the associated income and royalties benefits. The results will shed light on the consequences of exposure to low doses of mercury using an innovative identification strategy. These could be used to decide when to allocate more mining permits in already polluted areas. The results will be disseminated in seminars in UC San Diego, Stanford University, Universidad de los Andes, and other conferences where possible. There would be a paper submittable for publication in peer reviewed journals.

\section{Institution and Personnel}

Mauricio Romero is a PhD student of economics at UC San Diego. He worked as consultant for Quantil SA and research assistant for three years before. (See Annex CV for more information. Santiago Saavedra is a PhD candidate in Economics at Stanford University. He worked as consultant for the National Planning Department and the Ministry of Environment, and research assistant two years before. (See Annex CV for more information. Both authors have BA in economics and mathematics from Universidad de los Andes.

\section{Timetable}
\begin{itemize}
\item Ongoing: analysis of Vital Statistics and gold production
\item Once LACEEP funding arrives: Mercury testing on stratified random sample of municipalities
\item Calibration of mercury concentration with field data
\item Final analysis of research with all its components.
\end{itemize}
\section{Budget}
\subsection{Direct Research Costs}
\begin{enumerate}
\item Research expenses
\begin{itemize}
\item Mercury testing $\$9,000$
\item Research Assistant $\$4,200$
\item Travel $\$1,500$
\end{itemize}
\item Dissemination
\begin{itemize}
\item Not applicable
\end{itemize}
\item Support services
\begin{itemize}
\item Computer with intel7 or equivalent processor ($\$300$, equivalent to $30\%$ of computer$\$1,000$ cost)
\end{itemize}
\item Overheads
\begin{itemize}
\item Not applicable
\end{itemize}
\item Recipient contribution
\begin{itemize}
\item We have funding and office space from our universities. We also get access to journal subscriptions and software as ArcGIS, STATA. 
\end{itemize}
\end{enumerate}
\subsection{Remuneration}
Not applicable







 
 \nocite{}
\bibliographystyle{apacite} 
 \bibliography{bibmin}
 
\end{document}



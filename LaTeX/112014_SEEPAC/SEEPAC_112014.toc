\beamer@endinputifotherversion {3.33pt}
\select@language {english}
\beamer@sectionintoc {1}{Motivation and context}{2}{0}{1}
\beamer@sectionintoc {2}{Brief Literature Review}{7}{0}{2}
\beamer@sectionintoc {3}{Data}{9}{0}{3}
\beamer@sectionintoc {4}{Identification Strategy and Results}{15}{0}{4}
\contentsline {section}{References}

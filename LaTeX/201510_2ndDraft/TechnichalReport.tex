\documentclass[12pt]{article}
\usepackage{nameref}
\usepackage{float}
\usepackage[margin=2cm]{geometry}
%This is the package that puts tables at the end
%\usepackage{float}
%\usepackage[nolists]{endfloat}
\usepackage[plainpages=false,pdfpagelabels]{hyperref}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{booktabs}
\usepackage{graphicx}
\usepackage[all]{hypcap}
\usepackage{apacite}
\usepackage{multicol}
\usepackage{setspace}
\usepackage{pdflscape}

\usepackage{hyperref}
\linespread{1.3}





% JEL Classifications
\long\gdef\@JEL{}
\long\def\JEL#1{\long\gdef\@JEL{#1}}

% Keywords
\long\gdef\@Keywords{}
\long\def\Keywords#1{\long\gdef\@Keywords{#1}}


\newenvironment{tablenotes}[1][Note]{\begin{minipage}[t]{\linewidth} \footnotesize }{\end{minipage}}
\newenvironment{figurenotes}[1][Note]{\begin{minipage}[t]{\linewidth} \footnotesize}{\end{minipage}}


\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}

\begin{document} 

\title{
}
%\thanks{}
 \date{December 2, 2015}
\author{Mauricio Romero\thanks{Romero: University of California - San Diego, \textit{e-mail contact:} \url{mtromero@ucsd.edu}} and Santiago Saavedra \thanks{Stanford University, \textit{e-mail contact:} \url{santisap@stanford.edu}}}
            % End of preamble and beginning of text.
                   % Produces the title.


\noindent\textbf{Final Technical report}  \\
\textbf{Contract number:} IDEA-302 - 2014\\
\textbf{Early title of the project:} The hidden health costs and benefits of mining\\
\textbf{Current title of the paper:} The Effect of Gold Mining on the Health of Newborns\\
\textbf{Grantholder:} Mauricio Romero Londoño\\

\section{Introduction}


The surge in mineral prices over the last decade has propelled mining and economic growth in many low- and middle-income, mineral-rich countries \cite{McMahon2014}. However, this industrial activity in developing countries is usually accompanied by environmental degradation. The net impact of gold mining on human well-being is unclear, as more income could potentially expand opportunities for people (e.g., access to more and better food), while environmental degradation could negatively affect human health (e.g., respiratory infections and heavy metal poisoning). Additionally, environmental degradation and its impact on human health could have a detrimental effect on future income growth by reducing human capital. In particular, if newborns are negatively affected by environmental degradation this could lead to lower future productivity as in-utero insults, birth outcomes and early-life shocks have been shown to have long-term negative impacts on educational attainment.

In this article, we study the effect of gold mining on newborns' health in Colombia. Although gold mining provided U\$ 100 million in royalties in 2012, it often results in the release of heavy metals (mercury in particular) that affect the health of the surrounding population. To estimate the effect of gold mining on newborns, we use a difference-in-differences strategy that compares births in municipalities over time with different levels of mining activity. The main dependent variable is whether the newborn has a low APGAR (Appearance, Pulse, Grimace, Activity and Respiration) score, which captures the presence of possible brain damage and is a significant predictor of health, cognitive ability, and behavioral problems later in life \cite{Almond2005}.  


\section{Methods}
\subsection{Theoretical model}

\subsubsection{Mercury, mining and health}

Metallic mercury is used in mining for amalgamation, the process of separating gold particles from other minerals \cite{Mol2001}. For every kilogram of gold produced, on average 9 kilograms of mercury are released into the environment \cite{Pfeiffer1993,Veiga1997,GUIZA2013}. In the U.S,. the EPA limits for metallic mercury in drinking water and for methyl-mercury consumption are 0.002 $mg/L$ and 0.1 $\mu$g/kg body weight/day, respectively. There is evidence that those limits are exceeded in Colombia near mining areas \cite{GUIZA2013,Olivero2002}. 

Mercury is a bioaccumulative toxin; that is, it moves from one place to another but is never removed from the environment. According to the UNEP Global Mercury Assessment 2013, artisanal and small-scale gold mining is the largest contributor to atmospheric mercury emissions at 727 tons annually, or $35\%$ of anthropogenic emissions. In addition, discharges into water bodies from small-scale mining are estimated at 800 tons each year, representing $63\% $ of anthropogenic releases \cite{UNEP2013}.

Mercury is one of the top ten chemicals of major public health concern by the World Health Organization \cite{WHO2013}. There are five factors that determine the intensity of mercury health effects: the type of mercury; the dose; duration of exposure; route of exposure; and the age of the person \cite{WHO2013}. A reason for studying the health effects on newborns is that the age, type, duration and route of exposure are known. The difficulty remains the dose that depends on the mothers fish consumption and the mercury in the fish. 

The medical literature consistently points out that exposure to methyl-mercury in high doses can be fatal \cite{Davidson2004}, but there is no consensus on how variation in the dosage affects health. Mercury is harmful to the heart, kidneys, and central nervous system, with women and children being the most sensitive to its effects.  There is evidence that the fetal brain is especially susceptible to damage from exposure to mercury \cite{Davidson2004}. The EPA states that the primary health effect of methyl-mercury on fetuses, infants and children is impaired neurological development \cite{EPA2013a}.

  
\subsection{Empirical Model}

To calculate the net impact of gold mining on the health of newborns, we compare municipalities with different levels of gold mining activity across time, controlling for time and municipality fixed effects.  This identification strategy relies on the assumption that, after controlling for municipality fixed effects, time fixed effects, and regional time trends, the health of newborns is only affected differentially by the gold mines. The specific regression we estimate is:

\begin{equation}\label{eq:reg1}
Y_{imt}=\beta f( Gold_{mt})+X_{imt} \alpha+\gamma_m+ \gamma_{t}+\lambda_{r(m)}\times t+\varepsilon_{imt},
\end{equation}
where $Y_{imt}$ is the outcome variable for birth $i$ in municipality $m$ at time $t$. $Gold_{mt}$ is some measure of gold production or gold-related activity in municipality $m$ at time $t$. $X_{imt}$ is a set of controls for birth $i$ which includes age of the mother, whether the mother has more than primary education and whether the mother is single or not. $\gamma_m$ are municipality fixed effects, $\gamma_{t}$ are time fixed effects and $\lambda_{r(m)}\times t$ are regional dummy variables interacted with a time trend to allow for differential time trends for each region. The coefficients of interest are the betas which measure the effect of gold mining on birth outcomes. 


\subsubsection{Constructing river pollution exposure and proximity exposure}\label{sec:upstream_exp}

Ideally we could classify each municipality perfectly as  upstream or downstream from a mine. Unfortunately this is not feasible as rivers often enter and exit municipalities several times; therefore, we estimate the exposure to pollution of the population in each municipality through various steps. We use a data set that has every river in Colombia. Each river is a directed graph (i.e., a set of nodes and segments connecting those nodes. Each segment has a starting and an ending node that indicate how the river flows). First, we use the location of each mine to identify the closest river segment (usually running through the mine). For each day, and each river segment we calculate the total area from active mines closest to it. For each river segment, we then calculate the total area found upstream up to 25 KM. We then create a 5, 10 and 20-kilometers buffer along the river and calculate the population-weighted average of the area exposure in each municipality. 

Since the potential for working in the mine and exposure to air pollution probably depends on the distance from a person's home to the mine, we calculate a ``proximity exposure'' measure similar to the ``river exposure'' measure. We create  5, 10 and 20 kilometers buffers around each mine, multiply by the population raster, and calculate the average of this variable for an inhabitant in each municipality. With these two measurements (proximity and river exposure) we estimate the following equation:

\begin{multline}\label{eq:reg2}
Y_{imt}=\beta_1 ProximityExposure_{mt}+\beta_2 RiverExposure_{mt} \\ 
+X_i \alpha+\gamma_m+ \gamma_{t}+\lambda_{r(m)}\times t+\varepsilon_{imt},
\end{multline}


\subsubsection{Holy Week}\label{sec:HW}

Colombia is predominantly a Catholic country, with over 75\% of the population declaring themselves as Catholic. Holy Week is one of the major Catholic holidays, and Maundy Thursday and Good Friday are national holidays. Fish consumption increases during Holy Week by 60\% \cite{ElColombiano2014}, and, together with the increase in fish consumption during Lent, this represents an approximate 5\% increase in fish consumption during the gestation period of a baby. We use this exogenous increase in fish consumption to estimate the treatment effect of methyl-mercury exposure on health. An advantage of using Holy Week is that its exact date varies from year to year allowing us to partially separate the effect of mercury exposure from seasonal effects.  Specifically, we estimate the following equation:

\begin{multline}\label{eq:regHW3d}
Y_{imt}=\beta_1 Gold_{mt}+\beta_2 Holy\_week_{i}+\beta_3 Gold_{mt}\times Holy\_week_{i} \\
+X_i \alpha+\gamma_m+ \gamma_{t}+\lambda_{r(m)}\times t+\varepsilon_{imt},
\end{multline}

where $Holy\_week_{i}$ is a dummy variable that indicates whether individual $i$'s gestational period overlaps with Holy Week and $\gamma_m$ are municipality fixed effects. Note that $\gamma_{t}$ includes week fixed effects so we are not capturing seasonal effects. The coefficient of interest is $\beta_3$, which captures the differential effect that eating more fish during gestation has on newborns in areas with gold production or affected by pollution from gold mines, while $\beta_2$ measures the effect of the gestational period overlapping with Holy Week (e.g., through additional fish consumption or behavioral change for religious reason) and $\beta_1$ captures the net effect of gold mining on newborns' health.

\subsubsection{Other strategies}

We tried several empirical strategies that we received as feedback from LACEEP XIX Workshop and from our first submission of the paper. However, most of them proved to be impossible to implement for a variety of reasons. In this section we document them for the sake of completeness. 
\begin{itemize}
\item Sensitivity analysis to the threshold for Low APGAR
\begin{itemize}
\item The raw APGAR score only appear in the data after 2008. We decided to use the ``raw'' score as the outcome of interest, but decided to not do sensitivity to the cut-offs since the medical literature has a clear definition of Low APGAR
\end{itemize}
\item Pollution data
\begin{itemize}
\item Almost all (99.7\%) of the municipality-month mercury observations for 2006-2012 are missing in the National Health Institute- Information System of Monitoring of Water for Human Consumption-SIVICAP \url{http://www.ins.gov.co/sivicap/Paginas/sivicap.aspx}
\end{itemize}
\item Splitting the data into say small and large municipalities was discussed
\begin{itemize}
\item We did this for our ``Holy Week'' specification
\end{itemize}
\item Data on what hospital they were born
\begin{itemize}
\item We gathered as much hospital data as we could, as well as their coordinates. However, we were only able to match 4457 of hospitals to an exact location, 3613 to a general location (``nearest town''), and 35 were left unmatched. Moreover, most births do not have a hospital record on them. Only 12,635 births in our sample were matched to an exact location, and 3,595 to the ``nearest town''. Finally,  once we did all of our analysis using hospital data, as opposed to municipality data using a Voronoi diagram, we found that there was no variation in our data (more than 99\% of hospitals Voroni polygons did were not in the vicinity of a river or a mine).
\end{itemize}
\item Information on when a supermarket was introduced in the municipality
\begin{itemize}
\item We could not find this information
\end{itemize}
\item The investigation of the holy week as a number of problems. These problems are so serious that they in my opinion affect the overall evaluation of the project. This part should not be included in a Laceep working paper.
\begin{itemize}
\item We decided to keep it in the working paper after we revised our GIS data work. 
\end{itemize}
\item The issue of illegal mines is interesting and important, so this seems like a worthwhile task, at least as long as this can be done within the timeline of the project.
\begin{itemize}
\item We have come to view this as two separate, but related projects. We believe this first project is complete, and with LACEEP's  help with have launch a second project.
\end{itemize}
\item A graph or event study showing that the introduction of mining corresponds with the change in APGAR scores. 
\begin{itemize}
\item We have added this to the paper. As well as a sensitivity analysis of distance to the exposure source.
\end{itemize}
\item Given the argument that gold mines may be improving health through an income or wealth effect, the authors should show whether maternal characteristics are predicted by their main treatment variables. 
\begin{itemize}
\item We have added this to the paper.
\end{itemize}
\item Given the argument that gold mines may be improving health through an income or wealth effect, the authors should show whether maternal characteristics are predicted by their main treatment variables. 
\begin{itemize}
\item We have added this to the paper.
\end{itemize}
\end{itemize}




\section{Results}\label{sec:results}

\subsection{Net effect of gold mining on the health of newborns}

The results from estimating equation \ref{eq:reg1} are presented in Table \ref{tab:mainreg}. Regardless of how we measure mining (Panels A-D), the results show that the net effect of gold mining on the APGAR score of newborns in mining municipalities is positive\footnote{The sign of the coefficient is negative, which implies a reduction in the probability of the newborn having low APGAR}. In other words, mining has an effect on health outcomes, but the level of mining does not. Given the measurement issues explained before and the fact that there is no intensive margin, our preferred specification (Panel D) uses a dummy variable indicating whether there is any active mining title in the municipality or not. The results show that changing from no mining titles to at least one mining title reduces the probability that a newborn will have a low APGAR score by 0.76 percentage points (Column 1), from a base of 4.5\%, which implies a reduction of nearly 17\%. This could be due to increased household employment and income, or or to an increase public health expenditure funded by royalties. We lack the necessary data to separate these two explanations.  %This is 0.05 standard deviations

%% Main results

\begin{table}[H]
\centering
\caption{Net effect of mining on APGAR, for different measurements of mining}
\begin{scriptsize}

\begin{tabular}{l*{4}{c}}
\toprule          
                  &  \multicolumn{1}{c}{Low APGAR}  &  \multicolumn{1}{c}{Low Birth Weight}  &  \multicolumn{1}{c}{Z-score height} \\
                    &  \multicolumn{1}{c}{(1)}  &  \multicolumn{1}{c}{(2)}  &  \multicolumn{1}{c}{(3)} \\
\midrule
\multicolumn{4}{l}{\textbf{Panel A: Proportion of mined area}} \\
\input{IndividualMeasure_AreaMinadaProp_lbwzh} 
\midrule
\multicolumn{4}{l}{\textbf{Panel B: Production per area}} \\
\input{IndividualMeasure_ProduccionPerArea_lbwzh}
\midrule
\multicolumn{4}{l}{\textbf{Panel C: Accumulated Production per area}} \\
\input{IndividualMeasure_ProduccionAccPerArea_lbwzh}
\midrule
\multicolumn{4}{l}{\textbf{Panel D: Any mining title}} \\
\input{IndividualDummies_AreaMinadaProp_lbwzh}
\bottomrule
\end{tabular}
\end{scriptsize}
\label{tab:mainreg}
\begin{tablenotes}
\tiny All regressions include municipality fixed effects, time fixed effects, individual controls and regional trends. Time fixed effects are year and week fixed effects. Individual controls include mother's age, an indicator for single mother and an indicator for whether the mother has any post-primary education. The regional trends allow for a trend for each geographic region (Pacific, Andean and Caribbean). Standard errors, clustered by municipalities, are in parentheses. \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) 
\end{tablenotes}
\end{table}





\subsection{River exposure}

The results from estimating the effect on nearby and downstream municipalities (equation \ref{eq:reg2}) are presented in Table \ref{tab:regTrace}. They show that although the net effect of gold mining activity on the APGAR score of newborns in surrounding populations is positive, the effect on downstream populations is negative. In Column 1 we observe how the positive effect of being close to a mine (``proximity exposure'') decays with distance. For the ``river exposure'' it also decays but remains large and significative up to 20 kilometers. 

%The direction of the effect is the same, independent of whether we use a continuous or a discrete measure of gold mining activity; however, the coefficients are only statistically significant when we use a dummy variable indicating whether there is (proximity and river) exposure. In the case of proximity and river exposure we believe the dummy variable results are more relevant, as they are less sensitive to our choice of buffer size and decay function parameters. 

%%%	ADJUST NUMBERS!!!

In line with our previous results, the estimates show that changing from no mining exposure to some exposure reduces the probability that a newborn will have a low APGAR score by 1.04 percentage points (from a base of 4.7\%, which represents a reduction of 22\%). However, using spatial variation of communities with respect to the mine we find that communities living downstream from the mine are negatively affected by mining activities. In short, mining increases the likelihood of a newborn having a low APGAR score by 0.57 percentage points (from a base of 4.7\%, which represents an increase of 12\%) in communities downstream from the mine. 

\begin{table}[H]
\centering
\caption{Effect of proximity and river pollution exposure }
\resizebox{\textwidth}{!}{
\begin{tabular}{l*{3}{c}}
\toprule          
                  \multicolumn{4}{c}{Dependent variable: }\\
                    &  \multicolumn{1}{c}{Low APGAR}  &  \multicolumn{1}{c}{Low Birth Weight}  &  \multicolumn{1}{c}{Stunning} \\

\multicolumn{4}{l}{\textbf{Panel A: Exposure up to 20 KM}} \\
\input{ProxRiverArea_RHS_Discretas_20KM}
\bottomrule
\multicolumn{4}{l}{\textbf{Panel B: Exposure by distance}} \\
\input{ProxRiverArea_RHS_Discretas}
\bottomrule
\end{tabular}
}
\label{tab:regTrace}
\begin{tablenotes}
All regressions include municipality fixed effects, time fixed effects, individual controls and regional trends. Time fixed effects are year and week fixed effects. Individual controls include mother's age, an indicator for single mother and an indicator for whether the mother has any post-primary education. The regional trends allow for a trend for each geographic region (Pacific, Andean and Caribbean). Standard errors, clustered by municipalities, are in parentheses.  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) 
\end{tablenotes}
\end{table}



\subsection{Holy Week}

The results of the estimation including Holy Week are presented in Table \ref{tab:hwreg}. The first column shows the results defining gestational overlap with Holy Week as equal to one if the newborn gestational period overlap with Holy Week at any point. The second column identifies what trimester, if any, of the gestational period overlaps with Holy Week. These regressions include week effects to isolate the effect of Holy Week from any seasonal effects. 

The coefficient of the interaction between river proximity and Holy Week is positive but not statistically significant when defining HolyWeek over the entire gestation. However, when in column 2 we analyze by trimester of overlap with Holy Week the coefficient for the first trimesters is large and significative. This is aligned with the fact that the brain is more susceptible to damage during the first two trimesters and brain development is affected by mercury exposure \cite{Black2013}. Note also that the magnitude is large compared to the coefficient of river exposure. Signaling that more than polluted water the problem is through polluted fish consumption.





\section{Conclusions and future work}\label{sec:conc}
Mining is an important revenue source for governments and households in some developing countries. However, there is mixed evidence on the overall welfare effect of mining. We contribute to this literature by estimating the net impact of gold mining on the health of newborns in Colombia. We find that mothers living within 20 km of a mine have a probability of 0.62 percentage points lower of having low APGAR score births (from a basis of 4.5\%). However, we find that mothers living in municipalities located downstream from a mine experience an increase of 0.56 percentage points in the probability of having low APGAR score births. We provide some suggestive evidence that contaminated-fish consumption is the main mechanisms behind our results by using an exogenous increase in fish consumption caused by the overlap of gestation and Holy Week, a religious holiday that increases fish consumption in the country.

\section{Policy implications and recommendations}
 We find heterogeneous effects depending on where mothers are located with respect to a mine. Mothers living within 20 km of a mine are positively affected by mining activities, experience a reduction of 0.62 percentage points in the probability of having a child with a low APGAR score (from a basis of 4.5\%). However, we find a negative effect on mothers living in municipalities located downstream from a mine, whose probability of having a child with a low APGAR score at birth increases by 0.56 percentage points. We show how the income effects decrease with distance from the mine, but are persistent up to 20 KM. The Colombian government set the goal of abolishing mercury from mining by 2018. Our results provide support of the benefits of this policy, but in the mean time the government should create compensation mechanism for downstream municipalities.


\section{Impact on my professional career}

This project was the first major undertaking by both of us (Santiago and myself) during grad school, and as such it was full of disappointments and lessons learned the hard way. However, having the support of LACEEP, both intellectually and financially, helped us overcome the obstacles we found, taught us some lessons on project management, and allowed us to spend more time doing the research that we love.  

The grant was valuable to our professional career in two dimensions. First it allowed us to explore two ideas we had to expand the econometric analysis. With the funding we could do the pilot survey of fish markets and learn that what we were assuming from our desk was not true in the field. We then use the funding to explore new techniques of machine learning and satellite data to identify machine learning. Second the LACEEP community of researchers provided us good feedback, intellectual stimulation and friendship.

\bibliographystyle{apacite}
\bibliography{bibmin}
 
 


\appendix
\section{Extra Tables}\label{sec:extfig}
\begin{table}[H]
\caption{Increased mercury exposure in-utero due to Holy Week}
\label{tab:hwreg}
\resizebox{\textwidth}{!}{
\begin{tabular}{l*{3}{c}}
\toprule
      \multicolumn{4}{c}{Dependent variable: Low APGAR}\\
      
     & \multicolumn{1}{c}{(1)} & \multicolumn{1}{c}{(2)} & \multicolumn{1}{c}{(3)} \\
\midrule

\input{IndividualHWDummies_Combo_trim}
\bottomrule
\end{tabular}
}
\begin{tablenotes}
All regressions include municipality fixed effects, time fixed effects, individual controls and state trends. Time fixed effects are year and week of the year fixed effects. Individual controls include mother's age, an indicator for single mother and an indicator for whether the mother has any post-primary education. Standard errors, clustered by municipalities, are in parentheses. \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) 
\end{tablenotes}
\end{table}





\end{document}


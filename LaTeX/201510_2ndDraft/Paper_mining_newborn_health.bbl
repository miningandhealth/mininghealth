\begin{thebibliography}{}

\bibitem [\protect \citeauthoryear {%
Almond%
}{%
Almond%
}{%
{\protect \APACyear {2006}}%
}]{%
Almond2006}
\APACinsertmetastar {%
Almond2006}%
\begin{APACrefauthors}%
Almond, D.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2006}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Is the 1918 Influenza Pandemic Over? Long-Term Effects
  of In Utero Influenza Exposure in the Post-1940 U.S. Population} {Is the 1918
  influenza pandemic over? long-term effects of in utero influenza exposure in
  the post-1940 u.s. population}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Journal of Political Economy}{114}{4}{pp. 672-712}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Almond%
, Chay%
\BCBL {}\ \BBA {} Lee%
}{%
Almond%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2005}}%
}]{%
Almond2005}
\APACinsertmetastar {%
Almond2005}%
\begin{APACrefauthors}%
Almond, D.%
, Chay, K\BPBI Y.%
\BCBL {}\ \BBA {} Lee, D\BPBI S.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2005}{}{}.
\newblock
{\BBOQ}\APACrefatitle {The Costs of Low Birth Weight} {The costs of low birth
  weight}.{\BBCQ}
\newblock
\APACjournalVolNumPages{The Quarterly Journal of Economics}{120}{3}{1031-1083}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Almond%
\ \BBA {} Currie%
}{%
Almond%
\ \BBA {} Currie%
}{%
{\protect \APACyear {2011}}%
}]{%
Almond2011}
\APACinsertmetastar {%
Almond2011}%
\begin{APACrefauthors}%
Almond, D.%
\BCBT {}\ \BBA {} Currie, J.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2011}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Killing Me Softly: The Fetal Origins Hypothesis}
  {Killing me softly: The fetal origins hypothesis}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Journal of Economic Perspectives}{25}{3}{153-72}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Almond%
\ \BBA {} Mazumder%
}{%
Almond%
\ \BBA {} Mazumder%
}{%
{\protect \APACyear {2011}}%
}]{%
Almond2011a}
\APACinsertmetastar {%
Almond2011a}%
\begin{APACrefauthors}%
Almond, D.%
\BCBT {}\ \BBA {} Mazumder, B.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2011}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Health Capital and the Prenatal Environment: The Effect
  of Ramadan Observance during Pregnancy} {Health capital and the prenatal
  environment: The effect of ramadan observance during pregnancy}.{\BBCQ}
\newblock
\APACjournalVolNumPages{American Economic Journal: Applied
  Economics}{3}{4}{56-85}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Arag\'{o}n%
\ \BBA {} Rud%
}{%
Arag\'{o}n%
\ \BBA {} Rud%
}{%
{\protect \APACyear {2013}}%
}]{%
Aragon2013}
\APACinsertmetastar {%
Aragon2013}%
\begin{APACrefauthors}%
Arag\'{o}n, F\BPBI M.%
\BCBT {}\ \BBA {} Rud, J\BPBI P.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Natural Resources and Local Communities: Evidence from a
  Peruvian Gold Mine} {Natural resources and local communities: Evidence from a
  peruvian gold mine}.{\BBCQ}
\newblock
\APACjournalVolNumPages{American Economic Journal: Economic
  Policy}{5}{2}{1-25}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Arag\'{o}n%
\ \BBA {} Rud%
}{%
Arag\'{o}n%
\ \BBA {} Rud%
}{%
{\protect \APACyear {2015}}%
}]{%
Aragon2015}
\APACinsertmetastar {%
Aragon2015}%
\begin{APACrefauthors}%
Arag\'{o}n, F\BPBI M.%
\BCBT {}\ \BBA {} Rud, J\BPBI P.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2015}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Polluting Industries and Agricultural Productivity:
  Evidence from Mining in Ghana} {Polluting industries and agricultural
  productivity: Evidence from mining in ghana}.{\BBCQ}
\newblock
\APACjournalVolNumPages{The Economic Journal}{}{}{1-31}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Benshaul-Tolonen%
}{%
Benshaul-Tolonen%
}{%
{\protect \APACyear {2018}}%
}]{%
Benshaul-Tolonen2018}
\APACinsertmetastar {%
Benshaul-Tolonen2018}%
\begin{APACrefauthors}%
Benshaul-Tolonen, A.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2018}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Local industrial shocks and infant mortality} {Local
  industrial shocks and infant mortality}.{\BBCQ}
\newblock
\APACjournalVolNumPages{The Economic Journal}{129}{620}{1561--1592}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Bharadwaj%
, L{\o}ken%
\BCBL {}\ \BBA {} Neilson%
}{%
Bharadwaj%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2013}}%
}]{%
Bharadwaj2013}
\APACinsertmetastar {%
Bharadwaj2013}%
\begin{APACrefauthors}%
Bharadwaj, P.%
, L{\o}ken, K\BPBI V.%
\BCBL {}\ \BBA {} Neilson, C.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Early Life Health Interventions and Academic
  Achievement} {Early life health interventions and academic
  achievement}.{\BBCQ}
\newblock
\APACjournalVolNumPages{American Economic Review}{103}{5}{1862-91}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Black%
, B\"{u}tikofer%
, Devereux%
\BCBL {}\ \BBA {} Salvanes%
}{%
Black%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2013}}%
}]{%
Black2013}
\APACinsertmetastar {%
Black2013}%
\begin{APACrefauthors}%
Black, S\BPBI E.%
, B\"{u}tikofer, A.%
, Devereux, P\BPBI J.%
\BCBL {}\ \BBA {} Salvanes, K\BPBI G.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013}{April}{}.
\newblock
\APACrefbtitle {This Is Only a Test? Long-Run Impacts of Prenatal Exposure to
  Radioactive Fallout} {This is only a test? long-run impacts of prenatal
  exposure to radioactive fallout}\ \APACbVolEdTR {}{Working Paper\ \BNUM\
  18987}.
\newblock
\APACaddressInstitution{}{National Bureau of Economic Research}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Black%
, Devereux%
\BCBL {}\ \BBA {} Salvanes%
}{%
Black%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2007}}%
}]{%
Black2007}
\APACinsertmetastar {%
Black2007}%
\begin{APACrefauthors}%
Black, S\BPBI E.%
, Devereux, P\BPBI J.%
\BCBL {}\ \BBA {} Salvanes, K\BPBI G.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2007}{}{}.
\newblock
{\BBOQ}\APACrefatitle {From the Cradle to the Labor Market? The Effect of Birth
  Weight on Adult Outcomes} {From the cradle to the labor market? the effect of
  birth weight on adult outcomes}.{\BBCQ}
\newblock
\APACjournalVolNumPages{The Quarterly Journal of Economics}{122}{1}{409-439}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Bose-O'Reilly%
\ \protect \BOthers {.}}{%
Bose-O'Reilly%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2010}}%
}]{%
Bose-OReilly2010}
\APACinsertmetastar {%
Bose-OReilly2010}%
\begin{APACrefauthors}%
Bose-O'Reilly, S.%
, Drasch, G.%
, Beinhoff, C.%
, Tesha, A.%
, Drasch, K.%
, Roider, G.%
\BDBL {}Siebert, U.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2010}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Health assessment of artisanal gold miners in Tanzania}
  {Health assessment of artisanal gold miners in tanzania}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Science of The Total Environment}{408}{4}{796 - 805}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
{Center for Disease Control}%
}{%
{Center for Disease Control}%
}{%
{\protect \APACyear {2009}}%
}]{%
cdc2009}
\APACinsertmetastar {%
cdc2009}%
\begin{APACrefauthors}%
{Center for Disease Control}.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2009}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Mercury fact sheet} {Mercury fact sheet}.{\BBCQ}
\newblock

\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Clarkson%
, Magos%
\BCBL {}\ \BBA {} Myers%
}{%
Clarkson%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2003}}%
}]{%
Clarkson2003}
\APACinsertmetastar {%
Clarkson2003}%
\begin{APACrefauthors}%
Clarkson, T\BPBI W.%
, Magos, L.%
\BCBL {}\ \BBA {} Myers, G\BPBI J.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2003}{}{}.
\newblock
{\BBOQ}\APACrefatitle {The Toxicology of Mercury - Current Exposures and
  Clinical Manifestations} {The toxicology of mercury - current exposures and
  clinical manifestations}.{\BBCQ}
\newblock
\APACjournalVolNumPages{New England Journal of Medicine}{349}{18}{1731-1737}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
{Congreso de la Republica}%
}{%
{Congreso de la Republica}%
}{%
{\protect \APACyear {2013}}%
}]{%
CongresoRepublica2013}
\APACinsertmetastar {%
CongresoRepublica2013}%
\begin{APACrefauthors}%
{Congreso de la Republica}.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013}{}{}.
\newblock
\APACrefbtitle {{Ley No. 1658}.} {{Ley No. 1658}.}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Cordy%
\ \protect \BOthers {.}}{%
Cordy%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2011}}%
}]{%
Cordy2011}
\APACinsertmetastar {%
Cordy2011}%
\begin{APACrefauthors}%
Cordy, P.%
, Veiga, M\BPBI M.%
, Salih, I.%
, Al-Saadi, S.%
, Console, S.%
, Garcia, O.%
\BDBL {}Roeser, M.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2011}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Mercury contamination from artisanal gold mining in
  Antioquia, Colombia: The world's highest per capita mercury pollution}
  {Mercury contamination from artisanal gold mining in antioquia, colombia: The
  world's highest per capita mercury pollution}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Science of The Total Environment}{410 - 411}{0}{154 -
  160}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Counter%
\ \BBA {} Buchanan%
}{%
Counter%
\ \BBA {} Buchanan%
}{%
{\protect \APACyear {2004}}%
}]{%
Counter2004}
\APACinsertmetastar {%
Counter2004}%
\begin{APACrefauthors}%
Counter, S.%
\BCBT {}\ \BBA {} Buchanan, L\BPBI H.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2004}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Mercury exposure in children: a review} {Mercury
  exposure in children: a review}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Toxicology and Applied Pharmacology}{198}{2}{209 -
  230}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
da Veiga%
}{%
da Veiga%
}{%
{\protect \APACyear {1997}}%
}]{%
Veiga1997}
\APACinsertmetastar {%
Veiga1997}%
\begin{APACrefauthors}%
da Veiga, M.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{1997}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Introducing New Technologies for Abatement of Global
  Mercury Pollution in Latin America} {Introducing new technologies for
  abatement of global mercury pollution in latin america}.{\BBCQ}
\newblock

\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Davidson%
, Myers%
\BCBL {}\ \BBA {} Weiss%
}{%
Davidson%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2004}}%
}]{%
Davidson2004}
\APACinsertmetastar {%
Davidson2004}%
\begin{APACrefauthors}%
Davidson, P\BPBI W.%
, Myers, G\BPBI J.%
\BCBL {}\ \BBA {} Weiss, B.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2004}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Mercury Exposure and Child Development Outcomes}
  {Mercury exposure and child development outcomes}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Pediatrics}{113}{Supplement 3}{1023-1029}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Ehrenstein%
}{%
Ehrenstein%
}{%
{\protect \APACyear {2009}}%
}]{%
Ehrenstein2009}
\APACinsertmetastar {%
Ehrenstein2009}%
\begin{APACrefauthors}%
Ehrenstein, V.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2009}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Association of {APGAR} scores with death and neurologic
  disability} {Association of {APGAR} scores with death and neurologic
  disability}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Clinical epidemiology}{1}{}{45-53}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
{El Colombiano}%
}{%
{El Colombiano}%
}{%
{\protect \APACyear {2014}}%
}]{%
ElColombiano2014}
\APACinsertmetastar {%
ElColombiano2014}%
\begin{APACrefauthors}%
{El Colombiano}.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2014}{}{}.
\newblock
\APACrefbtitle {En 60\% sube consumo de pescado en Colombia durante Semana
  Santa.} {En 60\% sube consumo de pescado en colombia durante semana santa.}
\newblock
\APAChowpublished
  {\url{http://www.eltiempo.com/colombia/tolima/ARTICULO-WEB-NEW_NOTA_INTERIOR-12955288.html}}.
\newblock
\APACrefnote{Accessed: 07/08/2014}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
{Environmental Protection Agency }%
}{%
{Environmental Protection Agency }%
}{%
{\protect \APACyear {2013}}%
}]{%
EPA2013a}
\APACinsertmetastar {%
EPA2013a}%
\begin{APACrefauthors}%
{Environmental Protection Agency }.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013}{}{}.
\newblock
\APACrefbtitle {Mercury health effects http://www.epa.gov/mercury/effects.htm}
  {Mercury health effects http://www.epa.gov/mercury/effects.htm}\
  \APACbVolEdTR{}{\BTR{}}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Greenstone%
\ \BBA {} Jack%
}{%
Greenstone%
\ \BBA {} Jack%
}{%
{\protect \APACyear {2015}}%
}]{%
Greenstone2015}
\APACinsertmetastar {%
Greenstone2015}%
\begin{APACrefauthors}%
Greenstone, M.%
\BCBT {}\ \BBA {} Jack, B\BPBI K.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2015}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Envirodevonomics: A research agenda for an emerging
  field} {Envirodevonomics: A research agenda for an emerging field}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Journal of Economic Literature}{53}{1}{5--42}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Guiza%
\ \BBA {} Aristizabal%
}{%
Guiza%
\ \BBA {} Aristizabal%
}{%
{\protect \APACyear {2013}}%
}]{%
GUIZA2013}
\APACinsertmetastar {%
GUIZA2013}%
\begin{APACrefauthors}%
Guiza, L.%
\BCBT {}\ \BBA {} Aristizabal, J\BPBI D.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013}{04}{}.
\newblock
{\BBOQ}\APACrefatitle {Mercury and gold mining in Colombia: a failed state}
  {Mercury and gold mining in colombia: a failed state}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Universitas Scientiarum}{18}{}{33 - 49}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Iba\~nez%
\ \BBA {} Laverde%
}{%
Iba\~nez%
\ \BBA {} Laverde%
}{%
{\protect \APACyear {2014}}%
}]{%
Ibanez2014}
\APACinsertmetastar {%
Ibanez2014}%
\begin{APACrefauthors}%
Iba\~nez, A.%
\BCBT {}\ \BBA {} Laverde, M.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2014}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Los municipios mineros en Colombia: caracteristicas e
  impactos sobre el desarrollo} {Los municipios mineros en colombia:
  caracteristicas e impactos sobre el desarrollo}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Insumos para el desarrollo del Plan Nacional de
  Ordenamiento Minero}{}{}{}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Jayachandran%
}{%
Jayachandran%
}{%
{\protect \APACyear {2009}}%
}]{%
Jayachandran2009}
\APACinsertmetastar {%
Jayachandran2009}%
\begin{APACrefauthors}%
Jayachandran, S.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2009}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Air quality and early-life mortality evidence from
  Indonesia’s wildfires} {Air quality and early-life mortality evidence from
  indonesia’s wildfires}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Journal of Human resources}{44}{4}{916--954}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
McMahon%
\ \BBA {} Remy%
}{%
McMahon%
\ \BBA {} Remy%
}{%
{\protect \APACyear {2001}}%
}]{%
McMahon2001}
\APACinsertmetastar {%
McMahon2001}%
\begin{APACrefauthors}%
McMahon, G.%
\BCBT {}\ \BBA {} Remy, F.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2001}{}{}.
\newblock
\APACrefbtitle {Large Mines and the Community : Socioeconomic and Environmental
  Effects in Latin America, Canada and Spain} {Large mines and the community :
  Socioeconomic and environmental effects in latin america, canada and spain}\
  \APACbVolEdTR{}{\BTR{}}.
\newblock
\APACaddressInstitution{}{World Bank and the International Development Research
  Centre}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Mergler%
\ \protect \BOthers {.}}{%
Mergler%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2007}}%
}]{%
Mergler2007}
\APACinsertmetastar {%
Mergler2007}%
\begin{APACrefauthors}%
Mergler, D.%
, Anderson, H\BPBI A.%
, Chan, L\BPBI H\BPBI M.%
, Mahaffey, K\BPBI R.%
, Murray, M.%
, Sakamoto, M.%
\BCBL {}\ \BBA {} Stern, A\BPBI H.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2007}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Methylmercury Exposure and Health Effects in Humans: A
  Worldwide Concern} {Methylmercury exposure and health effects in humans: A
  worldwide concern}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Ambio}{36}{1}{3-11}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Mol%
, Ramlal%
, Lietar%
\BCBL {}\ \BBA {} Verloo%
}{%
Mol%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2001}}%
}]{%
Mol2001}
\APACinsertmetastar {%
Mol2001}%
\begin{APACrefauthors}%
Mol, J.%
, Ramlal, J.%
, Lietar, C.%
\BCBL {}\ \BBA {} Verloo, M.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2001}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Mercury Contamination in Freshwater, Estuarine, and
  Marine Fishes in Relation to Small-Scale Gold Mining in Suriname, South
  America} {Mercury contamination in freshwater, estuarine, and marine fishes
  in relation to small-scale gold mining in suriname, south america}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Environmental Research}{86}{2}{183 - 197}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Montgomery%
}{%
Montgomery%
}{%
{\protect \APACyear {2000}}%
}]{%
Montgomery2000}
\APACinsertmetastar {%
Montgomery2000}%
\begin{APACrefauthors}%
Montgomery, K.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2000}{}{}.
\newblock
{\BBOQ}\APACrefatitle {{APGAR} scores: Examining the long term significance}
  {{APGAR} scores: Examining the long term significance}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Journal of Perinatal Education}{9}{3}{5-9}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Morel%
, Kraepiel%
\BCBL {}\ \BBA {} Amyot%
}{%
Morel%
\ \protect \BOthers {.}}{%
{\protect \APACyear {1998}}%
}]{%
Morel1998}
\APACinsertmetastar {%
Morel1998}%
\begin{APACrefauthors}%
Morel, F\BPBI M\BPBI M.%
, Kraepiel, A\BPBI M\BPBI L.%
\BCBL {}\ \BBA {} Amyot, M.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{1998}{}{}.
\newblock
{\BBOQ}\APACrefatitle {The Chemical Cycle and Bioaccumulation of Mercury} {The
  chemical cycle and bioaccumulation of mercury}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Annual Review of Ecology and
  Systematics}{29}{1}{543-566}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Moster%
, Lie%
, Irgens%
, Bjerkedal%
\BCBL {}\ \BBA {} Markestad%
}{%
Moster%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2001}}%
}]{%
Moster2001}
\APACinsertmetastar {%
Moster2001}%
\begin{APACrefauthors}%
Moster, D.%
, Lie, R\BPBI T.%
, Irgens, L\BPBI M.%
, Bjerkedal, T.%
\BCBL {}\ \BBA {} Markestad, T.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2001}{}{}.
\newblock
{\BBOQ}\APACrefatitle {The association of {APGAR} score with subsequent death
  and cerebral palsy: a population-based study in term infants} {The
  association of {APGAR} score with subsequent death and cerebral palsy: a
  population-based study in term infants}.{\BBCQ}
\newblock
\APACjournalVolNumPages{The Journal of pediatrics}{138}{6}{798--803}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Olivero%
\ \BBA {} Johnson%
}{%
Olivero%
\ \BBA {} Johnson%
}{%
{\protect \APACyear {2002}}%
}]{%
Olivero2002}
\APACinsertmetastar {%
Olivero2002}%
\begin{APACrefauthors}%
Olivero, J.%
\BCBT {}\ \BBA {} Johnson, B.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYear{2002}.
\unskip\
\newblock
\APACrefbtitle {El lado gris de la mineria de oro: La contaminacion con
  mercurio en el norte de Colombia} {El lado gris de la mineria de oro: La
  contaminacion con mercurio en el norte de colombia}\ \APACtypeAddressSchool
  {\BUMTh}{}{}.
\unskip\
\newblock
\APACaddressSchool {}{Universidad de Cartagena}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Pfeiffer%
\ \BBA {} de Lacerda%
}{%
Pfeiffer%
\ \BBA {} de Lacerda%
}{%
{\protect \APACyear {1988}}%
}]{%
Pfeiffer1988}
\APACinsertmetastar {%
Pfeiffer1988}%
\begin{APACrefauthors}%
Pfeiffer, W\BPBI C.%
\BCBT {}\ \BBA {} de Lacerda, L\BPBI D.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{1988}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Mercury inputs into the {Amazon Region, Brazil}}
  {Mercury inputs into the {Amazon Region, Brazil}}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Environmental Technology Letters}{9}{4}{325-330}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Pfeiffer%
, Lacerda%
, Salomons%
\BCBL {}\ \BBA {} Malm%
}{%
Pfeiffer%
\ \protect \BOthers {.}}{%
{\protect \APACyear {1993}}%
}]{%
Pfeiffer1993}
\APACinsertmetastar {%
Pfeiffer1993}%
\begin{APACrefauthors}%
Pfeiffer, W\BPBI C.%
, Lacerda, L\BPBI D.%
, Salomons, W.%
\BCBL {}\ \BBA {} Malm, O.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{1993}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Environmental fate of mercury from gold mining in the
  {Brazilian Amazon}} {Environmental fate of mercury from gold mining in the
  {Brazilian Amazon}}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Environmental Reviews}{1}{1}{26-37}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Saavedra%
\ \BBA {} Romero%
}{%
Saavedra%
\ \BBA {} Romero%
}{%
{\protect \APACyear {2019}}%
}]{%
SaavedraRomero2019}
\APACinsertmetastar {%
SaavedraRomero2019}%
\begin{APACrefauthors}%
Saavedra, S.%
\BCBT {}\ \BBA {} Romero, M.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2019}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Local incentives and national tax evasion: The response
  of illegal mining to a tax reform in Colombia} {Local incentives and national
  tax evasion: The response of illegal mining to a tax reform in
  colombia}.{\BBCQ}
\newblock

\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Schofield%
}{%
Schofield%
}{%
{\protect \APACyear {2015}}%
}]{%
Schofield2015}
\APACinsertmetastar {%
Schofield2015}%
\begin{APACrefauthors}%
Schofield, H.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYear{2015}.
\unskip\
\newblock
\APACrefbtitle {The Economic Costs of Low Caloric Intake: Evidence from India}
  {The economic costs of low caloric intake: Evidence from india}\
  \APACtypeAddressSchool {\BUPhD}{}{}.
\unskip\
\newblock
\APACaddressSchool {}{Working Paper}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
{UNEP-INTERPOL}%
}{%
{UNEP-INTERPOL}%
}{%
{\protect \APACyear {2016}}%
}]{%
UNEPINTERPOL(2016)}
\APACinsertmetastar {%
UNEPINTERPOL(2016)}%
\begin{APACrefauthors}%
{UNEP-INTERPOL}.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2016}{}{}.
\newblock
\APACrefbtitle {The Rise of Environmental Crime} {The rise of environmental
  crime}\ \APACbVolEdTR{}{\BTR{}}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
UNODC%
}{%
UNODC%
}{%
{\protect \APACyear {2016}}%
}]{%
unodc2016}
\APACinsertmetastar {%
unodc2016}%
\begin{APACrefauthors}%
UNODC, U.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2016}{}{}.
\newblock
\APACrefbtitle {Colombia: Explotaci\'on de oro de aluvi\'on. Evidencias a
  partir de percepci\'on remota.} {Colombia: Explotaci\'on de oro de aluvi\'on.
  evidencias a partir de percepci\'on remota.}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
von~der Goltz%
\ \BBA {} Barnwal%
}{%
von~der Goltz%
\ \BBA {} Barnwal%
}{%
{\protect \APACyear {2019}}%
}]{%
vonBarnwal2019}
\APACinsertmetastar {%
vonBarnwal2019}%
\begin{APACrefauthors}%
von~der Goltz, J.%
\BCBT {}\ \BBA {} Barnwal, P.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2019}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Mines: The local wealth and health effects of mineral
  mining in developing countries} {Mines: The local wealth and health effects
  of mineral mining in developing countries}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Journal of Development Economics}{139}{}{1--16}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
{World Bank}%
}{%
{World Bank}%
}{%
{\protect \APACyear {2009}}%
}]{%
WorldBank2009}
\APACinsertmetastar {%
WorldBank2009}%
\begin{APACrefauthors}%
{World Bank}.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2009}{}{}.
\newblock
\APACrefbtitle {Mining Together: Large-Scale Mining Meets Artisanal Mining, A
  Guide for Action} {Mining together: Large-scale mining meets artisanal
  mining, a guide for action}\ \APACbVolEdTR{}{\BTR{}}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
{World Health Organization}%
}{%
{World Health Organization}%
}{%
{\protect \APACyear {2006}}%
}]{%
WHO2006}
\APACinsertmetastar {%
WHO2006}%
\begin{APACrefauthors}%
{World Health Organization}.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2006}{}{}.
\newblock
\APACrefbtitle {Child Growth Standards: Methods and Development} {Child growth
  standards: Methods and development}\ \APACbVolEdTR{}{\BTR{}}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
{World Health Organization}%
}{%
{World Health Organization}%
}{%
{\protect \APACyear {2013}}%
}]{%
WHO2013}
\APACinsertmetastar {%
WHO2013}%
\begin{APACrefauthors}%
{World Health Organization}.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013}{}{}.
\newblock
\APACrefbtitle {Mercury and health, fact sheet Number 361} {Mercury and health,
  fact sheet number 361}\ \APACbVolEdTR{}{\BTR{}}.
\PrintBackRefs{\CurrentBib}

\end{thebibliography}

\begin{thebibliography}{}

\bibitem [\protect \citeauthoryear {%
Aragon%
\ \BBA {} Rud%
}{%
Aragon%
\ \BBA {} Rud%
}{%
{\protect \APACyear {2013}}%
{\protect \APACexlab {{\protect \BCnt {1}}}}}]{%
Aragon2013a}
\APACinsertmetastar {%
Aragon2013a}%
\begin{APACrefauthors}%
Aragon, F\BPBI M.%
\BCBT {}\ \BBA {} Rud, J\BPBI P.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013{\protect \BCnt {1}}}{February}{}.
\newblock
\APACrefbtitle {Modern Industries, Pollution and Agricultural Productivity:
  Evidence from Mining in Ghana.} {Modern industries, pollution and
  agricultural productivity: Evidence from mining in ghana.}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Aragon%
\ \BBA {} Rud%
}{%
Aragon%
\ \BBA {} Rud%
}{%
{\protect \APACyear {2013}}%
{\protect \APACexlab {{\protect \BCnt {2}}}}}]{%
Aragon2013}
\APACinsertmetastar {%
Aragon2013}%
\begin{APACrefauthors}%
Aragon, F\BPBI M.%
\BCBT {}\ \BBA {} Rud, J\BPBI P.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013{\protect \BCnt {2}}}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Natural Resources and Local Communities: Evidence from a
  Peruvian Gold Mine} {Natural resources and local communities: Evidence from a
  peruvian gold mine}.{\BBCQ}
\newblock
\APACjournalVolNumPages{American Economic Journal: Economic
  Policy}{5}{2}{1-25}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Davidson%
, Myers%
\BCBL {}\ \BBA {} Weiss%
}{%
Davidson%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2004}}%
}]{%
Davidson2004}
\APACinsertmetastar {%
Davidson2004}%
\begin{APACrefauthors}%
Davidson, P\BPBI W.%
, Myers, G\BPBI J.%
\BCBL {}\ \BBA {} Weiss, B.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2004}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Mercury Exposure and Child Development Outcomes}
  {Mercury exposure and child development outcomes}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Pediatrics}{113}{Supplement 3}{1023-1029}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Ehrenstein%
\ \protect \BOthers {.}}{%
Ehrenstein%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2009}}%
}]{%
Ehrenstein2009}
\APACinsertmetastar {%
Ehrenstein2009}%
\begin{APACrefauthors}%
Ehrenstein, V.%
, Pedersen, L.%
, Grijota, M.%
, Nielsen, G.%
, Rothman, K.%
\BCBL {}\ \BBA {} Sorensen, H.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2009}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Association of Apgar score at five minutes with
  long-term neurologic disability and cognitive function in a prevalence study
  of Danish conscripts} {Association of apgar score at five minutes with
  long-term neurologic disability and cognitive function in a prevalence study
  of danish conscripts}.{\BBCQ}
\newblock
\APACjournalVolNumPages{BMC Pregnancy and Childbirth}{9}{1}{14}.
\newblock
\begin{APACrefURL} \url{http://www.biomedcentral.com/1471-2393/9/14}
  \end{APACrefURL}
\newblock
\begin{APACrefDOI} \doi{10.1186/1471-2393-9-14} \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
{El Colombiano}%
}{%
{El Colombiano}%
}{%
{\protect \APACyear {2014}}%
}]{%
ElColombiano2014}
\APACinsertmetastar {%
ElColombiano2014}%
\begin{APACrefauthors}%
{El Colombiano}.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2014}{}{}.
\newblock
\APACrefbtitle {En 60\% sube consumo de pescado en Colombia durante Semana
  Santa.} {En 60\% sube consumo de pescado en colombia durante semana santa.}
\newblock
\APAChowpublished
  {\url{http://www.eltiempo.com/colombia/tolima/ARTICULO-WEB-NEW_NOTA_INTERIOR-12955288.html}}.
\newblock
\APACrefnote{Accessed: 07/08/2014}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Guiza%
\ \BBA {} Aristizabal%
}{%
Guiza%
\ \BBA {} Aristizabal%
}{%
{\protect \APACyear {2013}}%
}]{%
GUIZA2013}
\APACinsertmetastar {%
GUIZA2013}%
\begin{APACrefauthors}%
Guiza, L.%
\BCBT {}\ \BBA {} Aristizabal, J\BPBI D.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013}{04}{}.
\newblock
{\BBOQ}\APACrefatitle {Mercury and gold mining in Colombia: a failed state}
  {Mercury and gold mining in colombia: a failed state}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Universitas Scientiarum}{18}{}{33 - 49}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
{Ministerio de Minas y Energia}%
}{%
{Ministerio de Minas y Energia}%
}{%
{\protect \APACyear {2012}}%
}]{%
Ministerio2012}
\APACinsertmetastar {%
Ministerio2012}%
\begin{APACrefauthors}%
{Ministerio de Minas y Energia}.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2012}{}{}.
\newblock
\APACrefbtitle {Censo Minero Departamental Colombiano 2010-2011} {Censo minero
  departamental colombiano 2010-2011}\ \APACbVolEdTR{}{\BTR{}}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Olivero%
\ \BBA {} Johnson%
}{%
Olivero%
\ \BBA {} Johnson%
}{%
{\protect \APACyear {2002}}%
}]{%
Olivero2002}
\APACinsertmetastar {%
Olivero2002}%
\begin{APACrefauthors}%
Olivero, J.%
\BCBT {}\ \BBA {} Johnson, B.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYear{2002}.
\unskip\
\newblock
\APACrefbtitle {El lado gris de la mineria de oro: La contaminacion con
  mercurio en el norte de Colombia} {El lado gris de la mineria de oro: La
  contaminacion con mercurio en el norte de colombia}\ \APACtypeAddressSchool
  {\BUMTh}{}{}.
\unskip\
\newblock
\APACaddressSchool {}{Universidad de Cartagena}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
UNEP%
}{%
UNEP%
}{%
{\protect \APACyear {2013}}%
}]{%
UNEP2013}
\APACinsertmetastar {%
UNEP2013}%
\begin{APACrefauthors}%
UNEP.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013}{}{}.
\newblock
\APACrefbtitle {Global Mercury Assessment 2013: Sources, emissions, releases,
  and environmental transport} {Global mercury assessment 2013: Sources,
  emissions, releases, and environmental transport}\ \APACbVolEdTR{}{\BTR{}}.
\newblock
\APACaddressInstitution{}{United Nations Environment Programme Chemicals
  Branch, Geneva, Switzerland}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
von~der Goltz%
\ \BBA {} Barnwal%
}{%
von~der Goltz%
\ \BBA {} Barnwal%
}{%
{\protect \APACyear {2014}}%
}]{%
Goltz2014}
\APACinsertmetastar {%
Goltz2014}%
\begin{APACrefauthors}%
von~der Goltz, J.%
\BCBT {}\ \BBA {} Barnwal, P.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2014}{February}{}.
\newblock
{\BBOQ}\APACrefatitle {Mines: The Local Welfare Effects of Mineral Mining in
  Developing Countries} {Mines: The local welfare effects of mineral mining in
  developing countries}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Discussion Paper No.: 1314-19 Columbia
  University}{}{}{}.
\PrintBackRefs{\CurrentBib}

\end{thebibliography}

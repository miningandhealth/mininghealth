\documentclass[pdf]{beamer}
\usepackage{booktabs}
\usepackage{tabularx,colortbl}
\usepackage{multirow}
\usepackage[export]{adjustbox}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{comment}
\usepackage{wrapfig}
\usepackage{dsfont}
\usepackage{bbm}
\usepackage[flushleft]{threeparttable}      % Option: Table notes
\usepackage{floatpag}
\usepackage{wrapfig}
\usepackage{relsize}
\usepackage{setspace}  
\usepackage{apacite}  


\usepackage{nameref}
\usepackage{float}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{latexsym}
\usepackage{amsmath,amsthm}
\usepackage{amsfonts}

\usepackage{apacite}

                   
\mode<presentation>{}
\usetheme{Warsaw}


\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}
 \newcommand{\specialcell}[2][c]{%
    \begin{tabular}[#1]{@{}c@{}}#2\end{tabular}
}
\expandafter\def\expandafter\insertshorttitle\expandafter{%
  \insertshorttitle\hfill%
  \insertframenumber\,/\,\inserttotalframenumber}
\title[Mining and health]{The hidden health costs and benefits of mining}

\author[Romero-Saavedra]{Mauricio Romero (UC - San Diego)  \\ Santiago Saavedra   }

\normalsize

\date{September 25th 2014}




\begin{document}

\frame{\titlepage}
 
\AtBeginSection[]
{
   \begin{frame}
       \frametitle{Table of Contents}
       \tableofcontents[currentsection]
   \end{frame}
}

\section{Motivation and context}


\begin{frame}[plain]

\frametitle{Motivation}

\begin{figure}[H]
\begin{center}
\includegraphics[width=0.45\textwidth]{Prop_2000.pdf} \quad
\includegraphics[width=0.45\textwidth]{Prop_2013.pdf} 
\caption{ \scriptsize{Proportion of area mined in each municipality. Each panel presents the proportion of the area in a municipality that is part of a mining title. Source: Tierra Minada. Calculations: Authors.}}
\label{fig:evol_mineria}
\end{center}
\end{figure}

\end{frame}



\begin{frame}
\frametitle{Recent mining boom}
\begin{itemize}
\item Evidence that EPA mercury limits are exceeded in Colombia near mining areas(Guiza \& Aristizabal, 2013; Olivero \&
Johnson, 2002).
\item But not historical measurements of mercury in all municipalities.
\item Fetal brain is especially susceptible to damage from exposure to mercury (Davidson, 2004).
\item Long term effects of low APGAR (Appearance, Pulse, Grimace, Activity, Respiration) scores on cognitive skills: 2 IQ points (Ehrenstein, 2009).
\end{itemize}
\end{frame}





\begin{frame}
\frametitle{Research Question}
What is the impact of gold mining in the health of newborns?
\begin{itemize}
\item Diff-in-diff strategy to calculate \textbf{reduced} form estimates [ (+)income \& (-) pollution ] effects of mining.
\item \textbf{Treatment intensity} depending on scale of mining activity
\item To isolate ``pollution effect'' we plan to:
\begin{enumerate}
\item Use spatial variation: municipalities upstream/downstream from the mines.
\item Use Holy Week as a source of exogenous variation in exposure to mercury (through fish consumption).
\end{enumerate}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Preview of results}

\begin{itemize}
\item There is a quadratic relationship between proportion of area of the municipality mined and health of newborns
\begin{itemize}
\item For most of the municipalities the net effect is positive.
\item The gains from income are partially reversed by pollution.
\end{itemize}
\item Having gold mining activity reduces the probability of low APGAR by 1 percentage point, from a basis of 5\%
\item  River pollution increases the probability in 0.5 percentage points for downstream municipalities.
\item Babies with gestation period during Holy Week in mining municipalities  have a 0.6 percentage point probability of having low APGAR.

\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Contribution}

\begin{itemize}
\item Estimate non-linear impacts of mining.
\item Exploit location of mines and rivers for downstream effects.
\item Triple Diff-Diff-Diff strategy for effects of extra fish consumption during Holy Week.
\end{itemize}
\end{frame}


\section{Brief Literature Review}

\begin{frame}
\frametitle{Gold context}
\begin{itemize}
\item Mercury is used in gold mining for amalgamation. That is, binding gold particles together apart from silt.
\item The remaining silt is washed away (water pollution)
\item The amalgam is heated to evaporate mercury (air pollution)
\item Artisanal and small scale gold mining $\rightarrow$ largest contributor to atmospheric mercury emissions (727 tones or $35\%$ of emissions) (UNEP, 2013).
\item Releases from small scale and artisanal to water bodies are estimated at 800 tones ($63\%$ of emissions) (UNEP, 2013).
\item Mercury is a persistent bio-accumulative toxin.

\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Related Economics literature}
\begin{itemize}
\item Econ literature on mining was mainly focused on input reallocation, ignoring externalities.
\item Aragon and Rud (2013a) find that mining reduces agricultural productivity.
\item Aragon and Rud (2013b) find a positive effect on real income for non-mining workers in Peru.
\item von der Goltz and Barnwal (2014) finds communities near mine increase asset wealth and anemia rates (Only extensive margin variation).

\end{itemize}
\end{frame}





\section{Data}



\begin{frame}[label=dat2]
\frametitle{Mining Data}
Mines
\begin{itemize}
\item Mining permits location and database collected from 1990 to 2012. 
\item Gold production per municipality (Yearly 2001-2003, Quarterly 2004-2013)
\end{itemize}
%\hyperlink{sumstat2<1>}{\beamergotobutton{Summary Statistics}}
\end{frame}

\begin{frame}[plain,label=sumstat2]
\frametitle{Summary Statistics Mines}
\input{SummaryMines}

\end{frame}

\begin{frame}[plain]
\frametitle{Mining Data}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.7\textwidth]{ProdArea} 
\caption{ \tiny{Evolution over time of gold production (in kilograms) and total mined area (in square kilometers). Source: Ministerio de Minas y Energia de Colombia. Calculations: Authors.}}

\label{fig:evol_mineria}
\end{center}
\end{figure}
\end{frame}



\begin{frame}[label=dat1]
\frametitle{Vital Statistics Data}
Health
\begin{itemize}
\item Vital Statistics for all 1,100 Colombian municipalities 
\item From 1999-2012, there is information on all newborns:
\begin{itemize}
\item Sex
\item Weight 
\item Height
\item APGAR score 
\item Mother's age, education and marital status.
\end{itemize}
\end{itemize}
%\hyperlink{sumstat1<1>}{\beamergotobutton{Summary Statistics}}
\end{frame}


\begin{frame}[plain,label=sumstat1]
\frametitle{Vital Statistics Data}
{\scriptsize 
\input{SummaryBirth}
}
\end{frame}

\begin{frame}[plain]
\frametitle{Vital Statistics Data}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.65\textwidth]{apgar_region} 
\caption{ \scriptsize{Evolution of the proportion of newborns with low APGAR by region Source: Vital Statistics. Calculations: Authors.}}
\label{fig:evol_apgar}
\end{center}
\end{figure}
\end{frame}







\section{Identification Strategy and Results}


\begin{frame}
\frametitle{Preliminary results}
{\scriptsize $Y_{imt}=\beta_1 Gold_{mt}+ \beta_2 Gold^2_{mt}+X_i \alpha+\gamma_m+ \gamma_{t}+\lambda_{r(m)}\times t+\varepsilon_{imt} $}
\begin{scriptsize}
\begin{itemize}
\item $Y_{imt}$ outcome variable for birth $i$ in municipality $m$ at time $t$
\item $Gold_{mt}$ measure of gold mining activity in municipality $m$ at time $t$
\item $\gamma_m$ municipality fixed effects
\item $\gamma_{t}$ year and week of birth fixed effects 
\item $\lambda_{r(m)}\times t$ regional trends
\item $X_i$ individual controls (mother's age, mother's education, mother's marital status).
\item $ \varepsilon_{imt}$ error term
\end{itemize}
$\beta_1$ and $\beta_2$ are reduced form estimates of the effect of gold mining on health
\end{scriptsize}

\end{frame}

\begin{frame}[plain]
\frametitle{Summary gold exposure}

\begin{tiny}
\begin{tabular}{l*{1}{ccccccc}}
\toprule
                    &        Mean&      Median&   Std. Dev.&         Min&         Max&    Perc. 95&           N\\
\midrule
\multicolumn{4}{l}{\textbf{Panel A: All observations}} \\
Mining Area/Municipality Area&      0.0044&           0&       0.029&           0&        0.90&      0.0078&     9281041\\
Production/Municipality Area&      0.0087&           0&        0.12&           0&        11.4&       0.026&     7797167\\
\midrule
\multicolumn{4}{l}{\textbf{Panel B: Positive Values}} \\
Mining Area/Municipality Area&       0.041&      0.0075&       0.081&    &        0.90&        0.19&     1001736\\
Production/Municipality Area&       0.039&      0.0036&        0.26&  &        11.4&        0.13&     1748194\\
\bottomrule
\end{tabular}
\end{tiny}

\end{frame}

\begin{frame}[plain]
\frametitle{Effect of mining on birth outcomes -Individual level data}

\begin{table}[H]\centering
\caption{Effect of mining on birth outcomes -Individual level data}
\begin{tiny}
\resizebox{!}{3.9 cm}{
\begin{tabular}{l*{3}{c}}
\toprule
                    &\multicolumn{1}{c}{(1)}&\multicolumn{1}{c}{(2)} &\multicolumn{1}{c}{(3)}\\
                    &\multicolumn{1}{c}{APGAR}&\multicolumn{1}{c}{LBW} &\multicolumn{1}{c}{Height}\\
\midrule
\multicolumn{4}{l}{\textbf{Panel A: Proportion of mined area}} \\
\input{IndividualMeasure_AreaMinadaProp} 
\midrule
\multicolumn{4}{l}{\textbf{Panel B: Production per area}} \\
\input{IndividualMeasure_ProduccionPerArea}
\bottomrule
Time F.E. & Yes & Yes & Yes\\
Municipality F.E. & Yes & Yes & Yes\\
Regional Trends & Yes & Yes & Yes\\
\bottomrule
\multicolumn{4}{l}{\tiny \specialcell{Clustered standard errors, by municipality, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }}\\
\end{tabular}
}
\end{tiny}
\label{tab:regIndividualMeasure}
\end{table}
\end{frame}

\begin{frame}
\frametitle{Measurement Error}
\begin{itemize}
\item Measurement Error is potentially an issue
\item Use dummy variables to attenuate the problem
\end{itemize}

 $Y_{imt}=\beta_1 1[Gold_{mt}>0]+X_i \alpha+\gamma_m+ \gamma_{t}+\lambda_{m}\times t+\varepsilon_{imt}$
\end{frame}

\begin{frame}[plain]
\frametitle{Effect of mining on birth outcomes -Individual level data}
\begin{tiny}

\begin{table}[H]\centering
\caption{Effect of mining on birth outcomes- Individual level data}
\resizebox{11 cm}{!}{
\begin{tabular}{l*{3}{c}}
\toprule
                    &\multicolumn{1}{c}{(1)}&\multicolumn{1}{c}{(2)} &\multicolumn{1}{c}{(3)}\\
                    &\multicolumn{1}{c}{APGAR}&\multicolumn{1}{c}{LBW} &\multicolumn{1}{c}{Height}\\
\midrule
\multicolumn{4}{l}{\textbf{Panel A: Proportion of mined area}} \\
\input{IndividualDummies_AreaMinadaProp} 
\midrule
\multicolumn{4}{l}{\textbf{Panel B: Production per area}} \\
\input{IndividualDummies_ProduccionPerArea} 
\bottomrule
\multicolumn{4}{l}{\footnotesize \specialcell{Clustered standard errors, by municipality, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }}\\
\end{tabular}
}
\label{tab:dummies}
\end{table}
\end{tiny}
\end{frame}

\begin{frame}
\frametitle{River and proximity exposure}
\begin{itemize}
\item Estimate pollution exposure of population through rivers and proximity to mines.
\item For each mine identify the closest river (usually inside the mine) and attach a ``pollution'' index equal to the size of the mine.
\item Diffuse pollution downstream according to $River Pollution=\sum_{i \in mines\_upstream} 0.76^{dist_i}Area_i$.
\item Buffer of 10km around the river.

\end{itemize}
\end{frame}

\begin{frame}
\frametitle{River pollution}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.40\textwidth]{Data_mines.png} \quad
\includegraphics[width=0.40\textwidth]{Data_polu.png} 
\caption{Left panel shows an example of mines (in brown) located in municipalities (boundaries in black) and the rivers (in blue) in the area. The right panel shows the diffusion of pollution through rivers (low pollution in green and high pollution in red).}
\label{fig:1}
\end{center}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{River pollution}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.40\textwidth]{Data_pop.png} \quad
\includegraphics[width=0.40\textwidth]{Data_exp.png} 
\caption{Light pink (low) to dark red (High) scale: population density by pixel (left) and resultant exposure (right). Exposure is the product of population and pollution values.}
\end{center}
\end{figure}
\end{frame}



\begin{frame}[plain]
\frametitle{Effect of mining on birth outcomes -Individual level data}

\begin{table}[H]\centering
\caption{Effect of mining on birth outcomes -Individual level data}
\begin{tiny}
\resizebox{!}{3.9 cm}{
\begin{tabular}{l*{3}{c}}
\toprule
                    &\multicolumn{1}{c}{(1)}&\multicolumn{1}{c}{(2)} &\multicolumn{1}{c}{(3)}\\
                    &\multicolumn{1}{c}{APGAR}&\multicolumn{1}{c}{LBW} &\multicolumn{1}{c}{Height}\\
\midrule
\multicolumn{4}{l}{\textbf{Panel A: Combined Exposure}} \\
\input{IndividualMeasure_Combo}
\bottomrule
Time F.E. & Yes & Yes & Yes\\
Municipality F.E. & Yes & Yes & Yes\\
Regional Trends & Yes & Yes & Yes\\
\bottomrule
\multicolumn{4}{l}{\tiny \specialcell{Clustered standard errors, by municipality, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }}\\
\end{tabular}
}
\end{tiny}
\label{tab:regIndividualMeasure}
\end{table}
\end{frame}


\begin{frame}[plain]
\frametitle{Effect of mining on birth outcomes -Individual level data}
\begin{tiny}

\begin{table}[H]\centering
\caption{Effect of mining on birth outcomes- Individual level data}
\resizebox{11cm}{!}{
\begin{tabular}{l*{3}{c}}
\toprule
                    &\multicolumn{1}{c}{(1)}&\multicolumn{1}{c}{(2)} &\multicolumn{1}{c}{(3)}\\
                    &\multicolumn{1}{c}{APGAR}&\multicolumn{1}{c}{LBW} &\multicolumn{1}{c}{Height}\\
\midrule
\multicolumn{4}{l}{\textbf{Panel A: Combined Exposure}} \\
\input{IndividualDummies_Combo} 
\bottomrule
\multicolumn{4}{l}{\footnotesize \specialcell{Clustered standard errors, by municipality, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }}\\
\end{tabular}
}
\label{tab:dummies}
\end{table}
\end{tiny}
\end{frame}

\begin{frame}
\frametitle{Holy Week}
\begin{itemize}
\item Colombia is predominantly a catholic country ($> 75\%$ of population). Latinobarometro 2011
\item Holy Week is major Catholic holiday and Maundy Thursday and Good Friday are national holidays 
\item Fish consumption increases during holy week by 60\% \cite{ElColombiano2014}. 
\item Exogenous variation of in-utero mercury exposure.
\item Holy Week date varies from year to year (partially remove seasonality).
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Preliminary results}
\begin{align}\label{eq:regHW}
Y_{imt}=& \beta_1Mining_{m}+\beta_2 Active_{mt}+\beta_3 Holy\_week_{i}+ \nonumber \\
&\beta_4 Mining_{m}\times Active_{mt}+\beta_5 Mining_{m}\times Holy\_week_{i} + \nonumber \\
& +\beta_6 Holy\_week_{i} \times Active_{mt}+  \beta_7  Mining_{m}\times Active_{mt} \times Holy\_week_{i} +\nonumber \\
&  +X_i \alpha+\gamma_{Year(t)}+ \gamma_{Week(t)}+\lambda_{r(m)}+\varepsilon_{imt} \nonumber 
\end{align}
\begin{scriptsize}
\begin{itemize}
\item $Y_{imt}$ indicator of low APGAR for birth $i$ in municipality $m$ at time $t$
\item $Holy\_week_{i}$ is a dummy variable that indicates if individual $i$ gestational period overlaps with holy week.
\item $Mining_m$ indicates if the municipality ever has mining activity
\item $Active_{mt}$ if the municipality has an active title
\item We define the active period for a non-mining municipality as the period after which the closest mine is active.
\item $ \varepsilon_{imt}$ error term
\end{itemize}
$\beta_7$ is the coefficient of interest
\end{scriptsize}

\end{frame}

\begin{frame}[plain]
\input{hw3d}
\end{frame}







\begin{frame}[label=others]
\frametitle{Others}
\begin{itemize}
\item Illegal mining. \hyperlink{ilegal}{\beamergotobutton{Results}}
\item Migration. \hyperlink{migration}{\beamergotobutton{Results}}
\item Mercury related events other population. \hyperlink{poison}{\beamergotobutton{Results}}
\end{itemize}
\end{frame}



\begin{frame}
\frametitle{Future steps}
\begin{itemize}
\item Robustness checks for exposure parameters.
\item Data on fish markets by municipality?
\item Use other type of mines for falsification.
\item Individual measures of pollution
\item Measures of illegal mining using satellite images?
\end{itemize}

\end{frame}


\begin{frame}
\frametitle{Thank you}
\begin{itemize}
\item Gracias
\item Asante Sana
\item Merci
\item Obrigado
\item Grazie
\end{itemize}

\end{frame}

\begin{frame}[label=ilegal]
\frametitle{Illegal Mining}
\begin{itemize}
\item Over 60\% of mines do not have a permit \cite{Ministerio2012}.
\item Classify municipalities are prone to illegal mining or not
\item Information from 2010-2011 mining census and police reports of illegal mines.
\end{itemize}
\end{frame}

\begin{frame}[plain]
\begin{table}[H]\centering
\caption{Effect of mining on birth outcomes by illegal mining}
\begin{footnotesize}
\resizebox{11cm}{!}{
\begin{tabular}{l*{2}{c}}
\toprule
                    &\multicolumn{1}{c}{(1)}&\multicolumn{1}{c}{(2)} \\
                    &\multicolumn{1}{c}{APGAR}&\multicolumn{1}{c}{LBW} \\
\midrule
\multicolumn{3}{l}{\textbf{Panel A: Proportion of mined area - Legal Prone Municipalities}} \\
\input{IndividualMeasure_AreaMinadaProp_legalidad0} 
\midrule
\multicolumn{3}{l}{\textbf{Panel B: Proportion of mined area - Illegal Prone Municipalities}} \\
\input{IndividualMeasure_AreaMinadaProp_legalidad1} 
\bottomrule
\multicolumn{3}{l}{\footnotesize \specialcell{Coefficient for fixed effects not shown. Clustered standard errors, by municipality, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }}\\
\end{tabular}
}
\end{footnotesize}
\label{tab:illegalmeassure}
\end{table}
\hyperlink{others}{\beamergotobutton{Back}}
\end{frame}

\begin{frame}[plain, label=migration]
\begin{table}[H]\centering
\caption{Effect of mining on fertility}
\resizebox{!}{3.9cm}{
\begin{tabular}{l*{2}{c}}
\toprule
                    &\multicolumn{1}{c}{(1)}&\multicolumn{1}{c}{(2)}\\
                    &\multicolumn{1}{c}{Fertility}&\multicolumn{1}{c}{Births}\\
\midrule
\multicolumn{3}{l}{\textbf{Panel A: Proportion of mined area}} \\
\input{FertilidadMeassure_AreaMinadaProp} 
\midrule
\multicolumn{3}{l}{\textbf{Panel B: Production per area}} \\
\input{FertilidadMeassure_ProduccionPerArea}
\bottomrule
\multicolumn{3}{l}{\footnotesize \specialcell{Clustered standard errors, by municipality, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }}\\
\multicolumn{3}{l}{\footnotesize Observations are weighted by population in the case of fertility. }\\
\end{tabular}
}
\label{tab:birth_measure}
\end{table}
\end{frame}

\begin{frame}[plain]
\begin{table}[H]\centering
\caption{Effect of mining on fertility}
\begin{tabular}{l*{2}{c}}
\toprule
                    &\multicolumn{1}{c}{(1)}&\multicolumn{1}{c}{(2)}\\
                    &\multicolumn{1}{c}{Fertility}&\multicolumn{1}{c}{Births}\\
\midrule
\multicolumn{3}{l}{\textbf{Panel A: Proportion of mined area}} \\
\input{FertilidadDummies_AreaMinadaProp} 
\midrule
\multicolumn{3}{l}{\textbf{Panel B: Production per area}} \\
\input{FertilidadDummies_ProduccionPerArea}
\bottomrule
\multicolumn{3}{l}{\footnotesize \specialcell{Clustered standard errors, by municipality, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }}\\
\multicolumn{3}{l}{\footnotesize Observations are weighted by population in the case of fertility. }\\
\end{tabular}
\label{tab:birth_dummies}
\end{table}
\hyperlink{others}{\beamergotobutton{Back}}
\end{frame}

\begin{frame}[plain, label=poison]
\begin{table}[H]\centering
\caption{Effect of mining on health}
\resizebox{11cm}{!}{
\begin{tabular}{l*{3}{c}}

\toprule
                    &\multicolumn{1}{c}{Heavy Metal}&\multicolumn{2}{c}{Mercury Related Diagnosis}\\
                    &\multicolumn{1}{c}{Poisoning}&\multicolumn{1}{c}{Proportion}&\multicolumn{1}{c}{Per capita}\\
\midrule
\multicolumn{4}{l}{\textbf{Panel A: Proportion of mined area}} \\
\input{SaludMeasure_AreaMinadaProp} 
\bottomrule
\multicolumn{4}{l}{\footnotesize \specialcell{Clustered standard errors, by municipality, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }}\\
\multicolumn{4}{l}{\footnotesize Observations are weighted by population. }\\
\end{tabular}
}
\label{tab:salud_measure}
\end{table}
\hyperlink{others}{\beamergotobutton{Back}}
\end{frame}



%\renewcommand*{\refname}{} % This will define heading of bibliography to be empty, so you can...
\begin{frame}[allowframebreaks]{Bibliography}
	\bibliographystyle{apacite}
\bibliography{bibmin}
\end{frame}

%\begin{frame}[label=sumstat2]
%\frametitle{Summary Statistics II}
%\begin{table}[H]\centering \caption{Summary statistics \label{sumstat}}
%\begin{tabular}{l c c c c }\hline\hline
%\multicolumn{1}{c}{\textbf{Variable}} & \textbf{Mean}
% & \textbf{Std. Dev.}& \textbf{Min.} &  \textbf{Max.} \\ \hline
%Area km\^{}2 & 1.338 & 7.091 & 0 & 190.609 \\
%Year & 2003.338 & 3.87 & 1990 & 2008 \\
%\multicolumn{1}{c}{N} & \multicolumn{4}{c}{7638}\\ \hline
%\end{tabular}
%\end{table}
%\hyperlink{dat2<1>}{\beamerreturnbutton{Go Back}}
%\end{frame}

\end{document}

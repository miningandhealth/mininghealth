


global latexcodes     "$gitpath/LaTeX/201510_2ndDraft"
set matsize 5000
eststo clear
******************************************************************************
******************************************************************************
************************** STUFF DONE WITH INDIVIDUAL DATA *******************
******************************************************************************
******************************************************************************
use "$base_out/DataCompleta_Stata.dta", clear
global control4 MadreSoltera EduMadrePostPrimaria EDAD_MADRE
qui gen coddepto=(codmpio-mod(codmpio,1000))/1000
drop if ano<=2003 /*for comparison */
drop if mod(codmpio,1000)==1  /*for comparison */
reghdfe APGAR_BAJO D_ExpAreaMuni20KM D_UpstreamAreaMuni20KM $control4 , absorb(i.codmpio i.Semana_Naci  i.ano) vce(cluster codmpio)
reghdfe APGAR_BAJO D_ExpAreaMuni20KM D_UpstreamAreaMuni20KM $control4 , absorb(i.codmpio i.Semana_Naci i.coddepto#c.ano i.ano) vce(cluster codmpio)


/*Something we need to check is why this numbers are so high */
replace ExpAreaMuni5KM=ExpAreaMuni5KM/1e6
replace ExpAreaMuni10KM=ExpAreaMuni10KM/1e6
replace ExpAreaMuni20KM=ExpAreaMuni20KM/1e6

qui gen coddepto=(codmpio-mod(codmpio,1000))/1000

/*
gen ihwb=0 if wgesthw!=.
replace ihwb=1 if ihw==1 & wgesthw<=20
label var ihwb "Holyweek first 20 weeks of gestation"

gen ihwe=0 if wgesthw!=.
replace ihwe=1 if ihw==1 & ihwb==0
label var ihwe "Holyweek last weeks of gestation"
*/
gen ihw1tr=0 if wgesthw!=.
replace ihw1tr=1 if ihw==1 & wgesthw<=13
label var ihw1tr "Holyweek 1st trimester"

gen ihw2tr=0 if wgesthw!=.
replace ihw2tr=1 if ihw==1 & wgesthw>=14 & wgesthw<=27
label var ihw2tr "Holyweek 2nd trimester"

gen ihw3tr=0 if wgesthw!=.
replace ihw3tr=1 if ihw==1 & wgesthw>=28 & wgesthw<=43
label var ihw3tr "Holyweek 3rd trimester"


gen ihw12tr=0 if wgesthw!=.
replace ihw12tr=1 if ihw==1 & wgesthw<=27
label var ihw12tr "Holyweek 1-2 trimester"



global control1 i.ano i.Semana_Naci 
global control2 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
global control3 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE coddepto#c.ano
global control4 MadreSoltera EduMadrePostPrimaria EDAD_MADRE


**************************
**TABLE 1 Sum stats birth
**************************
eststo clear
estpost tabstat APGAR_BAJO PartoHospital PESO_NAC LBW TALLA_NAC STUNT EDAD_MADRE MadreSoltera EduMadrePostPrimaria, statistics(mean median sd max min count) columns(statistics)
esttab using "$latexcodes/Tab1_SummaryBirth.tex", cells("mean(fmt(a2) label(Mean)) p50(fmt(a2) label(Median)) sd(fmt(a2) label(Std. Dev.)) min(fmt(a2) label(Min)) max(fmt(a2) label(Max)) count(fmt(a2) label(N))") nomtitle nonumber label  replace noobs booktabs

**************************
**TABLE 1 A
**************************
*Sumstat AreaMinada

gen AreaMinadaPropPositiva=AreaMinadaProp if AreaMinadaProp>0 & !missing(AreaMinadaProp)
label var Produccion "Production(gr.)"
label var AreaMinadaPropPositiva "Mining Area/Municipality Area (Conditional)"
label var AreaMinadaProp "Mining Area/Municipality Area (Unconditional)"
eststo clear
estpost tabstat Produccion AreaMinadaProp D_AreaMinadaProp AreaMinadaPropPositiva  D_ExpAreaMuni20KM D_UpstreamAreaMuni20KM, statistics(mean median sd max min count) columns(statistics)
esttab using "$latexcodes/Tab1A_SummaryMines.tex", cells("mean(fmt(a2) label(Mean)) p50(fmt(a2) label(Median)) sd(fmt(a2) label(Std. Dev.)) min(fmt(a2) label(Min)) max(fmt(a2) label(Max)) count(fmt(a2) label(N))") nomtitle nonumber label  replace noobs booktabs

label var AreaMinadaProp "Mining Area/Municipality Area"
label var D_ProduccionPerCapita "Production `=char(36)'>0`=char(36)' 0"

eststo clear

**************************
**TABLE 2 PANEL A
**************************
foreach var of varlist APGAR_BAJO LBW STUNT{
	eststo Basic_`var': quietly  reghdfe `var' D_ExpAreaMuni20KM D_UpstreamAreaMuni20KM $control4 , absorb(i.codmpio Semana_Naci i.coddepto#c.ano i.ano) vce(cluster codmpio)
	estadd ysumm
}
estout Basic_APGAR_BAJO Basic_LBW Basic_STUNT using "$latexcodes/Tab2_MainResults.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_ExpAreaMuni20KM D_UpstreamAreaMuni20KM)  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "`=char(36)' R^2`=char(36)'" )) replace

**************************
**TABLE 2 PANEL B
**************************
foreach var of varlist APGAR_BAJO LBW STUNT{
	eststo Distance_`var': quietly reghdfe `var' D_ExpAreaMuni5KM D_ExpAreaMuni510KM D_ExpAreaMuni1020KM  D_UpstreamAreaMuni5KM D_UpstreamAreaMuni510KM D_UpstreamAreaMuni1020KM $control4 , absorb(i.codmpio Semana_Naci i.coddepto#c.ano i.ano) vce(cluster codmpio)
	estadd ysumm
}

estout Distance_APGAR_BAJO Distance_LBW Distance_STUNT using "$latexcodes/Tab2_MainResults_Distance.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
	keep(D_ExpAreaMuni5KM D_ExpAreaMuni510KM D_ExpAreaMuni1020KM  D_UpstreamAreaMuni5KM D_UpstreamAreaMuni510KM D_UpstreamAreaMuni1020KM )   stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "`=char(36)' R^2`=char(36)'" )) replace
	
**************************
**TABLE 3
**************************
eststo hw1: quietly reghdfe APGAR_BAJO ihw D_ExpAreaMuni20KM D_UpstreamAreaMuni20KM c.D_ExpAreaMuni20KM#c.ihw  c.D_UpstreamAreaMuni20KM#c.ihw   $control4  if wgesthw!=., absorb( i.codmpio Semana_Naci i.coddepto#c.ano i.ano) vce(cluster codmpio) 
estadd ysumm

eststo hw2: quietly reghdfe APGAR_BAJO c.(D_ExpAreaMuni20KM D_UpstreamAreaMuni20KM)##c.(ihw1tr ihw2tr ihw3tr)  $control4  if wgesthw!=., absorb( i.codmpio Semana_Naci i.coddepto#c.ano i.ano) vce(cluster codmpio) 
estadd ysumm

*This regression runs HW for small munis. 
** Small is defined by the median of muni population calculated in the Muni Panel
*eststo hwmp : quietly reghdfe APGAR_BAJO c.(D_ExpAreaMuni20KM D_UpstreamAreaMuni20KM)##c.(ihw1tr ihw2tr ihw3tr)  $control4 if wgesthw!=. & pobl_tot<50000 , absorb(i.codmpio Semana_Naci i.coddepto#c.ano i.ano) vce(cluster codmpio) 
*estadd ysumm

estout hw1 hw2 using "$latexcodes/Tab3_HW.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "`=char(36)' R^2`=char(36)'")) replace ///
keep(D_ExpAreaMuni20KM D_UpstreamAreaMuni20KM ihw ihw1tr ihw2tr ihw3tr c.D_ExpAreaMuni20KM#* c.D_UpstreamAreaMuni20KM#* ) varl(ihw "Holy Week" c.D_ExpAreaMuni20KM#c.ihw "Near mining `=char(36)'\times`=char(36)' Holy Week" c.D_UpstreamAreaMuni20KM#c.ihw "Upstream mining  `=char(36)'\times`=char(36)' Holy Week"  ///
c.D_ExpAreaMuni20KM#c.ihw1tr "Near mining `=char(36)'\times`=char(36)' Holy Week 1st Trim" c.D_UpstreamAreaMuni20KM#c.ihw1tr "Upstream mining  `=char(36)'\times`=char(36)' Holy Week 1st Trim"  ///
c.D_ExpAreaMuni20KM#c.ihw2tr "Near mining `=char(36)'\times`=char(36)' Holy Week 2nd Trim" c.D_UpstreamAreaMuni20KM#c.ihw2tr "Upstream mining  `=char(36)'\times`=char(36)' Holy Week 2nd Trim"  ///
c.D_ExpAreaMuni20KM#c.ihw3tr "Near mining `=char(36)'\times`=char(36)' Holy Week 3rd Trim" c.D_UpstreamAreaMuni20KM#c.ihw3tr "Upstream mining  `=char(36)'\times`=char(36)' Holy Week 3rd Trim" )

**************************
**TABLE 4
**************************
label var D_ExpAreaMuni20KMxprice "Near mining 20 km `=char(36)'>0`=char(36)' x Price"
label var D_UpstreamAreaMuni20KMxprice "Mining upstream 20 km `=char(36)'>0`=char(36)' x Price"
eststo Precio_APGAR_BAJO: quietly reghdfe APGAR_BAJO ihw D_ExpAreaMuni20KMxprice D_UpstreamAreaMuni20KMxprice   $control4 , absorb( i.codmpio Semana_Naci i.coddepto#c.ano i.ano) vce(cluster codmpio) 
estadd ysumm
estout Basic_APGAR_BAJO Precio_APGAR_BAJO using "$latexcodes/Tab4_Price.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_ExpAreaMuni* D_UpstreamArea*  )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "`=char(36)' R^2`=char(36)'" )) replace




**************************
**TABLE 7 - Panel B
**************************
forvalues i=0/1{
eststo Illegal`i': quietly reghdfe APGAR_BAJO D_ExpAreaMuni20KM D_UpstreamAreaMuni20KM $control4  if Ilegal==`i', absorb(i.codmpio Semana_Naci i.coddepto#c.ano i.ano) vce(cluster codmpio)
estadd ysumm
}
estout Basic_APGAR_BAJO Illegal0 Illegal1 using "$latexcodes/Tab7_Illegal.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_ExpAreaMuni20KM D_UpstreamAreaMuni20KM )  stats(N N_clust ymean r2, fmt(a2 a2 a2 a2) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "`=char(36)' R^2`=char(36)'")) replace



**************************
**TABLE 2 A
**************************
foreach var of varlist APGAR_BAJO LBW STUNT{
	eststo Area_`var': quietly  reghdfe `var' AreaMinadaProp  $control4 , absorb(i.codmpio Semana_Naci i.coddepto#c.ano i.ano) vce(cluster codmpio)
	estadd ysumm
	eststo D_Area_`var': quietly  reghdfe `var' D_AreaMinadaProp  $control4 , absorb(i.codmpio Semana_Naci i.coddepto#c.ano i.ano) vce(cluster codmpio)
	estadd ysumm
	eststo ProdCapita_`var': quietly  reghdfe `var' ProduccionPerCapita  $control4 , absorb(i.codmpio Semana_Naci i.coddepto#c.ano i.ano) vce(cluster codmpio)
	estadd ysumm
	eststo D_ProdCapita_`var': quietly  reghdfe `var' D_ProduccionPerCapita  $control4 , absorb(i.codmpio Semana_Naci i.coddepto#c.ano i.ano) vce(cluster codmpio)
	estadd ysumm
	eststo ProdAccArea_`var': quietly  reghdfe `var' ProduccionAccPerArea  $control4 , absorb(i.codmpio Semana_Naci i.coddepto#c.ano i.ano) vce(cluster codmpio)
	estadd ysumm
	eststo D_ProdAccArea_`var': quietly  reghdfe `var' D_ProduccionAccPerArea  $control4 , absorb(i.codmpio Semana_Naci i.coddepto#c.ano i.ano) vce(cluster codmpio)
	estadd ysumm
	eststo ProdArea_`var': quietly  reghdfe `var' ProduccionPerArea  $control4 , absorb(i.codmpio Semana_Naci i.coddepto#c.ano i.ano) vce(cluster codmpio)
	estadd ysumm
}
estout Area_APGAR_BAJO Area_LBW Area_STUNT using "$latexcodes/Tab2A_PanelA.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(AreaMinadaProp )   replace

estout D_Area_APGAR_BAJO D_Area_LBW D_Area_STUNT using "$latexcodes/Tab2A_PanelB.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_AreaMinadaProp )   replace

estout ProdCapita_APGAR_BAJO ProdCapita_LBW ProdCapita_STUNT using "$latexcodes/Tab2A_PanelC.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(ProduccionPerCapita )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "`=char(36)' R^2`=char(36)'" )) replace

estout D_ProdCapita_APGAR_BAJO D_ProdCapita_LBW D_ProdCapita_STUNT using "$latexcodes/Tab2A_PanelD.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_ProduccionPerCapita )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "`=char(36)' R^2`=char(36)'" )) replace

estout ProdAccArea_APGAR_BAJO ProdAccArea_LBW ProdAccArea_STUNT using "$latexcodes/Tab2A_PanelE.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(ProduccionAccPerArea )  replace

estout D_ProdAccArea_APGAR_BAJO D_ProdAccArea_LBW D_ProdAccArea_STUNT using "$latexcodes/Tab2A_DProdAcc.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_ProduccionAccPerArea )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "`=char(36)' R^2`=char(36)'" )) replace

estout ProdArea_APGAR_BAJO ProdArea_LBW ProdArea_STUNT using "$latexcodes/Tab2A_PanelF.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(ProduccionPerArea )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "`=char(36)' R^2`=char(36)'" )) replace


**************************
**TABLE 3 A
**************************
foreach var of varlist APGAR_CONTINUO PESO_NAC TALLA_NAC{
	eststo Basic_`var': quietly  reghdfe `var' D_ExpAreaMuni20KM D_UpstreamAreaMuni20KM  $control4 , absorb(i.codmpio Semana_Naci i.coddepto#c.ano i.ano) vce(cluster codmpio)

}
estout Basic_APGAR_CONTINUO Basic_PESO_NAC Basic_TALLA_NAC using "$latexcodes/Tab3A_Continuas.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_ExpAreaMuni20KM D_UpstreamAreaMuni20KM )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "`=char(36)' R^2`=char(36)'" )) replace

**************************
**TABLE 4 A
**************************
foreach var of varlist APGAR_BAJO LBW STUNT{
	eststo Intesnive_`var': quietly  reghdfe `var' ExpAreaMuni20KM UpstreamAreaMuni20KM  $control4 , absorb(i.codmpio Semana_Naci i.coddepto#c.ano i.ano) vce(cluster codmpio)

}
estout Intesnive_APGAR_BAJO Intesnive_LBW Intesnive_STUNT using "$latexcodes/Tab4A_Intensive.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(ExpAreaMuni20KM UpstreamAreaMuni20KM )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "`=char(36)' R^2`=char(36)'" )) replace


**************************
**FIGURES HOW MANY DOWNSTREAM/NEAR
**************************
*Figure 1 - raw-municiplaities
preserve
collapse (max) D_ExpAreaMuni5KM D_ExpAreaMuni510KM D_ExpAreaMuni1020KM D_UpstreamAreaMuni5KM D_UpstreamAreaMuni510KM D_UpstreamAreaMuni1020KM, by(ano MES codmpio)
collapse (sum) D_ExpAreaMuni5KM D_ExpAreaMuni510KM D_ExpAreaMuni1020KM D_UpstreamAreaMuni5KM D_UpstreamAreaMuni510KM D_UpstreamAreaMuni1020KM, by(ano MES)
foreach var of varlist D_ExpAreaMuni5KM D_ExpAreaMuni510KM D_ExpAreaMuni1020KM D_UpstreamAreaMuni5KM D_UpstreamAreaMuni510KM D_UpstreamAreaMuni1020KM{
	replace `var'=. if `var'==0
}

gen date=ym(ano,MES)
format date %tm

foreach distancia in 5 510 1020{
	if (`distancia'==5) local label_dist "0-5"
	if (`distancia'==210) local label_dist "5-10"
	if (`distancia'==1020) local label_dist "10-20"
	twoway (line D_ExpAreaMuni`distancia'KM date, sort lcolor(blue) lpattern(longdash) legend(lab(1 "Near mining"))) (line D_UpstreamAreaMuni`distancia'KM date, sort lcolor(red) lpattern(solid) legend(lab(2 "Upstream mining"))), xtitle(Date) ytitle(Municipalities) legend(order(1 2)) title("Municipalities near and downstream from a mine - `label_dist' KM", size(vsmall))
	graph export "$latexcodes/Municipalities_`distancia'.pdf", as(pdf) replace
}
restore
*Figure 2 - proportion-municiplaities
preserve
collapse (max) D_ExpAreaMuni5KM D_ExpAreaMuni510KM D_ExpAreaMuni1020KM D_UpstreamAreaMuni5KM D_UpstreamAreaMuni510KM D_UpstreamAreaMuni1020KM, by(ano MES codmpio)
collapse (mean)  D_ExpAreaMuni5KM D_ExpAreaMuni510KM D_ExpAreaMuni1020KM D_UpstreamAreaMuni5KM D_UpstreamAreaMuni510KM D_UpstreamAreaMuni1020KM, by(ano MES)
gen date=ym(ano,MES)
format date %tm
foreach var of varlist D_ExpAreaMuni5KM D_ExpAreaMuni510KM D_ExpAreaMuni1020KM D_UpstreamAreaMuni5KM D_UpstreamAreaMuni510KM D_UpstreamAreaMuni1020KM{
	replace `var'=. if `var'==0
}

foreach distancia in 5 510 1020{
	if (`distancia'==5) local label_dist "0-5"
	if (`distancia'==210) local label_dist "5-10"
	if (`distancia'==1020) local label_dist "10-20"
	twoway (line D_ExpAreaMuni`distancia'KM  date, sort lcolor(blue) lpattern(longdash) legend(lab(1 "Near mining"))) (line D_UpstreamAreaMuni`distancia'KM date, sort lcolor(red) lpattern(solid) legend(lab(2 "Upstream mining"))), xtitle(Date) ytitle(Municipalities) legend(order(1 2)) title("Municipalities near and downstream from a mine - `label_dist' KM", size(vsmall))
	graph export "$latexcodes/PropMunicipalities_`distancia'.pdf", as(pdf) replace
}
restore
*Figure 3 - raw-births
preserve
collapse (sum)  D_ExpAreaMuni5KM D_ExpAreaMuni510KM D_ExpAreaMuni1020KM D_UpstreamAreaMuni5KM D_UpstreamAreaMuni510KM D_UpstreamAreaMuni1020KM, by(ano MES)
gen date=ym(ano,MES)
format date %tm
foreach var of varlist D_ExpAreaMuni5KM D_ExpAreaMuni510KM D_ExpAreaMuni1020KM D_UpstreamAreaMuni5KM D_UpstreamAreaMuni510KM D_UpstreamAreaMuni1020KM{
	replace `var'=. if `var'==0
}
foreach distancia in 5 510 1020{
	if (`distancia'==5) local label_dist "0-5"
	if (`distancia'==210) local label_dist "5-10"
	if (`distancia'==1020) local label_dist "10-20"
	 twoway (line D_ExpAreaMuni`distancia'KM date, sort lcolor(blue) lpattern(longdash) legend(lab(1 "Near mining"))) (line D_UpstreamAreaMuni`distancia'KM date, sort lcolor(red) lpattern(solid) legend(lab(2 "Upstream mining"))), xtitle(Date) ytitle(Births) legend(order(1 2)) title("Births near and downstream from a mine - `label_dist' KM", size(vsmall))
	graph export "$latexcodes/Bebes_`distancia'.pdf", as(pdf) replace
}
restore

*Figure 4 - prop-births
preserve
collapse (mean)  D_ExpAreaMuni5KM D_ExpAreaMuni510KM D_ExpAreaMuni1020KM D_UpstreamAreaMuni5KM D_UpstreamAreaMuni510KM D_UpstreamAreaMuni1020KM, by(ano MES)
gen date=ym(ano,MES)
format date %tm
foreach var of varlist D_ExpAreaMuni5KM D_ExpAreaMuni510KM D_ExpAreaMuni1020KM D_UpstreamAreaMuni5KM D_UpstreamAreaMuni510KM D_UpstreamAreaMuni1020KM{
	replace `var'=. if `var'==0
}
foreach distancia in 5 510 1020{
	if (`distancia'==5) local label_dist "0-5"
	if (`distancia'==210) local label_dist "5-10"
	if (`distancia'==1020) local label_dist "10-20"
	 twoway (line D_ExpAreaMuni`distancia'KM date, sort lcolor(blue) lpattern(longdash) legend(lab(1 "Near mining"))) (line D_UpstreamAreaMuni`distancia'KM date, sort lcolor(red) lpattern(solid) legend(lab(2 "Upstream mining"))), xtitle(Date) ytitle(Births) legend(order(1 2)) title("Births near and downstream from a mine - `label_dist' KM", size(vsmall))
	graph export "$latexcodes/PropBebes_`distancia'.pdf", as(pdf) replace
}
restore

**************************
**TABLE 6 - Panel B
**************************
*Need to clear results... thats why its here
eststo clear

local RHS_INDIVIDUO  ConsultasPreMayor4 Premature PartoHospital    EDAD_MADRE  MadreSoltera EduMadrePostPrimaria  
label var ConsultasPreMayor4 "Prenatal checkups $>$ 4"
label var Premature "Premature"
label var PartoHospital "In-hospital birth"
label var EDAD_MADRE "Mother's age"
label var MadreSoltera "Single Mother"
label var EduMadrePostPrimaria "At least secondary education (mother)"
foreach var_lhs in D_ExpAreaMuni20KM D_UpstreamAreaMuni20KM{

	foreach var in `RHS_INDIVIDUO'{
		eststo `var': quietly reghdfe `var' D_ExpAreaMuni20KM D_UpstreamAreaMuni20KM, absorb(i.codmpio Semana_Naci i.coddepto#c.ano i.ano) vce(cluster codmpio)
	}
	esttab `RHS_INDIVIDUO', se nostar keep(`var_lhs')
	matrix C = r(coefs)
	local rnames : rownames C
	local models : coleq C
	local models : list uniq models
	local i 0
	foreach name of local rnames {
		  local ++i
		  local j 0
		  capture matrix drop b
		  capture matrix drop se
		  foreach model of local models {
			  local ++j
			  matrix tmp = C[`i', 2*`j'-1]
			  if tmp[1,1]<.{
				 matrix colnames tmp = `model'
				 matrix b = nullmat(b), tmp
				 matrix tmp[1,1] = C[`i', 2*`j']
				 matrix se = nullmat(se), tmp
			 }
		 }
		 ereturn post b
		 quietly estadd matrix se
		 eststo `name'
	}
	eststo drop `RHS_INDIVIDUO'

}

esttab using "$latexcodes/Tab6_MotherCharac.tex", se nomtitles noobs replace fragment label

******************************************************************************
******************************************************************************
************************** STUFF DONE WITH MUNICIPALITY DATA *****************
******************************************************************************
******************************************************************************


use "$base_out/PanelMunicipal.dta", clear
qui gen coddepto=(codmpio-mod(codmpio,1000))/1000
foreach var of varlist D_*{
	di "`var'"
	replace `var'=1 if `var'>0 & !missing(`var')
}

tsset codmpio YrMonth, monthly
gen IMR=TMI
gen Fertility= fertilidad
gen Births= NBirths

local RHS_MUNICIPALITY IMR PNM Births Fertility 

**************************
**TABLE 6 - Panel A
**************************
	
	
eststo clear
foreach var_lhs in D_ExpAreaMuni20KM D_UpstreamAreaMuni20KM{

	foreach var in `RHS_MUNICIPALITY'{
		eststo `var': quietly reghdfe `var' D_ExpAreaMuni20KM D_UpstreamAreaMuni20KM [aw= NBirths], absorb(codmpio coddepto#c.YrMonth i.YrMonth) vce(cluster codmpio)
	}
	esttab `RHS_MUNICIPALITY', se nostar keep(`var_lhs')
	matrix C = r(coefs)
	local rnames : rownames C
	local models : coleq C
	local models : list uniq models
	local i 0
	foreach name of local rnames {
		  local ++i
		  local j 0
		  capture matrix drop b
		  capture matrix drop se
		  foreach model of local models {
			  local ++j
			  matrix tmp = C[`i', 2*`j'-1]
			  if tmp[1,1]<.{
				 matrix colnames tmp = `model'
				 matrix b = nullmat(b), tmp
				 matrix tmp[1,1] = C[`i', 2*`j']
				 matrix se = nullmat(se), tmp
			 }
		 }
		 ereturn post b
		 quietly estadd matrix se
		 eststo `name'
	}
	eststo drop `RHS_MUNICIPALITY'

}

esttab using "$latexcodes/Tab6_MuniCharac.tex", se nomtitles noobs replace fragment



eststo clear

**************************
**TABLE 5
**************************

foreach vardep of varlist y_total y_cap_regalias{
eststo fisc_`vardep': quietly reghdfe `vardep' D_ExpAreaMuni20KM D_UpstreamAreaMuni20KM, absorb( codmpio coddepto#c.YrMonth i.YrMonth) vce(cluster codmpio) 
}

estout fisc_y_total fisc_y_cap_regalias using "$latexcodes/Table5_FiscalMeassure.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_ExpAreaMuni20KM D_UpstreamAreaMuni20KM ) stats(N N_clust ymean r2, fmt(a2 a2 a2 a2) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "`=char(36)' R^2`=char(36)'")) replace




**************************
**FIGURES: EVENT STUDIES MAURO
**************************
use "$base_out/PanelMunicipal.dta", clear
qui gen coddepto=(codmpio-mod(codmpio,1000))/1000
foreach var of varlist D_*{
	di "`var'"
	replace `var'=1 if `var'>0 & !missing(`var')
}

bys codmpio: egen YearStrUpstream2=min(YrMonth) if D_UpstreamAreaMuni20KM==1
bys codmpio: egen YearStrNear2=min(YrMonth) if D_ExpAreaMuni20KM==1
bys codmpio: egen YearStrUpstream=min(YearStrUpstream2) 
bys codmpio: egen YearStrNear=min(YearStrNear2)
drop YearStrNear2 YearStrNear2


gen Tiempo_Near=YrMonth-YearStrNear
gen Tiempo_Upstream=YrMonth-YearStrUpstream

replace Tiempo_Near=-12*4 if Tiempo_Near<-12*4 & !missing(Tiempo_Near)
replace Tiempo_Near=12*4 if Tiempo_Near>12*4 & !missing(Tiempo_Near)

replace Tiempo_Upstream=-12*4 if Tiempo_Upstream<-12*4 & !missing(Tiempo_Upstream)
replace Tiempo_Upstream=12*4 if Tiempo_Upstream>12*4 & !missing(Tiempo_Upstream)
  
sum Tiempo_Near
scalar define min_near= r(min)
gen Tiempo_Near2=Tiempo_Near+(-r(min))+1
sum Tiempo_Upstream
scalar define min_upstream= r(min)
gen Tiempo_Upstream2=Tiempo_Upstream+(-r(min))+1

replace Tiempo_Near2=0 if Tiempo_Near2==.
replace Tiempo_Upstream2=0 if Tiempo_Upstream2==.

fvset base `=-1*scalar(min_near)' Tiempo_Near2
fvset base `=-1*scalar(min_upstream)' Tiempo_Upstream2
char Tiempo_Near2[omit] 0
char Tiempo_Upstream2[omit] 0

levelsof Tiempo_Near2, local(values_Tiempo_Near2)
foreach val of local values_Tiempo_Near2{ 
	label define l_Tiempo_Near2 `val' "`=`val'+`=scalar(min_near)'-1'",add modify
}
label define l_Tiempo_Near2 1 "<-4",add modify
sum Tiempo_Near2
label define l_Tiempo_Near2 `=r(max)' ">4",add modify
label values Tiempo_Near2  l_Tiempo_Near2

levelsof Tiempo_Upstream2, local(values_Tiempo_Upstream2)
foreach val of local values_Tiempo_Upstream2{ 
	label define l_Tiempo_Upstream2 `val' "`=`val'+`=scalar(min_upstream)'-1'",add modify
}
label define l_Tiempo_Upstream2 1 "<-4",add modify
sum Tiempo_Upstream2
label define l_Tiempo_Upstream2 `=r(max)' ">4",add modify
label values Tiempo_Upstream2  l_Tiempo_Upstream2


areg APGAR_BAJO i.Tiempo_Near2 D_UpstreamAreaMuni20KM coddepto#c.YrMonth i.YrMonth [aw= NBirths], absorb(codmpio ) vce(cluster codmpio)
coefplot, graphregion(color(white)) baselevels keep(*Tiempo_Near2) ci vertical yline(0) xline(`=-1*scalar(min_near)')
 graph export "$latexcodes/EventStudyyear_AreaProx_Detailed.pdf", replace
 
 
areg APGAR_BAJO i.Tiempo_Upstream2 D_ExpAreaMuni20KM coddepto#c.YrMonth i.YrMonth [aw= NBirths], absorb(codmpio ) vce(cluster codmpio)
coefplot, graphregion(color(white)) baselevels keep(*Tiempo_Upstream2) ci vertical yline(0) xline(`=-1*scalar(min_upstream)')
 graph export "$latexcodes/EventStudyyear_AreaRiver_Detailed.pdf", replace


 **************************
**FIGURES: EVENT STUDIES MAURO BY YEAR
**************************
use "$base_out/PanelMunicipal.dta", clear
qui gen coddepto=(codmpio-mod(codmpio,1000))/1000
foreach var of varlist D_*{
	di "`var'"
	replace `var'=1 if `var'>0 & !missing(`var')
}

bys codmpio: egen YearStrUpstream2=min(ano) if D_UpstreamAreaMuni20KM==1
bys codmpio: egen YearStrNear2=min(ano) if D_ExpAreaMuni20KM==1
bys codmpio: egen YearStrUpstream=min(YearStrUpstream2) 
bys codmpio: egen YearStrNear=min(YearStrNear2)
drop YearStrNear2 YearStrNear2


gen Tiempo_Near=ano-YearStrNear
gen Tiempo_Upstream=ano-YearStrUpstream

replace Tiempo_Near=-5 if Tiempo_Near<-5 & !missing(Tiempo_Near)
replace Tiempo_Near=5 if Tiempo_Near>5 & !missing(Tiempo_Near)

replace Tiempo_Upstream=-5 if Tiempo_Upstream<-5 & !missing(Tiempo_Upstream)
replace Tiempo_Upstream=5 if Tiempo_Upstream>5 & !missing(Tiempo_Upstream)
  
sum Tiempo_Near
scalar define min_near= r(min)
gen Tiempo_Near2=Tiempo_Near+(-r(min))+1
sum Tiempo_Upstream
scalar define min_upstream= r(min)
gen Tiempo_Upstream2=Tiempo_Upstream+(-r(min))+1

replace Tiempo_Near2=0 if Tiempo_Near2==.
replace Tiempo_Upstream2=0 if Tiempo_Upstream2==.

fvset base `=-1*scalar(min_near)' Tiempo_Near2
fvset base `=-1*scalar(min_upstream)' Tiempo_Upstream2
char Tiempo_Near2[omit] 0
char Tiempo_Upstream2[omit] 0

levelsof Tiempo_Near2, local(values_Tiempo_Near2)
foreach val of local values_Tiempo_Near2{ 
	label define l_Tiempo_Near2 `val' "`=`val'+`=scalar(min_near)'-1'",add modify
}
label define l_Tiempo_Near2 1 "<-4",add modify
sum Tiempo_Near2
label define l_Tiempo_Near2 `=r(max)' ">4",add modify
label values Tiempo_Near2  l_Tiempo_Near2

levelsof Tiempo_Upstream2, local(values_Tiempo_Upstream2)
foreach val of local values_Tiempo_Upstream2{ 
	label define l_Tiempo_Upstream2 `val' "`=`val'+`=scalar(min_upstream)'-1'",add modify
}
label define l_Tiempo_Upstream2 1 "<-4",add modify
sum Tiempo_Upstream2
label define l_Tiempo_Upstream2 `=r(max)' ">4",add modify
label values Tiempo_Upstream2  l_Tiempo_Upstream2


areg APGAR_BAJO i.Tiempo_Near2 D_UpstreamAreaMuni20KM coddepto#c.YrMonth i.YrMonth [aw= NBirths], absorb(codmpio ) vce(cluster codmpio)
coefplot, graphregion(color(white)) baselevels keep(*Tiempo_Near2) ci vertical yline(0) xline(`=-1*scalar(min_near)')  xtitle("Years to mining nearby") ytitle("Coefficent on APGAR") title("Event study for Low APGAR and nearby mining") 
 graph export "$latexcodes/EventStudyyear_AreaProx_Detailed_ano.pdf", replace
 
 
areg APGAR_BAJO i.Tiempo_Upstream2 D_ExpAreaMuni20KM  coddepto#c.YrMonth i.YrMonth [aw= NBirths], absorb(codmpio ) vce(cluster codmpio)
coefplot, graphregion(color(white)) baselevels keep(*Tiempo_Upstream2) ci vertical yline(0) xline(`=-1*scalar(min_upstream)') xtitle("Years to mining upstream") ytitle("Coefficent on APGAR") title("Event study for Low APGAR and upstream mining") 
 graph export "$latexcodes/EventStudyyear_AreaRiver_Detailed_ano.pdf", replace





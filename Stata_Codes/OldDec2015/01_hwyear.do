clear
import excel using "$base_in\Fechas_semana_santa.xlsx", first cellrange(A1:E24)
*Put easter sunday in Stata date format
gen hw_wdate=mdy(eastersunm,eastersund,ano)
*Calculate the friday before palm Sunday
gen hw_wfdate=hw_wdate-9
gen by_wdate=mdy(1,1,ano)
gen w_hweek=ceil((hw_wfdate-by_wdate)/7)
keep ano w_hweek
save "$base_out\Year_hw.dta", replace

use "$base_in\PANELES_CEDE\CARACTER�STICAS GENERALES\PANEL CARAC. GENERALES.dta", clear
keep if ano==2010
keep municipio depto codmpio
save "$base_out\Temporary\muni_names.dta", replace


import excel "$base_in\Mercurio_x_oro.xlsx", sheet("municipio") firstrow clear
merge 1:1 municipio depto using "C:\Users\santi\Copy\PYP_Birth\CreatedData\Temporary\muni_names.dta"
drop if _merge==2
replace codmpio=23466 if municipio=="Montel�bano"
replace codmpio=23682 if municipio=="San Jos� de Ur�"
replace codmpio=13600 if municipio =="R�o Viejo"

save "$base_out\Temporary\Mercurio_x_oro_CodDane.dta", replace

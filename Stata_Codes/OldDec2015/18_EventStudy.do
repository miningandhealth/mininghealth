use "$base_out\PanelMunicipalEEVV_Stata.dta",clear
drop MineriaArea_Ever MineriaProd_Ever Upstream_Ever Exposicion_Ever MineriaArea_DateStr MineriaProd_DateStr Upstream_DateStr Exposicion_DateStr
tsset codmpio YrQrt 
tsfill
global areapropcut=0.15
gen region=gandina+2*gcaribe+3*gpacifica+4*gorinoquia+5*gamazonia
drop if AreaMinadaProp>$areapropcut
replace APGAR_BAJO=100*APGAR_BAJO
keep if region<=3
keep if NearGoldMine==1
*Sumstat Babies
drop if APGAR_BAJO==.
drop if PESO_NAC==.
drop if TALLA_NAC==. 
drop if EDAD_MADRE==.
drop if MadreSoltera==.
drop if EduMadrePostPrimaria==.
*First determined date at which things happen for the first time
egen FirstArea=min(YrQrt)  if AreaMinadaKm2>0 & !missing(AreaMinadaKm2) , by(codmpio)
egen FirstProd=min(YrQrt)  if Produccion>0 & !missing(Produccion) , by(codmpio)
egen FirstExpo=min(YrQrt)  if ExposicionMuni>0 & !missing(ExposicionMuni) , by(codmpio)
egen FirstUpstream=min(YrQrt)  if Upstream>0 & !missing(Upstream) , by(codmpio)

format FirstArea %tq
format FirstProd %tq
format FirstExpo %tq
format FirstUpstream %tq

 
collapse (mean) FirstArea FirstProd FirstExpo FirstUpstream, by(codmpio)
save "$base_out\FirstDates.dta",replace

use "$base_out\PanelMunicipalEEVV_Stata.dta",clear
drop MineriaArea_Ever MineriaProd_Ever Upstream_Ever Exposicion_Ever MineriaArea_DateStr MineriaProd_DateStr Upstream_DateStr Exposicion_DateStr

merge m:1 codmpio using "$base_out\FirstDates.dta"
drop _merge
format FirstArea %tq
format FirstProd %tq
format FirstExpo %tq
format FirstUpstream %tq

tsset codmpio YrQrt 
tsfill

gen TimeToArea=YrQrt-FirstArea
gen TimeToProd=YrQrt-FirstProd
gen TimeToExpo=YrQrt-FirstExpo
gen TimeToUpst=YrQrt-FirstUpstream

global areapropcut=0.15
gen region=gandina+2*gcaribe+3*gpacifica+4*gorinoquia+5*gamazonia
drop if AreaMinadaProp>$areapropcut
replace APGAR_BAJO=100*APGAR_BAJO
keep if region<=3
keep if NearGoldMine==1
*Sumstat Babies
drop if APGAR_BAJO==.
drop if PESO_NAC==.
drop if TALLA_NAC==. 
drop if EDAD_MADRE==.
drop if MadreSoltera==.
drop if EduMadrePostPrimaria==.
label var APGAR_BAJO "Low APGAR"
label var PESO_NAC "Weight (gr)"
label var TALLA_NAC "Height (cm)"
replace PriceReal=PriceReal*2.36

 areg APGAR_BAJO D_AreaMinadaProp i.YrQrt region#c.YrQrt [pw=Nacimientos], absorb(codmpio) vce(cluster codmpio)
 areg APGAR_BAJO D_ExposicionMuni D_Upstream i.YrQrt region#c.YrQrt [pw=Nacimientos], absorb(codmpio) vce(cluster codmpio)
  areg APGAR_BAJO AreaMinadaProp i.YrQrt region#c.YrQrt [pw=Nacimientos], absorb(codmpio) vce(cluster codmpio)
 
 /*
gen TimeToArea2=TimeToArea
replace TimeToArea2=. if TimeToArea<(-12)
replace TimeToArea2=. if TimeToArea>12
replace TimeToArea2=TimeToArea2+13
replace TimeToArea2=0 if FirstArea==.

gen TimeToExpo2=TimeToExpo 
replace TimeToExpo2=. if TimeToExpo< -12
replace TimeToExpo2=. if TimeToExpo>12
replace TimeToExpo2=TimeToExpo2+13
replace TimeToExpo2=0 if FirstExpo==.

gen TimeToUpst2=TimeToUpst
replace TimeToUpst2=. if TimeToUpst< -12
replace TimeToUpst2=. if TimeToUpst>12
replace TimeToUpst2=TimeToUpst2+13
replace TimeToUpst2=0 if FirstUpstream==.

gen TimeToArea3=TimeToArea+55
replace TimeToArea3=0 if TimeToArea3==.

gen TimeToExpo3=TimeToExpo+55
replace TimeToExpo3=0 if TimeToExpo3==.

gen TimeToUpst3=TimeToUpst+50
replace TimeToUpst3=0 if TimeToUpst3==.
 */
 
 tab TimeToArea, gen(ITimeToArea)
 tab TimeToExpo, gen(ITimeToExpo)
 tab TimeToUpst, gen(ITimeToUpst) 
 
 
foreach var of varlist ITimeTo*{
 replace `var'=0 if `var'==.
}
 
 
 forvalues i=1/110{ 
  local a=`i'-55
  label var ITimeToArea`i' "`a'"
  label var ITimeToExpo`i' "`a'"
 }
  forvalues i=1/104{ 
  local a=`i'-49
  label var ITimeToUpst`i' "`a'"
 }
 
  
 
areg APGAR_BAJO ITimeToArea43-ITimeToArea53  ITimeToArea55 -ITimeToArea65 i.YrQrt region#c.YrQrt [pw=Nacimientos], absorb(codmpio) vce(cluster codmpio)
coefplot, baselevels keep(ITimeToArea43 ITimeToArea44 ITimeToArea45 ITimeToArea46 ITimeToArea47 ITimeToArea48 ITimeToArea49 ITimeToArea50 ITimeToArea51 ITimeToArea52 ITimeToArea53 ///
ITimeToArea55 ITimeToArea56 ITimeToArea57 ITimeToArea58 ITimeToArea59 ITimeToArea60 ITimeToArea61 ITimeToArea62 ITimeToArea63 ITimeToArea64 ITimeToArea65) ci levels(90) vertical yline(0) xtitle("Time to BP") ytitle("Coefficient") title("Event study for Mined Area")
    graph export "EventStudy_AREA.pdf", replace 

 
 areg APGAR_BAJO ITimeToExpo1- ITimeToExpo53 ITimeToExpo55- ITimeToExpo110 ITimeToUpst1- ITimeToUpst47 ITimeToUpst49- ITimeToUpst104 i.YrQrt region#c.YrQrt [pw=Nacimientos], absorb(codmpio) vce(cluster codmpio)
coefplot, baselevels keep(ITimeToExpo43 ITimeToExpo44 ITimeToExpo45 ITimeToExpo46 ITimeToExpo47 ITimeToExpo48 ITimeToExpo49 ITimeToExpo50 ITimeToExpo51 ITimeToExpo52 ITimeToExpo53 ///
ITimeToExpo55 ITimeToExpo56 ITimeToExpo57 ITimeToExpo58 ITimeToExpo59 ITimeToExpo60 ITimeToExpo61 ITimeToExpo62 ITimeToExpo63 ITimeToExpo64 ITimeToExpo65) ci  levels(90) vertical yline(0) xtitle("Time to BP") ytitle("Coefficient") title("Event study for proximity")
   graph export "EventStudy_EXPO.pdf", replace
 
coefplot, baselevels keep(ITimeToUpst37 ITimeToUpst38 ITimeToUpst39 ITimeToUpst40 ITimeToUpst41 ITimeToUpst42 ITimeToUpst43 ITimeToUpst44 ITimeToUpst45 ITimeToUpst46 ITimeToUpst47 ///
  ITimeToUpst49 ITimeToUpst50 ITimeToUpst51 ITimeToUpst52 ITimeToUpst53 ITimeToUpst54 ITimeToUpst55 ITimeToUpst56 ITimeToUpst57 ITimeToUpst58 ITimeToUpst59) ci levels(90)  vertical yline(0) xtitle("Time to BP") ytitle("Coefficient") title("Event study for upstream")
  graph export "EventStudy_UPST.pdf", replace
 

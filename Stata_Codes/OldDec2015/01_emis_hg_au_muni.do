import delimited "$base_out/PanelProduccion.csv", clear 
rename codigodane codmpio
collapse (sum) produccion, by(codmpio)
drop if produccion==0
save "$base_out/Temporary/muni_oro.dta", replace

import excel "$base_in/Mercurio_x_oro.xlsx", sheet("depto") firstrow clear
rename departamento depto
save "$base_out/Temporary/depto_Hg.dta", replace
import excel "$basein/Mercurio_x_oro.xlsx", sheet("municipio") firstrow clear
merge m:1 depto using "$base_out/Temporary/depto_Hg.dta"
drop _merge
merge 1:1 depto municipio using "$base_out/Temporary/muni_names.dta"
drop if _merge==2
drop _merge
replace codmpio=23682 if municipio=="Río Viejo"
replace codmpio=13600 if municipio=="San José de Uré"
replace codmpio=23466 if municipio=="Montelíbano"
gen emis_gHg_x_gAu_filon= emis_gHg_x_gAu_filon_abierto* pct_filon_abierto+ emis_gHg_x_gAu_filon_cerrado* pct_filon_cerrado
gen emis_gHg_x_gAu_aluvion= emis_gHg_x_gAu_aluvion_abierto* pct_aluvion_abierto+ emis_gHg_x_gAu_aluvion_cerrado* pct_aluvion_cerrado
replace emis_gHg_x_gAu_aluvion= emis_gHg_x_gAu_aluvion_abierto* pct_aluvion_abierto if pct_aluvion_cerrado==0
gen emis_gHg_x_gAu=emis_gHg_x_gAu_filon*pct_oro_filon/100+emis_gHg_x_gAu_aluvion*pct_oro_aluvion/100
replace emis_gHg_x_gAu=emis_gHg_x_gAu_filon if emis_gHg_x_gAu_aluvion==.
replace emis_gHg_x_gAu=emis_gHg_x_gAu_aluvion if emis_gHg_x_gAu_filon==.

merge 1:1 codmpio using "$base_out/Temporary/muni_oro.dta"
gen coddepto=(codmpio-mod(codmpio,1000))/1000
bysort coddepto: egen mean_Hg_Au_depto=mean( emis_gHg_x_gAu)
replace emis_gHg_x_gAu= mean_Hg_Au_depto if emis_gHg_x_gAu==.

keep codmpio emis_gHg_x_gAu
drop if emis_gHg_x_gAu==.

export delimited using "$base_out/emis_Hg_Au_muni.csv", replace

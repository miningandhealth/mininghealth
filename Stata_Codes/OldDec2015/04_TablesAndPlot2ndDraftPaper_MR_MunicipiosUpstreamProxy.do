clear all
global latexcodes     "$gitpath\LaTeX\201510_2ndDraft"

use "$base_out\DataCompleta_Stata.dta", clear


gen D_ProxAreaMuni10KM2=(D_ProxAreaMuni10KM==1 & D_ProxAreaMuni5KM==0)
gen D_ProxAreaMuni20KM2=(D_ProxAreaMuni20KM==1 & D_ProxAreaMuni10KM==0)
gen D_RiverAreaMuni10KM2=(D_RiverAreaMuni10KM==1 & D_RiverAreaMuni5KM==0)
gen D_RiverAreaMuni20KM2=(D_RiverAreaMuni20KM==1 & D_RiverAreaMuni10KM==0)


gen D_ProxProdAIMuni10KM2=(D_ProxProdAIMuni10KM==1 & D_ProxProdAIMuni5KM==0)
gen D_ProxProdAIMuni20KM2=(D_ProxProdAIMuni20KM==1 & D_ProxProdAIMuni10KM==0)
gen D_RiverProdAIMuni10KM2=(D_RiverProdAIMuni10KM==1 & D_RiverProdAIMuni5KM==0)
gen D_RiverProdAIMuni20KM2=(D_RiverProdAIMuni20KM==1 & D_RiverProdAIMuni10KM==0)

foreach distancia in 10 20{
foreach medida in Area ProdAI{
drop D_Prox`medida'Muni`distancia'KM 
drop D_River`medida'Muni`distancia'KM 
gen D_Prox`medida'Muni`distancia'KM=D_Prox`medida'Muni`distancia'KM2
gen D_River`medida'Muni`distancia'KM=D_River`medida'Muni`distancia'KM2
}
}

keep D_ProxAreaMuni10KM D_RiverAreaMuni10KM D_ProxProdAIMuni10KM D_RiverProdAIMuni10KM D_ProxAreaMuni20KM D_RiverAreaMuni20KM D_ProxProdAIMuni20KM D_RiverProdAIMuni20KM D_ProxAreaMuni5KM D_ProxProdAIMuni5KM D_RiverAreaMuni5KM D_RiverProdAIMuni5KM ano MES codmpio


collapse (max) D_ProxAreaMuni5KM- D_RiverProdAIMuni20KM, by(ano MES codmpio)
collapse (sum)  D_ProxAreaMuni5KM- D_RiverProdAIMuni20KM, by(ano MES)
foreach var of varlist D_ProxAreaMuni5KM- D_RiverProdAIMuni20KM{
	replace `var'=. if `var'==0
}

gen date=ym(ano,MES)
format date %tm

foreach distancia in 5 10 20{
foreach medida in Area ProdAI{
if (`distancia'==5) local label_dist "0-5"
if (`distancia'==10) local label_dist "5-10"
if (`distancia'==20) local label_dist "10-20"
 twoway (line D_Prox`medida'Muni`distancia'KM date, sort lcolor(blue) lpattern(longdash) legend(lab(1 "Proximity"))) (line D_River`medida'Muni`distancia'KM date, sort lcolor(red) lpattern(solid) legend(lab(2 "Upstream"))), xtitle(Date) ytitle(Municipalities) legend(order(1 2)) title("Municipalities near and upstream from a mine - `label_dist' KM", size(vsmall))
graph export "$latexcodes\Municipalities_`medida'_`distancia'.pdf", as(pdf) replace
}
}

use "$base_out\DataCompleta_Stata.dta", clear

gen D_ProxAreaMuni10KM2=(D_ProxAreaMuni10KM==1 & D_ProxAreaMuni5KM==0)
gen D_ProxAreaMuni20KM2=(D_ProxAreaMuni20KM==1 & D_ProxAreaMuni10KM==0)
gen D_RiverAreaMuni10KM2=(D_RiverAreaMuni10KM==1 & D_RiverAreaMuni5KM==0)
gen D_RiverAreaMuni20KM2=(D_RiverAreaMuni20KM==1 & D_RiverAreaMuni10KM==0)


gen D_ProxProdAIMuni10KM2=(D_ProxProdAIMuni10KM==1 & D_ProxProdAIMuni5KM==0)
gen D_ProxProdAIMuni20KM2=(D_ProxProdAIMuni20KM==1 & D_ProxProdAIMuni10KM==0)
gen D_RiverProdAIMuni10KM2=(D_RiverProdAIMuni10KM==1 & D_RiverProdAIMuni5KM==0)
gen D_RiverProdAIMuni20KM2=(D_RiverProdAIMuni20KM==1 & D_RiverProdAIMuni10KM==0)

foreach distancia in 10 20{
foreach medida in Area ProdAI{
drop D_Prox`medida'Muni`distancia'KM 
drop D_River`medida'Muni`distancia'KM 
gen D_Prox`medida'Muni`distancia'KM=D_Prox`medida'Muni`distancia'KM2
gen D_River`medida'Muni`distancia'KM=D_River`medida'Muni`distancia'KM2
}
}

keep D_ProxAreaMuni10KM D_RiverAreaMuni10KM D_ProxProdAIMuni10KM D_RiverProdAIMuni10KM D_ProxAreaMuni20KM D_RiverAreaMuni20KM D_ProxProdAIMuni20KM D_RiverProdAIMuni20KM D_ProxAreaMuni5KM D_ProxProdAIMuni5KM D_RiverAreaMuni5KM D_RiverProdAIMuni5KM ano MES codmpio

collapse (max) D_ProxAreaMuni5KM- D_RiverProdAIMuni20KM, by(ano MES codmpio)
collapse (mean)  D_ProxAreaMuni5KM- D_RiverProdAIMuni20KM, by(ano MES)
gen date=ym(ano,MES)
format date %tm
foreach var of varlist D_ProxAreaMuni5KM- D_RiverProdAIMuni20KM{
	replace `var'=. if `var'==0
}

foreach distancia in 5 10 20{
foreach medida in Area ProdAI{
if (`distancia'==5) local label_dist "0-5"
if (`distancia'==10) local label_dist "5-10"
if (`distancia'==20) local label_dist "10-20"
 twoway (line D_Prox`medida'Muni`distancia'KM date, sort lcolor(blue) lpattern(longdash) legend(lab(1 "Proximity"))) (line D_River`medida'Muni`distancia'KM date, sort lcolor(red) lpattern(solid) legend(lab(2 "Upstream"))), xtitle(Date) ytitle(Municipalities) legend(order(1 2)) title("Municipalities near and upstream from a mine - `label_dist' KM", size(vsmall))
graph export "$latexcodes\PropMunicipalities_`medida'_`distancia'.pdf", as(pdf) replace
}
}



use "$base_out\DataCompleta_Stata.dta", clear


gen D_ProxAreaMuni10KM2=(D_ProxAreaMuni10KM==1 & D_ProxAreaMuni5KM==0)
gen D_ProxAreaMuni20KM2=(D_ProxAreaMuni20KM==1 & D_ProxAreaMuni10KM==0)
gen D_RiverAreaMuni10KM2=(D_RiverAreaMuni10KM==1 & D_RiverAreaMuni5KM==0)
gen D_RiverAreaMuni20KM2=(D_RiverAreaMuni20KM==1 & D_RiverAreaMuni10KM==0)


gen D_ProxProdAIMuni10KM2=(D_ProxProdAIMuni10KM==1 & D_ProxProdAIMuni5KM==0)
gen D_ProxProdAIMuni20KM2=(D_ProxProdAIMuni20KM==1 & D_ProxProdAIMuni10KM==0)
gen D_RiverProdAIMuni10KM2=(D_RiverProdAIMuni10KM==1 & D_RiverProdAIMuni5KM==0)
gen D_RiverProdAIMuni20KM2=(D_RiverProdAIMuni20KM==1 & D_RiverProdAIMuni10KM==0)

foreach distancia in 10 20{
foreach medida in Area ProdAI{
drop D_Prox`medida'Muni`distancia'KM 
drop D_River`medida'Muni`distancia'KM 
gen D_Prox`medida'Muni`distancia'KM=D_Prox`medida'Muni`distancia'KM2
gen D_River`medida'Muni`distancia'KM=D_River`medida'Muni`distancia'KM2
}
}

keep D_ProxAreaMuni10KM D_RiverAreaMuni10KM D_ProxProdAIMuni10KM D_RiverProdAIMuni10KM D_ProxAreaMuni20KM D_RiverAreaMuni20KM D_ProxProdAIMuni20KM D_RiverProdAIMuni20KM D_ProxAreaMuni5KM D_ProxProdAIMuni5KM D_RiverAreaMuni5KM D_RiverProdAIMuni5KM ano MES codmpio

collapse (sum)  D_ProxAreaMuni5KM- D_RiverProdAIMuni20KM, by(ano MES)
gen date=ym(ano,MES)
format date %tm
foreach var of varlist D_ProxAreaMuni5KM- D_RiverProdAIMuni20KM{
	replace `var'=. if `var'==0
}
foreach distancia in 5 10 20{
foreach medida in Area ProdAI{
if (`distancia'==5) local label_dist "0-5"
if (`distancia'==10) local label_dist "5-10"
if (`distancia'==20) local label_dist "10-20"
 twoway (line D_Prox`medida'Muni`distancia'KM date, sort lcolor(blue) lpattern(longdash) legend(lab(1 "Proximity"))) (line D_River`medida'Muni`distancia'KM date, sort lcolor(red) lpattern(solid) legend(lab(2 "Upstream"))), xtitle(Date) ytitle(Births) legend(order(1 2)) title("Births near and upstream from a mine - `label_dist' KM", size(vsmall))
graph export "$latexcodes\Bebes_`medida'_`distancia'.pdf", as(pdf) replace
}
}


use "$base_out\DataCompleta_Stata.dta", clear


gen D_ProxAreaMuni10KM2=(D_ProxAreaMuni10KM==1 & D_ProxAreaMuni5KM==0)
gen D_ProxAreaMuni20KM2=(D_ProxAreaMuni20KM==1 & D_ProxAreaMuni10KM==0)
gen D_RiverAreaMuni10KM2=(D_RiverAreaMuni10KM==1 & D_RiverAreaMuni5KM==0)
gen D_RiverAreaMuni20KM2=(D_RiverAreaMuni20KM==1 & D_RiverAreaMuni10KM==0)


gen D_ProxProdAIMuni10KM2=(D_ProxProdAIMuni10KM==1 & D_ProxProdAIMuni5KM==0)
gen D_ProxProdAIMuni20KM2=(D_ProxProdAIMuni20KM==1 & D_ProxProdAIMuni10KM==0)
gen D_RiverProdAIMuni10KM2=(D_RiverProdAIMuni10KM==1 & D_RiverProdAIMuni5KM==0)
gen D_RiverProdAIMuni20KM2=(D_RiverProdAIMuni20KM==1 & D_RiverProdAIMuni10KM==0)

foreach distancia in 10 20{
foreach medida in Area ProdAI{
drop D_Prox`medida'Muni`distancia'KM 
drop D_River`medida'Muni`distancia'KM 
gen D_Prox`medida'Muni`distancia'KM=D_Prox`medida'Muni`distancia'KM2
gen D_River`medida'Muni`distancia'KM=D_River`medida'Muni`distancia'KM2
}
}

keep D_ProxAreaMuni10KM D_RiverAreaMuni10KM D_ProxProdAIMuni10KM D_RiverProdAIMuni10KM D_ProxAreaMuni20KM D_RiverAreaMuni20KM D_ProxProdAIMuni20KM D_RiverProdAIMuni20KM D_ProxAreaMuni5KM D_ProxProdAIMuni5KM D_RiverAreaMuni5KM D_RiverProdAIMuni5KM ano MES codmpio

collapse (mean)  D_ProxAreaMuni5KM- D_RiverProdAIMuni20KM, by(ano MES)
gen date=ym(ano,MES)
format date %tm
foreach var of varlist D_ProxAreaMuni5KM- D_RiverProdAIMuni20KM{
	replace `var'=. if `var'==0
}
foreach distancia in 5 10 20{
foreach medida in Area ProdAI{
if (`distancia'==5) local label_dist "0-5"
if (`distancia'==10) local label_dist "5-10"
if (`distancia'==20) local label_dist "10-20"


 twoway (line D_Prox`medida'Muni`distancia'KM date, sort lcolor(blue) lpattern(longdash) legend(lab(1 "Proximity"))) (line D_River`medida'Muni`distancia'KM date, sort lcolor(red) lpattern(solid) legend(lab(2 "Upstream"))), xtitle(Date) ytitle(Births) legend(order(1 2)) title("Births near and upstream from a mine - `label_dist' KM", size(vsmall))
graph export "$latexcodes\PropBebes_`medida'_`distancia'.pdf", as(pdf) replace
}
}

	
tsset codmpio YrQrt 

sort codmpio YrQrt 
by codmpio: egen astartm=min(YrQrt ) if D_AreaMinadaProp==1
by codmpio: egen astartm2=min(astartm )
gen tiempo_mineria=YrQrt -astartm


xi: areg APGAR_BAJO i.tiempo_mineria i.ano i.Quarter region#c.YrQrt [aw= APGAR_BAJO_Count], absorb(codmpio) vce(cluster codmpio)
	
	
	
	xi: areg APGAR_BAJO D_AreaMinadaProp i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE region#c.FECHA_NAC , absorb(codmpio) vce(cluster codmpio)
	
	
	xi: xtreg quit_after_passenger_nowait 
	 bin_UNDER_10 bin_UNDER_9 bin_UNDER_8 bin_UNDER_7 bin_UNDER_6 bin_UNDER_5 bin_UNDER_4 bin_UNDER_3 bin_UNDER_2 bin_UNDER_1
	 bin_OVER_1 bin_OVER_2 bin_OVER_3 bin_OVER_4 bin_OVER_5 bin_OVER_6 bin_OVER_7 bin_OVER_8 bin_OVER_9 bin_OVER_10
	 `time`k'' need_amt i.day_of_week i.week ${spec`i'}, i(boda_id) cluster(boda_id) fe;
	sum quit_after_passenger_nowait if e(sample);
	local mean=r(mean);
	test bin_OVER_1-bin_UNDER_1=bin_UNDER_1-bin_UNDER_2;
	local p=r(p);
	quietly outreg bin_UNDER_10 bin_UNDER_9 bin_UNDER_8 bin_UNDER_7 bin_UNDER_6 bin_UNDER_5 bin_UNDER_4 bin_UNDER_3 bin_UNDER_2 bin_UNDER_1
	 bin_OVER_1 bin_OVER_2 bin_OVER_3 bin_OVER_4 bin_OVER_5 bin_OVER_6 bin_OVER_7 bin_OVER_8 bin_OVER_9 bin_OVER_10 
	 `time`k'' using "results\sept2012\hazard.out", nonote se sigsymb(***,**,*) `append_replace' nolabel bdec(2)
	 addstat("mean", `mean', "p-value for jump at need", `p', "spec, 1-all, 2-need<50, 3-50<need<100, 4-100<need<200, 5-need>200, 6-running time<median hours, 
	  7-running time<25th ptile hours", `i') adec(2);

	quietly for any coef lower_bound upper_bound: gen X=.;
	quietly gen n=_n;

	di "${coef${make_graph`i'}}";
	sum quit_after_passenger_nowait if e(sample) & ${coef${make_graph`i'}}==1;
	local add=r(mean);

	local j ${make_graph`i'};
	while `j' <= 20 {;
		quietly lincom ${coef`j'}-${coef${make_graph`i'}};
		quietly replace coef=r(estimate) +`add' if _n==`j';
		quietly replace coef=`add' if _n==`j' & (`i'!=2 | `i'!=3) & `j'==1;
		quietly replace coef=`add' if _n==`j' & (`i'==2 ) & `j'==9;
		quietly replace coef=`add' if _n==`j' & (`i'==3 ) & `j'==6;
		quietly replace upper_bound=coef+1.96*r(se) if _n==`j';
		quietly replace lower_bound=coef-1.96*r(se) if _n==`j';
		local j = `j' + 1;
		};

		graph twoway 
		(scatter coef n if _n<=20, connect(l))
		(rcapsym lower_bound upper_bound n if _n<=20, msymbol("-")), 
		ytitle("Pr(quitting)") xtitle("Ksh from need") xline(10.5)
		xlabel(1 "-200" 3 "-160" 5 "-120" 7 "-80" 9 "-40" 11 "0" 13 "40" 15 "80" 17 "120"  19 "160")
	 	ylabel(${min`i'} (${label_inc`i'}) ${max`i'})
		ytick(${min`i'} (${tick_inc`i'}) ${max`i'})
		legend(rows(2) order(1 "Coefficient" 2 "95% CI")) 
		title("${title_group`i'}", size(medium)) saving("results\sept2012\figure1_`i'`substring`k''", replace);

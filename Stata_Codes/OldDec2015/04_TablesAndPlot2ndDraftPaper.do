


global latexcodes     "$gitpath/LaTeX/201510_2ndDraft"
set matsize 5000
use "$base_out/DataCompleta_Stata.dta", clear
*sample 5
*save "$base_out/DataTest.dta", replace
*use "$base_out/DataTest.dta", clear

keep PartoHospital PESO_NAC TALLA_NAC ConsultasPreMayor4 RegimenContributivo RegimenSubsidiado PriceRealMA STUNT ano MES region ///
D_AreaMinadaProp* D_ProduccionPerArea* D_Prox* D_River*  ///
AreaMinadaProp* ProduccionPerArea* ProduccionAccPerArea* Prox* River*  ///
Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE APGAR_BAJO LBW codmpio COD_INST FECHA_NAC STUNT
qui gen coddepto=(codmpio-mod(codmpio,1000))/1000
eststo clear
local control1 i.ano i.Semana_Naci 
local control2 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
local control3 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE coddepto#c.ano



**
**TABLE 1 Sum stats birth
**
estpost tabstat APGAR_BAJO PartoHospital PESO_NAC LBW TALLA_NAC STUNT EDAD_MADRE MadreSoltera EduMadrePostPrimaria, statistics(mean median sd max min count) columns(statistics)
esttab using "$latexcodes/SummaryBirth.tex", cells("mean(fmt(a2) label(Mean)) p50(fmt(a2) label(Median)) sd(fmt(a2) label(Std. Dev.)) min(fmt(a2) label(Min)) max(fmt(a2) label(Max)) count(fmt(a2) label(N))") nomtitle nonumber label  replace noobs booktabs






*Sumstat AreaMinada
gen AreaMinadaPropPositiva=AreaMinadaProp if AreaMinadaProp>0 & !missing(AreaMinadaProp)
eststo clear
estpost tabstat AreaMinadaProp D_AreaMinadaProp AreaMinadaPropPositiva ProduccionAccPerArea , statistics(mean median sd max min count) columns(statistics)
esttab using "$latexcodes/SummaryMines.tex", cells("mean(fmt(a2) label(Mean)) p50(fmt(a2) label(Median)) sd(fmt(a2) label(Std. Dev.)) min(fmt(a2) label(Min)) max(fmt(a2) label(Max)) count(fmt(a2) label(N))") nomtitle nonumber label  replace noobs booktabs








 

*Linear regressions area-production
foreach var_indep of varlist AreaMinadaProp {

eststo clear
local control1 i.ano i.Semana_Naci 
local control2 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
local control3 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE coddepto#c.ano
local control4 i.ano i.Semana_Naci coddepto#c.ano

eststo m1: quietly xi: areg APGAR_BAJO `var_indep' `control1' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm


eststo m2: quietly xi: areg APGAR_BAJO `var_indep' `control2' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm


 eststo m3: quietly xi: areg APGAR_BAJO `var_indep' `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

estout m1 m2 m3 using "$latexcodes/IndividualMeasure_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(`var_indep')  stats( r2 , fmt( a2 ) labels ( "/$R^2/$" )) replace


 eststo m4: quietly xi: areg APGAR_BAJO `var_indep'xprice `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

estout m3 m4 using "$latexcodes/IndividualMeasure_`var_indep'_price.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(`var_indep'* )  stats( r2 , fmt( a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "/$R^2/$" )) replace

eststo m5: quietly xi: areg LBW `var_indep' `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m6: quietly xi: areg STUNT `var_indep' `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
**
**TABLE2 
**
estout m3 m5 m6 using "$latexcodes/IndividualMeasure_`var_indep'_lbwzh.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(`var_indep'* )  stats( r2 , fmt(a2 ) labels ( "/$R^2/$" )) replace



 eststo mh: quietly xi: areg STUNT `var_indep' `var_indep'2 `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local timefe "Yes"
estadd local momc "Yes"
estadd local rtrend "Yes"
estout m3 mh using "APGAR_height_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(`var_indep' `var_indep'2 )  stats(N ymean r2 timefe momc rtrend, fmt(a2 a2 a2 %~#s %~#s %~#s) labels ("N. of obs." "Mean of Dep. Var." "/$R^2/$" "Time FE" "Ind. Controls" "Reg. Trends")) replace


}

foreach var_indep of varlist ProduccionPerArea ProduccionAccPerArea{

eststo clear
local control1 i.ano i.Semana_Naci 
local control2 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
local control3 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE coddepto#c.ano
local control4 i.ano i.Semana_Naci coddepto#c.ano

eststo m1: quietly xi: areg APGAR_BAJO `var_indep' `control1' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm


eststo m2: quietly xi: areg APGAR_BAJO `var_indep' `control2' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm


 eststo m3: quietly xi: areg APGAR_BAJO `var_indep' `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

estout m1 m2 m3 using "$latexcodes/IndividualMeasure_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(`var_indep')  stats( r2 , fmt( a2 ) labels ( "/$R^2/$" )) replace



eststo m5: quietly xi: areg LBW `var_indep' `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m6: quietly xi: areg STUNT `var_indep' `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
**
**TABLE2 
**
estout m3 m5 m6 using "$latexcodes/IndividualMeasure_`var_indep'_lbwzh.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(`var_indep'* )  stats( r2 , fmt(a2 ) labels ( "/$R^2/$" )) replace



 eststo mh: quietly xi: areg STUNT `var_indep' `var_indep'2 `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local timefe "Yes"
estadd local momc "Yes"
estadd local rtrend "Yes"
estout m3 mh using "APGAR_height_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(`var_indep' `var_indep'2 )  stats(N ymean r2 timefe momc rtrend, fmt(a2 a2 a2 %~#s %~#s %~#s) labels ("N. of obs." "Mean of Dep. Var." "/$R^2/$" "Time FE" "Ind. Controls" "Reg. Trends")) replace


}



*Dummy regressions Area/Prod
local var_indepList AreaMinadaProp 
foreach var_indep of varlist `var_indepList'{
eststo clear
local control1 i.ano i.Semana_Naci 
local control2 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
local control3 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE coddepto#c.ano
local control4 i.ano i.Semana_Naci coddepto#c.ano

eststo m1: quietly xi: areg APGAR_BAJO D_`var_indep' `control1' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local momc "No"
estadd local rtrend "No"



eststo m2: quietly xi: areg APGAR_BAJO D_`var_indep' `control2' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local momc "Yes"
estadd local rtrend "No"

eststo m3: quietly xi: areg APGAR_BAJO D_`var_indep' `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local momc "Yes"
estadd local rtrend "Yes"

eststo m4: quietly xi: areg APGAR_BAJO D_`var_indep'xprice `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local momc "Yes"
estadd local rtrend "Yes"
estout m1 m2 m3 using "$latexcodes/IndividualDummies_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( D_`var_indep'* )  stats(N N_clust ymean r2 munife timefe momc rtrend, fmt(a2 a2 a2 a2 %~#s %~#s %~#s %~#s) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "/$R^2/$" "Municipality FE" "Time FE" "Ind. Controls" "Reg. Trends")) replace
*note("/specialcell{Clustered standard errors, by municipality, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") ///

estout m3 m4 using "$latexcodes/IndividualDummies_`var_indep'_price.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( D_`var_indep'* )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "/$R^2/$" )) replace

eststo m5: quietly xi: areg LBW D_`var_indep' `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m6: quietly xi: areg STUNT D_`var_indep' `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estout m3 m5 m6 using "$latexcodes/IndividualDummies_`var_indep'_lbwzh.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( D_`var_indep' )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "/$R^2/$" "Municipality FE" "Time FE" "Ind. Controls" "Reg. Trends")) replace
*note("/specialcell{Clustered standard errors, by municipality, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") ///
 
}


***********
*Proximity and river 
*****************


eststo clear

local control1 i.ano i.Semana_Naci 
local control2 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
local control3 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE coddepto#c.ano
local control4 i.ano i.Semana_Naci coddepto#c.ano

eststo m1: quietly xi: areg APGAR_BAJO ProxAreaMuni20KM RiverAreaMuni20KM  `control3' , absorb(codmpio) vce(cluster codmpio)

eststo m2: quietly xi: areg LBW ProxAreaMuni20KM RiverAreaMuni20KM  `control3' , absorb(codmpio) vce(cluster codmpio)

eststo m3: quietly xi: areg STUNT ProxAreaMuni20KM RiverAreaMuni20KM  `control3' , absorb(codmpio) vce(cluster codmpio)

**
**APPENDIX TABLE 11 PANEL A
**
estout m1 m2 m3 using "$latexcodes/IndividualMeasure_Combo.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(Prox* River* ) stats( r2 , fmt( a2 ) labels ( "/$R^2/$" ))  replace


*Dummy proximity-river
*gen riverxproximity=D_ProxAreaMuni10KM*D_RiverAreaMuni10KM

foreach var_indep of varlist ProxAreaMuni20KM RiverAreaMuni20KM {

qui gen D_`var_indep'xprice=D_`var_indep'*PriceRealMA
}


label var D_ProxAreaMuni20KMxprice "Proximity exposure area muni 20 km /$>0/$ x Price"
label var D_RiverAreaMuni20KMxprice "River exposure area muni 20 km /$>0/$ x Price"
*label var riverxproximity "River /$/times/$ proximity"

eststo clear
local control1 i.ano i.Semana_Naci 
local control2 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
local control3 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE coddepto#c.ano
local control4 i.ano i.Semana_Naci coddepto#c.ano

eststo m1: quietly xi: areg APGAR_BAJO D_ProxAreaMuni20KM D_RiverAreaMuni20KM `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm




eststo m2: quietly xi: areg LBW D_ProxAreaMuni20KM D_RiverAreaMuni20KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm


eststo m3: quietly xi: areg STUNT D_ProxAreaMuni20KM D_RiverAreaMuni20KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm


**
**APPENDIX TABLE 11 PANEL B
**
estout m1 m2 m3 using "$latexcodes/IndividualDummies_Combo.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_Prox* D_River* )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "/$R^2/$" )) replace
*note("/specialcell{Clustered standard errors, by municipality, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") ///

eststo m4: quietly xi: areg APGAR_BAJO D_ProxAreaMuni20KMxprice D_RiverAreaMuni20KMxprice  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

estout m1 m4 using "$latexcodes/IndividualDummies_Combo_price.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_Prox* D_River*  )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "/$R^2/$" )) replace
*note("/specialcell{Clustered standard errors, by municipality, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") ///

eststo m5: quietly xi: areg LBW D_ProxAreaMuni5KM D_RiverAreaMuni20KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local momc "Yes"
estadd local rtrend "Yes"

eststo m6: quietly xi: areg STUNT D_ProxAreaMuni5KM D_RiverAreaMuni20KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local momc "Yes"
estadd local rtrend "Yes"
estout m3 m5 m6 using "$latexcodes/IndividualDummies_Combo_lbwzh.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_Prox* D_River* )  stats(N N_clust ymean r2 munife timefe momc rtrend, fmt(a2 a2 a2 a2 %~#s %~#s %~#s %~#s) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "/$R^2/$" "Municipality FE" "Time FE" "Ind. Controls" "Reg. Trends")) replace

**Trace regression 
eststo clear
local control3 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE region#c.FECHA_NAC
qui eststo m1: xi: areg APGAR_BAJO D_ProxAreaMuni5KM D_ProxAreaMuni510KM D_ProxAreaMuni1020KM /// 
                              D_RiverAreaMuni5KM D_RiverAreaMuni510KM D_RiverAreaMuni1020KM  `control3' , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m2: xi: areg LBW D_ProxAreaMuni5KM D_ProxAreaMuni510KM D_ProxAreaMuni1020KM /// 
                              D_RiverAreaMuni5KM D_RiverAreaMuni510KM D_RiverAreaMuni1020KM  `control3' , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui eststo m3: xi: areg STUNT D_ProxAreaMuni5KM D_ProxAreaMuni510KM D_ProxAreaMuni1020KM /// 
                              D_RiverAreaMuni5KM D_RiverAreaMuni510KM D_RiverAreaMuni1020KM  `control3' , absorb(codmpio) vce(cluster codmpio)
qui estadd ysumm

qui estout m1 m2 m3 using "$latexcodes/IndividualDummies_ComboMuni_Trace.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_Prox* D_River* )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "/$R^2/$" )) replace

local var_indepList AreaMinadaProp
foreach var_indep of varlist `var_indepList'{
eststo clear
local control i.ano i.Semana_Naci coddepto#c.ano
eststo clear
eststo m1: quietly xi: areg MadreSoltera D_`var_indep' `control'  , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m2: quietly xi: areg EduMadrePostPrimaria D_`var_indep' `control'  , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m3: quietly xi: areg EDAD_MADRE D_`var_indep' `control'  , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estout m1 m2 m3 using "$latexcodes/IndividualDummies_`var_indep'_momchar.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( D_`var_indep' )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "/$R^2/$" )) replace
}

**Individual measures combo hospital robustness 
/*
*** Apendix proximity river

*Robustness measure combo

eststo clear
local control3 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE region#c.FECHA_NAC


eststo m1: quietly xi: areg APGAR_BAJO ProxAreaMuni5KM RiverAreaMuni5KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m2: quietly xi: areg APGAR_BAJO ProxAreaMuni10KM RiverAreaMuni10KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m3: quietly xi: areg APGAR_BAJO ProxAreaMuni20KM RiverAreaMuni20KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m4: quietly xi: areg APGAR_BAJO ProxProdMuni5KM RiverProdMuni5KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m5: quietly xi: areg APGAR_BAJO ProxProdMuni10KM RiverProdMuni10KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m6: quietly xi: areg APGAR_BAJO ProxProdMuni20KM RiverProdMuni20KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

estout m1 m2 m3 m4 m5 m6 using "$latexcodes/IndividualMeasure_ComboMuniRobust.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(Prox* River* )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "/$R^2/$" )) replace

eststo m7: quietly xi: areg APGAR_BAJO ProxProdAIMuni5KM RiverProdAIMuni5KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m8: quietly xi: areg APGAR_BAJO ProxProdAIMuni10KM RiverProdAIMuni10KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m9: quietly xi: areg APGAR_BAJO ProxProdAIMuni20KM RiverProdAIMuni20KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

estout m4 m5 m6 m7 m8 m9 using "$latexcodes/IndividualMeasure_ComboMuniRobustAI.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(Prox* River* )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "/$R^2/$" )) replace


*Robustness measure Dummies combo

eststo clear
local control3 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE region#c.FECHA_NAC

eststo m1: quietly xi: areg APGAR_BAJO D_ProxAreaMuni5KM D_RiverAreaMuni5KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m2: quietly xi: areg APGAR_BAJO D_ProxAreaMuni10KM D_RiverAreaMuni10KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m3: quietly xi: areg APGAR_BAJO D_ProxAreaMuni20KM D_RiverAreaMuni20KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m4: quietly xi: areg APGAR_BAJO D_ProxProdMuni5KM D_RiverProdMuni5KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m5: quietly xi: areg APGAR_BAJO D_ProxProdMuni10KM D_RiverProdMuni10KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m6: quietly xi: areg APGAR_BAJO D_ProxProdMuni20KM D_RiverProdMuni20KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

estout m1 m2 m3 m4 m5 m6 using "$latexcodes/IndividualDummies_ComboMuniRobust.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_Prox* D_River* )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "/$R^2/$" )) replace

eststo m7: quietly xi: areg APGAR_BAJO D_ProxProdAIMuni5KM D_RiverProdAIMuni5KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m8: quietly xi: areg APGAR_BAJO D_ProxProdAIMuni10KM D_RiverProdAIMuni10KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m9: quietly xi: areg APGAR_BAJO D_ProxProdAIMuni20KM D_RiverProdAIMuni20KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

estout m4 m5 m6 m7 m8 m9 using "$latexcodes/IndividualDummies_ComboMuniRobust.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_Prox* D_River* )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "/$R^2/$" )) replace


**Hospital
eststo clear
local control3 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE region#c.FECHA_NAC

eststo m1: quietly xi: areg APGAR_BAJO ProxAreaHosp5KM RiverAreaHosp5KM  `control3' , absorb(COD_INST) vce(cluster COD_INST)
estadd ysumm

eststo m2: quietly xi: areg APGAR_BAJO ProxAreaHosp10KM RiverAreaHosp10KM  `control3' , absorb(COD_INST) vce(cluster COD_INST)
estadd ysumm

eststo m3: quietly xi: areg APGAR_BAJO ProxAreaHosp20KM RiverAreaHosp20KM  `control3' , absorb(COD_INST) vce(cluster COD_INST)
estadd ysumm

/*
eststo m4: quietly xi: areg APGAR_BAJO ProxProdHosp5KM RiverProdHosp5KM  `control3' , absorb(COD_INST) vce(cluster COD_INST)
estadd ysumm

eststo m5: quietly xi: areg APGAR_BAJO ProxProdHosp10KM RiverProdHosp10KM  `control3' , absorb(COD_INST) vce(cluster COD_INST)
estadd ysumm

eststo m6: quietly xi: areg APGAR_BAJO ProxProdHosp20KM RiverProdHosp20KM  `control3' , absorb(COD_INST) vce(cluster COD_INST)
estadd ysumm
*/
estout m1 m2 m3 using "$latexcodes/IndividualMeasure_ComboHospRobust.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(Prox* River* )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Hospcipalities" "Mean of Dep. Var." "/$R^2/$" )) replace


**Dummies combo hospital robustness
eststo clear
local control3 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE region#c.FECHA_NAC

eststo m1: quietly xi: areg APGAR_BAJO D_ProxAreaHosp5KM D_RiverAreaHosp5KM  `control3' , absorb(COD_INST) vce(cluster COD_INST)
estadd ysumm

eststo m2: quietly xi: areg APGAR_BAJO D_ProxAreaHosp10KM D_RiverAreaHosp10KM  `control3' , absorb(COD_INST) vce(cluster COD_INST)
estadd ysumm

eststo m3: quietly xi: areg APGAR_BAJO D_ProxAreaHosp20KM D_RiverAreaHosp20KM  `control3' , absorb(COD_INST) vce(cluster COD_INST)
estadd ysumm


eststo m4: quietly xi: areg APGAR_BAJO D_ProxProdHosp5KM D_RiverProdHosp5KM  `control3' , absorb(COD_INST) vce(cluster COD_INST)
estadd ysumm

eststo m5: quietly xi: areg APGAR_BAJO D_ProxProdHosp10KM D_RiverProdHosp10KM  `control3' , absorb(COD_INST) vce(cluster COD_INST)
estadd ysumm

eststo m6: quietly xi: areg APGAR_BAJO D_ProxProdHosp20KM D_RiverProdHosp20KM  `control3' , absorb(COD_INST) vce(cluster COD_INST)
estadd ysumm

estout m1 m2 m3 using "$latexcodes/IndividualDummies_ComboHospRobust.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_Prox* D_River* )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Hospcipalities" "Mean of Dep. Var." "/$R^2/$" )) replace
*/
******************
**
** HOLY WEEK
**
******************

*use "$base_out/DataTest.dta", clear
use "$base_out/DataCompleta_Stata.dta", clear
keep wgesthw ihw ano MES region D_AreaMinadaProp D_ProduccionPerArea D_Prox* D_River* PoblacionMuni AreaMinadaProp ProduccionPerArea AreaMinadaProp2 ProduccionPerArea2 Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE APGAR_BAJO LBW codmpio FECHA_NAC
qui gen coddepto=(codmpio-mod(codmpio,1000))/1000

gen ihwb=0 if wgesthw!=.
replace ihwb=1 if ihw==1 & wgesthw<=20
label var ihwb "Holyweek first 20 weeks of gestation"

gen ihwe=0 if wgesthw!=.
replace ihwe=1 if ihw==1 & ihwb==0
label var ihwe "Holyweek last weeks of gestation"

gen ihw1tr=0 if wgesthw!=.
replace ihw1tr=1 if ihw==1 & wgesthw<=13
label var ihw1tr "Holyweek 1st trimester"

gen ihw2tr=0 if wgesthw!=.
replace ihw2tr=1 if ihw==1 & wgesthw>=14 & wgesthw<=27
label var ihw2tr "Holyweek 2nd trimester"

gen ihw3tr=0 if wgesthw!=.
replace ihw3tr=1 if ihw==1 & wgesthw>=28 & wgesthw<=43
label var ihw3tr "Holyweek 3rd trimester"


gen ihw12tr=0 if wgesthw!=.
replace ihw12tr=1 if ihw==1 & wgesthw<=27
label var ihw12tr "Holyweek 1-2 trimester"


eststo clear

local control3 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE coddepto#c.ano




eststo hw1: quietly xi: areg APGAR_BAJO ihw D_ProxAreaMuni20KM D_RiverAreaMuni20KM c.D_ProxAreaMuni20KM#c.ihw  c.D_RiverAreaMuni20KM#c.ihw   `control3'  if wgesthw!=., absorb( codmpio) vce(cluster codmpio) 
estadd ysumm

eststo hw2 : quietly xi: areg APGAR_BAJO (c.D_ProxAreaMuni20KM  c.D_RiverAreaMuni20KM)##(c.ihw1tr c.ihw2tr c.ihw3tr)   `control3'  if wgesthw!=., absorb( codmpio) vce(cluster codmpio) 
estadd ysumm

*This regression runs HW for small munis. 
** Small is defined by the median of muni population calculated in the Muni Panel
eststo hwmp : quietly xi: areg APGAR_BAJO (c.D_ProxAreaMuni20KM  c.D_RiverAreaMuni20KM)##(c.ihw1tr c.ihw2tr c.ihw3tr)   `control3'  if wgesthw!=. & PoblacionMuni<50000 , absorb( codmpio) vce(cluster codmpio) 
estadd ysumm

estout hw1 hw2 hwmp using "$latexcodes/IndividualHWDummies_Combo_trim.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "/$R^2/$")) replace ///
keep(D_ProxAreaMuni20KM D_RiverAreaMuni20KM ihw ihw1tr ihw2tr ihw3tr c.D_ProxAreaMuni20KM#* c.D_RiverAreaMuni20KM#* ) varl(ihw "Holy Week" c.D_ProxAreaMuni20KM#c.ihw "Proximity /$/times/$ Holy Week" c.D_RiverAreaMuni20KM#c.ihw "River  /$/times/$ Holy Week"  ///
c.D_ProxAreaMuni20KM#c.ihw1tr "Proximity /$/times/$ Holy Week 1st Trim" c.D_RiverAreaMuni20KM#c.ihw1tr "River  /$/times/$ Holy Week 1st Trim"  ///
c.D_ProxAreaMuni20KM#c.ihw2tr "Proximity /$/times/$ Holy Week 2nd Trim" c.D_RiverAreaMuni20KM#c.ihw2tr "River  /$/times/$ Holy Week 2nd Trim"  ///
c.D_ProxAreaMuni20KM#c.ihw3tr "Proximity /$/times/$ Holy Week 3rd Trim" c.D_RiverAreaMuni20KM#c.ihw3tr "River  /$/times/$ Holy Week 3rd Trim" )

/*
eststo hwmpb : quietly xi: areg APGAR_BAJO (c.D_ProxAreaMuni20KM  c.D_RiverAreaMuni20KM)##(c.ihw1tr c.ihw2tr c.ihw3tr)   `control3'  if wgesthw!=. & PoblacionMuni<50000 & ano<2009 , absorb( codmpio) vce(cluster codmpio) 
estadd ysumm

eststo hwmpa : quietly xi: areg APGAR_BAJO (c.D_ProxAreaMuni20KM  c.D_RiverAreaMuni20KM)##(c.ihw1tr c.ihw2tr c.ihw3tr)   `control3'  if wgesthw!=. & PoblacionMuni<50000 & ano>=2009 , absorb( codmpio) vce(cluster codmpio) 
estadd ysumm

eststo hwmgb : quietly xi: areg APGAR_BAJO (c.D_ProxAreaMuni20KM  c.D_RiverAreaMuni20KM)##(c.ihw1tr c.ihw2tr c.ihw3tr)   `control3'  if wgesthw!=. & PoblacionMuni>=50000 & ano<2009 , absorb( codmpio) vce(cluster codmpio) 
estadd ysumm

eststo hwmga : quietly xi: areg APGAR_BAJO (c.D_ProxAreaMuni20KM  c.D_RiverAreaMuni20KM)##(c.ihw1tr c.ihw2tr c.ihw3tr)   `control3'  if wgesthw!=. & PoblacionMuni>=50000 & ano>=2009 , absorb( codmpio) vce(cluster codmpio) 
estadd ysumm

estout hw1 hw2 hwmpb hwmpa hwmgb hwmga using "$latexcodes/IndividualHWDummies_Combo_trim_pgba.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "/$R^2/$")) replace ///
keep(D_ProxAreaMuni20KM D_RiverAreaMuni20KM ihw ihw1tr ihw2tr ihw3tr c.D_ProxAreaMuni20KM#* c.D_RiverAreaMuni20KM#* ) varl(ihw "Holy Week" c.D_ProxAreaMuni20KM#c.ihw "Proximity /$/times/$ Holy Week" c.D_RiverAreaMuni20KM#c.ihw "River  /$/times/$ Holy Week"  ///
c.D_ProxAreaMuni20KM#c.ihw1tr "Proximity /$/times/$ Holy Week 1st Trim" c.D_RiverAreaMuni20KM#c.ihw1tr "River  /$/times/$ Holy Week 1st Trim"  ///
c.D_ProxAreaMuni20KM#c.ihw2tr "Proximity /$/times/$ Holy Week 2nd Trim" c.D_RiverAreaMuni20KM#c.ihw2tr "River  /$/times/$ Holy Week 2nd Trim"  ///
c.D_ProxAreaMuni20KM#c.ihw3tr "Proximity /$/times/$ Holy Week 3rd Trim" c.D_RiverAreaMuni20KM#c.ihw3tr "River  /$/times/$ Holy Week 3rd Trim" )
*/
/*
eststo hw3 : quietly xi: areg APGAR_BAJO (c.D_ProxAreaMuni5KM  c.D_RiverAreaMuni20KM)##(c.ihw12tr c.ihw3tr)   `control3' if wgesthw!=., absorb( codmpio) vce(cluster codmpio) 
estadd ysumm

eststo hw4 : quietly xi: areg APGAR_BAJO (c.D_ProxAreaMuni5KM  c.D_RiverAreaMuni20KM)##(c.ihwe c.ihwb)   `control3' if wgesthw!=., absorb( codmpio) vce(cluster codmpio) 
estadd ysumm

estout hw1 hw3 using "$latexcodes/IndividualHWDummies_Combo_trimjunto.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "/$R^2/$")) replace ///
keep(D_ProxAreaMuni5KM D_River* ihw ihw12tr ihw3tr c.D_Prox* c.D_River* ) varl(ihw "Holy Week" c.D_ProxAreaMuni5KM#c.ihw "Proximity /$/times/$ Holy Week" c.D_River*KM#c.ihw "River  /$/times/$ Holy Week"  ///
c.D_ProxAreaMuni5KM#c.ihw12tr "Proximity /$/times/$ Holy Week 1-2 Trim" c.D_River*KM#c.ihw12tr "River  /$/times/$ Holy Week 1-2 Trim"  ///
c.D_ProxAreaMuni5KM#c.ihw3tr "Proximity /$/times/$ Holy Week 3rd Trim" c.D_River*KM#c.ihw3tr "River  /$/times/$ Holy Week 3rd Trim" )

estout hw1 hw4 using "$latexcodes/IndividualHWDummies_Combo_20sem.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "/$R^2/$")) replace ///
keep(D_ProxAreaMuni5KM D_River* ihw ihwb ihwe c.D_ProxAreaMuni5KM#* c.D_River* ) varl(ihw "Holy Week" c.D_ProxAreaMuni5KM#c.ihw "Proximity /$/times/$ Holy Week" c.D_River*KM#c.ihw "River  /$/times/$ Holy Week"  ///
c.D_ProxAreaMuni5KM#c.ihwb "Proximity /$/times/$ Holy Week /$</$ 20 weeks" c.D_River*KM#c.ihwb "River  /$/times/$ Holy Week /$</$ 20 weeks"  ///
c.D_ProxAreaMuni5KM#c.ihwe "Proximity /$/times/$ Holy Week  /$>/$ 20 weeks" c.D_River*KM#c.ihwe "River  /$/times/$ Holy Week /$>/$ 20 weeks" )


eststo clear
local control3 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE region#c.FECHA_NAC

eststo: quietly xi: areg APGAR_BAJO c.D_ProxAreaMuni5KM##c.ihw  c.D_RiverAreaMuni20KM##c.ihw   `control3'  if wgesthw!=.  , absorb( codmpio) vce(cluster codmpio) 
estadd ysumm

eststo: quietly xi: areg APGAR_BAJO c.D_ProxAreaMuni5KM##c.ihw  c.D_RiverAreaMuni20KM##c.ihw   `control3'  if wgesthw!=. & ano<=2006 , absorb( codmpio) vce(cluster codmpio) 
estadd ysumm

eststo: quietly xi: areg APGAR_BAJO c.D_ProxAreaMuni5KM##c.ihw  c.D_RiverAreaMuni20KM##c.ihw   `control3' if wgesthw!=. & ano>2006 , absorb( codmpio) vce(cluster codmpio) 
estadd ysumm

estout using "$latexcodes/IndividualHWDummies_Combo2006.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "/$R^2/$")) replace ///
keep(D_ProxAreaMuni5KM D_River* ihw c.D_ProxAreaMuni5KM#c.ihw c.D_River* ) varl(ihw "Holy Week" c.D_ProxAreaMuni5KM#c.ihw " Proximity /$/times/$ Holy Week" c.D_River*KM#c.ihw "River  /$/times/$ Holy Week")
*/

/* Holyweek regression by each week
mat def mathw=J(40,2,.)
forval i=1/40{
xi: areg APGAR_BAJO c.D_ProxAreaMuni10KM##c.ihw`i'##c.D_RiverAreaMuni10KM  `control'  if wgesthw!=., absorb( codmpio) vce(cluster codmpio) 

}
*/

***Illegal mining

use "$base_out/DataCompleta_Stata.dta", clear
*use "$base_out/DataTest.dta", clear

merge m:1 codmpio using "$base_out/ilegalMunis.dta"
set matsize 5000
drop if _merge==2
replace Ilegal=0 if _merge==1
drop _merge


keep Ilegal ano MES region D_AreaMinadaProp D_ProduccionPerArea D_ProxAreaMuni5KM D_RiverAreaMuni5KM AreaMinadaProp ProduccionPerArea ProxAreaMuni5KM RiverAreaMuni5KM AreaMinadaProp2 ProduccionPerArea2 Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE APGAR_BAJO LBW codmpio FECHA_NAC
qui gen coddepto=(codmpio-mod(codmpio,1000))/1000

eststo clear
forvalues i=0/1{
local var_indepList AreaMinadaProp 
foreach var_indep of varlist `var_indepList'{

local control3 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE coddepto#c.ano

eststo m`i': quietly xi: areg APGAR_BAJO `var_indep' `control3'  if Ilegal==`i', absorb(codmpio) vce(cluster codmpio)
estadd ysumm
}
}
eststo m2: quietly xi: areg APGAR_BAJO AreaMinadaProp `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estout m2 m0 m1 using "$latexcodes/IndividualMeasure_`var_indep'_illegal.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(AreaMinadaProp )  stats( r2, fmt( a2) labels ( "/$R^2/$")) replace

forvalues i=0/1{
local var_indepList AreaMinadaProp 
foreach var_indep of varlist `var_indepList'{


eststo k`i': quietly xi: areg APGAR_BAJO D_`var_indep' `control3'  if Ilegal==`i', absorb(codmpio) vce(cluster codmpio)
estadd ysumm
}
}
eststo k2: quietly xi: areg APGAR_BAJO D_AreaMinadaProp `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estout k2 k0 k1 using "$latexcodes/IndividualDummies_`var_indep'_illegal.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_AreaMinadaProp )  stats(N N_clust ymean r2, fmt(a2 a2 a2 a2) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "/$R^2/$")) replace

/*
forvalues i=0/1{


eststo j`i': quietly xi: areg APGAR_BAJO D_RiverAreaMuni5KM D_ProxAreaMuni5KM `control3' if Ilegal==`i', absorb(codmpio) vce(cluster codmpio)
estadd ysumm

}
eststo j2: quietly xi: areg APGAR_BAJO D_RiverAreaMuni5KM D_ProxAreaMuni5KM `control3'  , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estout j2 j0 j1 using "$latexcodes/IndividualDummies_Combo_illegal.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_RiverAreaMuni5KM D_ProxAreaMuni5KM )  stats(N N_clust ymean r2, fmt(a2 a2 a2 a2) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "/$R^2/$")) replace


forvalues i=0/1{


eststo l`i': quietly xi: areg APGAR_BAJO RiverAreaMuni5KM ProxAreaMuni5KM `control3' if Ilegal==`i', absorb(codmpio) vce(cluster codmpio)
estadd ysumm

}
eststo l2: quietly xi: areg APGAR_BAJO RiverAreaMuni5KM ProxAreaMuni5KM `control3'  , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estout l2 l0 l1 using "$latexcodes/IndividualMeassures_Combo_illegal.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(RiverAreaMuni5KM ProxAreaMuni5KM )  stats(N N_clust ymean r2, fmt(a2 a2 a2 a2) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "/$R^2/$")) replace
*/

use "$base_out/PanelMunicipalEEVV_Stata.dta", clear

merge 1:1 ano Quarter codmpio using "$base_out/NonMissingsCount.dta"
drop _merge

tsset codmpio YrQrt, quarterly
gen fertilidad_Count=PoblacionMuni
gen TMI_Count=Nacimientos
gen Nacimientos_Count=1
qui gen coddepto=(codmpio-mod(codmpio,1000))/1000
eststo clear

local control3 i.ano i.Quarter coddepto#c.ano [aw= APGAR_BAJO_Count]

eststo m1: quietly xi: areg APGAR_BAJO ProxAreaMuni5KM RiverAreaMuni5KM  `control' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m2: quietly xi: areg LBW ProxAreaMuni5KM RiverAreaMuni5KM  `control' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m3: quietly xi: areg STUNT ProxAreaMuni5KM RiverAreaMuni5KM  `control' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm



estout m1 m2 m3 using "$latexcodes/MunicipalMeasure_Combo.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(Prox* River* )   replace


*Dummy proximity-river


eststo clear
local control i.ano i.Quarter coddepto#c.ano [aw= APGAR_BAJO_Count]

eststo m1: quietly xi: areg APGAR_BAJO D_ProxAreaMuni5KM D_RiverAreaMuni5KM `control' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local rtrend "Yes"



eststo m2: quietly xi: areg LBW D_ProxAreaMuni5KM D_RiverAreaMuni5KM  `control' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local rtrend "Yes"

eststo m3: quietly xi: areg STUNT D_ProxAreaMuni5KM D_RiverAreaMuni5KM  `control' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local rtrend "Yes"
estout m1 m2 m3 using "$latexcodes/MunicipalDummies_Combo.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_Prox* D_River* )  stats(N N_clust ymean r2 munife timefe momc rtrend, fmt(a2 a2 a2 a2 %~#s %~#s %~#s %~#s) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "/$R^2/$" "Municipality FE" "Time FE" "Ind. Controls" "Reg. Trends")) replace
*note("/specialcell{Clustered standard errors, by municipality, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") ///

local var_indepList AreaMinadaProp ProduccionPerArea
foreach var_indep of varlist `var_indepList'{
eststo clear
local var_try1 fertilidad Nacimientos TMI
foreach vardep of varlist `var_try1'{
eststo: quietly xi: areg `vardep' `var_indep' i.ano i.Quarter coddepto#c.ano [aw=`vardep'_Count], absorb( codmpio) vce(cluster codmpio) 

}

estout using "$latexcodes/FertilidadMeassure_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(`var_indep'  )  stats( r2, fmt( a2) labels ( "/$R^2/$")) replace

eststo clear
local var_try1 fertilidad Nacimientos TMI
foreach vardep of varlist `var_try1'{
eststo: quietly xi: areg `vardep' D_`var_indep'  i.ano i.Quarter coddepto#c.ano [aw=`vardep'_Count], absorb( codmpio) vce(cluster codmpio) 
estadd ysumm

}
estout using "$latexcodes/FertilidadDummies_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_`var_indep')  stats( r2, fmt( a2) labels ( "/$R^2/$")) replace


}

eststo clear
local var_try1 fertilidad Nacimientos TMI
foreach vardep of varlist `var_try1'{
eststo: quietly xi: areg `vardep' D_ProxAreaMuni5KM D_RiverAreaMuni20KM  i.ano i.Quarter coddepto#c.ano [aw=`vardep'_Count], absorb( codmpio) vce(cluster codmpio) 
estadd ysumm
}
estout using "$latexcodes/FertilidadDummies_Combo.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( D_Prox* D_River*)  stats(N N_clust ymean r2, fmt(a2 a2 a2 a2) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "/$R^2/$")) replace
/*
eststo clear
local var_try1 fertilidad Nacimientos TMI
foreach vardep of varlist `var_try1'{
eststo: quietly xi: areg `vardep' RiverAreaMuni5KM ProxAreaMuni5KM  i.ano i.Quarter region#c.YrQrt [aw=`vardep'_Count], absorb( codmpio) vce(cluster codmpio) 
estadd ysumm
}
estout using "$latexcodes/FertilidadMeassure_Combo.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(RiverAreaMuni5KM ProxAreaMuni5KM)  stats(N N_clust ymean r2, fmt(a2 a2 a2 a2) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "/$R^2/$")) replace
*/

*** Variables Fiscales
**
eststo clear
local var_indepList AreaMinadaProp Produccion
foreach var_indep of varlist `var_indepList'{
eststo clear
local var_try1 y_total y_cap_regalias
foreach vardep of varlist `var_try1'{
eststo: quietly xi: areg `vardep' `var_indep' i.ano i.Quarter coddepto#c.ano , absorb( codmpio) vce(cluster codmpio) 

}

estout using "$latexcodes/FiscalMeassure_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(`var_indep'  )  replace

eststo clear

local var_try1 y_total y_cap_regalias
foreach vardep of varlist `var_try1'{
eststo: quietly xi: areg `vardep' D_`var_indep'  i.ano i.Quarter coddepto#c.ano , absorb( codmpio) vce(cluster codmpio) 
estadd ysumm

}
estout using "$latexcodes/FiscalDummies_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_`var_indep')  stats(N N_clust ymean r2, fmt(a2 a2 a2 a2) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "/$R^2/$")) replace


}


**
*** Appendix
***

*Robustness measure combo

eststo clear
local control3 i.ano i.Quarter coddepto#c.ano [aw= APGAR_BAJO_Count]

eststo m1: quietly xi: areg APGAR_BAJO ProxAreaMuni5KM RiverAreaMuni5KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m2: quietly xi: areg APGAR_BAJO ProxAreaMuni10KM RiverAreaMuni10KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m3: quietly xi: areg APGAR_BAJO ProxAreaMuni20KM RiverAreaMuni20KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m7: quietly xi: areg APGAR_BAJO ProxProdAIMuni5KM RiverProdAIMuni5KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m8: quietly xi: areg APGAR_BAJO ProxProdAIMuni10KM RiverProdAIMuni10KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m9: quietly xi: areg APGAR_BAJO ProxProdAIMuni20KM RiverProdAIMuni20KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

estout m1 m2 m3 m7 m8 m9 using "$latexcodes/MunicipalMeasure_ComboMuniRobustAI.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(Prox* River* )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "/$R^2/$" )) replace

*Robustness Dummy combo

eststo clear
local control3 i.ano i.Quarter coddepto#c.ano [aw= APGAR_BAJO_Count]

eststo m1: quietly xi: areg APGAR_BAJO D_ProxAreaMuni5KM D_RiverAreaMuni5KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m2: quietly xi: areg APGAR_BAJO D_ProxAreaMuni10KM D_RiverAreaMuni10KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m3: quietly xi: areg APGAR_BAJO D_ProxAreaMuni20KM D_RiverAreaMuni20KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m7: quietly xi: areg APGAR_BAJO D_ProxProdAIMuni5KM D_RiverProdAIMuni5KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m8: quietly xi: areg APGAR_BAJO D_ProxProdAIMuni10KM D_RiverProdAIMuni10KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m9: quietly xi: areg APGAR_BAJO D_ProxProdAIMuni20KM D_RiverProdAIMuni20KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

estout m1 m2 m3 m7 m8 m9 using "$latexcodes/MunicipalDummies_ComboMuniRobustAI.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_Prox* D_River* )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "/$R^2/$" )) replace

/*
**Event study by quarters D_AreaMinadaProp
use "$base_out/PanelMunicipalEEVV_Stata.dta", clear

merge 1:1 ano Quarter codmpio using "$base_out/NonMissingsCount.dta"
drop _merge
qui gen coddepto=(codmpio-mod(codmpio,1000))/1000
sort codmpio YrQrt
by codmpio: egen qstartm=min(YrQrt) if D_AreaMinadaProp==1
*by codmpio: egen qstartm=min(quartercont) if D_RiverProdAIMuni10KM==1 & D_ProxProdAIMuni10KM!=1

by codmpio: egen qstartm2=min(qstartm)
gen qdistm=YrQrt-qstartm2

gen qdistm2=.
	replace qdistm2=1 if qdistm<=-16 & !missing(qdistm)
forvalues i=-15/15{
	replace qdistm2=`i'+17 if qdistm==`i' & !missing(qdistm)
}
replace qdistm2=33 if qdistm>=16 & !missing(qdistm)
replace qdistm2=0 if qdistm==.


fvset base 16 qdistm2
char qdistm2[omit] 0

areg APGAR_BAJO i.qdistm2 i.ano i.Quarter coddepto#c.ano [aw= APGAR_BAJO_Count], absorb(codmpio) vce(cluster codmpio)
coefplot, graphregion(color(white)) baselevels keep(*qdistm2) ci ///
rename(*.qdistm2=*) ///
vertical yline(0) xtitle("Quarters to Star Mining Extraction") ytitle("Coefficent on APGAR") title("Event study for Low APGAR and Start of Mining operations")



local nqcut=6

qui gen D_bb`nqcut'=0
qui replace D_bb`nqcut'=1 if qdistm<-`nqcut'

forval i=`nqcut'(-1)2 {
qui gen D_b`i'=0
qui replace D_b`i'=1 if qdistm==-`i'
}

forval i=0/`nqcut'{
qui gen D_a`i'=0
qui replace D_a`i'=1 if qdistm==`i'
}

qui gen D_aa`nqcut'=0
qui replace D_aa`nqcut'=1 if qdistm>`nqcut' & qdistm!=.

qui xi: areg APGAR_BAJO D_bb`nqcut'-D_aa`nqcut' i.ano i.Quarter coddepto#c.ano [aw= APGAR_BAJO_Count], absorb(codmpio) vce(cluster codmpio)

coefplot, graphregion(color(white)) baselevels keep(D_*) ci ///
rename(D_bb`nqcut'="-`nqcut'" )   ///
vertical yline(0) xtitle("Quarters to Star Mining Extraction") ytitle("Coefficent on APGAR") title("Event study for Low APGAR and Start of Mining operations")

 graph export "$latexcodes/EventStudyquarter.pdf", replace
*/
	
	**Event study by year D_AreaMinadaProp
use "$base_out/PanelMunicipalEEVV_Stata.dta", clear

merge 1:1 ano Quarter codmpio using "$base_out/NonMissingsCount.dta"
drop _merge

sort codmpio ano
by codmpio: egen astartm=min(ano) if D_AreaMinadaProp==1
by codmpio: egen astartm2=min(astartm)
gen adistm=ano-astartm2
qui gen coddepto=(codmpio-mod(codmpio,1000))/1000
local nqcut=4

qui gen D_bb`nqcut'=0
qui replace D_bb`nqcut'=1 if adistm<-`nqcut'

forval i=`nqcut'(-1)2 {
qui gen D_b`i'=0
qui replace D_b`i'=1 if adistm==-`i'
}

forval i=0/`nqcut'{
qui gen D_a`i'=0
qui replace D_a`i'=1 if adistm==`i'
}

qui gen D_aa`nqcut'=0
qui replace D_aa`nqcut'=1 if adistm>`nqcut' & adistm!=.

qui xi: areg APGAR_BAJO D_bb`nqcut'-D_aa`nqcut' i.ano i.Quarter coddepto#c.ano [aw= APGAR_BAJO_Count], absorb(codmpio) vce(cluster codmpio)


coefplot, graphregion(color(white)) baselevels keep(D_*) ci ///
rename(D_bb`nqcut'="<-`nqcut'" D_b`nqcut'="-`nqcut'"  D_b3="-3" D_b2="-2"  ///
       D_aa`nqcut'=">`nqcut'" D_a`nqcut'="`nqcut'"  D_a3="3" D_a2="2" D_a1="1" D_a0="0") ///
vertical yline(0) xtitle("Years to mining title") ytitle("Coefficent on APGAR") title("Event study for Low APGAR and mining titles") 
 graph export "$latexcodes/EventStudyyear.pdf", replace
 
 	**Event study by year D_ProxAreaMuni20KM
use "$base_out/PanelMunicipalEEVV_Stata.dta", clear

merge 1:1 ano Quarter codmpio using "$base_out/NonMissingsCount.dta"
drop _merge

sort codmpio ano
by codmpio: egen astartm=min(ano) if D_ProxAreaMuni20KM==1
by codmpio: egen astartm2=min(astartm)
gen adistm=ano-astartm2
qui gen coddepto=(codmpio-mod(codmpio,1000))/1000
local nqcut=4

qui gen D_bb`nqcut'=0
qui replace D_bb`nqcut'=1 if adistm<-`nqcut'

forval i=`nqcut'(-1)2 {
qui gen D_b`i'=0
qui replace D_b`i'=1 if adistm==-`i'
}

forval i=0/`nqcut'{
qui gen D_a`i'=0
qui replace D_a`i'=1 if adistm==`i'
}

qui gen D_aa`nqcut'=0
qui replace D_aa`nqcut'=1 if adistm>`nqcut' & adistm!=.

qui xi: areg APGAR_BAJO D_bb`nqcut'-D_aa`nqcut' D_RiverAreaMuni20KM i.ano i.Quarter coddepto#c.ano [aw= APGAR_BAJO_Count], absorb(codmpio) vce(cluster codmpio)


coefplot, graphregion(color(white)) baselevels keep(D_b* D_a*) ci ///
rename(D_bb`nqcut'="<-`nqcut'" D_b`nqcut'="-`nqcut'" D_b3="-3" D_b2="-2"  ///
       D_aa`nqcut'=">`nqcut'" D_a`nqcut'="`nqcut'"  D_a3="3" D_a2="2" D_a1="1" D_a0="0") ///
vertical yline(0) xtitle("Years to proximity exposure") ytitle("Coefficent on APGAR") title("Event study for Low APGAR and proximity exposure") 
 graph export "$latexcodes/EventStudyyear_AreaProx.pdf", replace
 
 **Event study by year D_RiverAreaMuni20KM
use "$base_out/PanelMunicipalEEVV_Stata.dta", clear

merge 1:1 ano Quarter codmpio using "$base_out/NonMissingsCount.dta"
drop _merge

sort codmpio ano
by codmpio: egen astartm=min(ano) if D_RiverAreaMuni20KM==1
by codmpio: egen astartm2=min(astartm)
gen adistm=ano-astartm2
qui gen coddepto=(codmpio-mod(codmpio,1000))/1000
local nqcut=4

qui gen D_bb`nqcut'=0
qui replace D_bb`nqcut'=1 if adistm<-`nqcut'

forval i=`nqcut'(-1)2 {
qui gen D_b`i'=0
qui replace D_b`i'=1 if adistm==-`i'
}

forval i=0/`nqcut'{
qui gen D_a`i'=0
qui replace D_a`i'=1 if adistm==`i'
}

qui gen D_aa`nqcut'=0
qui replace D_aa`nqcut'=1 if adistm>`nqcut' & adistm!=.

qui xi: areg APGAR_BAJO D_bb`nqcut'-D_aa`nqcut' D_ProxAreaMuni20KM i.ano i.Quarter coddepto#c.ano [aw= APGAR_BAJO_Count], absorb(codmpio) vce(cluster codmpio)


coefplot, graphregion(color(white)) baselevels keep(D_b* D_a*) ci ///
rename(D_bb`nqcut'="<-`nqcut'" D_b`nqcut'="-`nqcut'"  D_b3="-3" D_b2="-2"  ///
       D_aa`nqcut'=">`nqcut'" D_a`nqcut'="`nqcut'"  D_a3="3" D_a2="2" D_a1="1" D_a0="0") ///
vertical yline(0) xtitle("Years to river exposure") ytitle("Coefficent on APGAR") title("Event study for Low APGAR and river exposure") 
 graph export "$latexcodes/EventStudyyear_AreaRiver.pdf", replace
	
	
/*
collapse APGAR_BAJO [fw= APGAR_BAJO_Count], by(qdistm)
graph twoway (scatter APGAR_BAJO qdistm if qdistm>-8 & qdistm<8) ///
(lpoly APGAR_BAJO qdistm if qdistm>-8 & qdistm<8), ///
saving("$latexcodes/event_downstream", replace)



*Example of exporting nice graph
graph twoway 
 (scatter coef lot_pay if _n<=4, connect(l))
 (rcapsym lower_bound upper_bound lot_pay if _n<=4, msymbol("-")), 
 ytitle("Hours worked") xtitle("Lottery Prize") 
 xlabel(20 "20" 200 "200" 250 "250" 300 "300" )
 legend(rows(2) order(1 "Coefficient" 2 "95% CI")) 
 title("Hours worked and lottery", size(medium)) saving("$results/lottery_hours", replace);

 */

/*
*Falsification Test with height 

local var_indepList AreaMinadaProp ProduccionPerArea
local control1 i.ano i.Semana_Naci 
local control2 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
local control3 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE region#c.FECHA_NAC
foreach var_indep of varlist `var_indepList'{
eststo clear
 eststo mh: quietly xi: areg Z_TALLA_NAC `var_indep' `var_indep'2 `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local timefe "Yes"
estadd local momc "Yes"
estadd local rtrend "Yes"
estout m3 mh using "APGAR_height_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(`var_indep' `var_indep'2 )  stats(N ymean r2 timefe momc rtrend, fmt(a2 a2 a2 %~#s %~#s %~#s) labels ("N. of obs." "Mean of Dep. Var." "/$R^2/$" "Time FE" "Ind. Controls" "Reg. Trends")) replace
*/


***Heterogeneous Effects*****

/*
local N=5
forval i=1/`N'{
qui gen ibinAreaProp`i'=0
replace ibinAreaProp`i'=1 if AreaMinadaProp>(`i'-1)*($areapropcut )/`N'  & AreaMinadaProp<=(`i')*($areapropcut )/`N'
}

forval i=1/`N'{
qui gen iboxAreaProp`i'=0
replace iboxAreaProp`i'=1 if AreaMinadaProp>(`i'-1)*($areapropcut )/`N'  & AreaMinadaProp<=($areapropcut) 
}
label var ibinAreaProp1 "Proportion of area mined between 0 and 2.8/%"
label var ibinAreaProp2 "Proportion of area mined between 2.8/% and 5.6/%"
label var ibinAreaProp3 "Proportion of area mined between 5.6/% and 8.4/%"
label var ibinAreaProp4 "Proportion of area mined between 8.4/% and 11.2/%"
label var ibinAreaProp5 "Proportion of area mined between 11.2/% and 14/%"

label var iboxAreaProp1 "Proportion of area mined greater than 0/%"
label var iboxAreaProp2 "Proportion of area mined greater than 2.8/% "
label var iboxAreaProp3 "Proportion of area mined greater than 5.6/% "
label var iboxAreaProp4 "Proportion of area mined greater than 8.4/% "
label var iboxAreaProp5 "Proportion of area mined greater than 11.2/% "

xi: areg APGAR_BAJO ibinAreaProp* `control3' , absorb(codmpio) vce(cluster codmpio)
xi: areg APGAR_BAJO iboxAreaProp* `control3' , absorb(codmpio) vce(cluster codmpio)

**How to add the notes on fixed effects yes no
eststo m4: quietly xi: areg APGAR_BAJO D_ProxAreaMuni5KM D_RiverAreaMuni5KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local momc "Yes"
estadd local rtrend "Yes"
estout m3 m4 using "$latexcodes/IndividualDummies_Combo_price.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_Prox* D_River*  )  stats(N N_clust ymean r2 munife timefe momc rtrend, fmt(a2 a2 a2 a2 %~#s %~#s %~#s %~#s) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "/$R^2/$" "Municipality FE" "Time FE" "Ind. Controls" "Reg. Trends")) replace
*note("/specialcell{Clustered standard errors, by municipality, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") ///


*/


import excel "$base_out\Temporary\centros_poblados_colombia.xlsx", sheet("centros_poblados_colombia") firstrow clear
rename CODANE codmpio
destring codmpio, force replace
rename CODANEDEP coddepto
destring coddepto, force replace
replace NOM_PTO=lower(NOM_PTO)
gen ind_cabecera=1 if CATEGORIA=="CABECERA MUNICIPAL"
replace ind_cabecera=0 if ind_cabecera==.
keep coddepto codmpio ind_cabecera CATEGORIA NOM_PTO LAT LON OBJECTID
save "$base_out\Temporary\lat_lon_centros_poblados.dta", replace



import excel "$base_out\Temporary\hospitalesgeocoded.xlsx", sheet("hospitalesgeocoded") firstrow clear
keep if Score!=0
gen coddepto=(codmpio-mod(codmpio,1000))/1000

keep coddepto codmpio cod_inst X Y
gen original_gps=1
save "$base_out\Temporary\hospitals_original_gps.dta", replace

import excel "$base_out\Temporary\hospitalesgeocoded.xlsx", sheet("hospitalesgeocoded") firstrow clear

keep if Score==0
gen coddepto=(codmpio-mod(codmpio,1000))/1000
keep coddepto codmpio cod_inst direccion
gen ind_cabecera=.
gen NOM_PTO=""
foreach x in Vereda CORREGIMIENTO "CENTRO DE SALUD" "INSPECCION DE POLICIA"{
replace NOM_PTO=subinstr(direccion,"`x'","",.) if strpos(direccion,"`x'")!=0
replace ind_cabecera=0 if strpos(direccion,"`x'")!=0
}

foreach x in VERDA VDA VEREDA corregimiento vereda {
replace NOM_PTO=subinstr(direccion,"`x'","",.) if strpos(direccion,"`x'")!=0
replace ind_cabecera=0 if strpos(direccion,"`x'")!=0
}

foreach x in "AV " "AV." AVENIDA CALLE CARRERA CENTRO "CL " CLL "CR " "CRA " Calle {
replace ind_cabecera=1 if strpos(direccion,"`x'")!=0 & ind_cabecera==.
}

foreach x in Carrera DIAG DG Diagonal EDIF "K " "K2 " "KA " KARRERA KDX "KR " {
replace ind_cabecera=1 if strpos(direccion,"`x'")!=0 & ind_cabecera==.
}

foreach x in "Plaza Principal" TRANS "TV " avenida barrio calle carrera "cra " "kra "{
replace ind_cabecera=1 if strpos(direccion,"`x'")!=0 & ind_cabecera==.
}


** If I uncomment the line below the reclink match doesnt work
*replace NOM_PTO=direccion if CATEGORIA=="" & NOM_PTO==""
drop if ind_cabecera==. & NOM_PTO==""

replace NOM_PTO=lower(NOM_PTO)
replace NOM_PTO="timba" if NOM_PTO==" de timba frente al parque principal"
replace NOM_PTO="casas bajas" if NOM_PTO==" santa teresa-corregimiento de casas bajas"
replace NOM_PTO="chacon" if NOM_PTO=="hospital ese occidente timbiqui zona rural  de chacon"
replace NOM_PTO="chimila" if NOM_PTO==" la victoria la independencia corr chimila"
replace NOM_PTO="paramo" if NOM_PTO=="sr la capilla,  el paramo"
replace NOM_PTO="bazan" if NOM_PTO=="cs  baz�n"
replace NOM_PTO="simana" if NOM_PTO==" de sima-a"
replace NOM_PTO="briceno" if NOM_PTO==" de brice-o"
replace NOM_PTO="guaimia" if NOM_PTO==" de guainia"
replace NOM_PTO="puerto carreno" if NOM_PTO==" pto. carre-o"
replace NOM_PTO="bolivar(san pedro)" if NOM_PTO=="cs  san pedro"
replace NOM_PTO="inguapi del carmen 2" if NOM_PTO=="hospital san andres e.s.e de tumaco  inguapi del carmen kilometro 23 carretera de tumaco"
replace NOM_PTO="bazan(la bocana)" if NOM_PTO==" de la bocana"
replace NOM_PTO="puerto patino" if NOM_PTO==" de villa de puerto pati-o"
replace NOM_PTO="farallones" if NOM_PTO=="entrada principal  farallones"
replace NOM_PTO="santacruz de robles" if NOM_PTO==" de robles"
replace NOM_PTO="santafe" if NOM_PTO==" de santa fe"
replace NOM_PTO="pital(chimbuzal)" if NOM_PTO==" pital de la costa"
replace NOM_PTO="san jose calabazal" if NOM_PTO==" calabazal"

** There are many observations with same cod_inst
** I could drop them, or try to match the different names and collapse later
*If we decide to "average" coordinates
*gen temp_id_inst=_n
*reclink coddepto codmpio ind_cabecera NOM_PTO using "$base_out\Temporary\lat_lon_centros_poblados.dta", idm(temp_id_inst) idu(OBJECTID) gen(myscore) required(codmpio coddepto )
egen group_inst=group(cod_inst)
gen tempindi=1
bysort group_inst: egen ngroup=total(tempindi)
drop if ngroup>1
reclink coddepto codmpio ind_cabecera NOM_PTO using "$base_out\Temporary\lat_lon_centros_poblados.dta", idm(cod_inst) idu(OBJECTID) gen(myscore) required(codmpio coddepto )

drop if _merge==1
drop if myscore<0.9678 & ind_cabecera==0



rename LAT Y
rename LON X
gen original_gps=-ind_cabecera
keep coddepto codmpio cod_inst X Y original_gps

append using "$base_out\Temporary\hospitals_original_gps.dta"

drop if codmpio==0
sort codmpio Y X original_gps
egen group_gps=group( Y X)
save "$base_out\hospitals_gps.dta", replace
export delimited using "$base_out\hospitals_gps.csv", replace

collapse (min) cod_inst (count) X, by(group_gps)
rename X Nhosp_group

save "$base_out\hosp_uniq_group_gps.dta", replace
export delimited using "$base_out\hosp_uniq_group_gps.csv", replace

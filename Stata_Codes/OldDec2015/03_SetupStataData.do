global areapropcut=15


import delimited "$base_out/MunicipioNearestMine.csv", clear
rename codane2 codmpio
drop if distmine>25000
keep codmpio
	
save "$base_out/Munis_mineros.dta",replace

***Select munis ilegal mines


use "$base_in\cm_bd_final.dta", clear


drop if at_pyr_03_01_07!=1

gen legalidad=(tipo_mina==1) if !missing(tipo_mina)
estpost tabstat legalidad, by(ag_ue_01) statistics(mean count) columns(statistics)
collapse (mean) legalidad (count) minasCenso=legalidad, by( cod_mpio)
save "$base_out/cm_bd_oro_cod_mpio.dta", replace

import delim "$base_out/BaseMineriaIlegal_MinDefensa.csv", clear

collapse (sum) cantidad, by(cod_muni)
rename cod_muni cod_mpio

merge 1:1 cod_mpio using "$base_out/cm_bd_oro_cod_mpio.dta"

replace cantidad=0 if cantidad==.
replace minasCenso=0 if minasCenso==.
replace legalidad=. if minasCenso==0



label var cantidad "Police"
label var legalidad "Proportion of legal mines"
label var minasCenso "Total number of mines in 2011"

keep if cantidad>0 | legalidad<1

drop cantidad legalidad minasCenso
replace cod_mpio=13600 if cod_mpio==13490
replace cod_mpio=19142 if cod_mpio==19300
replace cod_mpio=27787 if cod_mpio==27160
replace cod_mpio=27205 if cod_mpio==27580
replace cod_mpio=27999 if cod_mpio==27205 | cod_mpio==27361 | cod_mpio==27491



gen Ilegal=1
drop _merge
rename cod_mpio codmpio
collapse (mean) Ilegal, by(codmpio)
save "$base_out/ilegalMunis.dta", replace


use "$base_out/PanelMunicipalEEVV.dta",clear
compress 
*Restrict analysis to years we have production AND area data
drop if ANO>2012
drop if ANO<2001
*Drop capitals
*drop if mod(CODIGO_DANE_M,1000)==1
drop if Quarter==.
gen YrQrt = yq(ANO, Quarter)
tsset CODIGO_DANE_M YrQrt, quarterly
rename CODIGO_DANE_M codmpio
rename ANO ano
foreach arpr in Area Prod {
foreach i in 5 10 20 {

qui rename Upstream`arpr'Muni`i'KM River`arpr'Muni`i'KM
qui rename Exp`arpr'Muni`i'KM Prox`arpr'Muni`i'KM

}
}
collapse (max) AreaMinadaKm2 Produccion RiverAreaMuni20KM RiverProdMuni20KM, by(codmpio)
qui gen MineriaArea_Ever=1 if AreaMinadaKm2>0 & AreaMinadaKm2!=.
qui gen MineriaProd_Ever=1 if Produccion>0 & Produccion!=.
qui gen RiverArea_Ever=1 if RiverAreaMuni20KM>0 & RiverAreaMuni20KM!=.
qui gen RiverProd_Ever=1 if RiverProdMuni20KM>0 & RiverProdMuni20KM!=.
foreach vrn in MineriaArea_Ever MineriaProd_Ever RiverArea_Ever RiverProd_Ever  {
replace `vrn'=0 if `vrn'==.
}

keep codmpio *_Ever

save "$base_out/Temporary/Muni_Ever.dta", replace

use "$base_out/PanelMunicipalEEVV.dta",clear
compress 
*Restrict analysis to years we have production AND area data
drop if ANO>2012
drop if ANO<2001
*Drop capitals
*drop if mod(CODIGO_DANE_M,1000)==1
drop if Quarter==.
gen YrQrt = yq(ANO, Quarter)
tsset CODIGO_DANE_M YrQrt, quarterly
rename CODIGO_DANE_M codmpio
rename ANO ano
rename AreaMinadaKm2 AreaMinada_sqkm
rename areaMuni_km2 areaMuni_sqkm
foreach arpr in Area {
foreach i in 5 10 20 {

qui rename Upstream`arpr'Muni`i'KM River`arpr'Muni`i'KM
qui rename Exp`arpr'Muni`i'KM Prox`arpr'Muni`i'KM

}
}

**After comparing raw production to Ana Ibanez
** we conclude A Ibanez trick is better
foreach arpr in Prod {
foreach i in 5 10 20 {

qui drop Upstream`arpr'Muni`i'KM 
qui drop Exp`arpr'Muni`i'KM 

}
}

foreach arpr in Prod {
foreach i in 5 10 20 {

qui rename AI_Upstream`arpr'Muni`i'KM River`arpr'AIMuni`i'KM
qui rename AI_Exp`arpr'Muni`i'KM Prox`arpr'AIMuni`i'KM

}
}
merge m:1 codmpio using "$base_out/Temporary/Muni_Ever.dta"
drop if _merge==2
drop _merge
merge m:1 codmpio ano using "$base_in\PANELES_CEDE\CARACTERÍSTICAS GENERALES\PANEL CARAC. GENERALES.dta", keepus(gandina gcaribe gpacifica gorinoquia gamazonia pobl_rur pobl_urb pobl_tot indrural areaoficialkm2 altura discapital dismdo TMI disbogota codmdo gpc pobreza gini nbi minorias parques religioso estado otras pecsaludc pepsaludc pehosclinc peplantaec pebibliopc ipm_ti_p ipm_tdep_p ipm_assalud_p ipm_accsalud_p ipm_accagua_p ipm_templeof_p ipm_tdep_p ipm_excretas_p ipm_pisos_p ipm_paredes_p ipm_hacinam_p)
drop if _merge==2
drop _merge
merge m:1 codmpio ano using "$base_in\PANELES_CEDE\agricultura y tierra\PANEL AGRICULTURA Y TIERRA.dta", keepus(r_acelga- r_Ñame agua erosion aptitud cover_land_c2 cover_land_s8 cover_land_t8 used_land_3 g_terreno g_prop uafpmhas htapromedio)
drop if _merge==2
drop _merge
merge m:1 codmpio ano using "$base_in\PANELES_CEDE\FISCAL\PANEL FISCAL.dta", keepus(categoria FNR regalias_compensa SGP_propgeneral SGP_salud SGP_educacion SGP_alescolar inv_en_salud inv_prevdesastr inv_gruposvunera inv_dllocomun inv_ambiental inv_aguasani inv_sp inv_promdllo inv_a_educacion y_total y_cap_regalias)
drop if _merge==2
drop _merge

*Crear indice de rendimiento agricola
forvalues i=2007/2012{
foreach var of varlist r_acelga- r_Ñame{ 
qui replace `var'=. if `var'==0
qui sum `var' if ano==`i'
if `i'==2007{
qui gen Z_`var'=(`var'-r(mean))/r(sd) if ano==`i'
}
if `i'!=2007{
qui replace Z_`var'=(`var'-r(mean))/r(sd) if ano==`i'
}
}
}

qui egen Z_Productividad=rowtotal(Z_r_acelga- Z_r_Ñame),missing
forvalues i=2007/2012{
qui sum Z_Productividad if ano==`i'
qui replace Z_Productividad=(Z_Productividad-r(mean))/r(sd)
}
qui drop Z_r_acelga- Z_r_Ñame 
qui drop r_acelga- r_Ñame
*Create other variables

qui gen AreaMinadaProp=100*AreaMinada_sqkm/areaMuni_sqkm
qui gen ProduccionPerCapita=(Produccion/PoblacionMuni)/10^2





foreach prri in Prox River {
foreach arpr in Area ProdAI {
foreach i in 5 10 20 {
qui replace `prri'`arpr'Muni`i'KM=`prri'`arpr'Muni`i'KM/10^6

qui sum `prri'`arpr'Muni`i'KM if `prri'`arpr'Muni`i'KM>0,d 
qui replace `prri'`arpr'Muni`i'KM=0 if `prri'`arpr'Muni`i'KM<r(p50)/100 & `prri'`arpr'Muni`i'KM!=.
qui replace `prri'`arpr'Muni`i'KM=r(p99) if `prri'`arpr'Muni`i'KM>r(p99) & `prri'`arpr'Muni`i'KM!=.


*qui gen `prri'`arpr'Muni`i'KM2=`prri'`arpr'Muni`i'*`prri'`arpr'Muni`i'
qui gen D_`prri'`arpr'Muni`i'KM=(`prri'`arpr'Muni`i'KM>0) if !missing(`prri'`arpr'Muni`i'KM)
}
}
}



foreach i in 5 10 20 {
qui label var ProxAreaMuni`i'KM "Proximity exposure area `i' km"
*label var ProxAreaMuni`i'KM2 "{[Proximity exposure area `i' km]}\$^2\$"
*label var ProxProdMuni`i'KM "Proximity exposure production `i' km"
*label var ProxProdMuni`i'KM2 "{[Proximity exposure production `i' km]}\$^2\$"
qui label var RiverAreaMuni`i'KM "River exposure area `i' km"
*label var RiverAreaMuni`i'KM2 "{[River exposure area `i' km]}\$^2\$"
*label var RiverProdMuni`i'KM "River exposure production `i' km"
*label var RiverProdMuni`i'KM2 "{[River exposure production `i' km]}\$^2\$"
qui label var D_ProxAreaMuni`i'KM "Proximity exposure area `i' km \$>0\$"
*label var D_ProxProdMuni`i'KM "Proximity exposure production `i' km \$>0\$"
qui label var D_RiverAreaMuni`i'KM "River exposure area `i' km \$>0\$"
*label var D_RiverProdMuni`i'KM "River exposure production `i' km \$>0\$"

label var RiverProdAIMuni`i'KM "River exposure production AI `i' km"
*label var RiverProdAIMuni`i'KM2 "{[River exposure production AI `i' km]}\$^2\$"
label var D_ProxProdAIMuni`i'KM "Proximity exposure production AI `i' km \$>0\$"
label var D_RiverProdAIMuni`i'KM "River exposure production AI `i' km \$>0\$"
label var ProxProdAIMuni`i'KM "Proximity exposure production AI `i' km"
*label var ProxProdAIMuni`i'KM2 "{[Proximity exposure production AI `i' km]}\$^2\$"
}

qui gen AreaMinadaProp2=AreaMinadaProp*AreaMinadaProp
qui gen ProduccionPerCapita2=ProduccionPerCapita*ProduccionPerCapita
qui gen ProduccionPerArea=(Produccion/areaMuni_sqkm)/10^3
qui gen ProduccionPerArea2=ProduccionPerArea*ProduccionPerArea
qui gen ProduccionAccPerArea=(Produccion_Accumulada/areaMuni_sqkm)/10^3
qui gen ProduccionAccPerArea2=ProduccionAccPerArea*ProduccionAccPerArea
gen D_ProduccionPerArea=(ProduccionPerArea>0) if !missing(ProduccionPerArea)



*Set up labels for latex
label var AreaMinadaProp "Mining Area/Municipality Area"
label var AreaMinadaProp2 "{[Mining Area/Municipality Area]}\$^2\$"
label var ProduccionPerCapita "Production/Population"
label var ProduccionPerCapita2 "{[Production/Population]}\$^2\$"
label var ProduccionPerArea "Production/Municipality Area"
label var ProduccionPerArea2 "{[Production/Municipality Area]}\$^2\$"
label var ProduccionAccPerArea "Accumulated Production"
label var ProduccionAccPerArea2 "{[Accumulated Production/Municipality Area]}\$^2\$"

label var D_ProduccionPerArea "Production/Municipality Area \$>0\$"



local var_indep AreaMinadaProp ProduccionAccPerArea
*Uncomment if doing arena
*local var_indep AreaMinadaProp ExposicionMuni Upstream ProduccionAccPerArea Arena_AreaMinadaProp Arena_ExposicionMuni Arena_Upstream
drop MineriaArea MineriaProd 
foreach var_indep of varlist `var_indep'{
qui gen D_`var_indep'=(`var_indep'>0) if !missing(`var_indep')
}

label var D_AreaMinadaProp "Mining Area/Municipality Area \$>0\$"
label var D_ProduccionPerArea "Production \$>0\$"
label var D_ProduccionAccPerArea "Production Accumulated \$>0\$"
**Uncomment if doing arena
/*
label var D_Arena_AreaMinadaProp "Mining Area/Municipality Area \$>0\$"
label var D_Arena_ExposicionMuni "Municipality exposure \$>0\$"
label var D_Arena_Upstream "Upstream exposure \$>0\$"
*/

qui compress

merge m:1 codmpio using "$base_out/Munis_mineros.dta"
drop if _merge==2
gen NearGoldMine=1 if _merge==3
replace NearGoldMine=0 if _merge==1
drop _merge
qui compress
qui gen region=gandina+2*gcaribe+3*gpacifica+4*gorinoquia+5*gamazonia
qui keep if region<=3
keep if NearGoldMine==1
drop if AreaMinadaProp>$areapropcut
qui gen fertilidad=Nacimientos/PoblacionMuni


drop if ano==2014
drop if Quarter==.

local var_indepList AreaMinada_sqkm Produccion
foreach var_indep of varlist `var_indepList'{
qui gen D_`var_indep'=(`var_indep'>0) if !missing(`var_indep')
}
label var D_AreaMinada_sqkm "Mining Area \$>0\$"
label var D_Produccion "Production \$>0\$"



qui drop Upstream* Exp*
qui replace APGAR_BAJO=100*APGAR_BAJO
replace LBW=100*LBW
replace fertilidad=fertilidad*100
gen Z_TALLA_NAC=(TALLA_NAC-49.5)/1.8779
gen STUNT=(Z_TALLA_NAC<-2) if !missing(Z_TALLA_NAC)
label var STUNT "Stunted"

save "$base_out/PanelMunicipalEEVV_Stata.dta",replace

qui use "$base_out/DataCompleta.dta",clear
*Restrict analysis to years we have production AND area data
drop if ANO>2012
drop if ANO<2001
*Drop capitals
*drop if mod(CODIGO_DANE_M,1000)==1
replace APGAR_BAJO=100*APGAR_BAJO


*Sumstat Babies
drop if APGAR_BAJO==.
drop if PESO_NAC==.
replace TALLA_NAC=. if TALLA_NAC<40
replace TALLA_NAC=. if TALLA_NAC>55
drop if TALLA_NAC==. 
drop if EDAD_MADRE==.
drop if MadreSoltera==.
drop if EduMadrePostPrimaria==.


qui compress 
drop DPTO_NACI MUNIC_NACI SEXO DPTO_R MUNIC_R MES
rename MES2 MES
rename ANO ano
rename CODIGO_DANE_M codmpio 

** I took MineriaArea_DateStr MineriaProd_DateStr Upstream_DateStr out to test
qui merge m:1 codmpio ano Quarter using "$base_out/PanelMunicipalEEVV_Stata.dta", keepus(areaMuni_sqkm PoblacionMuni MineriaArea_Ever MineriaProd_Ever RiverArea_Ever RiverProd_Ever ///
gandina gcaribe gpacifica gorinoquia gamazonia pobl_rur pobl_urb pobl_tot indrural areaoficialkm2 gpc pobreza gini nbi altura discapital dismdo disbogota codmdo minorias parques religioso estado otras ///
pehosclinc pecsaludc pepsaludc agua erosion aptitud cover_land_c2 cover_land_s8 cover_land_t8 used_land_3)
drop if _merge==2
drop if codmpio==.
drop _merge

rename AreaMinada_M AreaMinada_sqkm
gen AreaMinadaProp=100*AreaMinada_sqkm/areaMuni_sqkm


drop if AreaMinadaProp>$areapropcut
rename ProduccionAprox_M Produccion
rename ProduccionAprox_Acc_M Produccion_Acc

gen region=gandina+2*gcaribe+3*gpacifica+4*gorinoquia+5*gamazonia
keep if region<=3



gen Semana_Naci=week(FECHA_NAC)


gen ProduccionPerCapita=(Produccion/PoblacionMuni)/10^2
gen ProduccionPerArea=(Produccion/areaMuni_sqkm)/10^3
gen ProduccionPerArea2=ProduccionPerArea*ProduccionPerArea
gen ProduccionAccPerArea=(Produccion_Acc/areaMuni_sqkm)/10^3
gen ProduccionAccPerArea2=ProduccionAccPerArea*ProduccionAccPerArea
gen D_ProduccionPerArea=(ProduccionPerArea>0) if !missing(ProduccionPerArea)
gen AreaMinadaProp2=AreaMinadaProp*AreaMinadaProp
gen ProduccionPerCapita2=ProduccionPerCapita*ProduccionPerCapita

foreach muho in Muni {
foreach arpr in Area  {
foreach i in 5 10 20 {

qui rename Upstream`arpr'`muho'`i'KM River`arpr'`muho'`i'KM
qui rename Exp`arpr'`muho'`i'KM Prox`arpr'`muho'`i'KM

}
}
}

foreach muho in Muni {
foreach arpr in Prod {
foreach i in 5 10 20 {

qui drop Upstream`arpr'`muho'`i'KM 
qui drop Exp`arpr'`muho'`i'KM 

}
}
}

foreach arpr in Prod {
foreach i in 5 10 20 {

qui rename AI_Upstream`arpr'Muni`i'KM River`arpr'AIMuni`i'KM
qui rename AI_Exp`arpr'Muni`i'KM Prox`arpr'AIMuni`i'KM

}
}



foreach muho in Muni {
foreach prri in Prox River {
foreach arpr in Area ProdAI {
foreach i in 5 10 20 {

qui replace `prri'`arpr'Muni`i'KM=`prri'`arpr'Muni`i'KM/10^6

qui sum `prri'`arpr'Muni`i'KM if `prri'`arpr'Muni`i'KM>0,d 
replace `prri'`arpr'Muni`i'KM=0 if `prri'`arpr'Muni`i'KM<r(p50)/100 & `prri'`arpr'Muni`i'KM!=.
replace `prri'`arpr'Muni`i'KM=r(p99) if `prri'`arpr'Muni`i'KM>r(p99) & `prri'`arpr'Muni`i'KM!=.

*gen `prri'`arpr'`muho'`i'KM2=`prri'`arpr'`muho'`i'KM*`prri'`arpr'`muho'`i'KM
gen D_`prri'`arpr'`muho'`i'KM=(`prri'`arpr'`muho'`i'KM>0) if !missing(`prri'`arpr'`muho'`i'KM)
}
}
}
}


foreach i in 5 10 20 {
label var ProxAreaMuni`i'KM "Proximity exposure area municipality `i' km"
*label var ProxAreaMuni`i'KM2 "{[Proximity exposure area municipality `i' km]}\$^2\$"
*label var ProxProdMuni`i'KM "Proximity exposure production municipality `i' km"
*label var ProxProdMuni`i'KM2 "{[Proximity exposure production municipality `i' km]}\$^2\$"
label var RiverAreaMuni`i'KM "River exposure area municipality `i' km"
*label var RiverAreaMuni`i'KM2 "{[River exposure area municipality `i' km]}\$^2\$"
*label var RiverProdMuni`i'KM "River exposure production municipality `i' km"
*label var RiverProdMuni`i'KM2 "{[River exposure production municipality `i' km]}\$^2\$"
label var D_ProxAreaMuni`i'KM "Proximity exposure area municipality `i' km \$>0\$"
*label var D_ProxProdMuni`i'KM "Proximity exposure production municipality `i' km \$>0\$"
label var D_RiverAreaMuni`i'KM "River exposure area municipality `i' km \$>0\$"
*label var D_RiverProdMuni`i'KM "River exposure production municipality `i' km \$>0\$"
/*
label var ProxAreaHosp`i'KM "Proximity exposure area hospital `i' km"
label var ProxAreaHosp`i'KM2 "{[Proximity exposure area hospital `i' km]}\$^2\$"
*label var ProxProdHosp`i'KM "Proximity exposure production hospital `i' km"
*label var ProxProdHosp`i'KM2 "{[Proximity exposure production hospital `i' km]}\$^2\$"
label var RiverAreaHosp`i'KM "River exposure area hospital `i' km"
label var RiverAreaHosp`i'KM2 "{[River exposure area hospital `i' km]}\$^2\$"
*label var RiverProdHosp`i'KM "River exposure production hospital `i' km"
*label var RiverProdHosp`i'KM2 "{[River exposure production hospital `i' km]}\$^2\$"
label var D_ProxAreaHosp`i'KM "Proximity exposure area hospital `i' km \$>0\$"
*label var D_ProxProdHosp`i'KM "Proximity exposure production hospital `i' km \$>0\$"
label var D_RiverAreaHosp`i'KM "River exposure area hospital `i' km \$>0\$"
*/
*label var D_RiverProdHosp`i'KM "River exposure production hospital `i' km \$>0\$"
label var RiverProdAIMuni`i'KM "River exposure production AI `i' km"
*label var RiverProdAIMuni`i'KM2 "{[River exposure production AI `i' km]}\$^2\$"
label var D_ProxProdAIMuni`i'KM "Proximity exposure production AI `i' km \$>0\$"
label var D_RiverProdAIMuni`i'KM "River exposure production AI `i' km \$>0\$"
label var ProxProdAIMuni`i'KM "Proximity exposure production AI `i' km"
*label var ProxProdAIMuni`i'KM2 "{[Proximity exposure production AI `i' km]}\$^2\$"
}

*Set up labels for latex
label var AreaMinadaProp "Mining Area/Municipality Area"
label var AreaMinadaProp2 "{[Mining Area/Municipality Area]}\$^2\$"

label var ProduccionPerCapita "Production/Population"
label var ProduccionPerCapita2 "{[Production/Population]}\$^2\$"
label var ProduccionPerArea "Production/Municipality Area"
label var ProduccionPerArea2 "{[Production/Municipality Area]}\$^2\$"
label var ProduccionAccPerArea "Accumulated Production"
label var ProduccionAccPerArea2 "{[Accumulated Production/Municipality Area]}\$^2\$"

label var D_ProduccionPerArea "Production/Municipality Area \$>0\$"


local var_indep AreaMinadaProp ProduccionAccPerArea 
foreach var_indep of varlist `var_indep'{
qui gen D_`var_indep'=(`var_indep'>0) if !missing(`var_indep')
}

label var D_AreaMinadaProp "Mining Area/Municipality Area \$>0\$"
label var D_ProduccionPerArea "Production \$>0\$"
label var D_ProduccionAccPerArea "Production Accumulated \$>0\$"


**Uncomment if doing Arena
/*
gen Arena_AreaMinadaProp=Arena_AreaMinadaKm2/areaMuni_km2
replace  Arena_ExposicionMuni= Arena_ExposicionMuni/10^5
replace  Arena_Upstream= Arena_Upstream/10^5
gen Arena_Upstream2=Arena_Upstream*Arena_Upstream
gen Arena_AreaMinadaProp2=Arena_AreaMinadaProp*Arena_AreaMinadaProp
gen Arena_ExposicionMuni2=Arena_ExposicionMuni*Arena_ExposicionMuni
label var Arena_AreaMinadaProp "Mining Area/Municipality Area"
label var Arena_AreaMinadaProp2 "{[Mining Area/Municipality Area]}\$^2\$"
label var Arena_ExposicionMuni "Proximity exposure"
label var Arena_ExposicionMuni2 "{[Proximity exposure]}\$^2\$"
label var Arena_Upstream "River exposure"
label var Arena_Upstream2 "{[River exposure]}\$^2\$"

local var_indep Arena_AreaMinadaProp Arena_ExposicionMuni Arena_Upstream
foreach var_indep of varlist `var_indep'{
gen D_`var_indep'=(`var_indep'>0) if !missing(`var_indep')
}
label var D_Arena_AreaMinadaProp "Sand Mining Area/Municipality Area \$>0\$"
label var D_Arena_ExposicionMuni "Sand Municipality exposure \$>0\$"
label var D_Arena_Upstream "Sand Upstream exposure \$>0\$"
*/



replace LBW=100*LBW
qui gen Z_TALLA_NAC=(TALLA_NAC-49.9)/1.8931 if MASC==1
qui replace Z_TALLA_NAC=(TALLA_NAC-49.1)/1.8627 if MASC==0
gen STUNT=(Z_TALLA_NAC<-2) if !missing(Z_TALLA_NAC)

label var Z_TALLA_NAC "(Z-score) Height"
label var STUNT "Stunted"

qui rename ano ANO
qui merge m:1 ANO MES using "$base_out/PriceGold.dta",keepus(PriceRealMA)
qui rename ANO ano
qui drop if _merge==2
qui drop _merge  
qui compress


qui gen YrMonth=ym(ano,MES)
qui gen YrQrt=yq(ano,Quarter)

local var_indepList AreaMinadaProp ProduccionPerArea ProduccionAccPerArea

foreach var_indep of varlist AreaMinadaProp{

qui gen `var_indep'xprice=`var_indep'*PriceRealMA
qui gen `var_indep'2xprice=`var_indep'2*PriceRealMA
qui gen D_`var_indep'xprice=D_`var_indep'*PriceRealMA
}

label var APGAR_BAJO "Low APGAR"
label var PESO_NAC "Weight (gr)"
label var TALLA_NAC "Height (cm)"
label var EDAD_MADRE "Mother's age"
label var MadreSoltera "Single mother"
label var EduMadrePostPrimaria "Mother has post-primary education"
label var AreaMinadaPropxprice "{[Mining Area/Municipality Area]} x Price"
label var AreaMinadaProp2xprice "{[Mining Area/Municipality Area]}\$^2\$ x Price"
label var D_AreaMinadaPropxprice "{[Mining Area/Municipality Area \$> 0\$]} x Price"


** Adding HW vars
gen dia=day(FECHA_NAC)
gen wdate=mdy(MES,dia,ano)
sort wdate
qui merge m:1 wdate using "$base_out/Closest_hw"
drop if _merge!=3
drop _merge
gen wgesthw=ceil((wdate-closest_hw)/7)

replace wgesthw=. if SEMANAS==.
qui label var wgesthw "Week during gestation when holy week happens"
gen ihw=1 if wgesthw<=SEMANAS & wgesthw!=.
qui replace ihw=0 if wgesthw>SEMANAS & wgesthw!=.
label var ihw "Holy Week during gestation"

qui gen D_ProxAreaMuni510KM=(D_ProxAreaMuni10KM==1 & D_ProxAreaMuni5KM==0)
qui gen D_ProxAreaMuni1020KM=(D_ProxAreaMuni20KM==1 & D_ProxAreaMuni10KM==0)
qui gen D_RiverAreaMuni510KM=(D_RiverAreaMuni10KM==1 & D_RiverAreaMuni5KM==0)
qui gen D_RiverAreaMuni1020KM=(D_RiverAreaMuni20KM==1 & D_RiverAreaMuni10KM==0)

label var D_ProxAreaMuni510KM "Proximity exposure area municipality 5-10 km \$>0\$"
label var D_ProxAreaMuni1020KM "Proximity exposure area municipality 10-20 km \$>0\$"
label var D_RiverAreaMuni510KM "River exposure area municipality 5-10 km \$>0\$"
label var D_RiverAreaMuni1020KM "River exposure area municipality 10-20 km \$>0\$"

qui gen D_ProxProdAIMuni510KM=(D_ProxProdAIMuni10KM==1 & D_ProxProdAIMuni5KM==0)
qui gen D_ProxProdAIMuni1020KM=(D_ProxProdAIMuni20KM==1 & D_ProxProdAIMuni10KM==0)
qui gen D_RiverProdAIMuni510KM=(D_RiverProdAIMuni10KM==1 & D_RiverProdAIMuni5KM==0)
qui gen D_RiverProdAIMuni1020KM=(D_RiverProdAIMuni20KM==1 & D_RiverProdAIMuni10KM==0)

replace PriceRealMA=PriceRealMA*2.36



merge m:1 codmpio using "$base_out/Munis_mineros.dta"
drop if _merge==2
gen NearGoldMine=1 if _merge==3
replace NearGoldMine=0 if _merge==1
drop _merge
qui compress
/*
merge m:1 codmpio using "$base_out/Munis_mineros_arena.dta"
drop if _merge==2
gen NearArenaMine=1 if _merge==3
replace NearArenaMine=0 if _merge==1
drop _merge
compress
drop if NearArenaMine==0 & NearGoldMine==0
*/
qui drop if NearGoldMine==0
save "$base_out/DataCompleta_Stata.dta",replace


qui collapse (count) APGAR_BAJO PESO_NAC TALLA_NAC LBW VLBW GEST_COMPLETA PartoHospital MadreSoltera EDAD_MADRE PartoEspontaneo MASC MadreMenor14 Madre14_17 ConsultasPreMayor4 EduMadrePostPrimaria , by(ano Quarter codmpio) 
rename * =_Count
rename ano_Count ano
rename Quarter_Count Quarter
rename codmpio_Count codmpio
qui compress
save "$base_out/NonMissingsCount.dta", replace






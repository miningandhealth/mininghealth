use "$base_out\PanelMunicipalEEVV_Stata.dta",clear
drop MineriaArea_Ever MineriaProd_Ever Upstream_Ever Exposicion_Ever MineriaArea_DateStr MineriaProd_DateStr Upstream_DateStr Exposicion_DateStr
tsset codmpio YrQrt 
tsfill
global areapropcut=0.15
gen region=gandina+2*gcaribe+3*gpacifica+4*gorinoquia+5*gamazonia
drop if AreaMinadaProp>$areapropcut
replace APGAR_BAJO=100*APGAR_BAJO
keep if region<=3
keep if NearGoldMine==1
*Sumstat Babies
drop if APGAR_BAJO==.
drop if PESO_NAC==.
drop if TALLA_NAC==. 
drop if EDAD_MADRE==.
drop if MadreSoltera==.
drop if EduMadrePostPrimaria==.
*First determined date at which things happen for the first time
egen FirstArea=min(YrQrt)  if AreaMinadaKm2>0 & !missing(AreaMinadaKm2) , by(codmpio)
egen FirstProd=min(YrQrt)  if Produccion>0 & !missing(Produccion) , by(codmpio)
egen FirstExpo=min(YrQrt)  if ExposicionMuni>0 & !missing(ExposicionMuni) , by(codmpio)
egen FirstUpstream=min(YrQrt)  if Upstream>0 & !missing(Upstream) , by(codmpio)

format FirstArea %tq
format FirstProd %tq
format FirstExpo %tq
format FirstUpstream %tq

 
collapse (mean) FirstArea FirstProd FirstExpo FirstUpstream, by(codmpio)
save "$base_out\FirstDates.dta",replace

use "$base_out\PanelMunicipalEEVV_Stata.dta",clear
drop MineriaArea_Ever MineriaProd_Ever Upstream_Ever Exposicion_Ever MineriaArea_DateStr MineriaProd_DateStr Upstream_DateStr Exposicion_DateStr

merge m:1 codmpio using "$base_out\FirstDates.dta"
drop _merge
format FirstArea %tq
format FirstProd %tq
format FirstExpo %tq
format FirstUpstream %tq

tsset codmpio YrQrt 
tsfill

gen TimeToArea=YrQrt-FirstArea
gen TimeToProd=YrQrt-FirstProd
gen TimeToExpo=YrQrt-FirstExpo
gen TimeToUpst=YrQrt-FirstUpstream

global areapropcut=0.15
gen region=gandina+2*gcaribe+3*gpacifica+4*gorinoquia+5*gamazonia
drop if AreaMinadaProp>$areapropcut
replace APGAR_BAJO=100*APGAR_BAJO
keep if region<=3
keep if NearGoldMine==1
*Sumstat Babies
drop if APGAR_BAJO==.
drop if PESO_NAC==.
drop if TALLA_NAC==. 
drop if EDAD_MADRE==.
drop if MadreSoltera==.
drop if EduMadrePostPrimaria==.
label var APGAR_BAJO "Low APGAR"
label var PESO_NAC "Weight (gr)"
label var TALLA_NAC "Height (cm)"
replace PriceReal=PriceReal*2.36

preserve
keep if TimeToArea<0 | TimeToArea==.
capture matrix drop Coefs
capture matrix drop SE
forvalues i=1/54{
drop D_AreaMinadaProp
gen D_AreaMinadaProp=0
replace D_AreaMinadaProp=1 if TimeToArea>=-`i'
areg APGAR_BAJO D_AreaMinadaProp i.YrQrt region#c.YrQrt  [pw=Nacimientos], absorb(codmpio) vce(cluster codmpio)
 matrix A=e(b)
 matrix B=e(V)
mat Coefs = nullmat(Coefs), A[1,1]
mat SE = nullmat(SE), sqrt(B[1,1])
}
 
restore
preserve
keep if TimeToExpo<0 | TimeToExpo==.
capture matrix drop Coefs2
capture matrix drop SE2
forvalues i=1/54{
drop D_ExposicionMuni
gen D_ExposicionMuni=0
replace D_ExposicionMuni=1 if TimeToExpo>=-`i'
areg APGAR_BAJO D_ExposicionMuni i.YrQrt region#c.YrQrt  [pw=Nacimientos], absorb(codmpio) vce(cluster codmpio)
 matrix A=e(b)
 matrix B=e(V)
mat Coefs2 = nullmat(Coefs2), A[1,1]
mat SE2 = nullmat(SE2), sqrt(B[1,1])
}
restore
preserve
keep if TimeToUpst<0 | TimeToUpst==.
capture matrix drop Coefs3
capture matrix drop SE3
forvalues i=1/54{
drop D_Upstream
gen D_Upstream=0
replace D_Upstream=1 if TimeToUpst>=-`i'
areg APGAR_BAJO D_Upstream i.YrQrt region#c.YrQrt  [pw=Nacimientos], absorb(codmpio) vce(cluster codmpio)
 matrix A=e(b)
 matrix B=e(V)
mat Coefs3 = nullmat(Coefs3), A[1,1]
mat SE3 = nullmat(SE3), sqrt(B[1,1])
}


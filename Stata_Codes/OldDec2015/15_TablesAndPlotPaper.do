*global mipath "C:\Users\santi\Copy\PYP_Birth\"
*global gitpath "C:\Users\santi\Documents\mininghealth\"

*global mipath "C:\Users\Mauricio\Copy\MiningAndBirth"
*global gitpath  "C:\Users\Mauricio\Documents\git\mininghealth"

*global gitpath "E:\Users\Mauricio\Documents\Git\mininghealth\"
*global mipath  "H:\Copy\MiningAndBirth"


global base_out   "$mipath\CreatedData"
global latexcodes     "$gitpath\LaTeX\201510_2ndDraft"
global areapropcut=0.15
use "$base_out\DataCompleta_Stata.dta", clear
*sample 5
*save "$base_out\DataTest.dta", replace
*use "$base_out\DataTest.dta", clear


set matsize 100

gen YrMonth=ym(ano,MES)
gen YrQrt=yq(ano,Quarter)
gen region=gandina+2*gcaribe+3*gpacifica+4*gorinoquia+5*gamazonia

drop if AreaMinadaProp>$areapropcut
replace APGAR_BAJO=100*APGAR_BAJO
keep if region<=3
keep if NearGoldMine==1
*Sumstat Babies
drop if APGAR_BAJO==.
drop if PESO_NAC==.
drop if TALLA_NAC==. 
drop if EDAD_MADRE==.
drop if MadreSoltera==.
drop if EduMadrePostPrimaria==.
label var APGAR_BAJO "Low APGAR"
label var PESO_NAC "Weight (gr)"
label var TALLA_NAC "Height (cm)"
label var EDAD_MADRE "Mother's age"
label var MadreSoltera "Single mother"
label var EduMadrePostPrimaria "Mother has post-primary education"

eststo clear
estpost tabstat APGAR_BAJO PartoHospital PESO_NAC TALLA_NAC EDAD_MADRE MadreSoltera EduMadrePostPrimaria, statistics(mean median sd max min count) columns(statistics)
esttab using "$latexcodes\SummaryBirth.tex", cells("mean(fmt(a2) label(Mean)) p50(fmt(a2) label(Median)) sd(fmt(a2) label(Std. Dev.)) min(fmt(a2) label(Min)) max(fmt(a2) label(Max)) count(fmt(a2) label(N))") nomtitle nonumber label  replace noobs booktabs
replace PriceRealMA=PriceRealMA*2.36
keep ConsultasPreMayor4 RegimenContributivo RegimenSubsidiado PriceRealMA Z_TALLA_NAC ano MES region D_AreaMinadaProp D_ProduccionPerArea D_ExposicionMuni D_Upstream D_Arena_ExposicionMuni D_Arena_Upstream AreaMinadaProp ProduccionPerArea ProduccionAccPerArea ExposicionMuni Upstream AreaMinadaProp2 ProduccionPerArea2 ProduccionAccPerArea2 ExposicionMuni2 Upstream2 Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE APGAR_BAJO LBW codmpio FECHA_NAC Z_TALLA_NAC
replace ExposicionMuni=ExposicionMuni*100
replace Upstream=Upstream*100
replace AreaMinadaProp=AreaMinadaProp*100



*Sumstat AreaMinada
gen AreaMinadaPropPositiva=AreaMinadaProp if AreaMinadaProp>0 & !missing(AreaMinadaProp)
eststo clear
estpost tabstat AreaMinadaProp D_AreaMinadaProp AreaMinadaPropPositiva ProduccionAccPerArea , statistics(mean median sd max min count) columns(statistics)
esttab using "$latexcodes\summaryIndep_IndividualTodos.tex", cells("mean(fmt(a2) label(Mean)) p50(fmt(a2) label(Median)) sd(fmt(a2) label(Std. Dev.)) min(fmt(a2) label(Min)) max(fmt(a2) label(Max)) count(fmt(a2) label(N))") nomtitle nonumber label  replace noobs booktabs



*Graphs APGAR AreaMinada
/*
quietly areg APGAR_BAJO i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE region#c.FECHA_NAC, absorb(codmpio)
predict resid_APGAR,resid
quietly areg AreaMinadaProp i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE region#c.FECHA_NAC, absorb(codmpio)
predict resid_AreaMinadaProp, resid

lpoly resid_APGAR resid_AreaMinadaProp, ci noscatter addplot((histogram resid_AreaMinadaProp, yaxis(2) fraction))
graph export "resid_apgar_areamin.pdf", replace

lpoly resid_APGAR AreaMinadaProp, ci noscatter addplot((histogram AreaMinadaProp, yaxis(2) fraction))
graph export "resid_apgar_Rawareamin.pdf", replace


lpoly APGAR AreaMinadaProp, ci msize(zero) noscatter addplot((histogram AreaMinadaProp, yaxis(2) fraction))
graph export "raw_apgar_areamin.pdf", replace

quietly areg APGAR_BAJO i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE region#c.FECHA_NAC if AreaMinadaProp>0, absorb(codmpio)
predict resid_APGAR_g0,resid 
quietly areg AreaMinadaProp i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE region#c.FECHA_NAC if AreaMinadaProp>0, absorb(codmpio)
predict resid_AreaMinadaProp_g0, resid 
lpoly resid_APGAR resid_AreaMinadaProp if AreaMinadaProp>0, ci noscatter addplot((histogram resid_AreaMinadaProp, yaxis(2) fraction))
graph export "resid_apgar_areamin_g0.pdf", replace
*/




local var_indepList AreaMinadaProp ProduccionPerArea ProduccionAccPerArea

foreach var_indep of varlist AreaMinadaProp{

qui gen `var_indep'xprice=`var_indep'*PriceRealMA
qui gen `var_indep'2xprice=`var_indep'2*PriceRealMA
qui gen D_`var_indep'xprice=D_`var_indep'*PriceRealMA
}
label var AreaMinadaPropxprice "{[Mining Area/Municipality Area]} x Price"
label var AreaMinadaProp2xprice "{[Mining Area/Municipality Area]}\$^2\$ x Price"
label var D_AreaMinadaPropxprice "{[Mining Area/Municipality Area \$> 0\$]} x Price"
local control1 i.ano i.Semana_Naci 
local control2 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
local control3 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE region#c.FECHA_NAC
local control4 i.ano i.Semana_Naci region#c.FECHA_NAC

**Placebo Tests
*Linear regressions area-production

*PartoHospital MadreMenor14 Madre14_17 ConsultasPreMayor4 RegimenContributivo RegimenSubsidiado EduMadrePostSecundaria
/*
eststo pa1: xi: areg MadreSoltera AreaMinadaProp `control4' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local rtrend "Yes"
eststo pa2: xi: areg EduMadrePostPrimaria AreaMinadaProp `control4' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local rtrend "Yes"
eststo pa3: xi: areg EDAD_MADRE AreaMinadaProp `control4' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local rtrend "Yes"
eststo pa4: xi: areg RegimenContributivo AreaMinadaProp `control4' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local rtrend "Yes"

eststo pb1: xi: areg MadreSoltera D_AreaMinadaProp `control4' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local rtrend "Yes"
eststo pb2: xi: areg EduMadrePostPrimaria D_AreaMinadaProp `control4' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local rtrend "Yes"
eststo pb3: xi: areg EDAD_MADRE D_AreaMinadaProp `control4' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local rtrend "Yes"
eststo pb4: xi: areg RegimenContributivo D_AreaMinadaProp `control4' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local rtrend "Yes"
*/

eststo pc1: qui xi: areg MadreSoltera D_ExposicionMuni D_Upstream `control4' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local rtrend "Yes"
eststo pc2: qui xi: areg EduMadrePostPrimaria D_ExposicionMuni D_Upstream `control4' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local rtrend "Yes"
eststo pc3: qui xi: areg EDAD_MADRE D_ExposicionMuni D_Upstream `control4' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local rtrend "Yes"
eststo pc4: qui xi: areg RegimenContributivo D_ExposicionMuni D_Upstream `control4' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local rtrend "Yes"

/*
estout pa1 pa2 pa3 pa4 using "$latexcodes\AreaMinadaProp_Placebo.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( AreaMinadaProp )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

estout pb1 pb2 pb3 pb4 using "$latexcodes\D_AreaMinadaProp_Placebo.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( D_AreaMinadaProp )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace
*/

estout pc1 pc2 pc3 pc4 using "$latexcodes\Exposure_Placebo.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( D_ExposicionMuni D_Upstream )  stats(N N_clust ymean r2 munife timefe rtrend, fmt(a2 a2 a2 a2 %~#s %~#s %~#s) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" "Municipality FE" "Time FE" "Reg. Trends")) replace
*note("\specialcell{Clustered standard errors, by municipality, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") ///

********
*areg APGAR_BAJO PCArea* `control3' , absorb(codmpio) vce(cluster codmpio)

***Heterogeneous Effects*****

/*
local N=5
forval i=1/`N'{
qui gen ibinAreaProp`i'=0
replace ibinAreaProp`i'=1 if AreaMinadaProp>(`i'-1)*($areapropcut )/`N'  & AreaMinadaProp<=(`i')*($areapropcut )/`N'
}

forval i=1/`N'{
qui gen iboxAreaProp`i'=0
replace iboxAreaProp`i'=1 if AreaMinadaProp>(`i'-1)*($areapropcut )/`N'  & AreaMinadaProp<=($areapropcut) 
}

replace PCArea1=PCArea1+PCArea2+PCArea3+PCArea4+PCArea5+PCArea6+PCArea7+PCArea8+PCArea9+PCArea10
replace PCArea2=PCArea2+PCArea3+PCArea4+PCArea5+PCArea6+PCArea7+PCArea8+PCArea9+PCArea10
replace PCArea3=PCArea3+PCArea4+PCArea5+PCArea6+PCArea7+PCArea8+PCArea9+PCArea10
replace PCArea4=PCArea4+PCArea5+PCArea6+PCArea7+PCArea8+PCArea9+PCArea10
replace PCArea5=PCArea5+PCArea6+PCArea7+PCArea8+PCArea9+PCArea10
replace PCArea6=PCArea6+PCArea7+PCArea8+PCArea9+PCArea10
replace PCArea7=PCArea7+PCArea8+PCArea9+PCArea10
replace PCArea8=PCArea8+PCArea9+PCArea10
replace PCArea9=PCArea9+PCArea10
replace PCArea10=PCArea10


label var PCArea1 "Proportion of area mined>0"
label var PCArea2 "Proportion of area mined>20th percentile"
label var PCArea3 "Proportion of area mined>40th percentile"
label var PCArea4 "Proportion of area mined>60th percentile"
label var PCArea5 "Proportion of area mined>80th percentile"

label var ibinAreaProp1 "Proportion of area mined between 0 and 2.8\%"
label var ibinAreaProp2 "Proportion of area mined between 2.8\% and 5.6\%"
label var ibinAreaProp3 "Proportion of area mined between 5.6\% and 8.4\%"
label var ibinAreaProp4 "Proportion of area mined between 8.4\% and 11.2\%"
label var ibinAreaProp5 "Proportion of area mined between 11.2\% and 14\%"

label var iboxAreaProp1 "Proportion of area mined greater than 0\%"
label var iboxAreaProp2 "Proportion of area mined greater than 2.8\% "
label var iboxAreaProp3 "Proportion of area mined greater than 5.6\% "
label var iboxAreaProp4 "Proportion of area mined greater than 8.4\% "
label var iboxAreaProp5 "Proportion of area mined greater than 11.2\% "
set more off
local control3 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE region#c.FECHA_NAC
eststo clear
eststo h1: xi: areg APGAR_BAJO ibinAreaProp* `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
eststo h2:xi: areg APGAR_BAJO iboxAreaProp* `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estout h1 using "$latexcodes\Hetero1.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(ibinAreaProp*)  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace
estout h2 using "$latexcodes\Hetero2.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( iboxAreaProp*)  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs."  "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace


*/

sum AreaMinadaProp if AreaMinadaProp>0,d
xtile PcentileArea=AreaMinadaProp if AreaMinadaProp>0, nquantiles(10)
tab PcentileArea, gen(PCArea)
foreach var of varlist PCArea1-PCArea10{
replace `var'=0 if `var'==.
}



qui areg APGAR_BAJO PCArea* `control3' , absorb(codmpio) vce(cluster codmpio)
coefplot, baselevels keep(PCArea*) ci rename(PCArea1="1" PCArea2="2" PCArea3="3" PCArea4="4" PCArea5="5"  ///
PCArea6="6" PCArea7="7" PCArea8="8" PCArea9="9" PCArea10="10") ///
   vertical yline(0) xtitle("Decile") ytitle("Coefficient on APGAR") title("Intensive Margin")
 graph export "$latexcodes\\HeterogeneousImpactQ10.pdf", replace

 drop PcentileArea PCArea*
xtile PcentileArea=AreaMinadaProp if AreaMinadaProp>0, nquantiles(5)
tab PcentileArea, gen(PCArea)
foreach var of varlist PCArea1-PCArea5{
replace `var'=0 if `var'==.
}



 areg APGAR_BAJO PCArea* `control3' , absorb(codmpio) vce(cluster codmpio)
coefplot, baselevels keep(PCArea*) ci rename(PCArea1="1" PCArea2="2" PCArea3="3" PCArea4="4" PCArea5="5") ///
   vertical yline(0) xtitle("Quantile") ytitle("Coefficient on APGAR") title("Intensive Margin")
 graph export "$latexcodes\\HeterogeneousImpactQ5.pdf", replace
 

*Linear regressions area-production
foreach var_indep of varlist AreaMinadaProp{

eststo clear

eststo m1: quietly xi: areg APGAR_BAJO `var_indep' `control1' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm


eststo m2: quietly xi: areg APGAR_BAJO `var_indep' `control2' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm


 eststo m3: quietly xi: areg APGAR_BAJO `var_indep' `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

estout m1 m2 m3 using "$latexcodes\IndividualMeasure_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(`var_indep')  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace


 eststo m4: quietly xi: areg APGAR_BAJO `var_indep'xprice `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

estout m3 m4 using "$latexcodes\IndividualMeasure_`var_indep'_price.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(`var_indep'* )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

eststo m5: quietly xi: areg LBW `var_indep' `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m6: quietly xi: areg Z_TALLA_NAC `var_indep' `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

estout m3 m5 m6 using "$latexcodes\IndividualMeasure_`var_indep'_lbwzh.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(`var_indep'* )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace


/*
 eststo mh: quietly xi: areg Z_TALLA_NAC `var_indep' `var_indep'2 `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local timefe "Yes"
estadd local momc "Yes"
estadd local rtrend "Yes"
estout m3 mh using "APGAR_height_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(`var_indep' `var_indep'2 )  stats(N ymean r2 timefe momc rtrend, fmt(a2 a2 a2 %~#s %~#s %~#s) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$" "Time FE" "Ind. Controls" "Reg. Trends")) replace
*/

}

foreach var_indep of varlist ProduccionPerArea ProduccionAccPerArea{

eststo clear

eststo m1: quietly xi: areg APGAR_BAJO `var_indep' `control1' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm


eststo m2: quietly xi: areg APGAR_BAJO `var_indep'  `control2' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm


 eststo m3: quietly xi: areg APGAR_BAJO `var_indep' `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

estout m1 m2 m3 using "$latexcodes\IndividualMeasure_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(`var_indep' )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace




}

*Dummy regressions Area/Prod
local var_indepList AreaMinadaProp 
foreach var_indep of varlist `var_indepList'{
eststo clear

eststo m1: quietly xi: areg APGAR_BAJO D_`var_indep' `control1' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local momc "No"
estadd local rtrend "No"



eststo m2: quietly xi: areg APGAR_BAJO D_`var_indep' `control2' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local momc "Yes"
estadd local rtrend "No"

eststo m3: quietly xi: areg APGAR_BAJO D_`var_indep' `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local momc "Yes"
estadd local rtrend "Yes"

eststo m4: quietly xi: areg APGAR_BAJO D_`var_indep'xprice `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local momc "Yes"
estadd local rtrend "Yes"
estout m1 m2 m3 using "$latexcodes\IndividualDummies_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( D_`var_indep'* )  stats(N N_clust ymean r2 munife timefe momc rtrend, fmt(a2 a2 a2 a2 %~#s %~#s %~#s %~#s) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" "Municipality FE" "Time FE" "Ind. Controls" "Reg. Trends")) replace
*note("\specialcell{Clustered standard errors, by municipality, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") ///

estout m3 m4 using "$latexcodes\IndividualDummies_`var_indep'_price.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( D_`var_indep'* )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

eststo m5: quietly xi: areg LBW D_`var_indep' `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local momc "Yes"
estadd local rtrend "Yes"

eststo m6: quietly xi: areg Z_TALLA_NAC D_`var_indep' `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local momc "Yes"
estadd local rtrend "Yes"
estout m3 m5 m6 using "$latexcodes\IndividualDummies_`var_indep'_lbwzh.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( D_`var_indep' )  stats(N N_clust ymean r2 munife timefe momc rtrend, fmt(a2 a2 a2 a2 %~#s %~#s %~#s %~#s) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" "Municipality FE" "Time FE" "Ind. Controls" "Reg. Trends")) replace
*note("\specialcell{Clustered standard errors, by municipality, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") ///

}
*/

*Proximity and river 

eststo clear

eststo m1: quietly xi: areg APGAR_BAJO ProxArea_5KM RiverArea_5KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m2: quietly xi: areg APGAR_BAJO ProxArea_20KM RiverArea_20KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m3: quietly xi: areg APGAR_BAJO ProxProd_5KM RiverProd_5KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

eststo m4: quietly xi: areg APGAR_BAJO ProxProd_20KM RiverProd_20KM  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm

estout m1 m2 m3 m4 using "$latexcodes\IndividualMeasure_Combo.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(Prox* River* )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace


*Dummy proximity-river
*gen riverxproximity=D_ExposicionMuni*D_Upstream
eststo clear
foreach var_indep of varlist ExposicionMuni Upstream{

qui gen D_`var_indep'xprice=D_`var_indep'*PriceRealMA
}

label var D_ExposicionMuni "Proximity exposure \$>0\$"
label var D_Upstream "River exposure \$>0\$"
label var D_ExposicionMunixprice "Proximity exposure \$>0\$ x Price"
label var D_Upstreamxprice "River exposure \$>0\$ x Price"
*label var riverxproximity "River \$\times\$ proximity"

eststo m1: quietly xi: areg APGAR_BAJO D_ExposicionMuni D_Upstream `control1' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local momc "No"
estadd local rtrend "No"



eststo m2: quietly xi: areg APGAR_BAJO D_ExposicionMuni D_Upstream  `control2' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local momc "Yes"
estadd local rtrend "No"

eststo m3: quietly xi: areg APGAR_BAJO D_ExposicionMuni D_Upstream  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local momc "Yes"
estadd local rtrend "Yes"
estout m1 m2 m3 using "$latexcodes\IndividualDummies_Combo.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_ExposicionMuni D_Upstream )  stats(N N_clust ymean r2 munife timefe momc rtrend, fmt(a2 a2 a2 a2 %~#s %~#s %~#s %~#s) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" "Municipality FE" "Time FE" "Ind. Controls" "Reg. Trends")) replace
*note("\specialcell{Clustered standard errors, by municipality, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") ///

eststo m4: quietly xi: areg APGAR_BAJO D_ExposicionMunixprice D_Upstreamxprice  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local momc "Yes"
estadd local rtrend "Yes"
estout m3 m4 using "$latexcodes\IndividualDummies_Combo_price.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_ExposicionMuni D_Upstream  D_ExposicionMunixprice D_Upstreamxprice )  stats(N N_clust ymean r2 munife timefe momc rtrend, fmt(a2 a2 a2 a2 %~#s %~#s %~#s %~#s) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" "Municipality FE" "Time FE" "Ind. Controls" "Reg. Trends")) replace
*note("\specialcell{Clustered standard errors, by municipality, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") ///

eststo m5: quietly xi: areg LBW D_ExposicionMuni D_Upstream  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local momc "Yes"
estadd local rtrend "Yes"

eststo m6: quietly xi: areg Z_TALLA_NAC D_ExposicionMuni D_Upstream  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local momc "Yes"
estadd local rtrend "Yes"
estout m3 m5 m6 using "$latexcodes\IndividualDummies_Combo_lbwzh.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_ExposicionMuni D_Upstream )  stats(N N_clust ymean r2 munife timefe momc rtrend, fmt(a2 a2 a2 a2 %~#s %~#s %~#s %~#s) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" "Municipality FE" "Time FE" "Ind. Controls" "Reg. Trends")) replace
*note("\specialcell{Clustered standard errors, by municipality, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") ///

/*
**
**Falsification with sand mines
*use "$base_out\DataTest.dta", clear
use "$base_out\DataCompleta_Stata.dta", clear
gen region=gandina+2*gcaribe+3*gpacifica+4*gorinoquia+5*gamazonia
drop if Arena_AreaMinadaProp>$areapropcut
replace APGAR_BAJO=100*APGAR_BAJO
keep if region<=3
keep if NearArenaMine==1
eststo clear
label var D_Arena_ExposicionMuni "Sand Proximity exposure \$>0\$"
label var D_Arena_Upstream "Sand River exposure \$>0\$"


eststo m1: quietly xi: areg APGAR_BAJO D_Arena_ExposicionMuni D_Arena_Upstream `control1' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local timefe "Yes"
estadd local momc "No"
estadd local rtrend "No"



eststo m2: quietly xi: areg APGAR_BAJO D_Arena_ExposicionMuni D_Arena_Upstream  `control2' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local timefe "Yes"
estadd local momc "Yes"
estadd local rtrend "No"

eststo m3: quietly xi: areg APGAR_BAJO D_Arena_ExposicionMuni D_Arena_Upstream  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local timefe "Yes"
estadd local momc "Yes"
estadd local rtrend "Yes"
estout m1 m2 m3 using "$latexcodes\IndividualDummies_Combo_Arena.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_Arena_ExposicionMuni D_Arena_Upstream )  stats(N ymean r2 timefe momc rtrend, fmt(a2 a2 a2 %~#s %~#s %~#s) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$" "Time FE" "Ind. Controls" "Reg. Trends")) replace
*/
/*
*Sumstat AreaMinada
eststo clear
estpost tabstat AreaMinadaProp ProduccionPerArea , statistics(mean median sd max min p95 count) columns(statistics)
esttab using "summaryIndep_IndividualTodos.tex", cells("mean(fmt(a2) label(Mean)) p50(fmt(a2) label(Median)) sd(fmt(a2) label(Std. Dev.)) min(fmt(a2) label(Min)) max(fmt(a2) label(Max)) p95(fmt(a2) label(Perc. 95)) count(fmt(a2) label(N))") nomtitle nonumber label  replace noobs booktabs

eststo clear
replace AreaMinadaProp=. if AreaMinadaProp==0
replace ExposicionMuni=. if ExposicionMuni==0
replace Upstream=. if Upstream==0
replace ProduccionPerArea=. if ProduccionPerArea==0
estpost tabstat AreaMinadaProp ProduccionPerArea , statistics(mean median sd max p95 count) columns(statistics)
esttab using "summaryIndep_IndividualPositivos.tex", cells("mean(fmt(a2) label(Mean)) p50(fmt(a2) label(Median)) sd(fmt(a2) label(Std. Dev.)) min(fmt(a2) label(Min)) max(fmt(a2) label(Max)) p95(fmt(a2) label(Perc. 95)) count(fmt(a2) label(N))") nomtitle nonumber label  replace noobs booktabs
clear all
*/
*Holy Week

*use "$base_out\DataTest.dta", clear
use "$base_out\DataCompleta_Stata.dta", clear

set matsize 100
drop if wgesthw==.
gen YrMonth=ym(ano,MES)
gen YrQrt=yq(ano,Quarter)
gen region=gandina+2*gcaribe+3*gpacifica+4*gorinoquia+5*gamazonia
drop if AreaMinadaProp>$areapropcut
replace APGAR_BAJO=100*APGAR_BAJO
keep if region<=3
keep if NearGoldMine==1
keep wgesthw ihw ano MES region D_AreaMinadaProp D_ProduccionPerArea D_ExposicionMuni D_Upstream AreaMinadaProp ProduccionPerArea ExposicionMuni Upstream AreaMinadaProp2 ProduccionPerArea2 ExposicionMuni2 Upstream2 Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE APGAR_BAJO LBW codmpio FECHA_NAC
replace ExposicionMuni=ExposicionMuni*100
replace Upstream=Upstream*100
replace AreaMinadaProp=AreaMinadaProp*100
*gen fixEf2a=string(ihw)+string(codmpio) if codmpio!=5001
*replace fixEf2a=string(codmpio) if codmpio==5001
*encode fixEf2a, gen(FE_AA)

gen ihwb=0 if wgesthw!=.
replace ihwb=1 if ihw==1 & wgesthw<=20
label var ihwb "Holyweek first 20 weeks of gestation"

gen ihwe=0 if wgesthw!=.
replace ihwe=1 if ihw==1 & ihwb==0
label var ihwe "Holyweek last weeks of gestation"

gen ihw1tr=0 if wgesthw!=.
replace ihw1tr=1 if ihw==1 & wgesthw<=13
label var ihw1tr "Holyweek 1st trimester"

gen ihw2tr=0 if wgesthw!=.
replace ihw2tr=1 if ihw==1 & wgesthw>=14 & wgesthw<=27
label var ihw2tr "Holyweek 2nd trimester"

gen ihw3tr=0 if wgesthw!=.
replace ihw3tr=1 if ihw==1 & wgesthw>=28 & wgesthw<=43
label var ihw3tr "Holyweek 3rd trimester"


gen ihw12tr=0 if wgesthw!=.
replace ihw12tr=1 if ihw==1 & wgesthw<=27
label var ihw12tr "Holyweek 1-2 trimester"


label var D_ExposicionMuni "Proximity exposure \$>0\$"
label var D_Upstream "River exposure \$>0\$"

eststo clear
local control i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE

eststo hw1: quietly xi: areg APGAR_BAJO c.D_ExposicionMuni##c.ihw  c.D_Upstream##c.ihw   `control' region#c.FECHA_NAC if wgesthw!=., absorb( codmpio) vce(cluster codmpio) 
estadd ysumm

eststo hw2 : quietly xi: areg APGAR_BAJO (c.D_ExposicionMuni  c.D_Upstream)##(c.ihw1tr c.ihw2tr c.ihw3tr)   `control' region#c.FECHA_NAC if wgesthw!=., absorb( codmpio) vce(cluster codmpio) 
estadd ysumm

eststo hw3 : quietly xi: areg APGAR_BAJO (c.D_ExposicionMuni  c.D_Upstream)##(c.ihw12tr c.ihw3tr)   `control' region#c.FECHA_NAC if wgesthw!=., absorb( codmpio) vce(cluster codmpio) 
estadd ysumm

eststo hw4 : quietly xi: areg APGAR_BAJO (c.D_ExposicionMuni  c.D_Upstream)##(c.ihwe c.ihwb)   `control' region#c.FECHA_NAC if wgesthw!=., absorb( codmpio) vce(cluster codmpio) 
estadd ysumm

estout hw1 hw2 using "$latexcodes\IndividualHWDummies_Combo_trim.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$")) replace ///
keep(D_ExposicionMuni D_Upstream ihw ihw1tr ihw2tr ihw3tr c.D_ExposicionMuni#* c.D_Upstream#* ) varl(ihw "Holy Week" c.D_ExposicionMuni#c.ihw "Proximity \$\times\$ Holy Week" c.D_Upstream#c.ihw "River  \$\times\$ Holy Week"  ///
c.D_ExposicionMuni#c.ihw1tr "Proximity \$\times\$ Holy Week 1st Trim" c.D_Upstream#c.ihw1tr "River  \$\times\$ Holy Week 1st Trim"  ///
c.D_ExposicionMuni#c.ihw2tr "Proximity \$\times\$ Holy Week 2nd Trim" c.D_Upstream#c.ihw2tr "River  \$\times\$ Holy Week 2nd Trim"  ///
c.D_ExposicionMuni#c.ihw3tr "Proximity \$\times\$ Holy Week 3rd Trim" c.D_Upstream#c.ihw3tr "River  \$\times\$ Holy Week 3rd Trim" )


estout hw1 hw3 using "$latexcodes\IndividualHWDummies_Combo_trimjunto.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$")) replace ///
keep(D_ExposicionMuni D_Upstream ihw ihw12tr ihw3tr c.D_ExposicionMuni#* c.D_Upstream#* ) varl(ihw "Holy Week" c.D_ExposicionMuni#c.ihw "Proximity \$\times\$ Holy Week" c.D_Upstream#c.ihw "River  \$\times\$ Holy Week"  ///
c.D_ExposicionMuni#c.ihw12tr "Proximity \$\times\$ Holy Week 1-2 Trim" c.D_Upstream#c.ihw12tr "River  \$\times\$ Holy Week 1-2 Trim"  ///
c.D_ExposicionMuni#c.ihw3tr "Proximity \$\times\$ Holy Week 3rd Trim" c.D_Upstream#c.ihw3tr "River  \$\times\$ Holy Week 3rd Trim" )

estout hw1 hw4 using "$latexcodes\IndividualHWDummies_Combo_20sem.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$")) replace ///
keep(D_ExposicionMuni D_Upstream ihw ihwb ihwe c.D_ExposicionMuni#* c.D_Upstream#* ) varl(ihw "Holy Week" c.D_ExposicionMuni#c.ihw "Proximity \$\times\$ Holy Week" c.D_Upstream#c.ihw "River  \$\times\$ Holy Week"  ///
c.D_ExposicionMuni#c.ihwb "Proximity \$\times\$ Holy Week \$<\$ 20 weeks" c.D_Upstream#c.ihwb "River  \$\times\$ Holy Week \$<\$ 20 weeks"  ///
c.D_ExposicionMuni#c.ihwe "Proximity \$\times\$ Holy Week  \$>\$ 20 weeks" c.D_Upstream#c.ihwe "River  \$\times\$ Holy Week \$>\$ 20 weeks" )

/*
eststo clear

eststo: quietly xi: areg APGAR_BAJO c.D_ExposicionMuni##c.ihw  c.D_Upstream##c.ihw   `control' region#c.FECHA_NAC if wgesthw!=.  , absorb( codmpio) vce(cluster codmpio) 
estadd ysumm

eststo: quietly xi: areg APGAR_BAJO c.D_ExposicionMuni##c.ihw  c.D_Upstream##c.ihw   `control' region#c.FECHA_NAC if wgesthw!=. & ano<=2006 , absorb( codmpio) vce(cluster codmpio) 
estadd ysumm

eststo: quietly xi: areg APGAR_BAJO c.D_ExposicionMuni##c.ihw  c.D_Upstream##c.ihw   `control' region#c.FECHA_NAC if wgesthw!=. & ano>2006 , absorb( codmpio) vce(cluster codmpio) 
estadd ysumm

estout using "$latexcodes\IndividualHWDummies_Combo2006.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$")) replace ///
keep(D_ExposicionMuni D_Upstream ihw c.D_ExposicionMuni#c.ihw c.D_Upstream#c.ihw ) varl(ihw "Holy Week" c.D_ExposicionMuni#c.ihw " Proximity \$\times\$ Holy Week" c.D_Upstream#c.ihw "River  \$\times\$ Holy Week")
*/

/* Holyweek regression by each week
mat def mathw=J(40,2,.)
forval i=1/40{
xi: areg APGAR_BAJO c.D_ExposicionMuni##c.ihw`i'##c.D_Upstream  `control'  if wgesthw!=., absorb( codmpio) vce(cluster codmpio) 

}
*/

***Illegal mining

use "$base_out\DataCompleta_Stata.dta", clear
*use "$base_out\DataTest.dta", clear

merge m:1 codmpio using "$base_out\ilegalMunis.dta"
set matsize 100
drop if _merge==2
replace Ilegal=0 if _merge==1
drop _merge

gen YrMonth=ym(ano,MES)
gen YrQrt=yq(ano,Quarter)
gen region=gandina+2*gcaribe+3*gpacifica+4*gorinoquia+5*gamazonia
drop if AreaMinadaProp>$areapropcut
replace APGAR_BAJO=100*APGAR_BAJO
keep if region<=3
keep if NearGoldMine==1
keep Ilegal ano MES region D_AreaMinadaProp D_ProduccionPerArea D_ExposicionMuni D_Upstream AreaMinadaProp ProduccionPerArea ExposicionMuni Upstream AreaMinadaProp2 ProduccionPerArea2 ExposicionMuni2 Upstream2 Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE APGAR_BAJO LBW codmpio FECHA_NAC
replace ExposicionMuni=ExposicionMuni*100
replace Upstream=Upstream*100
replace AreaMinadaProp=AreaMinadaProp*100

eststo clear
forvalues i=0/1{
local var_indepList AreaMinadaProp 
foreach var_indep of varlist `var_indepList'{

local control i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
eststo m`i': quietly xi: areg APGAR_BAJO `var_indep' `control' region#c.FECHA_NAC if Ilegal==`i', absorb(codmpio) vce(cluster codmpio)
estadd ysumm
}
}
eststo m2: quietly xi: areg APGAR_BAJO AreaMinadaProp `control' region#c.FECHA_NAC , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estout m2 m0 m1 using "$latexcodes\IndividualMeasure_`var_indep'_illegal.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(AreaMinadaProp )  stats(N N_clust ymean r2, fmt(a2 a2 a2 a2) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$")) replace

forvalues i=0/1{
local var_indepList AreaMinadaProp 
foreach var_indep of varlist `var_indepList'{

local control i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
eststo k`i': quietly xi: areg APGAR_BAJO D_`var_indep' `control' region#c.FECHA_NAC if Ilegal==`i', absorb(codmpio) vce(cluster codmpio)
estadd ysumm
}
}
eststo k2: quietly xi: areg APGAR_BAJO D_AreaMinadaProp `control' region#c.FECHA_NAC , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estout k2 k0 k1 using "$latexcodes\IndividualDummies_`var_indep'_illegal.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_AreaMinadaProp )  stats(N N_clust ymean r2, fmt(a2 a2 a2 a2) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$")) replace

forvalues i=0/1{

local control i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
eststo j`i': quietly xi: areg APGAR_BAJO D_Upstream D_ExposicionMuni `control' region#c.FECHA_NAC if Ilegal==`i', absorb(codmpio) vce(cluster codmpio)
estadd ysumm

}
eststo j2: quietly xi: areg APGAR_BAJO D_Upstream D_ExposicionMuni `control' region#c.FECHA_NAC , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estout j2 j0 j1 using "$latexcodes\IndividualDummies_Combo_illegal.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_Upstream D_ExposicionMuni )  stats(N N_clust ymean r2, fmt(a2 a2 a2 a2) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$")) replace


forvalues i=0/1{

local control i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
eststo l`i': quietly xi: areg APGAR_BAJO Upstream ExposicionMuni `control' region#c.FECHA_NAC if Ilegal==`i', absorb(codmpio) vce(cluster codmpio)
estadd ysumm

}
eststo l2: quietly xi: areg APGAR_BAJO Upstream ExposicionMuni `control' region#c.FECHA_NAC , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estout l2 l0 l1 using "$latexcodes\IndividualMeassures_Combo_illegal.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(Upstream ExposicionMuni )  stats(N N_clust ymean r2, fmt(a2 a2 a2 a2) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$")) replace


use "$base_out\PanelMunicipalEEVV_Stata.dta", clear

merge 1:1 ano Quarter codmpio using "$base_out\NonMissingsCount.dta"
drop _merge
gen region=gandina+2*gcaribe+3*gpacifica+4*gorinoquia+5*gamazonia
keep if region<=3
keep if NearGoldMine==1
drop if AreaMinadaProp>$areapropcut
gen fertilidad=Nacimientos/PoblacionMuni
replace ExposicionMuni=ExposicionMuni*100
replace Upstream=Upstream*100
replace AreaMinadaProp=AreaMinadaProp*100

drop if ano==2014
drop if Quarter==.
tsset codmpio YrQrt, quarterly
gen fertilidad_Count=PoblacionMuni
gen Nacimientos_Count=1


local var_indepList AreaMinadaProp ProduccionPerArea
foreach var_indep of varlist `var_indepList'{
eststo clear
local var_try1 fertilidad Nacimientos
foreach vardep of varlist `var_try1'{
eststo: quietly xi: areg `vardep' `var_indep' i.ano i.Quarter region#c.YrQrt [aw=`vardep'_Count], absorb( codmpio) vce(cluster codmpio) 
estadd ysumm
}

estout using "$latexcodes\FertilidadMeassure_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(`var_indep'  )  stats(N N_clust ymean r2, fmt(a2 a2 a2 a2) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$")) replace

eststo clear
local var_try1 fertilidad Nacimientos
foreach vardep of varlist `var_try1'{
eststo: quietly xi: areg `vardep' D_`var_indep'  i.ano i.Quarter region#c.YrQrt [aw=`vardep'_Count], absorb( codmpio) vce(cluster codmpio) 
estadd ysumm

}
estout using "$latexcodes\FertilidadDummies_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_`var_indep')  stats(N N_clust ymean r2, fmt(a2 a2 a2 a2) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$")) replace


}

eststo clear
local var_try1 fertilidad Nacimientos
foreach vardep of varlist `var_try1'{
eststo: quietly xi: areg `vardep' D_Upstream D_ExposicionMuni  i.ano i.Quarter region#c.YrQrt [aw=`vardep'_Count], absorb( codmpio) vce(cluster codmpio) 
estadd ysumm
}
estout using "$latexcodes\FertilidadDummies_Combo.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_Upstream D_ExposicionMuni)  stats(N N_clust ymean r2, fmt(a2 a2 a2 a2) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$")) replace
eststo clear
local var_try1 fertilidad Nacimientos
foreach vardep of varlist `var_try1'{
eststo: quietly xi: areg `vardep' Upstream ExposicionMuni  i.ano i.Quarter region#c.YrQrt [aw=`vardep'_Count], absorb( codmpio) vce(cluster codmpio) 
estadd ysumm
}
estout using "$latexcodes\FertilidadMeassure_Combo.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(Upstream ExposicionMuni)  stats(N N_clust ymean r2, fmt(a2 a2 a2 a2) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$")) replace


/*
*Falsification Test with height 

local var_indepList AreaMinadaProp ProduccionPerArea
local control1 i.ano i.Semana_Naci 
local control2 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
local control3 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE region#c.FECHA_NAC
foreach var_indep of varlist `var_indepList'{
eststo clear
 eststo mh: quietly xi: areg Z_TALLA_NAC `var_indep' `var_indep'2 `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local timefe "Yes"
estadd local momc "Yes"
estadd local rtrend "Yes"
estout m3 mh using "APGAR_height_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(`var_indep' `var_indep'2 )  stats(N ymean r2 timefe momc rtrend, fmt(a2 a2 a2 %~#s %~#s %~#s) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$" "Time FE" "Ind. Controls" "Reg. Trends")) replace
*/


***Heterogeneous Effects*****

/*
local N=5
forval i=1/`N'{
qui gen ibinAreaProp`i'=0
replace ibinAreaProp`i'=1 if AreaMinadaProp>(`i'-1)*($areapropcut )/`N'  & AreaMinadaProp<=(`i')*($areapropcut )/`N'
}

forval i=1/`N'{
qui gen iboxAreaProp`i'=0
replace iboxAreaProp`i'=1 if AreaMinadaProp>(`i'-1)*($areapropcut )/`N'  & AreaMinadaProp<=($areapropcut) 
}
label var ibinAreaProp1 "Proportion of area mined between 0 and 2.8\%"
label var ibinAreaProp2 "Proportion of area mined between 2.8\% and 5.6\%"
label var ibinAreaProp3 "Proportion of area mined between 5.6\% and 8.4\%"
label var ibinAreaProp4 "Proportion of area mined between 8.4\% and 11.2\%"
label var ibinAreaProp5 "Proportion of area mined between 11.2\% and 14\%"

label var iboxAreaProp1 "Proportion of area mined greater than 0\%"
label var iboxAreaProp2 "Proportion of area mined greater than 2.8\% "
label var iboxAreaProp3 "Proportion of area mined greater than 5.6\% "
label var iboxAreaProp4 "Proportion of area mined greater than 8.4\% "
label var iboxAreaProp5 "Proportion of area mined greater than 11.2\% "

xi: areg APGAR_BAJO ibinAreaProp* `control3' , absorb(codmpio) vce(cluster codmpio)
xi: areg APGAR_BAJO iboxAreaProp* `control3' , absorb(codmpio) vce(cluster codmpio)

*/

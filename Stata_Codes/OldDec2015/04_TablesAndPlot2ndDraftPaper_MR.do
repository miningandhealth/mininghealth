clear all
global latexcodes     "$gitpath\LaTeX\201510_2ndDraft"
use "$base_out\DefFetalesQuarter.dta", clear
rename Quarter Quarter
rename CodigoDane codmpio
rename ANO ano
merge 1:1 ano Quarter codmpio using "$base_out\PanelMunicipalEEVV_Stata.dta"
drop if _merge==1
replace DefFetales=0 if _merge==2
drop _merge
merge 1:1 ano Quarter codmpio using "$base_out\NonMissingsCount.dta"
drop _merge

tsset codmpio YrQrt, quarterly
qui gen coddepto=(codmpio-mod(codmpio,1000))/1000
gen D_ProxAreaMuni10KM2=(D_ProxAreaMuni10KM==1 & D_ProxAreaMuni5KM==0)
gen D_ProxAreaMuni20KM2=(D_ProxAreaMuni20KM==1 & D_ProxAreaMuni10KM==0)
gen D_RiverAreaMuni10KM2=(D_RiverAreaMuni10KM==1 & D_RiverAreaMuni5KM==0)
gen D_RiverAreaMuni20KM2=(D_RiverAreaMuni20KM==1 & D_RiverAreaMuni10KM==0)


gen D_ProxProdAIMuni10KM2=(D_ProxProdAIMuni10KM==1 & D_ProxProdAIMuni5KM==0)
gen D_ProxProdAIMuni20KM2=(D_ProxProdAIMuni20KM==1 & D_ProxProdAIMuni10KM==0)
gen D_RiverProdAIMuni10KM2=(D_RiverProdAIMuni10KM==1 & D_RiverProdAIMuni5KM==0)
gen D_RiverProdAIMuni20KM2=(D_RiverProdAIMuni20KM==1 & D_RiverProdAIMuni10KM==0)




gen PNM=1000*DefFetales/(Nacimientos+DefFetales)
sum PNM, d
replace PNM=r(p99) if PNM>r(p99) & !missing(PNM)
label var PNM "perinatal mortality"

gen IMR=TMI
gen Fertility= fertilidad
gen Births= Nacimientos

local RHS_MUNICIPALITY IMR PNM Births Fertility 

eststo clear
foreach var_lhs in D_AreaMinada_sqkm D_ProxAreaMuni20KM D_RiverAreaMuni20KM{

	foreach var in `RHS_MUNICIPALITY'{
		eststo `var': quietly areg `var' `var_lhs' i.YrQrt coddepto#c.YrQrt [aw= Nacimientos], absorb(codmpio) vce(cluster codmpio)
	}
	esttab `RHS_MUNICIPALITY', se nostar keep(`var_lhs')
	matrix C = r(coefs)
	local rnames : rownames C
	local models : coleq C
	local models : list uniq models
	local i 0
	foreach name of local rnames {
		  local ++i
		  local j 0
		  capture matrix drop b
		  capture matrix drop se
		  foreach model of local models {
			  local ++j
			  matrix tmp = C[`i', 2*`j'-1]
			  if tmp[1,1]<.{
				 matrix colnames tmp = `model'
				 matrix b = nullmat(b), tmp
				 matrix tmp[1,1] = C[`i', 2*`j']
				 matrix se = nullmat(se), tmp
			 }
		 }
		 ereturn post b
		 quietly estadd matrix se
		 eststo `name'
	}
	eststo drop `RHS_MUNICIPALITY'

}

esttab using "$latexcodes\AreaTodo_Municipality.tex", se nomtitles noobs replace fragment

 

use "$base_out\DataCompleta_Stata.dta", clear

local RHS_INDIVIDUO  ConsultasPreMayor4 Premature PartoHospital    EDAD_MADRE  MadreSoltera EduMadrePostPrimaria  
qui gen coddepto=(codmpio-mod(codmpio,1000))/1000
gen Premature=(1-GEST_COMPLETA) if !missing(GEST_COMPLETA)
label var ConsultasPreMayor4 "Prenatal checkups $>$ 4"
label var Premature "Premature"

label var PartoHospital "In-hospital birth"
label var EDAD_MADRE "Mother's age"
label var MadreSoltera "Single Mother"
label var EduMadrePostPrimaria "At least secondary education (mother)"


keep `RHS_INDIVIDUO' ano MES region  ///
D_AreaMinadaProp* D_ProduccionPerArea* D_Prox* D_River*  ///
AreaMinadaProp* ProduccionPerArea* ProduccionAccPerArea* Prox* River*  ///
Semana_Naci codmpio FECHA_NAC APGAR_BAJO coddepto
areg APGAR_BAJO D_ProxAreaMuni10KM D_RiverAreaMuni10KM MadreSoltera EduMadrePostPrimaria EDAD_MADRE i.ano i.Semana_Naci coddepto#c.ano, absorb(codmpio) vce(cluster codmpio)

eststo clear
foreach var_lhs in D_AreaMinadaProp D_ProxAreaMuni20KM D_RiverAreaMuni20KM{

	foreach var in `RHS_INDIVIDUO'{
		eststo `var': quietly areg `var' `var_lhs' i.ano i.Semana_Naci region#c.FECHA_NAC, absorb(codmpio) vce(cluster codmpio)
	}
	esttab `RHS_INDIVIDUO', se nostar keep(`var_lhs')
	matrix C = r(coefs)
	local rnames : rownames C
	local models : coleq C
	local models : list uniq models
	local i 0
	foreach name of local rnames {
		  local ++i
		  local j 0
		  capture matrix drop b
		  capture matrix drop se
		  foreach model of local models {
			  local ++j
			  matrix tmp = C[`i', 2*`j'-1]
			  if tmp[1,1]<.{
				 matrix colnames tmp = `model'
				 matrix b = nullmat(b), tmp
				 matrix tmp[1,1] = C[`i', 2*`j']
				 matrix se = nullmat(se), tmp
			 }
		 }
		 ereturn post b
		 quietly estadd matrix se
		 eststo `name'
	}
	eststo drop `RHS_INDIVIDUO'

}

esttab using "$latexcodes\AreaTodo_Individual.tex", se nomtitles noobs replace fragment label







clear all
set more off
*Set up the R option to run R
*global Rterm_path `"E:\Program Files\R\R-3.1.1\bin\x64\Rterm.exe"'
*global Rterm_options `"--vanilla"'

* Central Path
*global mipath "C:\Users\Mauricio\Copy\MiningAndBirth\"
*global mipath "H:\Copy\MiningAndBirth\"
*global mipath "C:\Users\santi\Copy\PYP_Birth\"
global mipath "/media/mauricio/TeraHDD1/Copy/MiningAndBirth"
*global mipath "C:/Users/admin/Copy/PYP_Birth/"
* Path Tree
  global base_in 	"$mipath/RawData"
  global base_out   "$mipath/CreatedData"
 *global gitpath     "C:\Users\Mauricio\Documents\git\mininghealth"
 global gitpath     "/media/mauricio/TeraHDD1/git/mininghealth/" 
 *global gitpath "C:\Users\santi\Documents\mininghealth\"
*global gitpath     "C:/Users/admin/Documents/mininghealth/"
 
 global dir_do "$gitpath/Stata_Codes" 
  global results    "$mipath/Results"
  global exceltables  "$mipath/Results/ExcelTables"
  global graphs     "$mipath/Results/Graphs"
  global latexcodes     "$mipath/Results/LatexCodes"
	
	

*do "$dir_do\01_holyweek_dates"
do "$dir_do/03_SetupStataData"
do "$dir_do/04_TablesAndPlot2ndDraftPaper"
*do "$dir_do\03_indicator_gestation_hw"
*do "$dir_do\04_RegAreaAndUpstream"
*do "$dir_do\05_RegHW"
*do "$dir_do\07_RegAreaAndUpstream"
*do "$dir_do\08_RegHW"

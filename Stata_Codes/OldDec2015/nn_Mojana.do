
set matsize 100

gen YrMonth=ym(ano,MES)
gen YrQrt=yq(ano,Quarter)
gen region=gandina+2*gcaribe+3*gpacifica+4*gorinoquia+5*gamazonia

drop if AreaMinadaProp>$areapropcut
replace APGAR_BAJO=100*APGAR_BAJO
keep if region<=3
keep if NearGoldMine==1
*Sumstat Babies
drop if APGAR_BAJO==.
drop if PESO_NAC==.
drop if TALLA_NAC==. 
drop if EDAD_MADRE==.
drop if MadreSoltera==.
drop if EduMadrePostPrimaria==.
label var APGAR_BAJO "Low APGAR"
label var PESO_NAC "Weight (gr)"
label var TALLA_NAC "Height (cm)"
label var EDAD_MADRE "Mother's age"
label var MadreSoltera "Single mother"
label var EduMadrePostPrimaria "Mother has post-primary education"

keep if codmpio==23068 | codmpio==70771 | codmpio==70429 | codmpio==13006 | codmpio==5495 | codmpio==70678 | codmpio==70265 | codmpio==70124 | codmpio==70708 | codmpio==13430 | codmpio==13655 

local control3 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE region#c.FECHA_NAC
xi: areg APGAR_BAJO D_ExposicionMuni D_Upstream  `control3' , absorb(codmpio) vce(cluster codmpio)

use "$base_out\PanelMunicipalEEVV_Stata.dta", clear
tsset codmpio YrQrt, quarterly
drop if AreaMinadaProp>0.4
replace APGAR_BAJO=100*APGAR_BAJO
gen region=gandina+2*gcaribe+3*gpacifica+4*gorinoquia+5*gamazonia
keep if region<=3
keep if NearGoldMine==1
foreach vardep of varlist inv_sp inv_en_salud SGP_salud regalias_compensa FNR{
gen `vardep'PC=`vardep'/PoblacionMuni
}
foreach var_indep of varlist D_ExposicionMuni D_Upstream AreaMinadaProp D_AreaMinadaProp{
qui gen `var_indep'xprice=`var_indep'*PriceReal
}

label var AreaMinadaPropxprice "[Mining Area/Municipality Area] x Price"
label var D_AreaMinadaPropxprice "[Mining Area/Municipality Area \$> 0\$] x Price"
label var D_ExposicionMunixprice "[Proximity exposure \$>0\$] x Price"
label var D_Upstreamxprice "[River exposure \$>0\$] x Price"



foreach vardep of varlist inv_sp inv_en_salud SGP_salud regalias_compensa FNR{
eststo clear
foreach var_indep of varlist AreaMinadaProp AreaMinadaPropxprice D_AreaMinadaPropxprice ProduccionPerArea ProduccionAccPerArea{
eststo: quietly xi:  areg `vardep'PC `var_indep' i.ano i.Quarter  , absorb(codmpio) vce(cluster codmpio)
}
eststo: quietly xi:  areg `vardep'PC D_ExposicionMuni D_Upstream i.ano i.Quarter , absorb(codmpio) vce(cluster codmpio)
eststo: quietly xi:  areg `vardep'PC D_ExposicionMunixprice D_Upstreamxprice i.ano i.Quarter  , absorb(codmpio) vce(cluster codmpio)
estout using "$latexcodes\Inversion_`vardep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
keep(AreaMinadaProp AreaMinadaPropxprice D_AreaMinadaPropxprice ProduccionPerArea ProduccionAccPerArea D_ExposicionMuni D_Upstream D_ExposicionMunixprice D_Upstreamxprice) ///
replace
}


areg inv_en_saludPC AreaMinadaProp i.ano i.Quarter  , absorb(codmpio) vce(cluster codmpio)

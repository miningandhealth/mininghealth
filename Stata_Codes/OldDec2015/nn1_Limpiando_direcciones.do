import excel "$basein\BASE_IPS.xlsx", sheet("BASE_IPS") firstrow
drop tienNombre sigla gerente nombre telefono email url url_red
rename codigo cod_inst
gen codmpio=substr( cod_inst,1,5)
destring cod_inst, force replace
collapse (min) cod_inst, by( depaNombre muniNombre direccion codmpio)

destring codmpio, force replace
merge m:1 codmpio using "C:\Users\santi\Copy\PYP_Birth\CreatedData\Munis_mineros.dta"
drop if _merge!=3
sort direccion

drop if strpos(direccion , "AL LADO")!=0
drop if direccion=="" | direccion=="." | direccion=="0"
drop if direccion=="800 METROS VIA IBAGUE" | direccion=="8335344" | direccion=="ABREGO"
drop if strpos(direccion , "ALCALDIA")!=0
drop if strpos(direccion , "Alcaldia")!=0
drop if strpos(direccion , "BARRIO")!=0
drop if strpos(direccion , "XXXX")!=0
drop if strpos(direccion , "ZONA")!=0
drop if mod(codmpio,1000)==1

drop _merge
save "$base_out\Dir_hospitales_clean.dta", replace


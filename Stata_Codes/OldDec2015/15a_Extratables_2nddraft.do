
global latexcodes     "$gitpath\LaTeX\201510_2ndDraft"

use "$base_out\DataCompleta_Stata.dta", clear

*Graphs APGAR AreaMinada

quietly areg APGAR_BAJO i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE region#c.FECHA_NAC, absorb(codmpio)
predict resid_APGAR,resid
quietly areg AreaMinadaProp i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE region#c.FECHA_NAC, absorb(codmpio)
predict resid_AreaMinadaProp, resid

lpoly resid_APGAR resid_AreaMinadaProp, ci noscatter addplot((histogram resid_AreaMinadaProp, yaxis(2) fraction))
graph export "resid_apgar_areamin.pdf", replace

lpoly resid_APGAR AreaMinadaProp, ci noscatter addplot((histogram AreaMinadaProp, yaxis(2) fraction))
graph export "resid_apgar_Rawareamin.pdf", replace


lpoly APGAR AreaMinadaProp, ci msize(zero) noscatter addplot((histogram AreaMinadaProp, yaxis(2) fraction))
graph export "raw_apgar_areamin.pdf", replace

quietly areg APGAR_BAJO i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE region#c.FECHA_NAC if AreaMinadaProp>0, absorb(codmpio)
predict resid_APGAR_g0,resid 
quietly areg AreaMinadaProp i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE region#c.FECHA_NAC if AreaMinadaProp>0, absorb(codmpio)
predict resid_AreaMinadaProp_g0, resid 
lpoly resid_APGAR resid_AreaMinadaProp if AreaMinadaProp>0, ci noscatter addplot((histogram resid_AreaMinadaProp, yaxis(2) fraction))
graph export "resid_apgar_areamin_g0.pdf", replace

**Placebo Tests
*Linear regressions area-production

*PartoHospital MadreMenor14 Madre14_17 ConsultasPreMayor4 RegimenContributivo RegimenSubsidiado EduMadrePostSecundaria
local control1 i.ano i.Semana_Naci 
local control2 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
local control3 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE region#c.FECHA_NAC
local control4 i.ano i.Semana_Naci region#c.FECHA_NAC

eststo pa1: xi: areg MadreSoltera AreaMinadaProp `control4' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local rtrend "Yes"
eststo pa2: xi: areg EduMadrePostPrimaria AreaMinadaProp `control4' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local rtrend "Yes"
eststo pa3: xi: areg EDAD_MADRE AreaMinadaProp `control4' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local rtrend "Yes"
eststo pa4: xi: areg RegimenContributivo AreaMinadaProp `control4' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local rtrend "Yes"

eststo pb1: xi: areg MadreSoltera D_AreaMinadaProp `control4' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local rtrend "Yes"
eststo pb2: xi: areg EduMadrePostPrimaria D_AreaMinadaProp `control4' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local rtrend "Yes"
eststo pb3: xi: areg EDAD_MADRE D_AreaMinadaProp `control4' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local rtrend "Yes"
eststo pb4: xi: areg RegimenContributivo D_AreaMinadaProp `control4' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local rtrend "Yes"

/*
estout pa1 pa2 pa3 pa4 using "$latexcodes\AreaMinadaProp_Placebo.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( AreaMinadaProp )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

estout pb1 pb2 pb3 pb4 using "$latexcodes\D_AreaMinadaProp_Placebo.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( D_AreaMinadaProp )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace
*/



eststo pc1: qui xi: areg MadreSoltera D_ProxAreaMuni10KM D_RiverAreaMuni10KM `control4' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local rtrend "Yes"
eststo pc2: qui xi: areg EduMadrePostPrimaria D_ProxAreaMuni10KM D_RiverAreaMuni10KM `control4' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local rtrend "Yes"
eststo pc3: qui xi: areg EDAD_MADRE D_ProxAreaMuni10KM D_RiverAreaMuni10KM `control4' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local rtrend "Yes"
eststo pc4: qui xi: areg RegimenContributivo D_ProxAreaMuni10KM D_RiverAreaMuni10KM `control4' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local munife "Yes"
estadd local timefe "Yes"
estadd local rtrend "Yes"



estout pc1 pc2 pc3 pc4 using "$latexcodes\Exposure_Placebo.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( D_ExposicionMuni D_Upstream )  stats(N N_clust ymean r2 munife timefe rtrend, fmt(a2 a2 a2 a2 %~#s %~#s %~#s) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" "Municipality FE" "Time FE" "Reg. Trends")) replace
*note("\specialcell{Clustered standard errors, by municipality, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") ///




***Heterogeneous Effects*****

qui sum AreaMinadaProp if AreaMinadaProp>0,d
qui xtile PcentileArea=AreaMinadaProp if AreaMinadaProp>0, nquantiles(10)
qui tab PcentileArea, gen(PCArea)
foreach var of varlist PCArea1-PCArea10{
qui replace `var'=0 if `var'==.
}


areg APGAR_BAJO PCArea* `control3' , absorb(codmpio) vce(cluster codmpio)


local N=5
forval i=1/`N'{
qui gen ibinAreaProp`i'=0
replace ibinAreaProp`i'=1 if AreaMinadaProp>(`i'-1)*($areapropcut )/`N'  & AreaMinadaProp<=(`i')*($areapropcut )/`N'
}

forval i=1/`N'{
qui gen iboxAreaProp`i'=0
replace iboxAreaProp`i'=1 if AreaMinadaProp>(`i'-1)*($areapropcut )/`N'  & AreaMinadaProp<=($areapropcut) 
}

replace PCArea1=PCArea1+PCArea2+PCArea3+PCArea4+PCArea5+PCArea6+PCArea7+PCArea8+PCArea9+PCArea10
replace PCArea2=PCArea2+PCArea3+PCArea4+PCArea5+PCArea6+PCArea7+PCArea8+PCArea9+PCArea10
replace PCArea3=PCArea3+PCArea4+PCArea5+PCArea6+PCArea7+PCArea8+PCArea9+PCArea10
replace PCArea4=PCArea4+PCArea5+PCArea6+PCArea7+PCArea8+PCArea9+PCArea10
replace PCArea5=PCArea5+PCArea6+PCArea7+PCArea8+PCArea9+PCArea10
replace PCArea6=PCArea6+PCArea7+PCArea8+PCArea9+PCArea10
replace PCArea7=PCArea7+PCArea8+PCArea9+PCArea10
replace PCArea8=PCArea8+PCArea9+PCArea10
replace PCArea9=PCArea9+PCArea10
replace PCArea10=PCArea10


label var PCArea1 "Proportion of area mined>0"
label var PCArea2 "Proportion of area mined>20th percentile"
label var PCArea3 "Proportion of area mined>40th percentile"
label var PCArea4 "Proportion of area mined>60th percentile"
label var PCArea5 "Proportion of area mined>80th percentile"

label var ibinAreaProp1 "Proportion of area mined between 0 and 2.8\%"
label var ibinAreaProp2 "Proportion of area mined between 2.8\% and 5.6\%"
label var ibinAreaProp3 "Proportion of area mined between 5.6\% and 8.4\%"
label var ibinAreaProp4 "Proportion of area mined between 8.4\% and 11.2\%"
label var ibinAreaProp5 "Proportion of area mined between 11.2\% and 14\%"

label var iboxAreaProp1 "Proportion of area mined greater than 0\%"
label var iboxAreaProp2 "Proportion of area mined greater than 2.8\% "
label var iboxAreaProp3 "Proportion of area mined greater than 5.6\% "
label var iboxAreaProp4 "Proportion of area mined greater than 8.4\% "
label var iboxAreaProp5 "Proportion of area mined greater than 11.2\% "
set more off
local control3 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE region#c.FECHA_NAC
eststo clear
eststo h1: xi: areg APGAR_BAJO ibinAreaProp* `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
eststo h2:xi: areg APGAR_BAJO iboxAreaProp* `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estout h1 using "$latexcodes\Hetero1.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(ibinAreaProp*)  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace
estout h2 using "$latexcodes\Hetero2.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( iboxAreaProp*)  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs."  "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace




qui areg APGAR_BAJO PCArea* `control3' , absorb(codmpio) vce(cluster codmpio)
coefplot, baselevels keep(PCArea*) ci rename(PCArea1="1" PCArea2="2" PCArea3="3" PCArea4="4" PCArea5="5"  ///
PCArea6="6" PCArea7="7" PCArea8="8" PCArea9="9" PCArea10="10") ///
   vertical yline(0) xtitle("Decile") ytitle("Coefficient on APGAR") title("Intensive Margin")
 graph export "$latexcodes\\HeterogeneousImpactQ10.pdf", replace

 drop PcentileArea PCArea*
xtile PcentileArea=AreaMinadaProp if AreaMinadaProp>0, nquantiles(5)
tab PcentileArea, gen(PCArea)
foreach var of varlist PCArea1-PCArea5{
replace `var'=0 if `var'==.
}



 areg APGAR_BAJO PCArea* `control3' , absorb(codmpio) vce(cluster codmpio)
coefplot, baselevels keep(PCArea*) ci rename(PCArea1="1" PCArea2="2" PCArea3="3" PCArea4="4" PCArea5="5") ///
   vertical yline(0) xtitle("Quantile") ytitle("Coefficient on APGAR") title("Intensive Margin")
 graph export "$latexcodes\\HeterogeneousImpactQ5.pdf", replace
 
 /*
**
**Falsification with sand mines
*use "$base_out\DataTest.dta", clear
use "$base_out\DataCompleta_Stata.dta", clear
gen region=gandina+2*gcaribe+3*gpacifica+4*gorinoquia+5*gamazonia
drop if Arena_AreaMinadaProp>$areapropcut
replace APGAR_BAJO=100*APGAR_BAJO
keep if region<=3
keep if NearArenaMine==1
eststo clear
label var D_Arena_ExposicionMuni "Sand Proximity exposure \$>0\$"
label var D_Arena_Upstream "Sand River exposure \$>0\$"


eststo m1: quietly xi: areg APGAR_BAJO D_Arena_ExposicionMuni D_Arena_Upstream `control1' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local timefe "Yes"
estadd local momc "No"
estadd local rtrend "No"



eststo m2: quietly xi: areg APGAR_BAJO D_Arena_ExposicionMuni D_Arena_Upstream  `control2' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local timefe "Yes"
estadd local momc "Yes"
estadd local rtrend "No"

eststo m3: quietly xi: areg APGAR_BAJO D_Arena_ExposicionMuni D_Arena_Upstream  `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local timefe "Yes"
estadd local momc "Yes"
estadd local rtrend "Yes"
estout m1 m2 m3 using "$latexcodes\IndividualDummies_Combo_Arena.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_Arena_ExposicionMuni D_Arena_Upstream )  stats(N ymean r2 timefe momc rtrend, fmt(a2 a2 a2 %~#s %~#s %~#s) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$" "Time FE" "Ind. Controls" "Reg. Trends")) replace
*/
/*
*Sumstat AreaMinada
eststo clear
estpost tabstat AreaMinadaProp ProduccionPerArea , statistics(mean median sd max min p95 count) columns(statistics)
esttab using "summaryIndep_IndividualTodos.tex", cells("mean(fmt(a2) label(Mean)) p50(fmt(a2) label(Median)) sd(fmt(a2) label(Std. Dev.)) min(fmt(a2) label(Min)) max(fmt(a2) label(Max)) p95(fmt(a2) label(Perc. 95)) count(fmt(a2) label(N))") nomtitle nonumber label  replace noobs booktabs

eststo clear
replace AreaMinadaProp=. if AreaMinadaProp==0
replace ExposicionMuni=. if ExposicionMuni==0
replace Upstream=. if Upstream==0
replace ProduccionPerArea=. if ProduccionPerArea==0
estpost tabstat AreaMinadaProp ProduccionPerArea , statistics(mean median sd max p95 count) columns(statistics)
esttab using "summaryIndep_IndividualPositivos.tex", cells("mean(fmt(a2) label(Mean)) p50(fmt(a2) label(Median)) sd(fmt(a2) label(Std. Dev.)) min(fmt(a2) label(Min)) max(fmt(a2) label(Max)) p95(fmt(a2) label(Perc. 95)) count(fmt(a2) label(N))") nomtitle nonumber label  replace noobs booktabs
clear all
*/

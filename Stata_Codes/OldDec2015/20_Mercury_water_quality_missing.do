
forval i=6/9 {
import delimited C:\Users\santi\Desktop\Research_bigdata\Mining\Calidad_agua\ConsolidadoParametrosPorPersonaPrestadora200`i'.csv, clear
keep if nombreparametroanalisis2=="Mercurio"
replace resultadopromedio=subinstr( resultadopromedio,",",".",.)
destring resultadopromedio, force replace
collapse resultadopromedio, by( nombredepartamento nombremunicipio mesdetoma)
keep if resultadopromedio!=.
sum resultadopromedio
save C:\Users\santi\Desktop\Research_bigdata\Mining\Calidad_agua\Mercurio_muni_mes_200`i'.dta, replace
}

forval i=10/12 {
import delimited C:\Users\santi\Desktop\Research_bigdata\Mining\Calidad_agua\ConsolidadoParametrosPorPersonaPrestadora20`i'.csv, clear
keep if nombreparametroanalisis2=="Mercurio"
replace resultadopromedio=subinstr( resultadopromedio,",",".",.)
destring resultadopromedio, force replace
collapse resultadopromedio, by( nombredepartamento nombremunicipio mesdetoma)
keep if resultadopromedio!=.
sum resultadopromedio
save C:\Users\santi\Desktop\Research_bigdata\Mining\Calidad_agua\Mercurio_muni_mes_20`i'.dta, replace
}

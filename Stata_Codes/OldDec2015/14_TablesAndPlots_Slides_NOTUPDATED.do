global mipath "C:\Users\santi\Copy\PYP_Birth\"
*global mipath  "E:\Copy\MiningAndBirth"
  global base_out   "$mipath\CreatedData"
*use "$base_out\DataTest.dta", clear
/*
use "$base_out\DataCompleta_Stata.dta", clear

 cd "C:\\Users\\santi\\Documents\\mininghealth\\LaTeX\\112014_SEEPAC_bis\\"
 *cd "E:\Users\Mauricio\Documents\Git\mininghealth\LaTeX\112014_SEEPAC_bis"
set matsize 100

gen YrMonth=ym(ano,MES)
gen YrQrt=yq(ano,Quarter)
gen region=gandina+2*gcaribe+3*gpacifica+4*gorinoquia+5*gamazonia

drop if AreaMinadaProp>0.4
replace APGAR_BAJO=100*APGAR_BAJO
*Sumstat Babies
drop if APGAR_BAJO==.
drop if PESO_NAC==.
drop if TALLA_NAC==. 
drop if EDAD_MADRE==.
drop if MadreSoltera==.
drop if EduMadrePostPrimaria==.
label var APGAR_BAJO "Low APGAR"
label var PESO_NAC "Weight (gr)"
label var TALLA_NAC "Height (cm)"
label var EDAD_MADRE "Mother's age"
label var MadreSoltera "Single mother"
label var EduMadrePostPrimaria "Mother has post-primary education"
eststo clear
estpost tabstat APGAR_BAJO PESO_NAC TALLA_NAC EDAD_MADRE MadreSoltera EduMadrePostPrimaria, statistics(mean median sd max min count) columns(statistics)
esttab using "SummaryBirth.tex", cells("mean(fmt(a2) label(Mean)) p50(fmt(a2) label(Median)) sd(fmt(a2) label(Std. Dev.)) min(fmt(a2) label(Min)) max(fmt(a2) label(Max)) count(fmt(a2) label(N))") nomtitle nonumber label  replace noobs booktabs

keep Z_TALLA_NAC ano MES region D_AreaMinadaProp D_ProduccionPerArea D_ExposicionMuni D_Upstream AreaMinadaProp ProduccionPerArea ExposicionMuni Upstream AreaMinadaProp2 ProduccionPerArea2 ExposicionMuni2 Upstream2 Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE APGAR_BAJO LBW codmpio FECHA_NAC Z_TALLA_NAC



*Graphs APGAR AreaMinada

quietly areg APGAR_BAJO i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE region#c.FECHA_NAC, absorb(codmpio) vce(cluster codmpio)
predict resid_APGAR,resid 
quietly areg AreaMinadaProp i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE region#c.FECHA_NAC, absorb(codmpio) vce(cluster codmpio)
predict resid_AreaMinadaProp, resid 

lpoly resid_APGAR resid_AreaMinadaProp, ci noscatter addplot((histogram resid_AreaMinadaProp, yaxis(2) fraction))
graph export "resid_apgar_areamin.pdf", replace

lpoly APGAR AreaMinadaProp, ci msize(zero) noscatter addplot((histogram AreaMinadaProp, yaxis(2) fraction))
graph export "raw_apgar_areamin.pdf", replace

quietly areg APGAR_BAJO i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE region#c.FECHA_NAC if AreaMinadaProp>0, absorb(codmpio) vce(cluster codmpio)
predict resid_APGAR_g0,resid 
quietly areg AreaMinadaProp i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE region#c.FECHA_NAC if AreaMinadaProp>0, absorb(codmpio) vce(cluster codmpio)
predict resid_AreaMinadaProp_g0, resid 
lpoly resid_APGAR resid_AreaMinadaProp if AreaMinadaProp>0, ci noscatter addplot((histogram resid_AreaMinadaProp, yaxis(2) fraction))
graph export "resid_apgar_areamin_g0.pdf", replace



*Quadratic regressions area-production

local var_indepList AreaMinadaProp ProduccionPerArea
local control1 i.ano i.Semana_Naci 
local control2 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
local control3 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE region#c.FECHA_NAC
foreach var_indep of varlist `var_indepList'{
eststo clear

eststo m1: quietly xi: areg APGAR_BAJO `var_indep' `var_indep'2 `control1' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local timefe "Yes"
estadd local momc "No"
estadd local rtrend "No"

eststo m2: quietly xi: areg APGAR_BAJO `var_indep' `var_indep'2 `control2' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local timefe "Yes"
estadd local momc "Yes"
estadd local rtrend "No"

 eststo m3: quietly xi: areg APGAR_BAJO `var_indep' `var_indep'2 `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local timefe "Yes"
estadd local momc "Yes"
estadd local rtrend "Yes"
estout m1 m2 m3 using "IndividualMeasure_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(`var_indep' `var_indep'2 )  stats(N ymean r2 timefe momc rtrend, fmt(a2 a2 a2 %~#s %~#s %~#s) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$" "Time FE" "Ind. Controls" "Reg. Trends")) replace

 eststo mh: quietly xi: areg Z_TALLA_NAC `var_indep' `var_indep'2 `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local timefe "Yes"
estadd local momc "Yes"
estadd local rtrend "Yes"
estout m3 mh using "APGAR_height_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(`var_indep' `var_indep'2 )  stats(N ymean r2 timefe momc rtrend, fmt(a2 a2 a2 %~#s %~#s %~#s) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$" "Time FE" "Ind. Controls" "Reg. Trends")) replace


}

*Dummy regressions Area/Prod
local var_indepList AreaMinadaProp ProduccionPerArea
foreach var_indep of varlist `var_indepList'{
eststo clear

eststo m1: quietly xi: areg APGAR_BAJO D_`var_indep' `control1' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local timefe "Yes"
estadd local momc "No"
estadd local rtrend "No"



eststo m2: quietly xi: areg APGAR_BAJO D_`var_indep' `control2' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local timefe "Yes"
estadd local momc "Yes"
estadd local rtrend "No"

eststo m3: quietly xi: areg APGAR_BAJO D_`var_indep' `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local timefe "Yes"
estadd local momc "Yes"
estadd local rtrend "Yes"
estout m1 m2 m3 using "IndividualDummies_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( D_`var_indep' )  stats(N ymean r2 timefe momc rtrend, fmt(a2 a2 a2 %~#s %~#s %~#s) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$" "Time FE" "Ind. Controls" "Reg. Trends")) replace


}



*Dummy proximity-river
gen riverxproximity=D_ExposicionMuni*D_Upstream
eststo clear
label var D_ExposicionMuni "Proximity exposure \$>0\$"
label var D_Upstream "River exposure \$>0\$"
label var riverxproximity "River \$\times\$ proximity"

eststo m1: xi: areg APGAR_BAJO D_ExposicionMuni D_Upstream riverxproximity `control1' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local timefe "Yes"
estadd local momc "No"
estadd local rtrend "No"



eststo m2: xi: areg APGAR_BAJO D_ExposicionMuni D_Upstream riverxproximity `control2' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local timefe "Yes"
estadd local momc "Yes"
estadd local rtrend "No"

eststo m3: xi: areg APGAR_BAJO D_ExposicionMuni D_Upstream riverxproximity `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local timefe "Yes"
estadd local momc "Yes"
estadd local rtrend "Yes"
estout m1 m2 m3 using "IndividualDummies_Combo.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_ExposicionMuni D_Upstream riverxproximity)  stats(N ymean r2 timefe momc rtrend, fmt(a2 a2 a2 %~#s %~#s %~#s) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$" "Time FE" "Ind. Controls" "Reg. Trends")) replace


*Sumstat AreaMinada
eststo clear
estpost tabstat AreaMinadaProp ProduccionPerArea , statistics(mean median sd max min p95 count) columns(statistics)
esttab using "summaryIndep_IndividualTodos.tex", cells("mean(fmt(a2) label(Mean)) p50(fmt(a2) label(Median)) sd(fmt(a2) label(Std. Dev.)) min(fmt(a2) label(Min)) max(fmt(a2) label(Max)) p95(fmt(a2) label(Perc. 95)) count(fmt(a2) label(N))") nomtitle nonumber label  replace noobs booktabs

eststo clear
replace AreaMinadaProp=. if AreaMinadaProp==0
replace ExposicionMuni=. if ExposicionMuni==0
replace Upstream=. if Upstream==0
replace ProduccionPerArea=. if ProduccionPerArea==0
estpost tabstat AreaMinadaProp ProduccionPerArea , statistics(mean median sd max p95 count) columns(statistics)
esttab using "summaryIndep_IndividualPositivos.tex", cells("mean(fmt(a2) label(Mean)) p50(fmt(a2) label(Median)) sd(fmt(a2) label(Std. Dev.)) min(fmt(a2) label(Min)) max(fmt(a2) label(Max)) p95(fmt(a2) label(Perc. 95)) count(fmt(a2) label(N))") nomtitle nonumber label  replace noobs booktabs
clear all
*/
*Holy Week

*use "$base_out\DataTest.dta", clear
use "$base_out\DataCompleta_Stata.dta", clear
drop if AreaMinadaProp>0.4
set matsize 100
drop if wgesthw==.
gen YrMonth=ym(ano,MES)
gen YrQrt=yq(ano,Quarter)
gen region=gandina+2*gcaribe+3*gpacifica+4*gorinoquia+5*gamazonia
keep wgesthw ihw  ano MES region D_AreaMinadaProp D_ProduccionPerArea D_ExposicionMuni D_Upstream AreaMinadaProp ProduccionPerArea ExposicionMuni Upstream AreaMinadaProp2 ProduccionPerArea2 ExposicionMuni2 Upstream2 Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE APGAR_BAJO LBW codmpio FECHA_NAC
replace APGAR_BAJO=100*APGAR_BAJO
*gen fixEf2a=string(ihw)+string(codmpio) if codmpio!=5001
*replace fixEf2a=string(codmpio) if codmpio==5001
*encode fixEf2a, gen(FE_AA)

gen ihwb=0 if wgesthw!=.
replace ihwb=1 if ihw==1 & wgesthw<=20
label var ihwb "Holyweek first 20 weeks of gestation"

gen ihwe=0 if wgesthw!=.
replace ihwe=1 if ihw==1 & ihwb==0
label var ihwe "Holyweek last weeks of gestation"

gen ihwsp=0 if wgesthw!=. & ihw!=1
replace ihwsp=1 if wgesthw>8 & wgesthw<25
label var ihwb "Holyweek between weeks 8 and 25"
eststo: xi: areg APGAR_BAJO c.D_ExposicionMuni##c.ihwsp  c.D_Upstream##c.ihwsp   `control' region#c.FECHA_NAC if wgesthw!=., absorb( codmpio) vce(cluster codmpio) 
/*

label var D_ExposicionMuni "Proximity exposure \$>0\$"
label var D_Upstream "River exposure \$>0\$"
eststo clear
local control i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
foreach varhw of varlist ihw ihwb ihwe{
eststo: quietly xi: areg APGAR_BAJO c.D_ExposicionMuni##c.`varhw'  c.D_Upstream##c.`varhw'   `control' region#c.FECHA_NAC if wgesthw!=., absorb( codmpio) vce(cluster codmpio) 
estadd ysumm
}
estout using "IndividualHWDummies_Combo.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$")) replace ///
rename(ihwb ihw ihwe ihw c.D_ExposicionMuni#c.ihwb c.D_ExposicionMuni#c.ihw c.D_ExposicionMuni#c.ihwe c.D_ExposicionMuni#c.ihw c.D_Upstream#c.ihwb c.D_Upstream#c.ihw c.D_Upstream#c.ihwe c.D_Upstream#c.ihw) ///
keep(D_ExposicionMuni D_Upstream ihw c.D_ExposicionMuni#c.ihw c.D_Upstream#c.ihw ) varl(ihw "Holy Week" c.D_ExposicionMuni#c.ihw " Proximity \$\times\$ Holy Week" c.D_Upstream#c.ihw "River  \$\times\$ Holy Week")

/* Holyweek regression by each week
mat def mathw=J(40,2,.)
forval i=1/40{
xi: areg APGAR_BAJO c.D_ExposicionMuni##c.ihw`i'##c.D_Upstream  `control'  if wgesthw!=., absorb( codmpio) vce(cluster codmpio) 

}
*/

*Falsification Test

local var_indepList AreaMinadaProp ProduccionPerArea
local control1 i.ano i.Semana_Naci 
local control2 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
local control3 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE region#c.FECHA_NAC
foreach var_indep of varlist `var_indepList'{
eststo clear
 eststo mh: quietly xi: areg Z_TALLA_NAC `var_indep' `var_indep'2 `control3' , absorb(codmpio) vce(cluster codmpio)
estadd ysumm
estadd local timefe "Yes"
estadd local momc "Yes"
estadd local rtrend "Yes"
estout m3 mh using "APGAR_height_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(`var_indep' `var_indep'2 )  stats(N ymean r2 timefe momc rtrend, fmt(a2 a2 a2 %~#s %~#s %~#s) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$" "Time FE" "Ind. Controls" "Reg. Trends")) replace

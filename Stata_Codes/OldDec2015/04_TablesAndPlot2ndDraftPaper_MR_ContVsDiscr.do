clear all
global latexcodes     "$gitpath\LaTeX\201510_2ndDraft"

use "$base_out\DataCompleta_Stata.dta", clear
local RHS_INDIVIDUO   NUM_CONSUL ConsultasPreMayor4 GEST_COMPLETA  Csection PartoHospital  RegimenContributivo  EDAD_MADRE MadreMenor14 Madre14_17 MadreSoltera EduMadrePostPrimaria EduMadrePostSecundaria  
qui gen coddepto=(codmpio-mod(codmpio,1000))/1000



keep `RHS_INDIVIDUO' ano MES region  APGAR_CONTINUO PESO_NAC TALLA_NAC  APGAR_BAJO LBW STUNT ///
D_AreaMinadaProp* D_ProduccionPerArea* D_Prox* D_River*  ///
AreaMinadaProp* ProduccionPerArea* ProduccionAccPerArea* Prox* River*  ///
Semana_Naci codmpio FECHA_NAC coddepto

gen D_ProxAreaMuni10KM2=(D_ProxAreaMuni10KM==1 & D_ProxAreaMuni5KM==0)
gen D_ProxAreaMuni20KM2=(D_ProxAreaMuni20KM==1 & D_ProxAreaMuni10KM==0)
gen D_RiverAreaMuni10KM2=(D_RiverAreaMuni10KM==1 & D_RiverAreaMuni5KM==0)
gen D_RiverAreaMuni20KM2=(D_RiverAreaMuni20KM==1 & D_RiverAreaMuni10KM==0)
_crcslbl D_ProxAreaMuni10KM2 D_ProxAreaMuni510KM
_crcslbl D_ProxAreaMuni20KM2 D_ProxAreaMuni1020KM
_crcslbl D_RiverAreaMuni10KM2 D_RiverAreaMuni510KM
_crcslbl D_RiverAreaMuni20KM2 D_RiverAreaMuni1020KM

   



gen D_ProxProdAIMuni10KM2=(D_ProxProdAIMuni10KM==1 & D_ProxProdAIMuni5KM==0)
gen D_ProxProdAIMuni20KM2=(D_ProxProdAIMuni20KM==1 & D_ProxProdAIMuni10KM==0)
gen D_RiverProdAIMuni10KM2=(D_RiverProdAIMuni10KM==1 & D_RiverProdAIMuni5KM==0)
gen D_RiverProdAIMuni20KM2=(D_RiverProdAIMuni20KM==1 & D_RiverProdAIMuni10KM==0)



local var_continuas  APGAR_CONTINUO PESO_NAC TALLA_NAC
local var_discretas  APGAR_BAJO LBW STUNT

foreach var_lhs in  Area{
	eststo clear
	foreach var_rhs in `var_discretas'{
		eststo: quietly xi: areg `var_rhs' D_Prox`var_lhs'Muni20KM D_River`var_lhs'Muni20KM i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE coddepto#c.ano , absorb(codmpio) vce(cluster codmpio)
		estadd ysumm
	}
	estout  using "$latexcodes/ProxRiver`var_lhs'_RHS_Discretas_20KM.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
	keep(D_Prox`var_lhs'Muni20KM D_River`var_lhs'Muni20KM )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace
	
	
	eststo clear
	foreach var_rhs in `var_continuas'{
		eststo: quietly xi: areg `var_rhs' D_Prox`var_lhs'Muni20KM D_River`var_lhs'Muni20KM i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE coddepto#c.ano , absorb(codmpio) vce(cluster codmpio)
		estadd ysumm
	}
	estout  using "$latexcodes/ProxRiver`var_lhs'_RHS_Continuas_20KM.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
	keep(D_Prox`var_lhs'Muni20KM  D_River`var_lhs'Muni20KM)   stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace
	
	}
	

	/*

foreach var_lhs in AreaMinadaProp ProduccionPerArea ProduccionAccPerArea D_AreaMinadaProp{
	eststo clear
	foreach var_rhs in `var_discretas'{
		eststo: quietly xi: areg `var_rhs' `var_lhs' i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE coddepto#c.ano , absorb(codmpio) vce(cluster codmpio)
		estadd ysumm
	}
	estout   using "$latexcodes/`var_lhs'_RHS_Discretas.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
	keep( `var_lhs' )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace
	
	eststo clear
	foreach var_rhs in `var_continuas'{
		eststo: quietly xi: areg `var_rhs' `var_lhs' i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE coddepto#c.ano , absorb(codmpio) vce(cluster codmpio)
		estadd ysumm
	}
	estout   using "$latexcodes/`var_lhs'_RHS_Continuas.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
	keep( `var_lhs' )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace
}




foreach var_lhs in Area{
	eststo clear
	foreach var_rhs in `var_discretas'{
		eststo: quietly xi: areg `var_rhs' D_Prox`var_lhs'Muni5KM D_Prox`var_lhs'Muni10KM2 D_Prox`var_lhs'Muni20KM2 D_River`var_lhs'Muni5KM D_River`var_lhs'Muni10KM2 D_River`var_lhs'Muni20KM2 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE coddepto#c.ano , absorb(codmpio) vce(cluster codmpio)
		estadd ysumm
	}
	estout  using "$latexcodes/ProxRiver`var_lhs'_RHS_Discretas.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
	keep(D_Prox`var_lhs'Muni5KM D_Prox`var_lhs'Muni10KM2 D_Prox`var_lhs'Muni20KM2 D_River`var_lhs'Muni5KM D_River`var_lhs'Muni10KM2 D_River`var_lhs'Muni20KM2 )  stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace
	
	
	eststo clear
	foreach var_rhs in `var_continuas'{
		eststo: quietly xi: areg `var_rhs' D_Prox`var_lhs'Muni5KM D_Prox`var_lhs'Muni10KM2 D_Prox`var_lhs'Muni20KM2 D_River`var_lhs'Muni5KM D_River`var_lhs'Muni10KM2 D_River`var_lhs'Muni20KM2 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE coddepto#c.ano , absorb(codmpio) vce(cluster codmpio)
		estadd ysumm
	}
	estout  using "$latexcodes/ProxRiver`var_lhs'_RHS_Continuas.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
	keep(D_Prox`var_lhs'Muni5KM D_Prox`var_lhs'Muni10KM2 D_Prox`var_lhs'Muni20KM2 D_River`var_lhs'Muni5KM D_River`var_lhs'Muni10KM2 D_River`var_lhs'Muni20KM2 )   stats(N N_clust ymean r2 , fmt(a2 a2 a2 a2 ) labels ("N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace
	
	}
	
	
*/





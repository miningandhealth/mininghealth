clear all
global latexcodes     "$gitpath\LaTeX\201510_2ndDraft"

use "$base_out\DataCompleta_Stata.dta", clear
local RHS_INDIVIDUO  NUM_CONSUL ConsultasPreMayor4 GEST_COMPLETA  Csection PartoHospital  RegimenContributivo  EDAD_MADRE MadreMenor14 Madre14_17 MadreSoltera EduMadrePostPrimaria EduMadrePostSecundaria  
qui gen coddepto=(codmpio-mod(codmpio,1000))/1000


keep `RHS_INDIVIDUO' ano MES region APGAR_BAJO  ///
D_AreaMinadaProp* D_ProduccionPerArea* D_Prox* D_River*  ///
AreaMinadaProp* ProduccionPerArea* ProduccionAccPerArea* Prox* River*  ///
Semana_Naci codmpio FECHA_NAC coddepto

gen D_ProxAreaMuni10KM2=(D_ProxAreaMuni10KM==1 & D_ProxAreaMuni5KM==0)
gen D_ProxAreaMuni20KM2=(D_ProxAreaMuni20KM==1 & D_ProxAreaMuni10KM==0)
gen D_RiverAreaMuni10KM2=(D_RiverAreaMuni10KM==1 & D_RiverAreaMuni5KM==0)
gen D_RiverAreaMuni20KM2=(D_RiverAreaMuni20KM==1 & D_RiverAreaMuni10KM==0)


gen D_ProxProdAIMuni10KM2=(D_ProxProdAIMuni10KM==1 & D_ProxProdAIMuni5KM==0)
gen D_ProxProdAIMuni20KM2=(D_ProxProdAIMuni20KM==1 & D_ProxProdAIMuni10KM==0)
gen D_RiverProdAIMuni10KM2=(D_RiverProdAIMuni10KM==1 & D_RiverProdAIMuni5KM==0)
gen D_RiverProdAIMuni20KM2=(D_RiverProdAIMuni20KM==1 & D_RiverProdAIMuni10KM==0)

/*
foreach var_lhs in APGAR_BAJO AreaMinadaProp ProduccionPerArea ProduccionAccPerArea ProxAreaMuni5KM ProxProdAIMuni5KM  RiverAreaMuni5KM  RiverProdAIMuni5KM{
areg  `var_lhs' i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE region#c.FECHA_NAC , absorb(codmpio) vce(cluster codmpio)
predict `var_lhs'_Resid, residuals
}
*/


		
foreach var_lhs in AreaMinadaProp ProduccionPerArea ProduccionAccPerArea ProxAreaMuni5KM ProxProdAIMuni5KM  RiverAreaMuni5KM  RiverProdAIMuni5KM{
capture drop `var_lhs'_Quint
capture drop `var_lhs'_Dec
egen `var_lhs'_Quint=xtile(`var_lhs') if `var_lhs'>0, nquantiles(5)
replace `var_lhs'_Quint=0 if `var_lhs'_Quint==.
egen `var_lhs'_Dec=xtile(`var_lhs') if `var_lhs'>0, nquantiles(10)
replace `var_lhs'_Dec=0 if `var_lhs'_Dec==.
estpost tabstat `var_lhs', by(`var_lhs'_Quint) s(mean count) columns(statistics)
esttab using "$latexcodes\Quint_`var_lhs'.tex", cells("mean(fmt(a3)) count") nostar unstack   nonote nomtitle nonumber replace
estpost tabstat `var_lhs', by(`var_lhs'_Dec) s(mean count) columns(statistics)
esttab using "$latexcodes\Dec_`var_lhs'.tex",  cells("mean(fmt(a3)) count") nostar unstack   nonote nomtitle nonumber replace

}




foreach var_lhs in AreaMinadaProp ProduccionPerArea ProduccionAccPerArea{
	areg APGAR_BAJO i.`var_lhs'_Quint  i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE coddepto#c.ano , absorb(codmpio) vce(cluster codmpio)
	coefplot, graphregion(color(white)) keep(*`var_lhs'_Quint) ci rename(1.`var_lhs'_Quint="1" 2.`var_lhs'_Quint ="2" 3.`var_lhs'_Quint= "3" 4.`var_lhs'_Quint= "4" 5.`var_lhs'_Quint= "5")   vertical yline(0) xtitle("Quintile") ytitle("Coefficient on low APGAR") title("Quantiles")
	graph export "$latexcodes\Quintil`var_lhs'.pdf", replace 
}

foreach var_lhs in AreaMinadaProp ProduccionPerArea ProduccionAccPerArea{
	 areg APGAR_BAJO i.`var_lhs'_Dec  i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE coddepto#c.ano , absorb(codmpio) vce(cluster codmpio)
	coefplot, graphregion(color(white)) keep(*`var_lhs'_Dec) ci rename(1.`var_lhs'_Dec="1" 2.`var_lhs'_Dec ="2" 3.`var_lhs'_Dec= "3" 4.`var_lhs'_Dec= "4" 5.`var_lhs'_Dec= "5" ///
	6.`var_lhs'_Dec="6" 7.`var_lhs'_Dec ="7" 8.`var_lhs'_Dec= "8" 9.`var_lhs'_Dec= "9" 10.`var_lhs'_Dec= "10") ///
	vertical yline(0) xtitle("Decile") ytitle("Coefficient on low APGAR") title("Deciles")
	graph export "$latexcodes\Deciles`var_lhs'.pdf", replace
}

 



foreach var_lhs in ProdAI Area{
areg APGAR_BAJO i.Prox`var_lhs'Muni5KM_Quint i.River`var_lhs'Muni5KM_Quint i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE coddepto#c.ano , absorb(codmpio) vce(cluster codmpio)
coefplot, graphregion(color(white))  keep(*Prox`var_lhs'Muni5KM*) ci rename(1.Prox`var_lhs'Muni5KM_Quint="1" 2.Prox`var_lhs'Muni5KM_Quint ="2" 3.Prox`var_lhs'Muni5KM_Quint= "3" 4.Prox`var_lhs'Muni5KM_Quint= "4" 5.Prox`var_lhs'Muni5KM_Quint= "5")   vertical yline(0) xtitle("Proximity") ytitle("Coefficient on low APGAR") title("Quantiles") name(a)
coefplot, graphregion(color(white))  keep(*River`var_lhs'Muni5KM*) ci rename(1.River`var_lhs'Muni5KM_Quint="1" 2.River`var_lhs'Muni5KM_Quint ="2" 3.River`var_lhs'Muni5KM_Quint= "3" 4.River`var_lhs'Muni5KM_Quint= "4" 5.River`var_lhs'Muni5KM_Quint= "5")   vertical yline(0) xtitle("Upstream") ytitle("Coefficient on low APGAR") title("Quantiles") name(b)
graph combine a b, xsize(8)	
graph export "$latexcodes\QuintilProxRiver`var_lhs'.pdf", replace
graph drop a b
}


foreach var_lhs in ProdAI Area{
areg APGAR_BAJO i.Prox`var_lhs'Muni5KM_Dec i.River`var_lhs'Muni5KM_Dec i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE coddepto#c.ano , absorb(codmpio) vce(cluster codmpio)
coefplot, graphregion(color(white))  keep(*Prox`var_lhs'Muni5KM*) ci rename(1.Prox`var_lhs'Muni5KM_Dec="1" 2.Prox`var_lhs'Muni5KM_Dec ="2" 3.Prox`var_lhs'Muni5KM_Dec= "3" 4.Prox`var_lhs'Muni5KM_Dec= "4" 5.Prox`var_lhs'Muni5KM_Dec= "5" ///
6.Prox`var_lhs'Muni5KM_Dec="6" 7.Prox`var_lhs'Muni5KM_Dec ="7" 8.Prox`var_lhs'Muni5KM_Dec= "8" 9.Prox`var_lhs'Muni5KM_Dec= "9" 10.Prox`var_lhs'Muni5KM_Dec= "10") name(a) ///
vertical yline(0) xtitle("Decile") ytitle("Coefficient on low APGAR") title("Proximity")
coefplot, graphregion(color(white))  keep(*River`var_lhs'Muni5KM*) ci rename(1.River`var_lhs'Muni5KM_Dec="1" 2.River`var_lhs'Muni5KM_Dec ="2" 3.River`var_lhs'Muni5KM_Dec= "3" 4.River`var_lhs'Muni5KM_Dec= "4" 5.River`var_lhs'Muni5KM_Dec= "5" ///
6.River`var_lhs'Muni5KM_Dec="6" 7.River`var_lhs'Muni5KM_Dec ="7" 8.River`var_lhs'Muni5KM_Dec= "8" 9.River`var_lhs'Muni5KM_Dec= "9" 10.River`var_lhs'Muni5KM_Dec= "10") name(b) ///
vertical yline(0) xtitle("Decile") ytitle("Coefficient on low APGAR") title("Upstream")
graph combine a b, xsize(8)	
graph export "$latexcodes\DecileProxRiver`var_lhs'.pdf", replace
graph drop a b
}






clear all
set more off

import delimited "$base_out\Fish_survey\municipioscolombiacsv.txt", clear 
destring v5, replace force
rename v1 muni_id
gen fish2b=muni_id
rename v5 lat_0
rename v6 long_0
gen long_1= long_0
gen lat_1= lat_0
save "$base_out\municipioscolombiacsv.dta", replace

import excel "$base_out\Fish_survey\Piloto_Nov2014.xlsx", sheet("Hoja1") firstrow case(lower) clear
merge m:1 muni_id using "$base_out\municipioscolombiacsv.dta", keepus(long_0 lat_0)
drop if _merge==2
drop _merge
merge m:1 fish2b using "$base_out\municipioscolombiacsv.dta", keepus(long_1 lat_1)
drop if _merge==2
drop _merge
save "$base_out\Piloto_Nov2014.dta", replace

* Primero unificamos nombres pescado

*La tilapia es lo mismo que la mojarra
replace fish1b=2 if fish1b==23 

*se ponen los precios en kilos! Tener en cuenta que 1 kilo tiene 2.20462 libras
foreach var of varlist fish4 fish5_p fish6_p fish7_p fish8_p fish9_p fish10_p fish11_p fish12_p fish13_p fish14_p fish15_p fish16_p fish17_p{
replace `var'=`var'*2.20462 if fish4a==1
}

replace fish4a=2 if fish4a==1
gen fish4_STD=.

*Se standarizan los precios por tipo de pescado del precio actual
forvalues i=1/27{
sum fish4 if fish1b==`i'
replace fish4_STD=(fish4-r(mean))/r(sd) if fish1b==`i' 
}



*Now lets collapse by fish type
collapse (mean) fish4_STD fish5_p fish6_p fish7_p fish8_p fish9_p fish10_p fish11_p fish12_p fish13_p fish14_p fish15_p fish16_p fish17_p (sum) fish5_q fish6_q fish7_q fish8_q fish9_q fish10_q fish11_q fish12_q fish13_q fish14_q fish15_q fish16_q fish17_q, by(id fish1b)



*Se pone los datos de precios y cantidades en formato long
reshape long fish@_p fish@_q, i(id fish1b) j(mes)

gen fish_p_STD=.

*Se standarizan los precios por tipo de pescado
forvalues i=1/27{
sum fish_p [aw=fish_q] if fish1b==`i'
replace fish_p_STD=(fish_p-r(mean))/r(sd) if fish1b==`i' 
}

replace mes=1 if mes==5
replace mes=2 if mes==6
replace mes=3 if mes==7
replace mes=4 if mes==8
replace mes=5 if mes==10
replace mes=6 if mes==11
replace mes=7 if mes==12
replace mes=8 if mes==13
replace mes=13 if mes==9
replace mes=9 if mes==14
replace mes=10 if mes==15
replace mes=11 if mes==16
replace mes=12 if mes==17

collapse (mean) fish4_STD fish_p_STD [aw=fish_q], by(mes)
sum fish_p_STD if mes==13
twoway (line fish_p_STD mes if mes<13, sort), xlabel(1(1)12) yline(`r(mean)') yscale(range(-0.3 0.3))
graph export "$graphs\fish_survey\PrecioPromedio_PonderadoCantidad.pdf", replace

clear all
set more off

use "$base_out\Piloto_Nov2014.dta", clear

* Primero unificamos nombres pescado

*La tilapia es lo mismo que la mojarra
replace fish1b=2 if fish1b==23 

*se ponen los precios en kilos! Tener en cuenta que 1 kilo tiene 2.20462 libras
foreach var of varlist fish4 fish5_p fish6_p fish7_p fish8_p fish9_p fish10_p fish11_p fish12_p fish13_p fish14_p fish15_p fish16_p fish17_p{
replace `var'=`var'*2.20462 if fish4a==1
}

replace fish4a=2 if fish4a==1
gen fish4_STD=.

*Se standarizan los precios por tipo de pescado del precio actual
forvalues i=1/27{
sum fish4 if fish1b==`i'
replace fish4_STD=(fish4-r(mean))/r(sd) if fish1b==`i' 
}



*Now lets collapse by fish type
collapse (mean) fish4_STD fish5_p fish6_p fish7_p fish8_p fish9_p fish10_p fish11_p fish12_p fish13_p fish14_p fish15_p fish16_p fish17_p (sum) fish5_q fish6_q fish7_q fish8_q fish9_q fish10_q fish11_q fish12_q fish13_q fish14_q fish15_q fish16_q fish17_q, by(id fish1b)



*Se pone los datos de precios y cantidades en formato long
reshape long fish@_p fish@_q, i(id fish1b) j(mes)

gen fish_p_STD=.

*Se standarizan los precios por tipo de pescado
forvalues i=1/27{
sum fish_p [aw=fish_q] if fish1b==`i'
replace fish_p_STD=(fish_p-r(mean))/r(sd) if fish1b==`i' 
}

replace mes=1 if mes==5
replace mes=2 if mes==6
replace mes=3 if mes==7
replace mes=4 if mes==8
replace mes=5 if mes==10
replace mes=6 if mes==11
replace mes=7 if mes==12
replace mes=8 if mes==13
replace mes=13 if mes==9
replace mes=9 if mes==14
replace mes=10 if mes==15
replace mes=11 if mes==16
replace mes=12 if mes==17

collapse (mean) fish4_STD fish_p_STD, by(mes)
sum fish_p_STD if mes==13
twoway (line fish_p_STD mes if mes<13, sort), xlabel(1(1)12) yline(`r(mean)') yscale(range(-0.3 0.3))
graph export "$graphs\fish_survey\PrecioPromedio.pdf", replace


*Municipio de origen
use "$base_out\Piloto_Nov2014.dta", clear

tab fish2b

*En 58.43% de los casos es un intermediario
*En 4.82% de los casos viene del exterior!!

geodist lat_0 long_0 lat_1 long_1 , generate(distance)
sum distance,d
*la distancia promedio es 14 km... con mas de las 95% de las observaciones a menos de 34 km

tab fish3
*El 35\% del pescado es de granja


*Evolucuion ventas y precio por mes


use "$base_out\Piloto_Nov2014.dta", clear

* Primero unificamos nombres pescado

*La tilapia es lo mismo que la mojarra
replace fish1b=2 if fish1b==23 

*se ponen los precios en kilos! Tener en cuenta que 1 kilo tiene 2.20462 libras
foreach var of varlist fish4 fish5_p fish6_p fish7_p fish8_p fish9_p fish10_p fish11_p fish12_p fish13_p fish14_p fish15_p fish16_p fish17_p{
replace `var'=`var'*2.20462 if fish4a==1
}

replace fish4a=2 if fish4a==1
gen fish4_STD=.

*Se standarizan los precios por tipo de pescado del precio actual
forvalues i=1/27{
sum fish4 if fish1b==`i'
replace fish4_STD=(fish4-r(mean))/r(sd) if fish1b==`i' 
}



*Now lets collapse by fish type
collapse (mean) fish4_STD fish5_p fish6_p fish7_p fish8_p fish9_p fish10_p fish11_p fish12_p fish13_p fish14_p fish15_p fish16_p fish17_p (sum) fish5_q fish6_q fish7_q fish8_q fish9_q fish10_q fish11_q fish12_q fish13_q fish14_q fish15_q fish16_q fish17_q, by(id fish1b)



*Se pone los datos de precios y cantidades en formato long
reshape long fish@_p fish@_q, i(id fish1b) j(mes)

gen fish_p_STD=.

*Se standarizan los precios por tipo de pescado
forvalues i=1/27{
sum fish_p [aw=fish_q] if fish1b==`i'
replace fish_p_STD=(fish_p-r(mean))/r(sd) if fish1b==`i' 
}

replace mes=1 if mes==5
replace mes=2 if mes==6
replace mes=3 if mes==7
replace mes=4 if mes==8
replace mes=5 if mes==10
replace mes=6 if mes==11
replace mes=7 if mes==12
replace mes=8 if mes==13
replace mes=13 if mes==9
replace mes=9 if mes==14
replace mes=10 if mes==15
replace mes=11 if mes==16
replace mes=12 if mes==17
preserve
collapse (mean) fish_p_STD, by(mes fish1b)

foreach i in 1 2 3 4 11{
sum fish_p_STD if mes==13 & fish1b==`i' 
twoway (line fish_p_STD mes if mes<13 & fish1b==`i' , sort), xlabel(1(1)12) yline(`r(mean)')
graph export "$graphs\fish_survey\Precio_Fish`i'.pdf", replace
}

restore
collapse (sum) fish_q, by(mes fish1b)

foreach i in 1 2 3 4 11{
sum fish_q if mes==13 & fish1b==`i' 
twoway (line fish_q mes if mes<13 & fish1b==`i' , sort), xlabel(1(1)12) yline(`r(mean)')
graph export "$graphs\fish_survey\Cantidad_Fish`i'.pdf", replace
}


*Que obs no cambian
use "$base_out\Piloto_Nov2014.dta", clear

* Primero unificamos nombres pescado

*La tilapia es lo mismo que la mojarra
replace fish1b=2 if fish1b==23 

*se ponen los precios en kilos! Tener en cuenta que 1 kilo tiene 2.20462 libras
foreach var of varlist fish4 fish5_p fish6_p fish7_p fish8_p fish9_p fish10_p fish11_p fish12_p fish13_p fish14_p fish15_p fish16_p fish17_p{
replace `var'=`var'*2.20462 if fish4a==1
}

*Now lets collapse by fish type
collapse (mean) fish4 fish5_p fish6_p fish7_p fish8_p fish9_p fish10_p fish11_p fish12_p fish13_p fish14_p fish15_p fish16_p fish17_p (sum) fish5_q fish6_q fish7_q fish8_q fish9_q fish10_q fish11_q fish12_q fish13_q fish14_q fish15_q fish16_q fish17_q, by(id fish1b)

egen variacion_ano_p=rowsd(fish5_p fish6_p fish7_p fish8_p fish10_p fish11_p fish12_p fish13_p fish14_p fish15_p fish16_p fish17_p)
egen variacion_ano_q=rowsd(fish5_q fish6_q fish7_q fish8_q fish9_q fish10_q fish11_q fish12_q fish13_q fish14_q fish15_q fish16_q fish17_q)
tab variacion_ano_p
tab variacion_ano_q
*Las cantidades cambian mas... pero el 90\% de los precios es igual todo el ano
egen media_ano_p=rowmean(fish5_p fish6_p fish7_p fish8_p fish10_p fish11_p fish12_p fish13_p fish14_p fish15_p fish16_p fish17_p)
egen media_ano_q=rowmean(fish5_q fish6_q fish7_q fish8_q fish9_q fish10_q fish11_q fish12_q fish13_q fish14_q fish15_q fish16_q fish17_q)

gen mayor_SS_p=(fish9_p>media_ano_p) & !missing(fish9_p) & !missing(media_ano_p)
gen mayor_SS_q=(fish9_q>media_ano_q) & !missing(fish9_q) & !missing(media_ano_q)
tab mayor_SS_p
tab mayor_SS_q
*La mayoria (69%) de las obs muestra un aumento en cantidades en semana santa... pero no en precios

gen igual_SS_p=(fish9_p==media_ano_p) & !missing(fish9_p) & !missing(media_ano_p)
gen igual_SS_q=(fish9_q==media_ano_q) & !missing(fish9_q) & !missing(media_ano_q)
tab igual_SS_p
tab igual_SS_q
*Aprox el 27% muestra que el precio no cambia en SS, mientras solo el 16\% dice que la cantidad no cambia

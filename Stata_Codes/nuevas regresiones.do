	local corredor=c(username)


**Carolina
if "`corredor'"=="carol" {
global base_out "C:\Users\carol\OneDrive - Universidad del rosario\MineriaAll"
global latexpaper "C:\Users\carol\OneDrive - Universidad del rosario\MineriaAll"
global latexslides "C:\Users\carol\OneDrive - Universidad del rosario\MineriaAll"
global latexcodes "C:\Users\carol\OneDrive - Universidad del rosario\MineriaAll"
}
**Santiago
if "`corredor'"=="Santi" {
global base_out "C:/Users/Santi/Dropbox/Research/PYP_Birth/MineriaAll"
global latexpaper "C:/Users/Santi/Documents/mininghealth/LaTeX/201910_3rdDraft"
*global latexslides "C:/Users/Santi/Dropbox/Research/PYP_Birth/MineriaAll"
*global latexcodes "C:/Users/Santi/Dropbox/Research/PYP_Birth/MineriaAll"
}

use "$base_out/DataCompleta_Stata_modificacion.dta",replace

** Tipo 1 Mining title without satelite detected mine (maybe underground)
** Tipo 2 Mine without title (illegal mine)
** Tipo 3 Mining title with mine
global control4 MadreSoltera EduMadrePostPrimaria EDAD_MADRE

/*Tabla1*/
qui eststo m1: reghdfe APGAR_ALTO D_ExpAreaMuni20KM_Tipo1 D_UpstreamAreaMuni20KM_Tipo1  $control4 , absorb(i.codmpio Semana_Naci i.coddepto#c.ano i.ano) vce(cluster codmpio)
qui estadd ysumm
estadd local Period "2001-2014"

qui eststo m2: reghdfe APGAR_ALTO D_ExpAreaMuni20KM_Tipo1 D_UpstreamAreaMuni20KM_Tipo1  $control4 if ano>2003 & ano<2013, absorb(i.codmpio Semana_Naci i.coddepto#c.ano i.ano) vce(cluster codmpio)
qui estadd ysumm
estadd local Period "2004-2012"

qui eststo m3: reghdfe APGAR_ALTO D_ExpAreaMuni20KM_Tipo1 D_UpstreamAreaMuni20KM_Tipo1  $control4 if ano>2003, absorb(i.codmpio Semana_Naci i.coddepto#c.ano i.ano) vce(cluster codmpio)
qui estadd ysumm
estadd local Period "2004-2014"

 estout  m1 m2 m3   using "$latexpaper/Tables/health_reg_mod1.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
varlabel(D_ExpAreaMuni20KM_Tipo1 "Near mining 20 km$>$0" D_UpstreamAreaMuni20KM_Tipo1 "Mining downstream 20 km$>$0" ) ///
keep(D_ExpAreaMuni20KM_Tipo1 D_UpstreamAreaMuni20KM_Tipo1) prefoot(\midrule ) stats( N ymean  , fmt(a2  a2) labels ( "N. of observations (babies)"  "Mean of Dep. Var." )) replace



/*Tabla2*/

qui eststo m3: reghdfe APGAR_ALTO D_ExpAreaMuni20KM_Tipo123 D_UpstreamAreaMuni20KM_Tipo2 D_UpstreamAreaMuni20KM_Tipo3  $control4 if ano>2003 & ano<2013, absorb(i.codmpio Semana_Naci i.coddepto#c.ano i.ano) vce(cluster codmpio)
qui estadd ysumm
estadd local Period "2004-2012"

qui eststo m4: reghdfe APGAR_ALTO D_ExpAreaMuni20KM_Tipo123 D_UpstreamAreaMuni20KM_Tipo2 D_UpstreamAreaMuni20KM_Tipo3  $control4 if ano>2003, absorb(i.codmpio Semana_Naci i.coddepto#c.ano i.ano) vce(cluster codmpio)
qui estadd ysumm
estadd local Period "2004-2014"

 estout  m3 m4  using "$latexpaper/health_reg_mod2.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
varlabel() ///
keep(D_ExpAreaMuni20KM_Tipo123 D_UpstreamAreaMuni20KM_Tipo2 D_UpstreamAreaMuni20KM_Tipo3) prefoot(\midrule ) stats( N ymean  , fmt(a2  a2) labels ( "N. of observations (babies)"  "Mean of Dep. Var." )) replace




eststo clear
estpost tabstat APGAR_ALTO PartoHospital PESO_NAC LBW TALLA_NAC STUNT EDAD_MADRE MadreSoltera EduMadrePostPrimaria, statistics(mean median sd max min count) columns(statistics)
esttab using "$latexpaper/Tab1_SummaryBirth.tex", cells("mean(fmt(a2) label(Mean)) p50(fmt(a2) label(Median)) sd(fmt(a2) label(Std. Dev.)) min(fmt(a2) label(Min)) max(fmt(a2) label(Max)) count(fmt(a2) label(N))") nomtitle nonumber label  replace noobs booktabs




eststo clear
estpost tabstat D_ExpAreaMuni20KM_Tipo1 D_UpstreamAreaMuni20KM_Tipo1 nearonlylegal nearonlyillegal nearboth upsonlylegal upsonlyillegal upsboth, statistics(mean median sd max min count) columns(statistics)
esttab using "$latexpaper/Tab2_SummaryMining.tex", cells("mean(fmt(a2) label(Mean)) p50(fmt(a2) label(Median)) sd(fmt(a2) label(Std. Dev.)) min(fmt(a2) label(Min)) max(fmt(a2) label(Max)) count(fmt(a2) label(N))") nomtitle nonumber label  replace noobs booktabs













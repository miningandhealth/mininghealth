clear
cd "C:\Users\santi\Copy\PYP_Birth\"
import excel using "RawData\Fechas_semana_santa.xlsx", first cellrange(A1:E23)
*Put easter sunday in Stata date format
gen hw_wdate=mdy(eastersunm,eastersund,ano)
*Calculate the friday before palm Sunday
gen hw_wfdate=hw_wdate-9

local stdate=mdy(1,1,1992)
local counter=`stdate'
sum hw_wfdate
gen closest_hw=.
gen wdate=.
sum hw_wfdate
local size=`r(N)'-1
set obs 10000
forval b=1/`size'{

local paaa=`b'+1
while `counter'<hw_wfdate[`paaa'] {
local temp=`counter'-`stdate'+1
qui replace closest_hw=hw_wfdate[`b'] in `temp'
qui replace wdate=`counter' in `temp'
local counter=`counter'+1
}

}

keep wdate closest_hw
drop if wdate==.
save "CreatedData\Closest_hw", replace

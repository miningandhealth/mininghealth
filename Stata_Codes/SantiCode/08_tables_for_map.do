forval i=1999/2013 {
use "C:\Users\santi\Copy\PYP_Birth\CreatedData\PanelMunicipalEEVV_Stata.dta", clear
keep if ano==`i' & Quarter==1
keep areaMuni_km2 AreaMinadaKm2 codmpio

qui gen prop_AreaMin=round(100*AreaMinadaKm2/areaMuni_km2)
qui replace prop_AreaMin=0 if prop_AreaMin==.
drop areaMuni_km2 AreaMinadaKm2
sort codmpio
rename codmpio CODANE
outsheet CODANE prop_AreaMin using  "C:\Users\santi\Copy\PYP_Birth\CreatedData\GIS\Temporary\Prop_AreaMin`i'.txt", replace
}


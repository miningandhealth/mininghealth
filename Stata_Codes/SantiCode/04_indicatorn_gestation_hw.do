clear
cd "C:\Users\santi\Copy\PYP_Birth\"
use "CreatedData\DataCompleta.dta", clear

gen wdate=mdy(MES,dia,ANO)
sort wdate
merge m:1 wdate using "CreatedData\Closest_hw"
drop if _merge!=3
drop _merge
gen wgesthw=ceil((wdate-closest_hw)/7)
replace wgesthw=-99 if wgesthw>SEMANAS
replace wgesthw=. if SEMANAS==.
qui label var wgesthw "Week during gestation when holy week happens"
gen ihw=1 if wgesthw>0 & wgesthw!=.
replace ihw=0 if wgesthw==-99

gen AreaMinadaxihw=AreaMinada*ihw
gen semanabirth=ceil((MES-1)*4.28+dia/7)
save "CreatedData\DataCompleta.dta", replace




use "C:\Users\santi\Copy\PYP_Birth\CreatedData\PanelMunicipalEEVV_Stata.dta", clear
*I don't see anything visually of the quadratic (neither in regresion for a quarter)
*Fixed effects important
keep if ano==2012 & Quarter==4

hist AreaMinadaProp, percent

twoway scatter APGAR_BAJO AreaMinadaProp
twoway scatter APGAR_BAJO AreaMinadaProp if AreaMinadaProp>0
twoway scatter APGAR_BAJO AreaMinadaProp if AreaMinadaProp>0 & APGAR_BAJO>0
reg APGAR_BAJO AreaMinadaProp
reg APGAR_BAJO AreaMinadaProp if AreaMinadaProp>0
reg APGAR_BAJO AreaMinadaProp if AreaMinadaProp>0 & APGAR_BAJO>0
reg APGAR_BAJO AreaMinadaProp AreaMinadaProp2 if AreaMinadaProp>0
reg APGAR_BAJO AreaMinadaProp AreaMinadaProp2 if AreaMinadaProp>0 & APGAR_BAJO>0

sort AreaMinadaProp
keep codmpio AreaMinadaProp
save "C:\Users\santi\Copy\PYP_Birth\CreatedData\Temporary\codmpio_area2012.dta"
clear
destring codane2, replace
rename codane2 codmpio
drop if codmpio==.
merge 1:1 codmpio using "C:\Users\santi\Copy\PYP_Birth\CreatedData\Temporary\codmpio_area2012.dta"

save "C:\Users\santi\Copy\PYP_Birth\CreatedData\Temporary\muni_mineros.dta"

use "$base_out\DataCompleta_Stata.dta", clear
collapse (mean) MadreSoltera EduMadrePostPrimaria EDAD_MADRE APGAR_BAJO LBW GEST_COMPLETA PartoHospital AreaMinadaProp ExposicionMuni ProduccionPerCapita Upstream IntervMines, by(ano Quarter codmpio)
rename * =_Ind
rename  ano_Ind ano
rename  Quarter_Ind Quarter
rename  codmpio_Ind codmpio

merge 1:1 ano Quarter codmpio using "$base_out\PanelMunicipalEEVV_Stata.dta", keepus(MadreSoltera EduMadrePostPrimaria EDAD_MADRE APGAR_BAJO LBW GEST_COMPLETA PartoHospital AreaMinadaProp ExposicionMuni ProduccionPerCapita Upstream IntervMines)

foreach var of varlist MadreSoltera EduMadrePostPrimaria EDAD_MADRE APGAR_BAJO LBW GEST_COMPLETA PartoHospital AreaMinadaProp ExposicionMuni ProduccionPerCapita Upstream IntervMines{
plot `var' `var'_Ind
cor `var' `var'_Ind
}

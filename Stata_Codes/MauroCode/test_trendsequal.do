use "$base_out\DataCompleta_Stata.dta", clear
gen region=gandina+2*gcaribe+3*gpacifica+4*gorinoquia+5*gamazonia
reg APGAR_BAJO c.FECHA_NAC##region
test (_b[1.region#c.FECHA_NAC] == _b[2.region#c.FECHA_NAC]== _b[3.region#c.FECHA_NAC]== _b[4.region#c.FECHA_NAC]== _b[5.region#c.FECHA_NAC])
test (_b[1.region] == _b[2.region]== _b[3.region]== _b[4.region]== _b[5.region])

tsset ID Year
reg y x
set matsize 11000
reg D.y D.x i.ID i.Year
reg D.y D.x i.ID i.Year, nocon
areg D.y D.x i.Year, absorb(ID)
xtreg y x i.Year, fe
xtreg D.y D.x i.Year, fe
help xtreg

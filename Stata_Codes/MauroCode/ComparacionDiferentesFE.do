use "$base_out\DataCompleta_Stata.dta", clear
set matsize 1100
merge m:1 ano using "$base_out\Year_hw.dta" ,keepus(w_hweek)
compress
drop if _merge==2
drop _merge

gen YrMonth=ym(ano,MES)
gen YrQrt=yq(ano,Quarter)
gen region=gandina+2*gcaribe+3*gpacifica+4*gorinoquia+5*gamazonia
keep w_hweek ihw ihw50 ano MES region D_AreaMinadaProp D_ProduccionPerArea D_ExposicionMuni D_Upstream AreaMinadaProp ProduccionPerArea ExposicionMuni Upstream AreaMinadaProp2 ProduccionPerArea2 ExposicionMuni2 Upstream2 Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE APGAR_BAJO LBW codmpio FECHA_NAC
drop ProduccionPerArea2 AreaMinadaProp2 Upstream2 ExposicionMuni2
compress
drop if codmpio==.
drop if MadreSoltera==.
drop if EduMadrePostPrimaria==.
drop if EDAD_MADRE==.
drop if ihw50==. & ihw==.
drop if region==.
 
gen fixEf2a=string(ihw)+string(codmpio)
gen fixEf2b=string(ihw50)+string(codmpio)

encode fixEf2a, gen(FE_A)
encode fixEf2b, gen(FE_B)
drop fixEf2a fixEf2b
felsdvreg APGAR_BAJO c.D_AreaMinadaProp##c.ihw50 i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE  region#c.FECHA_NAC, ivar(fixEf2a) jvar(codmpio) peff(name) cluster(codmpio)
felsdvreg APGAR_BAJO inter  D_AreaMinadaProp  ihw50  MadreSoltera EduMadrePostPrimaria EDAD_MADRE , ivar(  codmpio ) jvar(FE_A) cluster(codmpio) feff(feff) peff(peff) mover(mover) group(group) xb(xb) res(res) mnum(mnum) pobs(pobs)
areg APGAR_BAJO inter  D_AreaMinadaProp  ihw50  MadreSoltera EduMadrePostPrimaria EDAD_MADRE i.codmpio, absorb(FE_A) vce(cluster codmpio)
areg APGAR_BAJO inter  D_AreaMinadaProp  ihw50  MadreSoltera EduMadrePostPrimaria EDAD_MADRE, absorb(FE_A) vce(cluster codmpio)
areg APGAR_BAJO inter  D_AreaMinadaProp  ihw50  MadreSoltera EduMadrePostPrimaria EDAD_MADRE c.ihw#codmpio, absorb(codmpio) vce(cluster codmpio)

*At the end i.w_hweek no se incluye por que es colinear con i.ano
local var_indepList AreaMinadaProp ExposicionMuni Upstream ProduccionPerArea
foreach var_indep of varlist `var_indepList'{
eststo clear
local control i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
local var_try1 APGAR_BAJO LBW
foreach vardep of varlist `var_try1'{
quietly eststo: quietly felsdvreg `vardep' c.D_`var_indep'##c.ihw50 `control'  region#c.FECHA_NAC, ivar(codmpio) jvar(fixEf2a) cluster(codmpio)
quietly estadd ysumm, mean
clear results
}

quietly estout using "$latexcodes\RegIndividual50HWdummies_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) ///
label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  keep(D_`var_indep' ihw50 c.D_`var_indep'#c.ihw50 )  ///
varl(ihw50 "Holy Week" c.D_`var_indep'#c.ihw50 Interaction)  stats(N ymean, fmt(a2 a2) labels ("N. of obs." "Mean of Dep. Var.")) replace

}






local var_indepList AreaMinadaProp ExposicionMuni Upstream ProduccionPerArea
foreach var_indep of varlist `var_indepList'{
eststo clear
local control i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
local var_try1 APGAR_BAJO LBW
foreach vardep of varlist `var_try1'{
eststo: quietly  areg `vardep' c.`var_indep'##c.ihw50 `control' c.ihw50#codmpio region#c.FECHA_NAC, absorb(codmpio) vce(cluster codmpio)
quietly estadd ysumm, mean
clear results
}
quietly estout using "$latexcodes\RegIndividualHW50Meassure_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) ///
label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  keep(`var_indep' ihw50 c.`var_indep'#c.ihw50 )  ///
varl(ihw50 "Holy Week" c.`var_indep'#c.ihw50 Interaction)  stats(N ymean, fmt(a2 a2) labels ("N. of obs." "Mean of Dep. Var.")) replace


}


local var_indepList AreaMinadaProp ExposicionMuni Upstream ProduccionPerArea
foreach var_indep of varlist `var_indepList'{
eststo clear
local control i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
local var_try1 APGAR_BAJO LBW
foreach vardep of varlist `var_try1'{
eststo: quietly areg `vardep' c.D_`var_indep'##c.ihw `control' c.ihw#codmpio region#c.FECHA_NAC, absorb(codmpio) vce(cluster codmpio)
quietly estadd ysumm, mean
clear results
}
quietly estout using "$latexcodes\RegIndividualHWdummies_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) ///
label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  keep(D_`var_indep' ihw c.D_`var_indep'#c.ihw )  ///
varl(ihw "Holy Week" c.D_`var_indep'#c.ihw Interaction)  stats(N ymean, fmt(a2 a2) labels ("N. of obs." "Mean of Dep. Var.")) replace


}





local var_indepList AreaMinadaProp ExposicionMuni Upstream ProduccionPerArea
foreach var_indep of varlist `var_indepList'{
eststo clear
local control i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
local var_try1 APGAR_BAJO LBW
foreach vardep of varlist `var_try1'{
eststo: quietly  areg `vardep' c.`var_indep'##c.ihw `control' c.ihw#codmpio region#c.FECHA_NAC, absorb(codmpio) vce(cluster codmpio)
quietly estadd ysumm, mean
clear results
}

quietly estout using "$latexcodes\RegIndividualHWMeassure_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) ///
label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  keep(`var_indep' ihw c.`var_indep'#c.ihw )  ///
varl(ihw "Holy Week" c.`var_indep'#c.ihw Interaction)  stats(N ymean, fmt(a2 a2) labels ("N. of obs." "Mean of Dep. Var.")) replace
}







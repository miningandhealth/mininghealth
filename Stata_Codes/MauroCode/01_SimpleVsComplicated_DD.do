use "$base_out\DataCompleta_Stata.dta", clear
set matsize 1200
sample 0.1


reg APGAR_BAJO D_AreaMinadaProp##MineriaArea_Ever, vce(cluster codmpio)
reg APGAR_BAJO D_ProduccionPerCapita##MineriaProd_Ever, vce(cluster codmpio)
reg APGAR_BAJO D_Upstream##Upstream_Ever, vce(cluster codmpio)
  

local control MadreSoltera EduMadrePostPrimaria EDAD_MADRE
reg APGAR_BAJO D_AreaMinadaProp##MineriaArea_Ever, vce(cluster codmpio)
areg APGAR_BAJO D_AreaMinadaProp `control' i.codmpio, absorb(FECHA_NAC) vce(cluster codmpio)
areg APGAR_BAJO D_AreaMinadaProp `control'  i.ano i.Semana_Naci, absorb(codmpio) vce(cluster codmpio)





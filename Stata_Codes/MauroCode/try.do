use "E:\Copy\MiningAndBirth\CreatedData\PanelMunicipalEEVV.dta", clear
gen MineriaArea=(AreaMinadaKm2>0) if !missing(AreaMinadaKm2) 
gen MineriaProd=( Produccion >0) if !missing(Produccion) 
gen MineriaIlegal=( IntervMines >0) if !missing(IntervMines) 
drop if ANO==2014
drop if Quarter==.
gen YrQrt = yq(ANO, Quarter)
tsset CODIGO_DANE_M YrQrt, quarterly
areg EDAD_MADRE i.ANO i.Quarter L.L.MineriaArea , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg EDAD_MADRE i.ANO i.Quarter L.MineriaArea , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg EDAD_MADRE i.ANO i.Quarter MineriaArea , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg EDAD_MADRE i.ANO i.Quarter F.MineriaArea , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg EDAD_MADRE i.ANO i.Quarter F.F.MineriaArea , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)

areg EDAD_MADRE i.ANO i.Quarter MineriaArea , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg EDAD_MADRE i.ANO i.Quarter MineriaProd , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg EDAD_MADRE i.ANO i.Quarter MineriaIlegal , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg EDAD_MADRE i.ANO i.Quarter MineriaArea , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg EDAD_MADRE i.ANO i.Quarter MineriaProd , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg EDAD_MADRE i.ANO i.Quarter MineriaIlegal , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg EDAD_MADRE i.ANO i.Quarter MineriaArea , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg EDAD_MADRE i.ANO i.Quarter MineriaProd , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg EDAD_MADRE i.ANO i.Quarter MineriaIlegal , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg LBW i.ANO i.Quarter MineriaArea , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg LBW i.ANO i.Quarter MineriaProd , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg LBW i.ANO i.Quarter MineriaIlegal , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg PartoHospital i.ANO i.Quarter MineriaArea , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg PartoHospital i.ANO i.Quarter MineriaProd , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg PartoHospital i.ANO i.Quarter MineriaIlegal , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg ConsultasPreMayor4 i.ANO i.Quarter MineriaArea , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg ConsultasPreMayor4 i.ANO i.Quarter MineriaProd , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg ConsultasPreMayor4 i.ANO i.Quarter MineriaIlegal , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg MadreSoltera i.ANO i.Quarter MineriaArea , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg MadreSoltera i.ANO i.Quarter MineriaProd , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg MadreSoltera i.ANO i.Quarter MineriaIlegal , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)



areg SIVIGILA_PrevPer i.ANO i.Quarter MineriaArea , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg SIVIGILA_PrevPer i.ANO i.Quarter MineriaProd , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg SIVIGILA_PrevPer i.ANO i.Quarter MineriaIlegal , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)

areg SIVIGILA_PrevCaso i.ANO i.Quarter MineriaArea , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg SIVIGILA_PrevCaso i.ANO i.Quarter MineriaProd , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg SIVIGILA_PrevCaso i.ANO i.Quarter MineriaIlegal , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)

areg RIPS_CIE_MERC i.ANO i.Quarter MineriaArea , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg RIPS_CIE_MERC i.ANO i.Quarter MineriaProd , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg RIPS_CIE_MERC i.ANO i.Quarter MineriaIlegal , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)

areg APGAR_BAJO i.ANO i.Quarter MineriaArea , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg APGAR_BAJO i.ANO i.Quarter MineriaProd , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg APGAR_BAJO i.ANO i.Quarter MineriaIlegal , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)


areg SIVIGILA_PrevPer i.ANO i.Quarter AreaMinadaKm2 , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg SIVIGILA_PrevPer i.ANO i.Quarter Produccion , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg SIVIGILA_PrevPer i.ANO i.Quarter IntervMines , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)

areg SIVIGILA_PrevCaso i.ANO i.Quarter AreaMinadaKm2 , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg SIVIGILA_PrevCaso i.ANO i.Quarter Produccion , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg SIVIGILA_PrevCaso i.ANO i.Quarter IntervMines , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)

areg RIPS_CIE_MERC i.ANO i.Quarter AreaMinadaKm2 , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg RIPS_CIE_MERC i.ANO i.Quarter Produccion , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg RIPS_CIE_MERC i.ANO i.Quarter IntervMines , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)

areg APGAR_BAJO i.ANO i.Quarter AreaMinadaKm2 , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg APGAR_BAJO i.ANO i.Quarter Produccion , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)
areg APGAR_BAJO i.ANO i.Quarter IntervMines , absorb( CODIGO_DANE_M) vce(cluster CODIGO_DANE_M)



  
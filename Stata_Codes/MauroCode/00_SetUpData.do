*cd "E:\Copy\MiningAndBirth\"
cd "C:\Users\Mauricio\Copy\MiningAndBirth\"
*cd "C:\Copy\MiningAndBirth\"
*cd "Z:\MiningAndBirth\"
*cd "C:\Users\Santi\Copy\PYP_Birth\"
use "CreatedData\DataCompleta.dta",clear
compress 
save "CreatedData\DataCompleta.dta",replace
gen Semana_Naci=week(FECHA_NAC)
gen dummyMineria=1 if AreaMinada>0
replace dummyMineria=0 if AreaMinada==0
gen dummyUpstream=1 if AreaMinadaUpstream>0
replace dummyUpstream=0 if AreaMinadaUpstream==0
gen AreaMinadaProp=AreaMinada/AreaMunicipioTotal
gen AreaMinadaPropDensidad=AreaMinadaProp/Poblacion
gen AreaMinadaUpstreamProp=AreaMinadaUpstream/AreaMunicipioTotal
gen AreaMinadaUpstreamPropDensidad=AreaMinadaUpstream/Poblacion
gen LBW=1 if PESO_NAC<=2500
replace LBW=0 if PESO_NAC>2500
gen VLBW=1 if PESO_NAC<=1500
replace VLBW=0 if PESO_NAC>1500
gen Premature=1 if T_GES<=3
replace Premature=0 if T_GES>3

gen AreaMinada2=AreaMinada*AreaMinada
gen AreaMinadaUpstream2=AreaMinadaUpstream*AreaMinadaUpstream
gen AreaMinadaProp2=AreaMinadaProp*AreaMinadaProp
gen AreaMinadaPropDensidad2=AreaMinadaPropDensidad*AreaMinadaPropDensidad
gen AreaMinadaUpstreamProp2=AreaMinadaUpstreamProp*AreaMinadaUpstreamProp
gen AreaMinadaUpstreamPropDensidad2=AreaMinadaUpstreamPropDensidad*AreaMinadaUpstreamPropDensidad



label var dummyMineria "Mining(=1)"
label var dummyUpstream "Upstream Mining(=1)"
label var AreaMinada "Mining Area"
label var AreaMinadaUpstream "Upstream Area (Adjusted)"
label var AreaMinadaProp "Mining Area/Municipality Area"
label var AreaMinadaUpstreamProp "Upstream Area (Adjusted)/Municipality Area"
label var AreaMinadaPropDensidad "Mining Area/Population Density"
label var AreaMinadaUpstreamPropDensidad "Upstream Area (Adjusted)/Population Density"
label var AreaMinada2 "[Mining Area]^2"
label var AreaMinadaUpstream2 "[Upstream Area (Adjusted)]^2"
label var AreaMinadaProp2 "[Mining Area/Municipality Area]^2"
label var AreaMinadaUpstreamProp2 "[Upstream Area (Adjusted)/Municipality Area]^2"
label var AreaMinadaPropDensidad2 "[Mining Area/Population Density]^2"
label var AreaMinadaUpstreamPropDensidad "[Upstream Area (Adjusted)/Population Density]^2"

foreach vardep of varlist APGAR_BAJO PESO_NAC TALLA_NAC LBW VLBW Premature{
local control i.Semana_Naci i.ANO EDAD_MADRE i.AREA_NACI i.SIT_PARTO i.NIV_EDUM
eststo clear
eststo: quietly xi: areg `vardep' dummyMineria `control', absorb( MUNIC_NACI) vce(cluster MUNIC_NACI) 
estadd ysumm
eststo: quietly xi: areg `vardep' dummyUpstream `control', absorb( MUNIC_NACI) vce(cluster MUNIC_NACI) 
estadd ysumm
eststo: quietly xi: areg `vardep' dummyMineria dummyUpstream `control', absorb( MUNIC_NACI) vce(cluster MUNIC_NACI) 
estadd ysumm
eststo: quietly xi: areg `vardep' AreaMinada AreaMinada2 `control', absorb( MUNIC_NACI) vce(cluster MUNIC_NACI)
estadd ysumm
eststo: quietly xi: areg `vardep' AreaMinadaUpstream AreaMinadaUpstream2 `control', absorb( MUNIC_NACI) vce(cluster MUNIC_NACI)
estadd ysumm
eststo: quietly xi: areg `vardep' AreaMinada AreaMinada2 AreaMinadaUpstream AreaMinadaUpstream2 `control', absorb( MUNIC_NACI) vce(cluster MUNIC_NACI)
estadd ysumm
eststo: quietly xi: areg `vardep' AreaMinadaProp AreaMinadaProp2  `control', absorb( MUNIC_NACI) vce(cluster MUNIC_NACI)
estadd ysumm
eststo: quietly xi: areg `vardep' AreaMinadaUpstreamProp AreaMinadaUpstreamProp2 `control', absorb( MUNIC_NACI) vce(cluster MUNIC_NACI)
estadd ysumm
eststo: quietly xi: areg `vardep' AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstreamProp AreaMinadaUpstreamProp2 `control', absorb( MUNIC_NACI) vce(cluster MUNIC_NACI)
estadd ysumm
eststo: quietly xi: areg `vardep' AreaMinadaPropDensidad AreaMinadaPropDensidad2 `control', absorb( MUNIC_NACI) vce(cluster MUNIC_NACI)
estadd ysumm
eststo: quietly xi: areg `vardep' AreaMinadaUpstreamPropDensidad AreaMinadaUpstreamPropDensidad2 `control', absorb( MUNIC_NACI) vce(cluster MUNIC_NACI)
estadd ysumm
eststo: quietly xi: areg `vardep' AreaMinadaPropDensidad AreaMinadaPropDensidad2 AreaMinadaUpstreamPropDensidad AreaMinadaUpstreamPropDensidad2 `control', absorb( MUNIC_NACI) vce(cluster MUNIC_NACI)
estadd ysumm
esttab using "Results\Reg_`vardep'Ind.tex", se ar2 booktabs label /// 
indicate(  "Year F.E.= _ISemana_Na*"  "Week F.E.= _IANO*"  "Mother Charac.= EDAD_MADRE _INIV_ED* _ISIT_PA* _IAREA_NA*" ) ///
replace title("Effect of mining on APGAR Scores")   b(a2) se(a2) nocon nomtitles  ///
keep(Mining Mining^2 Upstream-Mining Upstream-Mining^2) ///
rename( ///
dummyMineria Mining dummyUpstream "Upstream-Mining"  ///
AreaMinada Mining AreaMinada2 Mining^2 AreaMinadaUpstream Upstream-Mining AreaMinadaUpstream2 Upstream-Mining^2  ///
AreaMinadaProp Mining AreaMinadaProp2 Mining^2 AreaMinadaUpstreamProp Upstream-Mining AreaMinadaUpstreamProp2 Upstream-Mining^2 ///
AreaMinadaPropDensidad Mining AreaMinadaPropDensidad2 Mining^2 AreaMinadaUpstreamPropDensidad Upstream-Mining AreaMinadaUpstreamPropDensidad2 Upstream-Mining^2)  ///
star(* 0.10 ** 0.05 *** 0.01) stats(N ymean, labels ("N. of obs." "Mean of Dep. Var.")) ///
nonotes addnotes("\specialcell{Clustered standard errors, by municipality, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")
}

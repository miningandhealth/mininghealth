cd "E:\Copy\MiningAndBirth\"
set matsize 11000
use "CreatedData/DataCompleta1.dta",clear
eststo clear

keep APGAR_BAJO AreaMinadaProp  AreaMinadaUpstreamProp AreaMinadaProp2  AreaMinadaUpstreamProp2 ANO MES AREA_NACI EDAD_MADRE CODIGO_DANE MinDist DPTO_NACI


****Regresiones Basicas
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstreamProp AreaMinadaUpstreamProp2 i.ANO i.MES, vce(cluster CODIGO_DANE)
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstreamProp AreaMinadaUpstreamProp2 i.ANO i.MES  i.AREA_NACI EDAD_MADRE , vce(cluster CODIGO_DANE)
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstreamProp AreaMinadaUpstreamProp2 i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.DPTO_NACI , vce(cluster CODIGO_DANE)
*eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstreamProp AreaMinadaUpstreamProp2 i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.DPTO_NACI  if MinDist<10000, vce(cluster CODIGO_DANE)
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstreamProp AreaMinadaUpstreamProp2 i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.DPTO_NACI  if MinDist<30000, vce(cluster CODIGO_DANE)
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstreamProp AreaMinadaUpstreamProp2 i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.DPTO_NACI  if MinDist<50000, vce(cluster CODIGO_DANE)

esttab using "Results\Result_InesityStat_DtoFE.tex", se ar2 booktabs label indicate( "Month and Year F.E.= _IANO* _IMES*" "Mother Characteristics= _IAREA_NACI* EDAD_MADRE"  "State F.E.=  _IDPTO_NACI*" ) replace b(a2) se(a2) nocon nomtitles  keep(AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstreamProp AreaMinadaUpstreamProp2)

cd "E:\Copy\MiningAndBirth\"
set matsize 11000
use "CreatedData/DataCompleta1.dta",clear
eststo clear
keep APGAR_BAJO AreaMinadaProp  AreaMinadaUpstreamProp AreaMinadaProp2  AreaMinadaUpstreamProp2 ANO MES AREA_NACI EDAD_MADRE CODIGO_DANE MinDist DPTO_NACI
replace AreaMinadaProp=0 if AreaMinadaProp==.
replace AreaMinadaProp2=0 if AreaMinadaProp2==.
replace AreaMinadaUpstreamProp=0 if AreaMinadaUpstreamProp==.
replace AreaMinadaUpstreamProp2=0 if AreaMinadaUpstreamProp2==.
****Regresiones Basicas
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstreamProp AreaMinadaUpstreamProp2 i.ANO i.MES, vce(cluster CODIGO_DANE)
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstreamProp AreaMinadaUpstreamProp2 i.ANO i.MES  i.AREA_NACI EDAD_MADRE , vce(cluster CODIGO_DANE)
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstreamProp AreaMinadaUpstreamProp2 i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.DPTO_NACI , vce(cluster CODIGO_DANE)
*eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstreamProp AreaMinadaUpstreamProp2 i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.DPTO_NACI  if MinDist<10000, vce(cluster CODIGO_DANE)
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstreamProp AreaMinadaUpstreamProp2 i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.DPTO_NACI  if MinDist<30000, vce(cluster CODIGO_DANE)
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstreamProp AreaMinadaUpstreamProp2 i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.DPTO_NACI  if MinDist<50000, vce(cluster CODIGO_DANE)
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstreamProp AreaMinadaUpstreamProp2 i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.CODIGO_DANE , vce(cluster CODIGO_DANE)

esttab using "Results\Result_InesityStat_DtoFE_0.tex", se ar2 booktabs label indicate( "Month and Year F.E.= _IANO* _IMES*" "Mother Characteristics= _IAREA_NACI* EDAD_MADRE"  "State F.E.=  _IDPTO_NACI*" ) replace b(a2) se(a2) nocon nomtitles  keep(AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstreamProp AreaMinadaUpstreamProp2)


cd "E:\Copy\MiningAndBirth\"
set matsize 11000
use "CreatedData/DataCompleta1.dta",clear
eststo clear
keep APGAR_BAJO FECHA_NAC INDEX_MUNMINERO ANO MES AREA_NACI EDAD_MADRE CODIGO_DANE MinDist DPTO_NACI
compress
****Regresiones Basicas



eststo:  xi: regress APGAR_BAJO c.FECHA_NAC##INDEX_MUNMINERO i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.DPTO_NACI  if MinDist<10000, vce(cluster CODIGO_DANE)
eststo:  xi: regress APGAR_BAJO c.FECHA_NAC##INDEX_MUNMINERO i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.DPTO_NACI  if MinDist<30000, vce(cluster CODIGO_DANE)
eststo:  xi: regress APGAR_BAJO c.FECHA_NAC##INDEX_MUNMINERO i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.DPTO_NACI  if MinDist<50000, vce(cluster CODIGO_DANE)
eststo:  xi: regress APGAR_BAJO c.FECHA_NAC##INDEX_MUNMINERO i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.DPTO_NACI, vce(cluster CODIGO_DANE)
esttab using "Results\Result_Parallel1.tex", se ar2 booktabs label indicate( "Month and Year F.E.= _IANO* _IMES*" "Mother Characteristics= _IAREA_NACI* EDAD_MADRE"  "State F.E.=  _IDPTO_NACI*" ) replace b(a2) se(a2) nocon nomtitles  keep(FECHA_NAC 1.INDEX_MUNMINERO 1.INDEX_MUNMINERO#c.FECHA_NAC)

/*
eststo:  xi: regress APGAR_BAJO c.FECHA_NAC##INDEX_MUNMINERO i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.CODIGO_DANE if MinDist<10000, vce(cluster CODIGO_DANE)
eststo:  xi: regress APGAR_BAJO c.FECHA_NAC##INDEX_MUNMINERO i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.CODIGO_DANE if MinDist<30000, vce(cluster CODIGO_DANE)
eststo:  xi: regress APGAR_BAJO c.FECHA_NAC##INDEX_MUNMINERO i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.CODIGO_DANE if MinDist<50000, vce(cluster CODIGO_DANE)
eststo:  xi: regress APGAR_BAJO c.FECHA_NAC##INDEX_MUNMINERO i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.CODIGO_DANE, vce(cluster CODIGO_DANE)
esttab using "Results\Result_Parallel.tex", se ar2 booktabs label indicate( "Month and Year F.E.= _IANO* _IMES*" "Mother Characteristics= _IAREA_NACI* EDAD_MADRE"  "Municipality F.E.=  _ICODIGO_DA*" ) replace b(a2) se(a2) nocon nomtitles  keep(AreaMinadaProp AreaMinadaUpstreamProp)
*/

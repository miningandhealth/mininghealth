/*
cd "E:\Copy\MiningAndBirth\"
set matsize 11000
use "CreatedData/DataCompleta.dta",clear
compress
save "CreatedData/DataCompleta1.dta",replace
*/

cd "E:\Copy\MiningAndBirth\"
*cd "C:\Copy\MiningAndBirth\"
set matsize 11000
use "CreatedData/DataCompleta1.dta",clear

eststo clear
keep APGAR_BAJO AreaMinadaProp  AreaMinadaUpstream  AreaMinadaProp_M  AreaMinadaUpstreamProp_M ANO MES AREA_NACI EDAD_MADRE CODIGO_DANE
****Regresiones Basicas
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp AreaMinadaUpstreamProp i.ANO i.MES , vce(cluster CODIGO_DANE)
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp AreaMinadaUpstreamProp i.ANO i.MES  i.AREA_NACI EDAD_MADRE , vce(cluster CODIGO_DANE)
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp AreaMinadaUpstreamProp i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.CODIGO_DANE, vce(cluster CODIGO_DANE)
	
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp AreaMinadaUpstreamProp i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.CODIGO_DANE if MinDist<10000, vce(cluster CODIGO_DANE)
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp AreaMinadaUpstreamProp i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.CODIGO_DANE if MinDist<30000, vce(cluster CODIGO_DANE)	
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp AreaMinadaUpstreamProp i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.CODIGO_DANE if MinDist<50000, vce(cluster CODIGO_DANE)	

	


esttab using "Results\Result_InesityStat.tex"", se ar2 booktabs label indicate( "Month and Year F.E.= _ANO* _MES" "Mother Characteristics= _AREA_NACI* EDAD_MADRE"  "Municipality F.E.=  _CODIGO_DANE* ) replace b(a2) se(a2) nocon nomtitles  keep(AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstreamProp AreaMinadaUpstreamProp2)



****Regresiones Basicas Madre
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp_M AreaMinadaUpstreamProp_M  i.ANO i.MES , vce(cluster CODIGO_DANE)
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp_M AreaMinadaUpstreamProp_M  i.ANO i.MES  i.AREA_NACI EDAD_MADRE , vce(cluster CODIGO_DANE)
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp_M AreaMinadaUpstreamProp_M  i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.CODIGO_DANE, vce(cluster CODIGO_DANE)
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp_M AreaMinadaUpstreamProp_M  i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.CODIGO_DANE if MinDist<10000, vce(cluster CODIGO_DANE)
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp_M AreaMinadaUpstreamProp_M  i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.CODIGO_DANE if MinDist<30000, vce(cluster CODIGO_DANE)	
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp_M AreaMinadaUpstreamProp_M  i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.CODIGO_DANE if MinDist<50000, vce(cluster CODIGO_DANE)	

	


esttab using "Results\Result_InesityStat_M.tex"", se ar2 booktabs label indicate( "Month and Year F.E.= _ANO* _MES" "Mother Characteristics= _AREA_NACI* EDAD_MADRE"  "Municipality F.E.=  _CODIGO_DANE* ) replace b(a2) se(a2) nocon nomtitles  keep(AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstreamProp AreaMinadaUpstreamProp2)

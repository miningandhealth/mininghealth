datareg=zeros(size(muni,1)*12*17,4);
years=[1992:2008]';
months=[1:12]';
idata=1;

ifecha=1;
datareg(:,1)=kron(muni, ones(12*17,1));
datareg(:,2)=kron(ones(size(muni,1),1),kron(years, ones(12,1)) );
datareg(:,3)=kron( ones(size(muni,1)*17,1), months);

for imuni=1:size(muni,1)
    for ifecha=1:size(fechas,2)
        while idata<46513 && datareg(idata,1)==muni(imuni,1) && (datareg(idata,2)*10000+datareg(idata,3)*100)<fechas(ifecha) 
            datareg(idata,4)=aream(imuni,ifecha);
            idata=idata+1;
        end
    end
end
/*
cd "E:\Copy\MiningAndBirth\"
set matsize 11000
use "CreatedData/DataCompleta.dta",clear
compress
save "CreatedData/DataCompleta1.dta",replace
*/


cd "E:\Copy\MiningAndBirth\"
*cd "C:\Copy\MiningAndBirth\"
*cd "Z:\MiningAndBirth\"
set matsize 11000
use "CreatedData/DataCompleta1.dta",clear
eststo clear
keep APGAR_BAJO AreaMinadaProp AreaMinadaProp2  AreaMinadaUpstream AreaMinadaUpstream2  AreaMinadaProp_M  AreaMinadaProp_M2 AreaMinadaUpstream_M AreaMinadaUpstream_M2 ANO MES AREA_NACI EDAD_MADRE CODIGO_DANE MinDist CODIGO_DANE_M DPTO_R DPTO_NACI

****Regresiones Basicas
eststo:  regress APGAR_BAJO AreaMinadaProp AreaMinadaUpstream i.AREA_NACI EDAD_MADRE i.CODIGO_DANE , vce(cluster CODIGO_DANE)

drop if CODIGO_DANE_M=="NA"
destring CODIGO_DANE_M, replace
eststo:  regress APGAR_BAJO AreaMinadaProp_M AreaMinadaUpstream_M i.AREA_NACI EDAD_MADRE i.CODIGO_DANE_M , vce(cluster CODIGO_DANE_M)

esttab using "Results\Result_InesityStat.tex", se ar2 booktabs label indicate( "Month and Year F.E.= _IANO* _IMES*" "Mother Characteristics= _IAREA_NACI* EDAD_MADRE"  "State F.E.=  _IDPTO_NACI*" ) replace b(a2) se(a2) nocon nomtitles  keep(AreaMinadaProp AreaMinadaProp2  AreaMinadaUpstream AreaMinadaUpstream2 )



****Regresiones Basicas Madre
eststo clear
gen AreaMinadaUpstream_M2=AreaMinadaUpstream_M*AreaMinadaUpstream_M
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp_M AreaMinadaProp_M2 AreaMinadaUpstream_M AreaMinadaUpstream_M2  i.ANO i.MES , vce(cluster CODIGO_DANE_M)
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp_M AreaMinadaProp_M2 AreaMinadaUpstream_M AreaMinadaUpstream_M2  i.ANO i.MES  i.AREA_NACI EDAD_MADRE , vce(cluster CODIGO_DANE_M)
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp_M AreaMinadaProp_M2 AreaMinadaUpstream_M AreaMinadaUpstream_M2  i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.DPTO_R, vce(cluster CODIGO_DANE_M)
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp_M AreaMinadaProp_M2 AreaMinadaUpstream_M AreaMinadaUpstream_M2  i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.DPTO_R if MinDist<50, vce(cluster CODIGO_DANE_M)
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp_M AreaMinadaProp_M2 AreaMinadaUpstream_M AreaMinadaUpstream_M2  i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.DPTO_R if MinDist<100, vce(cluster CODIGO_DANE_M)	
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp_M AreaMinadaProp_M2 AreaMinadaUpstream_M AreaMinadaUpstream_M2  i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.DPTO_R if MinDist<500, vce(cluster CODIGO_DANE_M)	


esttab using "Results\Result_InesityStat_M.tex", se ar2 booktabs label indicate( "Month and Year F.E.= _IANO* _IMES*" "Mother Characteristics= _IAREA_NACI* EDAD_MADRE"  "State F.E.=  _IDPTO_R*" ) replace b(a2) se(a2) nocon nomtitles  keep(AreaMinadaProp_M AreaMinadaProp_M2  AreaMinadaUpstream_M AreaMinadaUpstream_M2)


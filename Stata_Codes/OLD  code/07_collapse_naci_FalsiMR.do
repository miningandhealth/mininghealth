*cd "E:\Copy\MiningAndBirth\"
*cd "C:\Copy\MiningAndBirth\"
cd "Z:\MiningAndBirth\"
set matsize 11000
use "CreatedData/DataMuni_Falsi.dta",clear
compress
rename Group_1 ANO
rename Group_2 MES
rename Group_3 CODIGO_DANE

gen modate = ym(ANO, MES) 
tsset CODIGO_DANE modate, monthly



eststo clear
eststo: quietly  reg APGAR_BAJO AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstream i.CODIGO_DANE , vce(cluster CODIGO_DANE)
eststo: quietly  reg APGAR_BAJO AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstream i.CODIGO_DANE i.ANO , vce(cluster CODIGO_DANE)
eststo: quietly  reg APGAR_BAJO AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstream i.CODIGO_DANE i.ANO i.MES, vce(cluster CODIGO_DANE)
esttab using "Results\ResultItensity_Collap_Falsi.tex", se r2 booktabs label indicate(  "Month F.E.=  *MES*" "Year F.E.=  *ANO*" "Municipality F.E.=  *CODIGO_DA*" ) replace b(a2) se(a2) nocon nomtitles  

eststo clear
eststo: quietly  reg D.APGAR_BAJO D.AreaMinadaProp c.(D.AreaMinadaProp)#c.(D.AreaMinadaProp) D.AreaMinadaUpstream i.CODIGO_DANE , vce(cluster CODIGO_DANE)
eststo: quietly  reg D.APGAR_BAJO D.AreaMinadaProp c.(D.AreaMinadaProp)#c.(D.AreaMinadaProp) D.AreaMinadaUpstream i.CODIGO_DANE i.ANO , vce(cluster CODIGO_DANE)
eststo: quietly  reg D.APGAR_BAJO D.AreaMinadaProp c.(D.AreaMinadaProp)#c.(D.AreaMinadaProp) D.AreaMinadaUpstream i.CODIGO_DANE i.ANO i.MES, vce(cluster CODIGO_DANE)
esttab using "Results\ResultItensity_CollapFD_Falsi.tex", se r2 booktabs label indicate(  "Month F.E.=  *MES*" "Year F.E.=  *ANO*" "Municipality F.E.=  *CODIGO_DA*" ) replace b(a2) se(a2) nocon nomtitles  



*cd "E:\Copy\MiningAndBirth\"
*cd "C:\Copy\MiningAndBirth\"
cd "Z:\MiningAndBirth\"
set matsize 11000
use "CreatedData/DataMuni_M_Falsi.dta",clear
compress
rename Group_1 ANO
rename Group_2 MES
rename Group_3 CODIGO_DANE
destring CODIGO_DANE,replace
gen modate = ym(ANO, MES) 
tsset CODIGO_DANE modate, monthly

eststo clear
eststo: quietly  reg APGAR_BAJO AreaMinadaProp_M AreaMinadaProp_M2 AreaMinadaUpstream_M i.CODIGO_DANE , vce(cluster CODIGO_DANE)
eststo: quietly  reg APGAR_BAJO AreaMinadaProp_M AreaMinadaProp_M2 AreaMinadaUpstream_M i.CODIGO_DANE i.ANO , vce(cluster CODIGO_DANE)
eststo: quietly  reg APGAR_BAJO AreaMinadaProp_M AreaMinadaProp_M2 AreaMinadaUpstream_M i.CODIGO_DANE i.ANO i.MES, vce(cluster CODIGO_DANE)
esttab using "Results\ResultItensity_Collap_M_Falsi.tex", se r2 booktabs label indicate(  "Month F.E.=  *MES*" "Year F.E.=  *ANO*" "Municipality F.E.=  *CODIGO_DA*" ) replace b(a2) se(a2) nocon nomtitles  

eststo clear
eststo: quietly  reg D.APGAR_BAJO D.AreaMinadaProp_M c.(D.AreaMinadaProp_M)#c.(D.AreaMinadaProp_M)  D.AreaMinadaUpstream_M i.CODIGO_DANE , vce(cluster CODIGO_DANE)
eststo: quietly  reg D.APGAR_BAJO D.AreaMinadaProp_M c.(D.AreaMinadaProp_M)#c.(D.AreaMinadaProp_M)  D.AreaMinadaUpstream_M i.CODIGO_DANE i.ANO , vce(cluster CODIGO_DANE)
eststo: quietly  reg D.APGAR_BAJO D.AreaMinadaProp_M c.(D.AreaMinadaProp_M)#c.(D.AreaMinadaProp_M)  D.AreaMinadaUpstream_M i.CODIGO_DANE i.ANO i.MES, vce(cluster CODIGO_DANE)
esttab using "Results\ResultItensity_CollapFD_M_Falsi.tex", se r2 booktabs label indicate(  "Month F.E.=  *MES*" "Year F.E.=  *ANO*" "Municipality F.E.=  *CODIGO_DA*" ) replace b(a2) se(a2) nocon nomtitles  


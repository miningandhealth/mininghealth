use "$base_out\PanelMunicipalEEVV_Stata.dta", clear
merge 1:1 ano Quarter codmpio using "$base_out\NonMissingsCount.dta"
drop _merge
gen region=gandina+2*gcaribe+3*gpacifica+4*gorinoquia+5*gamazonia

gen MercurioPerCapita=MercurioRIPS/PoblacionMuni

drop if ano==2014
drop if Quarter==.
tsset codmpio YrQrt, quarterly



local var_indepList AreaMinadaProp ProduccionPerArea
foreach var_indep of varlist `var_indepList'{
eststo clear
local var_try1 SIVIGILA_PrevPer 
foreach vardep of varlist `var_try1'{
eststo: quietly xi: areg `vardep' `var_indep' `var_indep'2 i.ano i.Quarter region#c.YrQrt [aw=PoblacionMuni], absorb( codmpio) vce(cluster codmpio) 
estadd ysumm
}

estout using "$latexcodes\SaludMeasure_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(`var_indep' `var_indep'2 )  stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$")) replace

eststo clear
local var_try1 SIVIGILA_PrevPer
foreach vardep of varlist `var_try1'{
eststo: quietly xi: areg `vardep' D_`var_indep'  i.ano i.Quarter region#c.YrQrt [aw=PoblacionMuni], absorb( codmpio) vce(cluster codmpio) 
estadd ysumm

}
estout using "$latexcodes\SaludDummies_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_`var_indep')  stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$")) replace


}



eststo clear
local var_try1 SIVIGILA_PrevPer 
foreach vardep of varlist `var_try1'{
eststo: quietly xi: areg `vardep' ExposicionMuni ExposicionMuni2 Upstream Upstream2 i.ano i.Quarter region#c.YrQrt [aw=PoblacionMuni], absorb( codmpio) vce(cluster codmpio) 
estadd ysumm
}

estout using "$latexcodes\SaludMeasure_Combo.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(ExposicionMuni ExposicionMuni2 Upstream Upstream2 )  stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$")) replace

eststo clear
local var_try1 SIVIGILA_PrevPer 
foreach vardep of varlist `var_try1'{
eststo: quietly xi: areg `vardep' D_ExposicionMuni D_Upstream  i.ano i.Quarter region#c.YrQrt [aw=PoblacionMuni], absorb( codmpio) vce(cluster codmpio) 
estadd ysumm

}
estout using "$latexcodes\SaludDummies_Combo.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_ExposicionMuni D_Upstream)  stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$")) replace


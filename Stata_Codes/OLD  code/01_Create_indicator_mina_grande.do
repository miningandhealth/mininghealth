clear all
insheet using "C:\Users\santi\Copy\PYP_Birth\RawData\TMC18Agosto2011.csv"
gen concesion=0
replace concesion = 1 if strmatch(modalidade, "*CONTRATO DE CO*")
gen oro=0
replace oro = 1 if strmatch(minerales, "*ORO*")
gen leftp=strpos(titulares,"(")
gen rightp=strpos(titulares,")")
gen id_lic_holder=substr(titulares,leftp+1,rightp-2)
destring id_lic_holder, force replace
gen persona=0
replace persona=1 if id_lic_holder<1050000000
save "C:\Users\santi\Copy\PYP_Birth\CreatedData\All_mines_persona_concesion.dta", replace

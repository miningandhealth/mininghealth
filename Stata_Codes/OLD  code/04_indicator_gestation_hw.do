use "$base_out\DataCompleta_Stata.dta", clear
capture drop dia- ExposicionMunixihw50
gen dia=day(FECHA_NAC)
gen wdate=mdy(MES,dia,ano)
sort wdate
merge m:1 wdate using "$base_out\Closest_hw"
drop if _merge!=3
drop _merge
gen wgesthw=ceil((wdate-closest_hw)/7)

replace wgesthw=. if SEMANAS==.
qui label var wgesthw "Week during gestation when holy week happens"
gen ihw=1 if wgesthw<=SEMANAS & wgesthw!=.
replace ihw=0 if wgesthw>SEMANAS & wgesthw!=.
label var ihw "Holy Week during gestation"

/*
forval i=1/40{
gen ihw`i'=1 if wgesthw==`i' & wgesthw<=SEMANAS & wgesthw!=.
replace ihw`i'=0 if ihw==0
label var ihw`i' "Holy Week `i'th week of gestation"
}
*/
gen ihw_semana=wgesthw if wgesthw<=SEMANAS & wgesthw!=.
label var ihw_semana "Week of gestation that overlaps with Holy Week"


gen AreaMinadaPropxihw=AreaMinadaProp*ihw
gen Upstreamxihw=Upstream*ihw
gen ProduccionPerAreaxihw=ProduccionPerArea*ihw
gen ExposicionMunixihw=ExposicionMuni*ihw

gen Arena_AreaMinadaPropxihw=Arena_AreaMinadaProp*ihw
gen Arena_Upstreamxihw=Arena_Upstream*ihw
gen Arena_ExposicionMunixihw=Arena_ExposicionMuni*ihw


label var ihw "Holy Week"

label var AreaMinadaPropxihw "Mining Area/Municipality Area \$\times\$ Holy Week"
label var Upstreamxihw "Upstream exposure \$\times\$ Holy Week"
label var ProduccionPerAreaxihw "Production/Municipality Area \$\times\$ Holy Week"
label var ExposicionMunixihw "Exposure \$\times\$ Holy Week"

label var Arena_AreaMinadaPropxihw "Mining Area/Municipality Area \$\times\$ Holy Week"
label var Arena_Upstreamxihw "Upstream exposure \$\times\$ Holy Week"
label var Arena_ExposicionMunixihw "Exposure \$\times\$ Holy Week"


compress
save "$base_out\DataCompleta_Stata.dta", replace
clear all



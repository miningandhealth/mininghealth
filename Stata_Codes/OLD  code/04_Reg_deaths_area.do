clear
set mem 700000
set matsize 2000
use "C:\Users\Santi\Copy\PYP_Birth\CreatedData\panel_defmun.dta", clear
*use "panel_defmun.dta", clear
*save "panel_defmun_ind.dta", replace

tab mes, gen(imes)
tab ano, gen(iano)


reg  ndeaths  areamin imes* iano* 
outreg using death_area, se var starlevels(10 5 1) keep(  areamin _cons ) note( Coefficients for fixed effects not shown) tex fragment replace  

xi: reg  ndeaths  areamin imes* iano* i.coddane
outreg using death_area, se var starlevels(10 5 1) keep(  areamin _cons ) note( Coefficients for fixed effects not shown) tex fragment merge  


reg  ndeaths  areamin imes* iano*  if never!=1
outreg using death_area, se var starlevels(10 5 1) keep(  areamin _cons ) note( Coefficients for fixed effects not shown) tex fragment merge
xi: reg  ndeaths  areamin imes* iano* i.coddane if never!=1
outreg using death_area, se var starlevels(10 5 1) keep(  areamin _cons ) note( Coefficients for fixed effects not shown) tex fragment merge 


reg  hgrel  areamin imes* iano* 
outreg using hgrel_area, se var starlevels(10 5 1) keep(  areamin _cons) note( Coefficients for fixed effects not shown) tex fragment replace  

xi: reg  hgrel  areamin imes* iano* i.coddane
outreg using hgrel_area, se var starlevels(10 5 1) keep(  areamin _cons) note( Coefficients for fixed effects not shown) tex fragment merge  


reg  hgrel  areamin imes* iano*  if never!=1
outreg using hgrel_area, se var starlevels(10 5 1) keep(  areamin _cons) note( Coefficients for fixed effects not shown) tex fragment merge 
xi: reg  hgrel  areamin imes* iano* i.coddane if never!=1
outreg using hgrel_area, se var starlevels(10 5 1) keep(  areamin _cons ) note( Coefficients for fixed effects not shown) tex fragment merge 


reg  mbebe  areamin imes* iano* 
outreg using mbebe_area, se var starlevels(10 5 1) keep(  areamin _cons ) note( Coefficients for fixed effects not shown) tex fragment replace  

xi: reg  mbebe  areamin imes* iano* i.coddane
outreg using mbebe_area, se var starlevels(10 5 1) keep(  areamin _cons ) note( Coefficients for fixed effects not shown) tex fragment merge  


reg  mbebe  areamin imes* iano*  if never!=1
outreg using mbebe_area, se var starlevels(10 5 1) keep(  areamin _cons) note( Coefficients for fixed effects not shown) tex fragment merge 
xi: reg  mbebe  areamin imes* iano* i.coddane if never!=1
outreg using mbebe_area, se var starlevels(10 5 1) keep(  areamin _cons) note( Coefficients for fixed effects not shown) tex fragment merge 



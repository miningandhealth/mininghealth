
use "$base_in\cm_bd_final.dta", clear


drop if at_pyr_03_01_07!=1

gen legalidad=(tipo_mina==1) if !missing(tipo_mina)
estpost tabstat legalidad, by(ag_ue_01) statistics(mean count) columns(statistics)
collapse (mean) legalidad (count) minasCenso=legalidad, by( cod_mpio)
save "$base_out\cm_bd_oro_cod_mpio.dta", replace

import delim "$base_out\BaseMineriaIlegal_MinDefensa.csv", clear

collapse (sum) cantidad, by(cod_muni)
rename cod_muni cod_mpio

merge 1:1 cod_mpio using "$base_out\cm_bd_oro_cod_mpio.dta"

replace cantidad=0 if cantidad==.
replace minasCenso=0 if minasCenso==.
replace legalidad=. if minasCenso==0



label var cantidad "Police"
label var legalidad "Proportion of legal mines"
label var minasCenso "Total number of mines in 2011"

keep if cantidad>0 | legalidad<1

drop cantidad legalidad minasCenso
replace cod_mpio=13600 if cod_mpio==13490
replace cod_mpio=19142 if cod_mpio==19300
replace cod_mpio=27787 if cod_mpio==27160
replace cod_mpio=27205 if cod_mpio==27580
replace cod_mpio=27999 if cod_mpio==27205 | cod_mpio==27361 | cod_mpio==27491



gen Ilegal=1
drop _merge
rename cod_mpio codmpio
collapse (mean) Ilegal, by(codmpio)
save "$base_out\ilegalMunis.dta", replace


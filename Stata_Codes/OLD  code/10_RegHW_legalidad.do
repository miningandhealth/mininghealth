use "$base_out\DataCompleta_Stata.dta", clear
set matsize 1200
merge m:1 ano using "$base_out\Year_hw.dta" ,keepus(w_hweek)
compress
drop if _merge==2
drop _merge

merge m:1 codmpio using "$base_out\ilegalMunis.dta"
drop if _merge==2
replace Ilegal=0 if _merge==1
drop _merge

gen YrMonth=ym(ano,MES)
gen YrQrt=yq(ano,Quarter)
gen region=gandina+2*gcaribe+3*gpacifica+4*gorinoquia+5*gamazonia
keep Ilegal w_hweek ihw ihw50 ano MES region D_AreaMinadaProp D_ProduccionPerArea D_ExposicionMuni D_Upstream AreaMinadaProp ProduccionPerArea ExposicionMuni Upstream AreaMinadaProp2 ProduccionPerArea2 ExposicionMuni2 Upstream2 Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE APGAR_BAJO LBW codmpio FECHA_NAC
drop ProduccionPerArea2 AreaMinadaProp2 Upstream2 ExposicionMuni2
compress
drop if codmpio==.
drop if MadreSoltera==.
drop if EduMadrePostPrimaria==.
drop if EDAD_MADRE==.
drop if ihw50==. 
drop if ihw==.
drop if region==.

capture drop fixEf2a 
capture drop fixEf2b 
capture drop FE_A 
capture drop FE_B 
capture drop FE_AA
capture drop bogota



gen fixEf2a=string(ihw)+string(codmpio) if codmpio!=5001
replace fixEf2a=string(codmpio) if codmpio==5001
encode fixEf2a, gen(FE_AA)

gen fixEf2b=string(ihw)+string(codmpio) if codmpio!=5001
replace fixEf2b=string(codmpio) if codmpio==5001
encode fixEf2b, gen(FE_BB)


forvalues i=0/1{

local var_indepList AreaMinadaProp ExposicionMuni Upstream ProduccionPerArea
foreach var_indep of varlist `var_indepList'{
eststo clear
local control i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
local var_try1 APGAR_BAJO LBW
foreach vardep of varlist `var_try1'{
eststo: quietly  areg `vardep' c.`var_indep'##c.ihw `control' region#c.FECHA_NAC if Ilegal==`i', absorb(FE_AA) vce(cluster codmpio)
quietly estadd ysumm, mean
clear results
}

quietly estout using "$latexcodes\RegIndividualHWMeassure_`var_indep'_legalidad`i'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) ///
label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  keep(c.`var_indep'#c.ihw )  ///
varl(c.`var_indep'#c.ihw Interaction)  stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$")) replace
}



local var_indepList AreaMinadaProp ExposicionMuni Upstream ProduccionPerArea
foreach var_indep of varlist `var_indepList'{
eststo clear
local control i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
local var_try1 APGAR_BAJO LBW
foreach vardep of varlist `var_try1'{
eststo: quietly  areg `vardep' c.`var_indep'##c.ihw50 `control' region#c.FECHA_NAC if Ilegal==`i', absorb(FE_BB) vce(cluster codmpio)
quietly estadd ysumm, mean
clear results
}
quietly estout using "$latexcodes\RegIndividualHW50Meassure_`var_indep'_legalidad`i'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) ///
label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  keep(c.`var_indep'#c.ihw50 )  ///
varl(c.`var_indep'#c.ihw50 Interaction)  stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$")) replace


}


local var_indepList AreaMinadaProp ExposicionMuni Upstream ProduccionPerArea
foreach var_indep of varlist `var_indepList'{
eststo clear
local control i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
local var_try1 APGAR_BAJO LBW
foreach vardep of varlist `var_try1'{
eststo: quietly areg `vardep' c.D_`var_indep'##c.ihw `control' region#c.FECHA_NAC if Ilegal==`i', absorb(FE_AA) vce(cluster codmpio)
quietly estadd ysumm, mean
clear results
}
quietly estout using "$latexcodes\RegIndividualHWdummies_`var_indep'_legalidad`i'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) ///
label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  keep(c.D_`var_indep'#c.ihw )  ///
varl(c.D_`var_indep'#c.ihw Interaction)  stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$")) replace


}


local var_indepList AreaMinadaProp ExposicionMuni Upstream ProduccionPerArea
foreach var_indep of varlist `var_indepList'{
eststo clear
local control i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
local var_try1 APGAR_BAJO LBW
foreach vardep of varlist `var_try1'{
quietly eststo: quietly areg `vardep' c.D_`var_indep'##c.ihw50 `control' region#c.FECHA_NAC if Ilegal==`i', absorb(FE_BB) vce(cluster codmpio)
quietly estadd ysumm, mean
clear results
}
quietly estout using "$latexcodes\RegIndividual50HWdummies_`var_indep'_legalidad`i'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) ///
label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  keep(c.D_`var_indep'#c.ihw50 )  ///
varl(c.D_`var_indep'#c.ihw50 Interaction)  stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$")) replace

}

}

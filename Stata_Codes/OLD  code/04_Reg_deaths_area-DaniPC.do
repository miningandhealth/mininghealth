set mem 1000000
set matsize 11000
*use "C:\Users\Santi\Copy\PYP_Birth\CreatedData\panel_defmun.dta", clear
use "panel_defmun.dta", clear
*save "panel_defmun_ind.dta", replace

tab mes, gen(imes)
tab ano, gen(iano)

tab coddane, gen(imuni)
reg  ndeaths  areamin imes* iano* 
outreg using death_area, se var starlevels(10 5 1) keep(  areamin ) note( Coefficients for fixed effects not shown) tex fragment replace  

reg  ndeaths  areamin imes* iano* imuni*
outreg using death_area, se var starlevels(10 5 1) keep(  areamin ) note( Coefficients for fixed effects not shown) tex fragment merge  


reg  ndeaths  areamin imes* iano*  if never!=1
outreg using death_area_min, se var starlevels(10 5 1) keep(  areamin ) note( Coefficients for fixed effects not shown) tex fragment replace 
reg  ndeaths  areamin imes* iano* imuni* if never!=1
outreg using death_area_min, se var starlevels(10 5 1) keep(  areamin ) note( Coefficients for fixed effects not shown) tex fragment merge 


reg  hgrel  areamin imes* iano* 
outreg using hgrel_area, se var starlevels(10 5 1) keep(  areamin ) note( Coefficients for fixed effects not shown) tex fragment replace  

reg  hgrel  areamin imes* iano* imuni*
outreg using hgrel_area, se var starlevels(10 5 1) keep(  areamin ) note( Coefficients for fixed effects not shown) tex fragment merge  


reg  hgrel  areamin imes* iano*  if never!=1
outreg using hgrel_area_min, se var starlevels(10 5 1) keep(  areamin ) note( Coefficients for fixed effects not shown) tex fragment replace 
reg  hgrel  areamin imes* iano* imuni* if never!=1
outreg using hgrel_area_min, se var starlevels(10 5 1) keep(  areamin ) note( Coefficients for fixed effects not shown) tex fragment merge 


reg  mbebe  areamin imes* iano* 
outreg using mbebe_area, se var starlevels(10 5 1) keep(  areamin ) note( Coefficients for fixed effects not shown) tex fragment replace  

reg  mbebe  areamin imes* iano* imuni*
outreg using mbebe_area, se var starlevels(10 5 1) keep(  areamin ) note( Coefficients for fixed effects not shown) tex fragment merge  


reg  mbebe  areamin imes* iano*  if never!=1
outreg using mbebe_area_min, se var starlevels(10 5 1) keep(  areamin ) note( Coefficients for fixed effects not shown) tex fragment replace 
reg  mbebe  areamin imes* iano* imuni* if never!=1
outreg using mbebe_area_min, se var starlevels(10 5 1) keep(  areamin ) note( Coefficients for fixed effects not shown) tex fragment merge 



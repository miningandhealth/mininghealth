/*
cd "E:\Copy\MiningAndBirth\"
set matsize 11000
use "CreatedData/DataCompleta.dta",clear
compress
save "CreatedData/DataCompleta1.dta",replace
*/
cd "E:\Copy\MiningAndBirth\"
*cd "C:\Copy\MiningAndBirth\"
*cd "Z:\MiningAndBirth\"
set matsize 11000
use "CreatedData/DataCompleta1.dta",clear
keep APGAR_BAJO FECHA_NAC AreaMinadaProp   AreaMinadaUpstream   ANO MES AREA_NACI EDAD_MADRE CODIGO_DANE INDEX_MUNMINERO INDEX_MUNUPSTREAM MinDist CODIGO_DANE_M DPTO_R DPTO_NACI
drop if AreaMinadaProp>0


eststo: quietly  xi: regress APGAR_BAJO c.FECHA_NAC##INDEX_MUNMINERO  i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.DPTO_NACI, vce(cluster CODIGO_DANE)
eststo: quietly  xi: regress APGAR_BAJO c.FECHA_NAC##INDEX_MUNMINERO  i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.DPTO_NACI if MinDist<50, vce(cluster CODIGO_DANE)
eststo: quietly  xi: regress APGAR_BAJO c.FECHA_NAC##INDEX_MUNMINERO  i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.DPTO_NACI if MinDist<100, vce(cluster CODIGO_DANE)	
eststo: quietly  xi: regress APGAR_BAJO c.FECHA_NAC##INDEX_MUNMINERO  i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.DPTO_NACI if MinDist<500, vce(cluster CODIGO_DANE)	


esttab using "Results\ResultParallel.tex", se ar2 booktabs label indicate( "Month and Year F.E.= _IANO* _IMES*" "Mother Characteristics= _IAREA_NACI* EDAD_MADRE"  "State F.E.=  _IDPTO_NACI*" ) replace b(a2) se(a2) nocon nomtitles  

use "CreatedData/DataCompleta1.dta",clear
eststo clear
keep APGAR_BAJO FECHA_NAC AreaMinadaProp   AreaMinadaUpstream  ANO MES AREA_NACI EDAD_MADRE CODIGO_DANE INDEX_MUNUPSTREAM MinDist CODIGO_DANE_M DPTO_R DPTO_NACI
drop if AreaMinadaUpstream>0


eststo: quietly  xi: regress APGAR_BAJO c.FECHA_NAC##INDEX_MUNUPSTREAM  i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.DPTO_NACI, vce(cluster CODIGO_DANE)
eststo: quietly  xi: regress APGAR_BAJO c.FECHA_NAC##INDEX_MUNUPSTREAM  i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.DPTO_NACI if MinDist<50, vce(cluster CODIGO_DANE)
eststo: quietly  xi: regress APGAR_BAJO c.FECHA_NAC##INDEX_MUNUPSTREAM  i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.DPTO_NACI if MinDist<100, vce(cluster CODIGO_DANE)	
eststo: quietly  xi: regress APGAR_BAJO c.FECHA_NAC##INDEX_MUNUPSTREAM  i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.DPTO_NACI if MinDist<500, vce(cluster CODIGO_DANE)	


esttab using "Results\ResultParallel2.tex", se ar2 booktabs label indicate( "Month and Year F.E.= _IANO* _IMES*" "Mother Characteristics= _IAREA_NACI* EDAD_MADRE" "State F.E.=  _IDPTO_NACI*" ) replace b(a2) se(a2) nocon nomtitles 

esttab est1 est2 est3 est4 using "Results\ResultParallel.tex", se ar2 booktabs label indicate( "Month and Year F.E.= _IANO* _IMES*" "Mother Characteristics= _IAREA_NACI* EDAD_MADRE"  "State F.E.=  _IDPTO_NACI*" ) replace b(a2) se(a2) nocon nomtitles  
esttab est5 est6 est7 est8 using "Results\ResultParallel2.tex", se ar2 booktabs label indicate( "Month and Year F.E.= _IANO* _IMES*" "Mother Characteristics= _IAREA_NACI* EDAD_MADRE" "State F.E.=  _IDPTO_NACI*" ) replace b(a2) se(a2) nocon nomtitles 


cd "E:\Copy\MiningAndBirth\"
*cd "C:\Copy\MiningAndBirth\"
*cd "Z:\MiningAndBirth\"
set matsize 11000
use "CreatedData/DataCompleta1.dta",clear
eststo clear
keep APGAR_BAJO AreaMinadaProp AreaMinadaProp2  AreaMinadaUpstream  AreaMinadaProp_M  AreaMinadaProp_M2 AreaMinadaUpstream_M ANO MES AREA_NACI EDAD_MADRE CODIGO_DANE MinDist CODIGO_DANE_M DPTO_R DPTO_NACI
gen AreaMinadaUpstream2=AreaMinadaUpstream*AreaMinadaUpstream
****Regresiones Basicas
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstream AreaMinadaUpstream2 i.ANO i.MES , vce(cluster CODIGO_DANE)
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstream AreaMinadaUpstream2 i.ANO i.MES  i.AREA_NACI EDAD_MADRE , vce(cluster CODIGO_DANE)
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstream AreaMinadaUpstream2 i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.DPTO_NACI, vce(cluster CODIGO_DANE)
	
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstream AreaMinadaUpstream2 i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.DPTO_NACI if MinDist<50, vce(cluster CODIGO_DANE)
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstream AreaMinadaUpstream2 i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.DPTO_NACI if MinDist<100, vce(cluster CODIGO_DANE)	
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstream AreaMinadaUpstream2 i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.DPTO_NACI if MinDist<500, vce(cluster CODIGO_DANE)	

	


esttab using "Results\Result_InesityStat.tex", se ar2 booktabs label indicate( "Month and Year F.E.= _IANO* _IMES*" "Mother Characteristics= _IAREA_NACI* EDAD_MADRE"  "State F.E.=  _IDPTO_NACI*" ) replace b(a2) se(a2) nocon nomtitles  keep(AreaMinadaProp AreaMinadaProp2  AreaMinadaUpstream AreaMinadaUpstream2 )



****Regresiones Basicas Madre
eststo clear
gen AreaMinadaUpstream_M2=AreaMinadaUpstream_M*AreaMinadaUpstream_M
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp_M AreaMinadaProp_M2 AreaMinadaUpstream_M AreaMinadaUpstream_M2  i.ANO i.MES , vce(cluster CODIGO_DANE_M)
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp_M AreaMinadaProp_M2 AreaMinadaUpstream_M AreaMinadaUpstream_M2  i.ANO i.MES  i.AREA_NACI EDAD_MADRE , vce(cluster CODIGO_DANE_M)
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp_M AreaMinadaProp_M2 AreaMinadaUpstream_M AreaMinadaUpstream_M2  i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.DPTO_R, vce(cluster CODIGO_DANE_M)
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp_M AreaMinadaProp_M2 AreaMinadaUpstream_M AreaMinadaUpstream_M2  i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.DPTO_R if MinDist<50, vce(cluster CODIGO_DANE_M)
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp_M AreaMinadaProp_M2 AreaMinadaUpstream_M AreaMinadaUpstream_M2  i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.DPTO_R if MinDist<100, vce(cluster CODIGO_DANE_M)	
eststo: quietly  xi: regress APGAR_BAJO AreaMinadaProp_M AreaMinadaProp_M2 AreaMinadaUpstream_M AreaMinadaUpstream_M2  i.ANO i.MES  i.AREA_NACI EDAD_MADRE i.DPTO_R if MinDist<500, vce(cluster CODIGO_DANE_M)	


esttab using "Results\Result_InesityStat_M.tex", se ar2 booktabs label indicate( "Month and Year F.E.= _IANO* _IMES*" "Mother Characteristics= _IAREA_NACI* EDAD_MADRE"  "State F.E.=  _IDPTO_R*" ) replace b(a2) se(a2) nocon nomtitles  keep(AreaMinadaProp_M AreaMinadaProp_M2  AreaMinadaUpstream_M AreaMinadaUpstream_M2)


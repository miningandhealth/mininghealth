use "$base_out\DataCompleta_Stata.dta", clear
set matsize 100

gen YrMonth=ym(ano,MES)
gen YrQrt=yq(ano,Quarter)
gen region=gandina+2*gcaribe+3*gpacifica+4*gorinoquia+5*gamazonia
keep ano MES region D_AreaMinadaProp D_ProduccionPerArea D_ExposicionMuni D_Upstream AreaMinadaProp ProduccionPerArea ExposicionMuni Upstream AreaMinadaProp2 ProduccionPerArea2 ExposicionMuni2 Upstream2 Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE APGAR_BAJO LBW codmpio FECHA_NAC


quietly areg APGAR_BAJO i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE region#c.FECHA_NAC, absorb(codmpio) vce(cluster codmpio)
predict resid_APGAR,resid 
quietly areg AreaMinadaProp i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE region#c.FECHA_NAC, absorb(codmpio) vce(cluster codmpio)
predict resid_AreaMinadaProp, resid 
quietly areg ProduccionPerArea i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE region#c.FECHA_NAC, absorb(codmpio) vce(cluster codmpio)
predict resid_ProduccionPerArea,resid 
quietly areg ExposicionMuni i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE region#c.FECHA_NAC, absorb(codmpio) vce(cluster codmpio)
predict resid_ExposicionMuni, resid 
quietly areg Upstream i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE region#c.FECHA_NAC, absorb(codmpio) vce(cluster codmpio)
predict resid_Upstream,resid

lpoly resid_APGAR resid_AreaMinadaProp, ci noscatter addplot((histogram resid_AreaMinadaProp, yaxis(2) fraction))
graph export "$graphs\resid_apgar_areamin.pdf", replace
graph export "$graphs\resid_apgar_areamin.png", replace
lpoly APGAR AreaMinadaProp, ci msize(zero) noscatter addplot((histogram AreaMinadaProp, yaxis(2) fraction))
graph export "$graphs\raw_apgar_areamin.pdf", replace
graph export "$graphs\raw_apgar_areamin.png", replace
lpoly resid_APGAR resid_ProduccionPerArea, ci  noscatter addplot((histogram resid_ProduccionPerArea, yaxis(2) fraction))
graph export "$graphs\resid_apgar_prod.pdf", replace
graph export "$graphs\resid_apgar_prod.png", replace
lpoly resid_APGAR resid_ExposicionMuni, ci  noscatter addplot((histogram resid_ExposicionMuni, yaxis(2) fraction))
graph export "$graphs\resid_apgar_exp.pdf", replace
graph export "$graphs\resid_apgar_exp.png", replace
lpoly resid_APGAR resid_Upstream, ci  noscatter addplot((histogram resid_Upstream, yaxis(2) fraction))
graph export "$graphs\resid_apgar_upst.pdf", replace
graph export "$graphs\resid_apgar_upst.png", replace



use "C:\Users\santi\Copy\PYP_Birth\RawData\PANELES_CEDE\CARACTERÍSTICAS GENERALES\PANEL CARAC. GENERALES.dta", clear
collapse pobl_tot altura, by(codmpio)
drop if pobl_tot>30000
drop if altura>1000
save "C:\Users\santi\Copy\PYP_Birth\CreatedData\Temporary\muni_pop_less_30k_altura_less_1000.dta", replace 

use "C:\Users\santi\Copy\PYP_Birth\CreatedData\Temporary\Muni_close_mines_sea.dta", clear
rename codane2 codmpio
keep codmpio
gen sea=1
merge 1:1 codmpio using "C:\Users\santi\Copy\PYP_Birth\CreatedData\Temporary\muni_pop_less_30k_altura_less_1000.dta"
keep if _merge==2

drop _merge
merge 1:1 codmpio using "C:\Users\santi\Copy\PYP_Birth\CreatedData\Temporary\muni_close25k_mines.dta"
keep if _merge==3
keep codmpio
save "C:\Users\santi\Copy\PYP_Birth\CreatedData\Muni_peque_mine_fish.dta"

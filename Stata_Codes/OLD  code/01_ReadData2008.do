set mem 700000
use "C:\Users\Santi\Copy\PYP_Birth\RawData\def2008.dta", clear
keep  cod_dpto cod_munic a_defun cod_insp tipo_defun  sexo edad codptore codmunre area_res codigo veredafall man_muer c_dir1
gen death_A=regexm( c_dir1,"^A")
collapse (count)  sexo (sum)  death_A, by( cod_dpto cod_munic)
rename sexo deaths

use "$base_out\PanelMunicipalEEVV_Stata.dta", clear
merge 1:1 ano Quarter codmpio using "$base_out\NonMissingsCount.dta"
drop _merge
gen region=gandina+2*gcaribe+3*gpacifica+4*gorinoquia+5*gamazonia

gen fertilidad=Nacimientos/PoblacionMuni
gen TasaMad14=MadreMenor14/PoblacionMuni
gen TasaMad14_17=Madre14_17/PoblacionMuni
rename MASC_Count fertilidad_Count 
rename MadreMenor14_Count TasaMad14_Count
rename Madre14_17_Count TasaMad14_17_Count
 

drop if ano==2014
drop if Quarter==.
tsset codmpio YrQrt, quarterly



local var_indepList AreaMinadaProp ProduccionPerArea
foreach var_indep of varlist `var_indepList'{
eststo clear
local var_try1 APGAR_BAJO LBW
foreach vardep of varlist `var_try1'{
eststo: quietly xi: areg `vardep' `var_indep' `var_indep'2 i.ano i.Quarter region#c.YrQrt [aw=`vardep'_Count], absorb( codmpio) vce(cluster codmpio) 
estadd ysumm
}

estout using "$latexcodes\PanelRegMeasure_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(`var_indep' `var_indep'2 )  stats(N ymean, fmt(a2 a2) labels ("N. of obs." "Mean of Dep. Var.")) replace

eststo clear
local var_try1 APGAR_BAJO LBW
foreach vardep of varlist `var_try1'{
eststo: quietly xi: areg `vardep' D_`var_indep'  i.ano i.Quarter region#c.YrQrt [aw=`vardep'_Count], absorb( codmpio) vce(cluster codmpio) 
estadd ysumm

}
estout using "$latexcodes\PanelRegDummies_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_`var_indep')  stats(N ymean, fmt(a2 a2) labels ("N. of obs." "Mean of Dep. Var.")) replace


}



eststo clear
local var_try1 APGAR_BAJO LBW
foreach vardep of varlist `var_try1'{
eststo: quietly xi: areg `vardep' ExposicionMuni ExposicionMuni2 Upstream Upstream2 i.ano i.Quarter region#c.YrQrt [aw=`vardep'_Count], absorb( codmpio) vce(cluster codmpio) 
estadd ysumm
}

estout using "$latexcodes\PanelRegMeasure_Combo.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(ExposicionMuni ExposicionMuni2 Upstream Upstream2 )  stats(N ymean, fmt(a2 a2) labels ("N. of obs." "Mean of Dep. Var.")) replace

eststo clear
local var_try1 APGAR_BAJO LBW
foreach vardep of varlist `var_try1'{
eststo: quietly xi: areg `vardep' D_ExposicionMuni D_Upstream  i.ano i.Quarter region#c.YrQrt [aw=`vardep'_Count], absorb( codmpio) vce(cluster codmpio) 
estadd ysumm

}
estout using "$latexcodes\PanelRegDummies_Combo.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_ExposicionMuni D_Upstream)  stats(N ymean, fmt(a2 a2) labels ("N. of obs." "Mean of Dep. Var.")) replace


eststo clear
estpost tabstat AreaMinadaProp ExposicionMuni Upstream ProduccionPerArea, statistics(mean median sd max min p95 count) columns(statistics)
esttab using "$latexcodes\summaryIndepTodos_TR.tex", cells("mean(fmt(a2) label(Mean)) p50(fmt(a2) label(Median)) sd(fmt(a2) label(Std. Dev.)) min(fmt(a2) label(Min)) max(fmt(a2) label(Max)) p95(fmt(a2) label(Perc. 95)) count(fmt(a2) label(N))") nomtitle nonumber label  replace noobs booktabs


eststo clear
replace AreaMinadaProp=. if AreaMinadaProp==0
replace ExposicionMuni=. if ExposicionMuni==0
replace ProduccionPerCapita=. if ProduccionPerCapita==0
replace Upstream=. if Upstream==0
replace ProduccionPerArea=. if ProduccionPerArea==0
estpost tabstat AreaMinadaProp ExposicionMuni Upstream ProduccionPerArea, statistics(mean median sd max min p95 count) columns(statistics)
esttab using "$latexcodes\summaryIndepPositivos_TR.tex", cells("mean(fmt(a2) label(Mean)) p50(fmt(a2) label(Median)) sd(fmt(a2) label(Std. Dev.)) min(fmt(a2) label(Min)) max(fmt(a2) label(Max)) p95(fmt(a2) label(Perc. 95)) count(fmt(a2) label(N))") nomtitle nonumber label  replace noobs booktabs

clear all

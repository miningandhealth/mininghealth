*cd "E:\Copy\MiningAndBirth\"
*cd "C:\Copy\MiningAndBirth\"
*cd "Z:\MiningAndBirth\"
clear
clear matrix
cd "C:\Users\Santi\Copy\PYP_Birth\"
set matsize 5000
set mem 200000

use "CreatedData/DataMuni.dta",clear
compress
rename Group_1 ANO
rename Group_2 MES
rename Group_3 CODIGO_DANE

gen modate = ym(ANO, MES) 
tsset CODIGO_DANE modate, monthly

eststo clear
eststo est1: quietly  reg APGAR_BAJO AreaMinadaProp AreaMinadaProp2 i.CODIGO_DANE i.ANO i.MES, vce(cluster CODIGO_DANE)
eststo est2: quietly  reg APGAR_BAJO AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstream i.CODIGO_DANE i.ANO i.MES, vce(cluster CODIGO_DANE)
esttab est1 est2 using "Results\ResultSlide1.tex", se star(* 0.1 ** 0.05 *** 0.01) r2 booktabs label indicate(  "Month F.E.=  *MES*" "Year F.E.=  *ANO*" "Municipality F.E.=  *CODIGO_DA*" ) replace b(a2) se(a2) nocon nomtitles  

eststo est10: quietly  reg APGAR_BAJO AreaMinadaProp AreaMinadaProp2 i.CODIGO_DANE i.ANO i.MES if ANO==2004 | ANO==2005 | ANO==2006, vce(cluster CODIGO_DANE)
eststo est11: quietly  reg APGAR_BAJO AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstream i.CODIGO_DANE i.ANO i.MES if ANO==2004 | ANO==2005 | ANO==2006, vce(cluster CODIGO_DANE)

eststo est3: quietly  reg APGAR_BAJO AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstream i.CODIGO_DANE , vce(cluster CODIGO_DANE)
eststo est4: quietly  reg APGAR_BAJO AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstream i.CODIGO_DANE i.ANO , vce(cluster CODIGO_DANE)
eststo est5: quietly  reg APGAR_BAJO AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstream i.CODIGO_DANE i.ANO i.MES, vce(cluster CODIGO_DANE)
esttab est3 est4 est5 using "Results\ResultSlide2FE.tex", se star(* 0.1 ** 0.05 *** 0.01) r2 booktabs label indicate(  "Month F.E.=  *MES*" "Year F.E.=  *ANO*" "Municipality F.E.=  *CODIGO_DA*" ) replace b(a2) se(a2) nocon nomtitles  


use "CreatedData/DataMuni_M.dta",clear
compress
rename Group_1 ANO
rename Group_2 MES
rename Group_3 CODIGO_DANE
destring CODIGO_DANE,replace
gen modate = ym(ANO, MES) 
tsset CODIGO_DANE modate, monthly
fvset base last CODIGO_DANE
eststo est6 : quietly  reg APGAR_BAJO AreaMinadaProp_M AreaMinadaProp_M2 i.CODIGO_DANE i.ANO i.MES, vce(cluster CODIGO_DANE)
eststo est7: quietly  reg APGAR_BAJO AreaMinadaProp_M AreaMinadaProp_M2 AreaMinadaUpstream_M i.CODIGO_DANE i.ANO i.MES, vce(cluster CODIGO_DANE)
esttab est6 est7 using "Results\ResultSlide3Migration.tex", se r2 booktabs label indicate(  "Month F.E.=  *MES*" "Year F.E.=  *ANO*" "Municipality F.E.=  *CODIGO_DA*" ) replace b(a2) se(a2) nocon nomtitles  rename(AreaMinadaProp_M AreaMinadaProp AreaMinadaProp_M2 AreaMinadaProp2 AreaMinadaUpstream_M AreaMinadaUpstream)


use "CreatedData/DataMuni_Falsi.dta",clear
compress
rename Group_1 ANO
rename Group_2 MES
rename Group_3 CODIGO_DANE

gen modate = ym(ANO, MES) 
tsset CODIGO_DANE modate, monthly
eststo est8: quietly  reg APGAR_BAJO AreaMinadaProp AreaMinadaProp2 i.CODIGO_DANE i.ANO i.MES, vce(cluster CODIGO_DANE)
eststo est9: quietly  reg APGAR_BAJO AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstream i.CODIGO_DANE i.ANO i.MES, vce(cluster CODIGO_DANE)
esttab est8 est9 using "Results\ResultSlide4Falsi1.tex", se star(* 0.1 ** 0.05 *** 0.01) r2 booktabs label indicate(  "Month F.E.=  *MES*" "Year F.E.=  *ANO*" "Municipality F.E.=  *CODIGO_DA*" ) replace b(a2) se(a2) nocon nomtitles  
esttab est10 est11 using "Results\ResultSlide4Falsi2.tex", se star(* 0.1 ** 0.05 *** 0.01) r2 booktabs label indicate(  "Month F.E.=  *MES*" "Year F.E.=  *ANO*" "Municipality F.E.=  *CODIGO_DA*" ) replace b(a2) se(a2) nocon nomtitles  



import delim using "$mipath\CreatedData\GIS\Temporary\gold_mines.csv", clear
keep gold_id fecha_insc
gen year=substr(fecha_insc,-4,4)
destring year, replace force
save "$mipath\CreatedData\Temporary\gold_close.dta", replace

insheet using "$mipath\CreatedData\GIS\dist_muni_closest_mine.txt", clear
rename in_fid id
rename near_fid gold_id
label var near_dist "Distance muni to closest mine in ArcSeg"
drop rowid_ objectid
save "$mipath\CreatedData\Temporary\dist_muni_closest_mine.dta", replace

import delim using "$mipath\CreatedData\GIS\Temporary\Municipios.csv", clear
keep codane2 id
merge 1:1 id using "$mipath\CreatedData\Temporary\dist_muni_closest_mine.dta"
drop _merge id
rename codane2 codmpio
drop if gold_id==.
merge m:1 gold_id using "$mipath\CreatedData\Temporary\gold_close.dta"
drop _merge
drop if codmpio==.
save "$mipath\CreatedData\muni_date_active.dta", replace






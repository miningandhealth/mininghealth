use "$base_out\DataCompleta_Stata.dta", clear
*merge m:1 codmpio using "C:\Users\santi\Copy\PYP_Birth\CreatedData\Muni_peque_mine_fish.dta"
*keep if _merge==3
set matsize 100
drop if wgesthw==.
gen YrMonth=ym(ano,MES)
gen YrQrt=yq(ano,Quarter)
gen region=gandina+2*gcaribe+3*gpacifica+4*gorinoquia+5*gamazonia
keep wgesthw ihw  ano MES region D_AreaMinadaProp D_ProduccionPerArea D_ExposicionMuni D_Upstream AreaMinadaProp ProduccionPerArea ExposicionMuni Upstream AreaMinadaProp2 ProduccionPerArea2 ExposicionMuni2 Upstream2 Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE APGAR_BAJO LBW codmpio FECHA_NAC

gen fixEf2a=string(ihw)+string(codmpio) if codmpio!=5001
replace fixEf2a=string(codmpio) if codmpio==5001
encode fixEf2a, gen(FE_AA)
/*
gen fixEf2b=string(ihw50)+string(codmpio) if codmpio!=5001
replace fixEf2b=string(codmpio) if codmpio==5001
encode fixEf2b, gen(FE_BB)
*/

gen ihwb=0 & wgesthw!=.
*gen ihwb=0 if ihw!=.
replace ihwb=1 if ihw==1 & wgesthw<=20
label var ihwb "Holyweek first 20 weeks of gestation"

gen ihwe=0 & wgesthw!=.
*gen ihwe=0 if ihw!=.
replace ihwe=1 if ihw==1 & ihwb==0
label var ihwe "Holyweek last weeks of gestation"

/*
gen D_AreaMinadaPropxihw=D_AreaMinadaProp*ihw
label var D_AreaMinadaPropxihw`x' "Holy Week x Active"

foreach x in b e {
gen D_AreaMinadaPropxihw`x'=D_AreaMinadaProp*ihw`x'
label var D_AreaMinadaPropxihw`x' "Holy Week x Active"

}
*/


local var_indepList AreaMinadaProp ProduccionPerArea
foreach var_indep of varlist `var_indepList'{
eststo clear
local control i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE

foreach varhw of varlist ihw ihwb ihwe{
eststo: quietly xi: areg APGAR_BAJO c.`var_indep'##c.`varhw' `control' region#c.FECHA_NAC if wgesthw!=., absorb(FE_AA) vce(cluster codmpio)
estadd ysumm
}
estout using "$latexcodes\RegIndividualHWMeassure_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) ///
mlabels(none) collabels(none)  ///
rename(ihwb ihw ihwe ihw c.`var_indep'#c.ihwb c.`var_indep'#c.ihw c.`var_indep'#c.ihwe c.`var_indep'#c.ihw) ///
stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$")) replace ///
keep(`var_indep' ihw c.`var_indep'#c.ihw ) varl(ihw "Holy Week"  c.`var_indep'#c.ihw Interaction)
}



local var_indepList AreaMinadaProp ProduccionPerArea
foreach var_indep of varlist `var_indepList'{
eststo clear
local control i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
foreach varhw of varlist ihw ihwb ihwe{
eststo: quietly xi: areg APGAR_BAJO c.D_`var_indep'##c.`varhw' `control' region#c.FECHA_NAC if wgesthw!=., absorb( FE_AA) vce(cluster codmpio)
estadd ysumm
}
estout using "$latexcodes\IndividualHWDummies_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
rename(ihwb ihw ihwe ihw c.D_`var_indep'#c.ihwb c.D_`var_indep'#c.ihw c.D_`var_indep'#c.ihwe c.D_`var_indep'#c.ihw) ///
stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$")) replace ///
keep(D_`var_indep' ihw c.D_`var_indep'#c.ihw ) varl(ihw "Holy Week"  c.D_`var_indep'#c.ihw Interaction)
}


eststo clear
local control i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
foreach varhw of varlist ihw ihwb ihwe{
eststo: quietly xi: areg APGAR_BAJO c.ExposicionMuni##c.`varhw' c.Upstream##c.`varhw'  `control' region#c.FECHA_NAC if wgesthw!=., absorb( FE_AA) vce(cluster codmpio) 
estadd ysumm
}
estout using "$latexcodes\IndividualHWMeasure_Combo_peque.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$")) replace ///
rename(ihwb ihw ihwe ihw c.ExposicionMuni#c.ihwb c.ExposicionMuni#c.ihw c.ExposicionMuni#c.ihwe c.ExposicionMuni#c.ihw c.Upstream#c.ihwb c.Upstream#c.ihw c.Upstream#c.ihwe c.Upstream#c.ihw) ///
keep(ExposicionMuni Upstream ihw c.ExposicionMuni#c.ihw c.Upstream#c.ihw ) varl(ihw "Holy Week" c.ExposicionMuni#c.ihw "Proximity exposure \$\times\$ Holy Week" c.Upstream#c.ihw "River exposure \$\times\$ Holy Week")


eststo clear
local control i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
foreach varhw of varlist ihw ihwb ihwe{
eststo: quietly xi: areg APGAR_BAJO c.D_ExposicionMuni##c.`varhw'  c.D_Upstream##c.`varhw'   `control' region#c.FECHA_NAC if wgesthw!=., absorb( FE_AA) vce(cluster codmpio) 
estadd ysumm
}
estout using "$latexcodes\IndividualHWDummies_Combo.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$")) replace ///
rename(ihwb ihw ihwe ihw c.D_ExposicionMuni#c.ihwb c.D_ExposicionMuni#c.ihw c.D_ExposicionMuni#c.ihwe c.D_ExposicionMuni#c.ihw c.D_Upstream#c.ihwb c.D_Upstream#c.ihw c.D_Upstream#c.ihwe c.D_Upstream#c.ihw) ///
keep(D_ExposicionMuni D_Upstream ihw c.D_ExposicionMuni#c.ihw c.D_Upstream#c.ihw ) varl(ihw "Holy Week" c.D_ExposicionMuni#c.ihw " [Proximity exposure \$>0\$] \$\times\$ Holy Week" c.D_Upstream#c.ihw "[River exposure \$>0\$] \$\times\$ Holy Week")


/*
local var_indepList AreaMinadaProp ProduccionPerArea
foreach var_indep of varlist `var_indepList'{
eststo clear
local control i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
foreach varhw of varlist ihw ihwb ihwe{
eststo: quietly xi: areg APGAR_BAJO D_`var_indep'##c.ihw50 `control' region#c.FECHA_NAC, absorb( FE_BB) vce(cluster codmpio)
estadd ysumm
}
estout using "$latexcodes\IndividualHW50Dummies_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_`var_indep')  stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$")) replace

}

eststo clear
local control i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
foreach varhw of varlist ihw ihwb ihwe{
eststo: quietly xi: areg APGAR_BAJO ExposicionMuni##c.ihw50 Upstream##c.ihw50  `control' region#c.FECHA_NAC, absorb( FE_BB) vce(cluster codmpio) 
estadd ysumm
}

estout using "$latexcodes\IndividualHW50Measure_Combo.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(ExposicionMuni ExposicionMuni2 Upstream Upstream2 )  stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$")) replace


local var_indepList AreaMinadaProp ProduccionPerArea
foreach var_indep of varlist `var_indepList'{
eststo clear
local control i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
foreach varhw of varlist ihw ihwb ihwe{
eststo: quietly xi: areg `vardep' `var_indep'##c.ihw50 `control' region#c.FECHA_NAC, absorb(FE_BB) vce(cluster codmpio)
estadd ysumm
}
estout using "$latexcodes\RegIndividualHW50Meassure_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(`var_indep' `var_indep'2 )  stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$")) replace
}


eststo clear
local control i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
foreach varhw of varlist ihw ihwb ihwe{
eststo: quietly xi: areg APGAR_BAJO D_ExposicionMuni##c.ihw50  D_Upstream##c.ihw50   `control' region#c.FECHA_NAC, absorb( FE_BB) vce(cluster codmpio) 
estadd ysumm

}
estout using "$latexcodes\IndividualHW50Dummies_Combo.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_ExposicionMuni D_Upstream)  stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$")) replace

*/

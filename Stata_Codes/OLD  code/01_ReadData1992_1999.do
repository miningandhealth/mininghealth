set mem 700000
forval i=2/9 {
use C:\Users\Santi\Copy\PYP_Birth\RawData\def199`i'.dta, clear
if `i'<7{
rename zon_defun a_defun
rename coddpto_r codptore
rename codmuni_r codmunre
rename zon_res area_res
replace area_res=99 if area_res==3
rename caus9999 c_dir1
keep  cod_dpto cod_munic a_defun sexo edad codptore codmunre area_res c_dir1
collapse (count)  sexo , by( cod_dpto cod_munic)
}

else if `i'<=8{
rename dpto_d cod_dpto
rename munic_d cod_munic
if `i'==7{
rename zon_d a_defun
rename zon_res area_res
}
else {
rename area_d a_defun
rename area_r area_res
}
replace a_defun=99 if a_defun==3
rename dpto_r codptore
rename munic_r codmunre
rename c_basica c_dir1
keep  cod_dpto cod_munic a_defun sexo edad codptore codmunre area_res c_dir1
}
else{
keep  cod_dpto cod_munic a_defun cod_insp tipo_defun  sexo edad codptore codmunre area_res codigo man_muer c_dir1
gen death_A=regexm( c_dir1,"^A")
collapse (count)  sexo (sum)  death_A, by( cod_dpto cod_munic)
}

rename sexo deaths
save C:\Users\Santi\Copy\PYP_Birth\CreatedData\defbymun199`i'.dta, replace
}

use "$base_out\DataCompleta_Stata.dta", clear
set matsize 100

gen YrMonth=ym(ano,MES)
gen YrQrt=yq(ano,Quarter)
gen region=gandina+2*gcaribe+3*gpacifica+4*gorinoquia+5*gamazonia

drop if AreaMinadaProp>0.4
replace APGAR_BAJO=100*APGAR_BAJO
*Sumstat Babies
drop if APGAR_BAJO==.
drop if PESO_NAC==.
drop if TALLA_NAC==. 
drop if EDAD_MADRE==.
drop if MadreSoltera==.
drop if EduMadrePostPrimaria==.
label var APGAR_BAJO "Low APGAR"
label var PESO_NAC "Weight (gr)"
label var TALLA_NAC "Height (cm)"
label var EDAD_MADRE "Mother's age"
label var MadreSoltera "Single mother"
label var EduMadrePostPrimaria "Mother has post-primary education"
eststo clear
estpost tabstat APGAR_BAJO PESO_NAC TALLA_NAC EDAD_MADRE MadreSoltera EduMadrePostPrimaria, statistics(mean median sd max min count) columns(statistics)
esttab using "$latexcodes\SummaryBirth.tex", cells("mean(fmt(a2) label(Mean)) p50(fmt(a2) label(Median)) sd(fmt(a2) label(Std. Dev.)) min(fmt(a2) label(Min)) max(fmt(a2) label(Max)) count(fmt(a2) label(N))") nomtitle nonumber label  replace noobs booktabs

keep PriceRealMA Arena_AreaMinadaProp2 ProduccionAccPerArea2 D_Arena_ExposicionMuni D_Arena_Upstream Arena_ExposicionMuni Arena_Upstream D_Arena_AreaMinadaProp Arena_AreaMinadaProp NearGoldMine NearArenaMine ProduccionAccPerArea D_ProduccionAccPerArea ano MES region D_AreaMinadaProp D_ProduccionPerArea D_ExposicionMuni D_Upstream AreaMinadaProp ProduccionPerArea ExposicionMuni Upstream AreaMinadaProp2 ProduccionPerArea2 ExposicionMuni2 Upstream2 Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE APGAR_BAJO LBW codmpio FECHA_NAC Z_TALLA_NAC
gen PriceRealMA2=PriceRealMA*PriceRealMA


local var_indepList AreaMinadaProp ProduccionPerArea ProduccionAccPerArea /* Arena_AreaMinadaProp */
foreach var_indep of varlist `var_indepList'{
capture drop near
gen near=NearGoldMine
if("`var_indep'"=="Arena_AreaMinadaProp") replace near=NearArenaMine
eststo clear
local control i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE

eststo: quietly xi: areg APGAR_BAJO c.`var_indep'##c.PriceRealMA `control' region#c.FECHA_NAC if near==1 & region<=3, absorb(codmpio) vce(cluster codmpio)
estadd ysumm
eststo: quietly xi: areg APGAR_BAJO c.`var_indep'#c.PriceRealMA `control' region#c.FECHA_NAC if near==1 & region<=3, absorb(codmpio) vce(cluster codmpio)
estadd ysumm
eststo: quietly xi: areg LBW c.`var_indep'#c.PriceRealMA `control' region#c.FECHA_NAC if near==1 & region<=3, absorb(codmpio) vce(cluster codmpio)
estadd ysumm
eststo: quietly xi: areg Z_TALLA_NAC c.`var_indep'#c.PriceRealMA `control' region#c.FECHA_NAC if near==1 & region<=3, absorb(codmpio) vce(cluster codmpio)
estadd ysumm

estout using "$latexcodes\IndividualMeasure_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(`var_indep' PriceRealMA c.`var_indep'#c.PriceRealMA)  stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$")) replace
 
}

local var_indepList ProduccionAccPerArea AreaMinadaProp ProduccionPerArea /* Arena_AreaMinadaProp */
foreach var_indep of varlist `var_indepList'{
capture drop near
gen near=NearGoldMine
if("`var_indep'"=="Arena_AreaMinadaProp") replace near=NearArenaMine
eststo clear
local control i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE

eststo: quietly xi: areg APGAR_BAJO c.`var_indep'##c.PriceRealMA c.`var_indep'2##c.PriceRealMA2 `control' region#c.FECHA_NAC if near==1 & region<=3, absorb(codmpio) vce(cluster codmpio)
estadd ysumm
eststo: quietly xi: areg APGAR_BAJO c.`var_indep'#c.PriceRealMA c.`var_indep'2#c.PriceRealMA2 `control' region#c.FECHA_NAC if near==1 & region<=3, absorb(codmpio) vce(cluster codmpio)
estadd ysumm
eststo: quietly xi: areg LBW c.`var_indep'#c.PriceRealMA c.`var_indep'2#c.PriceRealMA2 `control' region#c.FECHA_NAC if near==1 & region<=3, absorb(codmpio) vce(cluster codmpio)
estadd ysumm
eststo: quietly xi: areg Z_TALLA_NAC c.`var_indep'#c.PriceRealMA c.`var_indep'2#c.PriceRealMA2 `control' region#c.FECHA_NAC if near==1 & region<=3, absorb(codmpio) vce(cluster codmpio)
estadd ysumm

estout using "$latexcodes\IndividualMeasure_`var_indep'2.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(`var_indep' PriceRealMA PriceRealMA2 c.`var_indep'#c.PriceRealMA `var_indep'2 c.`var_indep'2#c.PriceRealMA2)  stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$")) replace
 

}

local var_indepList AreaMinadaProp ProduccionPerArea ProduccionAccPerArea /*Arena_AreaMinadaProp */
foreach var_indep of varlist `var_indepList'{
capture drop near
gen near=NearGoldMine
if("`var_indep'"=="Arena_AreaMinadaProp") replace near=NearArenaMine
eststo clear
local control i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
local var_try1 APGAR_BAJO LBW Z_TALLA_NAC

eststo:  xi: areg APGAR_BAJO c.D_`var_indep'##c.PriceRealMA `control' region#c.FECHA_NAC if near==1 & region<=3, absorb( codmpio) vce(cluster codmpio)
estadd ysumm
eststo:  xi: areg APGAR_BAJO c.D_`var_indep'#c.PriceRealMA `control' region#c.FECHA_NAC if near==1 & region<=3, absorb( codmpio) vce(cluster codmpio)
estadd ysumm
eststo:  xi: areg LBW c.D_`var_indep'#c.PriceRealMA `control' region#c.FECHA_NAC if near==1 & region<=3, absorb( codmpio) vce(cluster codmpio)
estadd ysumm
eststo:  xi: areg Z_TALLA_NAC c.D_`var_indep'#c.PriceRealMA `control' region#c.FECHA_NAC if near==1 & region<=3, absorb( codmpio) vce(cluster codmpio)
estadd ysumm
estout using "$latexcodes\IndividualDummies_`var_indep'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_`var_indep' PriceRealMA c.D_`var_indep'#c.PriceRealMA)  stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$")) replace

}


eststo clear
local control i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
eststo: quietly xi: areg APGAR_BAJO c.ExposicionMuni##c.PriceRealMA c.Upstream##c.PriceRealMA `control' region#c.FECHA_NAC if NearGoldMine==1 & region<=3, absorb( codmpio) vce(cluster codmpio) 
estadd ysumm
eststo: quietly xi: areg APGAR_BAJO c.ExposicionMuni#c.PriceRealMA c.Upstream#c.PriceRealMA `control' region#c.FECHA_NAC if NearGoldMine==1 & region<=3, absorb( codmpio) vce(cluster codmpio) 
estadd ysumm
eststo: quietly xi: areg LBW c.ExposicionMuni#c.PriceRealMA c.Upstream#c.PriceRealMA `control' region#c.FECHA_NAC if NearGoldMine==1 & region<=3, absorb( codmpio) vce(cluster codmpio) 
estadd ysumm
eststo: quietly xi: areg Z_TALLA_NAC c.ExposicionMuni#c.PriceRealMA c.Upstream#c.PriceRealMA `control' region#c.FECHA_NAC if NearGoldMine==1 & region<=3, absorb( codmpio) vce(cluster codmpio) 
estadd ysumm
estout using "$latexcodes\IndividualMeasure_Combo.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(ExposicionMuni Upstream PriceRealMA c.ExposicionMuni#c.PriceRealMA c.Upstream#c.PriceRealMA)  stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$")) replace

/*
eststo clear
local control i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
eststo: quietly xi: areg APGAR_BAJO Arena_ExposicionMuni Arena_Upstream `control' region#c.FECHA_NAC if NearArenaMine==1 & region<=3, absorb( codmpio) vce(cluster codmpio) 
estadd ysumm
estout using "$latexcodes\IndividualMeasure_Combo_Arena.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(Arena_ExposicionMuni Arena_Upstream )  stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$")) replace
*/


eststo clear
local control i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
eststo: quietly xi: areg APGAR_BAJO c.D_ExposicionMuni##c.PriceRealMA c.D_Upstream##c.PriceRealMA  `control' region#c.FECHA_NAC if NearGoldMine==1 & region<=3, absorb( codmpio) vce(cluster codmpio) 
estadd ysumm
eststo: quietly xi: areg APGAR_BAJO c.D_ExposicionMuni#c.PriceRealMA c.D_Upstream#c.PriceRealMA  `control' region#c.FECHA_NAC if NearGoldMine==1 & region<=3, absorb( codmpio) vce(cluster codmpio) 
estadd ysumm
eststo: quietly xi: areg LBW c.D_ExposicionMuni#c.PriceRealMA c.D_Upstream#c.PriceRealMA  `control' region#c.FECHA_NAC if NearGoldMine==1 & region<=3, absorb( codmpio) vce(cluster codmpio) 
estadd ysumm
eststo: quietly xi: areg Z_TALLA_NAC c.D_ExposicionMuni#c.PriceRealMA c.D_Upstream#c.PriceRealMA  `control' region#c.FECHA_NAC if NearGoldMine==1 & region<=3, absorb( codmpio) vce(cluster codmpio) 
estadd ysumm
estout using "$latexcodes\IndividualDummies_Combo.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_ExposicionMuni D_Upstream PriceRealMA c.D_ExposicionMuni#c.PriceRealMA c.D_Upstream#c.PriceRealMA)  stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$")) replace

/*
eststo clear
local control i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
eststo: quietly xi: areg APGAR_BAJO D_Arena_ExposicionMuni D_Arena_Upstream  `control' region#c.FECHA_NAC if NearArenaMine==1 & region<=3, absorb( codmpio) vce(cluster codmpio) 
estadd ysumm
estout using "$latexcodes\IndividualDummies_Combo_Arena.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_Arena_ExposicionMuni D_Arena_Upstream)  stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$")) replace
*/

eststo clear
estpost tabstat AreaMinadaProp ExposicionMuni Upstream ProduccionPerArea if NearGoldMine==1 & region<=3, statistics(mean median sd max min p95 count) columns(statistics)
esttab using "$latexcodes\summaryIndep_IndividualTodos.tex", cells("mean(fmt(a2) label(Mean)) p50(fmt(a2) label(Median)) sd(fmt(a2) label(Std. Dev.)) min(fmt(a2) label(Min)) max(fmt(a2) label(Max)) p95(fmt(a2) label(Perc. 95)) count(fmt(a2) label(N))") nomtitle nonumber label  replace noobs booktabs



eststo clear
replace AreaMinadaProp=. if AreaMinadaProp==0
replace ExposicionMuni=. if ExposicionMuni==0
replace Upstream=. if Upstream==0
replace ProduccionPerArea=. if ProduccionPerArea==0
estpost tabstat AreaMinadaProp ExposicionMuni Upstream ProduccionPerArea if NearGoldMine==1 & region<=3, statistics(mean median sd max p95 count) columns(statistics)
esttab using "$latexcodes\summaryIndep_IndividualPositivos.tex", cells("mean(fmt(a2) label(Mean)) p50(fmt(a2) label(Median)) sd(fmt(a2) label(Std. Dev.)) min(fmt(a2) label(Min)) max(fmt(a2) label(Max)) p95(fmt(a2) label(Perc. 95)) count(fmt(a2) label(N))") nomtitle nonumber label  replace noobs booktabs
clear all

cd "E:\Copy\MiningAndBirth\"
*cd "C:\Copy\MiningAndBirth\"
*cd "Z:\MiningAndBirth\"
*cd "C:\Users\Santi\Copy\PYP_Birth\"
set matsize 5000
set mem 200000
use "CreatedData\DataMuni.dta",clear
compress
rename Group_1 ANO
rename Group_2 MES
rename Group_3 CODIGO_DANE

gen modate = ym(ANO, MES) 
tsset CODIGO_DANE modate, monthly

*sort  CODIGO_DANE
*merge m:1  CODIGO_DANE using "CreatedData\Illegal_munis.dta"
*eststo clear
*eststo: quietly  reg APGAR_BAJO AreaMinadaProp AreaMinadaUpstream i.CODIGO_DANE i.ANO i.MES, vce(cluster CODIGO_DANE)
*eststo: quietly  reg D.APGAR_BAJO D.AreaMinadaProp D.AreaMinadaUpstream i.CODIGO_DANE i.ANO i.MES, vce(cluster CODIGO_DANE)
*esttab using "Results\ResultItensity_Collap_Santi.tex", se r2 booktabs label indicate(  "Month F.E.=  *MES*" "Year F.E.=  *ANO*" "Municipality F.E.=  *CODIGO_DA*" ) replace b(a2) se(a2) nocon nomtitles  

eststo clear
eststo: quietly  reg APGAR_BAJO AreaMinadaProp AreaMinadaProp2 i.CODIGO_DANE i.ANO i.MES, vce(cluster CODIGO_DANE)
eststo: quietly  reg APGAR_BAJO AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstream i.CODIGO_DANE i.ANO i.MES, vce(cluster CODIGO_DANE)
esttab using "Results\ResultCambioVar.tex", se r2 booktabs label indicate(  "Month F.E.=  *MES*" "Year F.E.=  *ANO*" "Municipality F.E.=  *CODIGO_DA*" ) replace b(a2) se(a2) nocon nomtitles  

*eststo clear
*eststo: quietly  reg APGAR_BAJO AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstream i.CODIGO_DANE i.ANO i.MES, vce(cluster CODIGO_DANE)
*eststo: quietly  reg APGAR_BAJO AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstream i.CODIGO_DANE i.ANO i.MES if INDEX_MUNMINERO>0, vce(cluster CODIGO_DANE)
*esttab using "Results\ResultMing0Up.tex", se r2 booktabs label indicate(  "Month F.E.=  *MES*" "Year F.E.=  *ANO*" "Municipality F.E.=  *CODIGO_DA*" ) replace b(a2) se(a2) nocon nomtitles  

*Not significant
*eststo clear
*eststo: quietly  reg APGAR_BAJO AreaMinadaProp AreaMinadaProp2 i.CODIGO_DANE i.ANO i.MES if ilegal==0, vce(cluster CODIGO_DANE)
*eststo: quietly  reg APGAR_BAJO AreaMinadaProp AreaMinadaProp2 i.CODIGO_DANE i.ANO i.MES if ilegal==0 & AreaMinadaProp>0, vce(cluster CODIGO_DANE)
*esttab using "Results\ResultLegal.tex", se r2 booktabs label indicate(  "Month F.E.=  *MES*" "Year F.E.=  *ANO*" "Municipality F.E.=  *CODIGO_DA*" ) replace b(a2) se(a2) nocon nomtitles  

*Not significant
*eststo clear
*eststo: quietly  reg APGAR_BAJO AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstream i.CODIGO_DANE i.ANO i.MES if ilegal==0, vce(cluster CODIGO_DANE)
*eststo: quietly  reg APGAR_BAJO AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstream i.CODIGO_DANE i.ANO i.MES if ilegal==0 & AreaMinadaProp>0, vce(cluster CODIGO_DANE)
*esttab using "Results\ResultLegalUp.tex", se r2 booktabs label indicate(  "Month F.E.=  *MES*" "Year F.E.=  *ANO*" "Municipality F.E.=  *CODIGO_DA*" ) replace b(a2) se(a2) nocon nomtitles  

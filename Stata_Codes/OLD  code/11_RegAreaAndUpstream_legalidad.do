use "$base_out\DataCompleta_Stata.dta", clear
merge m:1 codmpio using "$base_out\ilegalMunis.dta"
set matsize 100
drop if _merge==2
replace Ilegal=0 if _merge==1
drop _merge

gen YrMonth=ym(ano,MES)
gen YrQrt=yq(ano,Quarter)
gen region=gandina+2*gcaribe+3*gpacifica+4*gorinoquia+5*gamazonia
keep Ilegal ano MES region D_AreaMinadaProp D_ProduccionPerArea D_ExposicionMuni D_Upstream AreaMinadaProp ProduccionPerArea ExposicionMuni Upstream AreaMinadaProp2 ProduccionPerArea2 ExposicionMuni2 Upstream2 Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE APGAR_BAJO LBW codmpio FECHA_NAC

forvalues i=0/1{
local var_indepList AreaMinadaProp ProduccionPerArea
foreach var_indep of varlist `var_indepList'{
eststo clear
local control i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
local var_try1 APGAR_BAJO LBW
foreach vardep of varlist `var_try1'{
eststo: quietly xi: areg `vardep' `var_indep' `var_indep'2 `control' region#c.FECHA_NAC if Ilegal==`i', absorb(codmpio) vce(cluster codmpio)
estadd ysumm
}
estout using "$latexcodes\IndividualMeasure_`var_indep'_legalidad`i'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(`var_indep' `var_indep'2 )  stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$")) replace
}


local var_indepList AreaMinadaProp ProduccionPerArea
foreach var_indep of varlist `var_indepList'{
eststo clear
local control i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
local var_try1 APGAR_BAJO LBW
foreach vardep of varlist `var_try1'{
eststo: quietly xi: areg `vardep' D_`var_indep' `control' region#c.FECHA_NAC if Ilegal==`i', absorb( codmpio) vce(cluster codmpio)
estadd ysumm
}
estout using "$latexcodes\IndividualDummies_`var_indep'_legalidad`i'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_`var_indep')  stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$")) replace

}


eststo clear
local control i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
local var_try1 APGAR_BAJO LBW
foreach vardep of varlist `var_try1'{
eststo: quietly xi: areg `vardep' ExposicionMuni ExposicionMuni2 Upstream Upstream2 `control' region#c.FECHA_NAC if Ilegal==`i', absorb( codmpio) vce(cluster codmpio) 
estadd ysumm
}

estout using "$latexcodes\IndividualMeasure_Combo_legalidad`i'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(ExposicionMuni ExposicionMuni2 Upstream Upstream2 )  stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$")) replace

eststo clear
local control i.ano i.Semana_Naci MadreSoltera EduMadrePostPrimaria EDAD_MADRE
local var_try1 APGAR_BAJO LBW
foreach vardep of varlist `var_try1'{
eststo: quietly xi: areg `vardep' D_ExposicionMuni D_Upstream  `control' region#c.FECHA_NAC if Ilegal==`i', absorb( codmpio) vce(cluster codmpio) 
estadd ysumm

}
estout using "$latexcodes\IndividualDummies_Combo_legalidad`i'.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(D_ExposicionMuni D_Upstream)  stats(N ymean r2, fmt(a2 a2 a2) labels ("N. of obs." "Mean of Dep. Var." "\$R^2\$")) replace

}



use "C:\Users\Santi\Copy\PYP_Birth\GIS\Apr7_river\vertex_river.dta", clear
drop fid near_dist
rename id starid
rename xcoord startx
rename ycoord starty
rename rastervalu startz
rename near_fid near_muniid_start
sort startx
merge m:1 startx starty using "C:\Users\Santi\Copy\PYP_Birth\GIS\Apr7_river\edge_river.dta"
drop _merge
save "C:\Users\Santi\Copy\PYP_Birth\GIS\Apr7_river\edge_master.dta", replace

use "C:\Users\Santi\Copy\PYP_Birth\GIS\Apr7_river\vertex_river.dta", clear
drop fid near_dist
rename id endid
rename xcoord endx
rename ycoord endy
rename rastervalu endz
rename near_fid near_muniid_end
sort endx
merge m:m endx endy using "C:\Users\Santi\Copy\PYP_Birth\GIS\Apr7_river\edge_master.dta"
drop _merge
drop if startz==.
drop if endz==.
save "C:\Users\Santi\Copy\PYP_Birth\GIS\Apr7_river\edge_master.dta", replace


use "C:\Users\Santi\Copy\PYP_Birth\CreatedData\PanelProduccion.dta", clear
collapse (sum)  produccion, by( year codigodane)
save "C:\Users\Santi\Copy\PYP_Birth\CreatedData\PanelProduccion_year.dta"
collapse  produccion, by( codigodane)
gen evermined=1 if produccion>0

*I didnt save the poblacion file before merging but use
*merge 1:1  year codigodane using "C:\Users\Santi\Copy\PYP_Birth\CreatedData\PanelProduccion_year.dta"

gen popgr= poblacion/ poblacion[_n-1]-1 if year!=1985
gen prodnew=0 if  produccion!=.
replace prodnew=1 if  produccion[_n-1]==0 & produccion>0 ^ produccion!=.
reg  poblacion produccion i.codigodane i.year
reg  poblacion produccion i.year i.codigodane
reg  popgr prodnew i.year
reg  popgr prodnew i.year

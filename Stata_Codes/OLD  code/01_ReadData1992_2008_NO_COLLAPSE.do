set mem 700000
forval i=1992/2008 {
use C:\Users\Santi\Copy\PYP_Birth\RawData\def`i'.dta, clear
if `i'<1997{
rename zon_defun a_defun
rename coddpto_r codptore
rename codmuni_r codmunre
rename zon_res area_res
replace area_res=99 if area_res==3
rename caus9999 c_dir1
keep  cod_dpto mes ano cod_munic a_defun sexo edad codptore codmunre area_res c_dir1
replace edad="0.003" if edad=="X1"
replace edad="0.01" if edad=="X2"
replace edad="0.05" if edad=="X3"
replace edad="0.29" if edad=="X4"
replace edad="0.75" if edad=="X5"
replace edad="." if edad=="00"
replace edad="." if edad=="0"

if `i'==1996 {
destring edad, replace force
}
else {
destring edad, replace
}


}

else if `i'<=1998{
rename dpto_d cod_dpto
rename munic_d cod_munic
if `i'==1997{
rename zon_d a_defun
rename zon_res area_res
rename c_basica c_dir1
}
else {
rename area_d a_defun
rename area_r area_res

}
replace a_defun=99 if a_defun==3
rename dpto_r codptore
rename munic_r codmunre

if `i'==1997 {
keep  cod_dpto cod_munic mes ano a_defun sexo edad codptore codmunre area_res c_dir1

replace edad="0.0015" if edad=="X1"
replace edad="0.003" if edad=="X2"
replace edad="0.01" if edad=="X3"
replace edad="0.05" if edad=="X4"
replace edad="0.08" if edad=="X5"
replace edad="0.25" if edad=="X6"
replace edad="0.75" if edad=="X7"
replace edad="." if edad=="00"
replace edad="." if edad=="0"
destring edad, replace
}
if `i'==1998 {
keep  cod_dpto cod_munic mes ano fecha_def a_defun sexo edad codptore codmunre area_res c_dir1
gen dia=mod(fecha_def,100)
replace edad=(edad-100)/(24*365) if edad<199
replace edad=1/(12*365) if edad==199
replace edad=(edad-200)/365 if (edad<299 & edad>200)
replace edad=15/365 if edad==299
replace edad=(edad-300)/12 if (edad<399 & edad>300)
replace edad=0.5 if edad==300
replace edad=1.5 if edad==399
replace edad=. if edad==400 | edad==499 | edad==999
replace edad=edad-400 if edad>400 & edad<499
}
}
else{

if `i'==2008{
keep  cod_dpto cod_munic fecha_def a_defun cod_insp tipo_defun  sexo edad codptore codmunre area_res codigo man_muer c_dir1
replace edad=edad/(24*60*365) if edad<60
replace edad=30/(60*24*365) if edad==99
replace edad=(edad-1000)/(24*365) if (edad<1024 & edad>1000)
replace edad=1/(365*2) if edad==1099
replace edad=(edad-2000)/365 if (edad<2032 & edad>2000)
replace edad=15/365 if edad==2099
replace edad=(edad-3000)/12 if (edad<3013 & edad>3000)
replace edad=. if edad==4000 | edad==4999 | edad==9999
replace edad=edad-4000 if edad>=4000 & edad<4180
gen ano=2008
gen mes=substr( fecha_def,1,strpos(fecha_def,"/") -1)
destring mes, replace
gen fsmes=substr( fecha_def,strpos(fecha_def,"/") +1,.)
gen dia=substr( fsmes,1,strpos(fsmes,"/") -1)
destring dia, replace
}
else{
keep  cod_dpto cod_munic fecha_def ano mes a_defun cod_insp tipo_defun  sexo edad codptore codmunre area_res codigo man_muer c_dir1
if `i'<2001{
gen dia=mod(fecha_def,100)
}

else{
gen fsmes=substr( fecha_def,strpos(fecha_def,"/") +1,.)
gen dia=substr( fsmes,1,strpos(fsmes,"/") -1)
destring dia, replace
}
replace edad=(edad-100)/(24*365) if edad<199
replace edad=1/(12*365) if edad==199
replace edad=(edad-200)/365 if (edad<299 & edad>200)
replace edad=15/365 if edad==299
replace edad=(edad-300)/12 if (edad<399 & edad>300)
replace edad=0.5 if edad==300
replace edad=1.5 if edad==399
replace edad=. if edad==400 | edad==499 | edad==999
replace edad=edad-400 if edad>400 & edad<499
}
}


*After making notation compatible, manipulate

gen male=1 if sexo==1
replace male=0 if sexo==2

label variable cod_dpto `"Codigo_dpto_defuncion"'
label variable cod_munic `"Codigo_munic_defuncion"'
label variable a_defun `"Area_def(1=cabecera)"'

rename a_defun cabecera

replace cabecera=0 if cabecera!=1 & cabecera!=.
label variable codptore `"Cod_dpto_residencia"'
label variable codmunre `"Cod_munic_residencia"'

label variable area_res `"Area_residencia(1=cabecera)"'
rename  area_res cabecera_res
label variable c_dir1 `"Causa_muerte_OJO_codigos_cambian"'
rename  c_dir1 causa


save C:\Users\Santi\Copy\PYP_Birth\CreatedData\def`i'clean.dta, replace
}



*cd "E:\Copy\MiningAndBirth\"
cd "C:\Users\Mauricio\Copy\MiningAndBirth\"
*cd "C:\Copy\MiningAndBirth\"
*cd "Z:\MiningAndBirth\"
*cd "C:\Users\Santi\Copy\PYP_Birth\"
set matsize 5000
set mem 200000
use "CreatedData\DataMuni.dta",clear
compress
rename Group_1 ANO
rename Group_2 MES
rename Group_3 CODIGO_DANE
rename x peso


gen modate = ym(ANO, MES) 
tsset CODIGO_DANE modate, monthly
replace ProduccionAprox=ProduccionAprox/(1000000)
reg APGAR_BAJO ProduccionAprox i.CODIGO_DANE i.ANO i.MES [pw=peso], vce(cluster CODIGO_DANE)

gen ProduccionAprox2=ProduccionAprox*ProduccionAprox
gen ProduccionAproxPcapita=ProduccionAprox/Poblacion
gen ProduccionAproxPcapita2=ProduccionAproxPcapita*ProduccionAproxPcapita
eststo clear
eststo est1: quietly  reg APGAR_BAJO AreaMinadaProp AreaMinadaProp2 i.CODIGO_DANE i.ANO i.MES [pw=peso], vce(cluster CODIGO_DANE)
eststo est2: quietly  reg APGAR_BAJO AreaMinadaProp AreaMinadaProp2 AreaMinadaUpstream i.CODIGO_DANE i.ANO i.MES [pw=peso], vce(cluster CODIGO_DANE)
eststo est3: quietly  reg APGAR_BAJO ProduccionAprox ProduccionAprox2 i.CODIGO_DANE i.ANO i.MES [pw=peso], vce(cluster CODIGO_DANE)
eststo est4: quietly  reg APGAR_BAJO ProduccionAprox ProduccionAprox2 AreaMinadaUpstream i.CODIGO_DANE i.ANO i.MES [pw=peso], vce(cluster CODIGO_DANE)
eststo est5: quietly  reg APGAR_BAJO ProduccionAproxPcapita ProduccionAproxPcapita2 i.CODIGO_DANE i.ANO i.MES [pw=peso], vce(cluster CODIGO_DANE)
eststo est6: quietly  reg APGAR_BAJO ProduccionAproxPcapita ProduccionAproxPcapita2 AreaMinadaUpstream i.CODIGO_DANE i.ANO i.MES [pw=peso], vce(cluster CODIGO_DANE)
esttab est1 est2 using "Results\ResultProducciona.tex", se r2 booktabs label indicate(  "Month F.E.=  *MES*" "Year F.E.=  *ANO*" "Municipality F.E.=  *CODIGO_DA*" ) replace b(a2) se(a2) nocon nomtitles  star(* 0.1 ** 0.05 * 0.01)
esttab est3 est4 using "Results\ResultProduccionb.tex", se r2 booktabs label indicate(  "Month F.E.=  *MES*" "Year F.E.=  *ANO*" "Municipality F.E.=  *CODIGO_DA*" ) replace b(a2) se(a2) nocon nomtitles  star(* 0.1 ** 0.05 * 0.01)
esttab est5 est6 using "Results\ResultProduccionc.tex", se r2 booktabs label indicate(  "Month F.E.=  *MES*" "Year F.E.=  *ANO*" "Municipality F.E.=  *CODIGO_DA*" ) replace b(a2) se(a2) nocon nomtitles  star(* 0.1 ** 0.05 * 0.01)


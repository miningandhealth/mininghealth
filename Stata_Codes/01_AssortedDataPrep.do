***************************************************************
***************************************************************
*********This code just gives us all the municpalities name/codes
***************************************************************
***************************************************************
use "$base_in/PANELES_CEDE/CARACTERÍSTICAS GENERALES/PANEL CARAC. GENERALES.dta", clear
keep if ano==2010
keep municipio depto codmpio
save "$base_out/Temporary/muni_names.dta", replace



***************************************************************
***************************************************************
********This code calculcates which municipalitiies have any gold production
***************************************************************
***************************************************************


import delimited "$base_out/PanelProduccion.csv", clear 
rename codigodane codmpio
collapse (sum) produccion, by(codmpio)
drop if produccion==0
save "$base_out/Temporary/muni_oro.dta", replace


***************************************************************
***************************************************************
********This code calculates how much mercury is used by gr of gold in different municipalities
***************************************************************
***************************************************************


import excel "$base_in/Mercurio_x_oro.xlsx", sheet("depto") firstrow clear
rename departamento depto
save "$base_out/Temporary/depto_Hg.dta", replace
import excel "$base_in/Mercurio_x_oro.xlsx", sheet("municipio") firstrow clear
merge m:1 depto using "$base_out/Temporary/depto_Hg.dta"
drop _merge
merge 1:1 depto municipio using "$base_out/Temporary/muni_names.dta"
drop if _merge==2
drop _merge
replace codmpio=23682 if municipio=="Río Viejo"
replace codmpio=13600 if municipio=="San José de Uré"
replace codmpio=23466 if municipio=="Montelíbano"
gen emis_gHg_x_gAu_filon= emis_gHg_x_gAu_filon_abierto* pct_filon_abierto+ emis_gHg_x_gAu_filon_cerrado* pct_filon_cerrado
gen emis_gHg_x_gAu_aluvion= emis_gHg_x_gAu_aluvion_abierto* pct_aluvion_abierto+ emis_gHg_x_gAu_aluvion_cerrado* pct_aluvion_cerrado
replace emis_gHg_x_gAu_aluvion= emis_gHg_x_gAu_aluvion_abierto* pct_aluvion_abierto if pct_aluvion_cerrado==0
gen emis_gHg_x_gAu=emis_gHg_x_gAu_filon*pct_oro_filon/100+emis_gHg_x_gAu_aluvion*pct_oro_aluvion/100
replace emis_gHg_x_gAu=emis_gHg_x_gAu_filon if emis_gHg_x_gAu_aluvion==.
replace emis_gHg_x_gAu=emis_gHg_x_gAu_aluvion if emis_gHg_x_gAu_filon==.

drop if codmpio==.
merge 1:1 codmpio using "$base_out/Temporary/muni_oro.dta"
gen coddepto=(codmpio-mod(codmpio,1000))/1000
bysort coddepto: egen mean_Hg_Au_depto=mean( emis_gHg_x_gAu)
replace emis_gHg_x_gAu= mean_Hg_Au_depto if emis_gHg_x_gAu==.

keep codmpio emis_gHg_x_gAu
drop if emis_gHg_x_gAu==.

export delimited using "$base_out/emis_Hg_Au_muni.csv", replace

***************************************************************
***************************************************************
********This code imports the holy week dates
***************************************************************
***************************************************************


clear
import excel using "$base_in/Fechas_semana_santa.xlsx", first cellrange(A1:E24)
*Put easter sunday in Stata date format
gen hw_wdate=mdy(eastersunm,eastersund,ano)
*Calculate the friday before palm Sunday
gen hw_wfdate=hw_wdate-9
gen by_wdate=mdy(1,1,ano)
gen w_hweek=ceil((hw_wfdate-by_wdate)/7)
keep ano w_hweek
save "$base_out/Year_hw.dta", replace


***************************************************************
***************************************************************
********This code imports the holy week dates again? and estimates the closest hw for each week of the year in several years
***************************************************************
***************************************************************

clear
import excel using "$base_in/Fechas_semana_santa.xlsx", first cellrange(A1:E24)
*Put easter sunday in Stata date format
gen hw_wdate=mdy(eastersunm,eastersund,ano)
*Calculate the friday before palm Sunday
gen hw_wfdate=hw_wdate-9

local stdate=mdy(1,1,1992)
local counter=`stdate'
sum hw_wfdate
gen closest_hw=.
gen wdate=.
sum hw_wfdate
local size=`r(N)'-1
set obs 10000
forval b=1/`size'{

local paaa=`b'+1
while `counter'<hw_wfdate[`paaa'] {
local temp=`counter'-`stdate'+1
qui replace closest_hw=hw_wfdate[`b'] in `temp'
qui replace wdate=`counter' in `temp'
local counter=`counter'+1
}

}

keep wdate closest_hw
drop if wdate==.
save "$base_out/Closest_hw.dta", replace

***************************************************************
***************************************************************
********Determine which municipalities area near mines
***************************************************************
***************************************************************

import delimited "$base_out/MunicipioNearestMine.csv", clear
rename codane2 codmpio
drop if distmine>25000
keep codmpio
save "$base_out/Munis_mineros.dta",replace


***************************************************************
***************************************************************
********Determine which municipalities area prone to illegal mining
***************************************************************
***************************************************************


use "$base_in/cm_bd_final.dta", clear
drop if at_pyr_03_01_07!=1
gen legalidad=(tipo_mina==1) if !missing(tipo_mina)
estpost tabstat legalidad, by(ag_ue_01) statistics(mean count) columns(statistics)
collapse (mean) legalidad (count) minasCenso=legalidad, by( cod_mpio)
save "$base_out/cm_bd_oro_cod_mpio.dta", replace

import delim "$base_out/BaseMineriaIlegal_MinDefensa.csv", clear
collapse (sum) cantidad, by(cod_muni)
rename cod_muni cod_mpio
merge 1:1 cod_mpio using "$base_out/cm_bd_oro_cod_mpio.dta"
replace cantidad=0 if cantidad==.
replace minasCenso=0 if minasCenso==.
replace legalidad=. if minasCenso==0
label var cantidad "Police"
label var legalidad "Proportion of legal mines"
label var minasCenso "Total number of mines in 2011"
keep if cantidad>0 | legalidad<1
drop cantidad legalidad minasCenso
replace cod_mpio=13600 if cod_mpio==13490
replace cod_mpio=19142 if cod_mpio==19300
replace cod_mpio=27787 if cod_mpio==27160
replace cod_mpio=27205 if cod_mpio==27580
replace cod_mpio=27999 if cod_mpio==27205 | cod_mpio==27361 | cod_mpio==27491
gen Ilegal=1
drop _merge
rename cod_mpio codmpio
collapse (mean) Ilegal, by(codmpio)
save "$base_out/ilegalMunis.dta", replace





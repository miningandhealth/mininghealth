local corredor=c(username)


**Carolina
if "`corredor'"=="carol" {
global base_out "C:\Users\carol\OneDrive - Universidad del rosario\MineriaAll"
global latexpaper "C:\Users\carol\OneDrive - Universidad del rosario\MineriaAll"
global latexslides "C:\Users\carol\OneDrive - Universidad del rosario\MineriaAll"
global latexcodes "C:\Users\carol\OneDrive - Universidad del rosario\MineriaAll"
}
**Santiago
if "`corredor'"=="Santi" {
global base_out "C:/Users/Santi/Dropbox/Research/PYP_Birth/MineriaAll"
global latexpaper "C:/Users/Santi/Dropbox/Research/PYP_Birth/MineriaAll"
global latexslides "C:/Users/Santi/Dropbox/Research/PYP_Birth/MineriaAll"
global latexcodes "C:/Users/Santi/Dropbox/Research/PYP_Birth/MineriaAll"
}


****************Generando la base completa
global ntipos=4

use "$base_out/DataCompleta_14.dta", clear
drop if ANO<2001

*Generate stunt info
qui gen Z_TALLA_NAC=(TALLA_NAC-49.9)/1.8931 if MASC==1
qui replace Z_TALLA_NAC=(TALLA_NAC-49.1)/1.8627 if MASC==0
gen STUNT=(Z_TALLA_NAC<-2) if !missing(Z_TALLA_NAC)
label var Z_TALLA_NAC "(Z-score) Height"
gen Premature=(1-GEST_COMPLETA) if !missing(GEST_COMPLETA)

*Change variables so that coefficients are easier to read
foreach var of varlist APGAR_BAJO LBW STUNT MadreSoltera EduMadrePostPrimaria PartoHospital{
	replace `var'=100*`var'
}
label var STUNT "Stunted (\%)"
label var LBW "Low birth weight (\%)"
label var PartoHospital "In-hospital birth (\%)"
label var APGAR_BAJO "Low APGAR"
label var PESO_NAC "Weight (gr)"
label var TALLA_NAC "Height (cm)"
label var EDAD_MADRE "Mother's age"
label var MadreSoltera "Single mother (\%)"
label var ConsultasPreMayor4 "Prenatal checkups $>$ 4"
label var Premature "Premature"

*Drop people who have important missing data
drop if APGAR_BAJO==.
*This is dropping 2013 and 2014
*drop if PESO_NAC==.
*replace TALLA_NAC=. if TALLA_NAC<40
*replace TALLA_NAC=. if TALLA_NAC>55
*drop if TALLA_NAC==. 
drop if EDAD_MADRE==.
drop if MadreSoltera==.
drop if EduMadrePostPrimaria==.


qui compress 
drop DPTO_NACI MUNIC_NACI SEXO DPTO_R MUNIC_R MES
rename MES2 MES
rename ANO ano
rename CODIGO_DANE_M codmpio 

keep Premature EDAD_MADRE SEMANAS MES FECHA_NAC PESO_NAC TALLA_NAC NUM_CONSUL APGAR_CONTINUO GEST_COMPLETA APGAR_BAJO LBW LBW VLBW MASC PartoEspontaneo Csection MultipleBirth PartoHospital MadreMenor14 Madre14_17 ConsultasPreMayor4 RegimenContributivo RegimenSubsidiado MadreSoltera EduMadrePostPrimaria EduMadrePostSecundaria Z_TALLA_NAC STUNT ano Quarter codmpio  AreaTipo* ExpArea* UpstreamArea*
*AreaMinada* (2012 no esta)
*AreaTipo* (2014)
*Produccion* (2012 no esta)

*Mark who is near a gold mine and drop who is not!
merge m:1 codmpio using "$base_out/Munis_mineros.dta"
drop if _merge==2
gen NearGoldMine=1 if _merge==3
replace NearGoldMine=0 if _merge==1
drop _merge
qui compress
drop if NearGoldMine==0
drop NearGoldMine

merge m:1 codmpio ano using "$base_in/PANEL CARAC. GENERALES.dta", keepus(pobl_tot gandina gcaribe gpacifica gorinoquia gamazonia pobl_rur pobl_urb pobl_tot indrural areaoficialkm2 altura discapital dismdo TMI disbogota codmdo gpc pobreza gini nbi minorias parques religioso estado otras pecsaludc pepsaludc pehosclinc peplantaec pebibliopc ipm_ti_p ipm_tdep_p ipm_assalud_p ipm_accsalud_p ipm_accagua_p ipm_templeof_p ipm_tdep_p ipm_excretas_p ipm_pisos_p ipm_paredes_p ipm_hacinam_p)
drop if _merge==2
drop _merge
//Para 2012
*rename AreaMinada_M AreaMinada_sqkm
*rename ProduccionAprox_M Produccion
*rename ProduccionAprox_Acc_M Produccion_Acc
*gen AreaMinadaProp=100*AreaMinada_sqkm/areaoficialkm2


qui foreach i of numlist 1/$ntipos {
rename AreaTipo`i' AreaMinadaTipo`i'_sqkm
gen AreaMinadaTipo`i'Prop=100*AreaMinadaTipo`i'_sqkm/areaoficialkm2
}

gen region=gandina+2*gcaribe+3*gpacifica+4*gorinoquia+5*gamazonia
bys codmpio: egen mregion=mean(region)
replace region=mregion if region==.
drop mregion
keep if region<=3

*Lets top code... FORGET ABOUT THAT 15% BULSHIT
*Produccion*
qui foreach vrn of varlist AreaMinadaTipo* ExpArea* UpstreamArea*  {
	qui sum `vrn' if `vrn'>0,d
	di r(p99)
	replace `vrn'=r(p99) if `vrn'>r(p99) & !missing(`vrn')
	capture drop D_`vrn'
	gen D_`vrn'=(`vrn'>0) if !missing(`vrn')
}
//para 2012 falta AreaMinada* Produccion*

*For Week FE
gen Semana_Naci=week(FECHA_NAC)
*Now to calculate some interesting stuff
/* Para 2012
gen ProduccionPerCapita=(Produccion/pobl_tot)/10^2
gen ProduccionPerArea=(Produccion/areaoficialkm2)/10^3
gen ProduccionAccPerArea=(Produccion_Acc/areaoficialkm2)/10^3
gen D_ProduccionPerArea=(ProduccionPerArea>0) if !missing(ProduccionPerArea)
gen D_ProduccionAccPerArea=(ProduccionAccPerArea>0) if !missing(ProduccionPerArea)
gen D_ProduccionPerCapita=(ProduccionPerCapita>0) if !missing(ProduccionPerCapita)
*/

/*Para 2012
foreach i in 5 10 20 {
label var ExpAreaMuni`i'KM "Near mining `i' km"
label var UpstreamAreaMuni`i'KM "Mining upstream `i' km"
label var D_ExpAreaMuni`i'KM "Near mining `i' km `=char(36)'>0`=char(36)'"
label var D_UpstreamAreaMuni`i'KM "Mining upstream `i' km `=char(36)'>0`=char(36)'"
}
*/

qui foreach i in 20 {
foreach t of numlist 1/$ntipos {
label var ExpAreaMuni`i'KM_Tipo`t' "Near mining `i' km Tipo`t'"
label var UpstreamAreaMuni`i'KM_Tipo`t' "Mining upstream `i' km Tipo`t'"
label var D_ExpAreaMuni`i'KM_Tipo`t' "Near mining `i' km `=char(36)'>0`=char(36)' Tipo`t'"
label var D_UpstreamAreaMuni`i'KM_Tipo`t' "Mining upstream `i' km `=char(36)'>0`=char(36)' Tipo`t'"
}
}


*Set up labels for latex 2012
/*

label var ProduccionPerCapita "Production/Population"
label var ProduccionPerArea "Production/Municipality Area"
label var ProduccionAccPerArea "Accumulated Production"
label var D_ProduccionPerArea "Production/Municipality Area `=char(36)'>0`=char(36)'"
label var D_AreaMinadaProp "Mining Area/Municipality Area `=char(36)'>0`=char(36)'"
label var D_ProduccionPerArea "Production `=char(36)'>0`=char(36)'"
label var D_ProduccionAccPerArea "Production Accumulated `=char(36)'>0`=char(36)'"
*/


*Lets add the price of gold
qui rename ano ANO
qui merge m:1 ANO MES using "$base_out/PriceGold.dta",keepus(PriceRealMA)
qui rename ANO ano
qui drop if _merge==2
qui drop _merge  
qui compress

foreach var_indep of varlist AreaMinada* ExpArea* UpstreamArea*{
	qui gen `var_indep'xp=`var_indep'*PriceRealMA
	qui gen D_`var_indep'xp=D_`var_indep'*PriceRealMA
}
*falta Produccion* y AreaMinada* para 2012
***La diferencia principal es que 2012 tiene las variables enteras y 2014 por tipos
**OJO CAMBIE NOMBRES AQUI ARRIBA

*Lets create some time variables
qui gen YrMonth=ym(ano,MES)
qui gen YrQrt=yq(ano,Quarter)

**Why this?
replace PriceRealMA=PriceRealMA*2.36

*Add some assorted labels
label var EduMadrePostPrimaria "Mother has post-primary education (\%)"
*label var AreaMinadaPropxp "{[Mining Area/Municipality Area]} x Price"
*2012



** Adding HW vars
gen dia=day(FECHA_NAC)
gen wdate=mdy(MES,dia,ano)
sort wdate
qui merge m:1 wdate using "$base_out/Closest_hw"
drop if _merge==2
drop _merge
gen wgesthw=ceil((wdate-closest_hw)/7)


replace wgesthw=. if SEMANAS==.
qui label var wgesthw "Week during gestation when holy week happens"
gen ihw=1 if wgesthw<=SEMANAS & wgesthw!=.
qui replace ihw=0 if wgesthw>SEMANAS & wgesthw!=.
label var ihw "Holy Week during gestation"



*Create "ring" proxiumty variables
*qui gen D_ExpAreaMuni510KM=(D_ExpAreaMuni10KM==1 & D_ExpAreaMuni5KM==0)
*qui gen D_ExpAreaMuni1020KM=(D_ExpAreaMuni20KM==1 & D_ExpAreaMuni10KM==0)
*qui gen D_UpstreamAreaMuni510KM=(D_UpstreamAreaMuni10KM==1 & D_UpstreamAreaMuni5KM==0)
*qui gen D_UpstreamAreaMuni1020KM=(D_UpstreamAreaMuni20KM==1 & D_UpstreamAreaMuni10KM==0)

*label var D_ExpAreaMuni510KM "Near mining 5-10 km `=char(36)'>0`=char(36)'"
*label var D_ExpAreaMuni1020KM "Near mining 10-20 km `=char(36)'>0`=char(36)'"
*label var D_UpstreamAreaMuni510KM "Mining upstream 5-10 km `=char(36)'>0`=char(36)'"
*label var D_UpstreamAreaMuni1020KM "Mining upstream 10-20 km `=char(36)'>0`=char(36)'"
//2012 

*Create "ring" proxiumty variables

foreach t of numlist 1/$ntipos {

label var AreaMinadaTipo`t'Prop "Mining Area Tipo`t'/Municipality Area"
label var AreaMinadaTipo`t'Propxp "{[Mining Area Tipo`t'/Municipality Area]} x Price"
qui gen D_ExpAreaMuni510KM_Tipo`t'=(D_ExpAreaMuni10KM_Tipo`t'==1 & D_ExpAreaMuni5KM_Tipo`t'==0)
qui gen D_ExpAreaMuni1020KM_Tipo`t'=(D_ExpAreaMuni20KM_Tipo`t'==1 & D_ExpAreaMuni10KM_Tipo`t'==0)
qui gen D_UpstreamAreaMuni510KM_Tipo`t'=(D_UpstreamAreaMuni10KM_Tipo`t'==1 & D_UpstreamAreaMuni5KM_Tipo`t'==0)
qui gen D_UpstreamAreaMuni1020KM_Tipo`t'=(D_UpstreamAreaMuni20KM_Tipo`t'==1 & D_UpstreamAreaMuni10KM_Tipo`t'==0)

label var D_ExpAreaMuni510KM_Tipo`t' "Near mining 5-10 km Tipo`t' `=char(36)'>0`=char(36)'"
label var D_ExpAreaMuni1020KM_Tipo`t' "Near mining 10-20 km Tipo`t' `=char(36)'>0`=char(36)'"
label var D_UpstreamAreaMuni510KM_Tipo`t' "Mining upstream 5-10 km Tipo`t' `=char(36)'>0`=char(36)'"
label var D_UpstreamAreaMuni1020KM_Tipo`t' "Mining upstream 10-20 km Tipo`t' `=char(36)'>0`=char(36)'"
}




merge m:1 codmpio using "$base_out/ilegalMunis.dta"
set matsize 5000
drop if _merge==2
replace Ilegal=0 if _merge==1
drop _merge

gen underground=0
replace underground=1 if D_UpstreamAreaMuni20KM_Tipo1==1 & D_UpstreamAreaMuni20KM_Tipo3==0 &  D_UpstreamAreaMuni20KM_Tipo2==0
bys codmpio: egen undergroundm=min(underground)
drop underground

qui gen coddepto=(codmpio-mod(codmpio,1000))/1000
qui gen APGAR_ALTO=100-APGAR_BAJO
gen ExpAreaMuni20KM_Tipo23=ExpAreaMuni20KM_Tipo2+ExpAreaMuni20KM_Tipo3
gen ExpAreaMuni20KM_Tipo13=ExpAreaMuni20KM_Tipo1+ExpAreaMuni20KM_Tipo3
gen ExpAreaMuni20KM_Tipo123=ExpAreaMuni20KM_Tipo1+ExpAreaMuni20KM_Tipo23
gen UpstreamAreaMuni20KM_Tipo23=UpstreamAreaMuni20KM_Tipo2+UpstreamAreaMuni20KM_Tipo3
gen UpstreamAreaMuni20KM_Tipo13=UpstreamAreaMuni20KM_Tipo1+UpstreamAreaMuni20KM_Tipo3
gen UpstreamAreaMuni20KM_Tipo123=UpstreamAreaMuni20KM_Tipo1+UpstreamAreaMuni20KM_Tipo23


gen D_ExpAreaMuni20KM_Tipo13=(ExpAreaMuni20KM_Tipo13>0) if ExpAreaMuni20KM_Tipo13!=.
gen D_ExpAreaMuni20KM_Tipo23=(ExpAreaMuni20KM_Tipo23>0) if ExpAreaMuni20KM_Tipo23!=.
gen D_ExpAreaMuni20KM_Tipo123=(ExpAreaMuni20KM_Tipo123>0) if ExpAreaMuni20KM_Tipo123!=.
gen D_UpstreamAreaMuni20KM_Tipo13=(UpstreamAreaMuni20KM_Tipo13>0) if UpstreamAreaMuni20KM_Tipo13!=.
gen D_UpstreamAreaMuni20KM_Tipo23=(UpstreamAreaMuni20KM_Tipo23>0) if UpstreamAreaMuni20KM_Tipo23!=.
gen D_UpstreamAreaMuni20KM_Tipo123=(UpstreamAreaMuni20KM_Tipo123>0) if UpstreamAreaMuni20KM_Tipo123!=.

gen ihw1tr=0 if wgesthw!=.
replace ihw1tr=1 if ihw==1 & wgesthw<=13
label var ihw1tr "Holyweek 1st trimester"

gen ihw2tr=0 if wgesthw!=.
replace ihw2tr=1 if ihw==1 & wgesthw>=14 & wgesthw<=27
label var ihw2tr "Holyweek 2nd trimester"

gen ihw3tr=0 if wgesthw!=.
replace ihw3tr=1 if ihw==1 & wgesthw>=28 & wgesthw<=43
label var ihw3tr "Holyweek 3rd trimester"


gen ihw12tr=0 if wgesthw!=.
replace ihw12tr=1 if ihw==1 & wgesthw<=27
label var ihw12tr "Holyweek 1-2 trimester"



local Ncentile=4
qui forval i=1/`Ncentile' {
local plcut=100*(`i'-1)/`Ncentile'
local pucut=100*(`i')/`Ncentile'
centile UpstreamAreaMuni20KM_Tipo23 if UpstreamAreaMuni20KM_Tipo23>0, centile(`plcut')
local lcut=r(c_1)
centile UpstreamAreaMuni20KM_Tipo23 if UpstreamAreaMuni20KM_Tipo23>0, centile(`pucut')
local ucut=r(c_1)
gen D_UpsBin`i'=(UpstreamAreaMuni20KM_Tipo23>`lcut')*(UpstreamAreaMuni20KM_Tipo23<=`ucut')
label var D_UpsBin`i' "Downstream from open pit mine q`i'"
gen D_UpsLegalBin`i'=(UpstreamAreaMuni20KM_Tipo3>`lcut')*(UpstreamAreaMuni20KM_Tipo3<=`ucut')
label var D_UpsLegalBin`i' "Downstream from legal open pit mine q`i'"
gen D_UpsIllegalBin`i'=(UpstreamAreaMuni20KM_Tipo2>`lcut')*(UpstreamAreaMuni20KM_Tipo2<=`ucut')
label var D_UpsIllegalBin`i' "Downstream from illegal open pit mine q`i'"
}

gen nearonlylegal=D_ExpAreaMuni20KM_Tipo3*(1-D_ExpAreaMuni20KM_Tipo2)
label var nearonlylegal "Near legal mine only"
gen nearonlyillegal=D_ExpAreaMuni20KM_Tipo2*(1-D_ExpAreaMuni20KM_Tipo3)
label var nearonlyillegal "Near illegal mine only"
gen nearboth=D_ExpAreaMuni20KM_Tipo2*D_ExpAreaMuni20KM_Tipo3
label var nearboth "Near both types of mines"

gen upsonlylegal=D_UpstreamAreaMuni20KM_Tipo3*(1-D_UpstreamAreaMuni20KM_Tipo2)
label var upsonlylegal "Downstream from legal mine only"
gen upsonlyillegal=D_UpstreamAreaMuni20KM_Tipo2*(1-D_UpstreamAreaMuni20KM_Tipo3)
label var upsonlyillegal "Downstream from illegal mine only"
gen upsboth=D_UpstreamAreaMuni20KM_Tipo2*D_UpstreamAreaMuni20KM_Tipo3
label var upsboth "Downstream from both types of mines"

** Tipo 1 Mining title without satelite detected mine (maybe underground)
** Tipo 2 Mine without title (illegal mine)
** Tipo 3 Mining title with mine
global control4 MadreSoltera EduMadrePostPrimaria EDAD_MADRE

label var D_ExpAreaMuni20KM_Tipo4 "Near mining title"
label var D_UpstreamAreaMuni20KM_Tipo4 "Downstream from mining title"
label var D_UpstreamAreaMuni20KM_Tipo3 "Downstream from legal mine"
label var D_ExpAreaMuni20KM_Tipo2 "Near illegal mine"
label var D_ExpAreaMuni20KM_Tipo3 "Near legal mine"
label var D_UpstreamAreaMuni20KM_Tipo2 "Downstream from illegal mine"
label var D_ExpAreaMuni20KM_Tipo123 "Near any mine"
label var D_UpstreamAreaMuni20KM_Tipo123 "Downstream from any mine"
label var D_ExpAreaMuni20KM_Tipo23 "Near open pit mine"
label var D_UpstreamAreaMuni20KM_Tipo23 "Downstream from open pit mine"

save "$base_out/DataCompleta_Stata_modificacion.dta",replace












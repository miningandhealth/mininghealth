OrderRiverFun=function(DataRiver){

#edge_master=edge_master[!duplicated(edge_master$id_edge),]
DataRiver=edge_master


#First identify vertex that no other river has


AllVertex=matrix(sort(unique(c(DataRiver$endid,DataRiver$starid))),ncol=1)
AllVertex=data.frame(AllVertex)
AllVertex$z=NA
AllVertex$x=NA
AllVertex$y=NA
index=match(AllVertex$AllVertex,DataRiver$endid)
AllVertex[,c("z","x","y")]=DataRiver[index,c("endz","endx","endy")]

index=match(AllVertex$AllVertex[which(is.na(AllVertex$z))],DataRiver$starid)
AllVertex[which(is.na(AllVertex$z)),c("z","x","y")]=DataRiver[index,c("startz","startx","starty")]


Numero=table(c(DataRiver$endid,DataRiver$starid))
VertexOnce=as.numeric(names(Numero)[which(Numero==1)])

VertexOnceMatrix=AllVertex[match(VertexOnce,AllVertex$AllVertex),]

VertexSalidasRio=VertexOnceMatrix[which(VertexOnceMatrix[,"z"]<1),]

VectorPorArreglar=AllVertex$AllVertex

DataRiver$Changed=0
FunIterRiv=function(VectorFin){
IndexTemp=which( (DataRiver$starid %in% VectorFin) & DataRiver$Changed==0)
if(length(IndexTemp)==0){return(0)}
if(length(IndexTemp)>0){
print("change")
DataRiver[IndexTemp,1:10]<<-DataRiver[IndexTemp,c(6,7,8,9,10,1,2,3,4,5)]
}
DataRiver$Changed[which(DataRiver$endid %in% VectorFin)]<<-1
VectorFinTemp=DataRiver[which(DataRiver$endid %in% VectorFin),"starid"]
if(length(VectorFinTemp)>0) { FunIterRiv(VectorFinTemp)}
}

FunIterRiv(VertexSalidasRio[,1])

sort(table(DataRiver$starid))
sort(table(edge_master$starid))



AllVertex[which(AllVertex$AllVertex==8629),]

head(DataRiver,10)
head(edge_master,10)

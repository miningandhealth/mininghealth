
## Create river data matrix

COL_riv_proj <- readOGR("RawData/GIS","COL_riv_proj")
COL_riv_proj <- spTransform(COL_riv_proj, CRS(projectionAll))

Coords=rbind(cbind(COL_riv_proj$xstart,COL_riv_proj$ystart),
             cbind(COL_riv_proj$xend,COL_riv_proj$yend))
Coords=unique(Coords)
Coords=data.frame(Coords)
colnames(Coords)=c("x","y")
Coords$ID_vertex=seq(1,dim(Coords)[1])
colnames(Coords)=c("xstart","ystart","id_start_vertex")
COL_riv_proj=merge(COL_riv_proj,Coords)
colnames(Coords)=c("xend","yend","id_end_vertex")
COL_riv_proj=merge(COL_riv_proj,Coords)
save(COL_riv_proj,file="CreatedData/COL_riv_proj.RData")

###############

## Municipality data 
dpto=dir(path = "RawData/GIS/MGN")[1]
Municipios=readOGR(paste0("RawData/GIS/MGN/",dpto), sub("^([^.]*).*", "\\1",dir(paste0("RawData/GIS/MGN/",dpto))[grep("MPIO",dir(paste0("RawData/GIS/MGN/",dpto)))])[1])
Municipios <- spTransform(Municipios, CRS(projectionAll))
for(dpto in dir(path = "RawData/GIS/MGN")[-1]){
  Temp = readOGR(paste0("RawData/GIS/MGN/",dpto), sub("^([^.]*).*", "\\1",dir(paste0("RawData/GIS/MGN/",dpto))[grep("MPIO",dir(paste0("RawData/GIS/MGN/",dpto)))])[1])
  Temp <- spTransform(Temp, CRS(projectionAll))
  row.names(Temp)=as.character(seq(dim(Municipios)[1]+1,length.out=dim(Temp)[1]))
  Municipios=spRbind(Municipios, Temp)
}
DataKeep=Municipios@data 
Municipios=gBuffer(Municipios,byid=T,width=0)
Municipios=SpatialPolygonsDataFrame(Municipios, DataKeep, match.ID = TRUE)
Municipios$CODANE2=as.numeric(as.character(Municipios$MPIO_CCNCT))
Municipios$CodigoDane=as.numeric(as.character(Municipios$MPIO_CCNCT))
Municipios$area_m2=sapply(slot(Municipios, "polygons"), slot, "area")
Municipios$area_km2=Municipios$area_m2/(1000^2) # Square Km
save(Municipios,file='CreatedData/Municipios_GIS.RData')

##################

#####This code reads the mining data and extracts the gold permits
##Creates a file with all the mines that have gold mining
TMC = readOGR("RawData/GIS", "TMC_Jul2012")
TMC <- spTransform(TMC, CRS(projectionAll))
##Santi tried to update to the new Tierra Minada file but it doesnt have FECHA_TERM
#TMC=read.dbf("RawData/GIS/TITULOS_PAIS.dbf")
DataKeep=TMC@data
TMC=gBuffer(TMC,byid=T,width=0)
TMC=SpatialPolygonsDataFrame(TMC, DataKeep, match.ID = TRUE)

##Make some adjustments to keep correct format
TMC$MINERALES=as.character(TMC$MINERALES)
TMC$MODALIDADE=as.character(TMC$MODALIDADE)
TMC$TITULARES=as.character(TMC$TITULARES)
TMC$FECHA_INSC=as.Date(as.character(TMC$FECHA_INSC),format="%Y/%m/%d")
TMC$FECHA_TERM=as.Date(as.character(TMC$FECHA_TERM),format="%Y/%m/%d")

TMC$area_m2=sapply(slot(TMC, "polygons"), slot, "area")
TMC$area_km2=TMC$area_m2/(1000^2) #para que quede en sqkm 

##only keep stuff post 1998(permit expires after this date)
INDEX_FECHA2=which(TMC$FECHA_TERM>as.Date("01-01-1998",format="%d-%m-%Y"))
TMC=TMC[INDEX_FECHA2,]
##Do not use exploration lisences, only explotation
TMC<-TMC[!(TMC$MODALIDADE=="LICENCIA DE EXPLORACION"),]
##which permits are "gold" related
INDEX_ORO=which(grepl("ORO",as.character(TMC$MINERALES)))
##which permits are in a consecion style, and which are trying to legalize small miners
INDEX_CONCENCION=which(grepl("CONTRATO DE CO",as.character(TMC$MODALIDADE)))
INDEX_LEGALIZACION=which(grepl("LEGAL",as.character(TMC$MODALIDADE)))

##Try to extract which permits are given to a natural person, as opposed to a juridical person
t0=regexpr("\\(", TMC$TITULARES)
tf=regexpr("\\)", TMC$TITULARES)
TMC$ID=substr(TMC$TITULARES, t0+1, tf-1)
TMC$ID=gsub("-", "", TMC$ID)
TMC$ID=as.numeric(as.character(TMC$ID))
INDEX_PERSONA=which(TMC$ID<1050000000)


##create some index that show which permits are gold, concesion, legalization and natural person
TMC$ORO=0
TMC$ORO[INDEX_ORO]=1
TMC$CONCENCION=0
TMC$CONCENCION[INDEX_CONCENCION]=1
TMC$LEGALIZACION=0
TMC$LEGALIZACION[INDEX_LEGALIZACION]=1
TMC$PERSONA=0
TMC$PERSONA[INDEX_PERSONA]=1


##keep only gold mines
MINAS_ORO=TMC[INDEX_ORO,]

##I exctract the Codigo_ORO for GIS... not sure if we need this anymore
CODIGO_ORO=MINAS_ORO$CODIGO_EXP
CODIGO_ORO=data.frame(CODIGO_ORO)
CODIGO_ORO=cbind(CODIGO_ORO,1)
colnames(CODIGO_ORO)=c("CODIGO_EXP","oro")
write.csv(CODIGO_ORO,file="CreatedData/GIS/Codigo_Oro.csv",row.names=F)
save(TMC,file="CreatedData/TMC.RData")
write.csv(TMC,file="CreatedData/TMC.csv",row.names=F)
save(MINAS_ORO,file="CreatedData/MINAS_ORO.RData")



## Closest river to mines 
COL_riv_proj = readOGR("RawData/GIS", "COL_riv_proj")
COL_riv_proj <- spTransform(COL_riv_proj, CRS(projectionAll))
load("CreatedData/MINAS_ORO.RData")
MatrizDistancia=gDistance(MINAS_ORO, COL_riv_proj,T)
EdgeNearestMine=apply(MatrizDistancia,2,which.min)
Distancia=apply(MatrizDistancia,2,min)
MINAS_ORO$EdgeDump=COL_riv_proj@data$id_edge[EdgeNearestMine]
MINAS_ORO$DistDump=Distancia
save(MINAS_ORO,file="CreatedData/MINAS_ORO_Dump.RData")

###################

 load("CreatedData/RasterizedStuff.RData") 
 load("CreatedData/MINAS_ORO.RData")
 load("CreatedData/COL_riv_proj.RData")
 load('CreatedData/Municipios_GIS.RData')
availableCores<-detectCores()-1
no_cores <- max(1, availableCores)
# Initiate cluster
cl <- makeCluster(no_cores)
registerDoParallel(cl)
#set seed for reproducible results
clusterSetRNGStream(cl,123)  
  ############ PARA AREA
  ## Get the names of the files in the folder AreaFechas that start with MinasActivasFecha
ArchiviosMinasActivas=dir("CreatedData/AreaFechas/", pattern="MinasActivasFecha_", full.names = TRUE)
## Sort the names list
ArchiviosMinasActivas=sort(ArchiviosMinasActivas) 

# MuniRaster=resample(MuniRaster, RasterPop)
  foreach(archivo=ArchiviosMinasActivas,.packages=c("sp","raster","rgeos","foreign")) %dopar% {
      #Select the mines
    fechatable=read.dbf(archivo)
    goldfile_merge=merge(MINAS_ORO,fechatable)
    goldfile_merge = subset(goldfile_merge, goldfile_merge@data$Activa ==1)
    goldfile_merge=goldfile_merge[,c("CODIGO_EXP","area_m2","Activa")]
    goldfile_buffer1 <- gBuffer(goldfile_merge, width=5*1000,byid=T) #units are in mts
    goldfile_buffer2 <- gBuffer(goldfile_merge, width=10*1000,byid=T) #units are in mts
    goldfile_buffer3 <- gBuffer(goldfile_merge, width=20*1000,byid=T) #units are in mts
    RasterGold1=rasterize(goldfile_buffer1,RasterPop,field="area_m2",background=0)
    RasterGold2=rasterize(goldfile_buffer2,RasterPop,field="area_m2",background=0)
    RasterGold3=rasterize(goldfile_buffer3,RasterPop,field="area_m2",background=0)
    RasterExposure1 <- overlay(RasterGold1, RasterPop, fun=function(x,y){return(x*y)})
    RasterExposure2 <- overlay(RasterGold2, RasterPop, fun=function(x,y){return(x*y)})
    RasterExposure3 <- overlay(RasterGold3, RasterPop, fun=function(x,y){return(x*y)}) 
    RasterExposure=brick(RasterExposure1,RasterExposure2,RasterExposure3)
    MuniExposure=zonal(RasterExposure, MuniRaster, fun='sum')
    MuniExposure[,c(2:4)]=MuniExposure[,(2:4)]/PopMuniRaster[,c(2,2,2)]
    HospExposureSingle=zonal(RasterExposure, HospRaster, fun='sum')
    HospExposureSingle[,c(2:4)]=HospExposureSingle[,(2:4)]/PopHospRaster[,c(2,2,2)]    
    #MuniExposure2=zonal(RasterExposure, MuniRaster2, fun='mean')
    colnames(MuniExposure)=c("CODANE2","Buffer5km","Buffer10km","Buffer20km")
    colnames(HospExposureSingle)=c("ID_HOSP_INTERNO","Buffer5km","Buffer10km","Buffer20km")
    MergeTemp=merge(HospExposureSingle,Hospitales_points_dataframe_unique,by="ID_HOSP_INTERNO")
    MergeTemp=MergeTemp[,c("coords.x1","coords.x2","Buffer5km","Buffer10km","Buffer20km")]
    MergeTemp=merge(MergeTemp,Hospitales_points_dataframe[,c("cod_inst","coords.x1","coords.x2")])
    HospExposure=MergeTemp[,c("cod_inst","Buffer5km","Buffer10km","Buffer20km")]
      
    #####Maps for exposure ##################
    secuencia=gsub("\\.dbf","",gsub("CreatedData/AreaFechas/MinasActivasFecha_","",archivo))
    pdf(paste0("Results/Maps/RasterPoblacion",secuencia,".pdf"))
    CropRasterPop=crop(RasterPop,gBuffer(goldfile_merge, width=50*1000,byid=T))
    plot(extent(CropRasterPop),bty="n",axes=F,xlab="",ylab="")
    plot(CropRasterPop,add=T)
    plot(goldfile_buffer1,add=T,lwd=2,lty=2,border=2)
    plot(goldfile_merge,add=T,lwd=2,border=2)
    plot(crop(Municipios,extent(CropRasterPop)),lwd=2,add=T)
    plot(crop(COL_riv_proj,extent(CropRasterPop)),add=T,col=4,lwd=2)
    text(coordinates(Municipios), labels=Municipios$NAME_2,cex=1.5)
    dev.off()
    
    pdf(paste0("Results/Maps/RasterExposure",secuencia,".pdf"))
    CropRasterPop=crop(RasterExposure1,gBuffer(goldfile_merge, width=50*1000,byid=T))
    plot(extent(CropRasterPop),bty="n",axes=F,xlab="",ylab="")
    plot(CropRasterPop,add=T)
    plot(goldfile_buffer1,add=T,lwd=2,lty=2,border=2)
    plot(goldfile_merge,add=T,lwd=2,border=2)
    plot(crop(Municipios,extent(CropRasterPop)),lwd=2,add=T)
    plot(crop(COL_riv_proj,extent(CropRasterPop)),add=T,col=4,lwd=2)
    text(coordinates(Municipios), labels=Municipios$NAME_2,cex=1.5)
    dev.off()
    ######################
    
    
      
    save(MuniExposure,file=paste0("CreatedData/AreaFechas/AreaExpMuniFecha_",gsub(".dbf","",gsub("CreatedData/AreaFechas/MinasActivasFecha_","",archivo)),"_All.RData"))
    #RasterExposure3=setExtent(RasterExposure3, MuniRaster)
    #Ret=zonal(RasterExposure3, MuniRaster, fun=mean)
     save(HospExposure,file=paste0("CreatedData/AreaFechas/AreaExpHospFecha_",gsub(".dbf","",gsub("CreatedData/AreaFechas/MinasActivasFecha_","",archivo)),"_All.RData"))
    gc()
    rm(goldfile_merge,fechatable,goldfile_buffer1,goldfile_buffer2,goldfile_buffer3,RasterGold1,RasterGold2,RasterGold3,RasterExposure1,RasterExposure2,RasterExposure3,RasterExposure,MuniExposure, HospExposure)

  }

  
  ############ PARA PRODUCCION
  
ArchiviosMinasActivas=dir("CreatedData/AreaFechas/", pattern="^MinasActivasProdFecha_", full.names = TRUE)
ArchiviosMinasActivas=sort(ArchiviosMinasActivas) 



  foreach(archivo=ArchiviosMinasActivas,.packages=c("sp","raster","rgeos","foreign")) %dopar% {
      #Select the mines
    fechatable=read.dbf(archivo)
    goldfile_merge=merge(MINAS_ORO,fechatable)
    goldfile_merge = subset(goldfile_merge, !is.na(goldfile_merge@data$Produccion))
    goldfile_merge=goldfile_merge[,c("CODIGO_EXP","area_m2","Produccion")]
    goldfile_buffer1 <- gBuffer(goldfile_merge, width=5*1000,byid=T) #units are in mts
    goldfile_buffer2 <- gBuffer(goldfile_merge, width=10*1000,byid=T) #units are in mts
    goldfile_buffer3 <- gBuffer(goldfile_merge, width=20*1000,byid=T) #units are in mts
    RasterGold1=rasterize(goldfile_buffer1,RasterPop,field="Produccion",background=0)
    RasterGold2=rasterize(goldfile_buffer2,RasterPop,field="Produccion",background=0)
    RasterGold3=rasterize(goldfile_buffer3,RasterPop,field="Produccion",background=0)    
    RasterExposure1 <- overlay(RasterGold1, RasterPop, fun=function(x,y){return(x*y)})
    RasterExposure2 <- overlay(RasterGold2, RasterPop, fun=function(x,y){return(x*y)})
    RasterExposure3 <- overlay(RasterGold3, RasterPop, fun=function(x,y){return(x*y)}) 
    RasterExposure=brick(RasterExposure1,RasterExposure2,RasterExposure3)
    MuniExposure=zonal(RasterExposure, MuniRaster, fun='sum')
    MuniExposure[,c(2:4)]=MuniExposure[,(2:4)]/PopMuniRaster[,c(2,2,2)]
    HospExposureSingle=zonal(RasterExposure, HospRaster, fun='sum')
    HospExposureSingle[,c(2:4)]=HospExposureSingle[,(2:4)]/PopHospRaster[,c(2,2,2)]
    colnames(MuniExposure)=c("CODANE2","Buffer5km","Buffer10km","Buffer20km")
    colnames(HospExposureSingle)=c("ID_HOSP_INTERNO","Buffer5km","Buffer10km","Buffer20km")
    MergeTemp=merge(HospExposureSingle,Hospitales_points_dataframe_unique,by="ID_HOSP_INTERNO")
    MergeTemp=MergeTemp[,c("coords.x1","coords.x2","Buffer5km","Buffer10km","Buffer20km")]
    MergeTemp=merge(MergeTemp,Hospitales_points_dataframe[,c("cod_inst","coords.x1","coords.x2")])
    HospExposure=MergeTemp[,c("cod_inst","Buffer5km","Buffer10km","Buffer20km")]
    
    save(MuniExposure,file=paste0("CreatedData/AreaFechas/ProdExpMuniFecha_",gsub(".dbf","",gsub("CreatedData/AreaFechas/MinasActivasProdFecha_","",archivo)),"_All.RData"))
    save(HospExposure,file=paste0("CreatedData/AreaFechas/ProdExpHospFecha_",gsub(".dbf","",gsub("CreatedData/AreaFechas/MinasActivasProdFecha_","",archivo)),"_All.RData"))
    gc()
    rm(goldfile_merge,fechatable,goldfile_buffer1,goldfile_buffer2,goldfile_buffer3,RasterGold1,RasterGold2,RasterGold3,RasterExposure1,RasterExposure2,RasterExposure3,RasterExposure,MuniExposure, HospExposureSingle,MergeTemp,HospExposure)

  }
  
  
  
   ############ PARA PRODUCCION  A LA ANA MARIA IBANEZ
  
ArchiviosMinasActivas=dir("CreatedData/AreaFechas/", pattern="AI_MinasActivasProdFecha_", full.names = TRUE)
ArchiviosMinasActivas=sort(ArchiviosMinasActivas) 



  foreach(archivo=ArchiviosMinasActivas,.packages=c("sp","raster","rgeos","foreign")) %dopar% {
      #Select the mines
    fechatable=read.dbf(archivo)
    goldfile_merge=merge(MINAS_ORO,fechatable)
    goldfile_merge = subset(goldfile_merge, !is.na(goldfile_merge@data$Produccion))
    goldfile_merge=goldfile_merge[,c("CODIGO_EXP","area_m2","Produccion")]
    goldfile_buffer1 <- gBuffer(goldfile_merge, width=5*1000,byid=T) #units are in mts
    goldfile_buffer2 <- gBuffer(goldfile_merge, width=10*1000,byid=T) #units are in mts
    goldfile_buffer3 <- gBuffer(goldfile_merge, width=20*1000,byid=T) #units are in mts
    RasterGold1=rasterize(goldfile_buffer1,RasterPop,field="Produccion",background=0)
    RasterGold2=rasterize(goldfile_buffer2,RasterPop,field="Produccion",background=0)
    RasterGold3=rasterize(goldfile_buffer3,RasterPop,field="Produccion",background=0)    
    RasterExposure1 <- overlay(RasterGold1, RasterPop, fun=function(x,y){return(x*y)})
    RasterExposure2 <- overlay(RasterGold2, RasterPop, fun=function(x,y){return(x*y)})
    RasterExposure3 <- overlay(RasterGold3, RasterPop, fun=function(x,y){return(x*y)}) 
    RasterExposure=brick(RasterExposure1,RasterExposure2,RasterExposure3)
    MuniExposure=zonal(RasterExposure, MuniRaster, fun='sum')
    MuniExposure[,c(2:4)]=MuniExposure[,(2:4)]/PopMuniRaster[,c(2,2,2)]
    HospExposureSingle=zonal(RasterExposure, HospRaster, fun='sum')
    HospExposureSingle[,c(2:4)]=HospExposureSingle[,(2:4)]/PopHospRaster[,c(2,2,2)]
    colnames(MuniExposure)=c("CODANE2","Buffer5km","Buffer10km","Buffer20km")
    colnames(HospExposureSingle)=c("ID_HOSP_INTERNO","Buffer5km","Buffer10km","Buffer20km")
    MergeTemp=merge(HospExposureSingle,Hospitales_points_dataframe_unique,by="ID_HOSP_INTERNO")
    MergeTemp=MergeTemp[,c("coords.x1","coords.x2","Buffer5km","Buffer10km","Buffer20km")]
    MergeTemp=merge(MergeTemp,Hospitales_points_dataframe[,c("cod_inst","coords.x1","coords.x2")])
    HospExposure=MergeTemp[,c("cod_inst","Buffer5km","Buffer10km","Buffer20km")]
    
    save(MuniExposure,file=paste0("CreatedData/AreaFechas/AI_ProdExpMuniFecha_",gsub(".dbf","",gsub("CreatedData/AreaFechas/AI_MinasActivasProdFecha_","",archivo)),"_All.RData"))
    save(HospExposure,file=paste0("CreatedData/AreaFechas/AI_ProdExpHospFecha_",gsub(".dbf","",gsub("CreatedData/AreaFechas/AI_MinasActivasProdFecha_","",archivo)),"_All.RData"))
    gc()
    rm(goldfile_merge,fechatable,goldfile_buffer1,goldfile_buffer2,goldfile_buffer3,RasterGold1,RasterGold2,RasterGold3,RasterExposure1,RasterExposure2,RasterExposure3,RasterExposure,MuniExposure, HospExposureSingle,MergeTemp,HospExposure)

  }
  
  stopCluster(cl)
stopImplicitCluster()
  
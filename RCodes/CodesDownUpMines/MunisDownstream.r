edge_master=read.dta("CreatedData/GIS/edge_master.dta")
edge_master=edge_master[which(duplicated(edge_master$id_edge)==0),]
MinaAreaDump=read.dbf("CreatedData/GIS/Mine_near_edge.dbf")
if(length(which(MinaAreaDump$NEAR_FID==-1))!=0) MinaAreaDump=MinaAreaDump[-which(MinaAreaDump$NEAR_FID==-1),]
vertex_river=read.dta("CreatedData/GIS/vertex_river.dta")
Municipios=read.dbf("CreatedData/GIS/Municipios.dbf")
gold_mines=read.dbf("CreatedData/GIS/gold_mines.dbf")
colnames(MinaAreaDump)=c("gold_id","NEAR_FID","NEAR_DIST")
MinaAreaDump=merge(MinaAreaDump,gold_mines)


MinaAreaDump$FECHA_INSC=as.Date(MinaAreaDump$FECHA_INSC,format="%m/%d/%Y")
MinaAreaDump$FECHA_TERM=as.Date(MinaAreaDump$FECHA_TERM,format="%m/%d/%Y")
INDEX_FECHA=which(MinaAreaDump$FECHA_INSC<as.Date("01-01-2015",format="%d-%m-%Y"))
MinaAreaDump=MinaAreaDump[INDEX_FECHA,]
INDEX_FECHA2=which(MinaAreaDump$FECHA_TERM>as.Date("01-01-1998",format="%d-%m-%Y"))
MinaAreaDump=MinaAreaDump[INDEX_FECHA2,]
#MinaAreaDump=rename(MinaAreaDump, c("area_sqkm"="Areakm2"))
MinaAreaDump$Areakm2=MinaAreaDump$area_m2/(1000^2)
MinaAreaDump$Areakm2=as.numeric(as.character(MinaAreaDump$Areakm2))
MinaAreaDump$NEAR_DIST=as.numeric(as.character(MinaAreaDump$NEAR_DIST))
MinaAreaDump=subset(MinaAreaDump,select=c(NEAR_FID,NEAR_DIST,Areakm2,FECHA_INSC,FECHA_TERM,CODIGO_EXP))
MinaAreaDump$FECHA_INSC=as.Date(MinaAreaDump$FECHA_INSC,format="%m/%d/%Y")
MinaAreaDump$FECHA_TERM=as.Date(MinaAreaDump$FECHA_TERM,format="%m/%d/%Y")

#A cada mina le voy a asignar los 15 municipios mas cercanos que esten downstream y los 15 que esten upstream...
MunisDownstream=list()
MunisUpstream=list()
for(i in 1:dim(MinaAreaDump)[1]){
#for(i in 1:100){
cod_mina=as.character(MinaAreaDump$CODIGO_EXP[i])
edge_mina=edge_master[which(edge_master$id_edge==MinaAreaDump$NEAR_FID[i]),] 
MunisDownstream[[cod_mina]]=c(MunisDownstream[[cod_mina]],as.character(Municipios$CODANE2[match(edge_mina$near_muniid_start,Municipios$id)]),
as.character(Municipios$CODANE2[match(edge_mina$near_muniid_end,Municipios$id)]))
    while(length(MunisDownstream[[cod_mina]])<10){
    edge_mina=edge_master[which(edge_master$starid==edge_mina$endid),]
    if(dim(edge_mina)[1]==1){
    MunisDownstream[[cod_mina]]=c(MunisDownstream[[cod_mina]],as.character(Municipios$CODANE2[match(edge_mina$near_muniid_start,Municipios$id)]),
    as.character(Municipios$CODANE2[match(edge_mina$near_muniid_end,Municipios$id)]))
    }
    if(dim(edge_mina)[1]==0){
    break;
    }    
    }#while
   
  edge_mina=edge_master[match(MinaAreaDump$NEAR_FID[i],edge_master$id_edge),]
  MunisUpstream[[cod_mina]]=c(MunisUpstream[[cod_mina]],as.character(Municipios$CODANE2[match(edge_mina$near_muniid_end,Municipios$id)]),
  as.character(Municipios$CODANE2[match(edge_mina$near_muniid_start,Municipios$id)]))

    while(length(MunisUpstream[[cod_mina]])<10){
#       edge_mina=edge_master[which(edge_master$endid==edge_mina$starid),]
    edge_mina=edge_master[which(!is.na(match(edge_master$endid,edge_mina$starid))),]
    edge_mina=edge_mina[complete.cases(edge_mina),]
      if(dim(edge_mina)[1]>1){
      MunisUpstream[[cod_mina]]=c(MunisUpstream[[cod_mina]],as.character(Municipios$CODANE2[match(edge_mina$near_muniid_end,Municipios$id)]),
      as.character(Municipios$CODANE2[match(edge_mina$near_muniid_start,Municipios$id)]))
      }
      if(dim(edge_mina)[1]==0){
      break;
      }
    }#while     
}#close for



save(MunisDownstream,MunisUpstream,file="CreatedData/MunisUpandDown.RData")

##############
################AREA MUNI MADRE 5KM
###############################
load("CreatedData/MatrizExpoUpstreamHosp.RData")
nombresVars=c("UpstreamAreaHosp5KM","UpstreamAreaHosp10KM","UpstreamAreaHosp20KM")
load('CreatedData/DataCompletaIntensity.RData')
CodigosHospitales=as.numeric(CodigosHospitales)
IndexSinHosp=which(is.na(DataCompleta$COD_INST))
IndexConHosp=which(!(is.na(DataCompleta$COD_INST)))
DataCompletaSinHospital=DataCompleta[IndexSinHosp,]
DataCompleta=DataCompleta[IndexConHosp,]

for(i in 1:3){
MatrizExpo=MatrizExpoUpstreamHosp[,,i]
MatrizExpo=t(MatrizExpo)
MunicipiosSinMineria=setdiff(unique(DataCompleta$COD_INST), CodigosHospitales[which(rowSums(MatrizExpo)!=0)])
IndexSinMineria=which(DataCompleta$COD_INST %in%  MunicipiosSinMineria)
DataCompleta[,nombresVars[i]]=NA
DataCompletaSinHospital[,nombresVars[i]]=NA
DataCompleta[IndexSinMineria,nombresVars[i]]=0
IndexConMineria=setdiff(1:dim(DataCompleta)[1],IndexSinMineria)

DataCompletaSinMineria=DataCompleta[IndexSinMineria,]
DataCompletaMineria=DataCompleta[IndexConMineria,]
rm(DataCompleta)
PosiblesNac=unique(DataCompletaMineria[,"FECHA_NAC"])
for(fecha_nac in PosiblesNac){
#temp=Sys.time()
INDEXOBS=which(DataCompletaMineria[,"FECHA_NAC"]==fecha_nac)
t_0=max(which(FechasVec<=(fecha_nac-280)))
t_f=max(which(FechasVec<(fecha_nac)))
FechasRelevantes=c(FechasVec[t_0:t_f],fecha_nac)
pesos=as.numeric(diff(FechasRelevantes))
CodigosTotales=DataCompletaMineria$COD_INST[INDEXOBS]
CodigosUnicos=unique(CodigosTotales)
MatrizRelevante=MatrizExpo[match(CodigosUnicos,CodigosHospitales),t_0:t_f]
if(is.null(dim(MatrizRelevante)))  MatrizRelevante=matrix(MatrizRelevante,nrow=1)
DataCompletaMineria[INDEXOBS,nombresVars[i]]=apply(MatrizRelevante,1,weighted.mean,w=pesos)[match(CodigosTotales,CodigosUnicos)]
}
DataCompleta=rbind(DataCompletaSinMineria,DataCompletaMineria)
}
save(DataCompleta,file='CreatedData/DataCompletaIntensity.RData')



##############
################ PROD MUNI MADRE 5KM
###############################
load("CreatedData/MatrizProdUpstreamHosp.RData")
nombresVars=c("UpstreamProdHosp5KM","UpstreamProdHosp10KM","UpstreamProdHosp20KM")
for(i in 1:3){
MatrizExpoProd=MatrizProdUpstreamHosp[,,i]
MatrizExpoProd=t(MatrizExpoProd)
MunicipiosSinMineria=setdiff(unique(DataCompleta$COD_INST), CodigosHospitales[which(rowSums(MatrizExpoProd)!=0)])
IndexSinMineria=which(DataCompleta$COD_INST %in%  MunicipiosSinMineria)
DataCompleta[,nombresVars[i]]=NA
DataCompletaSinHospital[,nombresVars[i]]=NA
DataCompleta[intersect(IndexSinMineria,which(DataCompleta$FECHA_NAC>as.Date("2001-10-08",format="%Y-%m-%d"))),nombresVars[i]]=0
IndexConMineria=setdiff(1:dim(DataCompleta)[1],IndexSinMineria)

DataCompletaSinMineria=DataCompleta[IndexSinMineria,]
DataCompletaMineria=DataCompleta[IndexConMineria,]
rm(DataCompleta)
PosiblesNac=unique(DataCompletaMineria[,"FECHA_NAC"])
PosiblesNac=PosiblesNac[PosiblesNac>as.Date("2001-10-08",format="%Y-%m-%d")]
for(fecha_nac in PosiblesNac){
#temp=Sys.time()
INDEXOBS=which(DataCompletaMineria[,"FECHA_NAC"]==fecha_nac)
t_0=max(which(FechasVec<=(fecha_nac-280)))
t_f=max(which(FechasVec<(fecha_nac)))
FechasRelevantes=c(FechasVec[t_0:t_f],fecha_nac)
pesos=as.numeric(diff(FechasRelevantes))
CodigosTotales=DataCompletaMineria$COD_INST[INDEXOBS]
CodigosUnicos=unique(CodigosTotales)
MatrizRelevante=MatrizExpoProd[match(CodigosUnicos,CodigosHospitales),t_0:t_f]
if(is.null(dim(MatrizRelevante)))  MatrizRelevante=matrix(MatrizRelevante,nrow=1)
DataCompletaMineria[INDEXOBS,nombresVars[i]]=  apply(MatrizRelevante,1,weighted.mean,w=pesos)[match(CodigosTotales,CodigosUnicos)]
}
DataCompleta=rbind(DataCompletaSinMineria,DataCompletaMineria)
}
DataCompleta=rbind(DataCompleta,DataCompletaSinHospital)



##############
################ PROD A LA AI MUNI MADRE 5KM
###############################
load("CreatedData/AI_MatrizProdUpstreamHosp.RData")
nombresVars=c("AI_UpstreamProdHosp5KM","AI_UpstreamProdHosp10KM","AI_UpstreamProdHosp20KM")
for(i in 1:3){
MatrizExpoProd=MatrizProdUpstreamHosp[,,i]
MatrizExpoProd=t(MatrizExpoProd)
MunicipiosSinMineria=setdiff(unique(DataCompleta$COD_INST), CodigosHospitales[which(rowSums(MatrizExpoProd)!=0)])
IndexSinMineria=which(DataCompleta$COD_INST %in%  MunicipiosSinMineria)
DataCompleta[,nombresVars[i]]=NA
DataCompletaSinHospital[,nombresVars[i]]=NA
DataCompleta[intersect(IndexSinMineria,which(DataCompleta$FECHA_NAC>as.Date("2001-10-08",format="%Y-%m-%d"))),nombresVars[i]]=0
IndexConMineria=setdiff(1:dim(DataCompleta)[1],IndexSinMineria)

DataCompletaSinMineria=DataCompleta[IndexSinMineria,]
DataCompletaMineria=DataCompleta[IndexConMineria,]
rm(DataCompleta)
PosiblesNac=unique(DataCompletaMineria[,"FECHA_NAC"])
PosiblesNac=PosiblesNac[PosiblesNac>as.Date("2001-10-08",format="%Y-%m-%d")]
for(fecha_nac in PosiblesNac){
#temp=Sys.time()
INDEXOBS=which(DataCompletaMineria[,"FECHA_NAC"]==fecha_nac)
t_0=max(which(FechasVec<=(fecha_nac-280)))
t_f=max(which(FechasVec<(fecha_nac)))
FechasRelevantes=c(FechasVec[t_0:t_f],fecha_nac)
pesos=as.numeric(diff(FechasRelevantes))
CodigosTotales=DataCompletaMineria$COD_INST[INDEXOBS]
CodigosUnicos=unique(CodigosTotales)
MatrizRelevante=MatrizExpoProd[match(CodigosUnicos,CodigosHospitales),t_0:t_f]
if(is.null(dim(MatrizRelevante)))  MatrizRelevante=matrix(MatrizRelevante,nrow=1)
DataCompletaMineria[INDEXOBS,nombresVars[i]]=  apply(MatrizRelevante,1,weighted.mean,w=pesos)[match(CodigosTotales,CodigosUnicos)]
}
DataCompleta=rbind(DataCompletaSinMineria,DataCompletaMineria)
}
DataCompleta=rbind(DataCompleta,DataCompletaSinHospital)
save(DataCompleta,file='CreatedData/DataCompletaIntensity.RData')

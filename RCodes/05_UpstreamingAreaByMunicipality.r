##This area calculates the mined area upstream... its unclear what the discount factor does...Need to fix this!

load("CreatedData/MINAS_ORO_Dump.RData")
load("CreatedData/Municipios_GIS.RData")
load("CreatedData/COL_riv_proj.RData")
MinaAreaDump=MINAS_ORO[,c("EdgeDump","DistDump","area_km2","FECHA_INSC","FECHA_TERM","CODIGO_EXP")]
MinaAreaDump$FECHA_INSC=as.Date(MinaAreaDump$FECHA_INSC,format="%m/%d/%Y")
MinaAreaDump$FECHA_TERM=as.Date(MinaAreaDump$FECHA_TERM,format="%m/%d/%Y")
TasaDesc=0.76
 
FechasVec=sort(unique(c(MinaAreaDump$FECHA_INSC,MinaAreaDump$FECHA_TERM)))
INDEX_FECHAS_RELEVANTES= which(FechasVec<as.Date("01-01-2015",format="%d-%m-%Y"))
FechasVec=FechasVec[INDEX_FECHAS_RELEVANTES]

FIDEdgeVec=sort(COL_riv_proj$id_edge)
COL_riv_proj=COL_riv_proj[sort.int(COL_riv_proj$id_edge,index.return=T)$ix,]
AreaMinadaFIDTiempo=matrix(0,ncol=length(FechasVec),nrow=length(FIDEdgeVec))


##Here we calculate which edges dont have a predecesor, and therefore is where a river begins!
SalidasAlMar=setdiff(COL_riv_proj$id_end_vertex, COL_riv_proj$id_start_vertex)
ComienzoRio=setdiff(COL_riv_proj$id_start_vertex, COL_riv_proj$id_end_vertex)
FIDComienzo=COL_riv_proj$id_edge[match(ComienzoRio,COL_riv_proj$id_start_vertex)]


#Here we calculate in each piece of river how many mines (area) are dumping their shit... 
for( fecha in FechasVec){
MinaAreaDumpFecha=MinaAreaDump[which(MinaAreaDump$FECHA_INSC<=fecha & MinaAreaDump$FECHA_TERM>fecha),]
AreaMinadaFechaFid=aggregate(MinaAreaDumpFecha$area_km2,by=list(MinaAreaDumpFecha$EdgeDump),FUN=sum)
AreaMinadaFIDTiempo[match(AreaMinadaFechaFid[,1],FIDEdgeVec),which(FechasVec==fecha)]=AreaMinadaFechaFid[,2]
}

#We create an empty matrix to store the results
AreaMinadaFIDTiempoAcumulada=matrix(NA,ncol=length(FechasVec),nrow=length(FIDEdgeVec))
AreaMinadaFIDTiempoAcumulada[match(FIDComienzo,FIDEdgeVec),]=AreaMinadaFIDTiempo[match(FIDComienzo,FIDEdgeVec),]



#This is the key function. It basically fills the upstream matrix, by looking intro predecesors and adding their polution (times the discount factor)

    
load("CreatedData/listEdgeDistMatAccumulada.RData")
load("CreatedData/listEdgeDistMat.RData")    
EncontrarPolucionArriba=function(id_edge,UpstreamIDs){
id_edge_pos=id_edge+1 #Esto es por que sortee la base para que fuera asi y nos ahorramos el match(id_edge,FIDEdgeVec)
if(is.na(AreaMinadaFIDTiempoAcumulada[id_edge_pos,1])){
  AreaAcum=AreaMinadaFIDTiempo[id_edge_pos,]
  FidsArriba=listEdgeDistMat_Upstream[[id_edge_pos]][,1]
  FidsArriba=FidsArriba[which(FidsArriba %in% UpstreamIDs)]
  for(fidA in FidsArriba){
    AreaAcum=AreaAcum+EncontrarPolucionArriba(fidA,UpstreamIDs)
  }
  return(AreaAcum)
}
else{
  return(AreaMinadaFIDTiempoAcumulada[id_edge_pos,])
}
}



#Now we apply the function
for( indexFid in setdiff(FIDEdgeVec,FIDComienzo)){
UpstreamInfo=listEdgeDistMatAccumulada_Upstream[[indexFid+1]]
UpstreamInfo=matrix(UpstreamInfo,ncol=3)
UpstreamInfo=UpstreamInfo[which(UpstreamInfo[,2]<max_dist),]
UpstreamInfo=matrix(UpstreamInfo,ncol=3)
AreaMinadaFIDTiempoAcumulada[indexFid+1,]=EncontrarPolucionArriba(indexFid,UpstreamInfo[,1])
}


AreaMinadaFIDTiempoUpstream=AreaMinadaFIDTiempoAcumulada-AreaMinadaFIDTiempo

#Lets plot it to make sure everything is fine! yes it looks fine!
save(FechasVec,AreaMinadaFIDTiempoUpstream,AreaMinadaFIDTiempoAcumulada,AreaMinadaFIDTiempo,file="CreatedData/AreaMinadaUpstream.RData")
matplot(t(AreaMinadaFIDTiempoAcumulada)-t(AreaMinadaFIDTiempo),type="l",lty=1,x=FechasVec)


#Now we create a single file for each date... this was done for when we used ArcGIS, but we keep it here for clarity
load(file="CreatedData/AreaMinadaUpstream.RData")
FechasVec=sort(unique(c(MinaAreaDump$FECHA_INSC,MinaAreaDump$FECHA_TERM)))
INDEX_FECHAS_RELEVANTES= which(FechasVec<as.Date("01-01-2015",format="%d-%m-%Y"))
FechasVec=FechasVec[INDEX_FECHAS_RELEVANTES]
FIDEdgeVec=sort(COL_riv_proj$id_edge)
COL_riv_proj=COL_riv_proj[sort.int(COL_riv_proj$id_edge,index.return=T)$ix,]
INDEX_SUCIO=rep(0,length(FIDEdgeVec))
for(fecha in FechasVec){
Archivo=cbind(FIDEdgeVec,AreaMinadaFIDTiempoAcumulada[,which(FechasVec==fecha)])
Archivo=data.frame(Archivo)
Archivo=Archivo[complete.cases(Archivo),]
INDEX_SUCIO[which(Archivo[,2]>0)]=1
colnames(Archivo)=c("id_edge","polucion")
Archivo=Archivo[which(Archivo$polucion>0),]
write.dbf(Archivo,file=paste("CreatedData/AreaUpstreamRiver/AreaMinadaEdgeFecha_",which(FechasVec==fecha),".dbf",sep=""))
}
##We dont need this anymore... I think
#INDEX_SUCIO=cbind(FIDEdgeVec,INDEX_SUCIO)
#colnames(INDEX_SUCIO)=c("id_edge","sucio")                                                     
#write.csv(INDEX_SUCIO,file=paste("CreatedData/AreaUpstreamRiver/INDEX_SUCIO.csv",sep=""),row.names=F)


 ##This area calculates the production (really the mercury)  upstream... discount factor here is very clear...

load("CreatedData/MINAS_ORO_Dump.RData")
load("CreatedData/Municipios_GIS.RData")
load("CreatedData/COL_riv_proj.RData")

emis_Hg_Au_muni=read.csv("RawData/emis_Hg_Au_muni.csv")
load("CreatedData/ProdPerMine.RData")
colnames(emis_Hg_Au_muni)=c("CODANE2","HG_AUG_FACT")
ProdPerMine=merge(emis_Hg_Au_muni,ProdPerMine)
FactorEmisionAgua=0.4
ProdPerMine$EmisionMercurio=ProdPerMine$Produccion*ProdPerMine$HG_AUG_FACT*FactorEmisionAgua
ProdPerMine=data.table(ProdPerMine)
ProdAccPerMine=ProdPerMine[,list(CODIGO_EXP=CODIGO_EXP,  Produccion=sum(Produccion), Fecha=Fecha, EmisionMercurio=sum(EmisionMercurio)),by=c("CODIGO_EXP","Fecha")]
ProdAccPerMine<-ProdAccPerMine[order(Fecha),list(Produccion=Produccion,EmisionMercurio=EmisionMercurio,EmisionAcc=cumsum(EmisionMercurio),Fecha=Fecha),by=c("CODIGO_EXP")]

##The code below makes me think stuff is looking good so far...
#require(ggplot2)
#ggplot(ProdAccPerMine, aes(Fecha, EmisionAcc,colour=CODIGO_EXP)) + 
#    geom_line() + 
#    geom_point()

MinaProdDump=merge(ProdAccPerMine,MINAS_ORO@data[,c("CODIGO_EXP","EdgeDump","DistDump")],by="CODIGO_EXP")


MinaProdDump$Fecha=as.Date(MinaProdDump$Fecha,format="%Y-%m-%d")

FechasVec=sort(unique(MinaProdDump$Fecha))
INDEX_FECHAS_RELEVANTES= which(FechasVec<as.Date("01-01-2015",format="%d-%m-%Y"))
FechasVec=FechasVec[INDEX_FECHAS_RELEVANTES]

FIDEdgeVec=sort(COL_riv_proj$id_edge)
COL_riv_proj=COL_riv_proj[sort.int(COL_riv_proj$id_edge,index.return=T)$ix,]
ProdMinadaFIDTiempo=matrix(0,ncol=length(FechasVec),nrow=length(FIDEdgeVec))


##Here we calculate which edges dont have a predecesor, and therefore is where a river begins!
SalidasAlMar=setdiff(COL_riv_proj$id_end_vertex, COL_riv_proj$id_start_vertex)
ComienzoRio=setdiff(COL_riv_proj$id_start_vertex, COL_riv_proj$id_end_vertex)
FIDComienzo=COL_riv_proj$id_edge[match(ComienzoRio,COL_riv_proj$id_start_vertex)]



for( fecha in FechasVec){
MinaProdDumpFecha=MinaProdDump[which(MinaProdDump$Fecha==fecha),]
ProdMinadaFechaFid=aggregate(MinaProdDumpFecha$EmisionAcc,by=list(MinaProdDumpFecha$EdgeDump),FUN=sum)
ProdMinadaFIDTiempo[match(ProdMinadaFechaFid[,1],FIDEdgeVec),which(FechasVec==fecha)]=ProdMinadaFechaFid[,2]
}

ProdMinadaFIDTiempoAcumulada=matrix(NA,ncol=length(FechasVec),nrow=length(FIDEdgeVec))
ProdMinadaFIDTiempoAcumulada[match(FIDComienzo,FIDEdgeVec),]=ProdMinadaFIDTiempo[match(FIDComienzo,FIDEdgeVec),]


EncontrarPolucionArriba=function(id_edge,UpstreamIDs){
id_edge_pos=id_edge+1 #Esto es por que sortee la base para que fuera asi y nos ahorramos el match(id_edge,FIDEdgeVec)
if(is.na(ProdMinadaFIDTiempoAcumulada[id_edge_pos,1])){
  AreaAcum=ProdMinadaFIDTiempo[id_edge_pos,]
  FidsArriba=listEdgeDistMat_Upstream[[id_edge_pos]][,1]
  FidsArriba=FidsArriba[which(FidsArriba %in% UpstreamIDs)]
  for(fidA in FidsArriba){
    AreaAcum=AreaAcum+EncontrarPolucionArriba(fidA,UpstreamIDs)
  }
  return(AreaAcum)      
}
else{
  return(ProdMinadaFIDTiempoAcumulada[id_edge_pos,])
}
}


load("CreatedData/listEdgeDistMatAccumulada.RData")
load("CreatedData/listEdgeDistMat.RData")
#Now we apply the function
for( indexFid in setdiff(FIDEdgeVec,FIDComienzo)){
UpstreamInfo=listEdgeDistMatAccumulada_Upstream[[indexFid+1]]
UpstreamInfo=matrix(UpstreamInfo,ncol=3)
UpstreamInfo=UpstreamInfo[which(UpstreamInfo[,2]<max_dist),]
UpstreamInfo=matrix(UpstreamInfo,ncol=3)
ProdMinadaFIDTiempoAcumulada[indexFid+1,]=EncontrarPolucionArriba(indexFid,UpstreamInfo[,1])
}






ProdMinadaFIDTiempoUpstream=ProdMinadaFIDTiempoAcumulada-ProdMinadaFIDTiempo


save(FechasVec,ProdMinadaFIDTiempoUpstream,ProdMinadaFIDTiempoAcumulada,ProdMinadaFIDTiempo,file="CreatedData/ProdMinadaUpstream.RData")
matplot(t(ProdMinadaFIDTiempoAcumulada),type="l",lty=1,x=FechasVec)



load(file="CreatedData/ProdMinadaUpstream.RData")
FechasVec=sort(unique(c(MinaProdDump$Fecha)))
INDEX_FECHAS_RELEVANTES= which(FechasVec<as.Date("01-01-2015",format="%d-%m-%Y"))
FechasVec=FechasVec[INDEX_FECHAS_RELEVANTES]
FIDEdgeVec=sort(COL_riv_proj$id_edge)
COL_riv_proj=COL_riv_proj[sort.int(COL_riv_proj$id_edge,index.return=T)$ix,]
INDEX_SUCIO=rep(0,length(FIDEdgeVec))
for(fecha in FechasVec){

Archivo=cbind(FIDEdgeVec,ProdMinadaFIDTiempoAcumulada[,which(FechasVec==fecha)])
Archivo=data.frame(Archivo)
Archivo=Archivo[complete.cases(Archivo),]
INDEX_SUCIO[which(Archivo[,2]>0)]=1
colnames(Archivo)=c("id_edge","polucion")
Archivo=Archivo[which(Archivo$polucion>0),]
write.dbf(Archivo,file=paste("CreatedData/AreaUpstreamRiver/ProdMinadaEdgeFecha_",which(FechasVec==fecha),".dbf",sep=""))
}

##I dont think we need this anymore
#INDEX_SUCIO=cbind(FIDEdgeVec,INDEX_SUCIO)
#colnames(INDEX_SUCIO)=c("id_edge","sucio")                                                     
#write.csv(INDEX_SUCIO,file=paste("CreatedData/AreaUpstreamRiver/INDEX_HG_SUCIO.csv",sep=""),row.names=F)




#################################################
##################################################
############ WE DO THE SAME THING BUT WITH AI

 ##This area calculates the production (really the mercury)  upstream... discount factor here is very clear...

load("CreatedData/MINAS_ORO_Dump.RData")
load("CreatedData/Municipios_GIS.RData")
load("CreatedData/COL_riv_proj.RData")
emis_Hg_Au_muni=read.csv("RawData/emis_Hg_Au_muni.csv")
load("CreatedData/AI_ProdPerMine.RData")
colnames(emis_Hg_Au_muni)=c("CODANE2","HG_AUG_FACT")
ProdPerMine=merge(emis_Hg_Au_muni,ProdPerMine)
FactorEmisionAgua=0.4
Threshold=0.0001
ProdPerMine$EmisionMercurio=ProdPerMine$Produccion*ProdPerMine$HG_AUG_FACT*FactorEmisionAgua
ProdPerMine=data.table(ProdPerMine)
ProdAccPerMine=ProdPerMine[,list(CODIGO_EXP=CODIGO_EXP,  Produccion=sum(Produccion), Fecha=Fecha, EmisionMercurio=sum(EmisionMercurio)),by=c("CODIGO_EXP","Fecha")]
ProdAccPerMine<-ProdAccPerMine[order(Fecha),list(Produccion=Produccion,EmisionMercurio=EmisionMercurio,EmisionAcc=cumsum(EmisionMercurio),Fecha=Fecha),by=c("CODIGO_EXP")]
MinaProdDump=merge(ProdAccPerMine,MINAS_ORO@data[,c("CODIGO_EXP","EdgeDump","DistDump")],by="CODIGO_EXP")


MinaProdDump$Fecha=as.Date(MinaProdDump$Fecha,format="%Y-%m-%d")
##Esto calibra la tasa de descuento tal que 27000 grms de mercurio desaparezcan despues de 25 km... note que 27000gr=27 kg
fun_solve=function(x) Threshold-27000*(x^25)
TasaDesc=uniroot(fun_solve,c(0,1))$root

FechasVec=sort(unique(MinaProdDump$Fecha))
INDEX_FECHAS_RELEVANTES= which(FechasVec<as.Date("01-01-2015",format="%d-%m-%Y"))
FechasVec=FechasVec[INDEX_FECHAS_RELEVANTES]

FIDEdgeVec=sort(COL_riv_proj$id_edge)
COL_riv_proj=COL_riv_proj[sort.int(COL_riv_proj$id_edge,index.return=T)$ix,]
ProdMinadaFIDTiempo=matrix(0,ncol=length(FechasVec),nrow=length(FIDEdgeVec))


##Here we calculate which edges dont have a predecesor, and therefore is where a river begins!
SalidasAlMar=setdiff(COL_riv_proj$id_end_vertex, COL_riv_proj$id_start_vertex)
ComienzoRio=setdiff(COL_riv_proj$id_start_vertex, COL_riv_proj$id_end_vertex)
FIDComienzo=COL_riv_proj$id_edge[match(ComienzoRio,COL_riv_proj$id_start_vertex)]



for( fecha in FechasVec){
MinaProdDumpFecha=MinaProdDump[which(MinaProdDump$Fecha==fecha),]
ProdMinadaFechaFid=aggregate(MinaProdDumpFecha$EmisionAcc,by=list(MinaProdDumpFecha$EdgeDump),FUN=sum)
ProdMinadaFIDTiempo[match(ProdMinadaFechaFid[,1],FIDEdgeVec),which(FechasVec==fecha)]=ProdMinadaFechaFid[,2]
}

ProdMinadaFIDTiempoAcumulada=matrix(NA,ncol=length(FechasVec),nrow=length(FIDEdgeVec))
ProdMinadaFIDTiempoAcumulada[match(FIDComienzo,FIDEdgeVec),]=ProdMinadaFIDTiempo[match(FIDComienzo,FIDEdgeVec),]


EncontrarPolucionArriba=function(id_edge,UpstreamIDs){
id_edge_pos=id_edge+1 #Esto es por que sortee la base para que fuera asi y nos ahorramos el match(id_edge,FIDEdgeVec)
if(is.na(ProdMinadaFIDTiempoAcumulada[id_edge_pos,1])){
  AreaAcum=ProdMinadaFIDTiempo[id_edge_pos,]
  FidsArriba=listEdgeDistMat_Upstream[[id_edge_pos]][,1]
  FidsArriba=FidsArriba[which(FidsArriba %in% UpstreamIDs)]
  for(fidA in FidsArriba){
    AreaAcum=AreaAcum+EncontrarPolucionArriba(fidA,UpstreamIDs)
  }
  return(AreaAcum)      
}
else{
  return(ProdMinadaFIDTiempoAcumulada[id_edge_pos,])
}
}


load("CreatedData/listEdgeDistMatAccumulada.RData")
load("CreatedData/listEdgeDistMat.RData")
#Now we apply the function
for( indexFid in setdiff(FIDEdgeVec,FIDComienzo)){
UpstreamInfo=listEdgeDistMatAccumulada_Upstream[[indexFid+1]]
UpstreamInfo=matrix(UpstreamInfo,ncol=3)
UpstreamInfo=UpstreamInfo[which(UpstreamInfo[,2]<max_dist),]
UpstreamInfo=matrix(UpstreamInfo,ncol=3)
ProdMinadaFIDTiempoAcumulada[indexFid+1,]=EncontrarPolucionArriba(indexFid,UpstreamInfo[,1])
}






ProdMinadaFIDTiempoUpstream=ProdMinadaFIDTiempoAcumulada-ProdMinadaFIDTiempo


save(FechasVec,ProdMinadaFIDTiempoUpstream,ProdMinadaFIDTiempoAcumulada,ProdMinadaFIDTiempo,file="CreatedData/AI_ProdMinadaUpstream.RData")
matplot(t(ProdMinadaFIDTiempoAcumulada),type="l",lty=1,x=FechasVec)



load(file="CreatedData/AI_ProdMinadaUpstream.RData")
FechasVec=sort(unique(c(MinaProdDump$Fecha)))
INDEX_FECHAS_RELEVANTES= which(FechasVec<as.Date("01-01-2015",format="%d-%m-%Y"))
FechasVec=FechasVec[INDEX_FECHAS_RELEVANTES]
FIDEdgeVec=sort(COL_riv_proj$id_edge)
COL_riv_proj=COL_riv_proj[sort.int(COL_riv_proj$id_edge,index.return=T)$ix,]
INDEX_SUCIO=rep(0,length(FIDEdgeVec))
for(fecha in FechasVec){

Archivo=cbind(FIDEdgeVec,ProdMinadaFIDTiempoAcumulada[,which(FechasVec==fecha)])
Archivo=data.frame(Archivo)
Archivo=Archivo[complete.cases(Archivo),]
INDEX_SUCIO[which(Archivo[,2]>0)]=1
colnames(Archivo)=c("id_edge","polucion")
Archivo=Archivo[which(Archivo$polucion>0),]
write.dbf(Archivo,file=paste("CreatedData/AreaUpstreamRiver/AI_ProdMinadaEdgeFecha_",which(FechasVec==fecha),".dbf",sep=""))
}

##I dont think we need this anymore
#INDEX_SUCIO=cbind(FIDEdgeVec,INDEX_SUCIO)
#colnames(INDEX_SUCIO)=c("id_edge","sucio")                                                     
#write.csv(INDEX_SUCIO,file=paste("CreatedData/AreaUpstreamRiver/INDEX_HG_SUCIO.csv",sep=""),row.names=F)



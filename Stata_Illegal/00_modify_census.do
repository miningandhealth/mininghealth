use "$mipath\RawData\CensoMinero\cm_bd_final.dta", clear
gen illegal=tipo_mina-1
sum illegal

gen paga_regalias=. 
replace paga_regalias=0 if aad_02==2
replace paga_regalias=1 if aad_02==1

gen titulo=. 
replace titulo=0 if aad_04==10
replace titulo=1 if aad_04<10

gen aut_ambiental=.
replace aut_ambiental=0 if ao_foyf_16_04==1
replace aut_ambiental=1 if ao_foyf_16_04==2

gen mineral=at_pyr_01
replace mineral=. if at_pyr_01==999
label define mineral 1 "Coal" 2 "Gems" 3 "Metals" 4 "Non-metals"
label values mineral mineral
gen type_metal=.
forval i=1/3{
replace type_metal=`i' if at_pyr_03_01_0`i'==1
}
replace type_metal=7 if at_pyr_03_01_07==1
forval i=10/12{
replace type_metal=`i' if at_pyr_03_01_`i'==1
}

label define type_metal 7 "Gold" 10 "Silver" 11 "Platinum" 
label values type_metal type_metal

gen type_nometal=.
forval i=1/7{
replace type_nometal=`i' if at_pyr_04_01_0`i'==1
}
replace type_nometal=9 if at_pyr_04_01_09==1
forval i=10/23{
replace type_nometal=`i' if at_pyr_04_01_`i'==1
}
label define type_nometal 1 "Clay" 2 "Sand" 9 "Cement limestone" 14 "Gravel" 16 "Rocks" 20 "Salt" 21 "Magnesium silicate"
label values type_nometal type_nometal

gen type_gem=.
forval i=1/2{
replace type_gem=`i' if at_pyr_05_01_0`i'==1
}

gen cielo_abierto=ag_ue_07-1

gen type_mineral=100 if mineral==1
replace type_mineral=200+type_gem if mineral==2
replace type_mineral=300+type_metal if mineral==3
replace type_mineral=400+type_nometal if mineral==4
label define type_mineral 307 "Gold" 310 "Silver" 311 "Platinum" 401 "Clay" 402 "Sand" 409 "Cement limestone" 414 "Gravel" 416 "Rocks" 420 "Salt" 421 "Magnesium silicate"
label values type_mineral type_mineral

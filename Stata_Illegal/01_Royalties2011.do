use "$mipath\RawData\PanelCEDE\PANEL CARAC. GENERALES.dta", clear
collapse ano, by(municipio depto codmpio coddepto)
forval i=1/5{
qui replace municipio=subinstr(municipio,"(`i')","",.)
}
drop ano
qui replace municipio=subinstr(municipio,"(CD)","",.)
qui replace municipio=subinstr(municipio," ","",.)
save "$mipath\CreatedData\Codigos_DANE.dta", replace

import excel "$mipath\RawData\ANH\HIST�RICO DE REGAL�AS PAGADAS 2011 (91 kb).xlsx", sheet("page 1") firstrow clear

keep BENEFICIARIO AE
rename AE TOTALPAGADO2011
save "$mipath\CreatedData\Temporary\ANH_2011_p1.dta", replace

forval i=2/4{
import excel "$mipath\RawData\ANH\HIST�RICO DE REGAL�AS PAGADAS 2011 (91 kb).xlsx", sheet("page `i'") firstrow clear
keep BENEFICIARIO TOTALPAGADO2011
save "$mipath\CreatedData\Temporary\ANH_2011_p`i'.dta", replace
}

clear
forval i=1/4{
append using "$mipath\CreatedData\Temporary\ANH_2011_p`i'.dta"
}
drop if TOTALPAGADO2011=="-"
drop if TOTALPAGADO2011==""
drop if BENEFICIARIO=="BENEFICIARIO"
drop if BENEFICIARIO[_n-1]=="TOTALES" & BENEFICIARIO!="SAN ANTERO"
drop if BENEFICIARIO=="TOTALES"
drop if BENEFICIARIO=="ANTIOQUIA"
replace BENEFICIARIO=subinstr(BENEFICIARIO,"PTO.","PUERTO",.)
*replace BENEFICIARIO=subinstr(BENEFICIARIO,"�","E",.)
replace TOTALPAGADO2011=subinstr(TOTALPAGADO2011,".","",.)
save "$mipath\CreatedData\Temporary\ANH_2011.dta", replace

*The original file Regalias_caliza2004_2012.xls was not suitable for Stata, I copy and transposed by hand
import excel "$mipath\RawData\SIMCO\Regalias_caliza2004_2012_forStata.xlsx", sheet("Sheet1") firstrow clear
drop A B C F
rename D depto
rename E municipio
replace municipio=subinstr(municipio,"�","�",.)
replace municipio=subinstr(municipio,"�","�",.)

replace municipio=substr(municipio, 1, 1)+ lower(substr(municipio, 2, .))

rename G y2005
rename H y2006
rename I y2007
rename J y2008
rename K y2009
rename L y2010
rename M y2011
rename N y2012
merge 1:m municipio using "$mipath\CreatedData\Codigos_DANE.dta"
drop if _merge==2
replace codmpio=5000 if municipio=="Antioquia"
replace codmpio=68000 if municipio=="Santander"
replace codmpio=76000 if municipio=="Valle del cauca"
drop if municipio=="Varios"
reshape long y, i(codmpio) j(ano)
rename y regalias
replace regalias=subinstr(regalias,".","",.)
destring regalias, force replace
keep codmpio ano regalias
gen mineral="Caliza"
save "$mipath\CreatedData\Temporary\Caliza2004_2012.dta", replace

clear
generate codmpio = .
set obs 1
replace codmpio = 76109 in 1
set obs 2
replace codmpio = 76000 in 2
generate var2 = 1316352 in 1
replace var2 = 1645441 in 2
rename var2 regalias
gen ano=2005
gen mineral="Manganeso"
save "$mipath\CreatedData\Temporary\Manganeso2004_2012.dta", replace

*The original file Regalias_niquel1990_2012.xls was not suitable for Stata, I copy and transposed by hand
import excel "$mipath\RawData\SIMCO\Regalias_niquel1990_2012_forStata.xlsx", sheet("Sheet1") firstrow clear
drop A B C 
rename D depto
rename E municipio
replace municipio=subinstr(municipio,"�","�",.)
replace municipio=subinstr(municipio,"�","�",.)
replace municipio=subinstr(municipio,"�","�",.)
replace municipio=subinstr(municipio,"�","�",.)

replace municipio=substr(municipio, 1, 1)+ lower(substr(municipio, 2, .))
replace municipio=proper(municipio)
replace F y1990
rename G y1991
rename H y1992
rename I y1993
rename J y1994
rename K y1995
rename L y1996
rename M y1997
rename N y1998
rename O y1999
rename P y2000
rename Q y2001
rename R y2002
rename S y2003
rename T y2004

replace municipio="Montel�bano" if municpio=="Montel�Bano"
merge 1:m municipio using "$mipath\CreatedData\Codigos_DANE.dta"
drop if _merge==2
replace codmpio=5000 if municipio=="Antioquia"
replace codmpio=68000 if municipio=="Santander"
replace codmpio=76000 if municipio=="Valle del cauca"
drop if municipio=="Varios"
reshape long y, i(codmpio) j(ano)
rename y regalias
replace regalias=subinstr(regalias,".","",.)
destring regalias, force replace
keep codmpio ano regalias
gen mineral="Caliza"
save "$mipath\CreatedData\Temporary\Caliza2004_2012.dta", replace

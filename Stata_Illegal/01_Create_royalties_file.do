local deptos "Antioquia  Atlantico BogotaDC Bolivar Boyaca Caldas Caqueta Cauca Cesar Cordoba Cundinamarca Choco Huila  LaGuajira  Magdalena  Meta  Narino  NorteDeSantander Quindio   Risaralda  Santander  Sucre Tolima Valle Arauca  Casanare Putumayo  SanAndres   Amazonas  Guainia   Guaviare  Vaupes  Vichada"

foreach iyear in 2012 2013 2014 {
foreach idepto of local deptos {                         
import excel "$mipath\RawData\SGR\SGR_`idepto'`iyear'.xls", sheet("Datos Reporte SGR") cellrange(B17) firstrow clear
gen ano=`iyear'
save "$mipath\CreatedData\Temporary\SGR_`idepto'`iyear'", replace
}
}


clear
foreach iyear in 2012 2013 2014 {
foreach idepto of local deptos                     {                         
append using "$mipath\CreatedData\Temporary\SGR_`idepto'`iyear'"
}
}
rename Codigo codmpio
destring codmpio, force replace
rename Total transf_resources

save "$mipath\CreatedData\SGR_All_2012_4", replace
keep codmpio ano transf_resources
drop if mod(codmpio,1000)==0
save "$mipath\CreatedData\Temporary\SGR_formerge", replace

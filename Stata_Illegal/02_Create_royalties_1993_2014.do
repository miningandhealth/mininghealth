use "$mipath\RawData\PanelCEDE\PANEL FISCAL.dta", clear
qui gen transf_resources=regalias_compensa

keep codmpio ano transf_resources 
gen source=1
append using "$mipath\CreatedData\Temporary\SGR_formerge"
sort codmpio ano source
gen transf_scale=100 if ano==2012
forval i=1993/2011{
qui replace transf_scale=transf_resources/transf_resources[_n+(2012-`i')] if codmpio==codmpio[_n+(2012-`i')] & ano[_n+(2012-`i')]==2012 & source[_n+(2012-`i')]==1
*replace transf_scale=transf_resources/transf_resources[_n+(2013-`i')] if codmpio==codmpio[_n+(2013-`i')] & ano[_n+(2013-`i')]==2012 & source[_n+(2013-`i')]==1
}

forval i=2013/2014{
qui replace transf_scale=transf_resources/transf_resources[_n+(2012-`i')] if codmpio==codmpio[_n+(2012-`i')] & ano[_n+(2012-`i')]==2012 & source[_n+(2012-`i')]==.
*replace transf_scale=transf_resources/transf_resources[_n+(2011-`i')] if codmpio==codmpio[_n+(2011-`i')] & ano[_n+(2011-`i')]==2012 & source[_n+(2011-`i')]==.
}

save "$mipath\CreatedData\Royalties1993_2014", replace

import arcpy
from arcpy import env
from arcpy import gp
from arcpy import sa
from arcpy.sa import *
import os


#env.workspace="U:/Documents/Mining/Created_data"
#env.workspace="C:/Users/santi/Copy/PYP_Birth/CreatedData/GIS"
#uncomment for Mauro
env.workspace="C:/Users/Mauricio/Copy/MiningAndBirth/CreatedData/GIS"

arcpy.env.overwriteOutput=True
#rutloc="U:/Documents/Mining/"
#rutloc="C:/Users/santi/Copy/PYP_Birth/"
#uncomment for Mauro
rutloc="C:/Users/Mauricio/Copy/MiningAndBirth/"


#Intersect gold+mines and muni and obtain pedacitos
inter_files=rutloc+"CreatedData/GIS/gold_mines.shp #; "+rutloc+"CreatedData/GIS/Municipios.shp #"
mine_pedac=rutloc+"CreatedData/GIS/Mines_pedacitos.shp"

arcpy.Intersect_analysis(inter_files,mine_pedac,"ALL","#","INPUT")

# Replace a layer/table view name with a path to a dataset (which can be a layer file) or create the layer/table view within the script
# The following inputs are layers or table views: "gold_mines"
arcpy.Buffer_analysis("gold_mines","C:/Users/santi/Copy/PYP_Birth/CreatedData/GIS/Buffer_10km_goldmines.shp","10 Kilometers","FULL","ROUND","ALL","#")

arcpy.Intersect_analysis("Municipios #;Buffer_25km_goldmines #","C:/Users/santi/Copy/PYP_Birth/CreatedData/GIS/Muni_25km_close.shp","ALL","#","INPUT")

arcpy.Buffer_analysis("arena_mines","C:/Users/santi/Copy/PYP_Birth/CreatedData/GIS/Buffer_25km_arenamines.shp","25 Kilometers","FULL","ROUND","ALL","#")
arcpy.Intersect_analysis("Municipios #;Buffer_25km_arenamines #","C:/Users/santi/Copy/PYP_Birth/CreatedData/GIS/Muni_25km_close_arena.shp","ALL","#","INPUT")
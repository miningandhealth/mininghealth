import arcpy
from arcpy import env
from arcpy import gp
from arcpy import sa
from arcpy.sa import *
import os

arcpy.env.overwriteOutput=True
#rutloc="U:/Documents/Mining/"
rutloc="C:/Users/santi/Copy/PYP_Birth/"
#uncomment for Mauro
#rutloc="E:/Copy/MiningAndBirth/"
#rutloc="C:/Users/santi/Desktop/"


env.workspace=rutloc+"CreatedData/GIS"




#Copy the file with the CODIGO_EXP of gold mines
goldfile=rutloc+"CreatedData/GIS/Codigo_Oro.csv"
goldtable=rutloc+"CreatedData/GIS/Temporary/gold_table"
arcpy.CopyRows_management(goldfile,goldtable,"#")


#Delete field for the future
minefile=rutloc+"CreatedData/GIS/COL_mine_proj.shp"
COL_mine_proj = "COL_mine_proj"

#GIS is crashing so I did it by hand
#Load CreatedData/GIS/COL_mine_proj.shp, and Codigos_Oro.cvs
#Right click on the mines shape Joins and select Codigo_EXP, keep only matching records
#And Data> Export and save as gold mines


arcpy.AddJoin_management(COL_mine_proj, "CODIGO_EXP", goldfile, "CODIGO_EXP", "KEEP_ALL")


# Select only gold mines
goldshape=rutloc+"CreatedData/GIS/gold_mines.shp"
arcpy.Select_analysis(minefile,goldshape,""""Codigo_Oro.csv.oro" =1""")

#### Need to create gold_id field by hand
arcpy.CalculateField_management(goldshape,"gold_id","[FID]","VB","#")
### #Need to create area_m2 by hand


# Replace a layer/table view name with a path to a dataset (which can be a layer file) or create the layer/table view within the script
# The following inputs are layers or table views: "gold_mines"
rivfile=rutloc+"CreatedData/GIS/COL_riv_proj.shp"
mineedge=rutloc+"CreatedData/GIS/Mine_near_edge.dbf"
arcpy.GenerateNearTable_analysis(goldshape,rivfile,mineedge,"#","NO_LOCATION","NO_ANGLE","CLOSEST","0")

#Intersect gold+mines and muni and obtain pedacitos
inter_files=rutloc+"CreatedData/GIS/gold_mines.shp #; "+rutloc+"CreatedData/GIS/Municipios.shp #"
mine_pedac=rutloc+"CreatedData/GIS/Mines_pedacitos.shp"

arcpy.Intersect_analysis(inter_files,mine_pedac,"ALL","#","INPUT")

## Add by hand field area_sqm
## By hand Calculate Geometry area_sqm (area in square metersd)
## Confirn names of columns are CODIGO_EXP, FECHA_INSC, FECHA_EXP
import arcpy
from arcpy import env
from arcpy import gp
from arcpy import sa
from arcpy.sa import *
#import numpy as np
import os
#os.chdir("C:/Users/Santi/Copy/PYP_Birth/GIS/Apr7_river")

if arcpy.CheckExtension("Spatial Analyst")!="Available" :
    print "Spatial license not available"
arcpy.env.overwriteOutput=True
rutloc="C:/Users/santi/Copy/PYP_Birth/"

#env.workspace="E:/Users/santisap/Documents/Mining/CreatedData/GIS"
env.workspace=rutloc+"CreatedData/GIS"
#uncomment for Mauro
#env.workspace="E:/Copy/MiningAndBirth/GIS/Apr7_river"




for i in range(100,101,1):
    #Select the mines
    fechafile=rutloc+"CreatedData/AreaFechas/MinasActivasFecha_"+str(i)+".dbf"
    fechatable=rutloc+"CreatedData/GIS/Temporary/pol_table"
    arcpy.CopyRows_management(fechafile,fechatable,"#")

    goldfile=rutloc+"CreatedData/GIS/gold_mines.shp"
    arcpy.JoinField_management(goldfile,"COL_mine_p",fechatable,"CODIGO_EXP","#")


    # Select only active mines
    goldact=rutloc+"CreatedData/GIS/Temporary/Gold_active.shp"
    arcpy.Select_analysis(goldfile,goldact,""""activa"=1""")

      

    #Remove info
    arcpy.DeleteField_management(goldfile,"OID_;CODIGO_E_1;ACTIVA")
    
    # Create buffer around mines (in this case 10km)
    goldactbuf=rutloc+"CreatedData/GIS/Temporary/Gold_active_buffer.shp"
    arcpy.Buffer_analysis(goldact,goldactbuf,"20 Kilometers","FULL","ROUND","NONE","#")
    
    #Convert the buffers into raster
    rastact=rutloc+"CreatedData/GIS/Temporary/raster_act"
    arcpy.PolygonToRaster_conversion(goldactbuf,"activa",rastact,"CELL_CENTER","activa","3500")    
    #Convert missing value sinto zeros
    rastact0=rutloc+"CreatedData/GIS/Temporary/raster_act0"
    
    arcpy.gp.RasterCalculator_sa("""Con(IsNull("raster_act"),"col_ras_zeros", ("raster_act" + "col_ras_zeros")) """,rastact0)
    # Multiply pollution raster times population raster to get exposure raster
    rastpop=rutloc+"CreatedData/GIS/pop_2012_clip"
    rastexp=rutloc+"CreatedData/GIS/Temporary/Exp_raster"
    arcpy.gp.Times_sa(rastact0,rastpop,rastexp)
    # Create the output file with the average area exposure by municipality in date i
    outfile=rutloc+"CreatedData/AreaFechaS/AreaExpMuniFecha_20KM"+str(i)+".DBF"
    munfile=rutloc+"CreatedData/GIS/Mauro_mail/muni_raster.shp"
    arcpy.gp.ZonalStatisticsAsTable_sa(munfile,"CODANE2",rastexp,outfile,"DATA","ALL")

#shutil.rmtree("C:/Users/santi/Copy/PYP_Birth/GIS/Temporary")   Melissa Dell cleaning trick
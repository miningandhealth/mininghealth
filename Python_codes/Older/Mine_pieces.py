# Replace a layer/table view name with a path to a dataset (which can be a layer file) or create the layer/table view within the script
# The following inputs are layers or table views: "Gold_b2009"
arcpy.RepairGeometry_management("Gold_b2009","DELETE_NULL")

# Replace a layer/table view name with a path to a dataset (which can be a layer file) or create the layer/table view within the script
# The following inputs are layers or table views: "Gold_b2009"
arcpy.FeatureToPoint_management("Gold_b2009","C:/Users/Santi/Desktop/Mining/Network_analysis/Mine_b2009_centroid.shp","INSIDE")

# Replace a layer/table view name with a path to a dataset (which can be a layer file) or create the layer/table view within the script
# The following inputs are layers or table views: "Muni_centroid"
arcpy.Buffer_analysis("Muni_centroid","C:/Users/Santi/Desktop/Mining/Network_analysis/Muni_centr_buffer50k.shp","50 Kilometers","FULL","ROUND","NONE","#")

#This intersect creates a lot of small pieces
arcpy.Intersect_analysis("Mine_b2009 #;Muni_centr_buffer50k #","C:/Users/Santi/Desktop/Mining/Network_analysis/Inter_mine_muni50k.shp","ALL","#","INPUT")
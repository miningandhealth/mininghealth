import arcpy
from arcpy import env
from arcpy.sa import *
#import numpy as np
#import os
#os.chdir("C:/Users/Santi/Copy/PYP_Birth/GIS/Apr7_river")
env.workspace="C:/Users/Santi/Copy/PYP_Birth/GIS/Apr7_river"
arcpy.env.overwriteOutput=True

if arcpy.CheckExtension("spatial")!="Available" :
    print "Spatial license not available"

    
for i in range(1,585) :
    #Add the polution data to the rivers shape file, using poledg, merging by id_edge
    polfile="C:/Users/Santi/Copy/PYP_Birth/CreatedData/AreaUpstreamRiver/AreaMinadaFecha_"+str(i)+".csv"
    arcpy.CopyRows_management(polfile,"c:/users/santi/copy/pyp_birth/gis/apr7_river/pol_table","#")
    #arcpy.AddJoin_management("C:/Users/Santi/Copy/PYP_Birth/GIS/Apr7_river/COL_riv_proj.dbf","id_edge","c:/users/santi/copy/pyp_birth/gis/apr7_river/pol_table","id_edge","KEEP_ALL")
    arcpy.JoinField_management("C:/Users/Santi/Copy/PYP_Birth/GIS/Apr7_river/COL_riv_suc.shp","id_edge","c:/users/santi/copy/pyp_birth/gis/apr7_river/pol_table","ID_EDGE","#")


    # Select only poluted rivers
    arcpy.Select_analysis("C:/Users/Santi/Copy/PYP_Birth/GIS/Apr7_river/COL_riv_suc.shp","C:/Users/Santi/Copy/PYP_Birth/GIS/Apr7_river/Riv_poluted.shp",""""POLUCION">0""")

    #Remove polution info from river so that next year no problem when adding new pollution
    #arcpy.RemoveJoin_management("C:/Users/Santi/Copy/PYP_Birth/GIS/Apr7_river/COL_riv_proj.shp","#")
    # Replace 
    arcpy.DeleteField_management("C:/Users/Santi/Copy/PYP_Birth/GIS/Apr7_river/COL_riv_suc.shp","OBJECTID;ID_EDGE_1;POLUCION")
    # Create buffer of pollution around rivers (in this case 10km)
    arcpy.Buffer_analysis("C:/Users/Santi/Copy/PYP_Birth/GIS/Apr7_river/Riv_poluted.shp","C:/Users/Santi/Copy/PYP_Birth/GIS/Apr7_river/Riv_poluted_buffer.shp","10 Kilometers","FULL","ROUND","NONE","#")
    
    #Convert the pollution buffers into raster
    arcpy.PolygonToRaster_conversion("C:/Users/Santi/Copy/PYP_Birth/GIS/Apr7_river/Riv_poluted_buffer.shp","POLUCION","C:/Users/Santi/Copy/PYP_Birth/GIS/Apr7_river/raster_suc2","CELL_CENTER","POLUCION","3500")    
    #Convert missing value sinto zeros
    arcpy.gp.RasterCalculator_sa("""Con(IsNull("raster_suc2"),0,"raster_suc2")""","C:/Users/Santi/Copy/PYP_Birth/GIS/Apr7_river/raster_suc20")
    # Multiply pollution raster times population raster to get exposure raster
    arcpy.gp.Times_sa("C:/Users/Santi/Copy/PYP_Birth/GIS/Apr7_river/raster_suc20","C:/Users/Santi/Copy/PYP_Birth/GIS/Apr7_river/pop_2012","C:/Users/Santi/Copy/PYP_Birth/GIS/Apr7_river/Exp_raster")
    # Create the output file with the average pollution by municipality in date i
    outfile="C:/Users/Santi/Copy/PYP_Birth/CreatedData/AreaUpstreamRiver/PolExpMuniFecha_"+str(i)+".txt"
    arcpy.gp.ZonalStatisticsAsTable_sa("C:/Users/Santi/Copy/PYP_Birth/GIS/Apr7_river/Municipios.shp","CODANE2","C:/Users/Santi/Copy/PYP_Birth/GIS/Apr7_river/Exp_raster",outfile,"DATA","ALL")    
import arcpy
from arcpy import env
from arcpy.sa import *
#import numpy as np
#import os
#os.chdir("C:/Users/Santi/Copy/PYP_Birth/GIS/Apr7_river")
env.workspace="C:/Users/Santi/Copy/PYP_Birth/GIS/Apr7_river"
arcpy.env.overwriteOutput=True

if arcpy.CheckExtension("spatial")!="Available" :
    print "Spatial license not available"
rutloc="E:/Copy/MiningAndBirth/"

# Local variables:
COL_riv_proj_shp = rutloc+"GIS/Apr7_river/COL_riv_proj.shp"
COL_riv_proj_Pol_shp = rutloc+"GIS/Apr7_river/RiverPol/COL_riv_proj_Pol.shp"

# Process: Copy Features
arcpy.CopyFeatures_management(COL_riv_proj_shp, COL_riv_proj_Pol_shp, "", "0", "0", "0")
  
for i in range(1,585) :

    # Local variables:
    COL_riv_proj_dbf = rutloc+"GIS/Apr7_river/RiverPol/COL_riv_proj_"+str(1)+".dbf"
    COL_riv_proj_Pol_dbf = rutloc+"GIS/Apr7_river/RiverPol/COL_riv_proj_Pol.dbf"

    # Process: Copy Rows
    arcpy.CopyRows_management(COL_riv_proj_dbf, COL_riv_proj_Pol_dbf, "")



    # Select only poluted rivers
    rivpol=rutloc+"GIS/Apr7_river/RiverPol/COL_riv_proj_Pol.shp"
    rivpolSimple=rutloc+"GIS/Apr7_river/RiverPol/COL_riv_proj_PolSimple.shp"
    arcpy.Select_analysis(rivpol,rivpolSimple,""""polucion">0""")

    #Remove polution info from river so that next year no problem when adding new pollution
    #arcpy.RemoveJoin_management("C:/Users/Santi/Copy/PYP_Birth/GIS/Apr7_river/COL_riv_proj.shp","#")
    # Replace 
    arcpy.DeleteField_management(rivfile,"OBJECTID;ID_EDGE_1;POLUCION")
    # Create buffer of pollution around rivers (in this case 10km)
    rivpolbuf=rutloc+"GIS/Apr7_river/Riv_poluted_buffer.shp"
    arcpy.Buffer_analysis(rivpol,rivpolbuf,"10 Kilometers","FULL","ROUND","NONE","#")
    
    #Convert the pollution buffers into raster
    rastpol=rutloc+"GIS/Apr7_river/raster_suc2"
    arcpy.PolygonToRaster_conversion(rivpolbuf,"POLUCION",rastpol,"CELL_CENTER","POLUCION","3500")    
    #Convert missing value sinto zeros
    rastpol0=rutloc+"GIS/Apr7_river/raster_suc20"
    arcpy.gp.RasterCalculator_sa("""Con(IsNull("raster_suc2"),0,"raster_suc2")""",rastpol0)
    # Multiply pollution raster times population raster to get exposure raster
    rastpop=rutloc+"GIS/Apr7_river/pop_2012"
    rastexp=rutloc+"GIS/Apr7_river/Exp_raster"
    arcpy.gp.Times_sa(rastpol0,rastpop,rastexp)
    # Create the output file with the average pollution by municipality in date i
    outfile=rutloc+"CreatedData/AreaUpstreamRiver/PolExpMuniFecha_"+str(i)+".txt"
    munfile=rutloc+"GIS/Apr7_river/Municipios.shp"
    arcpy.gp.ZonalStatisticsAsTable_sa(munfile,"CODANE2",rastexp,outfile,"DATA","ALL")    

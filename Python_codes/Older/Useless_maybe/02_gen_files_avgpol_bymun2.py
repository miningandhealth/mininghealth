import arcpy
import numpy as np
import os
os.chdir("E:/Copy/MiningAndBirth/GIS/Apr7_river")
arcpy.env.workspace="E:/Copy/MiningAndBirth/GIS/Apr7_river"
arcpy.env.overwriteOutput=True
for i in range(1,584) :
    #Add the polution data to the rivers shape file, using poledg, merging by id_edge
    archivo="E:/Copy/MiningAndBirth/CreatedData/AreaUpstreamRiver/AreaMinadaFecha_"+str(i)+".csv"
    arcpy.AddJoin_management("COL_riv_proj","id_edge",archivo,"id_edge","KEEP_ALL")


    # Select only poluted rivers
    arcpy.Select_analysis("COL_riv_proj","E:/Copy/MiningAndBirth/GIS/Apr7_river/Riv_poluted.shp",""""poledg_001.csv.polucion">0""")

    #Remove polution info from river so that next year no problem when adding new pollution
    arcpy.RemoveJoin_management("COL_riv_proj","#")
    # Create buffer of pollution around rivers (in this case 10km)
    arcpy.Buffer_analysis("Riv_poluted","E:/Copy/MiningAndBirth/GIS/Apr7_river/Riv_poluted_buffer.shp","10 Kilometers","FULL","ROUND","NONE","#")
    
    #Convert the pollution buffers into raster
    arcpy.PolygonToRaster_conversion("Riv_poluted_buffer","poledg_002","E:/Copy/MiningAndBirth/GIS/Apr7_river/raster_pol","CELL_CENTER","poledg_002","3500")

    
    #Convert missing value sinto zeros
    arcpy.gp.RasterCalculator_sa("""Con(IsNull("raster_pol"),0,"raster_pol")""","E:/Copy/MiningAndBirth/GIS/Apr7_river/raster_pol")

    # Multiply pollution raster times population raster to get exposure raster
    arcpy.gp.Times_sa("raster_pol","pop_2012","E:/Copy/MiningAndBirth/GIS/Apr7_river/Exp_raster")

    # Create the output file with the average pollution by municipality in date i
    archivo2="E:/Copy/MiningAndBirth/CreatedData/exp_fecha_"+str(i)
    arcpy.gp.ZonalStatisticsAsTable_sa("Municipios","CODANE2","Exp_raster",archivo2,"DATA","ALL")



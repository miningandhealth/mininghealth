# Replace a layer/table view name with a path to a dataset (which can be a layer file) or create the layer/table view within the script
# The following inputs are layers or table views: "pop_clip"
arcpy.ProjectRaster_management("pop_clip","C:/Users/Santi/Copy/PYP_Birth/GIS/Raster_pop_pol/pop_2012","PROJCS['South_America_Equidistant_Conic',GEOGCS['GCS_South_American_1969',DATUM['D_South_American_1969',SPHEROID['GRS_1967_Truncated',6378160.0,298.25]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]],PROJECTION['Equidistant_Conic'],PARAMETER['False_Easting',0.0],PARAMETER['False_Northing',0.0],PARAMETER['Central_Meridian',-60.0],PARAMETER['Standard_Parallel_1',-5.0],PARAMETER['Standard_Parallel_2',-42.0],PARAMETER['Latitude_Of_Origin',-32.0],UNIT['Meter',1.0]]","NEAREST","1051.36453507461","SAD_1969_To_WGS_1984_1","#","GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]]")

# Replace a layer/table view name with a path to a dataset (which can be a layer file) or create the layer/table view within the script
# The following inputs are layers or table views: "Gold_b2009_mun_Proj"
arcpy.CalculateField_management("Gold_b2009_mun_Proj","ind_mine","1","VB","#")

# Replace a layer/table view name with a path to a dataset (which can be a layer file) or create the layer/table view within the script
# The following inputs are layers or table views: "Gold_b2009_mun_Proj"
arcpy.PolygonToRaster_conversion("Gold_b2009_mun_Proj","ind_mine","C:/Users/Santi/Copy/PYP_Birth/GIS/Raster_pop_pol/mine2008_rst","CELL_CENTER","NONE","0.043")

#Treat null values(places without mine) as zero
arcpy.gp.RasterCalculator_sa("""Con(IsNull("mine2008_rst"),0,"mine2008_rst")""","C:/Users/Santi/Copy/PYP_Birth/GIS/Raster_pop_pol/mine2008_rst")

# Replace a layer/table view name with a path to a dataset (which can be a layer file) or create the layer/table view within the script
# The following inputs are layers or table views: "mine2008_rst", "pop_2012"
arcpy.gp.Times_sa("mine2008_rst","pop_2012","C:/Users/Santi/Copy/PYP_Birth/GIS/Raster_pop_pol/Exp_08_12")

# Replace a layer/table view name with a path to a dataset (which can be a layer file) or create the layer/table view within the script
# The following inputs are layers or table views: "Municipios", "Exp_08_12"
arcpy.gp.ZonalStatisticsAsTable_sa("Municipios","CODANE2","Exp_08_12","c:/users/santi/copy/pyp_birth/gis/raster_pop_pol/mun_exp0812","DATA","ALL")
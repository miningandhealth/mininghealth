import arcpy
from arcpy import env
from arcpy.sa import *
#import numpy as np
#import os
#os.chdir("E:/Copy/MiningAndBirth/GIS/Apr7_river")
env.workspace="E:/Copy/MiningAndBirth/GIS/Apr7_river"
arcpy.env.overwriteOutput=True

if arcpy.CheckExtension("spatial")!="Available" :
    print "Spatial license not available"

    
for i in range(584,583,-1) :
    print i
    #Add the polution data to the rivers shape file, using poledg, merging by id_edge
    polfile="E:/Copy/MiningAndBirth/CreatedData/AreaUpstreamRiver/AreaMinadaFecha_"+str(i)+".csv"
    arcpy.CopyRows_management(polfile,"E:/Copy/MiningAndBirth/gis/apr7_river/pol_table","#")
    #arcpy.AddJoin_management("E:/Copy/MiningAndBirth/GIS/Apr7_river/COL_riv_proj.dbf","id_edge","E:/Copy/MiningAndBirth/gis/apr7_river/pol_table","id_edge","KEEP_ALL")
    arcpy.JoinField_management("E:/Copy/MiningAndBirth/GIS/Apr7_river/COL_riv_suc.shp","id_edge","E:/Copy/MiningAndBirth/gis/apr7_river/pol_table","ID_EDGE","#")


    # Select only poluted rivers
    arcpy.Select_analysis("E:/Copy/MiningAndBirth/GIS/Apr7_river/COL_riv_suc.shp","E:/Copy/MiningAndBirth/GIS/Apr7_river/Riv_poluted.shp",""""POLUCION">0""")

    #Remove polution info from river so that next year no problem when adding new pollution
    #arcpy.RemoveJoin_management("E:/Copy/MiningAndBirth/GIS/Apr7_river/COL_riv_proj.shp","#")
    # Replace 
    arcpy.DeleteField_management("E:/Copy/MiningAndBirth/GIS/Apr7_river/COL_riv_suc.shp","OBJECTID;ID_EDGE_1;POLUCION")
    # Create buffer of pollution around rivers (in this case 10km)
    arcpy.Buffer_analysis("E:/Copy/MiningAndBirth/GIS/Apr7_river/Riv_poluted.shp","E:/Copy/MiningAndBirth/GIS/Apr7_river/Riv_poluted_buffer.shp","10 Kilometers","FULL","ROUND","NONE","#")
    
    #Convert the pollution buffers into raster
    arcpy.PolygonToRaster_conversion("E:/Copy/MiningAndBirth/GIS/Apr7_river/Riv_poluted_buffer.shp","POLUCION","E:/Copy/MiningAndBirth/GIS/Apr7_river/raster_suc2","CELL_CENTER","POLUCION","3500")    
    #Convert missing value sinto zeros
    arcpy.gp.RasterCalculator_sa("""Con(IsNull("raster_suc2"),0,"raster_suc2")""","E:/Copy/MiningAndBirth/GIS/Apr7_river/raster_suc20")
    # Multiply pollution raster times population raster to get exposure raster
    arcpy.gp.Times_sa("E:/Copy/MiningAndBirth/GIS/Apr7_river/raster_suc20","E:/Copy/MiningAndBirth/GIS/Apr7_river/pop_2012","E:/Copy/MiningAndBirth/GIS/Apr7_river/Exp_raster")
    # Create the output file with the average pollution by municipality in date i
    outfile="E:/Copy/MiningAndBirth/CreatedData/AreaUpstreamRiver/PolExpMuniFecha_"+str(i)+".txt"
    arcpy.gp.ZonalStatisticsAsTable_sa("E:/Copy/MiningAndBirth/GIS/Apr7_river/Municipios.shp","CODANE2","E:/Copy/MiningAndBirth/GIS/Apr7_river/Exp_raster",outfile,"DATA","ALL")    


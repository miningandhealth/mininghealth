import arcpy
from arcpy import env
from arcpy.sa import *
#import numpy as np
#import os
#os.chdir("C:/Users/Santi/Copy/PYP_Birth/GIS/Apr7_river")
env.workspace="E:/Copy/MiningAndBirth/GIS/Apr7_river"
arcpy.env.overwriteOutput=True

if arcpy.CheckExtension("spatial")!="Available" :
    print "Spatial license not available"
arcpy.env.overwriteOutput=True
rutloc="E:/Copy/MiningAndBirth/"
    
for i in range(436,512,1):
    #Add the polution data to the rivers shape file, using poledg, merging by id_edge
    polfile=rutloc+"CreatedData/AreaUpstreamRiver/AreaMinadaFecha_"+str(i)+".csv"
    poltable=rutloc+"GIS/Apr7_river/pol_table"
    arcpy.CopyRows_management(polfile,poltable,"#")
    arcpy.AddField_management("pol_table","polu","DOUBLE","#","#","#","#","NULLABLE","NON_REQUIRED","#")
    arcpy.CalculateField_management("pol_table","polu","[POLUCION]","VB","#")
    #arcpy.AddJoin_management("C:/Users/Santi/Copy/PYP_Birth/GIS/Apr7_river/COL_riv_proj.dbf","id_edge","c:/users/santi/copy/pyp_birth/gis/apr7_river/pol_table","id_edge","KEEP_ALL")
    rivfile=rutloc+"GIS/Apr7_river/COL_riv_suc.shp"
    arcpy.JoinField_management(rivfile,"id_edge",poltable,"ID_EDGE","#")


    # Select only poluted rivers
    rivpol=rutloc+"GIS/Apr7_river/Riv_poluted.shp"
    arcpy.Select_analysis(rivfile,rivpol,""""ID_EDGE_1">0""")

      

    #Remove polution info from river so that next year no problem when adding new pollution
    #arcpy.RemoveJoin_management("C:/Users/Santi/Copy/PYP_Birth/GIS/Apr7_river/COL_riv_proj.shp","#")
    # Replace 
    arcpy.DeleteField_management(rivfile,"OBJECTID;ID_EDGE_1;POLUCION;polu")
    # Create buffer of pollution around rivers (in this case 10km)
    rivpolbuf=rutloc+"GIS/Apr7_river/Riv_poluted_buffer.shp"
    arcpy.Buffer_analysis(rivpol,rivpolbuf,"10 Kilometers","FULL","ROUND","NONE","#")
    
    #Convert the pollution buffers into raster
    rastpol=rutloc+"GIS/Apr7_river/raster_suc"
    arcpy.PolygonToRaster_conversion(rivpolbuf,"polu",rastpol,"CELL_CENTER","polu","3500")    
    #Convert missing value sinto zeros
    rastpol0=rutloc+"GIS/Apr7_river/raster_suc0"
    #arcpy.gp.RasterCalculator_sa("""Con(IsNull("raster_suc3"),0,"raster_suc3")""",rastpol0)
    arcpy.gp.RasterCalculator_sa("""Con(IsNull("raster_suc"),"col_ras_zeros", ("raster_suc" + "col_ras_zeros")) """,rastpol0)
    # Multiply pollution raster times population raster to get exposure raster
    rastpop=rutloc+"GIS/Apr7_river/pop_2012_clip"
    rastexp=rutloc+"GIS/Apr7_river/Exp_raster"
    arcpy.gp.Times_sa(rastpol0,rastpop,rastexp)
    # Create the output file with the average pollution by municipality in date i
    outfile=rutloc+"CreatedData/AreaUpstreamRiver/PolExpMuniFecha_"+str(i)+".txt"
    munfile=rutloc+"GIS/Apr7_river/Municipios.shp"
    arcpy.gp.ZonalStatisticsAsTable_sa(munfile,"CODANE2",rastexp,outfile,"DATA","ALL")    

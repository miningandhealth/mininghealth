import arcpy
from arcpy import env
from arcpy import gp
from arcpy import sa
from arcpy.sa import *
#import numpy as np
import os
#os.chdir("C:/Users/Santi/Copy/PYP_Birth/GIS/Apr7_river")

env.workspace="C:/Users/santi/Copy/PYP_Birth/CreatedData/GIS"
#uncomment for Mauro
#env.workspace="E:/Copy/PYP_Birth/CreatedData/GIS"


if arcpy.CheckExtension("Spatial Analyst")!="Available" :
    print "Spatial license not available"
arcpy.env.overwriteOutput=True
rutloc="C:/Users/santi/Copy/PYP_Birth/"
#uncomment for Mauro
#rutloc="E:/Copy/MiningAndBirth/"

# os.mkdir("C:/Users/santi/Copy/PYP_Birth/GIS/Temporary") Melissa Dell cleaning trick
#gp.workspace("C:/Users/santi/Copy/PYP_Birth/GIS/Temporary") Melissa Dell cleaning trick

nfiles=460

#To speed up calculation I first select all the river evers dirty
polfile=rutloc+"CreatedData/AreaUpstreamRiver/Arena_INDEX_SUCIO.csv"
poltable=rutloc+"CreatedData/GIS/Temporary/pol_table"
arcpy.CopyRows_management(polfile,poltable,"#")

rivfile=rutloc+"CreatedData/GIS/COL_riv_proj.shp"
arcpy.JoinField_management(rivfile,"id_edge",poltable,"id_edge","#")

# Select only poluted rivers
rivpol=rutloc+"CreatedData/GIS/Temporary/COL_riv_suc_arena.shp"
arcpy.Select_analysis(rivfile,rivpol,""""SUCIO"=1""")

arcpy.DeleteField_management(rivfile,"OID_;ID_EDGE_1;POLUCION")

for i in range(1,nfiles+1,1):
    #Add the polution data to the rivers shape file, using poledg, merging by id_edge
    polfile=rutloc+"CreatedData/AreaUpstreamRiver/Arena_AreaMinadaEdgeFecha_"+str(i)+".dbf"
    poltable=rutloc+"CreatedData/GIS/Temporary/pol_table"
    arcpy.CopyRows_management(polfile,poltable,"#")
    #arcpy.AddField_management("pol_table","polu","DOUBLE","#","#","#","#","NULLABLE","NON_REQUIRED","#")
    #arcpy.CalculateField_management("pol_table","polu","[POLUCION]","VB","#")
    #arcpy.AddJoin_management("C:/Users/Santi/Copy/PYP_Birth/GIS/Apr7_river/COL_riv_proj.dbf","id_edge","c:/users/santi/copy/pyp_birth/gis/apr7_river/pol_table","id_edge","KEEP_ALL")
    rivfile=rutloc+"CreatedData/GIS/COL_riv_suc_arena.shp"
    arcpy.JoinField_management(rivfile,"id_edge",poltable,"id_edge","#")


    # Select only poluted rivers
    rivpol=rutloc+"CreatedData/GIS/Temporary/Riv_poluted.shp"
    arcpy.Select_analysis(rivfile,rivpol,""""ID_EDGE_1">0""")

      

    #Remove polution info from river so that next year no problem when adding new pollution
    #arcpy.RemoveJoin_management("C:/Users/Santi/Copy/PYP_Birth/GIS/Apr7_river/COL_riv_proj.shp","#")
    # arcpy.DeleteField_management(rivfile,"OBJECTID;ID_EDGE_1;POLUCION;polu") 
    arcpy.DeleteField_management(rivfile,"OID_;ID_EDGE_1;POLUCION")
    # Create buffer of pollution around rivers (in this case 10km)
    rivpolbuf=rutloc+"CreatedData/GIS/Temporary/Riv_poluted_buffer.shp"
    arcpy.Buffer_analysis(rivpol,rivpolbuf,"10 Kilometers","FULL","ROUND","NONE","#")
    
    #Convert the pollution buffers into raster
    rastpol=rutloc+"CreatedData/GIS/Temporary/raster_suc"
    arcpy.PolygonToRaster_conversion(rivpolbuf,"POLUCION",rastpol,"CELL_CENTER","POLUCION","3500")    
    #Convert missing value sinto zeros
    rastpol0=rutloc+"CreatedData/GIS/Temporary/raster_suc0"
    #arcpy.gp.RasterCalculator_sa("""Con(IsNull("raster_suc3"),0,"raster_suc3")""",rastpol0)
    arcpy.gp.RasterCalculator_sa("""Con(IsNull("raster_suc"),"col_ras_zeros", ("raster_suc" + "col_ras_zeros")) """,rastpol0)
    # Multiply pollution raster times population raster to get exposure raster
    rastpop=rutloc+"CreatedData/GIS/pop_2012_clip"
    rastexp=rutloc+"CreatedData/GIS/Temporary/Exp_raster"
    arcpy.gp.Times_sa(rastpol0,rastpop,rastexp)
    # Create the output file with the average pollution by municipality in date i
    outfile=rutloc+"CreatedData/AreaUpstreamRiver/Arena_PolExpMuniFecha_"+str(i)+".txt"
    munfile=rutloc+"CreatedData/GIS/Municipios.shp"
    arcpy.gp.ZonalStatisticsAsTable_sa(munfile,"CODANE2",rastexp,outfile,"DATA","ALL")

#shutil.rmtree("C:/Users/santi/Copy/PYP_Birth/GIS/Temporary")   Melissa Dell cleaning trick
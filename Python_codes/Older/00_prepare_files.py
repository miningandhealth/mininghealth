#Extract the location of each river vertex and edges endpoints

# Add an id field that appears when exporting tables
arcpy.CalculateField_management("Municipios","id","[FID]","VB","#")

# Project municipios to SouthAmeriucan equidistant conic coordinates
arcpy.Project_management("Municipios","C:/Users/Santi/Copy/PYP_Birth/GIS/Apr7_river/Municipios","PROJCS['South_America_Equidistant_Conic',GEOGCS['GCS_South_American_1969',DATUM['D_South_American_1969',SPHEROID['GRS_1967_Truncated',6378160.0,298.25]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]],PROJECTION['Equidistant_Conic'],PARAMETER['False_Easting',0.0],PARAMETER['False_Northing',0.0],PARAMETER['Central_Meridian',-60.0],PARAMETER['Standard_Parallel_1',-5.0],PARAMETER['Standard_Parallel_2',-42.0],PARAMETER['Latitude_Of_Origin',-32.0],UNIT['Meter',1.0]]","SAD_1969_To_WGS_1984_1","GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]]")

# Extract the altitude of each point
arcpy.gp.ExtractValuesToPoints_sa("COL_riv_network_Junctions","col_elev","C:/Users/Santi/Copy/PYP_Birth/GIS/Apr7_river/Riv_junct_altitud.shp","NONE","VALUE_ONLY")

# Replace a layer/table view name with a path to a dataset (which can be a layer file) or create the layer/table view within the script
# The following inputs are layers or table views: "Riv_junct_altitud"
arcpy.CalculateField_management("Riv_junct_altitud","Id","[FID]","VB","#")

#Calculate xcoord and y coord I didnt get the Python code)

# Replace a layer/table view name with a path to a dataset (which can be a layer file) or create the layer/table view within the script
# The following inputs are layers or table views: "Riv_junct_altitud"
arcpy.GenerateNearTable_analysis("Riv_junct_altitud","Municipios","c:/users/santi/copy/pyp_birth/gis/apr7_river/riv_vertex_muni","#","NO_LOCATION","NO_ANGLE","CLOSEST","0")

# Replace a layer/table view name with a path to a dataset (which can be a layer file) or create the layer/table view within the script
# The following inputs are layers or table views: "Riv_junct_altitud"
arcpy.Near_analysis("Riv_junct_altitud","Municipios","#","NO_LOCATION","NO_ANGLE")
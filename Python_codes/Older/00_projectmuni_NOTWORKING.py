import arcpy
from arcpy import env
from arcpy import gp
from arcpy import sa
from arcpy.sa import *

import os


env.workspace="C:/Users/santi/Copy/PYP_Birth/CreatedData/GIS"
#uncomment for Mauro
#env.workspace="E:/Copy/MiningAndBirth/CreatedData/GIS"

arcpy.env.overwriteOutput=True
rutloc="C:/Users/santi/Copy/PYP_Birth/"
#uncomment for Mauro
#rutloc="E:/Copy/MiningAndBirth/"

#Project shapefiles of mines
ori_minefile=rutloc+"RawData/GIS/TMC_18_Agosto_2011"
mine_proj=rutloc+"CreatedData/GIS/COL_mine_proj"
arcpy.Project_management(ori_minefile,mine_proj,"PROJCS['South_America_Equidistant_Conic',GEOGCS['GCS_South_American_1969',DATUM['D_South_American_1969',SPHEROID['GRS_1967_Truncated',6378160.0,298.25]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]],PROJECTION['Equidistant_Conic'],PARAMETER['False_Easting',0.0],PARAMETER['False_Northing',0.0],PARAMETER['Central_Meridian',-60.0],PARAMETER['Standard_Parallel_1',-5.0],PARAMETER['Standard_Parallel_2',-42.0],PARAMETER['Latitude_Of_Origin',-32.0],UNIT['Meter',1.0]]","SAD_1969_To_WGS_1984_1","GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]]")

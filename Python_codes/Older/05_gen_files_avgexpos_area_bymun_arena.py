import arcpy
from arcpy import env
from arcpy import gp
from arcpy import sa
from arcpy.sa import *
#import numpy as np
import os
#os.chdir("C:/Users/Santi/Copy/PYP_Birth/GIS/Apr7_river")


#env.workspace="E:/Users/santisap/Documents/Mining/CreatedData/GIS"
env.workspace="C:/Users/santi/Desktop/CreatedData/GIS"
#uncomment for Mauro
#env.workspace="E:/Copy/MiningAndBirth/GIS/Apr7_river"


if arcpy.CheckExtension("Spatial Analyst")!="Available" :
    print "Spatial license not available"
arcpy.env.overwriteOutput=True
rutloc="C:/Users/santi/Desktop/"
#rutloc="E:/Users/santisap/Documents/Mining/"
#uncomment for Mauro
#rutloc="E:/Copy/MiningAndBirth/"

# os.mkdir("C:/Users/santi/Copy/PYP_Birth/GIS/Temporary") Melissa Dell cleaning trick
#gp.workspace("C:/Users/santi/Copy/PYP_Birth/GIS/Temporary") Melissa Dell cleaning trick

nfiles=460



for i in range(85,nfiles,1):
    #Select the mines
    fechafile=rutloc+"CreatedData/AreaFechas/Arena_MinasActivasFecha_"+str(i)+".dbf"
    fechatable=rutloc+"CreatedData/GIS/Temporary/pol_table"
    arcpy.CopyRows_management(fechafile,fechatable,"#")

    arenafile=rutloc+"CreatedData/GIS/arena_mines.shp"
    arcpy.JoinField_management(arenafile,"CODIGO_EXP",fechatable,"CODIGO_EXP","#")


    # Select only active mines
    arenaact=rutloc+"CreatedData/GIS/Temporary/Arena_active.shp"
    arcpy.Select_analysis(arenafile,arenaact,""""activa"=1""")

      

    #Remove info
    arcpy.DeleteField_management(arenafile,"OID_;CODIGO_E_1;ACTIVA")
    
    # Create buffer around mines (in this case 10km)
    arenaactbuf=rutloc+"CreatedData/GIS/Temporary/Arena_active_buffer.shp"
    arcpy.Buffer_analysis(arenaact,arenaactbuf,"10 Kilometers","FULL","ROUND","NONE","#")
    
    #Convert the buffers into raster
    rastact=rutloc+"CreatedData/GIS/Temporary/raster_act"
    arcpy.PolygonToRaster_conversion(arenaactbuf,"activa",rastact,"CELL_CENTER","activa","3500")    
    #Convert missing value sinto zeros
    rastact0=rutloc+"CreatedData/GIS/Temporary/raster_act0"
    
    arcpy.gp.RasterCalculator_sa("""Con(IsNull("raster_act"),"col_ras_zeros", ("raster_act" + "col_ras_zeros")) """,rastact0)
    # Multiply pollution raster times population raster to get exposure raster
    rastpop=rutloc+"CreatedData/GIS/pop_2012_clip"
    rastexp=rutloc+"CreatedData/GIS/Temporary/Exp_raster"
    arcpy.gp.Times_sa(rastact0,rastpop,rastexp)
    # Create the output file with the average area exposure by municipality in date i
    outfile=rutloc+"CreatedData/AreaFechaS/Arena_AreaExpMuniFecha_"+str(i)+".DBF"
    munfile=rutloc+"CreatedData/GIS/Municipios.shp"
    arcpy.gp.ZonalStatisticsAsTable_sa(munfile,"CODANE2",rastexp,outfile,"DATA","ALL")

#shutil.rmtree("C:/Users/santi/Copy/PYP_Birth/GIS/Temporary")   Melissa Dell cleaning trick
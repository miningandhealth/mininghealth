import arcpy
from arcpy import env
from arcpy import gp
from arcpy import sa
from arcpy.sa import *
import os


#env.workspace="U:/Documents/Mining/Created_data"
#env.workspace="C:/Users/santi/Copy/PYP_Birth/CreatedData/GIS"
#uncomment for Mauro
env.workspace="C:/Users/Mauricio/Copy/MiningAndBirth/CreatedData/GIS"

arcpy.env.overwriteOutput=True
#rutloc="U:/Documents/Mining/"
#rutloc="C:/Users/santi/Copy/PYP_Birth/"
#uncomment for Mauro
rutloc="C:/Users/Mauricio/Copy/MiningAndBirth/"


#Copy the file with the CODIGO_EXP of gold mines
arenafile=rutloc+"CreatedData/GIS/Codigo_ARENA.csv"


#Delete field for the future
minefile=rutloc+"CreatedData/GIS/COL_mine_proj.shp"
COL_mine_proj = "COL_mine_proj"

arcpy.AddJoin_management(COL_mine_proj, "CODIGO_EXP", arenafile, "CODIGO_EXP", "KEEP_ALL")

COL_mine_proj_arena_shp = rutloc+"CreatedData\\GIS\\COL_mine_proj_arena.shp"

arcpy.Select_analysis(COL_mine_proj, COL_mine_proj_arena_shp, "\"Codigo_ARENA.csv.arena\" =1")

# Select only gold mines
goldshape=rutloc+"CreatedData/GIS/arena_mines.shp"
arcpy.Select_analysis(minefile,goldshape,""""arena"=1""")
arcpy.DeleteField_management(minefile,"OBJECTID;CODIGO_E_1;")
#Need to create gold_id field by hand
arcpy.CalculateField_management(goldshape,"gold_id","[FID]","VB","#")
#Need to create area_m2 by hand


# Replace a layer/table view name with a path to a dataset (which can be a layer file) or create the layer/table view within the script
# The following inputs are layers or table views: "gold_mines"
rivfile=rutloc+"CreatedData/GIS/COL_riv_proj.shp"
mineedge=rutloc+"CreatedData/GIS/Mine_near_edge.dbf"
arcpy.GenerateNearTable_analysis(goldshape,rivfile,mineedge,"#","NO_LOCATION","NO_ANGLE","CLOSEST","0")


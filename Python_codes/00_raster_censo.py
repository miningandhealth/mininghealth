# Project to UTM18
arcpy.Project_management("Censo_minas","C:/Users/santi/Copy/PYP_Birth/CreatedData/GIS/Censo_minas_projutm18.shp","PROJCS['Bogota_UTM_Zone_18N',GEOGCS['GCS_Bogota',DATUM['D_Bogota',SPHEROID['International_1924',6378388.0,297.0]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]],PROJECTION['Transverse_Mercator'],PARAMETER['False_Easting',500000.0],PARAMETER['False_Northing',0.0],PARAMETER['Central_Meridian',-75.0],PARAMETER['Scale_Factor',0.9996],PARAMETER['Latitude_Of_Origin',0.0],UNIT['Meter',1.0]]","Bogota_To_WGS_1984","GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]]")

# Create buffer
arcpy.Buffer_analysis("Censo_minas_projutm18","C:/Users/santi/Copy/PYP_Birth/CreatedData/GIS/Censo_buffer1943m.shp","1943 Meters","FULL","ROUND","NONE","#")

# Create Raster
# Replace a layer/table view name with a path to a dataset (which can be a layer file) or create the layer/table view within the script
# The following inputs are layers or table views: "Censo_buffer1943m"

arcpy.PolygonToRaster_conversion("Censo_buffer1943m","idmina","C:/Users/santi/Copy/PYP_Birth/CreatedData/GIS/Censo_rast30","CELL_CENTER","NONE","30")
##This area calculates the mined area upstream... its unclear what the discount factor does...Need to fix this!


load("CreatedData/Municipios_GIS.RData")
load("CreatedData/COL_riv_proj.RData")

 

FIDEdgeVec=sort(COL_riv_proj$id_edge)
COL_riv_proj=COL_riv_proj[sort.int(COL_riv_proj$id_edge,index.return=T)$ix,]
##Here we calculate which edges dont have a predecesor, and therefore is where a river begins!
SalidasAlMar=setdiff(COL_riv_proj$id_end_vertex, COL_riv_proj$id_start_vertex)
ComienzoRio=setdiff(COL_riv_proj$id_start_vertex, COL_riv_proj$id_end_vertex)
FIDComienzo=COL_riv_proj$id_edge[match(ComienzoRio,COL_riv_proj$id_start_vertex)]

for(tipo in 1:4){
AreaMinadaFIDTiempo=matrix(0,ncol=length(2004:2014),nrow=length(FIDEdgeVec))
for(i in 2004:2014){
load(paste0("CreatedData/TMC_",i,"_Dump.RData"))
INDEX=which(TMC$DN==tipo)

if(length(INDEX)>0){
TMC=TMC[INDEX,]
TMC$area_km2=sapply(slot(TMC, "polygons"), slot, "area")/(1000^2)
AreaMinadaFechaFid=aggregate(TMC$area_km2,by=list(TMC$EdgeDump),FUN=sum)
AreaMinadaFIDTiempo[match(AreaMinadaFechaFid[,1],FIDEdgeVec),(i-2004+1)]=AreaMinadaFechaFid[,2]
}
rm(TMC)
}



#We create an empty matrix to store the results
AreaMinadaFIDTiempoAcumulada=matrix(NA,ncol=length(2004:2014),nrow=length(FIDEdgeVec))
AreaMinadaFIDTiempoAcumulada[match(FIDComienzo,FIDEdgeVec),]=AreaMinadaFIDTiempo[match(FIDComienzo,FIDEdgeVec),]



#This is the key function. It basically fills the upstream matrix, by looking intro predecesors and adding their polution (times the discount factor)

    
load("CreatedData/listEdgeDistMatAccumulada.RData")
load("CreatedData/listEdgeDistMat.RData")    
EncontrarPolucionArriba=function(id_edge,UpstreamIDs){
id_edge_pos=id_edge+1 #Esto es por que sortee la base para que fuera asi y nos ahorramos el match(id_edge,FIDEdgeVec)
if(is.na(AreaMinadaFIDTiempoAcumulada[id_edge_pos,1])){
  AreaAcum=AreaMinadaFIDTiempo[id_edge_pos,]
  FidsArriba=listEdgeDistMat_Upstream[[id_edge_pos]][,1]
  FidsArriba=FidsArriba[which(FidsArriba %in% UpstreamIDs)]
  for(fidA in FidsArriba){
    AreaAcum=AreaAcum+EncontrarPolucionArriba(fidA,UpstreamIDs)
  }
  return(AreaAcum)
}
else{
  return(AreaMinadaFIDTiempoAcumulada[id_edge_pos,])
}
}



#Now we apply the function
for( indexFid in setdiff(FIDEdgeVec,FIDComienzo)){
UpstreamInfo=listEdgeDistMatAccumulada_Upstream[[indexFid+1]]
UpstreamInfo=matrix(UpstreamInfo,ncol=3)
UpstreamInfo=UpstreamInfo[which(UpstreamInfo[,2]<max_dist),]
UpstreamInfo=matrix(UpstreamInfo,ncol=3)
AreaMinadaFIDTiempoAcumulada[indexFid+1,]=EncontrarPolucionArriba(indexFid,UpstreamInfo[,1])
}


AreaMinadaFIDTiempoUpstream=AreaMinadaFIDTiempoAcumulada-AreaMinadaFIDTiempo

#Lets plot it to make sure everything is fine! yes it looks fine!
save(FIDEdgeVec,AreaMinadaFIDTiempoUpstream,AreaMinadaFIDTiempoAcumulada,AreaMinadaFIDTiempo,file=paste0("CreatedData/AreaMinadaUpstrea_",tipo,".RData"))
}#close tipo




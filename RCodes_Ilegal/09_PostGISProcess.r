 ###################
 ########### PARA AREA
 ###########################
 load('CreatedData/Municipios_GIS.RData')



FechasVec=2004:2014
CodigosDane=unique(Municipios$CodigoDane)
 
#So this matrix has 3 dimension. dim 1 is the dates, dim 2 is the municipalities, dim 3 is the buffers (5, 10, 20 km)
MatrizExpoMuni=array(0,c(length(FechasVec),length(CodigosDane),3))

for(tipo in 1:4){
for(fecha in FechasVec){
MuniExposure=NULL
try(load(paste0("CreatedData/AreaExpMuniFecha_",fecha,"_Tipo",tipo,".RData")),T)
if(!is.null(MuniExposure)){
MuniExposure=data.frame(MuniExposure)


IndexMatch=match(CodigosDane,as.numeric(as.character(MuniExposure$CODANE2)))
MatrizExpoMuni[which(FechasVec==fecha),,1]=MuniExposure[IndexMatch,"Buffer5km"]
MatrizExpoMuni[which(FechasVec==fecha),,2]=MuniExposure[IndexMatch,"Buffer10km"]
MatrizExpoMuni[which(FechasVec==fecha),,3]=MuniExposure[IndexMatch,"Buffer20km"]
}
}
MatrizExpoMuni[which(is.na(MatrizExpoMuni))]=0

matplot(MatrizExpoMuni[,,1],type="l",lty=1,x=FechasVec,ylab="km^2")
save(CodigosDane,FechasVec,MatrizExpoMuni,file=paste0("CreatedData/MatrizExpoMuni",tipo,".RData"))
}



 

 ###################
 ########### PARA AREA UPSTREAM
 ########################### 

 
#So this matrix has 3 dimension. dim 1 is the dates, dim 2 is the municipalities, dim 3 is the buffers (5, 10, 20 km)
MatrizExpoUpstreamMuni=array(0,c(length(FechasVec),length(CodigosDane),3))

for(tipo in 1:4){
for(fecha in FechasVec){
MuniExposure=NULL
try(load(paste0("CreatedData/PolExpMuniFecha_",fecha,"_Tipo",tipo,"_All.RData")),T)
if(!is.null(MuniExposure)){
MuniExposure=data.frame(MuniExposure)


IndexMatch=match(CodigosDane,as.numeric(as.character(MuniExposure$CODANE2)))
MatrizExpoUpstreamMuni[which(FechasVec==fecha),,1]=MuniExposure[IndexMatch,"Buffer5km"]
MatrizExpoUpstreamMuni[which(FechasVec==fecha),,2]=MuniExposure[IndexMatch,"Buffer10km"]
MatrizExpoUpstreamMuni[which(FechasVec==fecha),,3]=MuniExposure[IndexMatch,"Buffer20km"]
}

}
MatrizExpoUpstreamMuni[which(is.na(MatrizExpoUpstreamMuni))]=0

matplot(MatrizExpoUpstreamMuni[,,1],type="l",lty=1,x=FechasVec,ylab="km^2")

save(CodigosDane,FechasVec,MatrizExpoUpstreamMuni,file=paste0("CreatedData/MatrizExpoUpstreamMuni",tipo,".RData"))
}

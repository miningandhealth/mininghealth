load("CreatedData/RasterizedStuff.RData")
load("CreatedData/COL_riv_proj.RData")
 load('CreatedData/Municipios_GIS.RData')
  load("CreatedData/MINAS_ORO.RData")
  ############ PARA AREA

Municipios<-readShapePoly("RawData/GIS/COL_adm/COL_adm2")
prj.from<-CRS(proj4string(readOGR(dsn="RawData/GIS/COL_adm",layer="COL_adm2")))
proj4string(Municipios)=prj.from
Municipios=spTransform(Municipios,CRS(projectionAll))
Colombia=unionSpatialPolygons(Municipios,ID=rep(1,dim(Municipios)[1]),threshold=10000)
Colombia=crop(Colombia, COL_riv_proj)


pdf("Results/Maps/RiverData.pdf")
plot(Colombia,main="Rivers in Colombia")
plot(COL_riv_proj,add=T,col=4)
dev.off()


pdf("Results/Maps/MiningTitles.pdf")
plot(Colombia,main="Mining titles")
plot(MINAS_ORO,add=T,col=2)
dev.off()


load("CreatedData/MINAS_ORO_Dump.RData")
ArchiviosMinasActivas=dir("CreatedData/AreaUpstreamRiver/", pattern="AreaMinadaEdgeFecha_", full.names = TRUE)
ArchiviosMinasActivas=sort(ArchiviosMinasActivas)

    archivo=ArchiviosMinasActivas[10]
    t0=Sys.time()
    fechatable=read.dbf(archivo)
    MINASPLOT=MINAS_ORO[MINAS_ORO$EdgeDump %in% fechatable$id_edge,]
    Municipios2=crop(Municipios,extent(MINASPLOT)+100000)
    COL_riv_proj2=crop(COL_riv_proj,extent(MINASPLOT)+100000)
    plot(Municipios2,lwd=2)
    plot(MINASPLOT,add=T,col=3)
    plot(COL_riv_proj2,add=T,col=4)
    text(coordinates(Municipios2), labels=Municipios2$NAME_2)


    pdf("Resutext(coordinates(municipios_pathrows), labels=paste(municipios_pathrows$PATH,municipios_pathrows$ROW, sep=', '))ts/Graphs/MunicipioMinaRios.pdf")
    plot(Municipios2,lwd=2)
    plot(MINASPLOT,add=T,col=3)
    plot(COL_riv_proj2,add=T,col=4)
    dev.off()

    riverfile_merge=merge(COL_riv_proj,fechatable)
    riverfile_merge = subset(riverfile_merge, !is.na(riverfile_merge@data$polucion))
    riverfile_merge=riverfile_merge[,c("id_edge","polucion")]
    riverfile_buffer1 <- gBuffer(riverfile_merge, width=5*1000,byid=T,capStyle="FLAT") #units are in mts
    RasterGold1=rasterize(riverfile_buffer1,RasterPop,field="polucion",background=0)


    pdf("Results/Maps/MunicipioMinaRios_Buffer.pdf")
    riverfile_buffercrop=crop(riverfile_buffer2,extent(MINASPLOT)+100000)
    riverfile_buffercrop=gUnaryUnion(riverfile_buffercrop)
    plot(Municipios2)
    plot(riverfile_buffercrop,col="lightgray",add=T)
    plot(Municipios2,lwd=2,add=T)
    plot(MINASPLOT,add=T,col=3)
    plot(COL_riv_proj2,add=T,col=4)
    dev.off()

    pdf("Results/Maps/MunicipioMinaRios_Poblacion.pdf")
    RasterPopCrop=crop(RasterPop,extent(MINASPLOT)+1000000)
    plot(RasterPopCrop)
    plot(Municipios2,lwd=2,add=T)
    plot(MINASPLOT,add=T,col=3)
    plot(COL_riv_proj2,add=T,col=4)
    dev.off()

    pdf("Results/Maps/MunicipioMinaRios_Exposure.pdf")
    RasterExposureCrop=crop(RasterExposure1,extent(MINASPLOT)+100000)
    plot(Municipios2)
    plot(RasterExposureCrop,col="lightgray",add=T)
    plot(Municipios2,lwd=2,add=T)
    plot(MINASPLOT,add=T,col=3)
    plot(COL_riv_proj2,add=T,col=4)
    dev.off()





library(rgeos)
library(rgdal)
library(maptools)
setwd("C:/Users/santi/Dropbox/MiningAndBirth_Ilegal")
projectionAll="+proj=utm +zone=18 +datum=WGS84 +units=m +no_defs +ellps=WGS84 +towgs84=0,0,0"

minerpot = readOGR(dsn='RawData/GIS',layer='Minerals')
minerpot<-spTransform(minerpot,projectionAll)

oro_pot=minerpot[minerpot$type=='Gold',]
rm(minerpot)
oro_pot=gSimplify(oro_pot, tol=1000, topologyPreserve=T)

titulos = readOGR(dsn='RawData/GIS',layer='TITULOS_PAIS')
titulos<-spTransform(titulos,projectionAll)
titulos_oro=titulos[grepl("ORO",titulos$MINERALES)==TRUE,]
rm(titulos)

unodc = readOGR(dsn='RawData/GIS',layer='UNODC_digimap')
unodc<-spTransform(unodc,projectionAll)
plot(oro_pot, border="red", col="white", axes=TRUE)
plot(titulos_oro, add=TRUE)
plot(unodc,border="blue", add=TRUE)
titulos_y_unodc=gUnion(titulos_oro,unodc)


###

load('CreatedData/TMC_2010.RData')
TMC<-spTransform(TMC,projectionAll)
summary(TMC$DN)
plot(TMC[TMC$DN==1,],axes=TRUE)
plot(TMC[TMC$DN==2,],border="red", add=TRUE)


TMCcut <- crop(TMC,extent(726000 , 7285000, 912500, 915500))
oro_cut <- crop(titulos_oro,extent(726000 , 7285000, 912500, 915500))

plot(oro_cut,axes=TRUE)
plot(TMCcut,border="red", add=TRUE)



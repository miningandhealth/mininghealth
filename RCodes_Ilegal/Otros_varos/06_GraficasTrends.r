library(foreign)
rm(list=ls())
#setwd('Z:/MiningAndBirth')
library(stargazer)
setwd('E:/Copy/MiningAndBirth')
load('CreatedData/DataCompletaIntensity.RData')
load('CreatedData/AreaMunicipio.RData')
DataCompleta$AreaMunicipioTotal=AreaMunicipio[match(DataCompleta$CODIGO_DANE,AreaMunicipio$CODANE2),"AREA"]
DataCompleta$AreaMinadaProp=(111319.9^2)*DataCompleta$AreaMinada/DataCompleta$AreaMunicipioTotal
DataCompleta$AreaMinadaProp2=(DataCompleta$AreaMinadaProp)^2
DataCompleta$CODIGO_DANE=as.factor(DataCompleta$CODIGO_DANE)
DataCompleta$APGAR_BAJO=as.numeric(as.character(DataCompleta$APGAR1)=="1" | as.character(DataCompleta$APGAR1)=="2")
CODIGOS_MINA=DataCompleta[which(DataCompleta$AreaMinada>0),"CODIGO_DANE"]
DataCompleta$INDEX_MUNMINERO=as.numeric(DataCompleta$CODIGO_DANE %in% CODIGOS_MINA)
DataCompleta$CODIGO_DANE=droplevels(DataCompleta$CODIGO_DANE)


DataCompleta2=DataCompleta[-which(DataCompleta$AreaMinada>0),]
DataCompleta2$TimeTrend=as.numeric(DataCompleta2$FECHA_NAC)
A=lm(APGAR_BAJO~TimeTrend*INDEX_MUNMINERO,data=DataCompleta2)
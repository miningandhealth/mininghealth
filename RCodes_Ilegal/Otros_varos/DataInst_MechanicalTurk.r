rm(list=ls())
setwd('E:/Copy/MiningAndBirth')
load('CreatedData/DataInst.RData')
DataInst$CODIGO_DANE=paste(DataInst$DPTO_NACI,DataInst$MUNIC_NACI,sep="")
DIVIPOLA_20130930=read.csv('RawData/DIVIPOLA_20130930.csv')

DIVIPOLA_20130930=DIVIPOLA_20130930[which(DIVIPOLA_20130930$Tipo=="CM" | DIVIPOLA_20130930$Tipo=="CD"),]
DIVIPOLA_20130930$Nombre.Municipio=chartr('������������','aeiouuAEIOUU',DIVIPOLA_20130930$Nombre.Municipio)
DIVIPOLA_20130930$Nombre.Departamento=as.character(DIVIPOLA_20130930$Nombre.Departamento)
DIVIPOLA_20130930$Nombre.Departamento=chartr('������������','aeiouuAEIOUU',DIVIPOLA_20130930$Nombre.Departamento)
DIVIPOLA_20130930$Municipio=DIVIPOLA_20130930$Nombre.Municipio
DIVIPOLA_20130930$Departamento=DIVIPOLA_20130930$Nombre.Departamento
DIVIPOLA_20130930$CodigoDane=DIVIPOLA_20130930$C�digo.Municipio


DIVIPOLA_20130930$Municipio=tolower(DIVIPOLA_20130930$Municipio)
DIVIPOLA_20130930$Departamento=tolower(DIVIPOLA_20130930$Departamento)

DIVIPOLA_20130930=DIVIPOLA_20130930[,c("Departamento","Municipio","CodigoDane")]

DataInst$CODIGO_DANE=as.numeric(DataInst$CODIGO_DANE)
DataInst[,c("Departamento","Municipio")]=DIVIPOLA_20130930[match(DataInst$CODIGO_DANE,DIVIPOLA_20130930$CodigoDane),c("Departamento","Municipio")]
DataInst=DataInst[,3:6] 
DataInst=DataInst[which(nchar(DataInst$NOM_INST)>10),]  
DataInst$NOM_INST=gsub("IPS", "IPS", DataInst$NOM_INST)
DataInst$NOM_INST=gsub("I.P.S.", "IPS", DataInst$NOM_INST)
DataInst$NOM_INST=gsub("I.PS", "IPS", DataInst$NOM_INST)
DataInst$NOM_INST=gsub("I.P.S", "IPS", DataInst$NOM_INST)
DataInst$NOM_INST=gsub("IP.S.", "IPS", DataInst$NOM_INST)
DataInst$NOM_INST=gsub("I.PS.", "IPS", DataInst$NOM_INST)

DataInst$NOM_INST=gsub("I PS", "IPS", DataInst$NOM_INST)
DataInst$NOM_INST=gsub("I. P.S.", "IPS", DataInst$NOM_INST)
DataInst$NOM_INST=gsub("I. PS", "IPS", DataInst$NOM_INST)
DataInst$NOM_INST=gsub("I. P.S", "IPS", DataInst$NOM_INST)
DataInst$NOM_INST=gsub("I P.S.", "IPS", DataInst$NOM_INST)
DataInst$NOM_INST=gsub("I. PS.", "IPS", DataInst$NOM_INST)
DataInst$NOM_INST=gsub("I PS", "IPS", DataInst$NOM_INST)
  
DataInst$NOM_INST=gsub("C.L. ", "CLINICA ", DataInst$NOM_INST)
DataInst$NOM_INST=gsub("C.L ", "CLINICA ", DataInst$NOM_INST)
DataInst$NOM_INST=gsub("CL. ", "CLINICA ", DataInst$NOM_INST)
DataInst$NOM_INST=gsub("CL ", "CLINICA ", DataInst$NOM_INST)

DataInst$NOM_INST=gsub("P.S. ", "Puesto de salud ", DataInst$NOM_INST)
DataInst$NOM_INST=gsub("P.S ", "Puesto de salud ", DataInst$NOM_INST)
DataInst$NOM_INST=gsub("PS. ", "Puesto de salud ", DataInst$NOM_INST)
DataInst$NOM_INST=gsub("PS ", "Puesto de salud ", DataInst$NOM_INST)
DataInst$NOM_INST=gsub("P.S. ", "Puesto de salud ", DataInst$NOM_INST)

DataInst$NOM_INST=gsub("C.S. ", "Centro de Salud ", DataInst$NOM_INST)
DataInst$NOM_INST=gsub("C.S ", "Centro de Salud ", DataInst$NOM_INST)
DataInst$NOM_INST=gsub("CS. ", "Centro de Salud ", DataInst$NOM_INST)
DataInst$NOM_INST=gsub("CS ", "Centro de Salud ", DataInst$NOM_INST)
DataInst$NOM_INST=gsub("C.M. ", "Centro de Medico ", DataInst$NOM_INST)
DataInst$NOM_INST=gsub("C.M ", "Centro Medico ", DataInst$NOM_INST)
DataInst$NOM_INST=gsub("CM. ", "Centro Medico ", DataInst$NOM_INST)
DataInst$NOM_INST=gsub("CM ", "Centro Medico ", DataInst$NOM_INST)
DataInst$NOM_INST=gsub("SALU", "SALUD", DataInst$NOM_INST)
DataInst$NOM_INST=gsub("SALUF", "SALUD", DataInst$NOM_INST)
DataInst$NOM_INST=gsub("CLNCA", "CLINICA", DataInst$NOM_INST)



DataInst=DataInst[-which(DataInst$NOM_INST=="SIN INFORMACION"),]
DataInst=DataInst[-which(DataInst$NOM_INST=="NO ESPECIFICADA"),]
DataInst=DataInst[-which(DataInst$NOM_INST=="NO TIENE CODIGO"),]





DataInst=DataInst[-which(DataInst$Municipio=="bogota, d.c." |  DataInst$Municipio=="medellin"  |  DataInst$Municipio=="cali" |  DataInst$Municipio=="barranquilla"   |  DataInst$Municipio=="cartagena"),]

DataInst=DataInst[sort.int(DataInst$CODIGO_DANE,index.return=T)$ix,]
DataInst=unique(DataInst)


#DataInst=DataInst[sort.int(nchar(DataInst$NOM_INST),index.return=T)$ix,]




colnames(DataInst)=c("name","CODIGO_DANE","departamento","municipio")

DataInst$name=chartr('��������������','aeiouuAEIOUUnN',DataInst$name)
DataInst$municipio=chartr('��������������','aeiouuAEIOUUnN',DataInst$municipio)
DataInst$name=gsub("�", "", DataInst$name)
DataInst$name=gsub("�", "a", DataInst$name)
DataInst$name=gsub("�", "a", DataInst$name)

DataInst=unique(DataInst)
write.csv(DataInst[1:900,c(1,3,4)],file="prueba.csv",row.names=F)









################3


rm(list=ls())
setwd('E:/Copy/MiningAndBirth')
Batch=read.csv('CreatedData/BatchResults/Batch_1510715_batch_results.csv')


FunText=function(x){
as.numeric(unlist(strsplit(as.character(x),"\\|")))
}



MatrixRespuestas=sapply(Batch$Answer.Q1Url,FunText)

Batch$Reject[which(colSums(is.na(MatrixRespuestas))>0)]="Not the right format. Please use decimal notation. Precede South latitudes and West longitudes with a minus sign. Please do not use N,S,W,E indicators."

Batch$Reject[which( as.numeric(MatrixRespuestas[2,])< -80 | as.numeric(MatrixRespuestas[2,])> -67.5 | as.numeric(MatrixRespuestas[1,])< -4.5 | as.numeric(MatrixRespuestas[1,])> 12.5)]="coordinates not in Colombia"

TablaIDs=table(Batch$HITId[which((Batch$Reject!="coordinates not in Colombia" & Batch$Reject!="Not the right format. Please use decimal notation. Precede South latitudes and West longitudes with a minus sign. Please do not use N,S,W,E indicators.") | is.na(Batch$Reject))])
for(ID in names(TablaIDs)[which(TablaIDs==2)]){
Resp=MatrixRespuestas[,which(Batch$HITId==ID)]
Resp=matrix(as.numeric(Resp),ncol=2)
if(sum(is.na(Resp))==0 & (abs(Resp[1,1]-Resp[1,2])>0.05 | abs(Resp[2,1]-Resp[2,2])>0.05)){
print("diff") 
Batch$Reject[which(Batch$HITId==ID)]="Reponses between differnt workers are different"
}
}



write.csv(Batch,'CreatedData/BatchResults/Batch_1510715_batch_results_answer.csv',row.names=F)



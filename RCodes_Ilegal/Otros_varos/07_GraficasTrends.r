
rm(list=ls())

#setwd('E:/Copy/MiningAndBirth')
setwd('Z:/MiningAndBirth')

library(foreign)
library(plyr)
library(stargazer)

load('CreatedData/BaseNacimientosRelevantes.RData')
BaseNacimientosRelevantes=BaseNacimientosRelevantes[which(BaseNacimientosRelevantes$Fecha_Mineria>=as.Date("01-01-1999",format="%d-%m-%Y")),]
BaseNacimientosRelevantes$APGAR_BAJO=as.numeric(as.character(BaseNacimientosRelevantes$APGAR1)=="1" | as.character(BaseNacimientosRelevantes$APGAR1)=="2")
BaseNacimientosRelevantes$PostTratamiento=as.numeric(BaseNacimientosRelevantes$DiasTratamiento>0)


BaseNacimientosRelevantes$Mina=NA
BaseNacimientosRelevantes$Mina[which(BaseNacimientosRelevantes$StatusTreatment==0)]=1
BaseNacimientosRelevantes$Mina[which(BaseNacimientosRelevantes$StatusTreatment==-1)]=0
BaseNacimientosRelevantes$Mina[which(BaseNacimientosRelevantes$StatusTreatment==-2)]=0
BaseNacimientosRelevantes$Downstream=NA
BaseNacimientosRelevantes$Downstream[which(BaseNacimientosRelevantes$StatusTreatment==1)]=1
BaseNacimientosRelevantes$Downstream[which(BaseNacimientosRelevantes$StatusTreatment==2)]=1
BaseNacimientosRelevantes$Downstream[which(BaseNacimientosRelevantes$StatusTreatment==-1)]=0
BaseNacimientosRelevantes$Downstream[which(BaseNacimientosRelevantes$StatusTreatment==-2)]=0

BaseNacimientosRelevantes$CODIGO_DANE=as.factor(BaseNacimientosRelevantes$CODIGO_DANE)





Agregado=aggregate(BaseNacimientosRelevantes$APGAR_BAJO,by=list(BaseNacimientosRelevantes$DiasTratamiento,BaseNacimientosRelevantes$Mina),mean)
pdf("Results/Fig1.pdf")
plot(lowess(Agregado[which(Agregado[,2]==0),c(1,3)],f=2/3),ylim=c(0.04,0.1),type="l",lwd=2,xlab="Days before/after mine",ylab="Low APGAR",main="Low APGAR by days before/after mine")
lines(lowess(Agregado[which(Agregado[,2]==1),c(1,3)],f=2/3),col=2,lwd=2)
legend("topright",c("Control","Mine"),col=1:2,lwd=2,pch=19)
dev.off()


Agregado=aggregate(BaseNacimientosRelevantes$APGAR_BAJO,by=list(BaseNacimientosRelevantes$DiasTratamiento,BaseNacimientosRelevantes$Downstream),mean)
pdf("Results/Fig2.pdf")
plot(lowess(Agregado[which(Agregado[,2]==0),c(1,3)],f=2/3),ylim=c(0.04,0.1),type="l",lwd=2,xlab="Days before/after mine",ylab="Low APGAR",main="Low APGAR by days before/after mine")
lines(lowess(Agregado[which(Agregado[,2]==1),c(1,3)],f=2/3),col=2,lwd=2)
legend("topright",c("Control","Downstream"),col=1:2,lwd=2,pch=19)
dev.off()

rm(list=ls())

setwd('E:/Copy/MiningAndBirth')
setwd('Z:/MiningAndBirth')

library(foreign)
library(plyr)
library(stargazer)

load('CreatedData/BaseNacimientosRelevantes2.RData')
BaseNacimientosRelevantes=BaseNacimientosRelevantes[which(BaseNacimientosRelevantes$Fecha_Mineria>=as.Date("01-01-1999",format="%d-%m-%Y")),]
BaseNacimientosRelevantes$APGAR_BAJO=as.numeric(as.character(BaseNacimientosRelevantes$APGAR1)=="1" | as.character(BaseNacimientosRelevantes$APGAR1)=="2")
BaseNacimientosRelevantes$PostTratamiento=as.numeric(BaseNacimientosRelevantes$DiasTratamiento>0)


BaseNacimientosRelevantes$Mina=NA
BaseNacimientosRelevantes$Mina[which(BaseNacimientosRelevantes$StatusTreatment==0)]=1
BaseNacimientosRelevantes$Mina[which(BaseNacimientosRelevantes$StatusTreatment==-1)]=0
BaseNacimientosRelevantes$Mina[which(BaseNacimientosRelevantes$StatusTreatment==-2)]=0
BaseNacimientosRelevantes$Downstream=NA
BaseNacimientosRelevantes$Downstream[which(BaseNacimientosRelevantes$StatusTreatment==1)]=1
BaseNacimientosRelevantes$Downstream[which(BaseNacimientosRelevantes$StatusTreatment==2)]=1
BaseNacimientosRelevantes$Downstream[which(BaseNacimientosRelevantes$StatusTreatment==-1)]=0
BaseNacimientosRelevantes$Downstream[which(BaseNacimientosRelevantes$StatusTreatment==-2)]=0

BaseNacimientosRelevantes$CODIGO_DANE=as.factor(BaseNacimientosRelevantes$CODIGO_DANE)




Agregado=aggregate(BaseNacimientosRelevantes$APGAR_BAJO,by=list(BaseNacimientosRelevantes$DiasTratamiento,BaseNacimientosRelevantes$Mina),mean)
pdf("Results/Fig1_ControlLimpio.pdf")
plot(lowess(Agregado[which(Agregado[,2]==0),c(1,3)],f=2/3),ylim=c(0.04,0.1),type="l",lwd=2,xlab="Days before/after mine",ylab="Low APGAR",main="Low APGAR by days before/after mine")
lines(lowess(Agregado[which(Agregado[,2]==1),c(1,3)],f=2/3),col=2,lwd=2)
legend("topright",c("Control","Mine"),col=1:2,lwd=2,pch=19)
dev.off()


Agregado=aggregate(BaseNacimientosRelevantes$APGAR_BAJO,by=list(BaseNacimientosRelevantes$DiasTratamiento,BaseNacimientosRelevantes$Downstream),mean)
pdf("Results/Fig2_ControlLimpio.pdf")
plot(lowess(Agregado[which(Agregado[,2]==0),c(1,3)],f=2/3),ylim=c(0.04,0.1),type="l",lwd=2,xlab="Days before/after mine",ylab="Low APGAR",main="Low APGAR by days before/after mine")
lines(lowess(Agregado[which(Agregado[,2]==1),c(1,3)],f=2/3),col=2,lwd=2)
legend("topright",c("Control","Downstream"),col=1:2,lwd=2,pch=19)
dev.off()


rm(list=ls())

setwd('E:/Copy/MiningAndBirth')
#setwd('Z:/MiningAndBirth')

library(foreign)
library(plyr)
library(stargazer)

load('CreatedData/BaseNacimientosRelevantes3.RData')
BaseNacimientosRelevantes=BaseNacimientosRelevantes[which(BaseNacimientosRelevantes$Fecha_Mineria>=as.Date("01-01-1999",format="%d-%m-%Y")),]
BaseNacimientosRelevantes$APGAR_BAJO=as.numeric(as.character(BaseNacimientosRelevantes$APGAR1)=="1" | as.character(BaseNacimientosRelevantes$APGAR1)=="2")
BaseNacimientosRelevantes$PostTratamiento=as.numeric(BaseNacimientosRelevantes$DiasTratamiento>0)


BaseNacimientosRelevantes$Mina=NA
BaseNacimientosRelevantes$Mina[which(BaseNacimientosRelevantes$StatusTreatment==0)]=1
BaseNacimientosRelevantes$Mina[which(BaseNacimientosRelevantes$StatusTreatment==-1)]=0
BaseNacimientosRelevantes$Mina[which(BaseNacimientosRelevantes$StatusTreatment==-2)]=0
BaseNacimientosRelevantes$Downstream=NA
BaseNacimientosRelevantes$Downstream[which(BaseNacimientosRelevantes$StatusTreatment==1)]=1
BaseNacimientosRelevantes$Downstream[which(BaseNacimientosRelevantes$StatusTreatment==2)]=1
BaseNacimientosRelevantes$Downstream[which(BaseNacimientosRelevantes$StatusTreatment==-1)]=0
BaseNacimientosRelevantes$Downstream[which(BaseNacimientosRelevantes$StatusTreatment==-2)]=0

BaseNacimientosRelevantes$CODIGO_DANE=as.factor(BaseNacimientosRelevantes$CODIGO_DANE)


#lines(lowess(cbind(Agregado[which(Agregado[,2]==1),c(1)],Agregado[which(Agregado[,2]==0),c(3)]-Agregado[which(Agregado[,2]==1),c(3)])))
#
Agregado=aggregate(BaseNacimientosRelevantes$APGAR_BAJO,by=list(BaseNacimientosRelevantes$DiasTratamiento,BaseNacimientosRelevantes$Mina),mean)
pdf("Results/Fig1_ControlLimpio2.pdf")
plot(lowess(Agregado[which(Agregado[,2]==0),c(1,3)],f=2/3),ylim=c(0.04,0.1),type="l",lwd=2,xlab="Days before/after mine",ylab="Low APGAR",main="Low APGAR by days before/after mine")
lines(lowess(Agregado[which(Agregado[,2]==1),c(1,3)],f=2/3),col=2,lwd=2)
legend("topright",c("Control","Mine"),col=1:2,lwd=2,pch=19)
dev.off()


Agregado=aggregate(BaseNacimientosRelevantes$APGAR_BAJO,by=list(BaseNacimientosRelevantes$DiasTratamiento,BaseNacimientosRelevantes$Downstream),mean)
pdf("Results/Fig2_ControlLimpio2.pdf")
plot(lowess(Agregado[which(Agregado[,2]==0),c(1,3)],f=2/3),ylim=c(0.04,0.1),type="l",lwd=2,xlab="Days before/after mine",ylab="Low APGAR",main="Low APGAR by days before/after mine")
lines(lowess(Agregado[which(Agregado[,2]==1),c(1,3)],f=2/3),col=2,lwd=2)
legend("topright",c("Control","Downstream"),col=1:2,lwd=2,pch=19)
dev.off()


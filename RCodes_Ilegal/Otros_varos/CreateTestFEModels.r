rm(list=ls())
setwd("E:/Copy/MiningAndBirth")
setwd("C:/Users/Mauricio/Copy/MiningAndBirth")
dir_git="C:/Users/Mauricio/Documents/git/mininghealth/Rcodes/"
CRANMirror="http://cran.stat.ucla.edu/"


if (is.element("foreign",installed.packages())==0)  install.packages("foreign" , repos = CRANMirror)
if (is.element("plyr",installed.packages())==0)  install.packages("plyr" , repos = CRANMirror)
if (is.element("xtable",installed.packages())==0)  install.packages("xtable" , repos = CRANMirror)
if (is.element("stargazer",installed.packages())==0)  install.packages("stargazer" , repos = CRANMirror)
if (is.element("data.table",installed.packages())==0)  install.packages("data.table" , repos = CRANMirror)
if (is.element("zoo",installed.packages())==0)  install.packages("zoo" , repos = CRANMirror)

update.packages("foreign" , repos = CRANMirror)
update.packages("plyr" , repos = CRANMirror)
update.packages("xtable" , repos = CRANMirror)
update.packages("stargazer" , repos = CRANMirror)
update.packages("data.table" , repos = CRANMirror)
update.packages("zoo" , repos = CRANMirror)

library(foreign)
library(plyr)
library(xtable)
library(stargazer)
library(data.table)
library(zoo)


Data=NULL
Data$Year=rep(1:10,500)
Data=data.frame(Data)
Data$ID=rep(1:500,each=10)
Data$FE=rnorm(dim(Data)[1],mean=5)
Data$FET=rnorm(dim(Data)[1],mean=3)
Data$x=rnorm(dim(Data)[1],mean=10)
beta_real=-3.4
Data$y=Data$FET*Data$Year+Data$FE+Data$x*beta_real+rnorm(dim(Data)[1])+(Data$Year)*rnorm(10)

write.dta(Data,file="test_FDFE.dta")



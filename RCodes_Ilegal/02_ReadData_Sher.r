 ###########################
##The POPULATION  data
###########################
x <- new("GDALReadOnlyDataset", "RawData/GIS/col_gpwv3_pcount_wrk_25/colcount/colp00ag")
xx<-asSGDF_GROD(x)
RasterPop <- raster(xx)
RasterPop=projectRaster(RasterPop, crs=CRS(projectionAll))
save(RasterPop,file="CreatedData/RasterPop.RData")

###########################
##The RIVER data
###########################
COL_riv_proj <- readOGR("RawData/GIS","COL_riv_proj")
COL_riv_proj <- spTransform(COL_riv_proj, CRS(projectionAll))

Coords=rbind(cbind(COL_riv_proj$xstart,COL_riv_proj$ystart),
cbind(COL_riv_proj$xend,COL_riv_proj$yend))
Coords=unique(Coords)
Coords=data.frame(Coords)
colnames(Coords)=c("x","y")
Coords$ID_vertex=seq(1,dim(Coords)[1])
colnames(Coords)=c("xstart","ystart","id_start_vertex")
COL_riv_proj=merge(COL_riv_proj,Coords)
colnames(Coords)=c("xend","yend","id_end_vertex")
COL_riv_proj=merge(COL_riv_proj,Coords)
save(COL_riv_proj,file="CreatedData/COL_riv_proj.RData")

###########################
##The municipality data
###########################

##Now we need to find out how mines are split by municipality...
dpto=dir(path = "RawData/GIS/MGN")[1]
Municipios=readOGR(paste0("RawData/GIS/MGN/",dpto), sub("^([^.]*).*", "\\1",dir(paste0("RawData/GIS/MGN/",dpto))[grep("MPIO",dir(paste0("RawData/GIS/MGN/",dpto)))])[1])
Municipios <- spTransform(Municipios, CRS(projectionAll))
for(dpto in dir(path = "RawData/GIS/MGN")[-1]){
Temp = readOGR(paste0("RawData/GIS/MGN/",dpto), sub("^([^.]*).*", "\\1",dir(paste0("RawData/GIS/MGN/",dpto))[grep("MPIO",dir(paste0("RawData/GIS/MGN/",dpto)))])[1])
Temp <- spTransform(Temp, CRS(projectionAll))
row.names(Temp)=as.character(seq(dim(Municipios)[1]+1,length.out=dim(Temp)[1]))
Municipios=spRbind(Municipios, Temp)
}
DataKeep=Municipios@data 
Municipios=gBuffer(Municipios,byid=T,width=0)
Municipios=SpatialPolygonsDataFrame(Municipios, DataKeep, match.ID = TRUE)
Municipios$CODANE2=as.numeric(as.character(Municipios$MPIO_CCNCT))
Municipios$CodigoDane=as.numeric(as.character(Municipios$MPIO_CCNCT))
Municipios$area_m2=sapply(slot(Municipios, "polygons"), slot, "area")
Municipios$area_km2=Municipios$area_m2/(1000^2) #para que quede en sqkm
save(Municipios,file='CreatedData/Municipios_GIS.RData')
write.csv(Municipios,file='CreatedData/AreaMunicipio.csv',row.names=F)
  
        

###This codes reads all other data... in particular it reads
###The municipality data (below)
#


###########################
##The VITAL STATISTICS DATA
###########################
 
 
Data1998=read.dbf('RawData/Nacimientos/nac1998.dbf', as.is = FALSE)
Data1998$FECHA_NAC =as.Date(Data1998$FECHA_NAC,format="%Y-%m-%d")
Data1998$FECHA_ANTN =as.Date(Data1998$FECHA_ANTN,format="%Y-%m-%d")
Data1998$FECHA_EXP =as.Date(Data1998$FECHA_EXP,format="%Y-%m-%d")

Data1998$AREA_NACI[which(Data1998$AREA_NACI=='9')]=NA
Data1998$SIT_PARTO[which(Data1998$SIT_PARTO=='9')]=NA
Data1998$PESO_NAC[which(Data1998$PESO_NAC==9999)]=NA
Data1998$TALLA_NAC[which(Data1998$TALLA_NAC==99)]=NA
Data1998$PESO_NAC[which(Data1998$PESO_NAC==9998)]=NA
Data1998$TALLA_NAC[which(Data1998$TALLA_NAC==98)]=NA
Data1998$ATEN_PAR[which(Data1998$ATEN_PAR=='9')]=NA
Data1998$T_GES[which(Data1998$T_GES=='9')]=NA
Data1998$T_GES[which(Data1998$T_GES=='6')]=NA
Data1998$T_GES[which(Data1998$T_GES=='8')]=NA
Data1998$NUM_CONSUL[which(Data1998$NUM_CONSUL==99)]=NA
Data1998$NUM_CONSUL[which(Data1998$NUM_CONSUL=='99')]=NA
Data1998$T_GES[which(Data1998$T_GES=='.')]=NA
Data1998$T_GES=as.numeric(as.character(Data1998$T_GES)) 


Data1998$TIPO_PARTO[which(Data1998$TIPO_PARTO=='9')]=NA
Data1998$MUL_PARTO[which(Data1998$MUL_PARTO=='9')]=NA
Data1998$APGAR1[which(Data1998$APGAR1=='9')]=NA
Data1998$APGAR2[which(Data1998$APGAR2=='9')]=NA
Data1998$GRU_SAN[which(Data1998$GRU_SAN=='9')]=NA
Data1998$SEG_SOCIAL[which(Data1998$SEG_SOCIAL=='9')]=NA
Data1998$EDAD_MADRE[which(Data1998$EDAD_MADRE==99)]=NA
Data1998$EST_CIVM[which(Data1998$EST_CIVM=='9')]=NA
Data1998$NIV_EDUM[which(Data1998$NIV_EDUM=='9')]=NA
Data1998$NIV_EDUM=revalue(Data1998$NIV_EDUM, c("1"="0","8"="0","2"="1", "3"="1", "4"="2", "5"="2", "6"="3", "7"="3"))



Data1998$DPTO_R[which(Data1998$DPTO_R=='01')]=NA
Data1998$MUNIC_R[which(Data1998$MUNIC_R=='999')]=NA
Data1998$AREA_R[which(Data1998$AREA_R=='9')]=NA
Data1998$N_HIJOSV[which(Data1998$N_HIJOSV==99)]=NA
Data1998$N_EMB[which(Data1998$N_EMB==99)]=NA
Data1998$EDAD_PADRE[which(Data1998$EDAD_PADRE==99)]=NA
Data1998$N_EMB[which(Data1998$N_EMB==99)]=NA
Data1998$NIV_EDUP[which(Data1998$NIV_EDUP=='9')]=NA
Data1998$PROFESION[which(Data1998$PROFESION=='9')]=NA

Data1998$APGAR_CONTINUO=NA
Data1998$SEMANAS=NA
Data1998$CODPRES=NA
Data1998$COD_INST=NA
Data1998$NOM_INST=NA
Data1998$COD_REGION=NA
Data1998$VALIDAR=NA


Data1999=read.dbf('RawData/Nacimientos/nac1999.dbf', as.is = FALSE)

Data1999=rename(Data1999, c("COD_MUNIC"="MUNIC_NACI", "COD_INSP"="INSP_NACI", "COD_DPTO"="DPTO_NACI",
"AREANAC"="AREA_NACI", "NUMCONSUL"="NUM_CONSUL", "CODPTORE"="DPTO_R",
"CODMUNRE"="MUNIC_R", "AREA_RES"="AREA_R", "CODIGO"="CODIGO_CP", "FECHA_NACM"="FECHA_ANTN"))

Data1999$FECHA_NAC =as.Date(Data1999$FECHA_NAC,format="%Y-%m-%d")
Data1999$FECHA_ANTN =as.Date(Data1999$FECHA_ANTN,format="%Y-%m-%d")
Data1999$FECHA_EXP =as.Date(Data1999$FECHA_EXP,format="%Y-%m-%d")

Data1999$AREA_NACI[which(Data1999$AREA_NACI=='9')]=NA
Data1999$SIT_PARTO[which(Data1999$SIT_PARTO=='9')]=NA
Data1999$PESO_NAC[which(Data1999$PESO_NAC==9999)]=NA
Data1999$TALLA_NAC[which(Data1999$TALLA_NAC==99)]=NA
Data1999$ATEN_PAR[which(Data1999$ATEN_PAR=='9')]=NA
Data1999$T_GES[which(Data1999$T_GES=='9')]=NA
#Data1999$SEMANAS[which(Data1999$SEMANAS=='6')]=NA
Data1999$T_GES[which(Data1999$T_GES=='6')]=NA
Data1999$NUM_CONSUL[which(Data1999$NUM_CONSUL==99)]=NA
Data1999$NUM_CONSUL[which(Data1999$NUM_CONSUL=='99')]=NA
Data1999$T_GES=as.numeric(as.character(Data1999$T_GES)) 

Data1999$TIPO_PARTO[which(Data1999$TIPO_PARTO=='9')]=NA
Data1999$MUL_PARTO[which(Data1999$MUL_PARTO=='9')]=NA
Data1999$APGAR1[which(Data1999$APGAR1=='9')]=NA
Data1999$APGAR2[which(Data1999$APGAR2=='9')]=NA
Data1999$GRU_SAN[which(Data1999$GRU_SAN=='9')]=NA
Data1999$SEG_SOCIAL[which(Data1999$SEG_SOCIAL=='9')]=NA
Data1999$EDAD_MADRE[which(Data1999$EDAD_MADRE==99)]=NA
Data1999$EST_CIVM[which(Data1999$EST_CIVM=='9')]=NA
Data1999$NIV_EDUM[which(Data1999$NIV_EDUM=='9')]=NA
Data1999$NIV_EDUM=revalue(Data1999$NIV_EDUM, c("1"="0","8"="0","2"="1", "3"="1", "4"="2", "5"="2", "6"="3", "7"="3"))

Data1999$DPTO_R[which(Data1999$DPTO_R=='01')]=NA
Data1999$MUNIC_R[which(Data1999$MUNIC_R=='999')]=NA
Data1999$AREA_R[which(Data1999$AREA_R=='9')]=NA
Data1999$N_HIJOSV[which(Data1999$N_HIJOSV==99)]=NA
Data1999$N_EMB[which(Data1999$N_EMB==99)]=NA
Data1999$EDAD_PADRE[which(Data1999$EDAD_PADRE==99)]=NA
Data1999$N_EMB[which(Data1999$N_EMB==99)]=NA
Data1999$NIV_EDUP[which(Data1999$NIV_EDUP=='9')]=NA
Data1999$PROFESION[which(Data1999$PROFESION=='9')]=NA

Data1999$APGAR_CONTINUO=NA
Data1999$SEMANAS=NA
Data1999$CODPRES=NA
Data1999$COD_INST=NA
Data1999$NOM_INST=NA
Data1999$COD_REGION=NA
Data1999$VALIDAR=NA

Data2000=read.dbf('RawData/Nacimientos/nac2000.dbf', as.is = FALSE)


Data2000=rename(Data2000, c("COD_MUNIC"="MUNIC_NACI", "COD_INSP"="INSP_NACI", "COD_DPTO"="DPTO_NACI",
"AREANAC"="AREA_NACI", "NUMCONSUL"="NUM_CONSUL", "CODPTORE"="DPTO_R",
"CODMUNRE"="MUNIC_R", "AREA_RES"="AREA_R", "CODIGO"="CODIGO_CP", "FECHA_NACM"="FECHA_ANTN"))

Data2000$FECHA_NAC =as.Date(Data2000$FECHA_NAC,format="%Y-%m-%d")
Data2000$FECHA_ANTN =as.Date(Data2000$FECHA_ANTN,format="%Y-%m-%d")
Data2000$FECHA_EXP =as.Date(Data2000$FECHA_EXP,format="%Y-%m-%d")

Data2000$AREA_NACI[which(Data2000$AREA_NACI=='9')]=NA
Data2000$SIT_PARTO[which(Data2000$SIT_PARTO=='9')]=NA
Data2000$PESO_NAC[which(Data2000$PESO_NAC==9999)]=NA
Data2000$TALLA_NAC[which(Data2000$TALLA_NAC==99)]=NA
Data2000$ATEN_PAR[which(Data2000$ATEN_PAR=='9')]=NA
Data2000$T_GES[which(Data2000$T_GES=='9')]=NA
# Data2000$SEMANAS[which(Data2000$SEMANAS=='6')]=NA
Data2000$T_GES[which(Data2000$T_GES=='6')]=NA
Data2000$T_GES=as.numeric(as.character(Data2000$T_GES)) 

Data2000$NUM_CONSUL[which(Data2000$NUM_CONSUL==99)]=NA
Data2000$NUM_CONSUL[which(Data2000$NUM_CONSUL=='99')]=NA

Data2000$TIPO_PARTO[which(Data2000$TIPO_PARTO=='9')]=NA
Data2000$MUL_PARTO[which(Data2000$MUL_PARTO=='9')]=NA
Data2000$APGAR1[which(Data2000$APGAR1=='9')]=NA
Data2000$APGAR2[which(Data2000$APGAR2=='9')]=NA
Data2000$GRU_SAN[which(Data2000$GRU_SAN=='9')]=NA
Data2000$SEG_SOCIAL[which(Data2000$SEG_SOCIAL=='9')]=NA
Data2000$EDAD_MADRE[which(Data2000$EDAD_MADRE==99)]=NA
Data2000$EST_CIVM[which(Data2000$EST_CIVM=='9')]=NA
Data2000$NIV_EDUM[which(Data2000$NIV_EDUM=='9')]=NA
Data2000$NIV_EDUM=revalue(Data2000$NIV_EDUM, c("1"="0","8"="0","2"="1", "3"="1", "4"="2", "5"="2", "6"="3", "7"="3"))

Data2000$DPTO_R[which(Data2000$DPTO_R=='01')]=NA
Data2000$MUNIC_R[which(Data2000$MUNIC_R=='999')]=NA
Data2000$AREA_R[which(Data2000$AREA_R=='9')]=NA
Data2000$N_HIJOSV[which(Data2000$N_HIJOSV==99)]=NA
Data2000$N_EMB[which(Data2000$N_EMB==99)]=NA
Data2000$EDAD_PADRE[which(Data2000$EDAD_PADRE==99)]=NA
Data2000$N_EMB[which(Data2000$N_EMB==99)]=NA
Data2000$NIV_EDUP[which(Data2000$NIV_EDUP=='9')]=NA
Data2000$PROFESION[which(Data2000$PROFESION=='9')]=NA

Data2000$APGAR_CONTINUO=NA

Data2000$SEMANAS=NA
Data2000$NOM_INST=NA
Data2000$COD_REGION=NA
Data2000$VALIDAR=NA
Data2000$COD_INST=as.character(Data2000$COD_INST)

Data2001=read.dbf('RawData/Nacimientos/nac2001.dbf', as.is = FALSE)

Data2001=rename(Data2001, c("COD_MUNIC"="MUNIC_NACI", "COD_INSP"="INSP_NACI", "COD_DPTO"="DPTO_NACI",
"AREANAC"="AREA_NACI", "NUMCONSUL"="NUM_CONSUL", "CODPTORE"="DPTO_R",
"CODMUNRE"="MUNIC_R", "AREA_RES"="AREA_R", "CODIGO"="CODIGO_CP", "FECHA_NACM"="FECHA_ANTN"))

Data2001$FECHA_NAC =as.Date(Data2001$FECHA_NAC,format="%Y-%m-%d")
Data2001$FECHA_ANTN =as.Date(Data2001$FECHA_ANTN,format="%Y-%m-%d")
Data2001$FECHA_EXP =as.Date(Data2001$FECHA_EXP,format="%Y-%m-%d")

Data2001$AREA_NACI[which(Data2001$AREA_NACI=='9')]=NA
Data2001$SIT_PARTO[which(Data2001$SIT_PARTO=='9')]=NA
Data2001$PESO_NAC[which(Data2001$PESO_NAC==9999)]=NA
Data2001$TALLA_NAC[which(Data2001$TALLA_NAC==99)]=NA
Data2001$ATEN_PAR[which(Data2001$ATEN_PAR=='9')]=NA
Data2001$T_GES[which(Data2001$T_GES=='9')]=NA
#Data2001$SEMANAS[which(Data2001$SEMANAS=='6')]=NA
Data2001$T_GES[which(Data2001$T_GES=='6')]=NA
Data2001$T_GES=as.numeric(as.character(Data2001$T_GES)) 

Data2001$NUM_CONSUL[which(Data2001$NUM_CONSUL==99)]=NA
Data2001$NUM_CONSUL[which(Data2001$NUM_CONSUL=='99')]=NA

Data2001$TIPO_PARTO[which(Data2001$TIPO_PARTO=='9')]=NA
Data2001$MUL_PARTO[which(Data2001$MUL_PARTO=='9')]=NA
Data2001$APGAR1[which(Data2001$APGAR1=='9')]=NA
Data2001$APGAR2[which(Data2001$APGAR2=='9')]=NA
Data2001$GRU_SAN[which(Data2001$GRU_SAN=='9')]=NA
Data2001$SEG_SOCIAL[which(Data2001$SEG_SOCIAL=='9')]=NA
Data2001$EDAD_MADRE[which(Data2001$EDAD_MADRE==99)]=NA
Data2001$EST_CIVM[which(Data2001$EST_CIVM=='9')]=NA
Data2001$NIV_EDUM[which(Data2001$NIV_EDUM=='9')]=NA
Data2001$NIV_EDUM=revalue(Data2001$NIV_EDUM, c("1"="0","8"="0","2"="1", "3"="1", "4"="2", "5"="2", "6"="3", "7"="3"))

Data2001$DPTO_R[which(Data2001$DPTO_R=='01')]=NA
Data2001$MUNIC_R[which(Data2001$MUNIC_R=='999')]=NA
Data2001$AREA_R[which(Data2001$AREA_R=='9')]=NA
Data2001$N_HIJOSV[which(Data2001$N_HIJOSV==99)]=NA
Data2001$N_EMB[which(Data2001$N_EMB==99)]=NA
Data2001$EDAD_PADRE[which(Data2001$EDAD_PADRE==99)]=NA
Data2001$N_EMB[which(Data2001$N_EMB==99)]=NA
Data2001$NIV_EDUP[which(Data2001$NIV_EDUP=='9')]=NA
Data2001$PROFESION[which(Data2001$PROFESION=='9')]=NA

Data2001$APGAR_CONTINUO=NA

Data2001$SEMANAS=NA
Data2001$NOM_INST=NA
Data2001$COD_REGION=NA
Data2001$VALIDAR=NA
Data2001$COD_INST=as.character(Data2001$COD_INST)

Data2002=read.dbf('RawData/Nacimientos/nac2002.dbf', as.is = FALSE)

Data2002=rename(Data2002, c("COD_MUNIC"="MUNIC_NACI", "COD_INSP"="INSP_NACI", "COD_DPTO"="DPTO_NACI",
"AREANAC"="AREA_NACI", "NUMCONSUL"="NUM_CONSUL", "CODPTORE"="DPTO_R",
"CODMUNRE"="MUNIC_R", "AREA_RES"="AREA_R", "CODIGO"="CODIGO_CP", "FECHA_NACM"="FECHA_ANTN"))

Data2002$FECHA_NAC =as.Date(Data2002$FECHA_NAC,format="%Y-%m-%d")
Data2002$FECHA_ANTN =as.Date(Data2002$FECHA_ANTN,format="%Y-%m-%d")
Data2002$FECHA_EXP =as.Date(Data2002$FECHA_EXP,format="%Y-%m-%d")

Data2002$AREA_NACI[which(Data2002$AREA_NACI=='9')]=NA
Data2002$SIT_PARTO[which(Data2002$SIT_PARTO=='9')]=NA
Data2002$PESO_NAC[which(Data2002$PESO_NAC==9999)]=NA
Data2002$TALLA_NAC[which(Data2002$TALLA_NAC==99)]=NA
Data2002$ATEN_PAR[which(Data2002$ATEN_PAR=='9')]=NA
Data2002$SEMANAS[which(Data2002$SEMANAS=='99')]=NA
Data2002$SEMANAS[which(Data2002$SEMANAS=='98')]=NA
Data2002$SEMANAS=as.numeric(as.character(Data2002$SEMANAS))
Data2002$SEMANAS[which(Data2002$SEMANAS<16)]=NA
Data2002$T_GES[which(Data2002$T_GES=='9')]=NA
Data2002$T_GES[which(Data2002$T_GES=='6')]=NA

Data2002$T_GES=as.numeric(as.character(Data2002$T_GES)) 
Data2002$T_GES[which( Data2002$SEMANAS<20)]=1
Data2002$T_GES[which( Data2002$SEMANAS>=20 & Data2002$SEMANAS<=27)]=2
Data2002$T_GES[which( Data2002$SEMANAS>=28 & Data2002$SEMANAS<=37)]=3
Data2002$T_GES[which( Data2002$SEMANAS>=38 & Data2002$SEMANAS<=41)]=4
Data2002$T_GES[which( Data2002$SEMANAS>=42)]=5


Data2002$NUM_CONSUL[which(Data2002$NUM_CONSUL==99)]=NA
Data2002$NUM_CONSUL[which(Data2002$NUM_CONSUL=='99')]=NA

Data2002$TIPO_PARTO[which(Data2002$TIPO_PARTO=='9')]=NA
Data2002$MUL_PARTO[which(Data2002$MUL_PARTO=='9')]=NA
Data2002$APGAR1[which(Data2002$APGAR1=='9')]=NA
Data2002$APGAR2[which(Data2002$APGAR2=='9')]=NA
Data2002$GRU_SAN[which(Data2002$GRU_SAN=='9')]=NA
Data2002$SEG_SOCIAL[which(Data2002$SEG_SOCIAL=='9')]=NA
Data2002$EDAD_MADRE[which(Data2002$EDAD_MADRE==99)]=NA
Data2002$EST_CIVM[which(Data2002$EST_CIVM=='9')]=NA
Data2002$NIV_EDUM[which(Data2002$NIV_EDUM=='9')]=NA
Data2002$NIV_EDUM=revalue(Data2002$NIV_EDUM, c("1"="0","8"="0","2"="1", "3"="1", "4"="2", "5"="2", "6"="3", "7"="3"))

Data2002$DPTO_R[which(Data2002$DPTO_R=='01')]=NA
Data2002$MUNIC_R[which(Data2002$MUNIC_R=='999')]=NA
Data2002$AREA_R[which(Data2002$AREA_R=='9')]=NA
Data2002$N_HIJOSV[which(Data2002$N_HIJOSV==99)]=NA
Data2002$N_EMB[which(Data2002$N_EMB==99)]=NA
Data2002$EDAD_PADRE[which(Data2002$EDAD_PADRE==99)]=NA
Data2002$N_EMB[which(Data2002$N_EMB==99)]=NA
Data2002$NIV_EDUP[which(Data2002$NIV_EDUP=='9')]=NA
Data2002$PROFESION[which(Data2002$PROFESION=='9')]=NA

Data2002$APGAR_CONTINUO=NA

Data2002$NOM_INST=NA
Data2002$COD_REGION=NA
Data2002$VALIDAR=NA
Data2002$COD_INST=as.character(Data2002$COD_INST)

Data2003=read.dbf('RawData/Nacimientos/nac2003.dbf', as.is = FALSE)

Data2003=rename(Data2003, c("COD_MUNIC"="MUNIC_NACI", "COD_INSP"="INSP_NACI", "COD_DPTO"="DPTO_NACI",
"AREANAC"="AREA_NACI", "NUMCONSUL"="NUM_CONSUL", "CODPTORE"="DPTO_R",
"CODMUNRE"="MUNIC_R", "AREA_RES"="AREA_R", "CODIGO"="CODIGO_CP", "FECHA_NACM"="FECHA_ANTN"))

Data2003$FECHA_NAC =as.Date(Data2003$FECHA_NAC,format="%Y-%m-%d")
Data2003$FECHA_ANTN =as.Date(Data2003$FECHA_ANTN,format="%Y-%m-%d")
Data2003$FECHA_EXP =as.Date(Data2003$FECHA_EXP,format="%Y-%m-%d")

Data2003$AREA_NACI[which(Data2003$AREA_NACI=='9')]=NA
Data2003$SIT_PARTO[which(Data2003$SIT_PARTO=='9')]=NA
Data2003$PESO_NAC[which(Data2003$PESO_NAC==9999)]=NA
Data2003$TALLA_NAC[which(Data2003$TALLA_NAC==99)]=NA
Data2003$ATEN_PAR[which(Data2003$ATEN_PAR=='9')]=NA
Data2003$SEMANAS[which(Data2003$SEMANAS=='99')]=NA
Data2003$SEMANAS=as.numeric(as.character(Data2003$SEMANAS))
Data2003$SEMANAS[which(Data2003$SEMANAS<16)]=NA
Data2003$SEMANAS[which(Data2003$SEMANAS==99)]=NA
Data2003$SEMANAS[which(Data2003$SEMANAS==98)]=NA
Data2003$T_GES[which(Data2003$T_GES=='9')]=NA
Data2003$T_GES[which(Data2003$T_GES=='6')]=NA

Data2003$T_GES=as.numeric(as.character(Data2003$T_GES)) 
Data2003$T_GES[which( Data2003$SEMANAS<20)]=1
Data2003$T_GES[which( Data2003$SEMANAS>=20 & Data2003$SEMANAS<=27)]=2
Data2003$T_GES[which( Data2003$SEMANAS>=28 & Data2003$SEMANAS<=37)]=3
Data2003$T_GES[which( Data2003$SEMANAS>=38 & Data2003$SEMANAS<=41)]=4
Data2003$T_GES[which( Data2003$SEMANAS>=42)]=5


Data2003$NUM_CONSUL[which(Data2003$NUM_CONSUL==99)]=NA
Data2003$NUM_CONSUL[which(Data2003$NUM_CONSUL=='99')]=NA

Data2003$TIPO_PARTO[which(Data2003$TIPO_PARTO=='9')]=NA
Data2003$MUL_PARTO[which(Data2003$MUL_PARTO=='9')]=NA
Data2003$APGAR1[which(Data2003$APGAR1=='9')]=NA
Data2003$APGAR2[which(Data2003$APGAR2=='9')]=NA
Data2003$GRU_SAN[which(Data2003$GRU_SAN=='9')]=NA
Data2003$SEG_SOCIAL[which(Data2003$SEG_SOCIAL=='9')]=NA
Data2003$EDAD_MADRE[which(Data2003$EDAD_MADRE==99)]=NA
Data2003$EST_CIVM[which(Data2003$EST_CIVM=='9')]=NA
Data2003$NIV_EDUM[which(Data2003$NIV_EDUM=='9')]=NA
Data2003$NIV_EDUM=revalue(Data2003$NIV_EDUM, c("1"="0","8"="0","2"="1", "3"="1", "4"="2", "5"="2", "6"="3", "7"="3"))

Data2003$APGAR_CONTINUO=NA

Data2003$DPTO_R[which(Data2003$DPTO_R=='01')]=NA
Data2003$MUNIC_R[which(Data2003$MUNIC_R=='999')]=NA
Data2003$AREA_R[which(Data2003$AREA_R=='9')]=NA
Data2003$N_HIJOSV[which(Data2003$N_HIJOSV==99)]=NA
Data2003$N_EMB[which(Data2003$N_EMB==99)]=NA
Data2003$EDAD_PADRE[which(Data2003$EDAD_PADRE==99)]=NA
Data2003$N_EMB[which(Data2003$N_EMB==99)]=NA
Data2003$NIV_EDUP[which(Data2003$NIV_EDUP=='9')]=NA
Data2003$PROFESION[which(Data2003$PROFESION=='9')]=NA

Data2003$VALIDAR=NA
Data2003$COD_INST=as.character(Data2003$COD_INST)

Data2004=read.dbf('RawData/Nacimientos/nac2004.dbf', as.is = FALSE)

Data2004=rename(Data2004, c("COD_MUNIC"="MUNIC_NACI", "COD_INSP"="INSP_NACI", "COD_DPTO"="DPTO_NACI",
"AREANAC"="AREA_NACI", "NUMCONSUL"="NUM_CONSUL", "CODPTORE"="DPTO_R",
"CODMUNRE"="MUNIC_R", "AREA_RES"="AREA_R", "CODIGO"="CODIGO_CP", "FECHA_NACM"="FECHA_ANTN"))

Data2004$FECHA_NAC =as.Date(Data2004$FECHA_NAC,format="%Y-%m-%d")
Data2004$FECHA_ANTN =as.Date(Data2004$FECHA_ANTN,format="%Y-%m-%d")
Data2004$FECHA_EXP =as.Date(Data2004$FECHA_EXP,format="%Y-%m-%d")

Data2004$AREA_NACI[which(Data2004$AREA_NACI=='9')]=NA
Data2004$SIT_PARTO[which(Data2004$SIT_PARTO=='9')]=NA
Data2004$PESO_NAC[which(Data2004$PESO_NAC==9999)]=NA
Data2004$TALLA_NAC[which(Data2004$TALLA_NAC==99)]=NA
Data2004$ATEN_PAR[which(Data2004$ATEN_PAR=='9')]=NA
Data2004$SEMANAS=as.numeric(as.character(Data2004$SEMANAS))
Data2004$SEMANAS[which(Data2004$SEMANAS=='99')]=NA
Data2004$SEMANAS=as.numeric(as.character(Data2004$SEMANAS))
Data2004$SEMANAS[which(Data2004$SEMANAS<16)]=NA
Data2004$SEMANAS[which(Data2004$SEMANAS==99)]=NA
Data2004$T_GES[which(Data2004$T_GES=='9')]=NA
Data2004$T_GES[which(Data2004$T_GES=='6')]=NA
Data2004$SEMANAS[which(Data2004$SEMANAS==99)]=NA
Data2004$SEMANAS[which(Data2004$SEMANAS==98)]=NA

Data2004$APGAR_CONTINUO=NA

Data2004$T_GES=as.numeric(as.character(Data2004$T_GES)) 
Data2004$T_GES[which( Data2004$SEMANAS<20)]=1
Data2004$T_GES[which( Data2004$SEMANAS>=20 & Data2004$SEMANAS<=27)]=2
Data2004$T_GES[which( Data2004$SEMANAS>=28 & Data2004$SEMANAS<=37)]=3
Data2004$T_GES[which( Data2004$SEMANAS>=38 & Data2004$SEMANAS<=41)]=4
Data2004$T_GES[which( Data2004$SEMANAS>=42)]=5


Data2004$NUM_CONSUL[which(Data2004$NUM_CONSUL==99)]=NA
Data2004$NUM_CONSUL[which(Data2004$NUM_CONSUL=='99')]=NA

Data2004$TIPO_PARTO[which(Data2004$TIPO_PARTO=='9')]=NA
Data2004$MUL_PARTO[which(Data2004$MUL_PARTO=='9')]=NA
Data2004$APGAR1[which(Data2004$APGAR1=='9')]=NA
Data2004$APGAR2[which(Data2004$APGAR2=='9')]=NA
Data2004$GRU_SAN[which(Data2004$GRU_SAN=='9')]=NA
Data2004$SEG_SOCIAL[which(Data2004$SEG_SOCIAL=='9')]=NA
Data2004$EDAD_MADRE[which(Data2004$EDAD_MADRE==99)]=NA
Data2004$EST_CIVM[which(Data2004$EST_CIVM=='9')]=NA
Data2004$NIV_EDUM[which(Data2004$NIV_EDUM=='9')]=NA
Data2004$NIV_EDUM=revalue(Data2004$NIV_EDUM, c("1"="0","8"="0","2"="1", "3"="1", "4"="2", "5"="2", "6"="3", "7"="3"))

Data2004$DPTO_R[which(Data2004$DPTO_R=='01')]=NA
Data2004$MUNIC_R[which(Data2004$MUNIC_R=='999')]=NA
Data2004$AREA_R[which(Data2004$AREA_R=='9')]=NA
Data2004$N_HIJOSV[which(Data2004$N_HIJOSV==99)]=NA
Data2004$N_EMB[which(Data2004$N_EMB==99)]=NA
Data2004$EDAD_PADRE[which(Data2004$EDAD_PADRE==99)]=NA
Data2004$N_EMB[which(Data2004$N_EMB==99)]=NA
Data2004$NIV_EDUP[which(Data2004$NIV_EDUP=='9')]=NA
Data2004$PROFESION[which(Data2004$PROFESION=='9')]=NA


Data2004$COD_REGION=NA
Data2004$COD_INST=as.character(Data2004$COD_INST)

Data2005=read.dbf('RawData/Nacimientos/nac2005.dbf', as.is = FALSE)
Data2005=subset(Data2005,select=-c(X.,X,X.1,X.2,X.3,X.4,X.5,X.6))
Data2005=rename(Data2005, c("COD_MUNIC"="MUNIC_NACI", "COD_INSP"="INSP_NACI", "COD_DPTO"="DPTO_NACI",
"AREANAC"="AREA_NACI", "NUMCONSUL"="NUM_CONSUL", "CODPTORE"="DPTO_R",
"CODMUNRE"="MUNIC_R", "AREA_RES"="AREA_R", "CODIGO"="CODIGO_CP", "FECHA_NACM"="FECHA_ANTN"))

Data2005$FECHA_NAC =as.Date(Data2005$FECHA_NAC,format="%Y-%m-%d")
Data2005$FECHA_ANTN =as.Date(Data2005$FECHA_ANTN,format="%Y-%m-%d")
Data2005$FECHA_EXP =as.Date(Data2005$FECHA_EXP,format="%Y-%m-%d")

Data2005$AREA_NACI[which(Data2005$AREA_NACI=='9')]=NA
Data2005$SIT_PARTO[which(Data2005$SIT_PARTO=='9')]=NA
Data2005$PESO_NAC[which(Data2005$PESO_NAC==9999)]=NA
Data2005$TALLA_NAC[which(Data2005$TALLA_NAC==99)]=NA
Data2005$ATEN_PAR[which(Data2005$ATEN_PAR=='9')]=NA
Data2005$SEMANAS[which(Data2005$SEMANAS=='99')]=NA
Data2005$SEMANAS=as.numeric(as.character(Data2005$SEMANAS))
Data2005$SEMANAS[which(Data2005$SEMANAS<16)]=NA
Data2005$T_GES[which(Data2005$T_GES=='9')]=NA
Data2005$T_GES[which(Data2005$T_GES=='6')]=NA
Data2005$SEMANAS[which(Data2005$SEMANAS==99)]=NA
Data2005$SEMANAS[which(Data2005$SEMANAS==98)]=NA


Data2005$T_GES=as.numeric(as.character(Data2005$T_GES)) 
Data2005$T_GES[which( Data2005$SEMANAS<20)]=1
Data2005$T_GES[which( Data2005$SEMANAS>=20 & Data2005$SEMANAS<=27)]=2
Data2005$T_GES[which( Data2005$SEMANAS>=28 & Data2005$SEMANAS<=37)]=3
Data2005$T_GES[which( Data2005$SEMANAS>=38 & Data2005$SEMANAS<=41)]=4
Data2005$T_GES[which( Data2005$SEMANAS>=42)]=5

Data2005$APGAR_CONTINUO=NA

Data2005$NUM_CONSUL[which(Data2005$NUM_CONSUL=='99')]=NA
Data2005$NUM_CONSUL[which(Data2005$NUM_CONSUL==99)]=NA
Data2005$TIPO_PARTO[which(Data2005$TIPO_PARTO=='9')]=NA
Data2005$MUL_PARTO[which(Data2005$MUL_PARTO=='9')]=NA
Data2005$APGAR1[which(Data2005$APGAR1=='9')]=NA
Data2005$APGAR2[which(Data2005$APGAR2=='9')]=NA
Data2005$GRU_SAN[which(Data2005$GRU_SAN=='9')]=NA
Data2005$SEG_SOCIAL[which(Data2005$SEG_SOCIAL=='9')]=NA
Data2005$EDAD_MADRE[which(Data2005$EDAD_MADRE==99)]=NA
Data2005$EST_CIVM[which(Data2005$EST_CIVM=='9')]=NA
Data2005$NIV_EDUM[which(Data2005$NIV_EDUM=='9')]=NA
Data2005$NIV_EDUM=revalue(Data2005$NIV_EDUM, c("1"="0","8"="0","2"="1", "3"="1", "4"="2", "5"="2", "6"="3", "7"="3"))

Data2005$DPTO_R[which(Data2005$DPTO_R=='01')]=NA
Data2005$MUNIC_R[which(Data2005$MUNIC_R=='999')]=NA
Data2005$AREA_R[which(Data2005$AREA_R=='9')]=NA
Data2005$N_HIJOSV[which(Data2005$N_HIJOSV==99)]=NA
Data2005$N_EMB[which(Data2005$N_EMB==99)]=NA
Data2005$EDAD_PADRE[which(Data2005$EDAD_PADRE==99)]=NA
Data2005$N_EMB[which(Data2005$N_EMB==99)]=NA
Data2005$NIV_EDUP[which(Data2005$NIV_EDUP=='9')]=NA
Data2005$PROFESION[which(Data2005$PROFESION=='9')]=NA

Data2005$VALIDAR=NA
Data2005$COD_INST=as.character(Data2005$COD_INST)

Data2006=read.dbf('RawData/Nacimientos/nac2006.dbf', as.is = FALSE)

Data2006=rename(Data2006, c("COD_MUNIC"="MUNIC_NACI", "COD_INSP"="INSP_NACI", "COD_DPTO"="DPTO_NACI",
"AREANAC"="AREA_NACI", "NUMCONSUL"="NUM_CONSUL", "CODPTORE"="DPTO_R",
"CODMUNRE"="MUNIC_R", "AREA_RES"="AREA_R", "CODIGO"="CODIGO_CP", "FECHA_NACM"="FECHA_ANTN"))

Data2006$FECHA_NAC =as.Date(Data2006$FECHA_NAC,format="%Y-%m-%d")
Data2006$FECHA_ANTN =as.Date(Data2006$FECHA_ANTN,format="%Y-%m-%d")
Data2006$FECHA_EXP =as.Date(Data2006$FECHA_EXP,format="%Y-%m-%d")

Data2006$AREA_NACI[which(Data2006$AREA_NACI=='9')]=NA
Data2006$SIT_PARTO[which(Data2006$SIT_PARTO=='9')]=NA
Data2006$PESO_NAC[which(Data2006$PESO_NAC==9999)]=NA
Data2006$TALLA_NAC[which(Data2006$TALLA_NAC==99)]=NA
Data2006$ATEN_PAR[which(Data2006$ATEN_PAR=='9')]=NA
Data2006$SEMANAS[which(Data2006$SEMANAS=='99')]=NA
Data2006$SEMANAS[which(Data2006$SEMANAS=='6')]=NA
Data2006$SEMANAS=as.numeric(as.character(Data2006$SEMANAS))
Data2006$SEMANAS[which(Data2006$SEMANAS<16)]=NA
Data2006$T_GES[which(Data2006$T_GES=='9')]=NA
Data2006$T_GES[which(Data2006$T_GES=='6')]=NA
Data2006$SEMANAS[which(Data2006$SEMANAS==99)]=NA
Data2006$SEMANAS[which(Data2006$SEMANAS==98)]=NA

Data2006$T_GES=as.numeric(as.character(Data2006$T_GES)) 
Data2006$T_GES[which( Data2006$SEMANAS<20)]=1
Data2006$T_GES[which( Data2006$SEMANAS>=20 & Data2006$SEMANAS<=27)]=2
Data2006$T_GES[which( Data2006$SEMANAS>=28 & Data2006$SEMANAS<=37)]=3
Data2006$T_GES[which( Data2006$SEMANAS>=38 & Data2006$SEMANAS<=41)]=4
Data2006$T_GES[which( Data2006$SEMANAS>=42)]=5

Data2006$APGAR_CONTINUO=NA

Data2006$NUM_CONSUL[which(Data2006$NUM_CONSUL==99)]=NA
Data2006$NUM_CONSUL[which(Data2006$NUM_CONSUL=='99')]=NA

Data2006$TIPO_PARTO[which(Data2006$TIPO_PARTO=='9')]=NA
Data2006$MUL_PARTO[which(Data2006$MUL_PARTO=='9')]=NA
Data2006$APGAR1[which(Data2006$APGAR1=='9')]=NA
Data2006$APGAR2[which(Data2006$APGAR2=='9')]=NA
Data2006$GRU_SAN[which(Data2006$GRU_SAN=='9')]=NA
Data2006$SEG_SOCIAL[which(Data2006$SEG_SOCIAL=='9')]=NA
Data2006$EDAD_MADRE[which(Data2006$EDAD_MADRE==99)]=NA
Data2006$EST_CIVM[which(Data2006$EST_CIVM=='9')]=NA
Data2006$NIV_EDUM[which(Data2006$NIV_EDUM=='9')]=NA
Data2006$NIV_EDUM=revalue(Data2006$NIV_EDUM, c("1"="0","8"="0","2"="1", "3"="1", "4"="2", "5"="2", "6"="3", "7"="3"))

Data2006$DPTO_R[which(Data2006$DPTO_R=='01')]=NA
Data2006$MUNIC_R[which(Data2006$MUNIC_R=='999')]=NA
Data2006$AREA_R[which(Data2006$AREA_R=='9')]=NA
Data2006$N_HIJOSV[which(Data2006$N_HIJOSV==99)]=NA
Data2006$N_EMB[which(Data2006$N_EMB==99)]=NA
Data2006$EDAD_PADRE[which(Data2006$EDAD_PADRE==99)]=NA
Data2006$N_EMB[which(Data2006$N_EMB==99)]=NA
Data2006$NIV_EDUP[which(Data2006$NIV_EDUP=='9')]=NA
Data2006$PROFESION[which(Data2006$PROFESION=='9')]=NA

Data2006$COD_REGION=NA
Data2006$COD_INST=as.character(Data2006$COD_INST)

Data2007=read.dbf('RawData/Nacimientos/nac2007.dbf', as.is = FALSE)

Data2007=rename(Data2007, c("COD_MUNIC"="MUNIC_NACI", "COD_INSP"="INSP_NACI", "COD_DPTO"="DPTO_NACI",
"AREANAC"="AREA_NACI", "NUMCONSUL"="NUM_CONSUL", "CODPTORE"="DPTO_R",
"CODMUNRE"="MUNIC_R", "AREA_RES"="AREA_R", "CODIGO"="CODIGO_CP", "FECHA_NACM"="FECHA_ANTN"))

Data2007$FECHA_NAC =as.Date(Data2007$FECHA_NAC,format="%Y-%m-%d")
Data2007$FECHA_ANTN =as.Date(Data2007$FECHA_ANTN,format="%Y-%m-%d")
Data2007$FECHA_EXP =as.Date(Data2007$FECHA_EXP,format="%Y-%m-%d")

Data2007$AREA_NACI[which(Data2007$AREA_NACI=='9')]=NA
Data2007$SIT_PARTO[which(Data2007$SIT_PARTO=='9')]=NA
Data2007$PESO_NAC[which(Data2007$PESO_NAC==9999)]=NA
Data2007$TALLA_NAC[which(Data2007$TALLA_NAC==99)]=NA
Data2007$ATEN_PAR[which(Data2007$ATEN_PAR=='9')]=NA
Data2007$SEMANAS[which(Data2007$SEMANAS=='99')]=NA
Data2007$SEMANAS[which(Data2007$SEMANAS=='6')]=NA
Data2007$SEMANAS=as.numeric(as.character(Data2007$SEMANAS))
Data2007$SEMANAS[which(Data2007$SEMANAS<16)]=NA
Data2007$T_GES[which(Data2007$T_GES=='9')]=NA
Data2007$T_GES[which(Data2007$T_GES=='6')]=NA
Data2007$SEMANAS[which(Data2007$SEMANAS==99)]=NA
Data2007$SEMANAS[which(Data2007$SEMANAS==98)]=NA


Data2007$T_GES=as.numeric(as.character(Data2007$T_GES)) 
Data2007$T_GES[which( Data2007$SEMANAS<20)]=1
Data2007$T_GES[which( Data2007$SEMANAS>=20 & Data2007$SEMANAS<=27)]=2
Data2007$T_GES[which( Data2007$SEMANAS>=28 & Data2007$SEMANAS<=37)]=3
Data2007$T_GES[which( Data2007$SEMANAS>=38 & Data2007$SEMANAS<=41)]=4
Data2007$T_GES[which( Data2007$SEMANAS>=42)]=5


Data2007$APGAR_CONTINUO=NA

Data2007$NUM_CONSUL[which(Data2007$NUM_CONSUL==99)]=NA
Data2007$NUM_CONSUL[which(Data2007$NUM_CONSUL=='99')]=NA

Data2007$TIPO_PARTO[which(Data2007$TIPO_PARTO=='9')]=NA
Data2007$MUL_PARTO[which(Data2007$MUL_PARTO=='9')]=NA
Data2007$APGAR1[which(Data2007$APGAR1=='9')]=NA
Data2007$APGAR2[which(Data2007$APGAR2=='9')]=NA
Data2007$GRU_SAN[which(Data2007$GRU_SAN=='9')]=NA
Data2007$SEG_SOCIAL[which(Data2007$SEG_SOCIAL=='9')]=NA
Data2007$EDAD_MADRE[which(Data2007$EDAD_MADRE==99)]=NA
Data2007$EST_CIVM[which(Data2007$EST_CIVM=='9')]=NA
Data2007$NIV_EDUM[which(Data2007$NIV_EDUM=='9')]=NA
Data2007$NIV_EDUM=revalue(Data2007$NIV_EDUM, c("1"="0","8"="0","2"="1", "3"="1", "4"="2", "5"="2", "6"="3", "7"="3"))

Data2007$DPTO_R[which(Data2007$DPTO_R=='01')]=NA
Data2007$MUNIC_R[which(Data2007$MUNIC_R=='999')]=NA
Data2007$AREA_R[which(Data2007$AREA_R=='9')]=NA
Data2007$N_HIJOSV[which(Data2007$N_HIJOSV==99)]=NA
Data2007$N_EMB[which(Data2007$N_EMB==99)]=NA
Data2007$EDAD_PADRE[which(Data2007$EDAD_PADRE==99)]=NA
Data2007$N_EMB[which(Data2007$N_EMB==99)]=NA
Data2007$NIV_EDUP[which(Data2007$NIV_EDUP=='9')]=NA
Data2007$PROFESION[which(Data2007$PROFESION=='9')]=NA

Data2007$COD_REGION=NA
Data2007$COD_INST=as.character(Data2007$COD_INST)

Data2008=read.dbf('RawData/Nacimientos/nac2008.dbf', as.is = FALSE)

Data2008=rename(Data2008, c("COD_MUNIC"="MUNIC_NACI", "COD_INSP"="INSP_NACI", "COD_DPTO"="DPTO_NACI",
"AREANAC"="AREA_NACI", "NUMCONSUL"="NUM_CONSUL", "CODPTORE"="DPTO_R",
"CODMUNRE"="MUNIC_R", "AREA_RES"="AREA_R", "CODIGO"="CODIGO_CP", "FECHA_NACM"="FECHA_ANTN"))

Data2008$FECHA_NAC =as.Date(Data2008$FECHA_NAC,format="%Y-%m-%d")
Data2008$FECHA_ANTN =as.Date(Data2008$FECHA_ANTN,format="%Y-%m-%d")
Data2008$FECHA_EXP =as.Date(Data2008$FECHA_EXP,format="%Y-%m-%d")

Data2008$AREA_NACI[which(Data2008$AREA_NACI=='9')]=NA
Data2008$SIT_PARTO[which(Data2008$SIT_PARTO=='9')]=NA
Data2008$PESO_NAC[which(Data2008$PESO_NAC==9999)]=NA
Data2008$TALLA_NAC[which(Data2008$TALLA_NAC==99)]=NA
Data2008$ATEN_PAR[which(Data2008$ATEN_PAR=='9')]=NA
Data2008$T_GES[which(Data2008$SEMANAS=='98')]=NA
Data2008$T_GES[which(Data2008$SEMANAS=='99')]=NA
Data2008$SEMANAS[which(Data2008$SEMANAS=='99')]=NA
Data2008$SEMANAS[which(Data2008$SEMANAS=='6')]=NA
Data2008$SEMANAS[which(Data2008$SEMANAS=='98')]=NA
Data2008$SEMANAS=as.numeric(as.character(Data2008$SEMANAS))
Data2008$SEMANAS[which(Data2008$SEMANAS==99)]=NA
Data2008$SEMANAS[which(Data2008$SEMANAS==98)]=NA
Data2008$SEMANAS[which(Data2008$SEMANAS<16)]=NA
Data2008$T_GES[which(Data2008$T_GES=='9')]=NA
Data2008$T_GES[which(Data2008$T_GES=='6')]=NA

Data2008$T_GES=NA
Data2008$T_GES[which( Data2008$SEMANAS<20)]=1
Data2008$T_GES[which( Data2008$SEMANAS>=20 & Data2008$SEMANAS<=27)]=2
Data2008$T_GES[which( Data2008$SEMANAS>=28 & Data2008$SEMANAS<=37)]=3
Data2008$T_GES[which( Data2008$SEMANAS>=38 & Data2008$SEMANAS<=41)]=4
Data2008$T_GES[which( Data2008$SEMANAS>=42)]=5



Data2008$NUM_CONSUL[which(Data2008$NUM_CONSUL==99)]=NA
Data2008$NUM_CONSUL[which(Data2008$NUM_CONSUL=='99')]=NA

Data2008$TIPO_PARTO[which(Data2008$TIPO_PARTO=='9')]=NA
Data2008$MUL_PARTO[which(Data2008$MUL_PARTO=='9')]=NA

Data2008$APGAR_CONTINUO=Data2008$APGAR1
Data2008$APGAR1=revalue(Data2008$APGAR1, c("0"="1","01"="1","02"="1", "03"="1", "04"="1", "05"="2", "06"="2", "07"="3","08"="3","09"="3","10"="3"  ))
Data2008$APGAR2=revalue(Data2008$APGAR2, c("0"="1","01"="1","02"="1", "03"="1", "04"="1", "05"="2", "06"="2", "07"="3","08"="3","09"="3","10"="3"  ))
Data2008$APGAR1[which(Data2008$APGAR1=='99')]=NA
Data2008$APGAR2[which(Data2008$APGAR2=='99')]=NA
Data2008$APGAR_CONTINUO[which(Data2008$APGAR_CONTINUO=='99')]=NA



Data2008$GRU_SAN=NA
Data2008$GRU_SAN[which(Data2008$IDHEMOCLAS==1 & Data2008$IDFACTORRH==1) ]=1
Data2008$GRU_SAN[which(Data2008$IDHEMOCLAS==1 & Data2008$IDFACTORRH==2) ]=2
Data2008$GRU_SAN[which(Data2008$IDHEMOCLAS==2 & Data2008$IDFACTORRH==1) ]=3
Data2008$GRU_SAN[which(Data2008$IDHEMOCLAS==2 & Data2008$IDFACTORRH==2) ]=4
Data2008$GRU_SAN[which(Data2008$IDHEMOCLAS==3 & Data2008$IDFACTORRH==1) ]=5
Data2008$GRU_SAN[which(Data2008$IDHEMOCLAS==3 & Data2008$IDFACTORRH==2) ]=6
Data2008$GRU_SAN[which(Data2008$IDHEMOCLAS==4 & Data2008$IDFACTORRH==1) ]=7
Data2008$GRU_SAN[which(Data2008$IDHEMOCLAS==4 & Data2008$IDFACTORRH==2) ]=8
Data2008$GRU_SAN[which(Data2008$IDHEMOCLAS==9) ]=NA
Data2008=subset(Data2008,select=-c(IDHEMOCLAS,IDFACTORRH))


Data2008$GRU_SAN[which(Data2008$GRU_SAN=='9')]=NA
Data2008$SEG_SOCIAL[which(Data2008$SEG_SOCIAL=='9')]=NA
Data2008$EDAD_MADRE[which(Data2008$EDAD_MADRE==99)]=NA
Data2008$EST_CIVM[which(Data2008$EST_CIVM=='9')]=NA
Data2008$EST_CIVM=revalue(Data2008$EST_CIVM, c("1"="4","2"="4","6"="2", "4"="3", "3"="5", "5"="1" ))

Data2008$NIV_EDUM[which(Data2008$NIV_EDUM=='99')]=NA
Data2008$NIV_EDUM=revalue(Data2008$NIV_EDUM, c("01"="0","02"="1","03"="2", "04"="2", "05"="2", "06"="2", "07"="3", "08"="3", "09"="3", "10"="3", "11"="3", "12"="3" ,"13"="0"))




Data2008$DPTO_R[which(Data2008$DPTO_R=='01')]=NA
Data2008$MUNIC_R[which(Data2008$MUNIC_R=='999')]=NA
Data2008$AREA_R[which(Data2008$AREA_R=='9')]=NA
Data2008$N_HIJOSV[which(Data2008$N_HIJOSV==99)]=NA
Data2008$N_EMB[which(Data2008$N_EMB==99)]=NA
Data2008$EDAD_PADRE[which(Data2008$EDAD_PADRE==99)]=NA
Data2008$N_EMB[which(Data2008$N_EMB==99)]=NA
Data2008$NIV_EDUP[which(Data2008$NIV_EDUP=='9')]=NA

Data2008$NIV_EDUP2=NA
Data2008$NIV_EDUP2[which(Data2008$NIV_EDUP=='01')]='1'
Data2008$NIV_EDUP2[which(Data2008$ULTCURMAD<5 & Data2008$NIV_EDUP=='02')]='3'
Data2008$NIV_EDUP2[which(Data2008$ULTCURMAD==5 & Data2008$NIV_EDUP=='02')]='2'

Data2008$NIV_EDUP2[which((Data2008$ULTCURMAD==11 |Data2008$ULTCURMAD==12 |Data2008$ULTCURMAD==13) & (Data2008$NIV_EDUP=='03' | Data2008$NIV_EDUP=='04' | Data2008$NIV_EDUP=='05' | Data2008$NIV_EDUP=='06'))]='4'
Data2008$NIV_EDUP2[which(Data2008$ULTCURMAD>5 & Data2008$ULTCURMAD<11 & (Data2008$NIV_EDUP=='03'  | Data2008$NIV_EDUP=='04' | Data2008$NIV_EDUP=='05' | Data2008$NIV_EDUP=='06') )]='5'

Data2008$NIV_EDUP2[which(Data2008$NIV_EDUP=='13')]='8'
Data2008$NIV_EDUP2[which(Data2008$ULTCURMAD==0)]='8'

Data2008$NIV_EDUP2[which(Data2008$NIV_EDUP=='07' | Data2008$NIV_EDUP=='08' | Data2008$NIV_EDUP=='09' | Data2008$NIV_EDUP=='10' |
Data2008$NIV_EDUP=='11' |Data2008$NIV_EDUP=='12' )]='6' 

Data2008$NIV_EDUP=as.factor(Data2008$NIV_EDUP2)


Data2008$SECTOR=NA
Data2008$SECCION=NA
Data2008$PROFESION=NA
Data2008$COD_REGION=NA
Data2008$VALIDAR=NA
Data2008=subset(Data2008,select=-c(NIV_EDUP2))

#Remove data from 2008 that most other years do not have
Data2008=subset(Data2008,select=-c(OTRO_SIT,HORA,MINUTOS,OTRPARATX,IDPUEBLOIN,NOM_PUEB,ULTCURMAD,VEREDAMAD,IDCLASADMI,NOMCLASAD,ULTCURPAD,IDDEPTOEXP,IDMUNICEXP))
Data2008$COD_INST=as.character(Data2008$COD_INST)

######################
Data2009=read.dbf('RawData/Nacimientos/nac2009.dbf', as.is = FALSE)

Data2009=rename(Data2009, c("COD_MUNIC"="MUNIC_NACI", "COD_INSP"="INSP_NACI", "COD_DPTO"="DPTO_NACI",
"AREANAC"="AREA_NACI", "NUMCONSUL"="NUM_CONSUL", "CODPTORE"="DPTO_R",
"CODMUNRE"="MUNIC_R", "AREA_RES"="AREA_R", "CODIGO"="CODIGO_CP", "FECHA_NACM"="FECHA_ANTN"))

Data2009$FECHA_NAC =as.Date(Data2009$FECHA_NAC,format="%Y-%m-%d")
Data2009$FECHA_ANTN =as.Date(Data2009$FECHA_ANTN,format="%Y-%m-%d")
Data2009$FECHA_EXP =as.Date(Data2009$FECHA_EXP,format="%Y-%m-%d")

Data2009$SEMANAS=Data2009$T_GES
Data2009$SEMANAS[which(Data2009$SEMANAS=='98')]=NA
Data2009$SEMANAS[which(Data2009$SEMANAS=='99')]=NA
Data2009$SEMANAS=as.numeric(as.character(Data2009$SEMANAS))
Data2009$SEMANAS[which(Data2009$SEMANAS==99)]=NA
Data2009$SEMANAS[which(Data2009$SEMANAS==98)]=NA
Data2009$T_GES=NA
Data2009$T_GES[which(Data2009$SEMANAS<20)]=1
Data2009$T_GES[which(Data2009$SEMANAS>=20 & Data2009$SEMANAS<=27)]=2
Data2009$T_GES[which(Data2009$SEMANAS>=28 & Data2009$SEMANAS<=37)]=3
Data2009$T_GES[which(Data2009$SEMANAS>=38 & Data2009$SEMANAS<=41)]=4
Data2009$T_GES[which(Data2009$SEMANAS>=42)]=5


Data2009$AREA_NACI[which(Data2009$AREA_NACI=='9')]=NA
Data2009$SIT_PARTO[which(Data2009$SIT_PARTO=='9')]=NA
Data2009$PESO_NAC[which(Data2009$PESO_NAC==9999)]=NA
Data2009$TALLA_NAC[which(Data2009$TALLA_NAC==99)]=NA
Data2009$ATEN_PAR[which(Data2009$ATEN_PAR=='9')]=NA






Data2009$NUM_CONSUL[which(Data2009$NUM_CONSUL==99)]=NA
Data2009$NUM_CONSUL[which(Data2009$NUM_CONSUL=='99')]=NA

Data2009$TIPO_PARTO[which(Data2009$TIPO_PARTO=='9')]=NA
Data2009$MUL_PARTO[which(Data2009$MUL_PARTO=='9')]=NA

Data2009$APGAR_CONTINUO=Data2009$APGAR1
Data2009$APGAR1=revalue(Data2009$APGAR1, c("0"="1","01"="1","02"="1", "03"="1", "04"="1", "05"="2", "06"="2", "07"="3","08"="3","09"="3","10"="3"  ))
Data2009$APGAR2=revalue(Data2009$APGAR2, c("00"="1","01"="1","02"="1", "03"="1", "04"="1", "05"="2", "06"="2", "07"="3","08"="3","09"="3","10"="3"  ))
Data2009$APGAR1[which(Data2009$APGAR1=='99')]=NA
Data2009$APGAR2[which(Data2009$APGAR2=='99')]=NA
Data2009$APGAR_CONTINUO[which(Data2009$APGAR_CONTINUO=='99')]=NA


Data2009$GRU_SAN=NA
Data2009$GRU_SAN[which(Data2009$IDHEMOCLAS==1 & Data2009$IDFACTORRH==1) ]=1
Data2009$GRU_SAN[which(Data2009$IDHEMOCLAS==1 & Data2009$IDFACTORRH==2) ]=2
Data2009$GRU_SAN[which(Data2009$IDHEMOCLAS==2 & Data2009$IDFACTORRH==1) ]=3
Data2009$GRU_SAN[which(Data2009$IDHEMOCLAS==2 & Data2009$IDFACTORRH==2) ]=4
Data2009$GRU_SAN[which(Data2009$IDHEMOCLAS==3 & Data2009$IDFACTORRH==1) ]=5
Data2009$GRU_SAN[which(Data2009$IDHEMOCLAS==3 & Data2009$IDFACTORRH==2) ]=6
Data2009$GRU_SAN[which(Data2009$IDHEMOCLAS==4 & Data2009$IDFACTORRH==1) ]=7
Data2009$GRU_SAN[which(Data2009$IDHEMOCLAS==4 & Data2009$IDFACTORRH==2) ]=8
Data2009$GRU_SAN[which(Data2009$IDHEMOCLAS==9) ]=NA
Data2009=subset(Data2009,select=-c(IDHEMOCLAS,IDFACTORRH))


Data2009$GRU_SAN[which(Data2009$GRU_SAN=='9')]=NA
Data2009$SEG_SOCIAL[which(Data2009$SEG_SOCIAL=='9')]=NA
Data2009$EDAD_MADRE[which(Data2009$EDAD_MADRE==99)]=NA
Data2009$EST_CIVM[which(Data2009$EST_CIVM=='9')]=NA
Data2009$EST_CIVM=revalue(Data2009$EST_CIVM, c("1"="4","2"="4","6"="2", "4"="3", "3"="5", "5"="1" ))

Data2009$NIV_EDUM[which(Data2009$NIV_EDUM=='99')]=NA
Data2009$NIV_EDUM=revalue(Data2009$NIV_EDUM, c("01"="0","02"="1","03"="2", "04"="2", "05"="2", "06"="2", "07"="3", "08"="3", "09"="3", "10"="3", "11"="3", "12"="3" ,"13"="0"))


Data2009$DPTO_R[which(Data2009$DPTO_R=='01')]=NA
Data2009$MUNIC_R[which(Data2009$MUNIC_R=='999')]=NA
Data2009$AREA_R[which(Data2009$AREA_R=='9')]=NA
Data2009$N_HIJOSV[which(Data2009$N_HIJOSV==99)]=NA
Data2009$N_EMB[which(Data2009$N_EMB==99)]=NA
Data2009$EDAD_PADRE[which(Data2009$EDAD_PADRE==99)]=NA
Data2009$N_EMB[which(Data2009$N_EMB==99)]=NA
Data2009$NIV_EDUP[which(Data2009$NIV_EDUP=='9')]=NA

Data2009$NIV_EDUP2=NA
Data2009$NIV_EDUP2[which(Data2009$NIV_EDUP=='01')]='1'
Data2009$NIV_EDUP2[which(Data2009$ULTCURMAD<5 & Data2009$NIV_EDUP=='02')]='3'
Data2009$NIV_EDUP2[which(Data2009$ULTCURMAD==5 & Data2009$NIV_EDUP=='02')]='2'

Data2009$NIV_EDUP2[which((Data2009$ULTCURMAD==11 |Data2009$ULTCURMAD==12 |Data2009$ULTCURMAD==13) & (Data2009$NIV_EDUP=='03' | Data2009$NIV_EDUP=='04' | Data2009$NIV_EDUP=='05' | Data2009$NIV_EDUP=='06'))]='4'
Data2009$NIV_EDUP2[which(Data2009$ULTCURMAD>5 & Data2009$ULTCURMAD<11 & (Data2009$NIV_EDUP=='03'  | Data2009$NIV_EDUP=='04' | Data2009$NIV_EDUP=='05' | Data2009$NIV_EDUP=='06') )]='5'

Data2009$NIV_EDUP2[which(Data2009$NIV_EDUP=='13')]='8'
Data2009$NIV_EDUP2[which(Data2009$ULTCURMAD==0)]='8'

Data2009$NIV_EDUP2[which(Data2009$NIV_EDUP=='07' | Data2009$NIV_EDUP=='08' | Data2009$NIV_EDUP=='09' | Data2009$NIV_EDUP=='10' |
Data2009$NIV_EDUP=='11' |Data2009$NIV_EDUP=='12' )]='6' 

Data2009$NIV_EDUP=as.factor(Data2009$NIV_EDUP2)


Data2009$SECTOR=NA
Data2009$SECCION=NA
Data2009$PROFESION=NA
Data2009$COD_REGION=NA
Data2009$VALIDAR=NA
Data2009=subset(Data2009,select=-c(NIV_EDUP2))

#Remove data from 2009 that most other years do not have
Data2009=subset(Data2009,select=-c(OTRO_SIT,HORA,MINUTOS,OTRPARATX,IDPUEBLOIN,NOM_PUEB,ULTCURMAD,VEREDAMAD,IDCLASADMI,NOMCLASAD,ULTCURPAD,IDDEPTOEXP,IDMUNICEXP,FECHAGRA))
Data2009$COD_INST=as.character(Data2009$COD_INST)

######################
Data2010=read.dbf('RawData/Nacimientos/nac2010.dbf', as.is = FALSE)

Data2010=rename(Data2010, c("COD_MUNIC"="MUNIC_NACI", "COD_INSP"="INSP_NACI", "COD_DPTO"="DPTO_NACI",
"AREANAC"="AREA_NACI", "NUMCONSUL"="NUM_CONSUL", "CODPTORE"="DPTO_R",
"CODMUNRE"="MUNIC_R", "AREA_RES"="AREA_R", "CODIGO"="CODIGO_CP", "FECHA_NACM"="FECHA_ANTN"))

Data2010$FECHA_NAC =as.Date(Data2010$FECHA_NAC,format="%Y-%m-%d")
Data2010$FECHA_ANTN =as.Date(Data2010$FECHA_ANTN,format="%Y-%m-%d")
Data2010$FECHA_EXP =as.Date(Data2010$FECHA_EXP,format="%Y-%m-%d")


Data2010$AREA_NACI[which(Data2010$AREA_NACI=='9')]=NA
Data2010$SIT_PARTO[which(Data2010$SIT_PARTO=='9')]=NA
Data2010$PESO_NAC[which(Data2010$PESO_NAC==9999)]=NA
Data2010$TALLA_NAC[which(Data2010$TALLA_NAC==99)]=NA
Data2010$ATEN_PAR[which(Data2010$ATEN_PAR=='9')]=NA

Data2010$SEMANAS=Data2010$T_GES
Data2010$SEMANAS[which(Data2010$SEMANAS=='98')]=NA
Data2010$SEMANAS[which(Data2010$SEMANAS=='99')]=NA
Data2010$SEMANAS=as.numeric(as.character(Data2010$SEMANAS))
Data2010$T_GES=NA
Data2010$T_GES[which(Data2010$SEMANAS<20)]=1
Data2010$T_GES[which(Data2010$SEMANAS>=20 & Data2010$SEMANAS<=27)]=2
Data2010$T_GES[which(Data2010$SEMANAS>=28 & Data2010$SEMANAS<=37)]=3
Data2010$T_GES[which(Data2010$SEMANAS>=38 & Data2010$SEMANAS<=41)]=4
Data2010$T_GES[which(Data2010$SEMANAS>=42)]=5

Data2010$NUM_CONSUL[which(Data2010$NUM_CONSUL==99)]=NA
Data2010$NUM_CONSUL[which(Data2010$NUM_CONSUL=='99')]=NA

Data2010$TIPO_PARTO[which(Data2010$TIPO_PARTO=='9')]=NA
Data2010$MUL_PARTO[which(Data2010$MUL_PARTO=='9')]=NA

Data2010$APGAR_CONTINUO=Data2010$APGAR1
Data2010$APGAR1=revalue(Data2010$APGAR1, c("0"="1","01"="1","02"="1", "03"="1", "04"="1", "05"="2", "06"="2", "07"="3","08"="3","09"="3","10"="3"  ))
Data2010$APGAR2=revalue(Data2010$APGAR2, c("00"="1","01"="1","02"="1", "03"="1", "04"="1", "05"="2", "06"="2", "07"="3","08"="3","09"="3","10"="3"  ))
Data2010$APGAR1[which(Data2010$APGAR1=='99')]=NA
Data2010$APGAR2[which(Data2010$APGAR2=='99')]=NA
Data2010$APGAR_CONTINUO[which(Data2010$APGAR_CONTINUO=='99')]=NA



Data2010$GRU_SAN=NA
Data2010$GRU_SAN[which(Data2010$IDHEMOCLAS==1 & Data2010$IDFACTORRH==1) ]=1
Data2010$GRU_SAN[which(Data2010$IDHEMOCLAS==1 & Data2010$IDFACTORRH==2) ]=2
Data2010$GRU_SAN[which(Data2010$IDHEMOCLAS==2 & Data2010$IDFACTORRH==1) ]=3
Data2010$GRU_SAN[which(Data2010$IDHEMOCLAS==2 & Data2010$IDFACTORRH==2) ]=4
Data2010$GRU_SAN[which(Data2010$IDHEMOCLAS==3 & Data2010$IDFACTORRH==1) ]=5
Data2010$GRU_SAN[which(Data2010$IDHEMOCLAS==3 & Data2010$IDFACTORRH==2) ]=6
Data2010$GRU_SAN[which(Data2010$IDHEMOCLAS==4 & Data2010$IDFACTORRH==1) ]=7
Data2010$GRU_SAN[which(Data2010$IDHEMOCLAS==4 & Data2010$IDFACTORRH==2) ]=8
Data2010$GRU_SAN[which(Data2010$IDHEMOCLAS==9) ]=NA
Data2010=subset(Data2010,select=-c(IDHEMOCLAS,IDFACTORRH))


Data2010$GRU_SAN[which(Data2010$GRU_SAN=='9')]=NA
Data2010$SEG_SOCIAL[which(Data2010$SEG_SOCIAL=='9')]=NA
Data2010$EDAD_MADRE[which(Data2010$EDAD_MADRE==99)]=NA
Data2010$EST_CIVM[which(Data2010$EST_CIVM=='9')]=NA
Data2010$EST_CIVM=revalue(Data2010$EST_CIVM, c("1"="4","2"="4","6"="2", "4"="3", "3"="5", "5"="1" ))

Data2010$NIV_EDUM[which(Data2010$NIV_EDUM=='99')]=NA
Data2010$NIV_EDUM=revalue(Data2010$NIV_EDUM, c("01"="0","02"="1","03"="2", "04"="2", "05"="2", "06"="2", "07"="3", "08"="3", "09"="3", "10"="3", "11"="3", "12"="3" ,"13"="0"))


Data2010$DPTO_R[which(Data2010$DPTO_R=='01')]=NA
Data2010$MUNIC_R[which(Data2010$MUNIC_R=='999')]=NA
Data2010$AREA_R[which(Data2010$AREA_R=='9')]=NA
Data2010$N_HIJOSV[which(Data2010$N_HIJOSV==99)]=NA
Data2010$N_EMB[which(Data2010$N_EMB==99)]=NA
Data2010$EDAD_PADRE[which(Data2010$EDAD_PADRE==99)]=NA
Data2010$N_EMB[which(Data2010$N_EMB==99)]=NA
Data2010$NIV_EDUP[which(Data2010$NIV_EDUP=='9')]=NA

Data2010$NIV_EDUP2=NA
Data2010$NIV_EDUP2[which(Data2010$NIV_EDUP=='01')]='1'
Data2010$NIV_EDUP2[which(Data2010$ULTCURMAD<5 & Data2010$NIV_EDUP=='02')]='3'
Data2010$NIV_EDUP2[which(Data2010$ULTCURMAD==5 & Data2010$NIV_EDUP=='02')]='2'

Data2010$NIV_EDUP2[which((Data2010$ULTCURMAD==11 |Data2010$ULTCURMAD==12 |Data2010$ULTCURMAD==13) & (Data2010$NIV_EDUP=='03' | Data2010$NIV_EDUP=='04' | Data2010$NIV_EDUP=='05' | Data2010$NIV_EDUP=='06'))]='4'
Data2010$NIV_EDUP2[which(Data2010$ULTCURMAD>5 & Data2010$ULTCURMAD<11 & (Data2010$NIV_EDUP=='03'  | Data2010$NIV_EDUP=='04' | Data2010$NIV_EDUP=='05' | Data2010$NIV_EDUP=='06') )]='5'

Data2010$NIV_EDUP2[which(Data2010$NIV_EDUP=='13')]='8'
Data2010$NIV_EDUP2[which(Data2010$ULTCURMAD==0)]='8'

Data2010$NIV_EDUP2[which(Data2010$NIV_EDUP=='07' | Data2010$NIV_EDUP=='08' | Data2010$NIV_EDUP=='09' | Data2010$NIV_EDUP=='10' |
Data2010$NIV_EDUP=='11' |Data2010$NIV_EDUP=='12' )]='6' 

Data2010$NIV_EDUP=as.factor(Data2010$NIV_EDUP2)


Data2010$SECTOR=NA
Data2010$SECCION=NA
Data2010$PROFESION=NA
Data2010$COD_REGION=NA
Data2010$VALIDAR=NA
Data2010=subset(Data2010,select=-c(NIV_EDUP2))

#Remove data from 2010 that most other years do not have
Data2010=subset(Data2010,select=-c(OTRO_SIT,HORA,MINUTOS,OTRPARATX,IDPUEBLOIN,NOM_PUEB,ULTCURMAD,VEREDAMAD,IDCLASADMI,NOMCLASAD,ULTCURPAD,IDDEPTOEXP,IDMUNICEXP,FECHAGRA,BARRMADRE,DIREMADRE))
Data2010$COD_INST=as.character(Data2010$COD_INST)

######################
Data2011=read.dbf('RawData/Nacimientos/nac2011.dbf', as.is = FALSE)

Data2011=rename(Data2011, c("COD_MUNIC"="MUNIC_NACI", "COD_INSP"="INSP_NACI", "COD_DPTO"="DPTO_NACI",
"AREANAC"="AREA_NACI", "NUMCONSUL"="NUM_CONSUL", "CODPTORE"="DPTO_R",
"CODMUNRE"="MUNIC_R", "AREA_RES"="AREA_R", "CODIGO"="CODIGO_CP", "FECHA_NACM"="FECHA_ANTN"))

Data2011$FECHA_NAC =as.Date(Data2011$FECHA_NAC,format="%Y-%m-%d")
Data2011$FECHA_ANTN =as.Date(Data2011$FECHA_ANTN,format="%Y-%m-%d")
Data2011$FECHA_EXP =as.Date(Data2011$FECHA_EXP,format="%Y-%m-%d")


Data2011$AREA_NACI[which(Data2011$AREA_NACI=='9')]=NA
Data2011$SIT_PARTO[which(Data2011$SIT_PARTO=='9')]=NA
Data2011$PESO_NAC[which(Data2011$PESO_NAC==9999)]=NA
Data2011$TALLA_NAC[which(Data2011$TALLA_NAC==99)]=NA
Data2011$ATEN_PAR[which(Data2011$ATEN_PAR=='9')]=NA

Data2011$SEMANAS=Data2011$T_GES
Data2011$SEMANAS[which(Data2011$SEMANAS=='98')]=NA
Data2011$SEMANAS[which(Data2011$SEMANAS=='99')]=NA
Data2011$SEMANAS=as.numeric(as.character(Data2011$SEMANAS))
Data2011$T_GES=NA
Data2011$T_GES[which(Data2011$SEMANAS<20)]=1
Data2011$T_GES[which(Data2011$SEMANAS>=20 & Data2011$SEMANAS<=27)]=2
Data2011$T_GES[which(Data2011$SEMANAS>=28 & Data2011$SEMANAS<=37)]=3
Data2011$T_GES[which(Data2011$SEMANAS>=38 & Data2011$SEMANAS<=41)]=4
Data2011$T_GES[which(Data2011$SEMANAS>=42)]=5

Data2011$NUM_CONSUL[which(Data2011$NUM_CONSUL==99)]=NA
Data2011$NUM_CONSUL[which(Data2011$NUM_CONSUL=='99')]=NA

Data2011$TIPO_PARTO[which(Data2011$TIPO_PARTO=='9')]=NA
Data2011$MUL_PARTO[which(Data2011$MUL_PARTO=='9')]=NA

Data2011$APGAR_CONTINUO=Data2011$APGAR1
Data2011$APGAR1=revalue(Data2011$APGAR1, c("0"="1","01"="1","02"="1", "03"="1", "04"="1", "05"="2", "06"="2", "07"="3","08"="3","09"="3","10"="3"  ))
Data2011$APGAR2=revalue(Data2011$APGAR2, c("0"="1","01"="1","02"="1", "03"="1", "04"="1", "05"="2", "06"="2", "07"="3","08"="3","09"="3","10"="3"  ))
Data2011$APGAR1[which(Data2011$APGAR1=='99')]=NA
Data2011$APGAR2[which(Data2011$APGAR2=='99')]=NA
Data2011$APGAR_CONTINUO[which(Data2011$APGAR_CONTINUO=='99')]=NA



Data2011$GRU_SAN=NA
Data2011$GRU_SAN[which(Data2011$IDHEMOCLAS==1 & Data2011$IDFACTORRH==1) ]=1
Data2011$GRU_SAN[which(Data2011$IDHEMOCLAS==1 & Data2011$IDFACTORRH==2) ]=2
Data2011$GRU_SAN[which(Data2011$IDHEMOCLAS==2 & Data2011$IDFACTORRH==1) ]=3
Data2011$GRU_SAN[which(Data2011$IDHEMOCLAS==2 & Data2011$IDFACTORRH==2) ]=4
Data2011$GRU_SAN[which(Data2011$IDHEMOCLAS==3 & Data2011$IDFACTORRH==1) ]=5
Data2011$GRU_SAN[which(Data2011$IDHEMOCLAS==3 & Data2011$IDFACTORRH==2) ]=6
Data2011$GRU_SAN[which(Data2011$IDHEMOCLAS==4 & Data2011$IDFACTORRH==1) ]=7
Data2011$GRU_SAN[which(Data2011$IDHEMOCLAS==4 & Data2011$IDFACTORRH==2) ]=8
Data2011$GRU_SAN[which(Data2011$IDHEMOCLAS==9) ]=NA
Data2011=subset(Data2011,select=-c(IDHEMOCLAS,IDFACTORRH))


Data2011$GRU_SAN[which(Data2011$GRU_SAN=='9')]=NA
Data2011$SEG_SOCIAL[which(Data2011$SEG_SOCIAL=='9')]=NA
Data2011$EDAD_MADRE[which(Data2011$EDAD_MADRE==99)]=NA
Data2011$EST_CIVM[which(Data2011$EST_CIVM=='9')]=NA
Data2011$EST_CIVM=revalue(Data2011$EST_CIVM, c("1"="4","2"="4","6"="2", "4"="3", "3"="5", "5"="1" ))


Data2011$NIV_EDUM[which(Data2011$NIV_EDUM=='99')]=NA
Data2011$NIV_EDUM=revalue(Data2011$NIV_EDUM, c("01"="0","02"="1","03"="2", "04"="2", "05"="2", "06"="2", "07"="3", "08"="3", "09"="3", "10"="3", "11"="3", "12"="3" ,"13"="0"))


Data2011$DPTO_R[which(Data2011$DPTO_R=='01')]=NA
Data2011$MUNIC_R[which(Data2011$MUNIC_R=='999')]=NA
Data2011$AREA_R[which(Data2011$AREA_R=='9')]=NA
Data2011$N_HIJOSV[which(Data2011$N_HIJOSV==99)]=NA
Data2011$N_EMB[which(Data2011$N_EMB==99)]=NA
Data2011$EDAD_PADRE[which(Data2011$EDAD_PADRE==99)]=NA
Data2011$N_EMB[which(Data2011$N_EMB==99)]=NA
Data2011$NIV_EDUP[which(Data2011$NIV_EDUP=='9')]=NA

Data2011$NIV_EDUP2=NA
Data2011$NIV_EDUP2[which(Data2011$NIV_EDUP=='01')]='1'
Data2011$NIV_EDUP2[which(Data2011$ULTCURMAD<5 & Data2011$NIV_EDUP=='02')]='3'
Data2011$NIV_EDUP2[which(Data2011$ULTCURMAD==5 & Data2011$NIV_EDUP=='02')]='2'

Data2011$NIV_EDUP2[which((Data2011$ULTCURMAD==11 |Data2011$ULTCURMAD==12 |Data2011$ULTCURMAD==13) & (Data2011$NIV_EDUP=='03' | Data2011$NIV_EDUP=='04' | Data2011$NIV_EDUP=='05' | Data2011$NIV_EDUP=='06'))]='4'
Data2011$NIV_EDUP2[which(Data2011$ULTCURMAD>5 & Data2011$ULTCURMAD<11 & (Data2011$NIV_EDUP=='03'  | Data2011$NIV_EDUP=='04' | Data2011$NIV_EDUP=='05' | Data2011$NIV_EDUP=='06') )]='5'

Data2011$NIV_EDUP2[which(Data2011$NIV_EDUP=='13')]='8'
Data2011$NIV_EDUP2[which(Data2011$ULTCURMAD==0)]='8'

Data2011$NIV_EDUP2[which(Data2011$NIV_EDUP=='07' | Data2011$NIV_EDUP=='08' | Data2011$NIV_EDUP=='09' | Data2011$NIV_EDUP=='10' |
Data2011$NIV_EDUP=='11' |Data2011$NIV_EDUP=='12' )]='6' 

Data2011$NIV_EDUP=as.factor(Data2011$NIV_EDUP2)


Data2011$SECTOR=NA
Data2011$SECCION=NA
Data2011$PROFESION=NA
Data2011$COD_REGION=NA
Data2011$VALIDAR=NA
Data2011=subset(Data2011,select=-c(NIV_EDUP2))

#Remove data from 2011 that most other years do not have
Data2011=subset(Data2011,select=-c(OTRO_SIT,HORA,MINUTOS,OTRPARATX,IDPUEBLOIN,NOM_PUEB,ULTCURMAD,VEREDAMAD,IDCLASADMI,NOMCLASAD,ULTCURPAD,IDDEPTOEXP,IDMUNICEXP,FECHAGRA,BARRMADRE,DIREMADRE))
Data2011$COD_INST=as.character(Data2011$COD_INST)

######################
Data2012=read.dbf('RawData/Nacimientos/nac2012.dbf', as.is = FALSE)

Data2012=rename(Data2012, c("COD_MUNIC"="MUNIC_NACI", "COD_INSP"="INSP_NACI", "COD_DPTO"="DPTO_NACI",
"AREANAC"="AREA_NACI", "NUMCONSUL"="NUM_CONSUL", "CODPTORE"="DPTO_R",
"CODMUNRE"="MUNIC_R", "AREA_RES"="AREA_R", "CODIGO"="CODIGO_CP", "FECHA_NACM"="FECHA_ANTN"))

Data2012$FECHA_NAC =as.Date(Data2012$FECHA_NAC,format="%Y-%m-%d")
Data2012$FECHA_ANTN =as.Date(Data2012$FECHA_ANTN,format="%Y-%m-%d")
Data2012$FECHA_EXP =as.Date(Data2012$FECHA_EXP,format="%Y-%m-%d")


Data2012$AREA_NACI[which(Data2012$AREA_NACI=='9')]=NA
Data2012$SIT_PARTO[which(Data2012$SIT_PARTO=='9')]=NA
Data2012$PESO_NAC[which(Data2012$PESO_NAC==9999)]=NA
Data2012$TALLA_NAC[which(Data2012$TALLA_NAC==99)]=NA
Data2012$ATEN_PAR[which(Data2012$ATEN_PAR=='9')]=NA

Data2012$SEMANAS=Data2012$T_GES
Data2012$SEMANAS[which(Data2012$SEMANAS=='98')]=NA
Data2012$SEMANAS[which(Data2012$SEMANAS=='99')]=NA
Data2012$SEMANAS=as.numeric(as.character(Data2012$SEMANAS))
Data2012$T_GES=NA
Data2012$T_GES[which(Data2012$SEMANAS<20)]=1
Data2012$T_GES[which(Data2012$SEMANAS>=20 & Data2012$SEMANAS<=27)]=2
Data2012$T_GES[which(Data2012$SEMANAS>=28 & Data2012$SEMANAS<=37)]=3
Data2012$T_GES[which(Data2012$SEMANAS>=38 & Data2012$SEMANAS<=41)]=4
Data2012$T_GES[which(Data2012$SEMANAS>=42)]=5

Data2012$NUM_CONSUL[which(Data2012$NUM_CONSUL==99)]=NA
Data2012$NUM_CONSUL[which(Data2012$NUM_CONSUL=='99')]=NA

Data2012$TIPO_PARTO[which(Data2012$TIPO_PARTO=='9')]=NA
Data2012$MUL_PARTO[which(Data2012$MUL_PARTO=='9')]=NA
Data2012$APGAR_CONTINUO=Data2012$APGAR1
Data2012$APGAR1=revalue(Data2012$APGAR1, c("0"="1","01"="1","02"="1", "03"="1", "04"="1", "05"="2", "06"="2", "07"="3","08"="3","09"="3","10"="3"  ))
Data2012$APGAR2=revalue(Data2012$APGAR2, c("0"="1","01"="1","02"="1", "03"="1", "04"="1", "05"="2", "06"="2", "07"="3","08"="3","09"="3","10"="3"  ))
Data2012$APGAR1[which(Data2012$APGAR1=='99')]=NA
Data2012$APGAR2[which(Data2012$APGAR2=='99')]=NA
Data2012$APGAR_CONTINUO[which(Data2012$APGAR_CONTINUO=='99')]=NA



Data2012$GRU_SAN=NA
Data2012$GRU_SAN[which(Data2012$IDHEMOCLAS==1 & Data2012$IDFACTORRH==1) ]=1
Data2012$GRU_SAN[which(Data2012$IDHEMOCLAS==1 & Data2012$IDFACTORRH==2) ]=2
Data2012$GRU_SAN[which(Data2012$IDHEMOCLAS==2 & Data2012$IDFACTORRH==1) ]=3
Data2012$GRU_SAN[which(Data2012$IDHEMOCLAS==2 & Data2012$IDFACTORRH==2) ]=4
Data2012$GRU_SAN[which(Data2012$IDHEMOCLAS==3 & Data2012$IDFACTORRH==1) ]=5
Data2012$GRU_SAN[which(Data2012$IDHEMOCLAS==3 & Data2012$IDFACTORRH==2) ]=6
Data2012$GRU_SAN[which(Data2012$IDHEMOCLAS==4 & Data2012$IDFACTORRH==1) ]=7
Data2012$GRU_SAN[which(Data2012$IDHEMOCLAS==4 & Data2012$IDFACTORRH==2) ]=8
Data2012$GRU_SAN[which(Data2012$IDHEMOCLAS==9) ]=NA
Data2012=subset(Data2012,select=-c(IDHEMOCLAS,IDFACTORRH))


Data2012$GRU_SAN[which(Data2012$GRU_SAN=='9')]=NA
Data2012$SEG_SOCIAL[which(Data2012$SEG_SOCIAL=='9')]=NA
Data2012$EDAD_MADRE[which(Data2012$EDAD_MADRE==99)]=NA
Data2012$EST_CIVM[which(Data2012$EST_CIVM=='9')]=NA
Data2012$EST_CIVM=revalue(Data2012$EST_CIVM, c("1"="4","2"="4","6"="2", "4"="3", "3"="5", "5"="1" ))


Data2012$NIV_EDUM[which(Data2012$NIV_EDUM=='99')]=NA
Data2012$NIV_EDUM=revalue(Data2012$NIV_EDUM, c("01"="0","02"="1","03"="2", "04"="2", "05"="2", "06"="2", "07"="3", "08"="3", "09"="3", "10"="3", "11"="3", "12"="3" ,"13"="0"))


Data2012$DPTO_R[which(Data2012$DPTO_R=='01')]=NA
Data2012$MUNIC_R[which(Data2012$MUNIC_R=='999')]=NA
Data2012$AREA_R[which(Data2012$AREA_R=='9')]=NA
Data2012$N_HIJOSV[which(Data2012$N_HIJOSV==99)]=NA
Data2012$N_EMB[which(Data2012$N_EMB==99)]=NA
Data2012$EDAD_PADRE[which(Data2012$EDAD_PADRE==99)]=NA
Data2012$N_EMB[which(Data2012$N_EMB==99)]=NA
Data2012$NIV_EDUP[which(Data2012$NIV_EDUP=='9')]=NA

Data2012$NIV_EDUP2=NA
Data2012$NIV_EDUP2[which(Data2012$NIV_EDUP=='01')]='1'
Data2012$NIV_EDUP2[which(Data2012$ULTCURMAD<5 & Data2012$NIV_EDUP=='02')]='3'
Data2012$NIV_EDUP2[which(Data2012$ULTCURMAD==5 & Data2012$NIV_EDUP=='02')]='2'

Data2012$NIV_EDUP2[which((Data2012$ULTCURMAD==11 |Data2012$ULTCURMAD==12 |Data2012$ULTCURMAD==13) & (Data2012$NIV_EDUP=='03' | Data2012$NIV_EDUP=='04' | Data2012$NIV_EDUP=='05' | Data2012$NIV_EDUP=='06'))]='4'
Data2012$NIV_EDUP2[which(Data2012$ULTCURMAD>5 & Data2012$ULTCURMAD<11 & (Data2012$NIV_EDUP=='03'  | Data2012$NIV_EDUP=='04' | Data2012$NIV_EDUP=='05' | Data2012$NIV_EDUP=='06') )]='5'

Data2012$NIV_EDUP2[which(Data2012$NIV_EDUP=='13')]='8'
Data2012$NIV_EDUP2[which(Data2012$ULTCURMAD==0)]='8'

Data2012$NIV_EDUP2[which(Data2012$NIV_EDUP=='07' | Data2012$NIV_EDUP=='08' | Data2012$NIV_EDUP=='09' | Data2012$NIV_EDUP=='10' |
Data2012$NIV_EDUP=='11' |Data2012$NIV_EDUP=='12' )]='6' 

Data2012$NIV_EDUP=as.factor(Data2012$NIV_EDUP2)


Data2012$SECTOR=NA
Data2012$SECCION=NA
Data2012$PROFESION=NA
Data2012$COD_REGION=NA
Data2012$VALIDAR=NA
Data2012=subset(Data2012,select=-c(NIV_EDUP2))
Data2012$COD_INST=as.character(Data2012$COD_INST)

#Remove data from 2012 that most other years do not have
Data2012=subset(Data2012,select=-c(OTRO_SIT,HORA,MINUTOS,OTRPARATX,IDPUEBLOIN,NOM_PUEB,ULTCURMAD,VEREDAMAD,IDCLASADMI,NOMCLASAD,ULTCURPAD,IDDEPTOEXP,IDMUNICEXP,FECHAGRA,BARRMADRE,DIREMADRE))

##Now merge all the years
DataCompleta=rbind(Data1998,Data1999,Data2000,Data2001,Data2002,Data2003,Data2004,Data2005,Data2006,Data2007,Data2008,Data2009,Data2010,Data2011,Data2012)
rm(Data1998,Data1999,Data2000,Data2001,Data2002,Data2003,Data2004,Data2005,Data2006,Data2007,Data2008,Data2009,Data2010,Data2011,Data2012,Data2013,Data2014)

##Drop levels not used
DataCompleta=droplevels( DataCompleta)



##This is a data set with the hospitals babys are born at
DataInst=DataCompleta[,c("DPTO_NACI","MUNIC_NACI","NOM_INST")]
DataInst=unique(DataInst)
DataInst=DataInst[which(!is.na(DataInst[,3])),]
DataInst=unique(DataInst)
save( DataInst,file='CreatedData/DataInst.RData') 

##Now lets do some general data cleaning
DataCompleta <- subset(DataCompleta,select=-c(VALIDAR,PROFESION,SECTOR,SECCION, CODPRES,NOM_INST,COD_REGION))
DataCompleta$NUM_CONSUL=as.numeric(DataCompleta$NUM_CONSUL)

DataCompleta$T_GES[which(DataCompleta$T_GES=='.')]=NA
DataCompleta$T_GES[which(DataCompleta$T_GES=='9')]=NA
DataCompleta$GRU_SAN[which(DataCompleta$GRU_SAN=='0')]=NA

DataCompleta=DataCompleta[-which(DataCompleta$FECHA_EXP<as.Date("01-01-1998",format="%d-%m-%Y")),]
DataCompleta=DataCompleta[-which(DataCompleta$FECHA_EXP>as.Date("31-12-2013",format="%d-%m-%Y")),]

DataCompleta$SEMANAS=as.numeric(as.character(DataCompleta$SEMANAS))

DataCompleta$PESO_NAC[which(DataCompleta$PESO_NAC<1500)]=NA
DataCompleta$PESO_NAC[which(DataCompleta$PESO_NAC>4300)]=NA
DataCompleta$TALLA_NAC[which(DataCompleta$TALLA_NAC<40)]=NA
DataCompleta$TALLA_NAC[which(DataCompleta$TALLA_NAC>55)]=NA

DataCompleta$ANO=as.numeric(as.character(DataCompleta$ANO))
DataCompleta$MES=as.numeric(as.character(DataCompleta$MES))
DataCompleta$AREA_NACI=as.numeric(as.character(DataCompleta$AREA_NACI))
DataCompleta$SIT_PARTO=as.numeric(as.character(DataCompleta$SIT_PARTO))
DataCompleta$SEXO=as.numeric(as.character(DataCompleta$SEXO))
DataCompleta$ATEN_PAR=as.numeric(as.character(DataCompleta$ATEN_PAR))
DataCompleta$TIPO_PARTO=as.numeric(as.character(DataCompleta$TIPO_PARTO))
DataCompleta$MUL_PARTO=as.numeric(as.character(DataCompleta$MUL_PARTO))
DataCompleta$APGAR1=as.numeric(as.character(DataCompleta$APGAR1))
DataCompleta$APGAR2=as.numeric(as.character(DataCompleta$APGAR2))
DataCompleta$GRU_SAN=as.numeric(as.character(DataCompleta$GRU_SAN))
DataCompleta$SEG_SOCIAL=as.numeric(as.character(DataCompleta$SEG_SOCIAL))
DataCompleta$EST_CIVM=as.numeric(as.character(DataCompleta$EST_CIVM))
DataCompleta$NIV_EDUM=as.numeric(as.character(DataCompleta$NIV_EDUM))
DataCompleta$NIV_EDUP=as.numeric(as.character(DataCompleta$NIV_EDUP))
DataCompleta$MES2=format(DataCompleta$FECHA_NAC, "%m")
DataCompleta$MES2=as.numeric(as.character(DataCompleta$MES2))
DataCompleta$COD_INST=as.character(DataCompleta$COD_INST)

DataCompleta=subset(DataCompleta,select=-c(INSP_NACI,COD_LOCA,AREA_R,CODIGO_CP))
DataCompleta$Quarter=as.numeric(substr(quarters(DataCompleta$FECHA_NAC),2,2))
DataCompleta$GEST_COMPLETA=0
DataCompleta$GEST_COMPLETA[which(DataCompleta$T_GES>=4)]=1
DataCompleta$APGAR_BAJO=0
DataCompleta$APGAR_BAJO[which(DataCompleta$APGAR1==1 | DataCompleta$APGAR1==2)]=1
DataCompleta$LBW=0
DataCompleta$LBW[which(DataCompleta$PESO_NAC<2500)]=1
DataCompleta$VLBW=0
DataCompleta$VLBW[which(DataCompleta$PESO_NAC<1500)]=1
DataCompleta$MASC=0
DataCompleta$MASC[which(DataCompleta$SEXO==1)]=1
DataCompleta$PartoEspontaneo=0
DataCompleta$PartoEspontaneo[which(DataCompleta$TIPO_PARTO==1)]=1
DataCompleta$Csection=0
DataCompleta$Csection[which(DataCompleta$TIPO_PARTO==2)]=1
DataCompleta$MultipleBirth=0
DataCompleta$MultipleBirth[which(DataCompleta$MUL_PARTO>1)]=1
DataCompleta$PartoHospital=0
DataCompleta$PartoHospital[which(DataCompleta$SIT_PARTO==1)]=1
DataCompleta$MadreMenor14=0
DataCompleta$MadreMenor14[which(DataCompleta$EDAD_MADRE<14)]=1
DataCompleta$Madre14_17=0
DataCompleta$Madre14_17[which(DataCompleta$EDAD_MADRE>=14 & DataCompleta$EDAD_MADRE<=17)]=1
DataCompleta$ConsultasPreMayor4=0
DataCompleta$ConsultasPreMayor4[which(DataCompleta$NUM_CONSUL>=4)]=1
DataCompleta$RegimenContributivo=0
DataCompleta$RegimenContributivo[which(DataCompleta$SEG_SOCIAL==1)]=1
DataCompleta$RegimenSubsidiado=0
DataCompleta$RegimenSubsidiado[which(DataCompleta$SEG_SOCIAL==2)]=1
DataCompleta$MadreSoltera=0
DataCompleta$MadreSoltera[which(DataCompleta$EST_CIVM==1)]=1
DataCompleta$EduMadrePostPrimaria=0
DataCompleta$EduMadrePostPrimaria[which(DataCompleta$NIV_EDUM>=2)]=1
DataCompleta$EduMadrePostSecundaria=0
DataCompleta$EduMadrePostSecundaria[which(DataCompleta$NIV_EDUM>=3)]=1

DataCompleta=DataCompleta[which(DataCompleta$FECHA_NAC>=as.Date("01-01-1999",format="%d-%m-%Y")),]  #the data from 1998 is terrible in terms of APGAR
DataCompleta$CODIGO_DANE=paste(DataCompleta$DPTO_NACI,DataCompleta$MUNIC_NACI,sep="")
DataCompleta$CODIGO_DANE_M=paste(DataCompleta$DPTO_R,DataCompleta$MUNIC_R,sep="")
DataCompleta$CODIGO_DANE_M[which(is.na(DataCompleta$DPTO_R) | is.na(DataCompleta$MUNIC_R))]=NA
DataCompleta$CODIGO_DANE_M=as.numeric(DataCompleta$CODIGO_DANE_M)
DataCompleta$APGAR_BAJO=as.numeric(as.character(DataCompleta$APGAR1)=="1" | as.character(DataCompleta$APGAR1)=="2")
DataCompleta$CODIGO_DANE=as.numeric(DataCompleta$CODIGO_DANE)

#DataCompleta$CODIGO_DANE_M[which(DataCompleta$CODIGO_DANE_M==13490)]=13600
#DataCompleta$CODIGO_DANE_M[which(DataCompleta$CODIGO_DANE_M==19300)]=19142
#DataCompleta$CODIGO_DANE_M[which(DataCompleta$CODIGO_DANE_M==27160)]=27787
#DataCompleta$CODIGO_DANE_M[which(DataCompleta$CODIGO_DANE_M==27580)]=27205
#DataCompleta$CODIGO_DANE_M[which(DataCompleta$CODIGO_DANE_M==27205 | DataCompleta$CODIGO_DANE_M==27361 | DataCompleta$CODIGO_DANE_M==27491 | DataCompleta$CODIGO_DANE_M==27450)]=27999
setdiff(unique(DataCompleta$CODIGO_DANE_M), as.numeric(as.character(Municipios$CODANE2)))
setdiff(as.numeric(as.character(Municipios$CODANE2)),unique(DataCompleta$CODIGO_DANE_M))
##Solo dejamos municipios con codigo dane en DIVIPOLA GIS
DataCompleta=DataCompleta[which(DataCompleta$CODIGO_DANE_M %in% Municipios$CODANE2),]
save( DataCompleta,file='CreatedData/DataCompleta.RData')

###########################
##DIVIPOLA (ADMING BOUNDRIES COLOMBIA)
###########################
#Tildes dont work in Sherlock
if (FALSE) {


###########################
## GOLD production data
###########################



}

###########################
##The POPULATION DATA
###########################

PoblacionMunicipio=read.csv('RawData/PoblacionMunicipio.csv')

#PoblacionMunicipio$CodigoDane[which(PoblacionMunicipio$CodigoDane==13490)]=13600
#PoblacionMunicipio$CodigoDane[which(PoblacionMunicipio$CodigoDane==19300)]=19142
#PoblacionMunicipio$CodigoDane[which(PoblacionMunicipio$CodigoDane==27160)]=27787
#PoblacionMunicipio$CodigoDane[which(PoblacionMunicipio$CodigoDane==27580)]=27205
#PoblacionMunicipio$CodigoDane[which(PoblacionMunicipio$CodigoDane==27205 | PoblacionMunicipio$CodigoDane==27361 | PoblacionMunicipio$CodigoDane==27491 | PoblacionMunicipio$CodigoDane==27450)]=27999
PoblacionMunicipio=aggregate(PoblacionMunicipio[,setdiff(colnames(PoblacionMunicipio),c("CodigoDane"))],by=list(PoblacionMunicipio$CodigoDane),FUN=sum)
colnames(PoblacionMunicipio)[1]=c("CodigoDane")

PoblacionMunicipio$CodigoDane=as.numeric(as.character(PoblacionMunicipio$CodigoDane))
PoblacionMunicipio=reshape(PoblacionMunicipio, varying = list(colnames(PoblacionMunicipio)[-1]),direction="long",idvar="CodigoDane")
PoblacionMunicipio$Year=rep(1985:2200, each=length(unique(PoblacionMunicipio$CodigoDane)))[1:dim(PoblacionMunicipio)[1]]
PoblacionMunicipio$Poblacion=PoblacionMunicipio$X1985.00
PoblacionMunicipio=subset(PoblacionMunicipio,  select=c(CodigoDane,Year,Poblacion))
PoblacionMunicipio=PoblacionMunicipio[which(PoblacionMunicipio$CodigoDane %in% Municipios$CODANE2),] #Borrar todo lo que no este en divipola GIS
write.csv(PoblacionMunicipio,file='CreatedData/PoblacionMunicipio.csv',row.names=F)
save(PoblacionMunicipio,file='CreatedData/PoblacionMunicipio.RData') 

###########################
##The ILLEGAL MINING DATA
###########################


BaseMineriaIlegal_MinDefensa=read.csv('RawData/BaseMineriaIlegal_MinDefensa.csv')
BaseMineriaIlegal_MinDefensa$ACCION=as.character(BaseMineriaIlegal_MinDefensa$ACCION)
BaseMineriaIlegal_MinDefensa$TIPO=as.character(BaseMineriaIlegal_MinDefensa$TIPO)
BaseMineriaIlegal_MinDefensa$MUNICIPIO=as.character(BaseMineriaIlegal_MinDefensa$MUNICIPIO)
BaseMineriaIlegal_MinDefensa$DEPTO=as.character(BaseMineriaIlegal_MinDefensa$DEPTO)
INDEX_ORO=which(grepl("ORO",as.character(BaseMineriaIlegal_MinDefensa$TIPO)))
BaseMineriaIlegal_MinDefensa=BaseMineriaIlegal_MinDefensa[INDEX_ORO,]
BaseMineriaIlegal_MinDefensa=BaseMineriaIlegal_MinDefensa[which(BaseMineriaIlegal_MinDefensa$ACCION=="INTERVENIDA"),]
BaseMineriaIlegal_MinDefensa$FECHA=as.Date(as.character(BaseMineriaIlegal_MinDefensa$FECHA),format="%m/%d/%Y")
BaseMineriaIlegal_MinDefensa$Year=as.numeric(format(BaseMineriaIlegal_MinDefensa$FECHA,format="%Y"))
BaseMineriaIlegal_MinDefensa$Quarter=as.numeric(substr(quarters(BaseMineriaIlegal_MinDefensa$FECHA),2,2))
write.csv(BaseMineriaIlegal_MinDefensa,file='CreatedData/BaseMineriaIlegal_MinDefensa.csv',row.names=F)
save(BaseMineriaIlegal_MinDefensa,file='CreatedData/BaseMineriaIlegal_MinDefensa.RData')
 
BaseMineriaIlegal_Trim=aggregate(BaseMineriaIlegal_MinDefensa$CANTIDAD,by=list(BaseMineriaIlegal_MinDefensa$COD_MUNI,BaseMineriaIlegal_MinDefensa$Year,BaseMineriaIlegal_MinDefensa$Quarter),FUN=sum)
colnames(BaseMineriaIlegal_Trim)=c("CodigoDane","Year","Quarter","Interv_Mines")

#BaseMineriaIlegal_Trim$CodigoDane[which(BaseMineriaIlegal_Trim$CodigoDane==13490)]=13600
#BaseMineriaIlegal_Trim$CodigoDane[which(BaseMineriaIlegal_Trim$CodigoDane==19300)]=19142
#BaseMineriaIlegal_Trim$CodigoDane[which(BaseMineriaIlegal_Trim$CodigoDane==27160)]=27787
#BaseMineriaIlegal_Trim$CodigoDane[which(BaseMineriaIlegal_Trim$CodigoDane==27580)]=27205
#BaseMineriaIlegal_Trim$CodigoDane[which(BaseMineriaIlegal_Trim$CodigoDane==27205 | BaseMineriaIlegal_Trim$CodigoDane==27361 | BaseMineriaIlegal_Trim$CodigoDane==27491 | BaseMineriaIlegal_Trim$CodigoDane==27450)]=27999
BaseMineriaIlegal_Trim=aggregate(BaseMineriaIlegal_Trim[,setdiff(colnames(BaseMineriaIlegal_Trim),c("CodigoDane","Year","Quarter"))],by=list(BaseMineriaIlegal_Trim$CodigoDane,BaseMineriaIlegal_Trim$Year,BaseMineriaIlegal_Trim$Quarter),FUN=sum)
colnames(BaseMineriaIlegal_Trim)[1:3]=c("CodigoDane","Year","Quarter")

BaseMineriaIlegal_Trim=BaseMineriaIlegal_Trim[which(BaseMineriaIlegal_Trim$CodigoDane %in% Municipios$CODANE2),] #Borrar todo lo que no este en divipola GIS
write.csv(BaseMineriaIlegal_Trim,file='CreatedData/BaseMineriaIlegal_Trim.csv',row.names=F)
save(BaseMineriaIlegal_Trim,file='CreatedData/BaseMineriaIlegal_Trim.RData') 


###########################
##The SIVIGILA DATA
###########################



###########################
##The RIPS DATA
###########################



###########################
##The GOLD PRICE DATA
###########################

PriceGold=read.csv('RawData/PriceGold.csv')
colnames(PriceGold)=c("ANO","MES","PriceTroy")
CPI_USA=read.csv('RawData/CPI_USA.csv')

colnames(CPI_USA)[-1]=paste("M",1:12,sep=".")
CPI_USA=reshape(CPI_USA, varying = colnames(CPI_USA)[-1],
        idvar = colnames(CPI_USA)[1],direction="long") 
colnames(CPI_USA)=c("ANO","MES","CPI")

PriceGold=merge(CPI_USA,PriceGold)
PriceGold$PriceReal=100*PriceGold$PriceTroy/PriceGold$CPI
PriceGold$Quarter[PriceGold$MES<=3]=1
PriceGold$Quarter[PriceGold$MES>3 & PriceGold$MES<=6]=2
PriceGold$Quarter[PriceGold$MES>6 & PriceGold$MES<=9]=3
PriceGold$Quarter[PriceGold$MES>9 & PriceGold$MES<=12]=4
PriceGold$YM=PriceGold$ANO+(PriceGold$MES-1)/12
PriceGold=PriceGold[sort.int(PriceGold$YM,index.return=T)$ix,]

 PriceGold$PriceRealMA=c(rep(NA,8),rollmean(PriceGold$PriceReal, 9))

write.csv(PriceGold,file='CreatedData/PriceGold.csv',row.names=F)
save(PriceGold,file='CreatedData/PriceGold.RData')
write.dta(PriceGold,file='CreatedData/PriceGold.dta')    





###############################
############################
##### CREATE USEFUL RASTERS
###############################
############################


#La funcion de polygonos que se usa mas adelante
voronoipolygons <- function(x) {
  require(deldir)
  require(sp)
  if (.hasSlot(x, 'coords')) {
      crds <- x@coords
    } else {
      crds <- x 
    }
  z <- deldir(crds[,1], crds[,2])
  w <- tile.list(z)
  polys <- vector(mode='list', length=length(w))
  for (i in seq(along=polys)) {
    pcrds <- cbind(w[[i]]$x, w[[i]]$y)
    pcrds <- rbind(pcrds, pcrds[1,])
    polys[[i]] <- Polygons(list(Polygon(pcrds)), ID=row.names(x)[i])
  }
  voronoi <- SpatialPolygons(polys)
    voronoi <- SpatialPolygonsDataFrame(voronoi, data=data.frame(x=x@data, row.names=sapply(slot(voronoi, 'polygons'), function(x) slot(x, 'ID'))))
}


load("CreatedData/Municipios_GIS.RData")

## Import, project and crop population raster
load("CreatedData/RasterPop.RData")
RasterPop <- crop(RasterPop, extent(Municipios)) #crops the raster to the extent of the polygon, I do this first because it speeds the mask up
#MuniRaster=raster("CreatedData/GIS/Mauro_mail/muni_raster/")
#MuniRaster <- crop(MuniRaster, extent(Municipios))
#crs(MuniRaster) <- CRS(proj4string(goldfile))
Municipios@data$CODANE2=as.numeric(as.character(Municipios@data$CODANE2))
MuniRaster=rasterize(Municipios,RasterPop,field="CODANE2",background=NA)
PopMuniRaster=zonal(RasterPop, MuniRaster, fun='sum')
save(MuniRaster,file="CreatedData/MuniRaster.RData")

save(PopMuniRaster,MuniRaster,RasterPop,file="CreatedData/RasterizedStuff.RData")



##############################
###############################
#######THIS SECTION CALCULATES THE FLOW OF THE RIVER
#############################
###########################

load("CreatedData/COL_riv_proj.RData")
FindNextEdge <- function( edge, DS = 3 ){
  ##edge is the id_edge of the river, DS is whether we want downstream (1),upstream (2) or both (3)
  ##It returns a matrix with 3 columns. Column 1(named id_edge) has the id of the river edge next to the river. 
  ##Column 2(named distance) has the distance to the next edge. 
  ##Column 3(named direction) is equal to 1 if we are looking downstream, and -1 if we are looking upstream
  
  ##Lets intialize EdgeDistMat to NULL in case we dont return anything
  EdgeDistMat=NULL
  EdgeDistMatDown=NULL
  EdgeDistMatUp=NULL
  ##First lets sort the id_edges so we can use the trick of id+1 to locate the edge
  if(is.unsorted(COL_riv_proj$id_edge)==T) COL_riv_proj=COL_riv_proj[sort.int(COL_riv_proj$id_edge,index.return=T)$ix,]
  ##Since its sortes we can now just add 1 to locate the edge
  edge_start <- edge + 1
  if( DS==1 | DS==3 ){ ##if for downstream
    ##Lets see which one is the next edge first...
    vertex_end <- COL_riv_proj@data[ edge_start, c( "id_end_vertex" )]
    edge_end <- COL_riv_proj@data[which(COL_riv_proj@data$id_start_vertex==vertex_end), "id_edge"]
    ##its possible there are no next edges (i.e., the river goes to the ocean). In that case do not do anything else
    if(length(edge_end)==1){
      XY_start <- COL_riv_proj@data[ edge_start, c( "xstart", "ystart")]
      ##Mauricio:notice you can just get the this by using the end vertex coordinates from the edge... but your's is fine too.
      XY_end <- COL_riv_proj@data[ edge_end+1, c( "xstart", "ystart")]
      if(identical(COL_riv_proj@data[ edge_end+1, c( "xstart", "ystart")],COL_riv_proj@data[ edge_start, c( "xend", "yend")])) stop("We messed up") ##Mauricio: This is just a sanity check
      ##Using the which function we can get the ID from the edges that end have as a start vertex, the ending vertix of the input edge
      d <- sqrt( (XY_end[1,1] - XY_start[1,1])^2 + (XY_end[1,2] - XY_start[1,2])^2 )
      ##edge_end <- vertex_end - 1 ## Mauricio: This is wrong, because several edges can have the same vertex_end... the mapping between edge and vertex is not invertible. 
      EdgeDistMatDown <- matrix( c( edge_end, d,1 ), nrow = 1, ncol = 3 )
      colnames( EdgeDistMatDown ) <- c( "id_edge", "distance","direction" )
    }
    if(length(edge_end)>1){
      stop("This is really weird, there is a river that splits in two... look into it")
    }  
  }

  if( DS == 2 | DS==3 ){ ##For upstream
   vertex_start <- COL_riv_proj@data[ edge_start, c( "id_start_vertex" )]
   ## edgesUp has the id_edge for each edge that flows into edge we started with. 
   edgesUp <- COL_riv_proj@data[which(COL_riv_proj@data$id_end_vertex==vertex_start), "id_edge"]
   ##This is the number of rivers upstream... its possible there are none if this is where the river begins. In that case return NULL
    l <- length( edgesUp )
    if(l!=0){ ##only go forward if there are some upstream rivers edges
     
      EdgeDistMatUp <- matrix(NA,nrow = l, ncol = 3 ) ##Mauricio: Is more efficient to allocate the whole matrix from the beginning. 
      colnames( EdgeDistMatUp ) <- c( "id_edge", "distance","direction" ) 
      for( i in 1:l ){## This for loop should be quite short since it only goes through and mesures the
		      ## distances for the few edges we recorded.
	##Vertex_start <- edgesUp[i] + 1 ## Mauricio:This is wrong... see comment above 
	XY_start=COL_riv_proj@data[which(COL_riv_proj@data$id_edge==edgesUp[i]), c( "xstart", "ystart")]
	XY_end <- COL_riv_proj@data[ edge_start, c( "xstart", "ystart")]
	if(identical(COL_riv_proj@data[which(COL_riv_proj@data$id_edge==edgesUp[i]), c( "xend", "yend")],COL_riv_proj@data[ edge_start, c( "xstart", "ystart")])) stop("We messed up") ##Mauricio: This is just a sanity check
	d <- sqrt( (XY_end[1,1] - XY_start[1,1])^2 + (XY_end[1,2] - XY_start[1,2])^2 )
	EdgeDistMatUp[i,]= c(edgesUp[i], d,-1 )
      }
    }
  }
  EdgeDistMat=rbind(EdgeDistMatDown,EdgeDistMatUp)
  return( EdgeDistMat )
  ## This Function returns the imidiate id_edge next to it (DS or upstream) and the distance to it, plus whether is upstream or downstream
}

listEdgeDistMat_Downstream=list()
listEdgeDistMat_Upstream=list()
##Initialize to avoid trouble
for(edge in COL_riv_proj@data$id_edge){
listEdgeDistMat_Downstream[[edge+1]]=FindNextEdge(edge,1)
listEdgeDistMat_Upstream[[edge+1]]=FindNextEdge(edge,2)
}
listEdgeDistMat_Downstream[[length(COL_riv_proj@data$id_edge)+1]]=NA
listEdgeDistMat_Upstream[[length(COL_riv_proj@data$id_edge)+1]]=NA



##plot some stuff to check it out
# plot_id_edge=3
# plot(COL_riv_proj[listreturn[[plot_id_edge+1]][,1]+1,],lwd=2)
# plot(COL_riv_proj[plot_id_edge+1,],col=2,add=T,lwd=2)

save(listEdgeDistMat_Downstream,listEdgeDistMat_Upstream,file="CreatedData/listEdgeDistMat.RData")



FindEdge <- function(edge, DS = 1, maxdist=50000){
##This function can only take DS=1 or 2... making it 3 makes it weird... think about it and you will see... going up and then downstream...
  if(DS==3) stop("You can't do upstram and downstream at the same time.. sorry bud")
  if(DS==1) MatrixResults=listEdgeDistMat_Downstream[[edge+1]]
  if(DS==2) MatrixResults=listEdgeDistMat_Upstream[[edge+1]]
  if(!is.null(MatrixResults)){ ##only do if there is anything to do
    MatrixResults <- cbind( MatrixResults, 0 ) ##I added a 0 that will tell me whether I have already look into this river segment or not
    ##if column 4, which keeps track of whether we have checked a river segment or not is >0, then we have to look into them
    while(sum( MatrixResults[,4] == 0 )>0){ ##while there are some river edges to be visited
      index_look <- which(MatrixResults[,4]==0)[1] ##look at the first of the river segments not visited yet
      MatrixResults[index_look,4] <- 1 ##Basically I just note that I have already done this one
      if(MatrixResults[index_look,2] < maxdist){
	if(DS==1) MatrixResultTemp=listEdgeDistMat_Downstream[[MatrixResults[ index_look, 1]+1]]
	if(DS==2) MatrixResultTemp=listEdgeDistMat_Upstream[[MatrixResults[ index_look, 1]+1]]
	if(!is.null(MatrixResultTemp)){ ##Only do more... if there is anything to do
	  MatrixResultTemp[,2] <- MatrixResultTemp[,2] + MatrixResults[index_look,2] ##The distance up to that point needs to be added
	  MatrixResultTemp <- cbind(MatrixResultTemp,0) #I haven't look into this ones yet
	  MatrixResults <- rbind( MatrixResults, MatrixResultTemp )
	}##close if null
      }##close if maxdist
    }##close while
  }##if null beggining
  return(MatrixResults[,1:3])
}

listEdgeDistMatAccumulada_Downstream=list()
listEdgeDistMatAccumulada_Upstream=list()
for(edge in COL_riv_proj@data$id_edge){
listEdgeDistMatAccumulada_Downstream[[edge+1]]=FindEdge(edge,1, maxdist=50000)
listEdgeDistMatAccumulada_Upstream[[edge+1]]=FindEdge(edge,2, maxdist=50000)
}

save(listEdgeDistMatAccumulada_Downstream,listEdgeDistMatAccumulada_Upstream,file="CreatedData/listEdgeDistMatAccumulada.RData")



    
###This script calculates the mined area in each municipality and the cummulative mined area in each municipality, at each point in time.
##To do this it uses the mines_pedacitos file

load('CreatedData/Municipios_GIS.RData')
load('CreatedData/MuniRaster.RData')
MuniRaster30 <- raster(resolution=1000,ext=extent(MuniRaster),crs=CRS(projectionAll))
MuniRaster30=resample(MuniRaster, MuniRaster30,method="ngb")

for(i in 2004:2014){

load(paste0("CreatedData/TMC_",i,".RData"))
TMC=TMC_Final
#plot(TMC[1:100,])
#plot(MuniRaster,add=T,col=2)

MuniExposure1=NULL
MuniExposure2=NULL
MuniExposure3=NULL
if(length(which(TMC$DN==1))>0){
                               rTMC1=rasterize(TMC[which(TMC$DN==1),],MuniRaster30,field="DN",background=NA)
                               MuniExposure1=zonal(rTMC1, MuniRaster30, fun='count')*res(rTMC1)[1]*res(rTMC1)[2]/(1000^2)
}
if(length(which(TMC$DN==2))>0){
                               rTMC2=rasterize(TMC[which(TMC$DN==2),],MuniRaster30,field="DN",background=NA)
                               MuniExposure2=zonal(rTMC2, MuniRaster30, fun='count')*res(rTMC2)[1]*res(rTMC2)[2]/(1000^2)
}
if(length(which(TMC$DN==3))>0){
                               rTMC3=rasterize(TMC[which(TMC$DN==3),],MuniRaster30,field="DN",background=NA)
                               MuniExposure3=zonal(rTMC3, MuniRaster30, fun='count')*res(rTMC3)[1]*res(rTMC3)[2]/(1000^2)
}
if(length(which(TMC$DN==4))>0){
                            rTMC4=rasterize(TMC[which(TMC$DN==4),],MuniRaster30,field="DN",background=NA)
                            MuniExposure4=zonal(rTMC4, MuniRaster30, fun='count')*res(rTMC4)[1]*res(rTMC4)[2]/(1000^2)
}


save(MuniExposure1,MuniExposure2,MuniExposure3,MuniExposure4,file=paste0("CreatedData/MatrizAreaMinadaAccumulada",i,".RData"))   
rm(TMC,TMC_Final)
  }

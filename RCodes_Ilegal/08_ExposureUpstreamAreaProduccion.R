availableCores<-detectCores()-1
no_cores <- max(1, availableCores)
# Initiate cluster
cl <- makeCluster(no_cores)
registerDoParallel(cl)
#set seed for reproducible results
clusterSetRNGStream(cl,123) 

load("CreatedData/RasterizedStuff.RData")
load("CreatedData/COL_riv_proj.RData")


  ############ PARA AREA  

  foreach(i=2004:2014,.packages=c("sp","raster","rgeos","foreign")) %dopar% {
   for(tipo in 1:4){
   load(paste0("CreatedData/AreaMinadaUpstrea_",tipo,".RData"))

      #Select the mines
    t0=Sys.time()
    fechatable=cbind(FIDEdgeVec,AreaMinadaFIDTiempoUpstream[,i-2004+1])
    colnames(fechatable)=c("id_edge","polucion")
    riverfile_merge=merge(COL_riv_proj,fechatable,by="id_edge")
    INDEX=which(riverfile_merge@data$polucion>0)
    if(length(INDEX)>0){
    riverfile_merge=riverfile_merge[INDEX,]
    riverfile_merge=riverfile_merge[,c("id_edge","polucion")]
    riverfile_buffer1 <- gBuffer(riverfile_merge, width=5*1000,byid=T,capStyle="FLAT") #units are in mts
    riverfile_buffer2 <- gBuffer(riverfile_merge, width=10*1000,byid=T,capStyle="FLAT") #units are in mts
    riverfile_buffer3 <- gBuffer(riverfile_merge, width=20*1000,byid=T,capStyle="FLAT") #units are in mts
    RasterGold1=rasterize(riverfile_buffer1,RasterPop,field="polucion",background=0)
    RasterGold2=rasterize(riverfile_buffer2,RasterPop,field="polucion",background=0)
    RasterGold3=rasterize(riverfile_buffer3,RasterPop,field="polucion",background=0)   
    RasterExposure1 <- overlay(RasterGold1, RasterPop, fun=function(x,y){return(x*y)})
    RasterExposure2 <- overlay(RasterGold2, RasterPop, fun=function(x,y){return(x*y)})
    RasterExposure3 <- overlay(RasterGold3, RasterPop, fun=function(x,y){return(x*y)}) 
    RasterExposure=brick(RasterExposure1,RasterExposure2,RasterExposure3)
    
    MuniExposure=zonal(RasterExposure, MuniRaster, fun='sum')
    MuniExposure[,c(2:4)]=MuniExposure[,(2:4)]/PopMuniRaster[,c(2,2,2)]
   
    colnames(MuniExposure)=c("CODANE2","Buffer5km","Buffer10km","Buffer20km")
    
    save(MuniExposure,file=paste0("CreatedData/PolExpMuniFecha_",i,"_Tipo",tipo,"_All.RData"))

    tf=Sys.time()
    print(tf-t0)
    gc()
    rm(goldfile_merge,fechatable,goldfile_buffer1,goldfile_buffer2,goldfile_buffer3,RasterGold1,RasterGold2,RasterGold3,RasterExposure1,RasterExposure2,RasterExposure3,RasterExposure,MuniExposure)
    }
  }
  
  }

  
    stopCluster(cl)
stopImplicitCluster()


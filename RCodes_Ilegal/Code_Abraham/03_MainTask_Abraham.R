load("CreatedData/COL_riv_proj.RData")
load('CreatedData/Municipios_GIS.RData')
load('CreatedData/MINAS_ORO_Dump.RData')
load("CreatedData/listEdgeDistMat.RData")
load("CreatedData/RasterizedStuff.RData") 
load("CreatedData/listEdgeDistMatAccumulada.RData")

# Proportion of municiplaity's population within buffer
n = dim(MINAS_ORO)[1]
MuniProximityTotalDown=NULL
MuniPropInBufferTotalDown=NULL
#First lets do it downstream
for( i in 1:n ){ # The parallel comand should be checked before running. 
  t0 <- Sys.time()
  mine <- MINAS_ORO@data[i,'CODIGO_EXP']
  EdgeDump <- MINAS_ORO@data[i,'EdgeDump']
  path=listEdgeDistMatAccumulada_Downstream[[EdgeDump+1]]
  if(!is.null(path)){
    path=matrix(path,ncol=3)
    edges <- path[,1]
    reference_rows <- edges + 1 # This would let us extract the info from the COL_riv_proj
    # for these particular edges
    RiverData <- COL_riv_proj[reference_rows,]
    RiverBuf_1 <- gBuffer( RiverData, width = 1*1000, byid = T,capStyle="FLAT" ) ##1KM
    RiverBuf_5 <- gBuffer( RiverData, width = 5*1000, byid = T,capStyle="FLAT" )  ##5KM
    RiverBuf_10 <- gBuffer( RiverData, width = 10*1000, byid = T,capStyle="FLAT" )  ##10KM
    RiverBuf_20 <- gBuffer( RiverData, width = 20*1000, byid = T,capStyle="FLAT" )  ##20KM
    
    # Proportion of municiplaity's population within buffer
    RasterRiverBuf_1 <- rasterize(RiverBuf_1,RasterPop, field = 1, background=0)
    RasterRiverBuf_5 <- rasterize(RiverBuf_5,RasterPop, field = 1, background=0)
    RasterRiverBuf_10 <- rasterize(RiverBuf_10,RasterPop, field = 1, background=0)
    RasterRiverBuf_20 <- rasterize(RiverBuf_20,RasterPop, field = 1, background=0)
    
    Overlay_1 <- overlay(RasterRiverBuf_1, RasterPop, fun=function(x,y){return(x*y)})
    Overlay_5 <- overlay(RasterRiverBuf_5, RasterPop, fun=function(x,y){return(x*y)})
    Overlay_10 <- overlay(RasterRiverBuf_10, RasterPop, fun=function(x,y){return(x*y)})
    Overlay_20 <- overlay(RasterRiverBuf_20, RasterPop, fun=function(x,y){return(x*y)})
    
    RasterProximity <- brick(Overlay_1, Overlay_5, Overlay_10, Overlay_20 )
    MuniProximity <- zonal(RasterProximity, MuniRaster, fun='sum')
    MuniProximity[ , c(2:5) ] <- MuniProximity[,(2:5)]/PopMuniRaster[ , c(2,2,2,2) ]
    colnames(MuniProximity) <- c("CODANE2","Pop1km","Pop5km","Pop10km","Pop20km")
    MuniProximity=MuniProximity[which(rowSums(MuniProximity[,2:5])!=0),]
    MuniProximity=matrix(MuniProximity,ncol=5)
    MuniProximity=data.frame(MuniProximity)
    MuniProximity$CODIGO_EXP=as.character(mine)
    colnames(MuniProximity)=c("CODANE2","Pop1km","Pop5km","Pop10km","Pop20km","CODIGO_EXP")
    MuniProximityTotalDown=rbind(MuniProximityTotalDown,MuniProximity)
    
    # Proportion of municiplaity's Area within buffer
    # 1km
    RiverBuf_1=gUnaryUnion(RiverBuf_1)
    int_1 <- intersect(RiverBuf_1, Municipios)
    int_1$section_area_m2 <- sapply(slot(int_1, "polygons"), slot, "area") 
    int_1$ProCovered <- int_1$section_area_m2/int_1$area_m2
    tab_1 <- aggregate( int_1$ProCovered, by = list(CODANE2 = int_1$CODANE2), FUN = "sum")
    colnames(tab_1) <- c("CODANE2", "1km")
    
    # 5km
    RiverBuf_5=gUnaryUnion(RiverBuf_5)
    int_5 <- intersect(RiverBuf_5, Municipios)
    int_5$section_area_m2 <- sapply(slot(int_5, "polygons"), slot, "area")
    int_5$ProCovered <- int_5$section_area_m2/int_5$area_m2
    tab_5 <- aggregate( int_5$ProCovered, by = list(CODANE2 = int_5$CODANE2), FUN = "sum")
    colnames(tab_5) <- c("CODANE2", "5km")
    
    # 10km
    RiverBuf_10=gUnaryUnion(RiverBuf_10)
    int_10 <- intersect(RiverBuf_10, Municipios)
    int_10$section_area_m2 <- sapply(slot(int_10, "polygons"), slot, "area")
    int_10$ProCovered <- int_10$section_area_m2/int_10$area_m2
    tab_10 <- aggregate( int_10$ProCovered, by = list(CODANE2 = int_10$CODANE2), FUN = "sum")
    colnames(tab_10) <- c("CODANE2", "10km")
    
    # 20km
    RiverBuf_20=gUnaryUnion(RiverBuf_20)
    int_20 <- intersect(RiverBuf_20, Municipios)
    int_20$section_area_m2 <- sapply(slot(int_20, "polygons"), slot, "area")
    int_20$ProCovered <- int_20$section_area_m2/int_20$area_m2
    tab_20 <- aggregate( int_20$ProCovered, by = list(CODANE2 = int_20$CODANE2), FUN = "sum")
    colnames(tab_20) <- c("CODANE2", "20km")
    
    
    Mtemp <- merge( tab_1, tab_5, by = "CODANE2", all = TRUE )
    Mtemp <- merge( Mtemp, tab_10, by = "CODANE2", all = TRUE )
    MuniPropInBuffer <- merge( Mtemp, tab_20, by = "CODANE2", all = TRUE )
    colnames(MuniPropInBuffer)= c("CODANE2","Area1km","Area5km","Area10km","Area20km")
    MuniPropInBuffer$Area1km[is.na(MuniPropInBuffer$Area1km)]=0
    MuniPropInBuffer$Area5km[is.na(MuniPropInBuffer$Area5km)]=0
    MuniPropInBuffer$Area10km[is.na(MuniPropInBuffer$Area10km)]=0
  #MuniPropInBuffer$Area20km[is.na(MuniPropInBuffer$Area20km)]=0 ##This line is really useless
    # This following save comands should be checked before running the code to ensrue that they are properly written
    MuniPropInBuffer=matrix(MuniPropInBuffer,ncol=5)
    MuniPropInBuffer=data.frame(MuniPropInBuffer)
    MuniPropInBuffer$CODIGO_EXP=as.character(mine)
    colnames(MuniPropInBuffer)=c("CODANE2","Area1km","Area5km","Area10km","Area20km","CODIGO_EXP")
    MuniPropInBufferTotalDown=rbind(MuniPropInBufferTotalDown,MuniPropInBuffer)
  }
  tf <- Sys.time()
  print(tf - t0)
}



MuniProximityTotalUp=NULL
MuniPropInBufferTotalUp=NULL
##Now lets do it upstream
for( i in 1:n ){ # The parallel comand should be checked before running. 
  t0 <- Sys.time()
  mine <- MINAS_ORO@data[i,'CODIGO_EXP']
  EdgeDump <- MINAS_ORO@data[i,'EdgeDump']
  path=listEdgeDistMatAccumulada_Upstream[[EdgeDump+1]]
  if(!is.null(path)){
    path=matrix(path,ncol=3)
    edges <- path[,1]
    reference_rows <- edges + 1 # This would let us extract the info from the COL_riv_proj
    # for these particular edges
    RiverData <- COL_riv_proj[reference_rows,]
    RiverBuf_1 <- gBuffer( RiverData, width = 1*1000, byid = T,capStyle="FLAT" ) ##1KM
    RiverBuf_5 <- gBuffer( RiverData, width = 5*1000, byid = T,capStyle="FLAT" )  ##5KM
    RiverBuf_10 <- gBuffer( RiverData, width = 10*1000, byid = T,capStyle="FLAT" )  ##10KM
    RiverBuf_20 <- gBuffer( RiverData, width = 20*1000, byid = T,capStyle="FLAT" )  ##20KM
    
    # Proportion of municiplaity's population within buffer
    RasterRiverBuf_1 <- rasterize(RiverBuf_1,RasterPop, field = 1, background=0)
    RasterRiverBuf_5 <- rasterize(RiverBuf_5,RasterPop, field = 1, background=0)
    RasterRiverBuf_10 <- rasterize(RiverBuf_10,RasterPop, field = 1, background=0)
    RasterRiverBuf_20 <- rasterize(RiverBuf_20,RasterPop, field = 1, background=0)
    
    Overlay_1 <- overlay(RasterRiverBuf_1, RasterPop, fun=function(x,y){return(x*y)})
    Overlay_5 <- overlay(RasterRiverBuf_5, RasterPop, fun=function(x,y){return(x*y)})
    Overlay_10 <- overlay(RasterRiverBuf_10, RasterPop, fun=function(x,y){return(x*y)})
    Overlay_20 <- overlay(RasterRiverBuf_20, RasterPop, fun=function(x,y){return(x*y)})
    
    RasterProximity <- brick(Overlay_1, Overlay_5, Overlay_10, Overlay_20 )
    MuniProximity <- zonal(RasterProximity, MuniRaster, fun='sum')
    MuniProximity[ , c(2:5) ] <- MuniProximity[,(2:5)]/PopMuniRaster[ , c(2,2,2,2) ]
    colnames(MuniProximity) <- c("CODANE2","Pop1km","Pop5km","Pop10km","Pop20km")
    MuniProximity=MuniProximity[which(rowSums(MuniProximity[,2:5])!=0),]
    MuniProximity=matrix(MuniProximity,ncol=5)
    MuniProximity=data.frame(MuniProximity)
    MuniProximity$CODIGO_EXP=as.character(mine)
    colnames(MuniProximity)=c("CODANE2","Pop1km","Pop5km","Pop10km","Pop20km","CODIGO_EXP")
    MuniProximityTotalUp=rbind(MuniProximityTotalUp,MuniProximity)
    
    # Proportion of municiplaity's Area within buffer
    # 1km
    RiverBuf_1=gUnaryUnion(RiverBuf_1)
    int_1 <- intersect(RiverBuf_1, Municipios)
    int_1$section_area_m2 <- sapply(slot(int_1, "polygons"), slot, "area") 
    int_1$ProCovered <- int_1$section_area_m2/int_1$area_m2
    tab_1 <- aggregate( int_1$ProCovered, by = list(CODANE2 = int_1$CODANE2), FUN = "sum")
    colnames(tab_1) <- c("CODANE2", "1km")
    
    # 5km
    RiverBuf_5=gUnaryUnion(RiverBuf_5)
    int_5 <- intersect(RiverBuf_5, Municipios)
    int_5$section_area_m2 <- sapply(slot(int_5, "polygons"), slot, "area")
    int_5$ProCovered <- int_5$section_area_m2/int_5$area_m2
    tab_5 <- aggregate( int_5$ProCovered, by = list(CODANE2 = int_5$CODANE2), FUN = "sum")
    colnames(tab_5) <- c("CODANE2", "5km")
    
    # 10km
    RiverBuf_10=gUnaryUnion(RiverBuf_10)
    int_10 <- intersect(RiverBuf_10, Municipios)
    int_10$section_area_m2 <- sapply(slot(int_10, "polygons"), slot, "area")
    int_10$ProCovered <- int_10$section_area_m2/int_10$area_m2
    tab_10 <- aggregate( int_10$ProCovered, by = list(CODANE2 = int_10$CODANE2), FUN = "sum")
    colnames(tab_10) <- c("CODANE2", "10km")
    
    # 20km
    RiverBuf_20=gUnaryUnion(RiverBuf_20)
    int_20 <- intersect(RiverBuf_20, Municipios)
    int_20$section_area_m2 <- sapply(slot(int_20, "polygons"), slot, "area")
    int_20$ProCovered <- int_20$section_area_m2/int_20$area_m2
    tab_20 <- aggregate( int_20$ProCovered, by = list(CODANE2 = int_20$CODANE2), FUN = "sum")
    colnames(tab_20) <- c("CODANE2", "20km")
    
    
    Mtemp <- merge( tab_1, tab_5, by = "CODANE2", all = TRUE )
    Mtemp <- merge( Mtemp, tab_10, by = "CODANE2", all = TRUE )
    MuniPropInBuffer <- merge( Mtemp, tab_20, by = "CODANE2", all = TRUE )
    colnames(MuniPropInBuffer)= c("CODANE2","Area1km","Area5km","Area10km","Area20km")
    MuniPropInBuffer$Area1km[is.na(MuniPropInBuffer$Area1km)]=0
    MuniPropInBuffer$Area5km[is.na(MuniPropInBuffer$Area5km)]=0
    MuniPropInBuffer$Area10km[is.na(MuniPropInBuffer$Area10km)]=0
  #MuniPropInBuffer$Area20km[is.na(MuniPropInBuffer$Area20km)]=0 ##This line is really useless
    # This following save comands should be checked before running the code to ensrue that they are properly written
    MuniPropInBuffer=matrix(MuniPropInBuffer,ncol=5)
    MuniPropInBuffer=data.frame(MuniPropInBuffer)
    MuniPropInBuffer$CODIGO_EXP=as.character(mine)
    colnames(MuniPropInBuffer)=c("CODANE2","Area1km","Area5km","Area10km","Area20km","CODIGO_EXP")
    MuniPropInBufferTotalUp=rbind(MuniPropInBufferTotalUp,MuniPropInBuffer)
  }
  tf <- Sys.time()
  print(tf - t0)
}

MuniProximityTotalDown$Direction=1
MuniPropInBufferTotalDown$Direction=1
MuniProximityTotalUp$Direction=-1
MuniPropInBufferTotalUp$Direction=-1

MuniProximityTotal=rbind(MuniProximityTotalDown,MuniProximityTotalUp)
MuniPropInBufferTotal=rbind(MuniPropInBufferTotalDown,MuniPropInBufferTotalUp)

save(MuniProximityTotal,MuniPropInBufferTotal,file="CreatedData/UpstreamDownstreamMineMuni.RData")





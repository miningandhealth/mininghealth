load(paste0("CreatedData/TMC_",2010,"_Dump.RData"))

load(paste0("CreatedData/MINAS_ORO_Dump.RData"))
MINAS_ORO <- spTransform(MINAS_ORO, CRS(projectionAll))
Estadistico=NULL
for(i in 1:dim(MINAS_ORO)[1]){

INDEX=gIntersects(MINAS_ORO[i,],TMC,byid=T)
if(sum(INDEX)>0){
Edges=TMC$EdgeDump[which(INDEX==T)]
EdgeOriginal=MINAS_ORO$EdgeDump[i]
Estadistico=rbind(Estadistico,
c(MINAS_ORO$CODIGO_EXP[i],length(Edges),sum(Edges==EdgeOriginal)/length(Edges)))
}
}

ano=2010
Distance=30*5 #in meters
load(paste0("CreatedData/TMC_",ano,".RData"))
TMC=TMC[1:1000,]
TMC_Final=list()
TMC_Final[[1]]=NULL
TMC_Final[[2]]=NULL
TMC_Final[[3]]=NULL
i=0
for(dn in unique(TMC$DN)){
TMC_DN=TMC[TMC$DN==dn,]
TMC_Temp <- gBuffer(TMC_DN, width=Distance, byid=TRUE)
TMC_Temp <- disaggregate(gUnaryUnion(TMC_Temp))

ID=over(TMC_DN,TMC_Temp)
TMC_Convex <- gUnaryUnion(TMC_DN, id = ID)
TMC_Convex=gConvexHull(TMC_Convex, byid=T)
TMC_Convex <- SpatialPolygonsDataFrame(TMC_Convex, data.frame(DN=rep(dn,length(TMC_Convex))))
TMC_Convex$DN=dn
row.names(TMC_Convex)=as.character(i+(1:length(TMC_Convex)))
i=i+length(TMC_Convex)
TMC_Final[[dn]]=TMC_Convex
}

load(paste0("CreatedData/MINAS_ORO.RData"))
MINAS_ORO <- spTransform(MINAS_ORO, CRS(projectionAll))
TITULOS_ANO=MINAS_ORO[which(as.numeric(format(MINAS_ORO$FECHA_INSC,"%Y"))<=ano &  as.numeric(format(MINAS_ORO$FECHA_TERM,"%Y"))>=ano),]
TITULOS_ANO=gBuffer(TITULOS_ANO,width=0)
#Falta hacer el saca huecos
#Se le quitan lo que este dentro de titulos legales a tipo 2
TMC_Final[[2]]=gDifference(TMC_Final[[2]], gUnaryUnion(TITULOS_ANO), byid=T)
#PSe le quitan lo que este afuera de titulos legales a los titulos para tipo 2 y tipo 3
TMC_Final[[1]]=gIntersection(TMC_Final[[1]], gUnaryUnion(TITULOS_ANO), byid=T)
TMC_Final[[3]]=gIntersection(TMC_Final[[3]], gUnaryUnion(TITULOS_ANO), byid=T)
#Despues, se le quita el tipo 3 al tipo 1
TMC_Final[[1]]=gDifference(TMC_Final[[1]], gUnaryUnion(TMC_Final[[3]]), byid=T)

#se une en un solo archivo
TMC_Final=do.call( rbind, TMC_Final )












#####This code reads the mining data and extracts the gold permits
##Creates a file with all the mines that have gold mining
library(cleangeo)

#Load deptos shape, to select only deptos in Cariabean, Andes and Pacific
deptos = readOGR(dsn='RawData/GIS/COL_adm',layer='COL_adm1')

deptos<-spTransform(deptos,projectionAll)
deptos$NAME_1 <- c('AMAZONAS', 'ANTIOQUIA', 'ARAUCA', 'ATLANTICO', 'BOLIVAR', 'BOYACA', 'CALDAS',
                   'CAQUETA', 'CASANARE', 'CAUCA', 'CESAR', 'CHOCO', 'CORDOBA', 'CUNDINAMARCA', 'GUAINIA', 'GUAVIARE', 
                   'HUILA', 'LA GUAJIRA', 'MAGDALENA', 'META', 'NARINO', 'NORTE DE SANTANDER', 'PUTUMAYO', 'QUINDIO',
                   'RISARALDA', 'SAN ANDRES Y PROVIDENCIA', 'SANTANDER', 'SUCRE', 'TOLIMA', 'VALLE DEL CAUCA', 'VAUPES', 'VICHADA')

deptoshealth <-  c('ANTIOQUIA', 'ATLANTICO', 'BOLIVAR', 'BOYACA', 'CALDAS',
                   'CAUCA', 'CESAR', 'CHOCO', 'CORDOBA', 'CUNDINAMARCA',  
                   'HUILA', 'LA GUAJIRA', 'MAGDALENA',  'NARINO', 'NORTE DE SANTANDER',  'QUINDIO',
                   'RISARALDA', 'SANTANDER', 'SUCRE', 'TOLIMA', 'VALLE DEL CAUCA')

# add number if departmento in study regions , otherwise NA
deptos$inhealth <- sapply(deptos$NAME_1, function(x){
  if (x %in% deptoshealth) return(x)
  else return(NA)
})
# subset departmentos study regions
deptos <- deptos[sapply(deptos$inhealth, function(x) !is.na(x)), ]

ind_oro=1
#Load the map of mining potential
#minerpot = readOGR(dsn='RawData/GIS',layer='Minerals')
#minerpot<-spTransform(minerpot,projectionAll)
#oro_pot=minerpot[minerpot$type=='Gold',]
#rm(minerpot)
#oro_pot=gSimplify(oro_pot, tol=1000, topologyPreserve=T)

#Load the map of titles
titulos = readOGR(dsn='RawData/GIS',layer='TITULOS_PAIS')
titulos<-spTransform(titulos,projectionAll)
titulos_oro=titulos[grepl("ORO",titulos$MINERALES)==TRUE,]
rm(titulos)
report <- clgeo_CollectionReport(titulos_oro)
summary <- clgeo_SummaryReport(report)
issues <- report[report$valid == FALSE,]

nv <- clgeo_SuspiciousFeatures(report)

if (!is.na(nv) && nv!=0) {
titulos_oro=titulos_oro[-nv,]
}
titulos_oro=gSimplify(titulos_oro, tol=1000, topologyPreserve=T)

#Load the map of UNODC predictions
ind_unodc=1
if (ind_unodc==1) {
unodc = readOGR(dsn='RawData/GIS',layer='UNODC_digimap')
unodc<-spTransform(unodc,projectionAll)
unodc=gSimplify(unodc, tol=1000, topologyPreserve=T)
unodc=spChFIDs(unodc,as.character((10001:10060)))

titulos_y_unodc=rbind(titulos_oro,unodc)
} else {
titulos_y_unodc=titulos_oro
}

titulos_y_unodc=gSimplify(titulos_y_unodc, tol=1000, topologyPreserve=T)

report <- clgeo_CollectionReport(titulos_y_unodc)
summary <- clgeo_SummaryReport(report)
issues <- report[report$valid == FALSE,]

nv <- clgeo_SuspiciousFeatures(report)

if (!is.na(nv) && nv!=0) {
titulos_y_unodc=titulos_y_unodc[-nv,]
}

SubSetRelevante=gIntersection(titulos_y_unodc,deptos,byid=T)

SubSetRelevante=gSimplify(SubSetRelevante, tol=1000, topologyPreserve=T)


report <- clgeo_CollectionReport(SubSetRelevante)
summary <- clgeo_SummaryReport(report)
issues <- report[report$valid == FALSE,]
nv <- clgeo_SuspiciousFeatures(report)

while(!is.na(nv) & nv!=0){
SubSetRelevante=SubSetRelevante[-nv,]
SubSetRelevante=gSimplify(SubSetRelevante, tol=1000, topologyPreserve=T)
report <- clgeo_CollectionReport(SubSetRelevante)
summary <- clgeo_SummaryReport(report)
issues <- report[report$valid == FALSE,]
nv <- clgeo_SuspiciousFeatures(report)
}



SubSetRelevante=gConvexHull(SubSetRelevante, byid=T)

report <- clgeo_CollectionReport(SubSetRelevante)
summary <- clgeo_SummaryReport(report)
issues <- report[report$valid == FALSE,]
nv <- clgeo_SuspiciousFeatures(report)

while(!is.na(nv) & nv!=0){
SubSetRelevante=SubSetRelevante[-nv,]
SubSetRelevante=gSimplify(SubSetRelevante, tol=1000, topologyPreserve=T)
report <- clgeo_CollectionReport(SubSetRelevante)
summary <- clgeo_SummaryReport(report)
issues <- report[report$valid == FALSE,]
nv <- clgeo_SuspiciousFeatures(report)
}
#Before we were gUning and then disagregating (inefficient)
SubSetRelevantebu=SubSetRelevante
SubSetRelevante=gUnaryUnion(SubSetRelevante)



#####This code reads the mining data and extracts the gold permits
##Creates a file with all the mines that have gold mining
TMC = readOGR("RawData/GIS", "TMC_Jul2012")
TMC <- spTransform(TMC, CRS(projectionAll))
##Santi tried to update to the new Tierra Minada file but it doesnt have FECHA_TERM
#TMC=read.dbf("RawData/GIS/TITULOS_PAIS.dbf")
DataKeep=TMC@data
TMC=gBuffer(TMC,byid=T,width=0)
TMC=SpatialPolygonsDataFrame(TMC, DataKeep, match.ID = TRUE)

##Make some adjustments to keep correct format
TMC$MINERALES=as.character(TMC$MINERALES)
TMC$MODALIDADE=as.character(TMC$MODALIDADE)
TMC$TITULARES=as.character(TMC$TITULARES)
TMC$FECHA_INSC=as.Date(as.character(TMC$FECHA_INSC),format="%Y/%m/%d")
TMC$FECHA_TERM=as.Date(as.character(TMC$FECHA_TERM),format="%Y/%m/%d")

TMC$area_m2=sapply(slot(TMC, "polygons"), slot, "area")
TMC$area_km2=TMC$area_m2/(1000^2) #para que quede en sqkm 

##only keep stuff post 1998(permit expires after this date)
INDEX_FECHA2=which(TMC$FECHA_TERM>as.Date("01-01-1998",format="%d-%m-%Y"))
TMC=TMC[INDEX_FECHA2,]
##Do not use exploration lisences, only explotation
TMC<-TMC[!(TMC$MODALIDADE=="LICENCIA DE EXPLORACION"),]
##which permits are "gold" related
INDEX_ORO=which(grepl("ORO",as.character(TMC$MINERALES)))
##which permits are in a consecion style, and which are trying to legalize small miners
INDEX_CONCENCION=which(grepl("CONTRATO DE CO",as.character(TMC$MODALIDADE)))
INDEX_LEGALIZACION=which(grepl("LEGAL",as.character(TMC$MODALIDADE)))

##Try to extract which permits are given to a natural person, as opposed to a juridical person
t0=regexpr("\\(", TMC$TITULARES)
tf=regexpr("\\)", TMC$TITULARES)
TMC$ID=substr(TMC$TITULARES, t0+1, tf-1)
TMC$ID=gsub("-", "", TMC$ID)
TMC$ID=as.numeric(as.character(TMC$ID))
INDEX_PERSONA=which(TMC$ID<1050000000)


##create some index that show which permits are gold, concesion, legalization and natural person
TMC$ORO=0
TMC$ORO[INDEX_ORO]=1
TMC$CONCENCION=0
TMC$CONCENCION[INDEX_CONCENCION]=1
TMC$LEGALIZACION=0
TMC$LEGALIZACION[INDEX_LEGALIZACION]=1
TMC$PERSONA=0
TMC$PERSONA[INDEX_PERSONA]=1


##keep only gold mines
MINAS_ORO=TMC[INDEX_ORO,]

##I exctract the Codigo_ORO for GIS... not sure if we need this anymore
CODIGO_ORO=MINAS_ORO$CODIGO_EXP
CODIGO_ORO=data.frame(CODIGO_ORO)
CODIGO_ORO=cbind(CODIGO_ORO,1)
colnames(CODIGO_ORO)=c("CODIGO_EXP","oro")
write.csv(CODIGO_ORO,file="CreatedData/GIS/Codigo_Oro.csv",row.names=F)
save(TMC,file="CreatedData/TMC.RData")
write.csv(TMC,file="CreatedData/TMC.csv",row.names=F)
save(MINAS_ORO,file="CreatedData/MINAS_ORO.RData")
MINAS_ORO <- spTransform(MINAS_ORO, CRS(projectionAll))

COL_riv_proj = readOGR("RawData/GIS", "COL_riv_proj")
COL_riv_proj <- spTransform(COL_riv_proj, CRS(projectionAll))

save(SubSetRelevante,deptos,titulos_y_unodc,MINAS_ORO,COL_riv_proj,file=paste0("CreatedData/all03_Inputs.RData"))
	  
# 	  SubSetRelevante=SubSetRelevantebu
# 	   SubSetRelevante=SpatialPolygonsDataFrame(SubSetRelevante,data=data.frame(row=1:length(SubSetRelevante)))
# writeOGR(SubSetRelevante,dsn=paste0("CreatedData"), layer="SubSetRelevante",driver="ESRI Shapefile")
#save(SubSetRelevante,file=paste0("CreatedData/SubSetRelevante.RData"))
 load("CreatedData/RasterizedStuff.RData") 

availableCores<-detectCores()-1
no_cores <- max(1, availableCores)
# Initiate cluster
cl <- makeCluster(no_cores)
registerDoParallel(cl)
#set seed for reproducible results
clusterSetRNGStream(cl,123)  
  ############ PARA AREA

# MuniRaster=resample(MuniRaster, RasterPop)
  foreach(i=2004:2012,.packages=c("sp","raster","rgeos","foreign")) %dopar% {
   load(paste0("CreatedData/TMC_",i,".RData"))
   TMC$area_m2=sapply(slot(TMC, "polygons"), slot, "area")
   for(tipo in 1:ntipos){
      #Select the mines
    TMC_tipo=TMC[TMC$DN==tipo,]
    if(dim(TMC_tipo)[1]>0){
    goldfile_buffer1 <- gBuffer(TMC_tipo, width=5*1000,byid=T) #units are in mts
    goldfile_buffer2 <- gBuffer(TMC_tipo, width=10*1000,byid=T) #units are in mts
    goldfile_buffer3 <- gBuffer(TMC_tipo, width=20*1000,byid=T) #units are in mts
    RasterGold1=rasterize(goldfile_buffer1,RasterPop,field="area_m2",background=0)
    RasterGold2=rasterize(goldfile_buffer2,RasterPop,field="area_m2",background=0)
    RasterGold3=rasterize(goldfile_buffer3,RasterPop,field="area_m2",background=0)
    RasterExposure1 <- overlay(RasterGold1, RasterPop, fun=function(x,y){return(x*y)})
    RasterExposure2 <- overlay(RasterGold2, RasterPop, fun=function(x,y){return(x*y)})
    RasterExposure3 <- overlay(RasterGold3, RasterPop, fun=function(x,y){return(x*y)}) 
    RasterExposure=brick(RasterExposure1,RasterExposure2,RasterExposure3)
    MuniExposure=zonal(RasterExposure, MuniRaster, fun='sum')
    MuniExposure[,c(2:4)]=MuniExposure[,(2:4)]/PopMuniRaster[,c(2,2,2)]
    

    colnames(MuniExposure)=c("CODANE2","Buffer5km","Buffer10km","Buffer20km")
      
    save(MuniExposure,file=paste0("CreatedData/AreaExpMuniFecha_",i,"_Tipo",tipo,".RData"))
    gc()
    rm(goldfile_merge,fechatable,goldfile_buffer1,goldfile_buffer2,goldfile_buffer3,RasterGold1,RasterGold2,RasterGold3,RasterExposure1,RasterExposure2,RasterExposure3,RasterExposure,MuniExposure)
    }
    }
   }

 
  
  
  
  stopCluster(cl)
stopImplicitCluster()
  
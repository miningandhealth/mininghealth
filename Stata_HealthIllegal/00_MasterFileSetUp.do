clear all
*cap ado uninstall hdfe
*ssc install hdfe
set more off
*Set up the R option to run R
*global Rterm_path `"E:\Program Files\R\R-3.1.1\bin\x64\Rterm.exe"'
*global Rterm_options `"--vanilla"'


local corredor=c(username)

	if "`corredor'"=="Santi" {
	global mipath "C:/Users/Santi/Dropbox/MiningAndBirth_Ilegal/"
	  global illegalpath "C:/Users/Santi/Dropbox/MineriaIlegal/"
	global gitpath "C:/Users/Santi/Documents/mininghealth/"
    global latexslides "C:/Users/Santi/Documents/mineriailegal/Latex/201611_SEEPAC/Stata"
	global latexpaper "C:/Users/Santi/Documents/mineriailegal/Latex/201605_Paper_royalties_illegal"
	}
	
	if "`corredor'"=="mauricio" {
	  global illegalpath "/media/mauricio/TeraHDD2/Dropbox/MineriaIlegal/"
	global mipath "/media/mauricio/TeraHDD2/Dropbox/MiningAndBirth_Ilegal"
	global gitpath "/media/mauricio/TeraHDD2/git/mininghealth"
	global latexslides "/media/mauricio/TeraHDD2/git/mineriailegal/Latex/201611_SEEPAC/Stata"
	global latexpaper "/media/mauricio/TeraHDD2/git/mineriailegal/Latex/201605_Paper_royalties_illegal"
	}
	
	if "`corredor'"=="Mauricio" {
	 global illegalpath "C:/Users/Mauricio/Dropbox/MineriaIlegal/"
	global mipath "C:/Users/Mauricio/Dropbox/MiningAndBirth_Ilegal"
	global gitpath "C:/Users/Mauricio/Documents/git/mininghealth"
	global latexslides "C:/Users/Mauricio/Documents/git/mineriailegal/Latex/201611_SEEPAC/Stata"
	global latexpaper "C:/Users/Mauricio/Documents/git/mineriailegal/Latex/201605_Paper_royalties_illegal"
	}
	
	
		if "`corredor'"=="santiago.saavedrap" {
	global mipath "C:/Users/santiago.saavedrap/Dropbox/MiningAndBirth_Ilegal/"
	  global illegalpath "C:/Users/santiago.saavedrap/Dropbox/MineriaIlegal/"
	global gitpath "C:/Users/santiago.saavedrap/Documents/mininghealth/"
    global latexslides "C:/Users/santiago.saavedrap/Documents/mineriailegal/Latex/201611_SEEPAC/Stata"
	global latexpaper "C:/Users/santiago.saavedrap/Documents/mineriailegal/Latex/201605_Paper_royalties_illegal"
	}
	
	
	global base_in 	"$mipath/RawData"
  global base_out   "$mipath/CreatedData"
 
 global dir_do "$gitpath/Stata_HealthIllegal" 
  global results    "$mipath/Results"
  global exceltables  "$mipath/Results/ExcelTables"
  global graphs     "$mipath/Results/Graphs"
  global latexcodes     "$mipath/Results/LatexCodes"
	
	global ntipos=4

	

do "$dir_do/02_MainDataPrep"
do "$dir_do/02a_import_codane_upstream"
do "$dir_do/03_Regs"

set more off
use "$base_out/DataCompleta_Stata.dta", clear
qui gen coddepto=(codmpio-mod(codmpio,1000))/1000
qui gen APGAR_ALTO=100-APGAR_BAJO
gen ExpAreaMuni20KM_Tipo23=ExpAreaMuni20KM_Tipo2+ExpAreaMuni20KM_Tipo3
gen ExpAreaMuni20KM_Tipo13=ExpAreaMuni20KM_Tipo1+ExpAreaMuni20KM_Tipo3
gen ExpAreaMuni20KM_Tipo123=ExpAreaMuni20KM_Tipo1+ExpAreaMuni20KM_Tipo23
gen UpstreamAreaMuni20KM_Tipo23=UpstreamAreaMuni20KM_Tipo2+UpstreamAreaMuni20KM_Tipo3
gen UpstreamAreaMuni20KM_Tipo13=UpstreamAreaMuni20KM_Tipo1+UpstreamAreaMuni20KM_Tipo3
gen UpstreamAreaMuni20KM_Tipo123=UpstreamAreaMuni20KM_Tipo1+UpstreamAreaMuni20KM_Tipo23


gen D_ExpAreaMuni20KM_Tipo13=(ExpAreaMuni20KM_Tipo13>0) if ExpAreaMuni20KM_Tipo13!=.
gen D_ExpAreaMuni20KM_Tipo23=(ExpAreaMuni20KM_Tipo23>0) if ExpAreaMuni20KM_Tipo23!=.
gen D_ExpAreaMuni20KM_Tipo123=(ExpAreaMuni20KM_Tipo123>0) if ExpAreaMuni20KM_Tipo123!=.
gen D_UpstreamAreaMuni20KM_Tipo13=(UpstreamAreaMuni20KM_Tipo13>0) if UpstreamAreaMuni20KM_Tipo13!=.
gen D_UpstreamAreaMuni20KM_Tipo23=(UpstreamAreaMuni20KM_Tipo23>0) if UpstreamAreaMuni20KM_Tipo23!=.
gen D_UpstreamAreaMuni20KM_Tipo123=(UpstreamAreaMuni20KM_Tipo123>0) if UpstreamAreaMuni20KM_Tipo123!=.



/*
local Ncentile=4
qui forval i=1/`Ncentile' {
local plcut=100*(`i'-1)/`Ncentile'
local pucut=100*(`i')/`Ncentile'
centile UpstreamAreaMuni20KM_Tipo23 if UpstreamAreaMuni20KM_Tipo23>0, centile(`plcut')
local lcut=r(c_1)
centile UpstreamAreaMuni20KM_Tipo23 if UpstreamAreaMuni20KM_Tipo23>0, centile(`pucut')
local ucut=r(c_1)
gen D_UpsBin`i'=(UpstreamAreaMuni20KM_Tipo23>`lcut')*(UpstreamAreaMuni20KM_Tipo23<=`ucut')
label var D_UpsBin`i' "Downstream from open pit mine q`i'"
gen D_UpsLegalBin`i'=(UpstreamAreaMuni20KM_Tipo3>`lcut')*(UpstreamAreaMuni20KM_Tipo3<=`ucut')
label var D_UpsLegalBin`i' "Downstream from legal open pit mine q`i'"
gen D_UpsIllegalBin`i'=(UpstreamAreaMuni20KM_Tipo2>`lcut')*(UpstreamAreaMuni20KM_Tipo2<=`ucut')
label var D_UpsIllegalBin`i' "Downstream from illegal open pit mine q`i'"
}
*/

gen nearonlylegal=D_ExpAreaMuni20KM_Tipo3*(1-D_ExpAreaMuni20KM_Tipo2)
label var nearonlylegal "Near legal mine only"
gen nearonlyillegal=D_ExpAreaMuni20KM_Tipo2*(1-D_ExpAreaMuni20KM_Tipo3)
label var nearonlyillegal "Near illegal mine only"
gen nearboth=D_ExpAreaMuni20KM_Tipo2*D_ExpAreaMuni20KM_Tipo3
label var nearboth "Near both types of mines"

gen upsonlylegal=D_UpstreamAreaMuni20KM_Tipo3*(1-D_UpstreamAreaMuni20KM_Tipo2)
label var upsonlylegal "Downstream from legal mine only"
gen upsonlyillegal=D_UpstreamAreaMuni20KM_Tipo2*(1-D_UpstreamAreaMuni20KM_Tipo3)
label var upsonlyillegal "Downstream from illegal mine only"
gen upsboth=D_UpstreamAreaMuni20KM_Tipo2*D_UpstreamAreaMuni20KM_Tipo3
label var upsboth "Downstream from both types of mines"

** Tipo 1 Mining title without satelite detected mine (maybe underground)
** Tipo 2 Mine without title (illegal mine)
** Tipo 3 Mining title with mine
global control4 MadreSoltera EduMadrePostPrimaria EDAD_MADRE

label var D_ExpAreaMuni20KM_Tipo4 "Near mining title"
label var D_UpstreamAreaMuni20KM_Tipo4 "Downstream from mining title"
label var D_UpstreamAreaMuni20KM_Tipo3 "Downstream from legal mine"
label var D_ExpAreaMuni20KM_Tipo2 "Near illegal mine"
label var D_ExpAreaMuni20KM_Tipo3 "Near legal mine"
label var D_UpstreamAreaMuni20KM_Tipo2 "Downstream from illegal mine"
label var D_ExpAreaMuni20KM_Tipo123 "Near any mine"
label var D_UpstreamAreaMuni20KM_Tipo123 "Downstream from any mine"
label var D_ExpAreaMuni20KM_Tipo23 "Near open pit mine"
label var D_UpstreamAreaMuni20KM_Tipo23 "Downstream from open pit mine"
/*
qui eststo m1: reghdfe APGAR_ALTO D_ExpAreaMuni20KM_Tipo4 D_UpstreamAreaMuni20KM_Tipo4 $control4 , absorb(i.codmpio Semana_Naci i.coddepto#c.ano i.ano) vce(cluster codmpio)
qui estadd ysumm
estadd local Minas "Titles"

qui eststo m2: reghdfe APGAR_ALTO D_ExpAreaMuni20KM_Tipo123 D_UpstreamAreaMuni20KM_Tipo123 $control4 , absorb(i.codmpio Semana_Naci i.coddepto#c.ano i.ano) vce(cluster codmpio)
qui estadd ysumm
estadd local Minas "All"

qui eststo m3: reghdfe APGAR_ALTO D_ExpAreaMuni20KM_Tipo23 D_UpstreamAreaMuni20KM_Tipo23 $control4 if underground!=1, absorb(i.codmpio Semana_Naci i.coddepto#c.ano i.ano) vce(cluster codmpio)
qui estadd ysumm
estadd local Minas "Open pit"

qui eststo m4: reghdfe APGAR_ALTO D_ExpAreaMuni20KM_Tipo23 ups* $control4 if underground!=1, absorb(i.codmpio Semana_Naci i.coddepto#c.ano i.ano) vce(cluster codmpio)
qui estadd ysumm
test upsonlylegal=upsonlyillegal
local pvaluem4=r(p)
local pvaluem4=round(1000*`pvaluem4')/1000
test (_b[upsonlylegal] - _b[upsonlyillegal]=0)
estadd scalar p_ig=r(p)
file open newfile using "$latexslides/pvaluem4.tex", write replace
file write newfile "`pvaluem4'"
file close newfile
file open newfile using "$latexpaper/pvaluem4.tex", write replace
file write newfile "`pvaluem4'"
file close newfile
estadd local Minas "Open pit"


qui eststo m5: reghdfe APGAR_ALTO near* ups* $control4 if underground!=1, absorb(i.codmpio Semana_Naci i.coddepto#c.ano i.ano) vce(cluster codmpio)
qui estadd ysumm
test upsonlylegal=upsonlyillegal
local pvaluem5=r(p)
local pvaluem5=round(1000*`pvaluem5')/1000
test (_b[upsonlylegal] - _b[upsonlyillegal]=0)
estadd scalar p_ig=r(p)
file open newfile using "$latexslides/pvaluem5.tex", write replace
file write newfile "`pvaluem5'"
file close newfile
file open newfile using "$latexpaper/pvaluem5.tex", write replace
file write newfile "`pvaluem5'"
file close newfile
estadd local Minas "Open pit"

 estout m1 m2 m3 m4 m5 using "$latexslides/health_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
rename(D_ExpAreaMuni20KM_Tipo4 Near D_ExpAreaMuni20KM_Tipo23 Near D_ExpAreaMuni20KM_Tipo123 Near ///
D_UpstreamAreaMuni20KM_Tipo4 upstream D_UpstreamAreaMuni20KM_Tipo123 upstream D_UpstreamAreaMuni20KM_Tipo23 upstream) ///
varlabel(Near "Near Mine" upstream "Downstream from mine") ///
keep(Near  near* upstream ups*) prefoot(\midrule ) stats(Minas N ymean p_ig , fmt( %~#s a2  a2) labels ( "Mines" "N. of observations (babies)"  "Mean of Dep. Var."  "p-value (\$H_0\$:Legal=Illegal)" )) replace

 estout m1 m2 m3 m4 m5 using "$latexpaper/health_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
rename(D_ExpAreaMuni20KM_Tipo4 Near D_ExpAreaMuni20KM_Tipo23 Near D_ExpAreaMuni20KM_Tipo123 Near ///
D_UpstreamAreaMuni20KM_Tipo4 upstream D_UpstreamAreaMuni20KM_Tipo123 upstream D_UpstreamAreaMuni20KM_Tipo23 upstream) ///
varlabel(Near "Near Mine" upstream "Downstream from mine") ///
keep(Near  near* upstream ups*) prefoot(\midrule ) stats(Minas N N_clust ymean r2 p_ig , fmt( %~#s a2 a2 a2 a2) labels ( "Mines" "N. of observations (babies)" "Municipalities" "Mean of Dep. Var." "\$R^2\$" "p-value (\$H_0\$:Legal=Illegal)" )) replace

qui eststo clear
qui eststo m1: reghdfe APGAR_ALTO D_ExpAreaMuni20KM_Tipo23 D_UpsBin* $control4 if underground!=1 , absorb(codmpio coddepto#c.YrMonth i.YrMonth) vce(cluster codmpio)
qui estadd ysumm

qui eststo m2: reghdfe APGAR_ALTO D_ExpAreaMuni20KM_Tipo23 D_Ups*galBin* $control4 if underground!=1 , absorb(codmpio coddepto#c.YrMonth i.YrMonth) vce(cluster codmpio)
qui estadd ysumm

qui estout m1 m2 using "$latexslides/healthsize_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( D_* ) prefoot(\midrule ) stats( N N_clust ymean r2 , fmt( %9.2gc %9.2gc a2 a2 ) labels ( "N. of observations (babies)" "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui estout m1 m2 using "$latexpaper/healthsize_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( D_* ) prefoot(\midrule ) stats( N N_clust ymean r2 , fmt( %9.2gc %9.2gc a2 a2 ) labels ( "N. of observations (babies)" "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace
*/
*** IV strategy


merge m:1 codmpio ano using "$base_out\Temporary\MuniUpstream_All.dta"
drop _merge
merge m:1 codmpio ano using "$base_out\Temporary\MuniDownstream_All.dta"
drop _merge

replace ups_after_x_ind_leaf2=0 if ups_after_x_ind_leaf2==.
label var ups_after_x_ind_leaf2 "After X Weak Institutions Municipality Upstream"

label var down_after_x_ind_leaf2 "After X Weak Institutions Municipality Downstream"


qui eststo m1: reghdfe APGAR_ALTO D_UpstreamAreaMuni20KM_Tipo2 D_UpstreamAreaMuni20KM_Tipo3 $control4 if (undergroundm!=1 | ups_undergroundm!=1)  & codmpio!= ups_codane , absorb(i.codmpio Semana_Naci i.coddepto#c.ano i.ano) vce(cluster codmpio)
qui estadd ysumm
estadd local olsiv "OLS"

local dill_ols=round(100*(_b[D_UpstreamAreaMuni20KM_Tipo3] - _b[D_UpstreamAreaMuni20KM_Tipo2]))/100
file open newfile using "$latexslides/dill_ols.tex", write replace
file write newfile "`dill_ols'"
file close newfile
file open newfile using "$latexpaper/dill_ols.tex", write replace
file write newfile "`dill_ols'"
file close newfile

test (_b[D_UpstreamAreaMuni20KM_Tipo3] - _b[D_UpstreamAreaMuni20KM_Tipo2]=0)
local pvigols=r(p)/2
estadd scalar p_ig=`pvigols'
local pvigols=round(1000*`pvigols')/1000
file open newfile using "$latexslides/pvigols.tex", write replace
file write newfile "`pvigols'"
file close newfile
file open newfile using "$latexpaper/pvigols.tex", write replace
file write newfile "`pvigols'"
file close newfile

qui eststo m2: reghdfe APGAR_ALTO D_UpstreamAreaMuni20KM_Tipo3 $control4 (D_UpstreamAreaMuni20KM_Tipo2=ups_after_x_ind_leaf2)  if underground!=1 & codmpio!= ups_codane , absorb(i.codmpio Semana_Naci i.coddepto#c.ano i.ano) vce(cluster codmpio)
qui estadd ysumm
estadd local olsiv "IV Inst"

local dill_iv=round(100*(_b[D_UpstreamAreaMuni20KM_Tipo3] - _b[D_UpstreamAreaMuni20KM_Tipo2]))/100
file open newfile using "$latexslides/dill_iv.tex", write replace
file write newfile "`dill_iv'"
file close newfile
file open newfile using "$latexpaper/dill_iv.tex", write replace
file write newfile "`dill_iv'"
file close newfile

test (_b[D_UpstreamAreaMuni20KM_Tipo3] - _b[D_UpstreamAreaMuni20KM_Tipo2]=0)
local pvigiv=r(p)/2
estadd scalar p_ig=`pvigiv'
local pvigiv=round(1000*`pvigiv')/1000
file open newfile using "$latexslides/pvigiv.tex", write replace
file write newfile "`pvigiv'"
file close newfile
file open newfile using "$latexpaper/pvigiv.tex", write replace
file write newfile "`pvigiv'"
file close newfile


qui estout m1 m2 using "$latexslides/healthIVnsmbaby_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( D_UpstreamAreaMuni20KM_Tipo2 D_UpstreamAreaMuni20KM_Tipo3) prefoot(\midrule ) stats(olsiv N N_clust ymean r2 p_ig, fmt(%fmt %9.2gc %9.2gc a2 a2  a2) labels ("Method" "N. of observations" "Municipalities" "Mean of Dep. Var." "\$R^2\$" "p-value (\$H_0\$:Legal>Illegal)")) replace

qui estout m1 m2 using "$latexpaper/healthIVnsmbaby_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( D_UpstreamAreaMuni20KM_Tipo2 D_UpstreamAreaMuni20KM_Tipo3) prefoot(\midrule ) stats(olsiv N N_clust ymean r2 p_ig, fmt(%fmt %9.2gc %9.2gc a2 a2  a2) labels ("Method" "N. of observations" "Municipalities" "Mean of Dep. Var." "\$R^2\$" "p-value (\$H_0\$:Legal>Illegal)")) replace


qui eststo m1: reghdfe D_UpstreamAreaMuni20KM_Tipo2 ups_after_x_ind_leaf2 $control4 if underground!=1 & codmpio!= ups_codane  , absorb(i.codmpio Semana_Naci i.coddepto#c.ano i.ano) vce(cluster codmpio)
qui estadd ysumm

qui eststo m2: reghdfe D_UpstreamAreaMuni20KM_Tipo2 down_after_x_ind_leaf2 $control4 if underground!=1 & codmpio!= down_codane  , absorb(i.codmpio Semana_Naci i.coddepto#c.ano i.ano) vce(cluster codmpio)
qui estadd ysumm

qui estout m1 m2 using "$latexslides/healthIVfsbaby_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( *_after_x*) prefoot(\midrule ) stats( N N_clust ymean r2 F , fmt( %9.2gc %9.2gc a2 a2 a2) labels ( "N. of observations" "Municipalities" "Mean of Dep. Var." "\$R^2\$" "F-stat" )) replace

qui estout m1 m2 using "$latexpaper/healthIVfsbaby_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( *_after_x*) prefoot(\midrule ) stats( N N_clust ymean r2 F , fmt( %9.2gc %9.2gc a2 a2 a2) labels ( "N. of observations" "Municipalities" "Mean of Dep. Var." "\$R^2\$" "F-stat" )) replace

/*
******* At the muni level to run faster
use "$base_out/PanelMunicipal.dta", clear

foreach x of varlist ExpAreaMuni20KM_Tipo* {
replace `x'=`x'/10^6
}
qui gen APGAR_ALTO=100-APGAR_BAJO
/*
gen new=1
collapse new, by(codmpio)
save "C:\Users\santi\Dropbox\PYP_Birth\CreatedData\Temporary\codane_healthilegal.dta"
*/
qui gen coddepto=(codmpio-mod(codmpio,1000))/1000

* Tipo 1: parte de titulo sin mina satelital
* Tipo 2:  mina ilegal satelital
* Tipo 3: parte de titulo con mina satelital
*Tipo 4 titulo
* Tipo 5: parte de titulo sin mina satelitalUNODC
* Tipo 6:  mina ilegal satelitalUNODC
* Tipo 7: parte de titulo con mina satelitalUNODC

gen ExpAreaMuni20KM_Tipo23=ExpAreaMuni20KM_Tipo2+ExpAreaMuni20KM_Tipo3
gen ExpAreaMuni20KM_Tipo13=ExpAreaMuni20KM_Tipo1+ExpAreaMuni20KM_Tipo3
gen ExpAreaMuni20KM_Tipo123=ExpAreaMuni20KM_Tipo1+ExpAreaMuni20KM_Tipo23
gen ExpAreaMuni20KM_123_sq=ExpAreaMuni20KM_Tipo123*ExpAreaMuni20KM_Tipo123
*gen ExpAreaMuni20KM_Tipo57=ExpAreaMuni20KM_Tipo5+ExpAreaMuni20KM_Tipo7
*gen ExpAreaMuni20KM_Tipo567=ExpAreaMuni20KM_Tipo57+ExpAreaMuni20KM_Tipo6
gen UpstreamAreaMuni20KM_Tipo23=UpstreamAreaMuni20KM_Tipo2+UpstreamAreaMuni20KM_Tipo3
gen UpstreamAreaMuni20KM_Tipo13=UpstreamAreaMuni20KM_Tipo1+UpstreamAreaMuni20KM_Tipo3
gen UpstreamAreaMuni20KM_Tipo123=UpstreamAreaMuni20KM_Tipo1+UpstreamAreaMuni20KM_Tipo23
gen UpstreamAreaMuni20KM_123_sq=UpstreamAreaMuni20KM_Tipo123*UpstreamAreaMuni20KM_Tipo123
*gen UpstreamAreaMuni20KM_Tipo57=UpstreamAreaMuni20KM_Tipo5+UpstreamAreaMuni20KM_Tipo7
*gen UpstreamAreaMuni20KM_Tipo567=UpstreamAreaMuni20KM_Tipo57+UpstreamAreaMuni20KM_Tipo6

*foreach x of varlist ExpAreaMuni20KM_Tipo23-UpstreamAreaMuni20KM_Tipo567 {
foreach x of varlist ExpAreaMuni20KM_Tipo23-UpstreamAreaMuni20KM_Tipo123 {
gen D_`x'=(`x'>0) if `x'!=.
}

label var D_ExpAreaMuni20KM_Tipo4 "Near mining title"
label var D_UpstreamAreaMuni20KM_Tipo4 "Downstream from mining title"
label var APGAR_ALTO "Dummy High APGAR"
label var  ExpAreaMuni20KM_Tipo123 "Near mine measure"
label var  ExpAreaMuni20KM_123_sq "Near mine squared"
label var  ExpAreaMuni20KM_Tipo2 "Near illegal mine measure"
label var ExpAreaMuni20KM_Tipo3 "Near legal mine measure"
label var  D_ExpAreaMuni20KM_Tipo123 "Near mine "
label var  D_ExpAreaMuni20KM_Tipo2 "Near illegal mine "
label var D_ExpAreaMuni20KM_Tipo3 "Near legal mine "
label var  UpstreamAreaMuni20KM_Tipo123 "Downstream from mine measure"
label var  UpstreamAreaMuni20KM_123_sq "Downstream from mine squared"
label var  UpstreamAreaMuni20KM_Tipo2 "Downstream from illegal mine measure"
label var UpstreamAreaMuni20KM_Tipo3 "Downstream from legal mine measure"
label var  D_UpstreamAreaMuni20KM_Tipo123 "Downstream from mine "
label var  D_UpstreamAreaMuni20KM_Tipo2 "Downstream from illegal mine "
label var D_UpstreamAreaMuni20KM_Tipo3 "Downstream from legal mine "

gen underground=0
replace underground=1 if D_UpstreamAreaMuni20KM_Tipo1==1 & D_UpstreamAreaMuni20KM_Tipo3==0 &  D_UpstreamAreaMuni20KM_Tipo2==0

merge m:1 codmpio ano using "$base_out\Temporary\MuniUpstream_All.dta"

/*
merge m:1 codmpio ano using "$base_out\Temporary\MuniDownstream_All.dta"
reghdfe D_UpstreamAreaMuni20KM_Tipo2 down_after_x_ind_leaf2 [aw= NBirths] if underground!=1 & codmpio!= down_codane  , absorb(codmpio coddepto#c.YrMonth i.YrMonth) vce(cluster codmpio)
reghdfe APGAR_ALTO (D_UpstreamAreaMuni20KM_Tipo2=down_after_x_ind_leaf2) [aw= NBirths] if underground!=1 & codmpio!= down_codane , absorb(codmpio coddepto#c.YrMonth i.YrMonth) vce(cluster codmpio)
*/

replace ups_after_x_ind_leaf2=0 if ups_after_x_ind_leaf2==.
label var ups_after_x_ind_leaf2 "After X Weak Institutions Municipality Upstream"
replace ups_after_x_oil=0 if after_x_oil==.
label var ups_after_x_oil "After X Oil-Gas Municipality Upstream"


qui eststo m1: reghdfe APGAR_ALTO D_UpstreamAreaMuni20KM_Tipo2  [aw= NBirths] if underground!=1 & codmpio!= ups_codane , absorb(codmpio coddepto#c.YrMonth i.YrMonth) vce(cluster codmpio)
qui estadd ysumm
estadd local olsiv "OLS"

qui eststo m2: reghdfe APGAR_ALTO (D_UpstreamAreaMuni20KM_Tipo2=ups_after_x_ind_leaf2) [aw= NBirths] if underground!=1 & codmpio!= ups_codane , absorb(codmpio coddepto#c.YrMonth i.YrMonth) vce(cluster codmpio)
qui estadd ysumm
estadd local olsiv "IV Inst"


qui estout m1 m2  using "$latexslides/healthIVnsm_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( D_UpstreamAreaMuni20KM_Tipo2 ) prefoot(\midrule ) stats(olsiv N N_clust ymean r2 , fmt(%fmt a2 a2 a2 a2 ) labels ("Method" "N. of observations" "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui eststo m1: reghdfe D_UpstreamAreaMuni20KM_Tipo2 ups_after_x_ind_leaf2 [aw= NBirths] if underground!=1 & codmpio!= ups_codane  , absorb(codmpio coddepto#c.YrMonth i.YrMonth) vce(cluster codmpio)
qui estadd ysumm


qui estout m1  using "$latexslides/healthIVfs_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( after_x*) prefoot(\midrule ) stats( N N_clust ymean r2 F , fmt( a2 a2 a2 a2 a2) labels ( "N. of observations" "Municipalities" "Mean of Dep. Var." "\$R^2\$" "F-stat" )) replace


*Not necessary
*reghdfe APGAR_ALTO (D_UpstreamAreaMuni20KM_Tipo2=after_x_oil i.codmpio i.coddepto#c.YrMonth i.YrMonth) [aw= NBirths] if underground!=1  , absorb(codmpio coddepto#c.YrMonth i.YrMonth) vce(cluster codmpio)
qui eststo clear
qui estpost tabstat APGAR_ALTO D_ExpAreaMuni20KM_Tipo123 D_ExpAreaMuni20KM_Tipo2 D_ExpAreaMuni20KM_Tipo3 ExpAreaMuni20KM_Tipo123 ExpAreaMuni20KM_Tipo2 ExpAreaMuni20KM_Tipo3  [aw= NBirths] , statistics(mean p50 sd min max count) columns(statistics)
qui esttab using "$latexslides/sumstats_healthExp.tex", cells("mean(fmt(a2) label(Mean)) p50(fmt(a2) label(Median)) sd(fmt(a2) label(Std. Dev.)) min(fmt(a2) label(Min)) max(fmt(a2) label(Max)) count(fmt(a2) label(N))") nomtitle nonumber label  replace noobs  booktabs

qui eststo clear
qui estpost tabstat APGAR_ALTO D_UpstreamAreaMuni20KM_Tipo123 D_UpstreamAreaMuni20KM_Tipo2 D_UpstreamAreaMuni20KM_Tipo3 UpstreamAreaMuni20KM_Tipo123 UpstreamAreaMuni20KM_Tipo2 UpstreamAreaMuni20KM_Tipo3  [aw= NBirths] , statistics(mean p50 sd min max count) columns(statistics)
qui esttab using "$latexslides/sumstats_healthUps.tex", cells("mean(fmt(a2) label(Mean)) p50(fmt(a2) label(Median)) sd(fmt(a2) label(Std. Dev.)) min(fmt(a2) label(Min)) max(fmt(a2) label(Max)) count(fmt(a2) label(N))") nomtitle nonumber label  replace noobs  booktabs

sum UpstreamAreaMuni20KM_Tipo3, d
local cut=r(p99)
qui eststo m1: reghdfe APGAR_ALTO D_ExpAreaMuni20KM_Tipo4 D_UpstreamAreaMuni20KM_Tipo4 [aw= NBirths], absorb(codmpio coddepto#c.YrMonth i.YrMonth) vce(cluster codmpio)
qui estadd ysumm

qui eststo m2: reghdfe APGAR_ALTO D_ExpAreaMuni20KM_Tipo123 D_UpstreamAreaMuni20KM_Tipo123 [aw= NBirths] , absorb(codmpio coddepto#c.YrMonth i.YrMonth) vce(cluster codmpio)
qui estadd ysumm

qui eststo m3: reghdfe APGAR_ALTO D_ExpAreaMuni20KM_Tipo23 D_UpstreamAreaMuni20KM_Tipo23 [aw= NBirths] if underground!=1  , absorb(codmpio coddepto#c.YrMonth i.YrMonth) vce(cluster codmpio)
qui estadd ysumm

qui eststo m4: reghdfe APGAR_ALTO D_ExpAreaMuni20KM_Tipo23 D_UpstreamAreaMuni20KM_Tipo2 D_UpstreamAreaMuni20KM_Tipo3 [aw= NBirths] if underground!=1  , absorb(codmpio coddepto#c.YrMonth i.YrMonth) vce(cluster codmpio)
qui estadd ysumm

qui estout m1 m2 m3 m4 using "$latexslides/health_reg2.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( *AreaMuni* ) prefoot(\midrule ) stats( N N_clust ymean r2 , fmt( a2 a2 a2 a2 ) labels ( "N. of observations (babies)" "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace


**Old paper specification
reghdfe APGAR_ALTO D_ExpAreaMuni20KM_Tipo4 D_UpstreamAreaMuni20KM_Tipo4 [aw= NBirths], absorb(codmpio coddepto#c.YrMonth i.YrMonth) vce(cluster codmpio)

**Including illegal
reghdfe APGAR_ALTO D_ExpAreaMuni20KM_Tipo123 D_UpstreamAreaMuni20KM_Tipo123 [aw= NBirths], absorb(codmpio coddepto#c.YrMonth i.YrMonth) vce(cluster codmpio)

**Excluding underground mines and titulos without satellite mine results dissapear
reghdfe APGAR_ALTO D_ExpAreaMuni20KM_Tipo123 D_UpstreamAreaMuni20KM_Tipo23 [aw= NBirths], absorb(codmpio coddepto#c.YrMonth i.YrMonth) vce(cluster codmpio)




local Ncentile=4
qui forval i=1/`Ncentile' {
local plcut=100*(`i'-1)/`Ncentile'
local pucut=100*(`i')/`Ncentile'
centile UpstreamAreaMuni20KM_Tipo23 if UpstreamAreaMuni20KM_Tipo23>0, centile(`plcut')
local lcut=r(c_1)
centile UpstreamAreaMuni20KM_Tipo23 if UpstreamAreaMuni20KM_Tipo23>0, centile(`pucut')
local ucut=r(c_1)
gen D_UpsBin`i'=(UpstreamAreaMuni20KM_Tipo23>`lcut')*(UpstreamAreaMuni20KM_Tipo23<=`ucut')
gen D_UpsLegalBin`i'=(UpstreamAreaMuni20KM_Tipo3>`lcut')*(UpstreamAreaMuni20KM_Tipo3<=`ucut')
gen D_UpsIllegalBin`i'=(UpstreamAreaMuni20KM_Tipo2>`lcut')*(UpstreamAreaMuni20KM_Tipo2<=`ucut')
}

reghdfe APGAR_ALTO D_ExpAreaMuni20KM_Tipo23 D_UpsBin*[aw= NBirths] if underground!=1 , absorb(codmpio coddepto#c.YrMonth i.YrMonth) vce(cluster codmpio)

reghdfe APGAR_ALTO D_ExpAreaMuni20KM_Tipo23 D_Ups*galBin*[aw= NBirths] if underground!=1 , absorb(codmpio coddepto#c.YrMonth i.YrMonth) vce(cluster codmpio)

reghdfe APGAR_BAJO D_ExpAreaMuni20KM_Tipo1 D_UpstreamAreaMuni20KM_Tipo1 [aw= NBirths], absorb(codmpio coddepto#c.YrMonth i.YrMonth) vce(cluster codmpio)
reghdfe APGAR_BAJO D_ExpAreaMuni20KM_Tipo2 D_UpstreamAreaMuni20KM_Tipo2 [aw= NBirths], absorb(codmpio coddepto#c.YrMonth i.YrMonth) vce(cluster codmpio)
reghdfe APGAR_BAJO D_ExpAreaMuni20KM_Tipo3 D_UpstreamAreaMuni20KM_Tipo3 [aw= NBirths], absorb(codmpio coddepto#c.YrMonth i.YrMonth) vce(cluster codmpio)

tab D_ExpAreaMuni20KM_Tipo2 D_ExpAreaMuni20KM_Tipo3
reghdfe APGAR_BAJO D_ExpAreaMuni20KM_Tipo2 D_UpstreamAreaMuni20KM_Tipo2 D_ExpAreaMuni20KM_Tipo3 D_UpstreamAreaMuni20KM_Tipo3 [aw= NBirths], absorb(codmpio coddepto#c.YrMonth i.YrMonth) vce(cluster codmpio)


foreach x of varlist ExpAreaMuni20KM_Tipo* UpstreamAreaMuni20KM_Tipo* {
gen `x'_sq=`x'*`x'
}
tab D_ExpAreaMuni20KM_Tipo2 D_ExpAreaMuni20KM_Tipo3
tab D_UpstreamAreaMuni20KM_Tipo2 D_UpstreamAreaMuni20KM_Tipo3
tab D_UpstreamAreaMuni10KM_Tipo2 D_UpstreamAreaMuni10KM_Tipo3
tab D_UpstreamAreaMuni5KM_Tipo2 D_UpstreamAreaMuni5KM_Tipo3
reghdfe APGAR_BAJO ExpAreaMuni20KM_Tipo2 UpstreamAreaMuni20KM_Tipo2 ExpAreaMuni20KM_Tipo3 UpstreamAreaMuni20KM_Tipo3 [aw= NBirths], absorb(codmpio coddepto#c.YrMonth i.YrMonth) vce(cluster codmpio)
reghdfe APGAR_BAJO  ExpAreaMuni20KM_Tipo3 UpstreamAreaMuni20KM_Tipo3 [aw= NBirths], absorb(codmpio coddepto#c.YrMonth i.YrMonth) vce(cluster codmpio)
reghdfe APGAR_BAJO ExpAreaMuni20KM_Tipo2 UpstreamAreaMuni20KM_Tipo2  [aw= NBirths], absorb(codmpio coddepto#c.YrMonth i.YrMonth) vce(cluster codmpio)

**Given that D_upstream legal = illegal try to classify
plot UpstreamAreaMuni20KM_Tipo2 UpstreamAreaMuni20KM_Tipo3
gen diff= UpstreamAreaMuni20KM_Tipo2-UpstreamAreaMuni20KM_Tipo3
sum diff, d
gen D_UpsMostlyTipo2=0
replace D_UpsMostlyTipo2=1 if diff!=. & diff>0
gen D_UpsMostlyTipo3=0
replace D_UpsMostlyTipo3=1 if diff!=. & diff<0


gen diffExp= ExpAreaMuni20KM_Tipo2-ExpAreaMuni20KM_Tipo3
sum diffExp, d
gen D_ExpMostlyTipo2=0
replace D_ExpMostlyTipo2=1 if diffExp!=. & diffExp>0
gen D_ExpMostlyTipo3=0
replace D_ExpMostlyTipo3=1 if diffExp!=. & diffExp<0
reghdfe APGAR_BAJO D_ExpMostlyTipo2 D_ExpMostlyTipo3 D_UpsMostlyTipo2 D_UpsMostlyTipo3 [aw= NBirths], absorb(codmpio coddepto#c.YrMonth i.YrMonth) vce(cluster codmpio)
reghdfe APGAR_BAJO D_ExpAreaMuni20KM_Tipo2 D_ExpAreaMuni20KM_Tipo3 D_UpstreamAreaMuni20KM_Tipo2 [aw= NBirths], absorb(codmpio coddepto#c.YrMonth i.YrMonth) vce(cluster codmpio)

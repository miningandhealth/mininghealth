use "C:\Users\santi\Dropbox\PYP_Birth\CreatedData\PanelMunicipal.dta", clear
keep D_UpstreamAreaMuni20KM D_ExpAreaMuni20KM ano MES codmpio NBirths
rename NBirths NBirths_Old
save "C:\Users\santi\Dropbox\MiningAndBirth_Ilegal\CreatedData\OldPanelMunicipal.dta"

use "C:\Users\santi\Dropbox\MiningAndBirth_Ilegal\CreatedData\PanelMunicipal.dta", clear
merge 1:1 ano MES codmpio using "C:\Users\santi\Dropbox\MiningAndBirth_Ilegal\CreatedData\OldPanelMunicipal.dta"
keep if _merge==3

gen ExpAreaMuni20KM_Tipo13=ExpAreaMuni20KM_Tipo1+ExpAreaMuni20KM_Tipo3
gen UpstreamAreaMuni20KM_Tipo13=UpstreamAreaMuni20KM_Tipo1+UpstreamAreaMuni20KM_Tipo3
gen D_ExpAreaMuni20KM_Tipo13=(ExpAreaMuni20KM_Tipo13>0) if ExpAreaMuni20KM_Tipo13!=.
gen D_UpstreamAreaMuni20KM_Tipo13=(UpstreamAreaMuni20KM_Tipo13>0) if UpstreamAreaMuni20KM_Tipo13!=.
qui gen coddepto=(codmpio-mod(codmpio,1000))/1000
keep ano MES codmpio NBirths NBirths_Old D_ExpAreaMuni20KM D_UpstreamAreaMuni20KM D_ExpAreaMuni20KM_Tipo13 D_UpstreamAreaMuni20KM_Tipo13

tab codmpio if NBirths!= NBirths_Old
tab D_ExpAreaMuni20KM if D_ExpAreaMuni20KM!=D_ExpAreaMuni20KM_Tipo13
tab D_ExpAreaMuni20KM_Tipo13 if D_ExpAreaMuni20KM!=D_ExpAreaMuni20KM_Tipo13

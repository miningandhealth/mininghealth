
qui use "$base_out/DataCompleta.dta",clear
*Restrict analysis to years we have production AND area data
*drop if ANO>2012
drop if ANO<2004
*Drop capitals
*drop if mod(CODIGO_DANE_M,1000)==1 


*Generate stunt info
qui gen Z_TALLA_NAC=(TALLA_NAC-49.9)/1.8931 if MASC==1
qui replace Z_TALLA_NAC=(TALLA_NAC-49.1)/1.8627 if MASC==0
gen STUNT=(Z_TALLA_NAC<-2) if !missing(Z_TALLA_NAC)
label var Z_TALLA_NAC "(Z-score) Height"
gen Premature=(1-GEST_COMPLETA) if !missing(GEST_COMPLETA)


*Change variables so that coefficients are easier to read
foreach var of varlist APGAR_BAJO LBW STUNT MadreSoltera EduMadrePostPrimaria PartoHospital{
	replace `var'=100*`var'
}
label var STUNT "Stunted (\%)"
label var LBW "Low birth weight (\%)"
label var PartoHospital "In-hospital birth (\%)"
label var APGAR_BAJO "Low APGAR"
label var PESO_NAC "Weight (gr)"
label var TALLA_NAC "Height (cm)"
label var EDAD_MADRE "Mother's age"
label var MadreSoltera "Single mother (\%)"
 label var ConsultasPreMayor4 "Prenatal checkups $>$ 4"
label var Premature "Premature"

*Drop people who have important missing data
drop if APGAR_BAJO==.
*This is dropping 2013 and 2014
*drop if PESO_NAC==.
*replace TALLA_NAC=. if TALLA_NAC<40
*replace TALLA_NAC=. if TALLA_NAC>55
*drop if TALLA_NAC==. 
drop if EDAD_MADRE==.
drop if MadreSoltera==.
drop if EduMadrePostPrimaria==.

 
qui compress 
drop DPTO_NACI MUNIC_NACI SEXO DPTO_R MUNIC_R MES
rename MES2 MES
rename ANO ano
rename CODIGO_DANE_M codmpio 

** Where is Produccion*
keep Premature EDAD_MADRE SEMANAS MES FECHA_NAC PESO_NAC TALLA_NAC NUM_CONSUL APGAR_CONTINUO GEST_COMPLETA APGAR_BAJO LBW LBW VLBW MASC PartoEspontaneo Csection MultipleBirth PartoHospital MadreMenor14 Madre14_17 ConsultasPreMayor4 RegimenContributivo RegimenSubsidiado MadreSoltera EduMadrePostPrimaria EduMadrePostSecundaria AreaTipo* ExpArea* UpstreamArea* Z_TALLA_NAC STUNT ano Quarter codmpio


*Mark who is near a gold mine and drop who is not!
merge m:1 codmpio using "$base_out/Munis_mineros.dta"
drop if _merge==2
gen NearGoldMine=1 if _merge==3
replace NearGoldMine=0 if _merge==1
drop _merge
qui compress
drop if NearGoldMine==0
drop NearGoldMine


merge m:1 codmpio ano using "$base_in/PANELES_CEDE/CARACTERÍSTICAS GENERALES/PANEL CARAC. GENERALES.dta", keepus(pobl_tot gandina gcaribe gpacifica gorinoquia gamazonia pobl_rur pobl_urb pobl_tot indrural areaoficialkm2 altura discapital dismdo TMI disbogota codmdo gpc pobreza gini nbi minorias parques religioso estado otras pecsaludc pepsaludc pehosclinc peplantaec pebibliopc ipm_ti_p ipm_tdep_p ipm_assalud_p ipm_accsalud_p ipm_accagua_p ipm_templeof_p ipm_tdep_p ipm_excretas_p ipm_pisos_p ipm_paredes_p ipm_hacinam_p)
drop if _merge==2
drop _merge

*rename ProduccionAprox_M Produccion
*rename ProduccionAprox_Acc_M Produccion_Acc

qui foreach i of numlist 1/$ntipos {
rename AreaTipo`i' AreaMinadaTipo`i'_sqkm
gen AreaMinadaTipo`i'Prop=100*AreaMinadaTipo`i'_sqkm/areaoficialkm2
}

gen region=gandina+2*gcaribe+3*gpacifica+4*gorinoquia+5*gamazonia
bys codmpio: egen mregion=mean(region)
replace region=mregion if region==.
drop mregion
keep if region<=3

*Lets top code... FORGET ABOUT THAT 15% BULSHIT
*Produccion*
qui foreach vrn of varlist AreaMinadaTipo* ExpArea* UpstreamArea*  {
	qui sum `vrn' if `vrn'>0,d
	di r(p99)
	replace `vrn'=r(p99) if `vrn'>r(p99) & !missing(`vrn')
	capture drop D_`vrn'
	gen D_`vrn'=(`vrn'>0) if !missing(`vrn')
}



*For Week FE
gen Semana_Naci=week(FECHA_NAC)
*Now to calculate some interesting stuff
/*
gen ProduccionPerCapita=(Produccion/pobl_tot)/10^2
gen ProduccionPerArea=(Produccion/areaoficialkm2)/10^3
gen ProduccionAccPerArea=(Produccion_Acc/areaoficialkm2)/10^3
gen D_ProduccionPerArea=(ProduccionPerArea>0) if !missing(ProduccionPerArea)
gen D_ProduccionAccPerArea=(ProduccionAccPerArea>0) if !missing(ProduccionPerArea)
gen D_ProduccionPerCapita=(ProduccionPerCapita>0) if !missing(ProduccionPerCapita)
*/


*qui foreach i in 5 10 20 {
qui foreach i in 20 {
foreach t of numlist 1/$ntipos {
label var ExpAreaMuni`i'KM_Tipo`t' "Near mining `i' km Tipo`t'"
label var UpstreamAreaMuni`i'KM_Tipo`t' "Mining upstream `i' km Tipo`t'"
label var D_ExpAreaMuni`i'KM_Tipo`t' "Near mining `i' km `=char(36)'>0`=char(36)' Tipo`t'"
label var D_UpstreamAreaMuni`i'KM_Tipo`t' "Mining upstream `i' km `=char(36)'>0`=char(36)' Tipo`t'"
}
}

*Set up labels for latex
/*

label var ProduccionPerCapita "Production/Population"
label var ProduccionPerArea "Production/Municipality Area"
label var ProduccionAccPerArea "Accumulated Production"
label var D_ProduccionPerArea "Production/Municipality Area `=char(36)'>0`=char(36)'"
label var D_AreaMinadaProp "Mining Area/Municipality Area `=char(36)'>0`=char(36)'"
label var D_ProduccionPerArea "Production `=char(36)'>0`=char(36)'"
label var D_ProduccionAccPerArea "Production Accumulated `=char(36)'>0`=char(36)'"
*/

*Lets add the price of gold
qui rename ano ANO
qui merge m:1 ANO MES using "$base_out/PriceGold.dta",keepus(PriceRealMA)
qui rename ANO ano
qui drop if _merge==2
qui drop _merge  
qui compress

foreach var_indep of varlist AreaMinada*  ExpArea* UpstreamArea*{
	qui gen `var_indep'xp=`var_indep'*PriceRealMA
	qui gen D_`var_indep'xp=D_`var_indep'*PriceRealMA
}

**Why this?
replace PriceRealMA=PriceRealMA*2.36

*Lets create some time variables
qui gen YrMonth=ym(ano,MES)
qui gen YrQrt=yq(ano,Quarter)

*Add some assorted labels


label var EduMadrePostPrimaria "Mother has post-primary education (\%)"



** Adding HW vars
gen dia=day(FECHA_NAC)
gen wdate=mdy(MES,dia,ano)
sort wdate
qui merge m:1 wdate using "$base_out/Closest_hw"
drop if _merge==2
drop _merge
gen wgesthw=ceil((wdate-closest_hw)/7)

replace wgesthw=. if SEMANAS==.
qui label var wgesthw "Week during gestation when holy week happens"
gen ihw=1 if wgesthw<=SEMANAS & wgesthw!=.
qui replace ihw=0 if wgesthw>SEMANAS & wgesthw!=.
label var ihw "Holy Week during gestation"


*Create "ring" proxiumty variables

foreach t of numlist 1/$ntipos {

label var AreaMinadaTipo`t'Prop "Mining Area Tipo`t'/Municipality Area"
label var AreaMinadaTipo`t'Propxp "{[Mining Area Tipo`t'/Municipality Area]} x Price"
qui gen D_ExpAreaMuni510KM_Tipo`t'=(D_ExpAreaMuni10KM_Tipo`t'==1 & D_ExpAreaMuni5KM_Tipo`t'==0)
qui gen D_ExpAreaMuni1020KM_Tipo`t'=(D_ExpAreaMuni20KM_Tipo`t'==1 & D_ExpAreaMuni10KM_Tipo`t'==0)
qui gen D_UpstreamAreaMuni510KM_Tipo`t'=(D_UpstreamAreaMuni10KM_Tipo`t'==1 & D_UpstreamAreaMuni5KM_Tipo`t'==0)
qui gen D_UpstreamAreaMuni1020KM_Tipo`t'=(D_UpstreamAreaMuni20KM_Tipo`t'==1 & D_UpstreamAreaMuni10KM_Tipo`t'==0)

label var D_ExpAreaMuni510KM_Tipo`t' "Near mining 5-10 km Tipo`t' `=char(36)'>0`=char(36)'"
label var D_ExpAreaMuni1020KM_Tipo`t' "Near mining 10-20 km Tipo`t' `=char(36)'>0`=char(36)'"
label var D_UpstreamAreaMuni510KM_Tipo`t' "Mining upstream 5-10 km Tipo`t' `=char(36)'>0`=char(36)'"
label var D_UpstreamAreaMuni1020KM_Tipo`t' "Mining upstream 10-20 km Tipo`t' `=char(36)'>0`=char(36)'"
}



merge m:1 codmpio using "$base_out/ilegalMunis.dta"
set matsize 5000
drop if _merge==2
replace Ilegal=0 if _merge==1
drop _merge

gen underground=0
replace underground=1 if D_UpstreamAreaMuni20KM_Tipo1==1 & D_UpstreamAreaMuni20KM_Tipo3==0 &  D_UpstreamAreaMuni20KM_Tipo2==0
bys codmpio: egen undergroundm=min(underground)
drop underground

save "$base_out/DataCompleta_Stata.dta",replace

preserve
collapse (min) undergroundm, by(codmpio)
rename codmpio ups_codane
rename undergroundm ups_undergroundm
save "$base_out/Temporary/undergroundm_upscodane.dta",replace
restore


*Now lets create some important datasets from these one! 
collapse (count) NBirths= APGAR_BAJO (mean) PESO_NAC-SEMANAS  Quarter-EduMadrePostSecundaria AreaMinadaTipo1_sqkm-D_UpstreamAreaMuni1020KM_Tipo$ntipos, by(ano MES codmpio)
tsset codmpio YrMonth, monthly
qui compress
save "$base_out/PanelMunicipal.dta",replace


collapse (max) D_*, by(codmpio)
foreach vrn of varlist D_*  {
	qui gen `vrn'_E=1 if `vrn'>0 & `vrn'!=.
	qui replace `vrn'=0 if `vrn'==.
}
qui keep codmpio *_E
qui compress
qui save "$base_out/Temporary/Muni_Ever.dta", replace



qui use "$base_out/PanelMunicipal.dta",clear
merge m:1 codmpio using "$base_out/Temporary/Muni_Ever.dta"
drop if _merge==2
drop _merge
merge m:1 codmpio ano using "$base_in/PANELES_CEDE/CARACTERÍSTICAS GENERALES/PANEL CARAC. GENERALES.dta", keepus(gandina gcaribe gpacifica gorinoquia gamazonia areaoficialkm2 )
drop if _merge==2
drop _merge


foreach t of numlist 1/$ntipos {
*qui foreach i in 5 10 20 {
qui foreach i in  20 {

label var ExpAreaMuni`i'KM_Tipo`t' "Near mining `i' km Tipo`t'"
label var UpstreamAreaMuni`i'KM_Tipo`t' "Mining upstream `i' km Tipo`t'"
label var D_ExpAreaMuni`i'KM_Tipo`t' "Near mining `i' km `=char(36)'>0`=char(36)' Tipo`t'"
label var D_UpstreamAreaMuni`i'KM_Tipo`t' "Mining upstream `i' km `=char(36)'>0`=char(36)' Tipo`t'"
}

label var AreaMinadaTipo`t'Prop "Mining Area Tipo`t'/Municipality Area"
label var D_AreaMinadaTipo`t'Prop "Mining Area Tipo`t'/Municipality Area `=char(36)'>0`=char(36)'"
label var AreaMinadaTipo`t'Propxp "{[Mining Area Tipo`t'/Municipality Area]} x Price"
label var D_ExpAreaMuni510KM_Tipo`t' "Proximity exposure area Tipo`t' municipality 5-10 km `=char(36)'>0`=char(36)'"
label var D_ExpAreaMuni1020KM_Tipo`t' "Proximity exposure area Tipo`t' municipality 10-20 km `=char(36)'>0`=char(36)'"
label var D_UpstreamAreaMuni510KM_Tipo`t' "River exposure area Tipo`t' municipality 5-10 km `=char(36)'>0`=char(36)'"
label var D_UpstreamAreaMuni1020KM_Tipo`t' "River exposure area Tipo`t' municipality 10-20 km `=char(36)'>0`=char(36)'"

}

*Set up labels for latex
/*
label var ProduccionPerCapita "Production/Population"
label var ProduccionPerArea "Production/Municipality Area"
label var ProduccionAccPerArea "Accumulated Production"
label var D_ProduccionPerArea "Production/Municipality Area `=char(36)'>0`=char(36)'"
label var D_ProduccionPerArea "Production `=char(36)'>0`=char(36)'"
label var D_ProduccionAccPerArea "Production Accumulated `=char(36)'>0`=char(36)'"
*/
label var EduMadrePostPrimaria "Mother has post-primary education (\%)"

label var STUNT "Stunted (\%)"
label var LBW "Low birth weight (\%)"
label var PartoHospital "In-hospital birth (\%)"
label var APGAR_BAJO "Low APGAR"
label var PESO_NAC "Weight (gr)"
label var TALLA_NAC "Height (cm)"
label var EDAD_MADRE "Mother's age"
label var MadreSoltera "Single mother (\%)"
label var Z_TALLA_NAC "(Z-score) Height"
label var ConsultasPreMayor4 "Prenatal checkups $>$ 4"
label var Premature "Premature"

qui gen fertilidad=NBirths/pobl_tot
qui compress

save "$base_out/PanelMunicipal.dta",replace


use "$base_out/DefFetalesMensuales.dta", clear
rename CodigoDane codmpio
rename ANO ano
merge 1:1 ano MES codmpio using "$base_out/PanelMunicipal.dta"
gen PNM=1000*DefFetales/(NBirths+DefFetales)
sum PNM, d
replace PNM=r(p99) if PNM>r(p99) & !missing(PNM)
label var PNM "perinatal mortality"
drop if _merge==1
drop _merge

save "$base_out/PanelMunicipal.dta",replace

set more off
use "$base_out/DataCompleta_Stata.dta", clear
qui gen coddepto=(codmpio-mod(codmpio,1000))/1000
global control4 MadreSoltera EduMadrePostPrimaria EDAD_MADRE
drop if ano<=2003 /*for comparison */
drop if mod(codmpio,1000)==1  /*for comparison */
*For comparison

gen ExpAreaMuni20KM_Tipo23=ExpAreaMuni20KM_Tipo2+ExpAreaMuni20KM_Tipo3
gen ExpAreaMuni20KM_Tipo13=ExpAreaMuni20KM_Tipo1+ExpAreaMuni20KM_Tipo3
gen ExpAreaMuni20KM_Tipo123=ExpAreaMuni20KM_Tipo1+ExpAreaMuni20KM_Tipo23
gen UpstreamAreaMuni20KM_Tipo23=UpstreamAreaMuni20KM_Tipo2+UpstreamAreaMuni20KM_Tipo3
gen UpstreamAreaMuni20KM_Tipo13=UpstreamAreaMuni20KM_Tipo1+UpstreamAreaMuni20KM_Tipo3
gen UpstreamAreaMuni20KM_Tipo123=UpstreamAreaMuni20KM_Tipo1+UpstreamAreaMuni20KM_Tipo23


gen D_ExpAreaMuni20KM_Tipo13=(ExpAreaMuni20KM_Tipo13>0) if ExpAreaMuni20KM_Tipo13!=.
gen D_ExpAreaMuni20KM_Tipo23=(ExpAreaMuni20KM_Tipo23>0) if ExpAreaMuni20KM_Tipo23!=.
gen D_ExpAreaMuni20KM_Tipo123=(ExpAreaMuni20KM_Tipo123>0) if ExpAreaMuni20KM_Tipo123!=.
gen D_UpstreamAreaMuni20KM_Tipo13=(UpstreamAreaMuni20KM_Tipo13>0) if UpstreamAreaMuni20KM_Tipo13!=.
gen D_UpstreamAreaMuni20KM_Tipo23=(UpstreamAreaMuni20KM_Tipo23>0) if UpstreamAreaMuni20KM_Tipo23!=.
gen D_UpstreamAreaMuni20KM_Tipo123=(UpstreamAreaMuni20KM_Tipo123>0) if UpstreamAreaMuni20KM_Tipo123!=.

** Tipo 1 Mining title without satelite detected mine (maybe underground)
** Tipo 2 Mine without title (illegal mine)
** Tipo 3 Mining title with mine

label var D_ExpAreaMuni20KM_Tipo4 "Near legal mine"
label var D_UpstreamAreaMuni20KM_Tipo4 "Downstream from legal mine"
label var D_UpstreamAreaMuni20KM_Tipo3 "Downstream from legal mine"
label var D_ExpAreaMuni20KM_Tipo2 "Near illegal mine"
label var D_ExpAreaMuni20KM_Tipo2 "Near legal mine"
label var D_UpstreamAreaMuni20KM_Tipo2 "Downstream from illegal mine"
label var D_ExpAreaMuni20KM_Tipo123 "Near mine"
label var D_UpstreamAreaMuni20KM_Tipo123 "Downstream from mine"

qui eststo m1: reghdfe APGAR_BAJO D_ExpAreaMuni20KM_Tipo4 D_UpstreamAreaMuni20KM_Tipo4 $control4, absorb(i.codmpio  i.ano Semana_Naci i.coddepto#c.ano) vce(cluster codmpio)
qui estadd ysumm

qui eststo m2: reghdfe APGAR_BAJO D_ExpAreaMuni20KM_Tipo23 D_UpstreamAreaMuni20KM_Tipo23  $control4, absorb(i.codmpio  i.ano Semana_Naci i.coddepto#c.ano) vce(cluster codmpio)
qui estadd ysumm

qui eststo m3: reghdfe APGAR_BAJO D_ExpAreaMuni20KM_Tipo2 D_ExpAreaMuni20KM_Tipo3 D_UpstreamAreaMuni20KM_Tipo2 D_UpstreamAreaMuni20KM_Tipo3 $control4, absorb(i.codmpio i.ano Semana_Naci i.coddepto#c.ano) vce(cluster codmpio)
qui estadd ysumm

 estout m1 m2 m3  using "$latexslides/health_reg.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( D_* ) prefoot(\midrule ) stats( N N_clust ymean r2 , fmt( a2 a2 a2 a2 ) labels ( "N. of observations (babies)" "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

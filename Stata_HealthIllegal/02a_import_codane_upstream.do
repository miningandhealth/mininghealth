
qui forval t=1/3 {
qui forval i=2004/2012 {
import delimited "$base_out\MuniUpstream`i'_Tipo`t'_All.csv", clear
drop v1
rename zone codmpio
rename value ups_codane
destring ups_codane, force replace
gen ano=`i'
save "$base_out\Temporary\MuniUpstream`i'_Tipo`t'_All.dta", replace
}

****** The 5 lines below are to be able to merge with after_x_loser data
replace ups_codane=.
qui forval i=2013/2014 {
replace ano=`i'
save "$base_out\Temporary\MuniUpstream`i'_Tipo`t'_All.dta", replace
}

qui forval i=2004/2013 {
append using "$base_out\Temporary\MuniUpstream`i'_Tipo`t'_All.dta"
}


bys codmpio: egen mode_ups_codane_Tipo`t'=mode(ups_codane), maxmode
drop ups_codane
save "$base_out\Temporary\MuniUpstream_Tipo`t'_All.dta", replace
}

merge 1:1 codmpio ano using "$base_out\Temporary\MuniUpstream_Tipo2_All.dta"
drop _merge

merge 1:1 codmpio ano using "$base_out\Temporary\MuniUpstream_Tipo1_All.dta"
drop _merge

gen ups_codane= mode_ups_codane_Tipo2
replace ups_codane=mode_ups_codane_Tipo3 if ups_codane==.
replace ups_codane=mode_ups_codane_Tipo1 if ups_codane==.

keep codmpio ups_codane ano

merge m:1 ups_codane ano using "$illegalpath/CreatedData/illegal_panel_forhealth.dta"

drop _merge

merge m:1 ups_codane using "$base_out/Temporary/undergroundm_upscodane.dta"
drop _merge
drop if codmpio==.

save "$base_out\Temporary\MuniUpstream_All.dta", replace

use "$base_out\Temporary\MuniUpstream_All.dta", clear
collapse ups_codane, by(codmpio)
drop if codmpio==ups_codane
collapse (max) codmpio, by(ups_codane)
rename codmpio down_codane
rename ups_codane codmpio

merge 1:m down_codane using "$illegalpath/CreatedData/illegal_panel_forhealth_down.dta"
drop if _merge!=3

drop _merge



save "$base_out\Temporary\MuniDownstream_All.dta", replace

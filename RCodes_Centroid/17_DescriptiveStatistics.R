
load('CreatedData/DataCompleta.RData')


DataCompleta<-data.table(DataCompleta)
DataCompleta=DataCompleta[!is.na(DataCompleta$CODIGO_DANE_M),]
DataCompleta=DataCompleta[!is.na(DataCompleta$APGAR_BAJO),]
DataCompleta=DataCompleta[!is.na(DataCompleta$PESO_NAC),]
DataCompleta$TALLA_NAC[DataCompleta$TALLA_NAC<40]=NA
DataCompleta$TALLA_NAC[DataCompleta$TALLA_NAC>55]=NA
DataCompleta=DataCompleta[!is.na(DataCompleta$TALLA_NAC),]
DataCompleta=DataCompleta[!is.na(DataCompleta$EDAD_MADRE),]
DataCompleta=DataCompleta[!is.na(DataCompleta$MadreSoltera),]
DataCompleta=DataCompleta[!is.na(DataCompleta$EduMadrePostPrimaria),]

VarNames=c("PESO_NAC","TALLA_NAC","NUM_CONSUL","EDAD_MADRE","GEST_COMPLETA","APGAR_BAJO","LBW","VLBW","MASC","PartoEspontaneo","Csection",
"MultipleBirth","PartoHospital","MadreMenor14","Madre14_17","ConsultasPreMayor4","RegimenContributivo","RegimenSubsidiado","MadreSoltera",
"EduMadrePostPrimaria","EduMadrePostSecundaria")

MeanDay<-DataCompleta[,list(Nacimientos=length(SEXO),PESO_NAC=mean(PESO_NAC,na.rm=T),TALLA_NAC=mean(TALLA_NAC,na.rm=T),NUM_CONSUL=mean(NUM_CONSUL,na.rm=T),
EDAD_MADRE=mean(EDAD_MADRE,na.rm=T),
GEST_COMPLETA=mean(GEST_COMPLETA,na.rm=T),APGAR_BAJO=mean(APGAR_BAJO,na.rm=T),LBW=mean(LBW,na.rm=T),
VLBW=mean(VLBW,na.rm=T),MASC=mean(MASC,na.rm=T),PartoEspontaneo=mean(PartoEspontaneo,na.rm=T),
Csection=mean(Csection,na.rm=T),MultipleBirth=mean(MultipleBirth,na.rm=T),PartoHospital=mean(PartoHospital,na.rm=T),
MadreMenor14=sum(MadreMenor14,na.rm=T),Madre14_17=sum(Madre14_17,na.rm=T),ConsultasPreMayor4=mean(ConsultasPreMayor4,na.rm=T),
RegimenContributivo=mean(RegimenContributivo,na.rm=T),RegimenSubsidiado=mean(RegimenSubsidiado,na.rm=T),MadreSoltera=mean(MadreSoltera,na.rm=T),
EduMadrePostPrimaria=mean(EduMadrePostPrimaria,na.rm=T),EduMadrePostSecundaria=mean(EduMadrePostSecundaria,na.rm=T)),by=FECHA_NAC] 

MeanDay$FECHA_NAC=as.Date(MeanDay$FECHA_NAC)
MeanDay=MeanDay[sort.int(MeanDay$FECHA_NAC, index.return = T)$ix,]

dias_filtro=31                           
Sequencia_LVerticales=seq(from=as.Date("01-01-1998",format="%d-%m-%Y"),to=as.Date("01-01-2013",format="%d-%m-%Y"),by="year")
pdf('Results/Graphs/peso.pdf')
plot(y=filter(MeanDay$PESO_NAC, rep(1/dias_filtro, dias_filtro)),x=MeanDay$FECHA_NAC,type="l",col=2,lwd=2,main="Average weight at birth \n 30 day moving average",xlab="Date",ylab="gr")
abline(v=Sequencia_LVerticales)
dev.off()

pdf('Results/Graphs/talla.pdf')
plot(y=filter(MeanDay$TALLA_NAC, rep(1/dias_filtro, dias_filtro)),x=MeanDay$FECHA_NAC,type="l",col=2,lwd=2,main="Average height at birth \n 30 day moving average",xlab="Date",ylab="cm")
abline(v=Sequencia_LVerticales)
dev.off()

pdf('Results/Graphs/consultas.pdf')
plot(y=filter(MeanDay$NUM_CONSUL, rep(1/dias_filtro, dias_filtro)),x=MeanDay$FECHA_NAC,type="l",col=2,lwd=2,main="Number of prenatal checks \n 30 day moving average",xlab="Date",ylab="No.")
abline(v=Sequencia_LVerticales)
dev.off()


pdf('Results/Graphs/edadMadre.pdf')
plot(y=filter(MeanDay$EDAD_MADRE, rep(1/dias_filtro, dias_filtro)),x=MeanDay$FECHA_NAC,type="l",col=2,lwd=2,main="Mother's age \n 30 day moving average",xlab="Date",ylab="Age")
abline(v=Sequencia_LVerticales)
dev.off()

pdf('Results/Graphs/gestacion.pdf')
plot(y=100*filter(MeanDay$GEST_COMPLETA, rep(1/dias_filtro, dias_filtro)),x=MeanDay$FECHA_NAC,type="l",col=2,lwd=2,main=">37 week gestation \n 30 day moving average",xlab="Date",ylab="%")
abline(v=Sequencia_LVerticales)
dev.off()

pdf('Results/Graphs/apgar.pdf')
plot(y=100*filter(MeanDay$APGAR_BAJO, rep(1/dias_filtro, dias_filtro)),x=MeanDay$FECHA_NAC,type="l",col=2,lwd=2,main="Low APGAR \n 30 day moving average",xlab="Date",ylab="%")
abline(v=Sequencia_LVerticales)
dev.off()

pdf('Results/Graphs/LBW.pdf')
plot(y=100*filter(MeanDay$LBW, rep(1/dias_filtro, dias_filtro)),x=MeanDay$FECHA_NAC,type="l",col=2,lwd=2,main="Low birth weight (<2500 gr) \n 30 day moving average",xlab="Date",ylab="%")
abline(v=Sequencia_LVerticales)
dev.off()

pdf('Results/Graphs/VLBW.pdf')
plot(y=100*filter(MeanDay$VLBW, rep(1/dias_filtro, dias_filtro)),x=MeanDay$FECHA_NAC,type="l",col=2,lwd=2,main="Very low birth weight (<1500 gr) \n 30 day moving average",xlab="Date",ylab="%")
abline(v=Sequencia_LVerticales)
dev.off()

pdf('Results/Graphs/MASC.pdf')
plot(y=filter(MeanDay$MASC, rep(1/dias_filtro, dias_filtro)),x=MeanDay$FECHA_NAC,type="l",col=2,lwd=2,main="Sex ratio (proportion of males) \n 30 day moving average",xlab="Date",ylab="")
abline(v=Sequencia_LVerticales)
dev.off()

 pdf('Results/Graphs/PartoEspontaneo.pdf')
plot(y=100*filter(MeanDay$PartoEspontaneo, rep(1/dias_filtro, dias_filtro)),x=MeanDay$FECHA_NAC,type="l",col=2,lwd=2,main="Espontanous birth \n 30 day moving average",xlab="Date",ylab="%")
abline(v=Sequencia_LVerticales)
dev.off()

pdf('Results/Graphs/Csection.pdf')
plot(y=100*filter(MeanDay$Csection, rep(1/dias_filtro, dias_filtro)),x=MeanDay$FECHA_NAC,type="l",col=2,lwd=2,main="C-section \n 30 day moving average",xlab="Date",ylab="%")
abline(v=Sequencia_LVerticales)
dev.off()

pdf('Results/Graphs/MultipleBirth.pdf')
plot(y=100*filter(MeanDay$MultipleBirth, rep(1/dias_filtro, dias_filtro)),x=MeanDay$FECHA_NAC,type="l",col=2,lwd=2,main="Multiple birth \n 30 day moving average",xlab="Date",ylab="%")
abline(v=Sequencia_LVerticales)
dev.off()

pdf('Results/Graphs/PartoHospital.pdf')
plot(y=100*filter(MeanDay$PartoHospital, rep(1/dias_filtro, dias_filtro)),x=MeanDay$FECHA_NAC,type="l",col=2,lwd=2,main="Birth at medical facility \n 30 day moving average",xlab="Date",ylab="%")
abline(v=Sequencia_LVerticales)
dev.off()

pdf('Results/Graphs/ConsultasPreMayor4.pdf')
plot(y=100*filter(MeanDay$ConsultasPreMayor4, rep(1/dias_filtro, dias_filtro)),x=MeanDay$FECHA_NAC,type="l",col=2,lwd=2,main="More than 4 prenatal checks \n 30 day moving average",xlab="Date",ylab="%")
abline(v=Sequencia_LVerticales)
dev.off()

pdf('Results/Graphs/RegimenContributivo.pdf')
plot(y=100*filter(MeanDay$RegimenContributivo, rep(1/dias_filtro, dias_filtro)),x=MeanDay$FECHA_NAC,type="l",col=2,lwd=2,main="Mother in contributory regime \n 30 day moving average",xlab="Date",ylab="%")
abline(v=Sequencia_LVerticales)
dev.off()

pdf('Results/Graphs/RegimenSubsidiado.pdf')
plot(y=100*filter(MeanDay$RegimenSubsidiado, rep(1/dias_filtro, dias_filtro)),x=MeanDay$FECHA_NAC,type="l",col=2,lwd=2,main="Mother in subsidze regime \n 30 day moving average",xlab="Date",ylab="%")
abline(v=Sequencia_LVerticales)
dev.off()

pdf('Results/Graphs/MadreSoltera.pdf')
plot(y=100*filter(MeanDay$MadreSoltera, rep(1/dias_filtro, dias_filtro)),x=MeanDay$FECHA_NAC,type="l",col=2,lwd=2,main="Mother is single \n 30 day moving average",xlab="Date",ylab="%")
abline(v=Sequencia_LVerticales)
dev.off()

pdf('Results/Graphs/EduMadrePostPrimaria.pdf')
plot(y=100*filter(MeanDay$EduMadrePostPrimaria, rep(1/dias_filtro, dias_filtro)),x=MeanDay$FECHA_NAC,type="l",col=2,lwd=2,main="Mother has some post-primary education \n 30 day moving average",xlab="Date",ylab="%")
abline(v=Sequencia_LVerticales)
dev.off()

pdf('Results/Graphs/EduMadrePostSecundaria.pdf')
plot(y=100*filter(MeanDay$EduMadrePostSecundaria, rep(1/dias_filtro, dias_filtro)),x=MeanDay$FECHA_NAC,type="l",col=2,lwd=2,main="Mother has some post-secondary education \n 30 day moving average",xlab="Date",ylab="%")
abline(v=Sequencia_LVerticales)
dev.off()

pdf('Results/Graphs/NacimientosDias.pdf')
plot(y=filter(MeanDay$Nacimientos, rep(1/dias_filtro, dias_filtro)),x=MeanDay$FECHA_NAC,type="l",col=2,lwd=2,main="Births per day \n Promedio movil de 30 dias",xlab="Date",ylab="")
abline(v=Sequencia_LVerticales)
dev.off()       

FunDescr=function(x){
cbind(mean(x,na.rm=T),sd(x,na.rm=T),min(x,na.rm=T),max(x,na.rm=T),sum(!is.na(x)))
}


VarNames=c("PESO_NAC","TALLA_NAC","NUM_CONSUL","EDAD_MADRE","GEST_COMPLETA","APGAR_BAJO","LBW","VLBW","MASC","PartoEspontaneo","Csection",
"MultipleBirth","PartoHospital","MadreMenor14","Madre14_17","ConsultasPreMayor4","RegimenContributivo","RegimenSubsidiado","MadreSoltera",
"EduMadrePostPrimaria","EduMadrePostSecundaria")
Tabla=rbind(FunDescr(MeanDay$Nacimientos),FunDescr(DataCompleta$MASC),FunDescr(DataCompleta$PartoHospital),FunDescr(DataCompleta$GEST_COMPLETA),
FunDescr(DataCompleta$PESO_NAC),FunDescr(DataCompleta$TALLA_NAC),FunDescr(DataCompleta$APGAR_BAJO),FunDescr(DataCompleta$LBW),
FunDescr(DataCompleta$VLBW),FunDescr(DataCompleta$Csection),FunDescr(DataCompleta$MultipleBirth),FunDescr(DataCompleta$ConsultasPreMayor4),
FunDescr(DataCompleta$MadreSoltera),FunDescr(DataCompleta$EduMadrePostPrimaria),FunDescr( DataCompleta$EDAD_MADRE))

Tabla=round(Tabla,4)

row.names(Tabla)=c("Births per day","Male","Medical Institution",">38 weeks","Weight (gr.)","Height (cm)","Low APGAR 1 Min",
"LBW(< 2500 gr)","VLBW (< 1500 gr)","C-section","Multiple Birth","> 4 prenatal checks","Single mother","Mother has post primary education","Mother's Age")
colnames(Tabla)=c("Mean","Std. Dev.","Min","Max","N")

print(xtable(Tabla,caption=paste("\\footnotesize Summary statistics for all births from 1999 to 2011. Births per day is the number of recorded babies born alive in a day,", 
"Male is an indicator equalt to one if the baby is male, Medical Institution is an indicator of whether the baby was born at a medical institution or not,",
"$>$ 38 weeks indicates whether the baby had a complete term of over 38 weeks, Weight is the weight of the newborn at birth, Height is the height of the newborn at birth,",
"Low APGAR 1 Min indicates whether the baby had an APGAR score below 7 measured 1 minute after birth,",
"LBW($>$ 2500 gr) indicates if the newborn had a weight below 2500 gr,VLBW ($>$ 1500 gr) indicates if the newborn had a weight below 1500 gr,",
"C-section indicates if a cesarean section was performed, Multiple Birth indicates multiple births, $>$ 4 prenatal checks is equal to one if the mother reports over 4 prenatal checks,",
"Single mother is equal to one if the mother self reports as single, Mother has post primary education indicates if the mother has any secondary or tertiary education,",
"Mother's Age is the mothers age in years. Source: DANE. Calculations: Authors."),
label="tab:sumBirths",digits=c(0,2,2,0,0,0),display=c("s","f","f","d","d","d"),align=c("l","c","c","c","c","c")),file="Results/LatexCodes/SummaryBirth.tex",table.placement="H", booktabs = TRUE)


load("CreatedData/TMC.RData")
TMC$Duration=as.numeric(difftime(TMC$FECHA_TERM,TMC$FECHA_INSC,unit="days"))/365
TMC$Duration[which(TMC$Duration>quantile(TMC$Duration,0.99))]=quantile(TMC$Duration,0.99)
TMC$Duration[which(TMC$Duration<0)]=0
TMC$AreaKm2=TMC$Area_m/(1000^2)
Tabla=rbind(FunDescr(TMC$ORO),FunDescr(TMC$CONCENCION),FunDescr(TMC$CONCENCION[which(TMC$ORO==1)]),
FunDescr(TMC$LEGALIZACION),FunDescr(TMC$LEGALIZACION[which(TMC$ORO==1)]),
FunDescr(TMC$PERSONA),FunDescr(TMC$PERSONA[which(TMC$ORO==1)]),FunDescr(TMC$Duration),FunDescr(TMC$Duration[which(TMC$ORO==1)]),
FunDescr(TMC$AreaKm2),FunDescr(TMC$AreaKm2[which(TMC$ORO==1)]))


Tabla=round(Tabla,4)
row.names(Tabla)=c("Gold Mine (=1)",
"Concession(=1)","Concession (=1) for gold mines",
"Legalization(=1)","Legalization (=1) for gold mines",
"Natural Person(=1)","Natural Person (=1) for gold mines",
"Duration (years)","Duration (years) for gold mines",
"Area (km2)","Area (km2) for gold mines")
colnames(Tabla)=c("Mean","Std. Dev.","Min","Max","N")

print(xtable(Tabla,caption="{\\footnotesize Summary statistics for all mine permits active at any point from the beginning of 1998 through June 2012. Gold Mine indicates whether the mining permit allows gold extraction; Concession indicates whether the mining permit is granted as part of an administrative concession; Legalization indicates whether the permit is granted as part of an effort to formalize illegal and small-scale; Natural Person indicates whether the permit is granted to an individual as opposed to a corporation; Duration is the duration of the gold permit in years; and Area is the area are of the gold mine in square kilometers. Source: Authors' calculations based on the Catastro Minero Colombiano.}",
label="tab:sumMines",digits=c(0,2,2,0,0,0),display=c("s","f","f","d","d","d"),align=c("l","c","c","c","c","c")),file="Results/LatexCodes/SummaryMines.tex",table.placement="H", booktabs = TRUE
)






load(file="CreatedData/AreaMinadaMuni.RData")
AreaMinadaTotal=colSums(MatrizAreaMinada)
pdf("Results/Graphs/TotalMinedArea.pdf")
plot(x=FechasVec,AreaMinadaTotal,type="l",main="Total mined area",ylab="km^2",xlab="Date",xlim=c(2001,2013))
dev.off()

load("CreatedData/PanelMunicipalEEVV.RData")
PanelMunicipalEEVV=data.frame(PanelMunicipalEEVV)
PanelMunicipalEEVV=PanelMunicipalEEVV[which(PanelMunicipalEEVV$ANO<=2013),]
PanelMunicipalEEVV$Date=PanelMunicipalEEVV$ANO+(PanelMunicipalEEVV$Quarter-1)/4
PanelMunicipalEEVV=PanelMunicipalEEVV[sort.int(PanelMunicipalEEVV$Date,index.return=T)$ix,]
CuantosMuni= aggregate(cbind(!is.na(PanelMunicipalEEVV$Produccion),!is.na(PanelMunicipalEEVV$AreaMinadaKm2)),by=list(PanelMunicipalEEVV$Date),FUN=sum)
MuniProd= aggregate(cbind(PanelMunicipalEEVV$Produccion>0,PanelMunicipalEEVV$AreaMinadaKm2>0),by=list(PanelMunicipalEEVV$Date),FUN=sum)
MuniProd[,c(2,3)]=MuniProd[,c(2,3)]/CuantosMuni[,c(2,3)]
ProdArea= aggregate(cbind(PanelMunicipalEEVV$Produccion,PanelMunicipalEEVV$AreaMinadaKm2),by=list(PanelMunicipalEEVV$Date),FUN=sum)

ProdAreaOnly=cbind(PanelMunicipalEEVV$AreaMinadaKm2,PanelMunicipalEEVV$Produccion/1000)
ProdAreaOnly=ProdAreaOnly[complete.cases(ProdAreaOnly),]
ProdAreaOnly=data.frame(ProdAreaOnly)
colnames(ProdAreaOnly)=c("AreaMinadaKm2","Produccion")
pdf("Results/Graphs/MiningAreaProduction_individual.pdf")
plot(ProdAreaOnly$AreaMinadaKm2,ProdAreaOnly$Produccion,type="p",xlab="Mined Area (Km^2)",ylab="Production (Kg)",main="Mining area and production")
abline(lm(ProdAreaOnly$Produccion~ProdAreaOnly$AreaMinadaKm2),lwd=2)
dev.off()

ProdAreaOnly$AreaPos=as.numeric(ProdAreaOnly$AreaMinadaKm2>0)
ProdAreaOnly$ProduccionPos=as.numeric(ProdAreaOnly$Produccion>0)
xtable(table(ProdAreaOnly$ProduccionPos,ProdAreaOnly$AreaPos))






pdf("Results/Graphs/ProdArea.pdf")
par(mar = c(5,5,2,5))
plot(x=ProdArea[complete.cases(ProdArea),1],y=ProdArea[complete.cases(ProdArea),2]/1000,type="b",pch=1,lty=1,col=1,main="Mining in terms of area and production",xlab="Date",
ylab="Kg",xlim=c(2001,2012))
par(new = T)
plot(x=ProdArea[complete.cases(ProdArea),1],y=ProdArea[complete.cases(ProdArea),3], ,type="b",pch=2,lty=2,col=2, axes = F, xlab = NA, ylab = NA,xlim=c(2001,2013))
axis(side = 4)
mtext(side = 4, line = 3, "Km^2")
legend("topleft",c("Production (Kg)","Mined area (km^2)"),col=c(1,2),pch=1:2,lty=1:2)
dev.off()

pdf("Results/Graphs/MiningMunicipalities.pdf")
par(mar = c(5,5,2,5))
plot(x=MuniProd[complete.cases(MuniProd),1],y=MuniProd[complete.cases(MuniProd),2],type="b",pch=1,lty=1,col=1,main="Municipalities with mining activity",xlab="Date",
ylab="Proportion of municipalities",ylim=range(c(MuniProd[,2],MuniProd[,3]),na.rm=T),xlim=c(2001,2012))
lines(x=MuniProd[complete.cases(MuniProd),1],y=MuniProd[complete.cases(MuniProd),3],type="b",pch=2,lty=2,col=2,xlim=c(2001,2013))
legend("bottomright",c(">0 Production",">0 Mined area"),col=c(1,2),pch=1:2,lty=1:2)
dev.off()



A=cbind(ddply(PanelMunicipalEEVV[which(!is.na(PanelMunicipalEEVV$PoblacionMuni)),], .(Date),    function(x) data.frame(wret=weighted.mean(x$SIVIGILA_PrevPer, x$PoblacionMuni,na.rm=T))),
ddply(PanelMunicipalEEVV[which(!is.na(PanelMunicipalEEVV$PoblacionMuni)),], .(Date),    function(x) data.frame(wret=weighted.mean(x$RIPS_CIE_MERC, x$PoblacionMuni,na.rm=T)))[,2])
A=A[which(!is.na(A[,2]) |  !is.na(A[,3])),]

pdf("Results/Graphs/RIPS_SIVI.pdf")
par(mar = c(5,5,2,5))
plot(x=A[,1],y=A[,2],type="b",pch=1,lty=1,col=1,main="Mercury related health events",xlab="Date",
ylab="SIVIGILA - rate per 100,000 individuals")
par(new = T)
plot(x=A[,1],y=A[,3], ,type="b",pch=2,lty=2,col=2, axes = F, xlab = NA, ylab = NA)
axis(side = 4)
mtext(side = 4, line = 3, "RIPS - Proportion of events")
legend("bottom",c("SIVIGILA - Heavy metal poisoning","RIPS - Proportion of mercury related events"),col=c(1,2),pch=1:2,lty=1:2,cex=0.7)
dev.off()




Tabla=rbind(FunDescr(PanelMunicipalEEVV$SIVIGILA_PrevPer),FunDescr(PanelMunicipalEEVV$RIPS_CIE_MERC))

Tabla=round(Tabla,4)
row.names(Tabla)=c("Heavy metal poisoning per 100,000 individuals", "Proportion of mercury related events")
colnames(Tabla)=c("Mean","Std. Dev.","Min","Max","N")

print(xtable(Tabla,caption="\\footnotesize Summary statistics for mercury realted health events. The first row uses SIVIGILA information and refers to the number of heavy metal poisoning per 100,000 individuals in all municipalities between 2007 and 2012. The second row uses RIPS information and has the proportion of mercury related events for all municipalities between 2009 and 2013. Source: Ministerio de Salud. Calculations: Authors.",
label="tab:sumSalud",digits=c(0,2,2,0,0,0),display=c("s","f","f","d","d","d"),align=c("l","c","c","c","c","c")),file="Results/LatexCodes/SummaryMinSalud.tex",table.placement="H", booktabs = TRUE
)

A1=ddply(PanelMunicipalEEVV[which(!is.na(PanelMunicipalEEVV$PoblacionMuni)),], .(Date),    function(x) data.frame(wret=weighted.mean(x$SIVIGILA_PrevPer, x$PoblacionMuni,na.rm=T)))
A2=aggregate(cbind(PanelMunicipalEEVV$Produccion,PanelMunicipalEEVV$AreaMinadaKm2),by=list(PanelMunicipalEEVV$Date),FUN=sum)
colnames(A2)=c("Date","Prod","Area")
A=merge(A1,A2)

A=A[complete.cases(A),]

pdf("Results/Graphs/Prod_SIVI.pdf")
par(mar = c(5,5,2,5))
plot(x=A[,1],y=A[,2],type="b",pch=1,lty=1,col=1,main="Gold mining activity and heavy metal poisoning",xlab="Date",
ylab="SIVIGILA - rate per 100,000 individuals")
par(new = T)
plot(x=A[,1],y=A[,3], ,type="b",pch=2,lty=2,col=2, axes = F, xlab = NA, ylab = NA)
axis(side = 4)
mtext(side = 4, line = 3, "Kg")
legend("bottom",c("SIVIGILA - Heavy metal poisoning","Gold Production (Kg)"),col=c(1,2),pch=1:2,lty=1:2,cex=0.7)
dev.off()

pdf("Results/Graphs/Area_SIVI.pdf")
par(mar = c(5,5,2,5))
plot(x=A[,1],y=A[,2],type="b",pch=1,lty=1,col=1,main="Gold mining activity and heavy metal poisoning",xlab="Date",
ylab="SIVIGILA - rate per 100,000 individuals")
par(new = T)
plot(x=A[,1],y=A[,4], ,type="b",pch=2,lty=2,col=2, axes = F, xlab = NA, ylab = NA)
axis(side = 4)
mtext(side = 4, line = 3, "Km^2")
legend("bottom",c("SIVIGILA - Heavy metal poisoning","Mining Area (Km^2)"),col=c(1,2),pch=1:2,lty=1:2,cex=0.7)
dev.off()

#CaptureIlegal= aggregate(cbind(PanelMunicipalEEVV$IntervMines),by=list(PanelMunicipalEEVV$Date),FUN=sum,na.rm=T)
#CaptureIlegal=CaptureIlegal[which(CaptureIlegal[,1]>=2010),]
#
#pdf("Results/Graphs/CaptureIlegal.pdf")
#plot(CaptureIlegal,type="l",main="Illegal mines found by the police",xlab="Date",ylab="Illegal mines")
#dev.off()
###This script calculates the mined area in each municipality and the cummulative mined area in each municipality, at each point in time.
##To do this it uses the mines_pedacitos file

load('CreatedData/Municipios_GIS.RData')
load("CreatedData/Mines_pedacitos.RData")

MatrizAreaMinada=matrix(0,ncol=length(unique(c(Mines_pedacitos$FECHA_INSC,Mines_pedacitos$FECHA_TERM))),nrow=length(unique(Municipios$CODANE2))) 
MunicipiosVec=unique(Municipios$CODANE2)
FechasVec=sort(unique(c(Mines_pedacitos$FECHA_INSC,Mines_pedacitos$FECHA_TERM)))


for(municipio in MunicipiosVec){
  indexMun=which(MunicipiosVec==municipio)
  INDEX=which(Mines_pedacitos$CODANE2==municipio)
  BaseMuni=Mines_pedacitos@data[INDEX,]
  if(dim(BaseMuni)[1]>0){
    for(columna in 1:dim(MatrizAreaMinada)[2]){
      INDEX_FECHA=which(BaseMuni$FECHA_INSC<=FechasVec[columna] &  BaseMuni$FECHA_TERM>FechasVec[columna])
      MatrizAreaMinada[indexMun,columna]=sum(BaseMuni$area_km2[INDEX_FECHA])
  }#close if
  }#col muni 
}#for muni


INDEX_FECHAS_RELEVANTES= which(FechasVec<as.Date("01-01-2015",format="%d-%m-%Y"))
MatrizAreaMinada=MatrizAreaMinada[,INDEX_FECHAS_RELEVANTES]
FechasVec=FechasVec[INDEX_FECHAS_RELEVANTES]

matplot(t(MatrizAreaMinada),type="l",lty=1,x=as.Date(FechasVec),ylab="km^2")       
save(FechasVec,MunicipiosVec,MatrizAreaMinada,file="CreatedData/AreaMinadaMuni.RData")

MatrizAreaMinadaAccumulada=apply(MatrizAreaMinada,2,cumsum)
save(FechasVec,MunicipiosVec,MatrizAreaMinadaAccumulada,file="CreatedData/MatrizAreaMinadaAccumulada.RData")

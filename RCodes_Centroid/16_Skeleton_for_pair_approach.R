foreach iminia in ListaMines{
Mandar un 1 rio abajo por 25km
Manadar -1 rio arriba por 25km
Si hay una bifurcacion mandar el -1 x ambas
Buffer con 1s y -1s a 25km
Zonal statistics by muni calculate max y min
Save max_imina, min_imina en una tabla cuyas filas son municipios
 y se siguen anadiendo columnas
Verificar que si un muni no esta ni rio arriba, ni rio abajo entonces quede en 0
}

gen total_mines_upstream=sum(max_*)
gen total_mines_downstream=-sum(min_*)

#Municipios ambivalentes
count if total_mines_upstream>0 & total_mines_downstream>0

#Municipios puro downstream from mines
count if total_mines_upstream>0 & total_mines_downstream==0

#Municipios puro upstream from mines
count if total_mines_upstream==0 & total_mines_downstream>0
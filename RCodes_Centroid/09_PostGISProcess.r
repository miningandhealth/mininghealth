 ###################
 ########### PARA AREA
 ###########################
 load('CreatedData/Municipios_GIS.RData')
 load("CreatedData/AreaMinadaMuni.RData")
 load("CreatedData/MatrizAreaMinadaAccumulada.RData")
 load("CreatedData/MINAS_ORO.RData")
 Hospitales_points=readOGR("RawData/GIS", "hospitals_gps")
 CodigosHospitales=sort(unique(as.character(Hospitales_points$cod_inst)))
FechasVec=sort(unique(c(MINAS_ORO$FECHA_INSC,MINAS_ORO$FECHA_TERM)))
INDEX_FECHAS_RELEVANTES= which(FechasVec<as.Date("01-01-2015",format="%d-%m-%Y"))
FechasVec=FechasVec[INDEX_FECHAS_RELEVANTES]
CodigosDane=unique(Municipios$CodigoDane)
 
#So this matrix has 3 dimension. dim 1 is the dates, dim 2 is the municipalities, dim 3 is the buffers (5, 10, 20 km)
MatrizExpoMuni=array(0,c(length(FechasVec),length(CodigosDane),3))
#So this matrix has 3 dimension. dim 1 is the dates, dim 2 is the hospitals, dim 3 is the buffers (5, 10, 20 km)
MatrizExpoHosp=array(0,c(length(FechasVec),length(CodigosHospitales),3))


for(fecha in FechasVec){
load(paste0("CreatedData/AreaFechas/AreaExpMuniFecha_",which(FechasVec==fecha),"_All.RData"))
load(paste0("CreatedData/AreaFechas/AreaExpHospFecha_",which(FechasVec==fecha),"_All.RData"))
MuniExposure=data.frame(MuniExposure)
HospExposure=data.frame(HospExposure)

IndexMatch=match(CodigosDane,as.numeric(as.character(MuniExposure$CODANE2)))
MatrizExpoMuni[which(FechasVec==fecha),,1]=MuniExposure[IndexMatch,"Buffer5km"]
MatrizExpoMuni[which(FechasVec==fecha),,2]=MuniExposure[IndexMatch,"Buffer10km"]
MatrizExpoMuni[which(FechasVec==fecha),,3]=MuniExposure[IndexMatch,"Buffer20km"]

IndexMatch=match(CodigosHospitales,as.numeric(as.character(HospExposure$cod_inst)))
MatrizExpoHosp[which(FechasVec==fecha),,1]=HospExposure[IndexMatch,"Buffer5km"]
MatrizExpoHosp[which(FechasVec==fecha),,2]=HospExposure[IndexMatch,"Buffer10km"]
MatrizExpoHosp[which(FechasVec==fecha),,3]=HospExposure[IndexMatch,"Buffer20km"]
}
MatrizExpoMuni[which(is.na(MatrizExpoMuni))]=0
MatrizExpoHosp[which(is.na(MatrizExpoHosp))]=0
matplot(MatrizExpoMuni[,,1],type="l",lty=1,x=FechasVec,ylab="km^2")
matplot(MatrizExpoHosp[,,1],type="l",lty=1,x=FechasVec,ylab="km^2")

save(CodigosDane,FechasVec,MatrizExpoMuni,file=paste0("CreatedData/MatrizExpoMuni.RData"))
save(CodigosHospitales,FechasVec,MatrizExpoHosp,file=paste0("CreatedData/MatrizExpoHosp.RData"))



 ###################
 ########### PARA PRODUCCION
 ###########################
 load('CreatedData/Municipios_GIS.RData')
 load("CreatedData/ProdPerMine.RData")
 Hospitales_points=readOGR("RawData/GIS", "hospitals_gps")
 CodigosHospitales=sort(unique(as.character(Hospitales_points$cod_inst)))
CodigosDane=unique(Municipios$CodigoDane)
 
#So this matrix has 3 dimension. dim 1 is the dates, dim 2 is the municipalities, dim 3 is the buffers (5, 10, 20 km)
MatrizProdMuni=array(0,c(length(FechasVec),length(CodigosDane),3))
#So this matrix has 3 dimension. dim 1 is the dates, dim 2 is the hospitals, dim 3 is the buffers (5, 10, 20 km)
MatrizProdHosp=array(0,c(length(FechasVec),length(CodigosHospitales),3))


for(fecha in FechasVec){
load(paste0("CreatedData/AreaFechas/ProdExpMuniFecha_",which(FechasVec==fecha),"_All.RData"))
load(paste0("CreatedData/AreaFechas/ProdExpHospFecha_",which(FechasVec==fecha),"_All.RData"))
MuniExposure=data.frame(MuniExposure)
HospExposure=data.frame(HospExposure)

IndexMatch=match(CodigosDane,as.numeric(as.character(MuniExposure$CODANE2)))
MatrizProdMuni[which(FechasVec==fecha),,1]=MuniExposure[IndexMatch,"Buffer5km"]
MatrizProdMuni[which(FechasVec==fecha),,2]=MuniExposure[IndexMatch,"Buffer10km"]
MatrizProdMuni[which(FechasVec==fecha),,3]=MuniExposure[IndexMatch,"Buffer20km"]

IndexMatch=match(CodigosHospitales,as.numeric(as.character(HospExposure$cod_inst)))
MatrizProdHosp[which(FechasVec==fecha),,1]=HospExposure[IndexMatch,"Buffer5km"]
MatrizProdHosp[which(FechasVec==fecha),,2]=HospExposure[IndexMatch,"Buffer10km"]
MatrizProdHosp[which(FechasVec==fecha),,3]=HospExposure[IndexMatch,"Buffer20km"]
}
##No need cause no NA's since production exists for each municipality
MatrizProdMuni[which(is.na(MatrizProdMuni))]=0
MatrizProdHosp[which(is.na(MatrizProdHosp))]=0
matplot(MatrizProdMuni[,,1],type="l",lty=1,x=FechasVec,ylab="km^2")
matplot(MatrizProdHosp[,,1],type="l",lty=1,x=FechasVec,ylab="km^2")

save(CodigosDane,FechasVec,MatrizProdMuni,file=paste0("CreatedData/MatrizProdMuni.RData"))
save(CodigosHospitales,FechasVec,MatrizProdHosp,file=paste0("CreatedData/MatrizProdHosp.RData"))


 ###################
 ########### PARA PRODUCCION A LA ANA MARIA IBANEZ
 ###########################
 load('CreatedData/Municipios_GIS.RData')
 load("CreatedData/AI_ProdPerMine.RData")
 Hospitales_points=readOGR("RawData/GIS", "hospitals_gps")
 CodigosHospitales=sort(unique(as.character(Hospitales_points$cod_inst)))
CodigosDane=unique(Municipios$CodigoDane)
 
#So this matrix has 3 dimension. dim 1 is the dates, dim 2 is the municipalities, dim 3 is the buffers (5, 10, 20 km)
MatrizProdMuni=array(0,c(length(FechasVec),length(CodigosDane),3))
#So this matrix has 3 dimension. dim 1 is the dates, dim 2 is the hospitals, dim 3 is the buffers (5, 10, 20 km)
MatrizProdHosp=array(0,c(length(FechasVec),length(CodigosHospitales),3))


for(fecha in FechasVec){
load(paste0("CreatedData/AreaFechas/AI_ProdExpMuniFecha_",which(FechasVec==fecha),"_All.RData"))
load(paste0("CreatedData/AreaFechas/AI_ProdExpHospFecha_",which(FechasVec==fecha),"_All.RData"))
MuniExposure=data.frame(MuniExposure)
HospExposure=data.frame(HospExposure)

IndexMatch=match(CodigosDane,as.numeric(as.character(MuniExposure$CODANE2)))
MatrizProdMuni[which(FechasVec==fecha),,1]=MuniExposure[IndexMatch,"Buffer5km"]
MatrizProdMuni[which(FechasVec==fecha),,2]=MuniExposure[IndexMatch,"Buffer10km"]
MatrizProdMuni[which(FechasVec==fecha),,3]=MuniExposure[IndexMatch,"Buffer20km"]

IndexMatch=match(CodigosHospitales,as.numeric(as.character(HospExposure$cod_inst)))
MatrizProdHosp[which(FechasVec==fecha),,1]=HospExposure[IndexMatch,"Buffer5km"]
MatrizProdHosp[which(FechasVec==fecha),,2]=HospExposure[IndexMatch,"Buffer10km"]
MatrizProdHosp[which(FechasVec==fecha),,3]=HospExposure[IndexMatch,"Buffer20km"]
}
##No need cause no NA's since production exists for each municipality
MatrizProdMuni[which(is.na(MatrizProdMuni))]=0
MatrizProdHosp[which(is.na(MatrizProdHosp))]=0
matplot(MatrizProdMuni[,,1],type="l",lty=1,x=FechasVec,ylab="km^2")
matplot(MatrizProdHosp[,,1],type="l",lty=1,x=FechasVec,ylab="km^2")

save(CodigosDane,FechasVec,MatrizProdMuni,file=paste0("CreatedData/AI_MatrizProdMuni.RData"))
save(CodigosHospitales,FechasVec,MatrizProdHosp,file=paste0("CreatedData/AI_MatrizProdHosp.RData"))


 ###################
 ########### PARA AREA UPSTREAM
 ########################### 
 load('CreatedData/Municipios_GIS.RData')
 load("CreatedData/AreaMinadaMuni.RData")
 load("CreatedData/MatrizAreaMinadaAccumulada.RData")
 load("CreatedData/MINAS_ORO.RData")
 Hospitales_points=readOGR("RawData/GIS", "hospitals_gps")
 CodigosHospitales=sort(unique(as.character(Hospitales_points$cod_inst)))
FechasVec=sort(unique(c(MINAS_ORO$FECHA_INSC,MINAS_ORO$FECHA_TERM)))
INDEX_FECHAS_RELEVANTES= which(FechasVec<as.Date("01-01-2015",format="%d-%m-%Y"))
FechasVec=FechasVec[INDEX_FECHAS_RELEVANTES]
CodigosDane=unique(Municipios$CodigoDane)
 
#So this matrix has 3 dimension. dim 1 is the dates, dim 2 is the municipalities, dim 3 is the buffers (5, 10, 20 km)
MatrizExpoUpstreamMuni=array(0,c(length(FechasVec),length(CodigosDane),3))
#So this matrix has 3 dimension. dim 1 is the dates, dim 2 is the hospitals, dim 3 is the buffers (5, 10, 20 km)
MatrizExpoUpstreamHosp=array(0,c(length(FechasVec),length(CodigosHospitales),3))


for(fecha in FechasVec){
load(paste0("CreatedData/AreaUpstreamRiver/PolExpMuniFecha_",which(FechasVec==fecha),"_All.RData"))
load(paste0("CreatedData/AreaUpstreamRiver/PolExpHospFecha_",which(FechasVec==fecha),"_All.RData"))
MuniExposure=data.frame(MuniExposure)
HospExposure=data.frame(HospExposure)

IndexMatch=match(CodigosDane,as.numeric(as.character(MuniExposure$CODANE2)))
MatrizExpoUpstreamMuni[which(FechasVec==fecha),,1]=MuniExposure[IndexMatch,"Buffer5km"]
MatrizExpoUpstreamMuni[which(FechasVec==fecha),,2]=MuniExposure[IndexMatch,"Buffer10km"]
MatrizExpoUpstreamMuni[which(FechasVec==fecha),,3]=MuniExposure[IndexMatch,"Buffer20km"]

IndexMatch=match(CodigosHospitales,as.numeric(as.character(HospExposure$cod_inst)))
MatrizExpoUpstreamHosp[which(FechasVec==fecha),,1]=HospExposure[IndexMatch,"Buffer5km"]
MatrizExpoUpstreamHosp[which(FechasVec==fecha),,2]=HospExposure[IndexMatch,"Buffer10km"]
MatrizExpoUpstreamHosp[which(FechasVec==fecha),,3]=HospExposure[IndexMatch,"Buffer20km"]
}
MatrizExpoUpstreamMuni[which(is.na(MatrizExpoUpstreamMuni))]=0
MatrizExpoUpstreamHosp[which(is.na(MatrizExpoUpstreamHosp))]=0
matplot(MatrizExpoUpstreamMuni[,,1],type="l",lty=1,x=FechasVec,ylab="km^2")
matplot(MatrizExpoUpstreamHosp[,,1],type="l",lty=1,x=FechasVec,ylab="km^2")

save(CodigosDane,FechasVec,MatrizExpoUpstreamMuni,file=paste0("CreatedData/MatrizExpoUpstreamMuni.RData"))
save(CodigosHospitales,FechasVec,MatrizExpoUpstreamHosp,file=paste0("CreatedData/MatrizExpoUpstreamHosp.RData"))



 ###################
 ########### PARA PRODUCCION UPSTREAM
 ###########################
 load('CreatedData/Municipios_GIS.RData')
 load("CreatedData/ProdPerMine.RData")
 Hospitales_points=readOGR("RawData/GIS", "hospitals_gps")
 CodigosHospitales=sort(unique(as.character(Hospitales_points$cod_inst)))
CodigosDane=unique(Municipios$CodigoDane)
 
#So this matrix has 3 dimension. dim 1 is the dates, dim 2 is the municipalities, dim 3 is the buffers (5, 10, 20 km)
MatrizProdUpstreamMuni=array(0,c(length(FechasVec),length(CodigosDane),3))
#So this matrix has 3 dimension. dim 1 is the dates, dim 2 is the hospitals, dim 3 is the buffers (5, 10, 20 km)
MatrizProdUpstreamHosp=array(0,c(length(FechasVec),length(CodigosHospitales),3))


for(fecha in FechasVec){
load(paste0("CreatedData/AreaUpstreamRiver/ProdUpstreamMuniFecha_",which(FechasVec==fecha),"_All.RData"))
load(paste0("CreatedData/AreaUpstreamRiver/ProdUpstreamHospFecha_",which(FechasVec==fecha),"_All.RData"))
MuniExposure=data.frame(MuniExposure)
HospExposure=data.frame(HospExposure)

IndexMatch=match(CodigosDane,as.numeric(as.character(MuniExposure$CODANE2)))
MatrizProdUpstreamMuni[which(FechasVec==fecha),,1]=MuniExposure[IndexMatch,"Buffer5km"]
MatrizProdUpstreamMuni[which(FechasVec==fecha),,2]=MuniExposure[IndexMatch,"Buffer10km"]
MatrizProdUpstreamMuni[which(FechasVec==fecha),,3]=MuniExposure[IndexMatch,"Buffer20km"]

IndexMatch=match(CodigosHospitales,as.numeric(as.character(HospExposure$cod_inst)))
MatrizProdUpstreamHosp[which(FechasVec==fecha),,1]=HospExposure[IndexMatch,"Buffer5km"]
MatrizProdUpstreamHosp[which(FechasVec==fecha),,2]=HospExposure[IndexMatch,"Buffer10km"]
MatrizProdUpstreamHosp[which(FechasVec==fecha),,3]=HospExposure[IndexMatch,"Buffer20km"]
}
##No need cause no NA's since production exists for each municipality
MatrizProdUpstreamMuni[which(is.na(MatrizProdUpstreamMuni))]=0
MatrizProdUpstreamHosp[which(is.na(MatrizProdUpstreamHosp))]=0
matplot(MatrizProdUpstreamMuni[,,1],type="l",lty=1,x=FechasVec,ylab="km^2")
matplot(MatrizProdUpstreamHosp[,,1],type="l",lty=1,x=FechasVec,ylab="km^2")

save(CodigosDane,FechasVec,MatrizProdUpstreamMuni,file=paste0("CreatedData/MatrizProdUpstreamMuni.RData"))
save(CodigosHospitales,FechasVec,MatrizProdUpstreamHosp,file=paste0("CreatedData/MatrizProdUpstreamHosp.RData"))


###################
 ########### PARA PRODUCCION UPSTREAM  A LA ANA MARIA IBANEZ
 ###########################
 load('CreatedData/Municipios_GIS.RData')
 load("CreatedData/AI_ProdPerMine.RData")
 Hospitales_points=readOGR("RawData/GIS", "hospitals_gps")
 CodigosHospitales=sort(unique(as.character(Hospitales_points$cod_inst)))
CodigosDane=unique(Municipios$CodigoDane)
 
#So this matrix has 3 dimension. dim 1 is the dates, dim 2 is the municipalities, dim 3 is the buffers (5, 10, 20 km)
MatrizProdUpstreamMuni=array(0,c(length(FechasVec),length(CodigosDane),3))
#So this matrix has 3 dimension. dim 1 is the dates, dim 2 is the hospitals, dim 3 is the buffers (5, 10, 20 km)
MatrizProdUpstreamHosp=array(0,c(length(FechasVec),length(CodigosHospitales),3))


for(fecha in FechasVec){
load(paste0("CreatedData/AreaUpstreamRiver/AI_ProdUpstreamMuniFecha_",which(FechasVec==fecha),"_All.RData"))
load(paste0("CreatedData/AreaUpstreamRiver/AI_ProdUpstreamHospFecha_",which(FechasVec==fecha),"_All.RData"))
MuniExposure=data.frame(MuniExposure)
HospExposure=data.frame(HospExposure)

IndexMatch=match(CodigosDane,as.numeric(as.character(MuniExposure$CODANE2)))
MatrizProdUpstreamMuni[which(FechasVec==fecha),,1]=MuniExposure[IndexMatch,"Buffer5km"]
MatrizProdUpstreamMuni[which(FechasVec==fecha),,2]=MuniExposure[IndexMatch,"Buffer10km"]
MatrizProdUpstreamMuni[which(FechasVec==fecha),,3]=MuniExposure[IndexMatch,"Buffer20km"]

IndexMatch=match(CodigosHospitales,as.numeric(as.character(HospExposure$cod_inst)))
MatrizProdUpstreamHosp[which(FechasVec==fecha),,1]=HospExposure[IndexMatch,"Buffer5km"]
MatrizProdUpstreamHosp[which(FechasVec==fecha),,2]=HospExposure[IndexMatch,"Buffer10km"]
MatrizProdUpstreamHosp[which(FechasVec==fecha),,3]=HospExposure[IndexMatch,"Buffer20km"]
}
##No need cause no NA's since production exists for each municipality
MatrizProdUpstreamMuni[which(is.na(MatrizProdUpstreamMuni))]=0
MatrizProdUpstreamHosp[which(is.na(MatrizProdUpstreamHosp))]=0
matplot(MatrizProdUpstreamMuni[,,1],type="l",lty=1,x=FechasVec,ylab="km^2")
matplot(MatrizProdUpstreamHosp[,,1],type="l",lty=1,x=FechasVec,ylab="km^2")

save(CodigosDane,FechasVec,MatrizProdUpstreamMuni,file=paste0("CreatedData/AI_MatrizProdUpstreamMuni.RData"))
save(CodigosHospitales,FechasVec,MatrizProdUpstreamHosp,file=paste0("CreatedData/AI_MatrizProdUpstreamHosp.RData"))

#load(paste0("CreatedData/AI_MatrizProdUpstreamMuni.RData"))
#AI_MatrizProdUpstreamMuni=MatrizProdUpstreamMuni
#load(paste0("CreatedData/MatrizProdUpstreamMuni.RData"))
#identical(AI_MatrizProdUpstreamMuni,MatrizProdUpstreamMuni)
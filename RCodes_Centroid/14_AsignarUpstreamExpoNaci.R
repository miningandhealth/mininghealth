##############
################AREA MUNI MADRE 5KM
###############################
load("CreatedData/MatrizExpoUpstreamMuni.RData")
nombresVars=c("UpstreamAreaMuni5KM","UpstreamAreaMuni10KM","UpstreamAreaMuni20KM")
load('CreatedData/DataCompletaIntensity.RData')
for(i in 1:3){
MatrizExpo=MatrizExpoUpstreamMuni[,,i]
MatrizExpo=t(MatrizExpo)
MunicipiosSinMineria=setdiff(unique(DataCompleta$CODIGO_DANE_M), CodigosDane[which(rowSums(MatrizExpo)!=0)])
IndexSinMineria=which(DataCompleta$CODIGO_DANE_M %in%  MunicipiosSinMineria)
DataCompleta[,nombresVars[i]]=NA
DataCompleta[IndexSinMineria,nombresVars[i]]=0
IndexConMineria=setdiff(1:dim(DataCompleta)[1],IndexSinMineria)

DataCompletaSinMineria=DataCompleta[IndexSinMineria,]
DataCompletaMineria=DataCompleta[IndexConMineria,]
rm(DataCompleta)
PosiblesNac=unique(DataCompletaMineria[,"FECHA_NAC"])
for(fecha_nac in PosiblesNac){
#temp=Sys.time()
INDEXOBS=which(DataCompletaMineria[,"FECHA_NAC"]==fecha_nac)
t_0=max(which(FechasVec<=(fecha_nac-280)))
t_f=max(which(FechasVec<(fecha_nac)))
FechasRelevantes=c(FechasVec[t_0:t_f],fecha_nac)
pesos=as.numeric(diff(FechasRelevantes))
CodigosTotales=DataCompletaMineria$CODIGO_DANE_M[INDEXOBS]
CodigosUnicos=unique(CodigosTotales)
MatrizRelevante=MatrizExpo[match(CodigosUnicos,CodigosDane),t_0:t_f]
if(is.null(dim(MatrizRelevante)))  MatrizRelevante=matrix(MatrizRelevante,nrow=1)
DataCompletaMineria[INDEXOBS,nombresVars[i]]=apply(MatrizRelevante,1,weighted.mean,w=pesos)[match(CodigosTotales,CodigosUnicos)]
}
DataCompleta=rbind(DataCompletaSinMineria,DataCompletaMineria)
}



##############
################ PROD MUNI MADRE 5KM
###############################
load("CreatedData/MatrizProdUpstreamMuni.RData")
nombresVars=c("UpstreamProdMuni5KM","UpstreamProdMuni10KM","UpstreamProdMuni20KM")
for(i in 1:3){
MatrizExpoProd=MatrizProdUpstreamMuni[,,i]
MatrizExpoProd=t(MatrizExpoProd)
MunicipiosSinMineria=setdiff(unique(DataCompleta$CODIGO_DANE_M), CodigosDane[which(rowSums(MatrizExpoProd)!=0)])
IndexSinMineria=which(DataCompleta$CODIGO_DANE_M %in%  MunicipiosSinMineria)
DataCompleta[,nombresVars[i]]=NA
DataCompleta[intersect(IndexSinMineria,which(DataCompleta$FECHA_NAC>as.Date("2001-10-08",format="%Y-%m-%d"))),nombresVars[i]]=0
IndexConMineria=setdiff(1:dim(DataCompleta)[1],IndexSinMineria)

DataCompletaSinMineria=DataCompleta[IndexSinMineria,]
DataCompletaMineria=DataCompleta[IndexConMineria,]
rm(DataCompleta)
PosiblesNac=unique(DataCompletaMineria[,"FECHA_NAC"])
PosiblesNac=PosiblesNac[PosiblesNac>as.Date("2001-10-08",format="%Y-%m-%d")]
for(fecha_nac in PosiblesNac){
#temp=Sys.time()
INDEXOBS=which(DataCompletaMineria[,"FECHA_NAC"]==fecha_nac)
t_0=max(which(FechasVec<=(fecha_nac-280)))
t_f=max(which(FechasVec<(fecha_nac)))
FechasRelevantes=c(FechasVec[t_0:t_f],fecha_nac)
pesos=as.numeric(diff(FechasRelevantes))
CodigosTotales=DataCompletaMineria$CODIGO_DANE_M[INDEXOBS]
CodigosUnicos=unique(CodigosTotales)
MatrizRelevante=MatrizExpoProd[match(CodigosUnicos,CodigosDane),t_0:t_f]
if(is.null(dim(MatrizRelevante)))  MatrizRelevante=matrix(MatrizRelevante,nrow=1)
DataCompletaMineria[INDEXOBS,nombresVars[i]]=  apply(MatrizRelevante,1,weighted.mean,w=pesos)[match(CodigosTotales,CodigosUnicos)]
}
DataCompleta=rbind(DataCompletaSinMineria,DataCompletaMineria)
}




##############
################ PROD A LA AI MUNI MADRE 5KM
###############################
load("CreatedData/AI_MatrizProdUpstreamMuni.RData")
nombresVars=c("AI_UpstreamProdMuni5KM","AI_UpstreamProdMuni10KM","AI_UpstreamProdMuni20KM")
for(i in 1:3){
MatrizExpoProd=MatrizProdUpstreamMuni[,,i]
MatrizExpoProd=t(MatrizExpoProd)
MunicipiosSinMineria=setdiff(unique(DataCompleta$CODIGO_DANE_M), CodigosDane[which(rowSums(MatrizExpoProd)!=0)])
IndexSinMineria=which(DataCompleta$CODIGO_DANE_M %in%  MunicipiosSinMineria)
DataCompleta[,nombresVars[i]]=NA
DataCompleta[intersect(IndexSinMineria,which(DataCompleta$FECHA_NAC>as.Date("2001-10-08",format="%Y-%m-%d"))),nombresVars[i]]=0
IndexConMineria=setdiff(1:dim(DataCompleta)[1],IndexSinMineria)

DataCompletaSinMineria=DataCompleta[IndexSinMineria,]
DataCompletaMineria=DataCompleta[IndexConMineria,]
rm(DataCompleta)
PosiblesNac=unique(DataCompletaMineria[,"FECHA_NAC"])
PosiblesNac=PosiblesNac[PosiblesNac>as.Date("2001-10-08",format="%Y-%m-%d")]
for(fecha_nac in PosiblesNac){
#temp=Sys.time()
INDEXOBS=which(DataCompletaMineria[,"FECHA_NAC"]==fecha_nac)
t_0=max(which(FechasVec<=(fecha_nac-280)))
t_f=max(which(FechasVec<(fecha_nac)))
FechasRelevantes=c(FechasVec[t_0:t_f],fecha_nac)
pesos=as.numeric(diff(FechasRelevantes))
CodigosTotales=DataCompletaMineria$CODIGO_DANE_M[INDEXOBS]
CodigosUnicos=unique(CodigosTotales)
MatrizRelevante=MatrizExpoProd[match(CodigosUnicos,CodigosDane),t_0:t_f]
if(is.null(dim(MatrizRelevante)))  MatrizRelevante=matrix(MatrizRelevante,nrow=1)
DataCompletaMineria[INDEXOBS,nombresVars[i]]=  apply(MatrizRelevante,1,weighted.mean,w=pesos)[match(CodigosTotales,CodigosUnicos)]
}
DataCompleta=rbind(DataCompletaSinMineria,DataCompletaMineria)
}


save(DataCompleta,file='CreatedData/DataCompletaIntensity.RData')
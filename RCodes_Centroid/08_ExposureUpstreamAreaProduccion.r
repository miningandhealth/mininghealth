availableCores<-detectCores()-1
no_cores <- max(1, availableCores)
# Initiate cluster
cl <- makeCluster(no_cores)
registerDoParallel(cl)
#set seed for reproducible results
clusterSetRNGStream(cl,123) 

load("CreatedData/RasterizedStuff.RData")
load("CreatedData/COL_riv_proj.RData")


  ############ PARA AREA  
ArchiviosMinasActivas=dir("CreatedData/AreaUpstreamRiver", pattern="AreaMinadaEdgeFecha_", full.names = TRUE)
ArchiviosMinasActivas=sort(ArchiviosMinasActivas) 

  foreach(archivo=ArchiviosMinasActivas,.packages=c("sp","raster","rgeos","foreign")) %dopar% {
      #Select the mines
    t0=Sys.time()
    fechatable=read.dbf(archivo)
    riverfile_merge=merge(COL_riv_proj,fechatable)
    riverfile_merge = subset(riverfile_merge, !is.na(riverfile_merge@data$polucion))
    
    riverfile_merge=riverfile_merge[,c("id_edge","polucion")]
    riverfile_buffer1 <- gBuffer(riverfile_merge, width=5*1000,byid=T,capStyle="FLAT") #units are in mts
    riverfile_buffer2 <- gBuffer(riverfile_merge, width=10*1000,byid=T,capStyle="FLAT") #units are in mts
    riverfile_buffer3 <- gBuffer(riverfile_merge, width=20*1000,byid=T,capStyle="FLAT") #units are in mts
    RasterGold1=rasterize(riverfile_buffer1,RasterPop,field="polucion",background=0)
    RasterGold2=rasterize(riverfile_buffer2,RasterPop,field="polucion",background=0)
    RasterGold3=rasterize(riverfile_buffer3,RasterPop,field="polucion",background=0)   
    RasterExposure1 <- overlay(RasterGold1, RasterPop, fun=function(x,y){return(x*y)})
    RasterExposure2 <- overlay(RasterGold2, RasterPop, fun=function(x,y){return(x*y)})
    RasterExposure3 <- overlay(RasterGold3, RasterPop, fun=function(x,y){return(x*y)}) 
    RasterExposure=brick(RasterExposure1,RasterExposure2,RasterExposure3)
    
    MuniExposure=zonal(RasterExposure, MuniRaster, fun='sum')
    MuniExposure[,c(2:4)]=MuniExposure[,(2:4)]/PopMuniRaster[,c(2,2,2)]
    HospExposureSingle=zonal(RasterExposure, HospRaster, fun='sum')
    HospExposureSingle[,c(2:4)]=HospExposureSingle[,(2:4)]/PopHospRaster[,c(2,2,2)]
    #MuniExposure2=zonal(RasterExposure, MuniRaster2, fun='mean')
    colnames(MuniExposure)=c("CODANE2","Buffer5km","Buffer10km","Buffer20km")
    colnames(HospExposureSingle)=c("ID_HOSP_INTERNO","Buffer5km","Buffer10km","Buffer20km")
    MergeTemp=merge(HospExposureSingle,Hospitales_points_dataframe_unique,by="ID_HOSP_INTERNO")
    MergeTemp=MergeTemp[,c("coords.x1","coords.x2","Buffer5km","Buffer10km","Buffer20km")]
    MergeTemp=merge(MergeTemp,Hospitales_points_dataframe[,c("cod_inst","coords.x1","coords.x2")])
    HospExposure=MergeTemp[,c("cod_inst","Buffer5km","Buffer10km","Buffer20km")]
    save(MuniExposure,file=paste0("CreatedData/AreaUpstreamRiver/PolExpMuniFecha_",gsub(".dbf","",gsub("CreatedData/AreaUpstreamRiver/AreaMinadaEdgeFecha_","",archivo)),"_All.RData"))
    save(HospExposure,file=paste0("CreatedData/AreaUpstreamRiver/PolExpHospFecha_",gsub(".dbf","",gsub("CreatedData/AreaUpstreamRiver/AreaMinadaEdgeFecha_","",archivo)),"_All.RData"))

    tf=Sys.time()
    print(tf-t0)
    gc()
    rm(goldfile_merge,fechatable,goldfile_buffer1,goldfile_buffer2,goldfile_buffer3,RasterGold1,RasterGold2,RasterGold3,RasterExposure1,RasterExposure2,RasterExposure3,RasterExposure,MuniExposure,HospExposure)

  
  }

  
  ############ PARA PRODUCCION
  

ArchiviosMinasActivas=dir("CreatedData/AreaUpstreamRiver", pattern="^ProdMinadaEdgeFecha_", full.names = TRUE)
ArchiviosMinasActivas=sort(ArchiviosMinasActivas) 

  foreach(archivo=ArchiviosMinasActivas,.packages=c("sp","raster","rgeos","foreign")) %dopar% {
      #Select the mines
    t0=Sys.time()
    fechatable=read.dbf(archivo)
    riverfile_merge=merge(COL_riv_proj,fechatable)
    riverfile_merge = subset(riverfile_merge, !is.na(riverfile_merge@data$polucion))
    
    riverfile_merge=riverfile_merge[,c("id_edge","polucion")]
    riverfile_buffer1 <- gBuffer(riverfile_merge, width=5*1000,byid=T,capStyle="FLAT") #units are in mts
    riverfile_buffer2 <- gBuffer(riverfile_merge, width=10*1000,byid=T,capStyle="FLAT") #units are in mts
    riverfile_buffer3 <- gBuffer(riverfile_merge, width=20*1000,byid=T,capStyle="FLAT") #units are in mts
    RasterGold1=rasterize(riverfile_buffer1,RasterPop,field="polucion",background=0)
    RasterGold2=rasterize(riverfile_buffer2,RasterPop,field="polucion",background=0)
    RasterGold3=rasterize(riverfile_buffer3,RasterPop,field="polucion",background=0)   
    RasterExposure1 <- overlay(RasterGold1, RasterPop, fun=function(x,y){return(x*y)})
    RasterExposure2 <- overlay(RasterGold2, RasterPop, fun=function(x,y){return(x*y)})
    RasterExposure3 <- overlay(RasterGold3, RasterPop, fun=function(x,y){return(x*y)}) 
    RasterExposure=brick(RasterExposure1,RasterExposure2,RasterExposure3)
    
    
    MuniExposure=zonal(RasterExposure, MuniRaster, fun='sum')
    MuniExposure[,c(2:4)]=MuniExposure[,(2:4)]/PopMuniRaster[,c(2,2,2)]
    HospExposureSingle=zonal(RasterExposure, HospRaster, fun='sum')
    HospExposureSingle[,c(2:4)]=HospExposureSingle[,(2:4)]/PopHospRaster[,c(2,2,2)]
    #MuniExposure2=zonal(RasterExposure, MuniRaster2, fun='mean')
    colnames(MuniExposure)=c("CODANE2","Buffer5km","Buffer10km","Buffer20km")
    colnames(HospExposureSingle)=c("ID_HOSP_INTERNO","Buffer5km","Buffer10km","Buffer20km")
    MergeTemp=merge(HospExposureSingle,Hospitales_points_dataframe_unique,by="ID_HOSP_INTERNO")
    MergeTemp=MergeTemp[,c("coords.x1","coords.x2","Buffer5km","Buffer10km","Buffer20km")]
    MergeTemp=merge(MergeTemp,Hospitales_points_dataframe[,c("cod_inst","coords.x1","coords.x2")])
    HospExposure=MergeTemp[,c("cod_inst","Buffer5km","Buffer10km","Buffer20km")]
    
        save(MuniExposure,file=paste0("CreatedData/AreaUpstreamRiver/ProdUpstreamMuniFecha_",gsub(".dbf","",gsub("CreatedData/AreaUpstreamRiver/ProdMinadaEdgeFecha_","",archivo)),"_All.RData"))
    save(HospExposure,file=paste0("CreatedData/AreaUpstreamRiver/ProdUpstreamHospFecha_",gsub(".dbf","",gsub("CreatedData/AreaUpstreamRiver/ProdMinadaEdgeFecha_","",archivo)),"_All.RData"))

    
    
    tf=Sys.time()
    print(tf-t0)
    gc()
    rm(goldfile_merge,fechatable,goldfile_buffer1,goldfile_buffer2,goldfile_buffer3,RasterGold1,RasterGold2,RasterGold3,RasterExposure1,RasterExposure2,RasterExposure3,RasterExposure,MuniExposure, HospExposureSingle,MergeTemp,HospExposure)

  }
  
  
   ############ PARA PRODUCCION A LA AI
  

ArchiviosMinasActivas=dir("CreatedData/AreaUpstreamRiver", pattern="AI_ProdMinadaEdgeFecha_", full.names = TRUE)
ArchiviosMinasActivas=sort(ArchiviosMinasActivas) 

  foreach(archivo=ArchiviosMinasActivas,.packages=c("sp","raster","rgeos","foreign")) %dopar% {
      #Select the mines
    t0=Sys.time()
    fechatable=read.dbf(archivo)
    riverfile_merge=merge(COL_riv_proj,fechatable)
    riverfile_merge = subset(riverfile_merge, !is.na(riverfile_merge@data$polucion))
    
    riverfile_merge=riverfile_merge[,c("id_edge","polucion")]
    riverfile_buffer1 <- gBuffer(riverfile_merge, width=5*1000,byid=T,capStyle="FLAT") #units are in mts
    riverfile_buffer2 <- gBuffer(riverfile_merge, width=10*1000,byid=T,capStyle="FLAT") #units are in mts
    riverfile_buffer3 <- gBuffer(riverfile_merge, width=20*1000,byid=T,capStyle="FLAT") #units are in mts
    RasterGold1=rasterize(riverfile_buffer1,RasterPop,field="polucion",background=0)
    RasterGold2=rasterize(riverfile_buffer2,RasterPop,field="polucion",background=0)
    RasterGold3=rasterize(riverfile_buffer3,RasterPop,field="polucion",background=0)   
    RasterExposure1 <- overlay(RasterGold1, RasterPop, fun=function(x,y){return(x*y)})
    RasterExposure2 <- overlay(RasterGold2, RasterPop, fun=function(x,y){return(x*y)})
    RasterExposure3 <- overlay(RasterGold3, RasterPop, fun=function(x,y){return(x*y)}) 
    RasterExposure=brick(RasterExposure1,RasterExposure2,RasterExposure3)
    
    
    MuniExposure=zonal(RasterExposure, MuniRaster, fun='sum')
    MuniExposure[,c(2:4)]=MuniExposure[,(2:4)]/PopMuniRaster[,c(2,2,2)]
    HospExposureSingle=zonal(RasterExposure, HospRaster, fun='sum')
    HospExposureSingle[,c(2:4)]=HospExposureSingle[,(2:4)]/PopHospRaster[,c(2,2,2)]
    #MuniExposure2=zonal(RasterExposure, MuniRaster2, fun='mean')
    colnames(MuniExposure)=c("CODANE2","Buffer5km","Buffer10km","Buffer20km")
    colnames(HospExposureSingle)=c("ID_HOSP_INTERNO","Buffer5km","Buffer10km","Buffer20km")
    MergeTemp=merge(HospExposureSingle,Hospitales_points_dataframe_unique,by="ID_HOSP_INTERNO")
    MergeTemp=MergeTemp[,c("coords.x1","coords.x2","Buffer5km","Buffer10km","Buffer20km")]
    MergeTemp=merge(MergeTemp,Hospitales_points_dataframe[,c("cod_inst","coords.x1","coords.x2")])
    HospExposure=MergeTemp[,c("cod_inst","Buffer5km","Buffer10km","Buffer20km")]
    
        save(MuniExposure,file=paste0("CreatedData/AreaUpstreamRiver/AI_ProdUpstreamMuniFecha_",gsub(".dbf","",gsub("CreatedData/AreaUpstreamRiver/AI_ProdMinadaEdgeFecha_","",archivo)),"_All.RData"))
    save(HospExposure,file=paste0("CreatedData/AreaUpstreamRiver/AI_ProdUpstreamHospFecha_",gsub(".dbf","",gsub("CreatedData/AreaUpstreamRiver/AI_ProdMinadaEdgeFecha_","",archivo)),"_All.RData"))

    
    
    tf=Sys.time()
    print(tf-t0)
    gc()
    rm(goldfile_merge,fechatable,goldfile_buffer1,goldfile_buffer2,goldfile_buffer3,RasterGold1,RasterGold2,RasterGold3,RasterExposure1,RasterExposure2,RasterExposure3,RasterExposure,MuniExposure, HospExposureSingle,MergeTemp,HospExposure)

  }
  
    stopCluster(cl)
stopImplicitCluster()

